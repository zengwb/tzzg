<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onclick="forbidden(this,'<?php echo ($row['linkurl']); ?>')"   class="wordcolor first" style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"><?php echo ($row['name']); ?></a>
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  style="font-weight:<?php echo ($val['style']['bold']); ?>; color:<?php echo ($val['style']['color']); ?>;"><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                function forbidden(event,url){
                    var l = $(event).next("ul").find("li").length;
                    if(url == ""){
                        return false;
                    }else{
                        window.open(url);
                    }
                }
                $(function() {
                    var navi = $(".nav-list>ul>li");
                    navi.each(function() {
                        if ($(this).find("ul>li").length) {
                        } else {
                            $(this).find("i, ul").hide();
                        }
                    });
                    navi.find("a").each(function(){
                        if($(this).attr('naviid') == 1034){
                            $(this).removeClass("wordcolor");
                        }
                    });

                /*禁止点击未开启功能  start*/
                    // $(".nav-list>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     //if(id != 1039 && id != 1034 && id != 1038 && id != 1035){

                    //     if(id != 1034 && id != 1038 && id != 1035 && id != 1059){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });

                    // $(".nav-list>ul>li>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     if(id != 1037 && id != 1039 && id != 1040 && id != 1041 && id != 1044 && id != 1036 && id != 1045 && id != 1065){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    /*禁止点击未开启功能  end*/
                });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />
    <!-- 政务详情 -->
	<div class="tab-content" style="margin-top: 200px;" data-typeid="<?php echo ($typeid); ?>" data-mid="<?php echo ($mid); ?>">
			<div class="wrapper" data-unitid="<?php echo ($units["unitid"]); ?>">
				<ul class="tab">
                    <?php if(is_array($agency)): $i = 0; $__LIST__ = $agency;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($units['pid'] == $vo['unitid']): ?><li class="tab-item t-active" data-unitid="<?php echo ($vo["unitid"]); ?>"><?php echo ($vo["name"]); ?></li>
                        <?php else: ?>
                            <li class="tab-item" data-unitid="<?php echo ($vo["unitid"]); ?>"><?php echo ($vo["name"]); ?></li><?php endif; endforeach; endif; else: echo "" ;endif; ?>

				</ul>
			</div>
			<div class="department">
				<div class="main selected">

				</div>
			</div>
			<div class="province">
				<div class="pro-content">
                    <?php if(is_array($city)): $i = 0; $__LIST__ = $city;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><span><a href="javascript:;" data-cityid="<?php echo ($vo["linkageid"]); ?>" ><?php echo ($vo["name"]); ?></a></span><span>|</span><?php endforeach; endif; else: echo "" ;endif; ?>
				</div>
			</div>
		</div>
    <div class="goverment-body">
        <div class="goverment-nav floatleft">
            <ul>
				<?php $_parentid = 4024;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) : if($typeid == $row['linkageid']): ?><li class="tab-item red" data-linkid="<?php echo ($row['linkageid']); ?>" onClick="clickCat(this,<?php echo ($row["linkageid"]); ?>)"><span class="red-fk"></span><?php echo ($row['name']); ?></li>
                    <?php else: ?>
                        <li class="tab-item" data-linkid="<?php echo ($row['linkageid']); ?>" onClick="clickCat(this,<?php echo ($row["linkageid"]); ?>)"><span class="red-fk"></span><?php echo ($row['name']); ?></li><?php endif; endforeach; ?>
            </ul>
        </div>

        <div class="govermentdetail-content floatright wid8">
            <?php if($typeid != 4030): ?><div class="content-top">
                    <h2 class="content-title"><?php echo ($data["title"]); ?></h2>
                    <p class="contenr-date">发布时间：<?php echo (date("Y-m-d",$data["inputtime"])); ?></p>
                </div>
                <div class="goverment-content">
                    <?php echo ($data["content"]); ?>
                    <?php if($data["copyfrom"] != ''): ?><br/>
                        <p style="text-indent: 2em;font-size: 14px;">(转载自：<?php echo ($data["copyfrom"]); ?>)</p><?php endif; ?>
                </div>
                <p class="content-tiaozhuan">
                    <a href="javascript:nextprivs('<?php echo ($privs["mid"]); ?>')">上一篇：<?php echo ((isset($privs["title"]) && ($privs["title"] !== ""))?($privs["title"]):'没有数据！'); ?></a>
                    <a href="javascript:nextprivs('<?php echo ($next["mid"]); ?>')">下一篇：<?php echo ((isset($next["title"]) && ($next["title"] !== ""))?($next["title"]):'没有数据！'); ?></a>
                </p><?php endif; ?>
        </div>

        <div class="commissions-list no-mar wid9 floatright">
            <ul class="fb-tab selected">

            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <!-- 尾部 -->
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

	<script type="text/javascript">
		$(function () {
			$(".wrapper .tab-item").click(function () {
				$(this).addClass("t-active").siblings().removeClass("t-active");
				$(".department .main").eq($(this).index()).show().siblings().hide();
			});

			/*****加载省级机构下的部门**/
			var provinceid = $($(".wrapper>ul").find("li.t-active")).data("unitid");
            $.ajax({
                url:"/index.php?m=home&c=index&a=ajaxAgency",
                type:"get",
                data:{"parentid":provinceid},
                success:function(res){
                    if(res.code == 200 && res.data.length > 0){
                        var item = "";
                        for (var i = 0; i < res.data.length; i++){
                            item += "<p><a onclick='clickBmen(this,"+res.data[i].unitid+")' data-unitid='"+res.data[i].unitid+"'>"+res.data[i].name+"</a></p>";
                        }

                        $("div.department>div.main").html(item);

                        var unit = $("div.wrapper").data("unitid");
                        $("div.department>div.main>p").find("a").each(function(){
                           if($(this).data("unitid") == unit){
                               $(this).addClass("red");
                           }
                        });
                    }
                }
            });


            /**
             *  机构切换
             **/
            $("div.wrapper>ul").on("click", "li", function(){
                var data = {'parentid':$(this).data("unitid")};
                ajaxAgency(data);
                var params = getParams();
                getData(params);
            });

            /**
             * 如果分类是项目展示,则加载
             **/
            if($("div.tab-content").data("typeid") == 4030){
                var  data= getParams();
                var linkageid = $("div.wrapper>ul").find("li.t-active").data("unitid");
                data.units = linkageid + "," + $("div.wrapper").data("unitid");
                data.typeid = 4030;
                data['mid'] = $("div.tab-content").data("mid");
                getProjectDetail(data);
            }
		});

        /**
         * 上下篇
         */
        function nextprivs(mid){
            if(mid == undefined){
                alert("没有数据！");
                return false;
            }else{
                var units = $($("div.wrapper>ul").find("li.t-active")).data("unitid") + "," + $("div.wrapper").data("unitid");
                var typeid = $($("div.goverment-nav>ul").find("li.red")).data("linkid");
                var data = {
                    'units':units,
                    'mid':mid,
                    'typeid':typeid
                };

                $.ajax({
                    url:"/index.php?m=home&c=index&a=sjNextPrivsArticle",
                    type:"get",
                    data:data,
                    success:function(res){
                        if(res.code == 200){
                            //上一篇
                            var privs = '';
                            if(res.data.privs != null) {
                                privs += "<a href='javascript:nextprivs(" + res.data.privs.mid + ")'>上一篇：" + res.data.privs.title + "</a>";
                            }else{
                                privs += "<a href='javascript:viod(0)'>上一篇：没有数据</a>";
                            }
                            //下一篇
                            var next = '';
                            if(res.data.next != null){
                                next += "<a href='javascript:nextprivs(" + res.data.next.mid + ")'>下一篇：" + res.data.next.title + "</a>";
                            }else{
                                next += "<a href='javascript:viod(0)'>下一篇：没有数据</a>";
                            }
                            //内容
                            if(res.data.content != null){
                                var item = "<div class='content-top'>";
                                    item += "<h2 class='content-title'>" + res.data.content.title + "</h2>";
                                    item += "<p class='contenr-date'>发布时间："+res.data.content.inputtime+"</p>";
                                    item += "</div>";
                                    item += "<div class='goverment-content'>" + res.data.content.content;
                                    //转载自
                                    if(res.data.content.copyfrom.length >0){
                                        copyfrom = res.data.content.copyfrom;
                                        item +="<br/><p style='text-indent: 2em;font-size: 14px;'>(转载自："+res.data.content.copyfrom+")</p>";
                                    }
                                    item += "</div>";
                                    item += "<p class='content-tiaozhuan'>";
                                    item += privs + next;
                                    item += "</p>";
                                $(".govermentdetail-content").html(item);
                            }
                        }else{
                            alert("获取数据失败");
                        }
                    },
                    error:function(){
                        alert("请求错误");
                    }
                });
            }
        }

        /**
         * 获取机构下单位
         **/
        function ajaxAgency(data){
            $.ajax({
                url:"/index.php?m=home&c=index&a=ajaxAgency",
                type:'get',
                data:data,
                async:false,
                success:function(res){
                    if(res.code ==200 && res.data.length>0){
                        $(".department>div").find("p").remove();
                        var html='';

                        for(var i=0; i<res.data.length; i++){
                            if(i==0){
                                html += "<p><a class='red' data-unitid='"+res.data[i].unitid+"' onclick='clickBmen(this,"+res.data[i].unitid+")' >"+res.data[i].name+"</a></p>";
                            }else{
                                html += "<p><a data-unitid='"+res.data[i].unitid+"' onclick='clickBmen(this,"+res.data[i].unitid+")'>"+res.data[i].name+"</a></p>";
                            }
                        }
                        $(".department>div").append(html);
                        reserCat();
                    }
                }
            });
        }

        /**
         * 点击部门
         */
        function clickBmen(event,unitid){
            $("div.department>div.main>p").find("a").removeClass("red");
            $(event).addClass("red");
            reserCat();
            var data = getParams();
            getData(data);
        }

        /**
         * 点击分类
         */
        function clickCat(event,typeid){
            $("div.goverment-nav>ul").find("li").removeClass("red");
            $(event).addClass("red");
            if(typeid == 4030){
                var data = getParams();
                getProjectDetail(data);
                // return false;
            }else{
                var data = getParams();
                getData(data);
            }

        }

        /**
         * 获取参数
         * @returns {{typeid: jQuery, units: string}}
         */
        function getParams(){
            var linkageid = $("div.wrapper>ul").find("li.t-active").data("unitid");
            var unitid = $("div.department>div.main>p").find("a.red").data("unitid")
            var units = linkageid + "," + unitid;
            var typeid = $("div.goverment-nav>ul").find("li.red").data("linkid");
            var data = {
                'typeid':typeid,
                'units':units
            };
            return data;
        }
        /**
         *  获取项目详细
         **/
        function getProjectDetail(data){
            $.ajax({
                url:"/index.php?m=home&c=index&a=shengjiShowProject",
                type:"get",
                data:data,
                success:function(res){
                    $("div.govermentdetail-content").html('');

                    // 上一篇
                    var privs = '';
                    if(res.privs != null){
                        privs += "<div class='floatleft' onclick='proNextPrivs(this,"+res.privs.mid+")'>上一篇："+res.privs.title+"</div>";
                    }else{
                        privs += "<div class='floatleft'>上一篇：无数据</div>";
                    }

                    //下一篇
                    var next = "";
                    if(res.next != null){
                        next += "<div class='floatright' onclick='proNextPrivs(this,"+res.next.mid+")'>下一篇："+res.next.title+"</div>";
                    }else{
                        next += "<div class='floatright'>下一篇：无数据</div>";
                    }

                    if(res.code == 200){
                        var item = '';
                        item += "<li style='border: none;'>";
                        item += "<div class='activityDetails-list detail-list-leftb' style='margin-bottom:0px;'>";
                        item += "<div class='list-top list-topb'>";
                        item += "<h3>" + res.data.title + "</h3>";
                        item += "<p class='floatleft'>" + res.data.inputtime + "<span>浏览数：" + res.data.views + "人</span> </p>";
                        item += "<div class='clear'></div>";
                        item += "</div>";
                        item += "<div class='list-middle'>";
                        item += "<div class='list-middle-left'>";
                        item += "<p>项目名称：" + res.data.title + "</p>";
                        item += "<p>所在市（州）：" + res.data.cityid + "</p>";
                        item += "<p>具体地点：" + res.data.address + "</p>";
                        item += "<p>产业类别：&nbsp;&nbsp;" + res.data.industryid + "</p>";
                        item += "<p>引资额：&nbsp;&nbsp;" + res.data.quasi_amount + "亿元</p>";
                        item += "<p>合作方式：" + res.data.cooperation + "</p>";
                        item += "<p>业主单位：&nbsp;&nbsp;" + res.data.company + "</p>";
                        item += "<p>联系单位（人）及联系电话：&nbsp;&nbsp;" + res.data.contact_name + "&nbsp;&nbsp;" + res.data.contact_tel + "</p>";
                        item += "</div></div></div>";
                        item += "<div class='conditions-left conditions-leftb'>";
                        item += "<div class='conditions-left-top conditions-left-topb'>";
                        item += "<p class='activityDetails-conditions-title'><span style='width: 250px;'>项目建设条件及主要内容</span></p>";
                        item += "<p class='activityDetails-conditions-text'>" + res.data.content + "</p>";
                        item += "</div></div>";
                        item += "<div  style='width: 840px; border-top: 1px solid #ddd; padding-top: 20px; margin: 0 auto; margin-bottom: 40px;'>";
                        item += privs;
                        item += next;
                        item += "</div></li>";

                        $("div.govermentdetail-content").html(item);
                    }else{
                        var item = '';
                        item += "<div class='content-top'>";
                        item += "<h2 class='content-title'>无项目数据</h2>";
                        item += "</div>";

                        $("div.govermentdetail-content").html(item);
                    }
                }
            });
        }

        /**
         * 获取文章内容
         */
        function getData(data){
            $.ajax({
                url:"/index.php?m=home&c=index&a=sjGetNews",
                type:"get",
                data:data,
                success:function(res){
                    if(res.code == 200){
                        //上一篇
                        var privs = '';
                        if(res.data.privs != null) {
                            privs += "<a href='javascript:nextprivs(" + res.data.privs.mid + ")'>上一篇：" + res.data.privs.title + "</a>";
                        }else{
                            privs += "<a href='javascript:viod(0)'>上一篇：没有数据</a>";
                        }
                        //下一篇
                        var next = '';
                        if(res.data.next != null){
                            next += "<a href='javascript:nextprivs(" + res.data.next.mid + ")'>下一篇：" + res.data.next.title + "</a>";
                        }else{
                            next += "<a href='javascript:viod(0)'>下一篇：没有数据</a>";
                        }
                        //内容
                        if(res.data.content != null){
                            var item = "<div class='content-top'>";
                            item += "<h2 class='content-title'>" + res.data.content.title + "</h2>";
                            item += "<p class='contenr-date'>发布时间："+res.data.content.inputtime+"</p>";
                            item += "</div>";
                            item += "<div class='goverment-content'>" + res.data.content.content;
                            //转载自
                            if(res.data.content.copyfrom.length >0){
                                copyfrom = res.data.content.copyfrom;
                                item +="<br/><p style='text-indent: 2em;font-size: 14px;'>(转载自："+res.data.content.copyfrom+")</p>";
                            }
                            item += "</div>";
                            item += "<p class='content-tiaozhuan'>";
                            item += privs + next;
                            item += "</p>";
                            $(".govermentdetail-content").html(item);
                        }else{
                            var item = "<div class='content-top'>";
                            item += "<h2 class='content-title'>无数据</h2>";
                            item += "</div>";
                            $(".govermentdetail-content").html(item);
                        }
                    }else{
                        alert("获取数据失败");
                    }
                }
            });
        }

        /**
         * 重置分类
         */
        function reserCat(){
            $("div.goverment-nav>ul").find("li").removeClass("red");
            $("div.goverment-nav>ul").find("li").first().addClass('red');
        }

        /**
         * 项目上下篇
         */
        function proNextPrivs(event,mid){
            var data = getParams();
            data['mid'] = mid;
            getProjectDetail(data);
        }
    </script>
</body>

</html>