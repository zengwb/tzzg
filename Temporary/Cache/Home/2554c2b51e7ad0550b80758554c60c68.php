<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onClick="forbidden(this,'<?php echo ($row["linkurl"]); ?>')" class="wordcolor first" ><?php echo ($row['name']); ?></a>
                            <!--style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"-->
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  ><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                    function forbidden(event,url){
                        var l = $(event).next("ul").find("li").length;
                        if(url == ""){
                            return false;
                        }else{
                            window.open(url);
                        }
                    }

                    $(function() {
                        var navi = $(".nav-list>ul>li");
                        navi.each(function() {
                            if ($(this).find("ul>li").length) {
                            } else {
                                $(this).find("i, ul").hide();
                            }
                        });
                        navi.find("a").each(function(){
                            if($(this).attr('naviid') == 1034){
                                $(this).removeClass("wordcolor");
                            }
                        });

                        /*禁止点击未开启功能  start*/
                        $(".nav-list>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            if(id == 1060 || id == 1060){
                                alert("正在测试中");
                                return false;
                            }
                        });

                        $(".nav-list>ul>li>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            console.log(id);
                            if(id == 1072 || id == 1080 || id == 1081 || id == 1073  || id == 1074 || id == 1075 || id == 1076 || id == 1077){
                                alert("正在测试中");
                                return false;
                            }
                        });
                        /*禁止点击未开启功能  end*/
                    });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat;background-position:1px;border:1px solid rgb(230,166,166);border-left: none;"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />

    <div class="majordetails mar16" style="">
		<p class="details-color"></p>
		<div class="detail-top W1224">
			<p><i class="detail-top-search icon iconfont icon-sousuo1"></i> <?php echo cms_location($catid);?> &gt; 项目信息 &gt; <?php echo ($content["title"]); ?></p>
			<div class="detail-list">
				<div class="detail-list-left lf">

					<div class="list-top">
						<h3><?php echo ($content["title"]); ?></h3>
						<p><?php echo (date("Y-m-d",$content['inputtime'])); ?><span>浏览数：<?php echo ($content["views"]); ?>人</span> </p>
						<p class="list-top-last">
							<!--<i class="icon iconfont icon-shouji"></i>免费发送至手机
							<span id="collection"><i class="icon iconfont icon-aixinhaopingtaoxinshoucangxihuanmianxing"></i>收藏</span>
							<span><i class="icon iconfont icon-share-fill"></i>分享</span>-->
						</p>
						<div class="clear"></div>
					</div>
					<div class="list-middle">
						<div class="list-middle-left">
							<p>项目名称：<?php echo ($content["title"]); ?> </p>
							<p>产业类别：<?php if($content["industryid"] != 0): echo (cms_get_linkage_value($content["industryid"],false)); ?> <?php else: ?> 不详<?php endif; ?></p>
                            <p>所在地区：<?php echo (cms_get_linkage_value($content["cityid"])); ?></p>
							<p>具体地点：<?php echo ($content["address"]); ?></p>
                            <p>引资金额：<?php echo (unit_conversion($content["investment"])); ?>亿元</p>
                            <p>融资资额：<?php echo (unit_conversion($content["quasi_amount"])); ?>亿元</p>

							<p>融资方式：
                                <?php if($content["finacing_style"] != 0): echo (cms_get_linkage_value($content["finacing_style"],false)); else: ?> 不详<?php endif; ?>
                            </p>
                            <p>合作方式：<?php echo (cms_get_linkage_value($content["cooperation"],false)); ?></p>
                            <p>业主单位：<?php echo ($content["company"]); ?></p>
							<p>是否扶贫项目：<?php if($content["is_helpproject"] != 0): ?>是<?php else: ?> 否<?php endif; ?></p>
							<p>是否产业园区：<?php if($content["is_industrialpark"] != 0): ?>是<?php else: ?> 否<?php endif; ?></p>
                            <p>联系单位（人）电话：<?php echo ($content["company"]); ?> &nbsp;&nbsp;<?php echo ($content["contact_name"]); ?>&nbsp;&nbsp;<?php echo ($content["contact_tel"]); ?></p>
						</div>
						<div id="middleBtn" class="list-middle-right">
							<p class="class1"><i class="icon iconfont zhixiangliebiao"></i>立即参与</p>
							<p class="class1 "><i class="icon iconfont zhixiangliebiao"></i>中期关注</p>
							<p class="class1"><i class="icon iconfont zhixiangliebiao"></i>长期观望</p>
							<div id="finishBtn" class="middle-btn">完成</div>
						</div>
					</div>
				</div>
			</div>
            <div class="detail-conditions ">
                <div class="conditions-left">
                    <div class="conditions-left-top">
                        <p class="conditions-title"><span>项目介绍</span></p>
                        <div class="conditions-text">
                            <?php if($content["content"] != ''): echo ($content["content"]); ?>
                            <?php else: ?>
                                不详<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

<script>
		let middleBtn = document.getElementById('middleBtn')
		let class1 = document.getElementsByClassName('class1')
		middleBtn.onclick = function (e) {
			if ((e.target.classList)[1] != 'active' && (e.target.classList)[0] == 'class1') {
				for (let i = 0; i < class1.length; i++) {
					class1[i].classList.remove('active')
				}
				e.target.classList.add('active')
			}
		}
		let collection = document.getElementById('collection')
		collection.onclick = function () {
			if ((middleBtn.style.display) == 'none' || (middleBtn.style.display) == '') {
				middleBtn.style.display = 'block'
				collection.classList.add('collection')

			} else {
				middleBtn.style.display = 'none'
				collection.classList.remove('collection')
			}
		}
		let finishBtn = document.getElementById('finishBtn')
		finishBtn.onclick = function () {
			for (let i = 0; i < class1.length; i++) {
				if (class1[i].classList[1]) {
					middleBtn.style.display = 'none'

				}
			}
		}
	</script>
    <script language="javascript" src="<?php echo ($site["domain"]); ?>index.php?m=plugin&c=count&a=hits&module=article&catid=<?php echo ($_GET['catid']); ?>&id=<?php echo ($_GET['id']); ?>"></script>
</body>
</html>