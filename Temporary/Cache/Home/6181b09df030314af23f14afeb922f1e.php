<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onclick="forbidden(this,'<?php echo ($row['linkurl']); ?>')"   class="wordcolor first" style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"><?php echo ($row['name']); ?></a>
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  style="font-weight:<?php echo ($val['style']['bold']); ?>; color:<?php echo ($val['style']['color']); ?>;"><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                function forbidden(event,url){
                    var l = $(event).next("ul").find("li").length;
                    if(url == ""){
                        return false;
                    }else{
                        window.open(url);
                    }
                }
                $(function() {
                    var navi = $(".nav-list>ul>li");
                    navi.each(function() {
                        if ($(this).find("ul>li").length) {
                        } else {
                            $(this).find("i, ul").hide();
                        }
                    });
                    navi.find("a").each(function(){
                        if($(this).attr('naviid') == 1034){
                            $(this).removeClass("wordcolor");
                        }
                    });

                /*禁止点击未开启功能  start*/
                    // $(".nav-list>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     //if(id != 1039 && id != 1034 && id != 1038 && id != 1035){

                    //     if(id != 1034 && id != 1038 && id != 1035 && id != 1059){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });

                    // $(".nav-list>ul>li>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     if(id != 1037 && id != 1039 && id != 1040 && id != 1041 && id != 1044 && id != 1036 && id != 1045 && id != 1065){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    /*禁止点击未开启功能  end*/
                });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />
<style type="text/css">
	.selectDom{
		color:#b80000;
	}
	#pageBar{
		float:right;
		margin-right:20px;
		margin-top:27px;
	}
	.pageBtn {
		display: inline-block;
		float: left;
		margin-left:15px;
	}

	.pageBtn span{
		margin-right: 3px;
		display: inline-block;
	}

	.pageBtn-selected>a{
		color:red;
	}
</style>

<div class="acitivity-list mar16" >
	<div class="acitivity-list-top">
		<!-- <ul class="W1224">

            <li class="on">
                <div class="triangle_border_down">
                    <span></span>
                </div>
                <a href="">政府招商</a>

            </li>
            <li><a href="">项目融资</a></li>
            <li><a href="">资产交易</a></li>

        </ul> -->
	</div>
	<div class="activity-list-choice W1224">
		<div class="choice-title"><span></span>
			<h3>资产交易
			</h3>
			<!-- <p></p> -->
		</div>
		<div class="choice-region">
			<div class="asseet">交易类别：
				<?php $_parentid = 3338;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span trans_type="<?php echo ($row["linkageid"]); ?>"><?php echo ($row["name"]); ?></span><?php endforeach; ?>
			</div>
            <div class="enterprise">企业类型：
                <?php $_parentid = 3994;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span enter_type="<?php echo ($row["linkageid"]); ?>"><?php echo ($row["name"]); ?></span><?php endforeach; ?>
            </div>

            <div class="cooperation">交易方式：
                <?php $_parentid = 3333;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span trans_mode="<?php echo ($row["linkageid"]); ?>"><?php echo ($row["name"]); ?></span><?php endforeach; ?>
            </div>
            <div class="finance means">资产估值：
                <span asset_val="0">不限</span>
                <?php $_parentid = 3324;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span asset_val="<?php echo ($row["linkageid"]); ?>"><?php echo ($row["name"]); ?></span><?php endforeach; ?>
            </div>

            <div class="finance attorn">转让价值：
                <span transfer_val="0">不限</span>
                <?php $_parentid = 3324;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span transfer_val="<?php echo ($row["linkageid"]); ?>"><?php echo ($row["name"]); ?></span><?php endforeach; ?>
            </div>

			<div class="region" id="region">
				所在地区：
				<div class="region-select">

					<select name="shengid" style="width: 115px; height: 30px;" id="regionProvice">
						<option value="0" class="bt-input" selected="selected">请选择省份</option>
						<?php $_parentid = 1;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><option value="<?php echo ($row['linkageid']); ?>" class="bt-input"><?php echo ($row['name']); ?></option><?php endforeach; ?>
					</select>

					<select name="shiid" style="width: 115px; height: 30px;" id="regionCity">
						<option value="" class="bt-input" >请选择市</option>
					</select>

					<select name="quid" style="width: 115px; height: 30px;" id="regionQu">
						<option value="" class="bt-input" >请选择区</option>
					</select>
				</div>
			</div>
			<div>
				<p class="acitivity-search floatright"><input type="search" placeholder="在当前条件下搜索">
					<span class="acitivity-search-font" id="search">
							<i class="icon iconfont icon-sousuo1"></i>
						</span>
				</p>
				<div class="clear"></div>
			</div>
		</div>

	</div>
</div>
<div class="acitivity-sort W1224">
	<div class="acitivity-sort-left lf">
		<div class="sort-top " id="oderstyle">
			<span class="sort-on" order="`inputtime` DESC">综合排序<i class="icon iconfont jiantou"></i></span>
			<span order="`updatetime` DESC">更改时间<i class="icon iconfont jiantou"></i></span>
			<span order="`transfer_val` DESC">金额排序<i class="icon iconfont jiantou"></i></span>
			<p class="sort-total">共有<span class="projectCount">300+</span>个符合要求的项目信息</p>
		</div>
		<div class="sort-list">
			<ul id="projectList">


			</ul>
			<div class="sort-footer" id="page">
				<div id="pageBar"><!--这里添加分页按钮栏--></div>
				<!--<img src="/Resources/Skin/2019/image/sort-001.png" alt="">-->
				<p>共有<span class="projectCount">300+</span>个符合要求的项目信息</p>
			</div>
		</div>
	</div>
	<div class="acitivity-sort-right lf">
		<div class="acitivity-sort-right-top">
			<p>案列展示</p>
		</div>
		<div class="acitivity-sort-right-img">
			<p>
				<img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""><br />
				<span>融资金额：200万元<br /><b>所属行业：IT互联网</b> <br />
						河南化工医药网信息服务平台项目</span>
			</p>
			<p>
				<img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""><br />
				<span>融资金额：200万元<br /><b>所属行业：IT互联网</b> <br />
						河南化工医药网信息服务平台项目</span>
			</p>
			<p>
				<img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""><br />
				<span>融资金额：200万元<br /><b>所属行业：IT互联网</b> <br />
						河南化工医药网信息服务平台项目</span>
			</p>


		</div>
	</div>
</div>
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

<script>
    let more = function () {
        //   alert(1111)
        let region = document.getElementById('region')
        let jiantou = document.getElementById('jiantou')
        // console.log((jiantou.classList)[2])
        if ((jiantou.classList)[2] == 'jiantou') {
            region.style.height = '168px';

            jiantou.classList.remove('jiantou')
            jiantou.classList.add('jiantoutop')
        } else {
            jiantou.classList.add('jiantou')
            jiantou.classList.remove('jiantoutop')
            region.style.height = '58px';
        }
    }
    let more1 = function () {
        let category = document.getElementById('category')
        let jiantou1 = document.getElementById('jiantou1')

        if (!(category.classList)[1]) {
            category.classList.add('category-height')
            jiantou1.classList.remove('jiantou')
            jiantou1.classList.add('jiantoutop')

        } else {
            category.classList.remove('category-height')
            jiantou1.classList.remove('jiantoutop')
            jiantou1.classList.add('jiantou')

        }

    }
    let manufacture = function (m, n) {
        let m1 = document.getElementById(m)
        let n1 = document.getElementById(n)
        // let t1 = document.getElementById(t)
        let category1 = document.getElementsByClassName('category1')
        let manufacture = document.getElementsByClassName('manufacture')
        // if()
        // console.log(1,m1,2,n1,3,t1)
        if ((n1.classList)[2]) {
            m1.style.display = "none"
            n1.classList.remove('category-on')

        } else {
            for (let i = 0; i < category1.length; i++) {
                if ((category1[i].classList)[2])
                    category1[i].classList.remove('category-on')
            }
            for (let i = 0; i < manufacture.length; i++) {

                manufacture[i].style.display = "none"
                // console.log(manufacture[i])
            }
            // debugger;

            m1.style.display = "block"
            n1.classList.add('category-on')
        }
    }
/***************************自定义js脚本*********************************/
//定义地区变量
    var proId=0,cityId=0,quId=0;
    $(function() {
        /**
         *  禁用市区选择
         */

        $("#regionCity").hide();
        $("#regionQu").hide();

        /**
         *  选择省，加载市
         */
        $("#regionProvice").change(function () {
            proId = $(this).val();
            cityId = 0, quId = 0; //重置市级、区级id

            var pid = proId;
            if (pid != 0) {
                $(".citys").remove();
                $(".qus").remove();

                var data = getParams();
                data.cityid = proId + ",%";
                ajaxGetData(data);

                $.ajax({
                    url: '/index.php?m=home&c=index&a=ajaxLinkage',
                    method: 'get',
                    data: {'pid': pid},
                    success: function (res) {
                        var citys;
                        for (var i = 0; i < res.length; i++) {
                            citys += "<option value='" + res[i].linkageid + "' class='bt-input citys' >" + res[i].name + "</option>";
                        }
                        $("#regionCity").append(citys);
                        $("#regionCity").show();
                    }
                });
            } else {
                $("#regionCity").hide();
                $("#regionQu").hide();

                $(".citys").remove();
                $(".qus").remove();
                var data = getParams();
                ajaxGetData(data);
            }
        });

        /**
         *  选择市，加载区
         */
        $("#regionCity").change(function () {
            cityId = $(this).val();
            quId = 0; //重置区级id
            var pid = cityId;

            if (cityId != 0) {
                $(".qus").remove();
                var data = getParams();
                data.cityid = proId + "," + cityId + "%";
                ajaxGetData(data);
                $.ajax({
                    url: '/index.php?m=home&c=index&a=ajaxLinkage',
                    method: 'get',
                    data: {'pid': pid},
                    success: function (res) {
                        var qus;
                        for (var i = 0; i < res.length; i++) {
                            qus += "<option value='" + res[i].linkageid + "' class='bt-input qus' >" + res[i].name + "</option>";
                        }
                        $("#regionQu").append(qus);

                        $("#regionQu").show();
                    }
                });
            } else {
                $("#regionQu").hide();
                $(".qus").remove();
                var data = getParams();
                data.cityid = proId + ",%";
                ajaxGetData(data);
            }
        });

        /**
         * 选择区，加载数据
         */
        $("#regionQu").change(function () {
            quId = $(this).val();
            var data = getParams();
            if (quId != 0) {
                data.cityid = proId + "," + cityId + "," + quId;
            }
            ajaxGetData(data);
        });


        /************事件绑定************/
        $(".asseet").on("click", 'span', function(){
            //取消当前选择
            if($(this).hasClass("selectDom")){
                $(this).removeClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }else{
                //移除之前选择的元素添加的class
                $(".asseet>span").each(function(){
                    if($(this).hasClass("selectDom")){
                        $(this).removeClass("selectDom")
                    }
                });
                //添加选择的class
                $(this).addClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }
        });

        $(".enterprise").on("click", 'span', function(){
            //取消当前选择
            if($(this).hasClass("selectDom")){
                $(this).removeClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }else{
                //移除之前选择的元素添加的class
                $(".enterprise>span").each(function(){
                    if($(this).hasClass("selectDom")){
                        $(this).removeClass("selectDom")
                    }
                });
                //添加选择的class
                $(this).addClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }
        });

        $(".cooperation").on("click", 'span', function(){
            //取消当前选择
            if($(this).hasClass("selectDom")){
                $(this).removeClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }else{
                //移除之前选择的元素添加的class
                $(".cooperation>span").each(function(){
                    if($(this).hasClass("selectDom")){
                        $(this).removeClass("selectDom")
                    }
                });
                //添加选择的class
                $(this).addClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }
        });

        $(".means").on("click", 'span', function(){
            //取消当前选择
            if($(this).hasClass("selectDom")){
                $(this).removeClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }else{
                //移除之前选择的元素添加的class
                $(".means>span").each(function(){
                    if($(this).hasClass("selectDom")){
                        $(this).removeClass("selectDom")
                    }
                });
                //添加选择的class
                $(this).addClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }
        });

        $(".attorn").on("click", 'span', function(){
            //取消当前选择
            if($(this).hasClass("selectDom")){
                $(this).removeClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }else{
                //移除之前选择的元素添加的class
                $(".attorn>span").each(function(){
                    if($(this).hasClass("selectDom")){
                        $(this).removeClass("selectDom")
                    }
                });
                //添加选择的class
                $(this).addClass("selectDom");
                var data = getParams();
                ajaxGetData(data);
            }
        });

        $("#search").click(function(){
            var keywords = $(this).prev("input").val();
            var data = getParams();
            data.keyWords = keywords;
            ajaxGetData(data);
        });

        /**
         *  选择排序(改变样式)
         */
        $("#oderstyle>span").click(function(){
            $("#oderstyle>span").each(function(){
                $(this).removeClass("sort-on");
            });

            if(!$(this).hasClass('sort-on')){
                $(this).addClass("sort-on");
                var data = getParams();
                ajaxGetData(data);
            }
        });

        //页面加载获取数据
        var data = getParams();
        ajaxGetData(data);
});

/*
 * 获取筛选参数
 */
function getParams(){
    var region, trans_type, enter_type, trans_mode, asset_val, transfer_val;
    //获取所在地区id
    if(quId != 0){
        region = proId + "," + cityId + "," + quId;
    }else if(proId !=0 && cityId != 0 && quId == 0){
        region = proId + "," + cityId + "%";
    }else if(proId !=0 && cityId ==0 && quId == 0){
        region = proId + ",%";
    }else{
        region = 0;
    }
    //获取交易类别
    $(".asseet>span").each(function(){
        if($(this).hasClass('selectDom')){
            trans_type = $(this).attr('trans_type');
        }
    });
    if(trans_type == undefined){
        trans_type = 0;
    }

    //获取企业类型
    $(".enterprise>span").each(function(){
        if($(this).hasClass('selectDom')){
            enter_type = $(this).attr('enter_type');
        }
    });
    if(enter_type == undefined){
        enter_type = 0;
    }

    //获取交易方式
    $(".cooperation>span").each(function(){
        if($(this).hasClass('selectDom')){
            trans_mode = $(this).attr('trans_mode');
        }
    });
    if(trans_mode == undefined){
        trans_mode = 0;
    }

    //获取资产估值
    $(".means>span").each(function(){
        if($(this).hasClass('selectDom')){
            asset_val = $(this).attr('asset_val');
        }
    });
    if(asset_val == undefined){
        asset_val = 0;
    }

    //获取转让价值
    $(".attorn>span").each(function(){
        if($(this).hasClass('selectDom')){
            transfer_val = $(this).attr('transfer_val');
        }
    });
    if(transfer_val == undefined){
        transfer_val = 0;
    }


    //获取排序方式
    $("#oderstyle>span").each(function () {
        if($(this).hasClass("sort-on")){
            order = $(this).attr("order");
        }else{
            order = "`inputtime` DESC";
        }
    });
    //获取输入的搜索关键词
    var searchKey = $("input[type='search']").val();
    //获取分页页数
    var currentPage = 1;
    //获取以上所有数据（暂缺 适用单位），用aja请求，局部刷新页面
    //ajax请求数据
    var data = {
            'cityid': region,
            'transaction_type':trans_type,
            'enter_type':enter_type,
            'transaction_mode':trans_mode,
            'asset_val':asset_val,
            'transfer_val':transfer_val,
            'keyWords':searchKey,
            'page':currentPage,
            'order':order
        };
    return data;
}
/*
 * ajax请求数据
 */
var total,pageSize,curPage,totalPage;
function ajaxGetData(data){
    $.ajax({
        'url':'/index.php?m=home&c=index&a=get_asset_project',
        'type':'get',
        'data':data,
        success:function(res){
            var item='';
            $(".projectCount").html(res.count);
            $("#projectList").html('');

            if(res.count == "0"){
                item = "<li><h3 class='list-title'><a>当前搜索无结果！</a></h3></li>";
                $("#projectList").html(item);
            }else{
                for(var i=0 ;i < res.pro.length; i++){
                    item += "<li><h3 class='list-title'><span></span>";
                    item += "<a href='/index.php?m=home&c=index&a=shows&catid=187&id="+res.pro[i].mid+"' target='_blank' title='"+res.pro[i].title+"' style='font-weight:"+res.pro[i].title_style.bold+"; color:"+res.pro[i].title_style.color+";'>"+res.pro[i].title+"</a></h3>";
                    item += "<div class='list-left lf'><p>资产估值：<span class='money'>"+res.pro[i].asset_val+"亿元</span></p><p>所在地区："+res.pro[i].regionName+" <span class='industry'>所属行业："+res.pro[i].industryid+"</span></p><p>交易方式："+res.pro[i].transaction_mode+"</p></div>";
                    item += "<div class='list-center lf'><i class='icon iconfont icon-icondingdanxiangqingxiansheng'></i> "+res.pro[i].contact_name+"<br />"+res.pro[i].company+"</div><div class='list-right' style='float:right;margin-right:70px'><button>约谈项目方</button></div>";
                    item += "</li>";
                };

                $("#projectList").html(item);
            }
            //需补充分页信息
            total = res.pageinfo.total; //总记录数
            pageSize = res.pageinfo.pageSize; //每页显示条数
            curPage = res.pageinfo.curPage; //当前页
            totalPage = res.pageinfo.totalPage; //总页数
        },
        complete:function(res){ //生成分页条
            getPageBar();
        },
        error:function(){
            alert("数据加载失败");
        }
    });
}
//获取分页条（分页按钮栏的规则和样式根据自己的需要来设置）
function getPageBar(){
    if(curPage > totalPage) {
        curPage = totalPage;
    }
    if(curPage < 1) {
        curPage = 1;
    }
    pageBar = "";
    pageBar +="<span class='pageBtn' style='margin-right:25px;'>当前共<b style='color:red'> "+totalPage+"</b> 页</span>";
    //如果不是第一页
    if(curPage != 1){
        pageBar += "<span class='pageBtn'><a href='javascript:turnPage(1)'>首页</a></span>";
        pageBar += "<span class='pageBtn'><a href='javascript:turnPage("+(curPage-1)+")'><<</a></span>";
    }
    //显示的页码按钮(5个)
    var start,end;
    if(totalPage <= 5) {
        start = 1;
        end = totalPage;
    } else {
        if(curPage-2 <= 0) {
            start = 1;
            end = 5;
        } else {
            if(totalPage-curPage < 2) {
                start = totalPage - 4;
                end = totalPage;

            } else {
                start = curPage - 2;
                end = parseInt(curPage) + 2;
            }
        }
    }
    for(var i=start;i<=end;i++) {
        if(i == curPage) {
            pageBar += "<span class='pageBtn pageBtn-selected'><a href='javascript:turnPage("+i+")'>"+i+"</a></span>";
        } else {
            pageBar += "<span class='pageBtn'><a href='javascript:turnPage("+i+")'>"+i+"</a></span>";
        }
    }
    //如果不是最后页
    if(curPage != totalPage){
        pageBar += "<span class='pageBtn'><a href='javascript:turnPage("+(parseInt(curPage)+1)+")'>>></a></span>";
        pageBar += "<span class='pageBtn'><a href='javascript:turnPage("+totalPage+")'>尾页</a></span>";
    }

    $("#pageBar").html(pageBar);
}

//点击页码获取数据
function turnPage(curPage){
    var data = getParams();
    data.page = curPage;
    ajaxGetData(data);
}
</script>
</body>
</html>