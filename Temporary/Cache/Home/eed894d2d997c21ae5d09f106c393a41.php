<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onclick="forbidden(this,'<?php echo ($row['linkurl']); ?>')"   class="wordcolor first" style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"><?php echo ($row['name']); ?></a>
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  style="font-weight:<?php echo ($val['style']['bold']); ?>; color:<?php echo ($val['style']['color']); ?>;"><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                function forbidden(event,url){
                    var l = $(event).next("ul").find("li").length;
                    if(url == ""){
                        return false;
                    }else{
                        window.open(url);
                    }
                }
                $(function() {
                    var navi = $(".nav-list>ul>li");
                    navi.each(function() {
                        if ($(this).find("ul>li").length) {
                        } else {
                            $(this).find("i, ul").hide();
                        }
                    });
                    navi.find("a").each(function(){
                        if($(this).attr('naviid') == 1034){
                            $(this).removeClass("wordcolor");
                        }
                    });

                /*禁止点击未开启功能  start*/
                    // $(".nav-list>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     //if(id != 1039 && id != 1034 && id != 1038 && id != 1035){

                    //     if(id != 1034 && id != 1038 && id != 1035 && id != 1059){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });

                    // $(".nav-list>ul>li>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");


                    //     if(id != 1037 && id != 1039 && id != 1040 && id != 1041 && id != 1044 && id != 1036 && id != 1045 && id != 1065){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    /*禁止点击未开启功能  end*/
                });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/provincial.css" />
	<!--中间内容-->
	<div class="goverment-top mar16"></div>
	<div class="sj-content">
		<div class="W1224 goverment-commissions">
			<!--默认四川 id 24-->
			<div class="W1224 shen-top" data-linkageid="24"><div class="pro-content"><h3>四川省</h3></div></div>
			<div class="province" style="background-color:#fff;">
				<div class="pro-content">
					<?php $_parentid = 24;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span ><a onclick="clickCity(this,'<?php echo ($row["linkageid"]); ?>')" data-linkageid="<?php echo ($row["linkageid"]); ?>" ><?php echo ($row["name"]); ?></a></span><span>|</span><?php endforeach; ?>
				</div>
			</div>
        </div>
		<!--tab切换-->
		<div class="tab-content mar4">
			<div class="wrapper">
				<ul class="tab jgdt">
					<li class="tab-item t-active ">机构介绍</li>
					<li class="tab-item">最新动态</li>
				</ul>
			</div>
			<div class="departments">
				<div class="jg-content selected" id="jg-introduce" >
					<img src="/Resources/Skin/2019/image/sj-jgjs.gif" alt="机构介绍" />
					<div class="jgwz">都是对的</div>
				</div>
				<div class="jg-content" id="jg-data">
					<div class="part">
						<div class="goverment-body">
							<div class="goverment-nav floatleft">
								<ul>
									<?php $_parentid = 4024;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) : if($row["linkageid"] == 4025): ?><li class="tab-item red" data-linkid="<?php echo ($row['linkageid']); ?>" onclick="clickType(this,'<?php echo ($row["linkageid"]); ?>')"><span class="red-fk"></span><?php echo ($row['name']); ?></li>
											<?php else: ?>
											<li class="tab-item" data-linkid="<?php echo ($row['linkageid']); ?>" onclick="clickType(this,<?php echo ($row["linkageid"]); ?>)"><span class="red-fk"></span><?php echo ($row['name']); ?></li><?php endif; endforeach; ?>
								</ul>
								<div>
								</div>
							</div>
							<div class="commissions-list no-mar wid9 floatright">
								<ul class="fb-tab selected" id="content"></ul>
								<div class="commissions-page"></div>
							</div>
							<div class="clear"></div>
						</div>
				
					</div>
				</div>
			</div>
		</div>

	</div>
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

	<script type="text/javascript">
		$(function () {
			$(".goverment-nav .tab-item").click(function () {
				$(this).addClass("red").siblings().removeClass("red");
				// $(".commissions-list .fb-tab").eq($(this).index()).show().siblings().hide();
			});

            $(".tabjg .tab-item").click(function () {
                $(this).addClass("t-active").siblings().removeClass("t-active");
                $(".main").eq($(this).index()).show().siblings().hide();
            });

            $(".jgdt .tab-item").click(function () {
                $(this).addClass("t-active").siblings().removeClass("t-active");
                $(".jg-content").eq($(this).index()).show().siblings().hide();
            });

            //默认选中成都市
            $("div.province>div.pro-content>span>a:first").addClass("red");
            //默认获取成都市 id 382
            getCityInfo(382);
            //加载默认数据
            var data = getParams();
            getData(data);

		});

        /**
         * 点击市
         */
        function clickCity(event,linkageid){
            $("div.province>div.pro-content>span>a").removeClass("red");
            $(event).addClass("red");
            getCityInfo(linkageid);
            resetType();
            resetTab();
            var data = getParams();
            getData(data);
        }

        /**
         * 点击分类
         */
        function clickType(event,linkageid){
            var data = getParams();
            data.typeid = linkageid;
            if(linkageid == 4030){
                data['type'] = 'project';
            }
            getData(data);
        }

        /**
         *  重置分类
         */
        function resetType(){
            $("div.goverment-nav>ul").find("li.red").removeClass("red");
            $("div.goverment-nav>ul>li:first").addClass("red");
        }

        //机构介绍和最新动态重置
        function resetTab(){
            $("#jg-data").hide();
            $("#jg-introduce").show();
            $("ul.jgdt>li").removeClass("t-active");

            $("ul.jgdt>li:first").addClass("t-active");
        }

        /**
         * 获取市图片和介绍
         */
        function getCityInfo(linkageid){
            $.ajax({
                url:'/index.php?m=home&c=index&a=szcsxcCityInfo',
                type:'get',
                data:{'linkageid':linkageid},
                success:function(res){
                    var src = res.image ? res.image:'/Resources/Skin/2019/image/zw-bw.gif';
                    $("#jg-introduce>img").attr("src",src);
                    var info = res.introduce ? res.introduce : "无介绍信息";
                    $("div.jgwz").html(info);
                }
            });
        }

        /**
         * 获取参数
         */
        function getParams(){
            var provinceid = $("div.shen-top").data("linkageid");
            var cityid = $("div.province>div.pro-content>span").find("a.red").data("linkageid");
            var typeid = $("div.goverment-nav>ul").find("li.red").data("linkid");
            var data = {
                'cityid':provinceid +","+cityid,
                'typeid':typeid,
                'type':'news',
                'page':1
            };
            return data;
        }

        /**
         * 分页
         */
        var total,pageSize,curPage,totalPage;   //分页信息：数据总量、每页显示数量、当前页码、总页数
        /**
         * 获取数据
         */
        function getData(data){
            $.ajax({
                url: "/index.php?m=home&c=index&a=szcsxcData",
                method:'get',
                data:data,
                success:function(res){
                    console.log(res);
                    if(res.code == 200){
                        $(".commissions-page").html('');
                        if(res.data.length > 0){
                            // $(".commissions-page").html('');
                            var item = "";
                            if(res.type == "news"){
                                for(var i = 0; i < res.data.length; i++){
                                    item += "<li><div class='commissions-list-img lf'>";
                                    item += "<img style='width:220px;height:160px' src='"+res.data[i].thumb+"'/></div>";
                                    item += "<div class='commissions-list-text lf wid6'><h5>"+res.data[i].title+"<span>"+res.data[i].inputtime+"</span></h5>";
                                    item += "<p>"+res.data[i].description+"</p>";
                                    item += "<p><a target='_blank' href='/index.php?m=home&c=index&a=shows&catid=206&id="+res.data[i].mid+"'>&lt;详情&gt;</a></p></div></li>"
                                }
                                $("#content").html(item);
                                //分页
                                total = res.pageinfo.total; //总记录数
                                pageSize = res.pageinfo.pageSize; //每页显示条数
                                curPage = res.pageinfo.curPage; //当前页
                                totalPage = res.pageinfo.totalPage; //总页数
                                getPageBar();
                            }else{
                                // $(".commissions-page").html('');
                                for (var i = 0; i < res.data.length; i++){
                                    item += "<li>";
                                    item += "<h3 class='list-title'><span> </span><a target='_blank' href='/index.php?m=home&c=index&a=shows&catid=207&id="+res.data[i].mid+"'>"+res.data[i].title+"</a></h3>";
                                    item += "<div class='list-left lf'>";
                                    item += "<p>融资资金：<span class='money'>"+res.data[i].quasi_amount+"亿元</span></p>";
                                    item += "<p>所属行业：<span >"+res.data[i].industryid+"</span></p>";
                                    item += "<p>所在地区："+res.data[i].cityid+"</p>";
                                    item += "<p>项目融资方式："+res.data[i].finacing_style+"</p>";
                                    item += "</div>";
                                    item += "<div class='list-center lf'>";
                                    item += "<i class='icon iconfont icon-icondingdanxiangqingxiansheng'></i>"+res.data[i].contact_name+"<br />"+res.data[i].company;
                                    item += "</div>";
                                    item += "<div class='list-right' style='float:right;margin-right:70px'><button>约谈资方</button></div>";
                                    item += "</li>";
                                }
                                $("#content").html(item);
                                //分页
                                total = res.pageinfo.total; //总记录数
                                pageSize = res.pageinfo.pageSize; //每页显示条数
                                curPage = res.pageinfo.curPage; //当前页
                                totalPage = res.pageinfo.totalPage; //总页数
                                getPageBar();
                            }
                        }else{
                            item = "<h5>暂无数据</h5>";
                            $("#content").html(item);
                        }
                    }
                }
            });
        }

        //获取分页条（分页按钮栏的规则和样式根据自己的需要来设置）
        function getPageBar(){
            if(curPage > totalPage) {
                curPage = totalPage;
            }
            if(curPage < 1) {
                curPage = 1;
            }
            pageBar = "";
            // pageBar +="<span class='pageBtn' style='margin-right:25px;'>当前共<b style='color:#B80000'> "+totalPage+"</b> 页</span>";
            //如果不是第一页
            if(curPage != 1){
                // pageBar += "<span class='pageBtn'><a href='javascript:turnPage(1)'>首页</a></span>";
                pageBar += "<span class=''><a href='javascript:turnPage("+(curPage-1)+")'>上一页</a></span>";
            }
            //显示的页码按钮(5个)
            var start,end;
            if(totalPage <= 5) {
                start = 1;
                end = totalPage;
            } else {
                if(curPage-2 <= 0) {
                    start = 1;
                    end = 5;
                } else {
                    if(totalPage-curPage < 2) {
                        start = totalPage - 4;
                        end = totalPage;

                    } else {
                        start = curPage - 2;
                        end = parseInt(curPage) + 2;
                    }
                }
            }
            for(var i=start;i<=end;i++) {
                if(i == curPage) {
                    pageBar += "<span class='page-on'><a href='javascript:;'>"+i+"</a></span>";
                } else {
                    pageBar += "<span class=''><a href='javascript:turnPage("+i+")'>"+i+"</a></span>";
                }
            }
            //如果不是最后页
            if(curPage != totalPage){
                pageBar += "<span class=''><a href='javascript:turnPage("+(parseInt(curPage)+1)+")'>下一页</a></span>";
                pageBar += "<span class=''><a href='javascript:turnPage("+totalPage+")'>尾页</a></span>";
            }

            $(".commissions-page").html(pageBar);
        }

        //点击页码获取数据
        function turnPage(curPage){
            var data = getParams();
            data.page = curPage;
            getData(data);
        }

		
</script>
</body>

</html>