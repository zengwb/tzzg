<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onClick="forbidden(this,'<?php echo ($row["linkurl"]); ?>')" class="wordcolor first" ><?php echo ($row['name']); ?></a>
                            <!--style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"-->
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  ><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                    function forbidden(event,url){
                        var l = $(event).next("ul").find("li").length;
                        if(url == ""){
                            return false;
                        }else{
                            window.open(url);
                        }
                    }

                    $(function() {
                        var navi = $(".nav-list>ul>li");
                        navi.each(function() {
                            if ($(this).find("ul>li").length) {
                            } else {
                                $(this).find("i, ul").hide();
                            }
                        });
                        navi.find("a").each(function(){
                            if($(this).attr('naviid') == 1034){
                                $(this).removeClass("wordcolor");
                            }
                        });

                        /*禁止点击未开启功能  start*/
                        $(".nav-list>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            if(id == 1060 || id == 1060){
                                alert("正在测试中");
                                return false;
                            }
                        });

                        $(".nav-list>ul>li>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            console.log(id);
                            if(id == 1072 || id == 1080 || id == 1081 || id == 1073  || id == 1074 || id == 1075 || id == 1076 || id == 1077){
                                alert("正在测试中");
                                return false;
                            }
                        });
                        /*禁止点击未开启功能  end*/
                    });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat;background-position:1px;border:1px solid rgb(230,166,166);border-left: none;"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<!--幻灯片-->
    <div class="index-banner" style="margin-top:160px;"> 
        <!--<img src="/Resources/Skin/2019/image/banner1.gif" alt="" class="img-wid" />-->
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner1.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner2-1.jpg" class="swiper-img" /> </div>
                 <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner3-1.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner4-1.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner5-1.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner3.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner4.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner5.jpg" class="swiper-img" /> </div>

                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner6.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner7.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner8.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner9.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner10.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner11.jpg" class="swiper-img" /> </div>
                <div class="swiper-slide"> <img src="/Resources/Skin/2019/image/banner12.jpg" class="swiper-img" /> </div>
            </div>
            <!-- 如果需要分页器 -->
            <div class="swiper-pagination"></div>
            <!-- 如果需要导航按钮 -->
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </div>
    <div class="header-info">
        <div class="info-word"> <img src="/Resources/Skin/2019/image/head-word.gif" alt="文字信息" class="img-wid" /> </div>
    </div>
<div class="content">
    
		<div id="xwzx" class="con-xwzx xwzx">
            <div>
				<p class="jzzs-title  text-center">新闻中心</p>
			</div>
			<div class="jzzs-wid">
                
                <?php $_siteid = $site['siteid'];$_catid = 188;$_modelid = 0;$_typeid = 0;$_keyword = '';$_field = '*';$_count = 5;$_order = '`listorder` ASC, `inputtime` DESC';$_where = [];$_where['siteid'] = $_siteid;$_where['catid'] = $_catid;$_category = D('Admin/Category')->where($_where)->find();$_modelid = $_modelid ? : $_category['modelid'];$_where = ['modelid' => $_modelid];$_model = D('Admin/Model')->where($_where)->find();$_table = $_model['table'];$_table_data = $_model['data'] ? $_table.'_data' : null;if (!empty($_table)) :$_catids = D('Admin/Category')->childid($_catid, $_siteid);$_where = [];$_where['catid'] = ['IN', $_catids];$_where['disabled'] = 0;if ($_typeid) $_where['typeid'] = $_typeid;if (!empty($_keyword)) :$_keys = '' ? : '';$_keyr = [$_keys => '<font class="cf30">'.$_keys.'</font>'];$_temp['title'] = ['LIKE', "%$_keyword%"];$_temp['keywords'] = ['LIKE', "%$_keyword%"];$_temp['_logic'] = 'OR';$_where['_complex'] = $_temp;endif;$_nums = D($_table)->where($_where)->count();$_rows = $_count;$_page = cms_page($_nums, $_rows);$_limit = $_page->firstRow.','.$_page->listRows;$_ress = D($_table)->where($_where)->limit($_limit)->order($_order)->field($_field)->select();foreach ($_ress as $key => $data) :$_where = ['catid' => $data['catid']];$_catrs = D('Admin/category')->where($_where)->find();$data['catname'] = $_catrs['catname'];$data['title'] = $data['title'] ? :$data['title'];if ($_table_data) :$_where = ['mid' => $data['mid']];$data_data = D($_table_data)->where($_where)->find();$data = array_merge($data, $data_data);endif;if (!$_keyr) $data['title2'] = $data['title'];else $data['title2'] = strtr($data['title'], $_keyr);$data['style'] = unserialize($data['style']);$data['thumb'] = strtr($data['thumb'], ["./" => "/"]);$_args = [];$_args['catid'] = $data['catid'];$_args['id'] = $data['mid'];$_link = U('home/index/shows', $_args);$_html = 'show-'.$data['catid'].'-';$_html.= $data['mid'].'.html';$_link = $_link;$data['link'] = $data['url'] ?: $_link; if($key == 0): ?><div class="floatleft xw-left">
					<img src="<?php echo ($data["thumb"]); ?>" alt="<?php echo ($data["title"]); ?>" style="width:640px;" />
                    <div class="red-blcok">
						<div class="xw-num whitecolor">0<?php echo ($key+1); ?></div>
						<p class="xw-date whitecolor"><?php echo (date("Y.m.d",$data['inputtime'])); ?></p>
					</div>
					<p class="xw-title"> <a href="<?php echo ($data['link']); ?>" target="_blank" title="<?php echo ($data['title']); ?>" style="font-weight:<?php echo ($data['style']['bold']); ?>; color:<?php echo ($data['style']['color']); ?>;"><?php echo (mb_substr($data['title'],0,20,'utf-8')); ?></a></p>
					<p class="xw-info"><?php echo (mb_substr($data['description'],0,82,'utf-8')); ?></p>
					<p class="mar15"><a href="<?php echo ($data["link"]); ?>" class="xw-link">进入详情</a></p>
				</div><?php endif; endforeach; endif; if (!$_ress) {echo "<div class=\"cms-empty\"></div>";} ?>
                
				<div class="floatright xw-right">
                    <?php $_siteid = $site['siteid'];$_catid = 188;$_modelid = 0;$_typeid = 0;$_keyword = '';$_field = '*';$_count = 5;$_order = '`listorder` ASC, `inputtime` DESC';$_where = [];$_where['siteid'] = $_siteid;$_where['catid'] = $_catid;$_category = D('Admin/Category')->where($_where)->find();$_modelid = $_modelid ? : $_category['modelid'];$_where = ['modelid' => $_modelid];$_model = D('Admin/Model')->where($_where)->find();$_table = $_model['table'];$_table_data = $_model['data'] ? $_table.'_data' : null;if (!empty($_table)) :$_catids = D('Admin/Category')->childid($_catid, $_siteid);$_where = [];$_where['catid'] = ['IN', $_catids];$_where['disabled'] = 0;if ($_typeid) $_where['typeid'] = $_typeid;if (!empty($_keyword)) :$_keys = '' ? : '';$_keyr = [$_keys => '<font class="cf30">'.$_keys.'</font>'];$_temp['title'] = ['LIKE', "%$_keyword%"];$_temp['keywords'] = ['LIKE', "%$_keyword%"];$_temp['_logic'] = 'OR';$_where['_complex'] = $_temp;endif;$_nums = D($_table)->where($_where)->count();$_rows = $_count;$_page = cms_page($_nums, $_rows);$_limit = $_page->firstRow.','.$_page->listRows;$_ress = D($_table)->where($_where)->limit($_limit)->order($_order)->field($_field)->select();foreach ($_ress as $key => $data) :$_where = ['catid' => $data['catid']];$_catrs = D('Admin/category')->where($_where)->find();$data['catname'] = $_catrs['catname'];$data['title'] = $data['title'] ? :$data['title'];if ($_table_data) :$_where = ['mid' => $data['mid']];$data_data = D($_table_data)->where($_where)->find();$data = array_merge($data, $data_data);endif;if (!$_keyr) $data['title2'] = $data['title'];else $data['title2'] = strtr($data['title'], $_keyr);$data['style'] = unserialize($data['style']);$data['thumb'] = strtr($data['thumb'], ["./" => "/"]);$_args = [];$_args['catid'] = $data['catid'];$_args['id'] = $data['mid'];$_link = U('home/index/shows', $_args);$_html = 'show-'.$data['catid'].'-';$_html.= $data['mid'].'.html';$_link = $_link;$data['link'] = $data['url'] ?: $_link; if($key != 0): ?><div class="wz">
						<div class="floatleft">
							<div class="xw-num">0<?php echo ($key+1); ?></div>
							<p class="xw-date"><?php echo (date("Y.m.d",$data['inputtime'])); ?></p>
						</div>
						<div class="floatright" style="width: 365px;">
							<p class="xw-title"><a href="<?php echo ($data['link']); ?>" target="_blank" title="<?php echo ($data['title']); ?>" style="font-weight:<?php echo ($data['style']['bold']); ?>; color:<?php echo ($data['style']['color']); ?>;"><?php echo (mb_substr($data['title'],0,20,'utf-8')); ?></a></p>
							<p class="xw-info"><?php echo (mb_substr($data['description'],0,82,'utf-8')); ?></p>
						</div>
						<div class="clear"></div>
						<div class="solid-line mar15"></div>
					</div><?php endif; endforeach; endif; if (!$_ress) {echo "<div class=\"cms-empty\"></div>";} ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="click-more">
				<p class="see-more"><a href="/index.php?m=home&c=index&a=lists&catid=188">点击查看更多</a></p>
			</div>
		</div>
		<div class="banner2">
            <script language="javascript" src="/index.php?m=Advert&c=index&a=poster&spaceid=71"></script>
		</div>
		<div id="jzzs" class="con-jzzs jzzs">
			<div class="">
				<div class="red-line"></div>
				<p class="jzzs-title">精准招商</p>
			</div>
			<div class="jzzs-wid">
				<ul>
					<li style="margin-right: 18px;">
						<a target="_blank" href="http://www.touzizhongguo.com.cn/index.php?m=home&c=index&a=shows&catid=185&id=3807">
						<img src="/Resources/Skin/2019/image/sy-jzzs1.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
						<div class="fzzs-bg">凉山州宁南县拉洛布依水寨田园综合体开发建设项目</div>
						</a>
					</li>
					<li style="margin-right: 18px;">
						<a target="_blank" href="http://www.touzizhongguo.com.cn/index.php?m=home&c=index&a=shows&catid=185&id=2750">
							<img src="/Resources/Skin/2019/image/sy-jzzs2.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
						<div class="fzzs-bg">攀枝花市西区沿江快速通道西区段项目</div>
						</a>
					</li>
					<li>
						<a target="_blank" href="http://www.touzizhongguo.com.cn/index.php?m=home&c=index&a=shows&catid=185&id=4081">
						<img src="/Resources/Skin/2019/image/sy-jzzs3.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
						<div class="fzzs-bg">攀枝花市东区金沙江中心区段沿江开发建设项目</div>
					</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="click-more">
				<p class="see-more"><a href="javascript:;">点击查看更多</a></p>
			</div>
		</div>
		<div id="jzfp" class="con-jzfp jzfp">
			<div class="">
				<div class="red-line"></div>
				<p class="jzzs-title">精准扶贫</p>
			</div>
			<div class="jzzs-wid">
				<ul>
					<li style="margin-right: 18px;">
                        <a target="_blank" href="http://www.touzizhongguo.com.cn/index.php?m=home&c=index&a=shows&catid=185&id=3809">
						<img src="/Resources/Skin/2019/image/sy-jzfp1.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
						<div class="fzzs-bg">凉山州宁南县高山林海康养旅游开发项目</div>
					</a>
					</li>
					<li style="margin-right: 18px;">
                        <a target="_blank" href="http://www.touzizhongguo.com.cn/index.php?m=home&c=index&a=shows&catid=185&id=1235">
						<img src="/Resources/Skin/2019/image/sy-jzfp2.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
						<div class="fzzs-bg">凉山州会东县乌金猪原种场 建设项目</div>
					</a>
					</li>
					<li>
                        <a target="_blank" href="http://www.touzizhongguo.com.cn/index.php?m=home&c=index&a=shows&catid=185&id=1155">
						<img src="/Resources/Skin/2019/image/sy-jzfp3.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
						<div class="fzzs-bg">凉山州昭觉县昭觉无公害苦荞麦综合开发项目</div>
					</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="click-more">
				<p class="see-more"><a href="javascript:;">点击查看更多</a></p>
			</div>
		</div>
		<div class="banner2">
			<script language="javascript" src="/index.php?m=Advert&c=index&a=poster&spaceid=72"></script>
		</div>
		<div id="jztz" class="con-jztz jztz">
			<div class="">
				<div class="red-line"></div>
				<p class="jzzs-title">精准投资</p>
			</div>
			<div class="jzzs-wid">
				<ul>
					<li style="margin-right: 18px;">
						<img src="/Resources/Skin/2019/image/sy-jztz1.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
					</li>
					<li style="margin-right: 18px;">
						<img src="/Resources/Skin/2019/image/sy-jztz2.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
					</li>
					<li>
						<img src="/Resources/Skin/2019/image/sy-jztz3.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="click-more">
				<p class="see-more"><a href="javascript:;">点击查看更多</a></p>
			</div>
		</div>
		<div id="jzfw" class="con-jzfw jzfw">
			<div class="">
				<div class="red-line"></div>
				<p class="jzzs-title">精准服务</p>
			</div>
			<div class="jzzs-wid">
				<ul>
					<li style="margin-right: 18px;">
						<img src="/Resources/Skin/2019/image/sy-jzfw1.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
					</li>
					<li style="margin-right: 18px;">
						<img src="/Resources/Skin/2019/image/sy-jzfw2.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
					</li>
					<li>
						<img src="/Resources/Skin/2019/image/sy-jzfw3.gif" alt="精准招商" class="jzzs-imgs" />
						<!--<div class="jzzs-link">
							<p class="jzzs-info">股权投资</p>
							<p class="see-info"><a href="javascript:;">查看详情</a></p>
						</div>-->
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="click-more">
				<p class="see-more"><a href="javascript:;">点击查看更多</a></p>
			</div>
		</div>
	</div>
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

<div class="content-body" style="display: none;">
		<div class="left-nav">
			<ul>
				<li class="xwzx active"><a href="#xwzx">新闻中心</a></li>
				<li class="jzzs"><a href="#jzzs">精准招商</a></li>
				<li class="jzfp"><a href="#jzfp">精准扶贫</a></li>
				<li class="jztz"><a href="#jztz">精准投资</a></li>
				<li class="jzfw"><a href="#jzfw">精准服务</a></li>
			</ul>
		</div>
	</div>
	<div class="tk-cs">
		<div class="cs-word">当前网站正在测试中，敬请期待。</div>
	</div>
	<script type="text/javascript">
	
		var arrOffsetTop = [
			$(".xwzx").offset().top - 220,
			$(".jzzs").offset().top - 220,
			$(".jzfp").offset().top - 220,
			$(".jztz").offset().top - 220,
			$(".jzfw").offset().top - 220
		]		
		
		$(function(){
			$(window).scroll(function(){
				var sTop = $(window).scrollTop();
				var sTops = parseInt(sTop);
				var foot = $(".footer").offset().top - 580;
				if(sTops > 520){
					$(".content-body").fadeIn(500,function(){
						$(this).show();
					})
				}else{
					$(".content-body").fadeOut(500,function(){
						$(this).hide();
					})				
				}
				if (sTops > arrOffsetTop[0] && sTops < arrOffsetTop[1]) {
					$(".left-nav .xwzx").addClass('left-nav active').siblings().removeClass('left-nav active');
				} else if (sTops >= arrOffsetTop[1] && sTops < arrOffsetTop[2]) {
					$(".left-nav .jzzs").addClass('left-nav active').siblings().removeClass('left-nav active');
				} else if (sTops >= arrOffsetTop[2] && sTops < arrOffsetTop[3]) {
					$(".left-nav .jzfp").addClass('left-nav active').siblings().removeClass('left-nav active');
				} else if (sTops >= arrOffsetTop[3] && sTops < arrOffsetTop[4]) {
					$(".left-nav .jztz").addClass('left-nav active').siblings().removeClass('left-nav active');
				} else if (sTops >= arrOffsetTop[4] && sTops < foot) {
					$(".left-nav .jzfw").addClass('left-nav active').siblings().removeClass('left-nav active');
				}
			})
			$(".left-nav ul li").click(function () {
				$(this).addClass('left-nav active').siblings().removeClass('left-nav active');
				$('body, html').animate({scrollTop: arrOffsetTop[$(this).index()]}, 500);			
			});
		})
		$(function () {
			var mySwiper = new Swiper('.swiper-container', {
				direction: 'horizontal', // 垂直切换选项
				autoplayDisableOnInteraction: false,
				autoplay: true,
				loop: true, 
				// 如果需要分页器
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},

				// 如果需要前进后退按钮
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			})

            //删除新闻列表 最后一个的下划线
			$("div.wz").each(function(){
                if($(this).index() == ($("div.wz").length - 1)){
                    $(this).find("div.solid-line").remove();
                }
			});
		});
		// 登录
		let logWord = function () {
			let loginPopup = document.getElementById('loginPopup')
			loginPopup.style.display = 'block'
		}
		let popupOff = function () {
			let loginPopup = document.getElementById('loginPopup')
			loginPopup.style.display = 'none'
		}
	</script>
</body>
</html>