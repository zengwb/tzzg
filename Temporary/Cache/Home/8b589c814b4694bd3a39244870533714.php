<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onclick="forbidden(this,'<?php echo ($row['linkurl']); ?>')"   class="wordcolor first" style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"><?php echo ($row['name']); ?></a>
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  style="font-weight:<?php echo ($val['style']['bold']); ?>; color:<?php echo ($val['style']['color']); ?>;"><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                function forbidden(event,url){
                    var l = $(event).next("ul").find("li").length;
                    if(url == ""){
                        return false;
                    }else{
                        window.open(url);
                    }
                }
                $(function() {
                    var navi = $(".nav-list>ul>li");
                    navi.each(function() {
                        if ($(this).find("ul>li").length) {
                        } else {
                            $(this).find("i, ul").hide();
                        }
                    });
                    navi.find("a").each(function(){
                        if($(this).attr('naviid') == 1034){
                            $(this).removeClass("wordcolor");
                        }
                    });

                /*禁止点击未开启功能  start*/
                    // $(".nav-list>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     //if(id != 1039 && id != 1034 && id != 1038 && id != 1035){

                    //     if(id != 1034 && id != 1038 && id != 1035 && id != 1059){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });

                    // $(".nav-list>ul>li>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     if(id != 1037 && id != 1039 && id != 1040 && id != 1041 && id != 1044 && id != 1036 && id != 1045 && id != 1065){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    /*禁止点击未开启功能  end*/
                });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />
<script src="/Resources/Skin/2019/js/php_zwbw.js"></script>

<style type="text/css">
    .selectDom{
        color:#b80000;
    }
    .page-on{
        background:#e60012;
        color:#fff
    }
</style>
    <!-- 政务详情 -->
    <div class="goverment-list mar16">
        <div class="goverment-top"></div>
        <div class="W1224 goverment-commissions ">
            <h3><span></span>部委</h3>
            <div class="goverment-text" id="govermentRegion">

                <?php $_parentid = 3773;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) : if($key == 0): ?><span class="goverment-on" linkid="<?php echo ($row['linkageid']); ?>"><?php echo ($row['name']); ?></span>
                        <?php else: ?>
                        <span linkid="<?php echo ($row['linkageid']); ?>"><?php echo ($row['name']); ?></span><?php endif; ?>
                    <div style="display: none" data-img="<?php echo ($row['image']); ?>"><?php echo ($row['description']); ?></div><?php endforeach; ?>

                <!--<span class="goverment-on">国家发展和改革委员会</span>-->
                <!--<span>教育部</span>-->
                <!--<span>科学技术部</span>-->
                <!--<span>工业和信息化部</span>-->
                <!--<span>国家民族事务委员会</span>-->
                <!--<span>民政部</span>-->
                <!--<span>财政部</span>-->
                <!--<span>人力资源和社会保障部</span><br />-->
                <!--<span>自然资源部</span>-->
                <!--<span>生态环境部</span>-->
                <!--<span>住房和城乡建设部</span>-->
                <!--<span>交通运输部</span>-->
                <!--<span>水利部</span>-->
                <!--<span>农业农村部</span>-->
                <!--<span>商务部文化和旅游部</span>-->
                <!--<span>国家卫生健康委员会</span><br />-->
                <!--<span>中国人民银行</span>-->

            </div>
        </div>
    </div>
   	<div class="goverment-img-text">
   		<div>
   			<img src="/Resources/Skin/2019/image/zw-bw.gif" alt="政务部委" class="bw-img" />
   			<p class="bw-info">中华人民共和国国家发展和改革委员会（简称：国家发展改革委，National Development and Reform Commission）。是国务院的职能机构。国家发改委的前身是国家计划委员会，国家发展计划委员会。</p>
   		</div>
   	</div>
    <div class="goverment-body">
    	<div class="goverment-nav floatleft">
    		<ul>
                <?php $_parentid = 4024;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) : if($row["linkageid"] == 4025): ?><li class="tab-item red" typeid="<?php echo ($row['linkageid']); ?>"><a class="red"><span class="red-fk"></span><?php echo ($row['name']); ?></a></li>
                        <?php else: ?>
                        <li class="tab-item" typeid="<?php echo ($row['linkageid']); ?>"><a><span class="red-fk"></span><?php echo ($row['name']); ?></a></li><?php endif; endforeach; ?>

    			<!--<li class="tab-item red"><span class="red-fk"></span>成果展示</li>-->
    			<!--<li class="tab-item"><span class="red-fk"></span>专题专访</li>-->
    			<!--<li class="tab-item"><span class="red-fk"></span>政策解读</li>-->
    			<!--<li class="tab-item"><span class="red-fk"></span>规划与投资新态势</li>-->
    			<!--<li class="tab-item"><span class="red-fk"></span>活动招商</li>-->
    			<!--<li class="tab-item"><span class="red-fk"></span>项目展示</li>-->
    			<!--<li class="tab-item"><span class="red-fk"></span>态势报告</li>-->
    		</ul>
    		<div>
    			<input type="text" class="zw-input floatleft" placeholder="请输入你要查找的内容" />
				<input type="button" class="zw-btns floatleft" style="background: url('/Resources/Skin/2019/image/zw-btn.gif') no-repeat center center;"/>
				<div class="clear"></div>
    		</div>
    	</div>
	   <div class="commissions-list no-mar wid9 floatright">
            <ul class="fb-tab selected" id="zwbz_list">

            </ul>

        </div>
	    <div class="clear"></div>
    </div>
    <!-- 尾部 -->
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

</body>
<script type="text/javascript">
			$(function () {
			$(".goverment-nav .tab-item").click(function () {
				$(this).addClass("red").siblings().removeClass("red");
				$(".commissions-list .fb-tab").eq($(this).index()).show().siblings().hide();
			});
		});
</script>
</html>