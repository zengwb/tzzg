<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onclick="forbidden(this,'<?php echo ($row['linkurl']); ?>')"   class="wordcolor first" style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"><?php echo ($row['name']); ?></a>
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  style="font-weight:<?php echo ($val['style']['bold']); ?>; color:<?php echo ($val['style']['color']); ?>;"><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                function forbidden(event,url){
                    var l = $(event).next("ul").find("li").length;
                    if(url == ""){
                        return false;
                    }else{
                        window.open(url);
                    }
                }
                $(function() {
                    var navi = $(".nav-list>ul>li");
                    navi.each(function() {
                        if ($(this).find("ul>li").length) {
                        } else {
                            $(this).find("i, ul").hide();
                        }
                    });
                    navi.find("a").each(function(){
                        if($(this).attr('naviid') == 1034){
                            $(this).removeClass("wordcolor");
                        }
                    });

                /*禁止点击未开启功能  start*/
                    // $(".nav-list>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");
                    //
                    //     //if(id != 1039 && id != 1034 && id != 1038 && id != 1035){
                    //
                    //     if(id != 1034 && id != 1038 && id != 1035 && id != 1059){
                    //
                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    //
                    // $(".nav-list>ul>li>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");
                    //
                    //     if(id != 1050 && id != 1049 && id != 1048 && id != 1057 && id != 1062 && id != 1063 && id != 1064 && id != 1037 && id != 1039 && id != 1040 && id != 1041 && id != 1044 && id != 1036 && id != 1045 && id != 1065){
                    //
                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    /*禁止点击未开启功能  end*/
                });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

	<!-- banner图 -->
	<div class="activity-banner mar16">
		<img src="/Resources/Skin/2019/Image/activity-001.gif" class="banner-img" alt="活动招商">
		<p class="details-color"></p>
	</div>
	<!-- 招商列表 -->
	<div class="acitivity-list">
		<div class="activity-list-choice W1224">
			<div class="management-title">
				<h3>精准扶贫项目</h3>
			</div>
			<div class="choice-region">
				<div id="category" class="category"><span class="lf">产业类别：</span>
					<?php $_parentid = 3868;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><div onclick="manufacture(this,'<?php echo ($row['linkageid']); ?>')" id="categoryAgriculture" industryid1="<?php echo ($row['linkageid']); ?>" class="category1 lf"><?php echo ($row['name']); ?><i class="icon iconfont jiantou"></i></div><?php endforeach; ?>
					<div onclick="more1() " class="more2">更多<i id="jiantou1" class="icon iconfont  jiantou"></i></div>

				</div>
				<div id="agriculture" class="agriculture manufacture" style="display:none">
				</div>
				<div class="region hezuo">合作方式：
					<?php $_parentid = 3304;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span onclick="hezuo1(this,'<?php echo ($row['linkageid']); ?>')"><?php echo ($row['name']); ?></span><?php endforeach; ?>

				</div>
				<div class="region touzhi">总投资额：
					<?php $_parentid = 4109;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span onclick="touzhi1(this,'<?php echo ($row['linkageid']); ?>')"><?php echo ($row['name']); ?></span><?php endforeach; ?>
				</div>
				<div class="region" id="region">
					所在地区：
					<div class="region-select">
						<select id="shen" style="width: 115px; height: 30px;">
							<option value="" class="bt-input" >请输入省份</option>
							<?php $_parentid = 1;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><option value="<?php echo ($row['linkageid']); ?>" class="bt-input"><?php echo ($row['name']); ?></option><?php endforeach; ?>
						</select>
						<select id="city" style="width: 115px; height: 30px; display:none;">
						</select>
						<select id="qu" style="width: 115px; height: 30px; display:none;">
						</select>
					</div>
				</div>
				<div>
					<p class="acitivity-search floatright"><input type="search" placeholder="在当前条件下搜索">
						<span class="acitivity-search-font">
							<i class="icon iconfont icon-sousuo1"></i>
						</span>
					</p>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div>
	<div class="acitivity-sort W1224">
		<div class="acitivity-sort-left lf">
			<div class="sort-top px">
				<span onclick="px(this,'zhpx')" class="sort-on">综合排序<i class="icon iconfont jiantou"></i></span>
				<span onclick="px(this,'sjpx')">更改时间<i class="icon iconfont jiantou"></i></span>
				<span onclick="px(this,'pricepx')">金额排序<i class="icon iconfont jiantou"></i></span>
				<p class="sort-total">共有<span id='abcd'>300+</span>个符合要求的项目信息</p>
			</div>
			<div class="sort-list">
				<!-- id='xmduij' -->
				<ul id='xmduij'>
					<li>
						<h3 class="list-title"><span> </span><a href="./majorDetails.html"><a href="./majorDetails.html">安徽某智能设备项目股权融资500万元-1000万元</a></a>
						</h3>
						<div class="list-left lf">
							<p>总投资额：<span class="money">500万元-1000万元</span></p>
							<p>所在地区：安徽省<span class="industry">所属行业：人工智能 智能设备</span></p>
							<p>项目融资方式：股权融资</p>
							<p><i class="icon iconfont icon-youjian"></i>1条留言</p>
						</div>
						<div class="list-center lf">
							<i class="icon iconfont icon-icondingdanxiangqingxiansheng"></i>刘先生<br />
							安徽省宿州市某公司
						</div>
						<div class="list-right lf">
							<button>约谈资方</button>
						</div>
					</li>
					<li>
						<h3 class="list-title"><span> </span> <a href="./majorDetails.html">安徽某智能设备项目股权融资500万元-1000万元</a></h3>
						<div class="list-left lf">
							<p>总投资额：<span class="money">500万元-1000万元</span></p>
							<p>所在地区：安徽省<span class="industry">所属行业：人工智能 智能设备</span></p>
							<p>项目融资方式：股权融资</p>
							<p><i class="icon iconfont icon-youjian"></i>1条留言</p>
						</div>
						<div class="list-center lf">
							<i class="icon iconfont icon-icondingdanxiangqingxiansheng"></i>刘先生<br />
							安徽省宿州市某公司
						</div>
						<div class="list-right lf">
							<button>约谈资方</button>
						</div>
					</li>
					<li>
						<h3 class="list-title"><span> </span> <a href="./majorDetails.html">安徽某智能设备项目股权融资500万元-1000万元</a></h3>
						<div class="list-left lf">
							<p>总投资额：<span class="money">500万元-1000万元</span></p>
							<p>所在地区：安徽省<span class="industry">所属行业：人工智能 智能设备</span></p>
							<p>项目融资方式：股权融资</p>
							<p><i class="icon iconfont icon-youjian"></i>1条留言</p>
						</div>
						<div class="list-center lf">
							<i class="icon iconfont icon-icondingdanxiangqingxiansheng"></i>刘先生<br />
							安徽省宿州市某公司
						</div>
						<div class="list-right lf">
							<button>约谈资方</button>
						</div>
					</li>
					<li>
						<h3 class="list-title"><span> </span> <a href="./majorDetails.html">安徽某智能设备项目股权融资500万元-1000万元</a></h3>
						<div class="list-left lf">
							<p>总投资额：<span class="money">500万元-1000万元</span></p>
							<p>所在地区：安徽省<span class="industry">所属行业：人工智能 智能设备</span></p>
							<p>项目融资方式：股权融资</p>
							<p><i class="icon iconfont icon-youjian"></i>1条留言</p>
						</div>
						<div class="list-center lf">
							<i class="icon iconfont icon-icondingdanxiangqingxiansheng"></i>刘先生<br />
							安徽省宿州市某公司
						</div>
						<div class="list-right lf">
							<button>约谈资方</button>
						</div>
					</li>
					<li>
						<h3 class="list-title"><span> </span> <a href="./majorDetails.html">安徽某智能设备项目股权融资500万元-1000万元</a></h3>
						<div class="list-left lf">
							<p>总投资额：<span class="money">500万元-1000万元</span></p>
							<p>所在地区：安徽省<span class="industry">所属行业：人工智能 智能设备</span></p>
							<p>项目融资方式：股权融资</p>
							<p><i class="icon iconfont icon-youjian"></i>1条留言</p>
						</div>
						<div class="list-center lf">
							<i class="icon iconfont icon-icondingdanxiangqingxiansheng"></i>刘先生<br />
							安徽省宿州市某公司
						</div>
						<div class="list-right lf">
							<button>约谈资方</button>
						</div>
					</li>
				</ul>
				<div class="sort-footer">
					<div class="commissions-page">
						    <span>上一页</span>
						    <span class="page-on">1</span>
						    <span>2</span>
						    <span>3</span>
						    <span>...</span>
						    <span>尾页</span>
						    <span>下一页</span>
					</div>
					<p>共有<span id='abc'>300+</span>个符合要求的项目信息</p>
					<!-- <img src="./img/sort-001" alt=""> -->
				</div>
			</div>
		</div>
		<div class="acitivity-sort-right lf">
			<div class="acitivity-sort-right-top">
				<p>案列展示</p>
			</div>
			<div class="acitivity-sort-right-img">
				<p>
					<img src="./img/exhibition-001_03.png" alt=""><br />
					<span>融资金额：200万元<br /><b>所属行业：IT互联网</b> <br />
						河南化工医药网信息服务平台项目</span>
				</p>
				<p>
					<img src="./img/exhibition-001_03.png" alt=""><br />
					<span>融资金额：200万元<br /><b>所属行业：IT互联网</b> <br />
						河南化工医药网信息服务平台项目</span>
				</p>
				<p>
					<img src="./img/exhibition-001_03.png" alt=""><br />
					<span>融资金额：200万元<br /><b>所属行业：IT互联网</b> <br />
						河南化工医药网信息服务平台项目</span>
				</p>


			</div>
		</div>
	</div>
	<!-- 活动 -->

	<!-- 尾部 -->
	<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

	<script type="text/javascript" language="javascript" src="/Resources/Skin/2019/js/linkage.js"></script>
	<script>
		var table='v1_tzzg_fpxm';
		is_status=2;
		fp=1;
		function hezuo1(m,n)
			{
				hezuo = n;
				$(m).addClass("red").siblings().removeClass("red");
				list();
			}
		function touzhi1(m,n)
			{
				//alert(n);
				touz = n;
				$(m).addClass("red").siblings().removeClass("red");
				list();
			}
		let more = function () {
			let region = document.getElementById('region')
			let jiantou = document.getElementById('jiantou')
			// console.log((jiantou.classList)[2])
			if ((jiantou.classList)[2] == 'jiantou') {
				region.style.height = '168px';

				jiantou.classList.remove('jiantou')
				jiantou.classList.add('jiantoutop')
			} else {
				jiantou.classList.add('jiantou')
				jiantou.classList.remove('jiantoutop')
				region.style.height = '58px';
			}
		}
		let more1 = function () {
			let category = document.getElementById('category')
			let jiantou1 = document.getElementById('jiantou1')

			if (!(category.classList)[1]) {
				category.classList.add('category-height')
				jiantou1.classList.remove('jiantou')
				jiantou1.classList.add('jiantoutop')

			} else {
				category.classList.remove('category-height')
				jiantou1.classList.remove('jiantoutop')
				jiantou1.classList.add('jiantou')

			}

		}
		let more2 = function (m, n) {
			let m1 = document.getElementById(m)
			let n1 = document.getElementById(n)
			console.log(m + '-height')
			if (!(m1.classList)[1]) {
				m1.classList.add(m + '-height')
				n1.classList.remove('jiantou')
				n1.classList.add('jiantoutop')

			} else {
				m1.classList.remove(m + '-height')
				n1.classList.remove('jiantoutop')
				n1.classList.add('jiantou')

			}
		}
		let manufacture = function (m, n) {
			$('#agriculture').attr('style',"display:none");
			linkage(n);
			$('#agriculture').attr('style',"display:block");
		}
	</script>
</body>

</html>