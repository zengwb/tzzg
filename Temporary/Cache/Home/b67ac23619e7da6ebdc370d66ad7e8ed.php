<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onclick="forbidden(this,'<?php echo ($row['linkurl']); ?>')"   class="wordcolor first" style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"><?php echo ($row['name']); ?></a>
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  style="font-weight:<?php echo ($val['style']['bold']); ?>; color:<?php echo ($val['style']['color']); ?>;"><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                function forbidden(event,url){
                    var l = $(event).next("ul").find("li").length;
                    if(url == ""){
                        return false;
                    }else{
                        window.open(url);
                    }
                }
                $(function() {
                    var navi = $(".nav-list>ul>li");
                    navi.each(function() {
                        if ($(this).find("ul>li").length) {
                        } else {
                            $(this).find("i, ul").hide();
                        }
                    });
                    navi.find("a").each(function(){
                        if($(this).attr('naviid') == 1034){
                            $(this).removeClass("wordcolor");
                        }
                    });

                /*禁止点击未开启功能  start*/
                    // $(".nav-list>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");

                    //     //if(id != 1039 && id != 1034 && id != 1038 && id != 1035){

                    //     if(id != 1034 && id != 1038 && id != 1035 && id != 1059){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });

                    // $(".nav-list>ul>li>ul>li>a").click(function(){
                    //     var id = $(this).attr("naviid");


                    //     if(id != 1037 && id != 1039 && id != 1040 && id != 1041 && id != 1044 && id != 1036 && id != 1045 && id != 1065){

                    //         alert("正在测试中");
                    //         return false;
                    //     }
                    // });
                    /*禁止点击未开启功能  end*/
                });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />
	<!-- banner图 -->
	<div class="activity-banner mar16">
		<img src="/Resources/Skin/2019/image/activity-001.gif" class="banner-img" alt="活动招商">
	</div>
	<!-- 招商列表 -->
	<div class="acitivity-list">
		<div class="acitivity-list-top"></div>
		<div class="activity-list-choice W1224">
			<div class="choice-title"><span></span>
				<h3>会务招商</h3>
			</div>
			<div class="choice-region">	
				<div class="enterprise">类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型：

                    <?php $_parentid = 4044;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span data-typeid="<?php echo ($row["linkageid"]); ?>" onclick="clickType(<?php echo ($row["linkageid"]); ?>)" <?php if($key == 0): ?>class="red"<?php endif; ?>><?php echo ($row["name"]); ?></span><?php endforeach; ?>

				</div>
				<div class="country">国家部委：
                    <?php $_parentid = 4031;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span data-buweiid="<?php echo ($row["linkageid"]); ?>" onclick="clickBuwei(<?php echo ($row["linkageid"]); ?>)" <?php if($key == 0): ?>class="red"<?php endif; ?>><?php echo ($row["name"]); ?></span><?php endforeach; ?>

				</div>
				<div class="country">发布时间：
					<span class="red" data-time="inoneweek">一周内</span>
					<span data-time="inonemonth">一月内 </span>
					<span data-time="inonequarter">一季度内</span>
					<span data-time="inhalfyear">半年内</span>
					<span data-time="inoneyear">一年内</span>
					<span data-time="overoneyear">一年以上</span>
				</div>
				<div class="enterprise">适用单位：
                    <?php $_parentid = 3994;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span data-syqyid="<?php echo ($row["linkageid"]); ?>" onclick="clickCompany(<?php echo ($row["linkageid"]); ?>)" <?php if($key == 0): ?>class="red"<?php endif; ?>><?php echo ($row["name"]); ?></span><?php endforeach; ?>
				</div>

				<div id="investment" class="region">
					所在地区：
					<div class="region-select">
                        <select name="shengid" style="width: 115px; height: 30px;">
                            <option value="0" class="bt-input" selected="selected">请选择省份</option>
                            <?php $_parentid = 1;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><option value="<?php echo ($row['linkageid']); ?>" class="bt-input"><?php echo ($row['name']); ?></option><?php endforeach; ?>

                        </select>
                        <select name="shiji" style="width: 115px; height: 30px;">

                        </select>
                        <select name="xian" style="width: 115px; height: 30px;">

                        </select>
					</div>
				</div>
				<div>
					<p class="acitivity-search floatright"><input type="search" placeholder="在当前条件下搜索">
						<span class="acitivity-search-font">
							<i class="icon iconfont icon-sousuo1"></i>
						</span>
					</p>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="W1224 activity-main">
		<div class="activity-content lf">
			<div class="sort-top ">
				<span class="sort-on" data-order="`listorder` ASC">综合排序<i class="icon iconfont jiantou"></i></span>
				<span data-order="`inputtime` DESC">发布时间<i class="icon iconfont jiantou"></i></span>
			</div>
			<div class="activity-content-list">
				<ul>

				</ul>
				<!--<p class="text-img"><img src="/Resources/Skin/2019/image/sort-001.png" alt=""></p>-->
                <div class="commissions-page"></div>
			</div>
		</div>
		<div class="activity-sider lf">
			<div class="activity-sider-top">
				<h4>热门招商信息</h4>
				<ul>
                    <?php $_siteid = $site['siteid'];$_catid = $catid;$_modelid = 49;$_typeid = 0;$_keyword = '';$_field = '*';$_count = 5;$_order = '`inputtime` DESC';$_where = [];$_where['siteid'] = $_siteid;$_where['catid'] = $_catid;$_category = D('Admin/Category')->where($_where)->find();$_modelid = $_modelid ? : $_category['modelid'];$_where = ['modelid' => $_modelid];$_model = D('Admin/Model')->where($_where)->find();$_table = $_model['table'];$_table_data = $_model['data'] ? $_table.'_data' : null;if (!empty($_table)) :$_catids = D('Admin/Category')->childid($_catid, $_siteid);$_where = [];$_where['catid'] = ['IN', $_catids];$_where['disabled'] = 0;if ($_typeid) $_where['typeid'] = $_typeid;if (!empty($_keyword)) :$_keys = '' ? : '';$_keyr = [$_keys => '<font class="cf30">'.$_keys.'</font>'];$_temp['title'] = ['LIKE', "%$_keyword%"];$_temp['keywords'] = ['LIKE', "%$_keyword%"];$_temp['_logic'] = 'OR';$_where['_complex'] = $_temp;endif;$_nums = D($_table)->where($_where)->count();$_rows = $_count;$_page = cms_page($_nums, $_rows);$_limit = $_page->firstRow.','.$_page->listRows;$_ress = D($_table)->where($_where)->limit($_limit)->order($_order)->field($_field)->select();foreach ($_ress as $key => $row) :$_where = ['catid' => $row['catid']];$_catrs = D('Admin/category')->where($_where)->find();$row['catname'] = $_catrs['catname'];$row['title'] = $row['title'] ? :$row['title'];if ($_table_data) :$_where = ['mid' => $row['mid']];$row_data = D($_table_data)->where($_where)->find();$row = array_merge($row, $row_data);endif;if (!$_keyr) $row['title2'] = $row['title'];else $row['title2'] = strtr($row['title'], $_keyr);$row['style'] = unserialize($row['style']);$row['thumb'] = strtr($row['thumb'], ["./" => "/"]);$_args = [];$_args['catid'] = $row['catid'];$_args['id'] = $row['mid'];$_link = U('home/index/shows', $_args);$_html = 'show-'.$row['catid'].'-';$_html.= $row['mid'].'.html';$_link = $_link;$row['link'] = $row['url'] ?: $_link;?><li>
                        <span>0<?php echo ($key+1); ?></span>
                        <p class="side-top-text"><a href="<?php echo ($row["link"]); ?>" target="_blank"><?php echo ($row["title"]); ?></a></p>
                        <p class="side-top-dizhi"><span><?php echo (date('Y-m-d',$row["inputtime"])); ?></span><i class="icon iconfont icon-dizhi"></i>四川</p>
                    </li><?php endforeach; endif; if (!$_ress) {echo "<div class=\"cms-empty\"></div>";} ?>

				</ul>
			</div>
			<div class="activity-sider-center">
				<img src="/Resources/Skin/2019/image/ac-11_07.png" alt="">
			</div>
			<!--<div class="activity-sider-buttom">-->
				<!--<h4>项目推荐</h4>-->
				<!--<ul>-->
					<!--<li>-->
						<!--<div class="sider-buttom-img lf"><img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""></div>-->
						<!--<div class="sider-buttom-text lf">-->
							<!--<p>融资金额：200万元</p>-->
							<!--<p>所属行业：IT互联网</p>-->
							<!--<p>河南化工医药网信息服务平台项目</p>-->
						<!--</div>-->
					<!--</li>-->
					<!--<li>-->
						<!--<div class="sider-buttom-img lf"><img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""></div>-->
						<!--<div class="sider-buttom-text lf">-->
							<!--<p>融资金额：200万元</p>-->
							<!--<p>所属行业：IT互联网</p>-->
							<!--<p>河南化工医药网信息服务平台项目</p>-->
						<!--</div>-->
					<!--</li>-->
					<!--<li>-->
						<!--<div class="sider-buttom-img lf"><img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""></div>-->
						<!--<div class="sider-buttom-text lf">-->
							<!--<p>融资金额：200万元</p>-->
							<!--<p>所属行业：IT互联网</p>-->
							<!--<p>河南化工医药网信息服务平台项目</p>-->
						<!--</div>-->
					<!--</li>-->
					<!--<li>-->
						<!--<div class="sider-buttom-img lf"><img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""></div>-->
						<!--<div class="sider-buttom-text lf">-->
							<!--<p>融资金额：200万元</p>-->
							<!--<p>所属行业：IT互联网</p>-->
							<!--<p>河南化工医药网信息服务平台项目</p>-->
						<!--</div>-->
					<!--</li>-->
					<!--<li>-->
						<!--<div class="sider-buttom-img lf"><img src="/Resources/Skin/2019/image/exhibition-001_03.png" alt=""></div>-->
						<!--<div class="sider-buttom-text lf">-->
							<!--<p>融资金额：200万元</p>-->
							<!--<p>所属行业：IT互联网</p>-->
							<!--<p>河南化工医药网信息服务平台项目</p>-->
						<!--</div>-->
					<!--</li>-->
				<!--</ul>-->
			<!--</div>-->
		</div>
	</div>
	<!-- 尾部 -->
<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

	<script>
		var more = function () {
			//   alert(1111)
			var region = document.getElementById('region')
			var jiantou = document.getElementById('jiantou')
			// console.log((jiantou.classList)[2])
			if ((jiantou.classList)[2] == 'jiantou') {
				region.style.height = '168px';

				jiantou.classList.remove('jiantou')
				jiantou.classList.add('jiantoutop')
			} else {
				jiantou.classList.add('jiantou')
				jiantou.classList.remove('jiantoutop')
				region.style.height = '58px';
			}
		}
		var more1 = function () {
			var category = document.getElementById('category')
			var jiantou1 = document.getElementById('jiantou1')

			if (!(category.classList)[1]) {
				category.classList.add('category-height')
				jiantou1.classList.remove('jiantou')
				jiantou1.classList.add('jiantoutop')

			} else {
				category.classList.remove('category-height')
				jiantou1.classList.remove('jiantoutop')
				jiantou1.classList.add('jiantou')

			}

		}
		var manufacture = function (m, n) {
			var m1 = document.getElementById(m)
			var n1 = document.getElementById(n)
			// var t1 = document.getElementById(t)
			var category1 = document.getElementsByClassName('category1')
			var manufacture = document.getElementsByClassName('manufacture')
			// if()
			// console.log(1,m1,2,n1,3,t1)
			if ((n1.classList)[2]) {
				m1.style.display = "none"
				n1.classList.remove('category-on')

			} else {
				for (var i = 0; i < category1.length; i++) {
					if ((category1[i].classList)[2])
						category1[i].classList.remove('category-on')
				}
				for (var i = 0; i < manufacture.length; i++) {

					manufacture[i].style.display = "none"
					// console.log(manufacture[i])
				}
				// debugger;

				m1.style.display = "block"
				n1.classList.add('category-on')
			}
		}
/************************* 自定义js代码 样式 *****************************/
$(function () {
    /**
     * 类型和适用单位点击，样式变化
     */
    $("div.enterprise:eq(0)").on("click", "span", function(){
        $("div.enterprise:eq(0)>span").removeClass("red");
        $(this).addClass("red");
    });

    $("div.enterprise:eq(1)").on("click", "span", function(){
        $("div.enterprise:eq(1)>span").removeClass("red");
        $(this).addClass("red");
    });

    /**
     * 点击国家部位和发布时间，样式改变
     */
    $("div.country:eq(0)").on("click", "span", function(){
        $("div.country:eq(0)>span").removeClass("red");
        $(this).addClass("red");
    });

    $("div.country:eq(1)").on("click", "span", function(){
        $("div.country:eq(1)>span").removeClass("red");
        $(this).addClass("red");
    });

    //页面加载完成执行
    var data = getParams();
    getData(data);
});


/******************************功能************************************/
        var cityid="";                      //省市县联动id
        $(function(){
            $("div.region-select>select[name='shiji']").hide();
            $("div.region-select>select[name='xian']").hide();

            $("div.region-select").on("change", "select", function(){
                var parentid = $(this).val();
                var region = $(this).attr("name");

                if(region == "shengid"){
                    cityid = "";
                    if(parentid != 0){
                        cityid += parentid + ",";
                    }
                    $("div.region-select>select[name='shiji']").hide();
                    $("div.region-select>select[name='xian']").hide();
                }else if(region == "shiji"){
                    if(parentid != 0) {
                        cityid = splitestring(cityid,'shiji');
                        cityid += parentid + ",";
                    }else{
                        cityid = splitestring(cityid,'shiji');
                    }
                    $("div.region-select>select[name='xian']").hide();
                }else{
                    if(parentid != 0) {
                        cityid = splitestring(cityid,'xianji');
                        cityid += parentid;
                    }else{
                        cityid = splitestring(cityid,'xianji');
                    }
                }
                var data = getParams();
                data.cityid = cityid;
                getData(data);
                if(parentid != 0 && region != "xian"){
                    getRegion(parentid,region);

                }
            });
        });

        /**
         * 逗号(,)分割字符串
         */
        function splitestring(str,type){
            var arr = str.split(",");
            var resstr = null;
            if(type == "shiji"){
                resstr = arr[0] + ",";
            }else{
                resstr = arr[0] + "," + arr[1] + ",";
            }
            return resstr;

        }

        /**
         * ajax 获取下级联动菜单
         * @param parentid
         * @param region
         */
        function getRegion(parentid,region){
            $.ajax({
                url:'/index.php?m=home&c=index&a=ajaxLinkage',
                method:'get',
                data:{'pid':parentid},
                success:function (res){
                    if(res.length > 0 ){
                        if(region == "shengid"){
                            $("div.region-select>select[name='shiji']").show();
                            $("div.region-select>select[name='shiji']").html("");
                            $("div.region-select>select[name='xian']").html("");
                        }else if(region == "shiji"){
                            $("div.region-select>select[name='xian']").show();
                            $("div.region-select>select[name='xian']").html("");
                        }

                        if(region == "shengid"){
                            var item = "<option value='0' class='bt-input' >请输入市</option>";
                            for(var i = 0; i < res.length; i++){
                                item += "<option value='"+res[i].linkageid+"' class='bt-input region'>"+res[i].name+"</option>";
                            }
                            $("div.region-select>select[name='shiji']").html(item);
                        }else if(region == "shiji"){
                            var item = '<option value="0" class="bt-input" >请输入区</option>';
                            for(var i = 0; i < res.length; i++){
                                item += "<option value='"+res[i].linkageid+"' class='bt-input region'>"+res[i].name+"</option>";
                            }
                            $("div.region-select>select[name='xian']").html(item);
                        }
                    }
                }
            });
        }

        /**
         * 点击搜索
         */
        $("span.acitivity-search-font").click(function(){
           var keywords = $(this).prev("input[type='search']").val();
           // console.log(keywords);
           var data = getParams();
           data.keywords = keywords;
           getData(data);
        });

        /**
         * 点击排序
         */
        $("div.sort-top").on("click", "span", function(){
           $("div.sort-top>span").removeClass("sort-on");
           $(this).addClass("sort-on");
            var order = $(this).data("order");
            var data = getParams();
            data.order = order;
            getData(data);
        });

        /**
         * 点击会务类型
         */
        function clickType(typeid){
           var data =  getParams();
           data.typeid = typeid;
           getData(data);
        }

        /**
         * 点击国家部委
         */
        function clickBuwei(groupid){
            // console.log("国家部委id:" + groupid)
            var data = getParams();
            data.group = groupid;
            getData(data);
        }

        /**
         * 发布时间
         */
        $("div.country:eq(1)").on("click", "span", function(){
            var pubtime = $(this).data("time");
            var data = getParams();
            data.inputtime = pubtime;
            getData(data);
        });
        /**
         * 点击适用单位
         */
        function clickCompany(companyid){
            var data = getParams();
            data.suit_company = companyid;
            getData(data);
        }

        /**
         * 获取参数
         */
        function getParams(){
            //类型
            var type = $("div.enterprise:eq(0)").find("span.red").data("typeid");
            //国家部委
            var buweiid = $('div.country:eq(0)').find("span.red").data("buweiid");
            //发布时间
            var pubtime = $("div.country:eq(1)").find("span.red").data("time");
            //适用单位
            var syqyid = $("div.enterprise:eq(1)").find("span.red").data("syqyid");
            //所在地区
            // console.log(cityid);
            //搜索关键词
            var keywords = $("input[type='search']").val();
            //排序
            var sort = $("div.sort-top").find("span.sort-on").data("order");

            var data = {
                'cityid':cityid != "0," && cityid != ""? cityid : null,
                'group':buweiid,
                'inputtime':pubtime,
                'suit_company':syqyid,
                'typeid':type,
                'keywords':keywords != '' ? keywords : null,
                'order':sort,
                'page':1
            };
            return data;
        }

        /**
         * 获取数据
         * @param data
         */
        function getData(data){
            $.ajax({
                url:"/index.php?m=home&c=index&a=conferencelist",
                method: "get",
                data:data,
                success:function(res){
                    $(".commissions-page").html('');
                    if(res.data != null && res.data.length > 0 ){
                        var item = "";
                        for (var i = 0; i < res.data.length; i++){
                            var image = res.data[i].thumb != "" ? res.data[i].thumb : "./Temporary/Attachment/Article/Image/20190107/1546847890121203.jpg";
                            var title = res.data[i].title;
                            var time = res.data[i].inputtime;
                            var units = res.data[i].units;
                            var be_induid = res.data[i].be_induid;
                            var company = res.data[i].suit_company;
                            var descr = res.data[i].description;
                            var mid = res.data[i].mid;

                            item += "<li>";
                            item += '<div class="activity-content-img lf">';
                            item += '<img src="'+image+'" alt="">';
                            item += '</div>';
                            item += '<div class="activity-content-text lf">';
                            item += '<h3>';
                            item += '<span></span><a  target="_blank" href="/index.php?m=home&c=index&a=shows&catid=205&id='+mid+'">'+title+'</a>';
                            // item += '<p><i class="icon iconfont icon-dizhi"></i>四川</p>';
                            item += '<b>'+time+'</b>';
                            item += '</h3>';
                            item += '<p>'+descr+'</p>';
                            item += '<p class="text-buttom">发布单位：'+units+'</p><span class="text-buttoms">适用企业：'+company+'</span><span class="text-buttoms">受益行业：'+be_induid+'</span>';
                            item += '</div></li>';
                        }

                        $('div.activity-content-list>ul').html(item);
                        //分页
                        total = res.pageinfo.total; //总记录数
                        pageSize = res.pageinfo.pageSize; //每页显示条数
                        curPage = res.pageinfo.curPage; //当前页
                        totalPage = res.pageinfo.totalPage; //总页数
                        getPageBar();
                    }else{
                        item = "<li>暂无数据</li>";
                        $('div.activity-content-list>ul').html(item);
                    }
                }
            });
        }

        //获取分页条（分页按钮栏的规则和样式根据自己的需要来设置）
        function getPageBar(){
            if(curPage > totalPage) {
                curPage = totalPage;
            }
            if(curPage < 1) {
                curPage = 1;
            }
            pageBar = "";
            // pageBar +="<span class='pageBtn' style='margin-right:25px;'>当前共<b style='color:#B80000'> "+totalPage+"</b> 页</span>";
            //如果不是第一页
            if(curPage != 1){
                // pageBar += "<span class='pageBtn'><a href='javascript:turnPage(1)'>首页</a></span>";
                pageBar += "<span class=''><a href='javascript:turnPage("+(curPage-1)+")'>上一页</a></span>";
            }
            //显示的页码按钮(5个)
            var start,end;
            if(totalPage <= 5) {
                start = 1;
                end = totalPage;
            } else {
                if(curPage-2 <= 0) {
                    start = 1;
                    end = 5;
                } else {
                    if(totalPage-curPage < 2) {
                        start = totalPage - 4;
                        end = totalPage;

                    } else {
                        start = curPage - 2;
                        end = parseInt(curPage) + 2;
                    }
                }
            }
            for(var i=start;i<=end;i++) {
                if(i == curPage) {
                    pageBar += "<span class='page-on'><a href='javascript:;'>"+i+"</a></span>";
                } else {
                    pageBar += "<span class=''><a href='javascript:turnPage("+i+")'>"+i+"</a></span>";
                }
            }
            //如果不是最后页
            if(curPage != totalPage){
                pageBar += "<span class=''><a href='javascript:turnPage("+(parseInt(curPage)+1)+")'>下一页</a></span>";
                pageBar += "<span class=''><a href='javascript:turnPage("+totalPage+")'>尾页</a></span>";
            }

            $(".commissions-page").html(pageBar);
        }

        //点击页码获取数据
        function turnPage(curPage){
            var data = getParams();
            data.page = curPage;
            getData(data);
        }

    </script>
</body>

</html>