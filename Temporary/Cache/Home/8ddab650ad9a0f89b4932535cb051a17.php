<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onClick="forbidden(this,'<?php echo ($row["linkurl"]); ?>')" class="wordcolor first" ><?php echo ($row['name']); ?></a>
                            <!--style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"-->
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  ><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                    function forbidden(event,url){
                        var l = $(event).next("ul").find("li").length;
                        if(url == ""){
                            return false;
                        }else{
                            window.open(url);
                        }
                    }

                    $(function() {
                        var navi = $(".nav-list>ul>li");
                        navi.each(function() {
                            if ($(this).find("ul>li").length) {
                            } else {
                                $(this).find("i, ul").hide();
                            }
                        });
                        navi.find("a").each(function(){
                            if($(this).attr('naviid') == 1034){
                                $(this).removeClass("wordcolor");
                            }
                        });

                        /*禁止点击未开启功能  start*/
                        $(".nav-list>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            if(id == 1060 || id == 1060){
                                alert("正在测试中");
                                return false;
                            }
                        });

                        $(".nav-list>ul>li>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            console.log(id);
                            if(id == 1072 || id == 1080 || id == 1081 || id == 1073  || id == 1074 || id == 1075 || id == 1076 || id == 1077){
                                alert("正在测试中");
                                return false;
                            }
                        });
                        /*禁止点击未开启功能  end*/
                    });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat;background-position:1px;border:1px solid rgb(230,166,166);border-left: none;"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


		<div class="sf-content mar16">
			<p class="details-color"></p>
			<div class="sf-width">
				<div class="W1224 goverment-commissions magtop26">
		            <h3><span></span>所在地区</h3>
		            <div class="goverment-text cursor">
		            	<?php $_parentid = 1;$_siteid = $site['siteid'];$_count = 99;$_order = '`listorder` ASC';$_where = [];$_where['parentid'] = $_parentid;$_where['display'] = 1;$_ress = D('Admin/Linkage')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :?><span class="goverment-tex" onclick="shen_fp(this,'<?php echo ($row['linkageid']); ?>')"><?php echo ($row['name']); ?></span><?php endforeach; ?>
		            </div>
		        </div>
		        <div class="gs"></div>
				<div>				
					<span class="sf-title floatleft">四川省扶贫开发局</span>
					<div class="clear"></div>
				</div>
				<img src="/Resources/Skin/2019/Image/fpkfj1.gif" alt="各省份扶贫开发局" class="gs-imgs" /> 
				<div class="sf-info">
					<p><span class="span-title"></span><span class="js-title">介绍</span><span class="span-lines"></span></p>
				</div>
				<p class="js-words j">根据省政府办公厅《关于印发四川省扶贫和移民工作局主要职责内设机构和人员编制规定的通知》(川办发〔2010〕52号)规定，主要职责为：贯彻执行国家扶贫开发和大中型水利水电工程移民迁建安置、后期扶持的方针、政策和法律、法规，拟订有关具体政策措施并组织实施；拟订全省扶贫开发中长期规划和年度计划，组织、协调和指导全省产业扶贫、科技扶贫、特殊类区扶贫开发工作，组织和指导社会扶贫工作，负责联络省外机构或组织在川定点扶贫与扶贫协作工作，组织扶贫开发对外交流与合作及外资外援扶贫项目的引进与实施；承担扶贫、移民资金管理责任。</p>
				<p class="js-words s">四川省扶贫开发局设14个处室、4个中心：</p>
				<p class="js-words d">办公室、政策法规处(行政审批处)、规划处、计划财务处、审计稽察处、信息统计监测处、扶贫开发指导处（省革命老区建设办公室）、督查考核处、国际合作与社会扶贫处、移民安置处、移民后扶处、信访处、 人事处、机关党委、项目中心、发展中心、  宣传信息中心、移民工程开发中心。</p>
				<div class="zy-word">
					<div class="floatleft">
						<img src="/Resources/Skin/2019/Image/fpkfj2.gif"/>
					</div>
					<div class="floatright">
						<p><span class="span-title"></span><span class="js-title">要闻</span><span class="span-lines" style="width: 400px;"></span></p>
						<ul class="yw-new">
							<li><a href="javascript:;">四川首单央企扶贫基金落地</a></li>
							<li><a href="javascript:;">编程、竹编、刺绣、美发......残疾人职业技能竞赛竟然这么牛！</a></li>
							<li><a href="javascript:;">“四川扶贫’集体商标产品亮相南昌</a></li>
							<li><a href="javascript:;">国家提前下达我省2019年财政预算内以工代赈计划6.032亿元</a></li>
							<li><a href="javascript:;">唱响脱贫攻坚“四川声音”我省扶贫宣传排名全国第三</a></li>
							<li><a href="javascript:;">双流园建巴塘易地扶贫搬迁项目入住392名贫困群众搬进桃花...</a></li>
							<li><a href="javascript:;">人民日报：德阳罗江民企进乡村促振兴</a></li>
							<li><a href="javascript:;">电子科大与凉山木里共建扶贫项目实现精准扶贫</a></li>
							<li><a href="javascript:;">计划总投资超70亿元扶贫 浙江衢州援川协作项目在蓉签约</a></li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
				<div class="sf-info zcjd">
					<p><span class="span-title"></span><span class="js-title">政策解读</span><span class="span-lines"  style="width: 925px;"></span></p>
					<div class="square-line martop4">
						<div class="commissions-list-img floatleft">
	                        <img src="./img/zw-list.gif" alt="">
	                    </div>
	                    <div class="commissions-list-text floatright" style="width: 720px;">
	                        <h5>2018年“创响中国”系列活动总结暨成果展示在京举<span>2018\01\24</span></h5>
	                        <p>1月24日，2017年“创响中国”系列活动总结暨成果展示在京举办，国家发展改革委高技术司、中国科协学会学术部、中国科协企业工作办公室相关负责同志，部分推进大众创业万众创新部际联席会议成员单位相关同志，5月9日下午，受何立峰主任委托，胡祖才副主任主持召开我委“十三五”规划实施中期评估工作领导小组及起草小组第一次全体会议。</p>
	                        <p>
	                            <a href="./governmentDetail.html">
	                                &lt;详情&gt;
	                            </a>
	                        </p>
	                    </div>
	                    <div class="clear"></div>
					</div>
					<div class="square-line martop4">
						<div class="commissions-list-img floatleft">
	                        <img src="./img/zw-list.gif" alt="">
	                    </div>
	                    <div class="commissions-list-text floatright" style="width: 720px;">
	                        <h5>2018年“创响中国”系列活动总结暨成果展示在京举<span>2018\01\24</span></h5>
	                        <p>1月24日，2017年“创响中国”系列活动总结暨成果展示在京举办，国家发展改革委高技术司、中国科协学会学术部、中国科协企业工作办公室相关负责同志，部分推进大众创业万众创新部际联席会议成员单位相关同志，5月9日下午，受何立峰主任委托，胡祖才副主任主持召开我委“十三五”规划实施中期评估工作领导小组及起草小组第一次全体会议。</p>
	                        <p>
	                            <a href="./governmentDetail.html">
	                                &lt;详情&gt;
	                            </a>
	                        </p>
	                    </div>
	                    <div class="clear"></div>
					</div>
					<div class="square-line martop4">
						<div class="commissions-list-img floatleft">
	                        <img src="./img/zw-list.gif" alt="">
	                    </div>
	                    <div class="commissions-list-text floatright" style="width: 720px;">
	                        <h5>2018年“创响中国”系列活动总结暨成果展示在京举<span>2018\01\24</span></h5>
	                        <p>1月24日，2017年“创响中国”系列活动总结暨成果展示在京举办，国家发展改革委高技术司、中国科协学会学术部、中国科协企业工作办公室相关负责同志，部分推进大众创业万众创新部际联席会议成员单位相关同志，5月9日下午，受何立峰主任委托，胡祖才副主任主持召开我委“十三五”规划实施中期评估工作领导小组及起草小组第一次全体会议。</p>
	                        <p>
	                            <a href="./governmentDetail.html">
	                                &lt;详情&gt;
	                            </a>
	                        </p>
	                    </div>
	                    <div class="clear"></div>
					</div>
					<div class="padbottom	">
						<a class="into-btn">进入四川扶贫开发局官网查看更多</a>
					</div>
				</div>
			</div>
			
		</div>
		<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

		<script>
			var table='v1_tzzg_fpkf';
			var address = '24';
			$(function () {
				listan1();
			});
			function shen_fp(m,n)
			{
				address = n;
				$(m).addClass("red").siblings().removeClass("red");
				listan1();
			}
			//获取项目案例展示
			function listan1()
			{
				var data = {
					'province_id':address,
					'table':table,
				};
				var content='';
				var html='<p><span class="span-title"></span><span class="js-title">政策解读</span><span class="span-lines"  style="width: 925px;"></span></p>';
				$.ajax({
			        url:'/index.php?m=home&c=Ajax&a=fpkfj',
			        method:'get',
			        data:data,
			        success:function (res){
			        	//console.log(res)
			        	if(res['yw'].length>0){
			        		for(i = 0; i< res['yw'].length; i++){
				        	//alert(res[i].catid)
					        	content += `<li><a href="/index.php?m=home&c=index&a=shows&catid=`+res['yw'][i].catid+`&id=`+res['yw'][i].mid+`">`+res['yw'][i].title+`</a></li>`;
						    }
			        	}else{
			        		content += '<li><a href="javascript:;">无数据</a></li>';
			        	}
				        if(res['zcjd'].length>0){
							for(i = 0; i< res['zcjd'].length; i++){
								//thumb
								html += '<div class="square-line martop4"><div class="commissions-list-img floatleft"><img src="'+res['zcjd'][i].thumb+'" alt=""></div><div class="commissions-list-text floatright" style="width: 720px;"><h5>'+res['zcjd'][i].title+'<span>'+res['zcjd'][i].inputtime+'</span></h5><p>'+res['zcjd'][i].description+`</p><p><a href="/index.php?m=home&c=index&a=shows&catid=`+res['zcjd'][i].catid+`&id=`+res['zcjd'][i].mid+`">&lt;详情&gt;</a></p></div><div class="clear"></div></div>`;
							}
				        }else{
				        	html += '<div class="square-line martop4">无数据</div>';
				        }
				        html += '<div class="padbottom	"><a class="into-btn" href="http://fpkfj.sc.gov.cn/">进入四川扶贫开发局官网查看更多</a></div>';
					    $(".yw-new").html(content);
					    if(res['js']){
					    	$(".j").html(res['js']['content_js']);
					    	$(".s").html(res['js']['form']);
					    	$(".d").html(res['js']['form_info']);
					    }else{
					    	$(".j").html('无数据');
					    	$(".s").html('无数据');
					    	$(".d").html('无数据');
					    }
					    $(".zcjd").html(html);
			        }
			    });
			}
		</script>
	</body>
</html>