<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="author" content="$Author$" />
<meta name="copyright" content="WWW.MjAPP-WORKS.COM" />
<title><?php echo ($seo["title"]); ?></title>
<meta name="keywords" content="<?php echo ($seo["keywords"]); ?>" />
<meta name="description" content="<?php echo ($seo["description"]); ?>" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/css/swiper.min.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/layout.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css">
<link rel="stylesheet" href="/Resources/Skin/2019/css/index.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/header.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/footer.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Skin/2019/css/activity.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/font/iconfont.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.0/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.2/js/swiper.min.js"></script>
</head>
<body>
<div class="header">
    <div class="login-head">
        <div class="head-content">
            <div class="head-phone floatleft">
                <p class="content-phone">四川省&nbsp;&nbsp;<span class="content-phone">[切换地区]</span></p>
            </div>
            <div class="floatright"><img src="/Resources/Skin/2019/image/use.png" alt="用户名" class="user-img padright5" /> <span class="log-word" onclick="logWord()">登录</span> <span class="log-word">｜</span> <span class="log-word"><a href="./grzc.html" style="color: #666;">注册</a></span> <span class="log-word">｜</span> <span class="log-word"><a href="javascript:;" style="color: #666;">新手指导</a></span> <img src="/Resources/Skin/2019/image/weixin.png" alt="微信" class="user-img padleft5" /> <img src="/Resources/Skin/2019/image/weibo.png" alt="微博" class="user-img" /> </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="title">
        <div class="nav-title"><img src="<?php echo ($site["logo"]); ?>" alt="<?php echo ($site["title"]); ?>" class="floatleft title-img" />
            <div class="title-padding floatleft nav-list">
                <ul>
                    <?php $_siteid = $site['siteid'];$_navigationid = 1033;$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $row) :$row['style'] = unserialize($row['style']);?><li >
                            <a  naviid="<?php echo ($row['navigationid']); ?>" onClick="forbidden(this,'<?php echo ($row["linkurl"]); ?>')" class="wordcolor first" ><?php echo ($row['name']); ?></a>
                            <!--style="font-weight:<?php echo ($row['style']['bold']); ?>; color:<?php echo ($row['style']['color']); ?>;"-->
                            <!--<i class="icon iconfont icon-zhixiangliebiao1 "></i>-->
                            <!--href="<?php echo ($row['linkurl']); ?>"-->
                            <!--target="_blank"-->
                            <ul class="hoverlist">
                            <?php $_siteid = $site['siteid'];$_navigationid = $row['navigationid'];$_count = 10;$_order = '`listorder` ASC, `navigationid` DESC';$_where = [];$_where['parentid'] = $_navigationid;$_where['display'] = 1;$_ress = D('Admin/Navigation')->where($_where)->limit('0,'.$_count)->order($_order)->select();foreach ($_ress as $key => $val) :$val['style'] = unserialize($val['style']);?><li><a target="_blank" naviid="<?php echo ($val['navigationid']); ?>" href="<?php echo ($val['linkurl']); ?>"  ><?php echo ($val['name']); ?></a></li><?php endforeach; ?>
                            </ul>
                        </li><?php endforeach; ?>
                </ul>
                <script language="javascript" type="text/javascript">
                    function forbidden(event,url){
                        var l = $(event).next("ul").find("li").length;
                        if(url == ""){
                            return false;
                        }else{
                            window.open(url);
                        }
                    }

                    $(function() {
                        var navi = $(".nav-list>ul>li");
                        navi.each(function() {
                            if ($(this).find("ul>li").length) {
                            } else {
                                $(this).find("i, ul").hide();
                            }
                        });
                        navi.find("a").each(function(){
                            if($(this).attr('naviid') == 1034){
                                $(this).removeClass("wordcolor");
                            }
                        });

                        /*禁止点击未开启功能  start*/
                        $(".nav-list>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            if(id == 1060 || id == 1060){
                                alert("正在测试中");
                                return false;
                            }
                        });

                        $(".nav-list>ul>li>ul>li>a").click(function(){
                            var id = $(this).attr("naviid");
                            console.log(id);
                            if(id == 1072 || id == 1080 || id == 1081 || id == 1073  || id == 1074 || id == 1075 || id == 1076 || id == 1077){
                                alert("正在测试中");
                                return false;
                            }
                        });
                        /*禁止点击未开启功能  end*/
                    });
                </script>
            </div>
            <div class="floatright" style="margin-top: 50px;">
                <input type="text" name="" placeholder="输入关键词,搜索......" class="search-input" />
                <a href="javascript:;" class="search-an" style="background:url(/Resources/Skin/2019/image/searimg.gif) no-repeat;background-position:1px;border:1px solid rgb(230,166,166);border-left: none;"></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<link rel="stylesheet" href="/Resources/Skin/2019/css/reset.css" />
<link rel="stylesheet" href="/Resources/Skin/2019/css/activity.css" />

    <div class="goverment-top mar16"></div>
    <div class="W1224 newsdetail-content">
        <div class="header-lujin ">
            <p><i class="icon iconfont icon-sousuo1"></i><?php echo ($content["title"]); ?></p>
        </div>
        <div class="content-top">
            <h2 class="content-title" style="font-size:26px;"><?php echo ($content["title"]); if($content["subtitle"] != ''): ?><br /><span style="font-size:16px; letter-spacing:0px;"><?php echo ($content["subtitle"]); ?></span><?php endif; ?></h2>
            <p class="contenr-date">发布时间：<?php echo (date("Y-m-d",$content['inputtime'])); ?></p>
        </div>
        <div class="goverment-content">
            <?php echo ($content["content"]); ?>
            <?php if($content["copyfrom"] != ''): ?><span style="color:#9d9d9d;">
                    <br>
                    (转载自 : <?php echo ($content["copyfrom"]); ?>)
                </span><?php endif; ?>
        </div>
        <p class="content-tiaozhuan">
            <a href="<?php if($privs["mid"] != 0): ?>/index.php?m=home&c=index&a=shows&catid=<?php echo ($catid); ?>&id=<?php echo ($privs["mid"]); else: ?>javascript:void(0);<?php endif; ?>">上一篇：<?php echo ((isset($privs["title"]) && ($privs["title"] !== ""))?($privs["title"]):'没有数据！'); ?></a>
            <a href="<?php if($nexts["mid"] != 0): ?>/index.php?m=home&c=index&a=shows&catid=<?php echo ($catid); ?>&id=<?php echo ($nexts["mid"]); else: ?>javascript:void(0);<?php endif; ?>">下一篇：<?php echo ((isset($nexts["title"]) && ($nexts["title"] !== ""))?($nexts["title"]):'没有数据！'); ?></a>
        </p>
    </div>

<div class="footer">
    <div class="con-foot">
        <div class="foot-contact">
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot1.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">010-57372480</p>
                    <p class="phone-word">免费服务热线</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot2.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">13910469179</p>
                    <p class="phone-word">联系电话</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot4.gif" alt="服务时间" /> </dt>
                <dd>
                    <p class="phone-num">09:00-18:00</p>
                    <p class="phone-word">服务时间</p>
                </dd>
            </dl>
            <dl class="foot-phone floatleft">
                <dt class="floatleft imgright"> <img src="/Resources/Skin/2019/image/foot3.gif" alt="联系电话" /> </dt>
                <dd>
                    <p class="phone-num">zgfptzzg@163.com</p>
                    <p class="phone-word">邮箱</p>
                </dd>
            </dl>
            <div class="clear"></div>
        </div>
    </div>
    <div class="contact-link">
        <div class="foot-link">
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">平台保障</a></p>
                    <p class="foot-word"><a href="javascript:;">会员身份实名认证</a></p>
                    <p class="foot-word"><a href="javascript:;">举报投诉违规处罚</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">新手指导</a></p>
                    <p class="foot-word"><a href="javascript:;">免费注册生成名片</a></p>
                    <p class="foot-word"><a href="javascript:;">免费发布投融信息</a></p>
                    <p class="foot-word"><a href="javascript:;">常见问题解答</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">投融指导</a></p>
                    <p class="foot-word"><a href="javascript:;">项目资源</a></p>
                    <p class="foot-word"><a href="javascript:;">委托刷新</a></p>
                    <p class="foot-word"><a href="javascript:;">投递项目</a></p>
                </div>
            </div>
            <div class="foot-info floatleft">
                <div class="jz">
                    <p class="foot-title"><a href="javascript:;">关于投资中国</a></p>
                    <p class="foot-word"><a href="javascript:;">投资中国介绍</a></p>
                    <p class="foot-word"><a href="javascript:;">联系我们</a></p>
                    <p class="foot-word"><a href="javascript:;">网站公告</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="mar3">
        <p class="text-center bah">备案/许可证编号为：京ICP备18055024号。</p>
    </div>
</div>
<div id="loginPopup" class="login-popup">
    <div class="popup">
        <div class="popup-top">登录<i onclick="popupOff()" class="icon iconfont icon-error"></i></div>
        <div>
            <p class="popup-input">&emsp;单位名称
                <input type="text">
            </p>
            <p class="popup-input">&emsp;手机号码
                <input type="text">
            </p>
            <p class="popup-input">手机验证码
                <input type="text">
                <span class="yanzheng">获取短信验证码</span></p>
            <p class="popup-input">&emsp;&emsp;&emsp;密码
                <input type="text">
            </p>
            <p class="popup-txt">6-20位字符，由数字和字母共同组成<span>
                <label>
                    <input type="checkbox" />
                    显示字符</label>
                </span></p>
            <p class="popup-btn" onclick="popupOff()">完成</p>
            <p class="popup-pwd"><a href="">忘记密码</a> </p>
        </div>
    </div>
</div>

</body>
</html>