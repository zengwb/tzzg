<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: LoginIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>管理登录-您需要登录后才能使用本系统！</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />

</head>
<body>
<div class="login-layout">
    <div class="header">
        <h5><?php echo C('CMS_LICENSE');?></h5>
        <h2>管理登录</h2>
        <h6>招商<em></em>投资<em></em>微信<em></em>APP</h6>
    </div>
    <form id="frmLogin" name="frmLogin" action="<?php echo U('check');?>" method="post">
    <div class="form-group">
        <label for="username"><i class="iconfont icon-yonghudenglu"></i></label>
        <input  id="username" type="text" name="data[username]" class="form-control" value="<?php echo ($data["username"]); ?>" maxlength="16" placeholder="您的登录帐号" required autofocus autocomplete="off" tabindex="1" />
    </div>
    <div class="form-group">
        <label for="password"><i class="iconfont icon-mimadenglu"></i></label>
        <input  id="password" type="password" name="data[password]" class="form-control" value="<?php echo ($data["password"]); ?>" maxlength="16" placeholder="您的登录密码" required autocomplete="off" tabindex="2" />
    </div>
    <div class="avatar">
        <img src="/Resources/Apps/Skin/Temp/Admin/login.jpg" alt="MjCMS" />
    </div>
    <div class="form-group">
        <label for="verify"><i class="iconfont icon-yanzhengmadenglu"></i></label>
        <input  id="verify" type="text" name="data[verify]" class="form-control" value="" maxlength="4" placeholder="验证码" required autocomplete="off" tabindex="3" />
        <img id="code" src="<?php echo U('verify');?>" class="code" onClick="this.src='<?php echo U('verify', ['random' => '']);?>' + Math.random()" alt="验证码" title="看不清楚，换一张！" />
        <input type="submit" name="dosubmit" class="btn btn-success" value="登录" tabindex="4" />
    </div>
    </form>
    <div class="footer">
        COPYRIGHT &copy; 2017-<?php echo date('Y', time()); ?> <a href="javascript:void(0);">北京英煌科技有限公司</a> ALL RIGHTS RESERVED.
    </div>
</div>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/jquery.backstretch.min.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>
<script type="text/javascript" language="javascript">
<!--
$(document).ready(function() {
	$.backstretch(
        [
            "/Resources/Apps/Skin/Image/Admin/Login/bg1.jpg",
            "/Resources/Apps/Skin/Image/Admin/Login/bg2.jpg",
            "/Resources/Apps/Skin/Image/Admin/Login/bg3.jpg",
            "/Resources/Apps/Skin/Image/Admin/Login/bg4.jpg",
            "/Resources/Apps/Skin/Image/Admin/Login/bg5.jpg"
            
        ], { fade: 1000, duration: 3000 } // 背景图片轮换
    );
    
    $("#verify").focus(function() { $("#code").fadeIn(300); });
});
-->
</script>
</body>
</html>