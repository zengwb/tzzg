<?php if (!defined('THINK_PATH')) exit();?>    <tr>
        <th>取值范围<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="minnumber" class="sr-only">最小数值</label>
            <input id="minnumber" type="text" name="setting[minnumber]" class="form-control input-sm"
                value="<?php echo ((isset($setting["minnumber"]) && ($setting["minnumber"] !== ""))?($setting["minnumber"]):'0'); ?>" placeholder="请输入最小数值" size="8" style="float:left; margin-right:8px; width:140px;" required />
            ~
            <label for="maxnumber" class="sr-only">最大数值</label>
            <input id="maxnumber" type="text" name="setting[maxnumber]" class="form-control input-sm"
                value="<?php echo ((isset($setting["maxnumber"]) && ($setting["maxnumber"] !== ""))?($setting["maxnumber"]):'99999'); ?>" placeholder="请输入最大数值" size="8" style="float:right; width:180px;" required />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>小数位数<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdecimaldigits" class="sr-only">小数位数</label>
            <select id="frmdecimaldigits" class="form-control input-sm cms-fleft" name="setting[decimaldigits]">
                <option value="-1" <?php if($setting['decimaldigits'] == -1): ?>selected<?php endif; ?>>自动</option>
                <option value="0" <?php if($setting['decimaldigits'] == 0): ?>selected<?php endif; ?>>0</option>
                <option value="1" <?php if($setting['decimaldigits'] == 1): ?>selected<?php endif; ?>>1</option>
                <option value="2" <?php if($setting['decimaldigits'] == 2): ?>selected<?php endif; ?>>2</option>
                <option value="3" <?php if($setting['decimaldigits'] == 3): ?>selected<?php endif; ?>>3</option>
                <option value="4" <?php if($setting['decimaldigits'] == 4): ?>selected<?php endif; ?>>4</option>
                <option value="5" <?php if($setting['decimaldigits'] == 5): ?>selected<?php endif; ?>>5</option>
            </select>
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>输入框长度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmsize" class="sr-only">文本框长度</label>
            <input id="frmsize" type="text" name="setting[size]" class="form-control input-sm"
                value="<?php echo ($setting["size"]); ?>" placeholder="请输入元素长度" size="3" />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>默认数值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认数值</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm"
                value="<?php echo ($setting["default"]); ?>" placeholder="请输入默认数值" size="40" />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>随机数值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="minrandom" class="sr-only">最小数值</label>
            <input id="minrandom" type="text" name="setting[minrandom]" class="form-control input-sm"
                value="<?php echo ((isset($setting["minrandom"]) && ($setting["minrandom"] !== ""))?($setting["minrandom"]):'0'); ?>" placeholder="请输入最小数值" size="8" style="float:left; margin-right:8px; width:140px;" required />
            ~
            <label for="maxrandom" class="sr-only">最大数值</label>
            <input id="maxrandom" type="text" name="setting[maxrandom]" class="form-control input-sm"
                value="<?php echo ((isset($setting["maxrandom"]) && ($setting["maxrandom"] !== ""))?($setting["maxrandom"]):'0'); ?>" placeholder="请输入最大数值" size="8" style="float:right; width:180px;" />
        </td>
        <td class="tips"></td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="double" />