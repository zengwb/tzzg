<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: InstitutionActionView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>单位操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="popup">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="info[linkageid]" value="<?php echo ($info["linkageid"]); ?>" />
<input type="hidden" name="info[unitid]" value="<?php echo ($info["unitid"]); ?>" />
<input type="hidden" name="info[name]" value="<?php echo ($data["name"]); ?>" />
<div class="popup-table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped popup-table-body">
    <tbody>
    <tr>
        <th>单位隶属<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="parentid" class="sr-only">单位隶属</label>
            <select id="parentid" class="form-control input-sm" name="data[parentid]">
                <option value="<?php echo ($info["parentid"]); ?>">指定为顶级单位</option>
                <?php echo ($tree); ?>
            </select>
        </td>
        <td class="tips">选择顶级单位！</td>
    </tr>
    <tr>
        <th>单位名称<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="name" class="sr-only">单位名称</label>
            <input id="name" type="text" name="data[name]" class="form-control input-sm" value="<?php echo ($data["name"]); ?>" placeholder="请输入菜单名称" required autofocus />
        </td>
        <td class="tips">填写单位名称！</td>
    </tr>
    <tr>
        <th>单位描述<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="description" class="sr-only">单位描述</label>
            <textarea id="description" name="data[description]" class="form-control input-sm" placeholder="请输入单位描述" rows="3"><?php echo ($data["description"]); ?></textarea>
        </td>
        <td class="tips">填写单位描述！</td>
    </tr>
    <!--<tr>
        <th>单位介绍<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="abouts" class="sr-only">单位介绍</label>
            <textarea id="abouts" name="data[abouts]" class="form-control input-94" placeholder="请输入单位介绍" rows="3"><?php echo ($data["abouts"]); ?></textarea>
        </td>
        <td class="tips">填写单位介绍！</td>
    </tr>-->
    </tbody>
</table>
</div>
<input id="dosubmit" type="submit" name="dosubmit" value="提交" class="popup-submit-dialog" />
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {});
-->
</script>
</body>
</html>