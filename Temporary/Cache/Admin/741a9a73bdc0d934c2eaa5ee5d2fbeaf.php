<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: PrivUserMenuView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>菜单管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="popup popup-lists">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="info[userid]" value="<?php echo ($info["userid"]); ?>" />
<div class="popup-table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th>栏目名称（导航）</th>
        <th style="width:80px; text-align:center;">栏目类型</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="6">暂无内容！</td></tr><?php endif; ?>
<?php echo ($tree); ?>
    </tbody>
</table>
</div>
<input id="dosubmit" type="submit" name="dosubmit" value="提交" class="popup-submit-dialog" />
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        });
    yhcms.common.dosubmit().checkall();
});
-->
</script>
</body>
</html>