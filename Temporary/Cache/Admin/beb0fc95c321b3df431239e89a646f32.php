<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: TemplateIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>模板管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type='hidden' name='info[dirname]' value='<?php echo ($dirname); ?>' />
<div class="list-tips">
    <a href="javascript:void(0);" onClick="yhcms.common.linkurl('<?php echo U('index');?>');" role="button" class="btn btn-default btn-sm">模板主题</a>
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">模板管理</a>
    <h3 class="btn btn-sm tips-head">您可以对【<?php echo ($themename); ?>】进行管理，如更新模板操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th style="width:180px;">模板目录（文件）</th>
        <th>模板描述</th>
        <th class="cms-tc" style="width:72px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$lists): ?><tr><td colspan="5">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($lists)): $i = 0; $__LIST__ = $lists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th class="list-checkbox">
            <input class='checkchild' type='checkbox' name='' value='' disabled />
        </th>
        <td class="list-small"><?php echo ($key); ?></td>
        <td data-dirs="<?php echo ($row["dirs"]); ?>">
            <?php if($row['dirs'] == '..'): ?><a href="<?php echo U('template', ['dirname' => $row['parent']]);?>" title="返回上级目录"><strong><?php echo ($row["dirs"]); ?></strong></a>
            <?php else: ?>
            <?php if(is_file($row['isfile'])): ?><a href="<?php echo U('edit', ['template' => $row['dirname']]);?>"><?php echo ($row["dirs"]); ?></a>
            <?php else: ?>
            <a href="<?php echo U('template', ['dirname' => $row['dirname']]);?>" title="进入模板列表"><?php echo ($row["dirs"]); ?></a><?php endif; endif; ?>
        </td>
        <td>
            <?php if($row['dirs'] == '..'): ?><a href="<?php echo U('template', ['dirname' => $row['parent']]);?>" title="返回上级目录"><?php echo ($row["name"]); ?></a>
            <?php else: ?>
            <input type='hidden' name='data[dirs][]' value='<?php echo ($row["dirs"]); ?>' />
            <input type="text" name="data[name][]" value="<?php echo ($row["name"]); ?>" class="form-control input-sm" style="margin-top:1px; padding:0px 8px; width:240px; height:24px;" tabindex="<?php echo ($key); ?>" /><?php endif; ?>
        </td>
        <td class="cms-tc">
             <?php if(is_file($row['isfile'])): ?><a href="javascript:void(0);" onClick="yhcms.common.linkurl('<?php echo U('edit', ['template' => $row['dirname']]);?>');" title="编辑模板">编辑</a>
            <?php else: ?>
            <a href="javascript:void(0);" class="cms-cccc">编辑</a><?php endif; ?>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="javascript:history.go(-1);" class="btn btn-default btn-sm">返回上级</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('update');?>', '确认更新模板描述！');" class="btn btn-danger btn-sm">更新描述</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.dblclick(function() {
            var href = $(this).find("td:eq(1)>a").attr("href");
            if (href) window.location.href = href;
        });
    
    yhcms.common.dosubmit();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>