<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: IndexBootstrapView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>欢迎光临！</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<div class="list-tips">
    <h3 class="btn btn-sm cms-desc">
    欢迎使用：<?php echo C('CMS_CMSNAME');?>
    </h3>
    <hr />
</div>
<h3 class="btn btn-sm cms-desc" style="height:auto; text-align:left;">
    当前站点：<a href="<?php echo ($site["domain"]); ?>" target="_blank"><?php echo ($site["name"]); ?></a>（<?php echo ($site["title"]); ?>）<br />
    访问域名：<a href="<?php echo ($site["domain"]); ?>" target="_blank"><?php echo ($site["domain"]); ?></a><br />
    主题模板：<?php echo ($site["theme"]); ?>（<span id="theme"></span>）<br />
    授权使用：<?php echo C('CMS_LICENSE');?>（编号：<?php echo C('CMS_CMSCODE');?>）<br />
    <hr style="border-width:1px;" />
    当前版本：<?php echo C('CMS_VERSION');?>.<?php echo C('CMS_COMPILE');?>_<?php echo C('CMS_RELEASE');?>_<?php echo C('CMS_CHARSET');?><br />
    计算机软件著作权登记号：<br />
    授权类型：未授权（<a href="javascript:void(0);">点击购买</a>）<br />
    运行环境：<?php echo $_SERVER["OS"]; ?> <?php echo strpos($_SERVER['SERVER_SOFTWARE'], 'PHP') === false ? $_SERVER['SERVER_SOFTWARE'] . ' PHP/' . phpversion() : $_SERVER['SERVER_SOFTWARE']; ?> MySQL<?php echo mysql_get_server_info(); ?><br />
    <hr style="border-width:1px;" />
    版权所有：<a href="http://www.mjapp-works.com/" target="_blank">成都木匠作坊科技有限公司</a><br />
    官方网站：<a href="http://www.mjcms.com.cn/" target="_blank">http://www.mjcms.com.cn/</a><br />
    技术咨询：QQ:75295008　Email:zengwb100@126.com
</h3>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var array = new Array();
        array[0] = "<?php echo ($site["theme"]); ?>";
        
    $.getJSON("<?php echo U('Admin/Site/skin');?>", {array: array}, function(data) {
        $.each(data, function(i, row) {
            $("#theme").append(row.name);
        });
    });
});
-->
</script>
</body>
</html>