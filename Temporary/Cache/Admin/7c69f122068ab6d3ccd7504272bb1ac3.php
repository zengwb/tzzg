<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: FieldActionView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>字段操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="info[fieldid]" value="<?php echo ($data["fieldid"]); ?>" />
<input type="hidden" name="info[modelid]" value="<?php echo ($data["modelid"]); ?>" />
<input type="hidden" name="info[field]" value="<?php echo ($data["field"]); ?>" />
<input type="hidden" name="info[alias]" value="<?php echo ($data["alias"]); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index', ['modelid' => $modelid]);?>');" class="btn btn-default btn-sm">字段管理</a>
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm"><?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>字段</a>
    <h3 class="btn btn-sm tips-head">您可以对【<?php echo ($modelname); ?>】字段管理，如增/删/改/查，显示排序、设置状态等操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="oper-title"><strong>基础信息</strong>（请填写字段的基本信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">字段类型<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:372px;">
            <label for="formtype" class="sr-only">字段类型</label>
            <select id="formtype" class="form-control input-sm cms-fleft" name="data[formtype]" required>
                <option value="">请选择字段类型</option>
                <?php if(is_array($forms)): $i = 0; $__LIST__ = $forms;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($row["elements"]); ?>" <?php if($row['elements'] == $data['formtype']): ?>selected<?php endif; ?>><?php echo ($row["alias"]); ?>（<?php echo ($row["elements"]); ?>）</option><?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>字段名称<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="field" class="sr-only">模型名称</label>
            <input id="field" type="text" name="data[field]" class="form-control input-sm" value="<?php echo ($data["field"]); ?>" placeholder="请输入字段名称" required autofocus />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>字段别名<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="alias" class="sr-only">字段别名</label>
            <input id="alias" type="text" name="data[alias]" class="form-control input-sm" value="<?php echo ($data["alias"]); ?>" placeholder="请输入字段别名" required />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>字段属性<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="checkbox input-sm">
                <label>
                    <input type="checkbox" name="data[issystem]" value="<?php echo ($data["issystem"]); ?>" <?php if($data['issystem'] == 1): ?>checked<?php endif; ?> />系统字段
                </label>
                <?php if($modeldata == 1): ?><label>
                    <input type="checkbox" name="data[ismain]" value="<?php echo ($data["ismain"]); ?>" <?php if($data['ismain'] == 1): ?>checked<?php endif; ?> <?php if(ACTION_NAME == 'edit'): ?>disabled<?php endif; ?> />主表字段
                    <?php if(ACTION_NAME == 'edit'): ?><input type="hidden" name="data[ismain]" value="<?php echo ($data["ismain"]); ?>" /><?php endif; ?>
                </label>
                <?php else: ?>
                <label>
                    <input type="hidden" name="data[ismain]" value="<?php echo ((isset($data["ismain"]) && ($data["ismain"] !== ""))?($data["ismain"]):1); ?>" />
                </label><?php endif; ?>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>其他属性<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="checkbox input-sm">
                <label>
                    <input type="checkbox" name="data[isbase]" value="<?php echo ((isset($data["isbase"]) && ($data["isbase"] !== ""))?($data["isbase"]):'1'); ?>" <?php if($data['isbase'] == 1): ?>checked<?php endif; ?> />基本字段
                </label>
                <label>
                    <input type="checkbox" name="data[isposition]" value="<?php echo ((isset($data["isposition"]) && ($data["isposition"] !== ""))?($data["isposition"]):'1'); ?>" <?php if($data['isposition'] == 1): ?>checked<?php endif; ?> />推荐标签
                </label>
                <label>
                    <input type="checkbox" name="data[issearch]" value="<?php echo ((isset($data["issearch"]) && ($data["issearch"] !== ""))?($data["issearch"]):'1'); ?>" <?php if($data['issearch'] == 1): ?>checked<?php endif; ?> />启用搜索
                </label>
                <label>
                    <input type="checkbox" name="data[display]" value="<?php echo ((isset($data["display"]) && ($data["display"] !== ""))?($data["display"]):'0'); ?>" <?php if(ACTION_NAME == 'edit' && $data['display'] == 0): ?>checked<?php endif; ?> />隐藏字段
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </if>
    </tbody>
</table>
<div class="oper-title" id="field_setting_title" style="display:none;"><strong>字段设置</strong>（请填写字段的基础设置）</div>
<table id="field_setting_body2" class="table table-condensed table-bordered cms-table-form oper-table" style="display:none;">
    <tbody>
    <tr id="fieldtype">
        <th style="width:160px;">表单元素<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:372px;">
            
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
    <tbody style="border-top:none;"></tbody>
</table>
<div class="oper-title"><strong>字段描述</strong>（请填写字段的描述信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">字段长度<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:372px;">
            <label for="minlength" class="sr-only">最小长度</label>
            <input id="minlength" type="text" name="data[minlength]" class="form-control input-sm" value="<?php echo ((isset($data["minlength"]) && ($data["minlength"] !== ""))?($data["minlength"]):'0'); ?>" placeholder="请输入最小长度" size="8" style="float:left; margin-right:8px; width:152px;" required />
            ~
            <label for="maxlength" class="sr-only">最大长度</label>
            <input id="maxlength" type="text" name="data[maxlength]" class="form-control input-sm" value="<?php echo ((isset($data["maxlength"]) && ($data["maxlength"] !== ""))?($data["maxlength"]):'0'); ?>" placeholder="请输入最大长度" size="8" style="float:right; width:180px;" required />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>字段提示<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="tips" class="sr-only">字段提示</label>
            <input id="tips" type="text" name="data[tips]" class="form-control input-sm" value="<?php echo ($data["tips"]); ?>" placeholder="请输入字段提示" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>CSS样式<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="style" class="sr-only">CSS样式</label>
            <input id="style" type="text" name="data[style]" class="form-control input-sm" value="<?php echo ($data["style"]); ?>" placeholder="请输入CSS样式" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>字段正则<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="pattern" class="sr-only">字段正则</label>
            <input id="pattern" type="text" name="data[pattern]" class="form-control input-sm" value="<?php echo ($data["pattern"]); ?>" placeholder="请输入字段正则" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>错误提示<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="errortips" class="sr-only">错误提示</label>
            <input id="errortips" type="text" name="data[errortips]" class="form-control input-sm" value="<?php echo ($data["errortips"]); ?>" placeholder="请输入错误提示" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>扩展设置</strong>（请填写字段的扩展设置）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">禁止角色<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:372px;">
            <div class="checkbox input-sm">
                <?php if(is_array($roleids)): $i = 0; $__LIST__ = $roleids;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><label>
                    <input type="checkbox" name="data[roleids][]" value="<?php echo ($row["roleid"]); ?>" <?php if(in_array($row['roleid'], $data['roleids'])): ?>checked<?php endif; ?> /><?php echo ($row["name"]); ?>
                </label><?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>禁止用户组<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="checkbox input-sm">
                <?php if(is_array($roleids)): $i = 0; $__LIST__ = $roleids;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><!--usergroupids-->
                <label>
                    <input type="checkbox" name="data[usergroupids][]" value="<?php echo ($row["roleid"]); ?>" <?php if(in_array($row['roleid'], $data['roleids'])): ?>checked<?php endif; ?> /><?php echo ($row["name"]); ?>
                </label>
                <!--<label>
                    <input type="checkbox" name="data[usergroupids][]" value="<?php echo ($row["roleid"]); ?>" <?php if(in_array($row['roleid'], $data['usergroupids'])): ?>checked<?php endif; ?> /><?php echo ($row["name"]); ?>
                </label>--><?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">字段状态<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:372px;">
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[disabled]" value="0" <?php if($data['disabled'] == 0): ?>checked<?php endif; ?> />启用字段
                </label>
                <label>
                    <input type="radio" name="data[disabled]" value="1" <?php if($data['disabled'] == 1): ?>checked<?php endif; ?> />禁用字段
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table oper-table-btns">
    <tbody>
    <tr>
        <td style="width:520px;">
            <input id="dosubmit" type="submit" name="dosubmit" value="<?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>字段" class="btn btn-danger btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <td style="width:520px;">
            <input type="button" name="doreturn" value="返回列表" onClick="javascript:history.go(-1);" class="btn btn-link btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<!--<div class="list-foot">
    <h3 class="btn btn-sm tips-head"></h3>
</div>-->
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    
    $("#field_setting_title").hide();
    $("#field_setting_body2").hide();
    
    $("#formtype").change(function() {
        if ($(this).val() == "") {
            $("#field_setting_title").hide();
            $("#field_setting_body2").hide();
            return ;
        }
        $("#field_setting_title").show();
        $("#field_setting_body2").show();
        $("#field_setting_body2>tbody>tr[id!='fieldtype']").html("");
        
        $("#field_setting_body2>tbody:eq(1)").html("");
        $.get("<?php echo U('forms');?>", {formtype: $(this).val(), fieldid: <?php echo ($data['fieldid']); ?>}, function(data) {
            $("#fieldtype>td:eq(0)").html($("#formtype>option:selected").html());
            $("#field_setting_body2>tbody:eq(1)").append(data);
        });
    });
    if ($("#formtype").val() != "") $("#formtype").trigger("change");
    
    yhcms.common.dosubmit();
});
-->
</script>
</body>
</html>