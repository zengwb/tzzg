<?php if (!defined('THINK_PATH')) exit();?>    <tr>
        <th>模块参数<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmmodule" class="sr-only">模块参数</label>
            <input id="frmmodule" type="text" name="setting[module]" class="form-control input-sm" value="<?php echo ($setting["module"]); ?>" placeholder="请输入模块参数" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模型参数<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmmodelid" class="sr-only">模型参数</label>
            <input id="frmmodelid" type="text" name="setting[modelid]" class="form-control input-sm" value="<?php echo ($setting["modelid"]); ?>" placeholder="请输入模型参数" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>栏目参数<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmcatid" class="sr-only">栏目参数</label>
            <input id="frmcatid" type="text" name="setting[catid]" class="form-control input-sm" value="<?php echo ($setting["catid"]); ?>" placeholder="请输入栏目参数" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>默认类别<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认类别</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm" value="<?php echo ($setting["default"]); ?>" placeholder="请输入默认类别" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="smallint" />
    <input type="hidden" name="setting[unsigned]" value="1" />