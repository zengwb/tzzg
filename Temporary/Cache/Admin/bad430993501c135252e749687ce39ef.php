<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: InstitutionAddAllView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>单位操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="popup">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>&linkageid=<?php echo ($data["linkageid"]); ?>&parentid=<?php echo ($data["parentid"]); ?>" enctype="multipart/form-data" method="post">
<div class="popup-table-responsive">
    <input type="file" name="photo" />
    <a style="margin-right: 20px;text-decoration: none"> <?php echo ($image); ?> </a>
    <input id="dosubmit" type="submit" name="dosubmit" value="提交" class="popup-submit-dialog" />
</div>

</form>

<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {});
-->
</script>
</body>
</html>