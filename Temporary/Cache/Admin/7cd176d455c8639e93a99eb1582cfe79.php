<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: FormsIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>表单元素</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">表单元素</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('add');?>');" class="btn btn-default btn-sm">添加元素</a>
    <h3 class="btn btn-sm tips-head">您可以对【表单元素】进行管理，如增/删/改/查，显示排序、元素配置及状态设置！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th style="width:180px;">表单元素</th>
        <th>元素描述</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:104px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="7">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" type="checkbox" name="info[elementid][]" value="<?php echo ($row["elementid"]); ?>" />
        </th>
        <td class="list-small"><?php echo ($row[elementid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[elementid][]" value="<?php echo ($row[elementid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td data-elementid='<?php echo ($row["elementid"]); ?>'><a><?php echo ($row["alias"]); ?></a>（<?php echo ($row["elements"]); ?>）</td>
        <td class="cms-c999"><?php echo ($row["description"]); ?></td>
        <td class="cms-tc icon-color">
        <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[elementid]); ?>" href="javascript:void(0);" title="点击启用元素">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[elementid]); ?>" href="javascript:void(0);" title="点击禁用元素">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('edit', ['elementid' => $row['elementid']]);?>');" title="修改元素">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('setting', ['elementid' => $row['elementid']]);?>');" title="配置元素">配置</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['elementid' => $row['elementid']]);?>', '确认删除『<?php echo ($row['alias']); ?>』表单元素！');" title="删除元素">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的元素！');" class="btn btn-danger btn-sm">删除元素</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), elementid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {elementid: elementid}, function(data) {
                if (data == 0) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击禁用元素");
                } else {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击启用元素");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var elementid = $(this).find("td:eq(2)").attr("data-elementid"),
                name = $(this).find("td:eq(1)").find("a").html(),
                url = "<?php echo U('edit', ['elementid' => '']);?>" + elementid;
            
            if (elementid) yhcms.common.linkurl(url);
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>