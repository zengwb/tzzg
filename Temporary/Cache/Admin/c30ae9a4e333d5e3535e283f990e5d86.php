<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: FilterIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>词汇管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">词汇管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('add');?>');" class="btn btn-default btn-sm">添加词汇</a>
    <h3 class="btn btn-sm tips-head">您可以对【敏感词汇】进行管理，如增/删/改/查操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">词汇搜索</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th style="width:160px;">敏感词汇</th>
        <th>替换词汇</th>
        <th class="cms-tc" style="width:80px;">级别</th>
        <th class="cms-tc" style="width:140px;">添加时间</th>
        <th class="cms-tc" style="width:104px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="7">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th class="list-checkbox">
            <input class="checkchild" name="info[keysid][]" value="<?php echo ($row[keysid]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($row[keysid]); ?></td>
        <td data-keysid='<?php echo ($row["keysid"]); ?>'><a><?php echo ($row["keyword2"]); ?></a></td>
        <td><?php echo ($row["replace"]); ?></td>
        <td class="cms-tc"><?php echo ($row["level"]); ?></td>
        <td class="cms-tc"><?php echo (date("Y-m-d H:i:s",$row["inputtime"])); ?></td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="yhcms.common.linkurl('<?php echo U('edit', ['keysid' => $row['keysid']]);?>');" title="修改词汇">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['keysid' => $row['keysid']]);?>', '确认删除【<?php echo ($row['keyword']); ?>】敏感词汇！');" title="删除词汇">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的词汇！');" class="btn btn-danger btn-sm">删除词汇</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('export');?>', '确认导出敏感词汇！');" class="btn btn-default btn-sm">导出词汇</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var keysid  = $(this).find("td:eq(1)").attr("data-keysid"),
                url     = "<?php echo U('edit', ['keysid' => '']);?>" + keysid;
            
            if (keysid) yhcms.common.linkurl(url);
        });
    
    yhcms.common.dosubmit().checkall();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>