<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: InstitutionIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>事业单位行政区域列表</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-danger btn-sm">行政区域</a>
    <h3 class="btn btn-sm tips-head">
        <?php if($info['parentid'] == 0): ?>您可以对【事业单位】进行管理！
        <?php else: ?>
        <a href="javascript:history.go(-1);" class="cms-cf30">返回上级行政区域</a>，行政区域目录：<span class="cms-c666"><?php echo ($list); ?></span><?php endif; ?>
    </h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" disabled /></th>
        <th class="list-small">ID</th>
        <th style="width:260px;">行政区域（国家部委）</th>
        <th>相关描述</th>
        <th class="cms-tc" style="width:163px;">单位管理</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="5">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th class="list-checkbox">
            <input class="checkchild" name="info[linkageid][]" value="<?php echo ($row[linkageid]); ?>" type="checkbox" disabled />
        </th>
        <td class="list-small"><?php echo ($row[linkageid]); ?></td>
        <td data-linkageid="<?php echo ($row[linkageid]); ?>" data-name="<?php echo ($row[name]); ?>">
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('index', ['parentid' => $row['linkageid']]);?>');" title="管理子项"><?php echo ($row["name"]); ?></a>
        </td>
        <td><?php echo ((isset($row[description]) && ($row[description] !== ""))?($row[description]):$row[name]); ?></td>
        </td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('<?php echo U('units', ['linkageid' => $row['linkageid']]);?>', '【<?php echo ($row['name']); ?>】单位管理', 'AdminInstitutionUnits-900-442');" title="单位管理">单位管理</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-danger btn-sm">行政区域</button>
    </div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var linkageid = $(this).find("td:eq(1)").attr("data-linkageid"),
                name = $(this).find("td:eq(1)").attr("data-name"),
                url = "<?php echo U('units', ['linkageid' => '']);?>" + linkageid;
            
            if (linkageid) {
                yhcms.dialog.window(url, '【'+name+'】单位管理', 'AdminInstitutionUnits-900-442');
            }
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>