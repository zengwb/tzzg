<?php if (!defined('THINK_PATH')) exit();?>   <tr>
        <th>文本框长度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmlength" class="sr-only">文本框长度</label>
            <input id="frmlength" type="text" name="setting[length]" class="form-control input-sm" value="<?php echo ($setting["length"]); ?>" placeholder="请输入文本框长度" autofocus />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>默认值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认值</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm" value="<?php echo ($setting["default"]); ?>" placeholder="请输入默认值" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>显示模式<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[showtype]" value="1" <?php if($setting['showtype'] == 1): ?>checked<?php endif; ?> required />图片
                </label>
                <label>
                    <input type="radio" name="setting[showtype]" value="0" <?php if($setting['showtype'] == 0): ?>checked<?php endif; ?> required />文本框
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>图片类型<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmallowext" class="sr-only">图片类型</label>
            <input id="frmallowext" type="text" name="setting[allowext]" class="form-control input-sm" value="<?php echo ((isset($setting["allowext"]) && ($setting["allowext"] !== ""))?($setting["allowext"]):'gif,jpg,jpeg,png,bmp'); ?>" placeholder="请输入图片类型" required />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>添加水印<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[watermark]" value="1" <?php if($setting['watermark'] == 1): ?>checked<?php endif; ?> required />是
                </label>
                <label>
                    <input type="radio" name="setting[watermark]" value="0" <?php if($setting['watermark'] == 0): ?>checked<?php endif; ?> required />否
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>图像尺寸<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="width" class="sr-only">图像宽度</label>
            <input id="width" type="text" name="setting[width]" class="form-control input-sm" value="<?php echo ((isset($setting["width"]) && ($setting["width"] !== ""))?($setting["width"]):'0'); ?>" placeholder="请输入图像宽度" size="8" style="float:left; margin-right:8px; width:140px;" />
            ~
            <label for="height" class="sr-only">图像高度</label>
            <input id="height" type="text" name="setting[height]" class="form-control input-sm" value="<?php echo ((isset($setting["height"]) && ($setting["height"] !== ""))?($setting["height"]):'0'); ?>" placeholder="请输入图像高度" size="8" style="float:right; width:180px;" />
        </td>
        <td class="cms-note">宽,高</td>
    </tr>
    <tr>
        <th>图库选择<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[selectimage]" value="1" <?php if($setting['selectimage'] == 1): ?>checked<?php endif; ?> required />是
                </label>
                <label>
                    <input type="radio" name="setting[selectimage]" value="0" <?php if($setting['selectimage'] == 0): ?>checked<?php endif; ?> required />否
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="varchar" />