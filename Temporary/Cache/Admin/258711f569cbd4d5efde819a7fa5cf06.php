<?php if (!defined('THINK_PATH')) exit();?>   <tr>
        <th>文本域宽度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmwidth" class="sr-only">文本域宽度</label>
            <input id="frmwidth" type="text" name="setting[width]" class="form-control input-sm" value="<?php echo ((isset($setting["width"]) && ($setting["width"] !== ""))?($setting["width"]):90); ?>" placeholder="请输入文本域宽度（%）" required autofocus />
        </td>
        <td class="tips">%</td>
    </tr>
    <tr>
        <th>文本域高度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmheight" class="sr-only">文本域高度</label>
            <input id="frmheight" type="text" name="setting[height]" class="form-control input-sm" value="<?php echo ((isset($setting["height"]) && ($setting["height"] !== ""))?($setting["height"]):48); ?>" placeholder="请输入文本域高度（PX）" required />
        </td>
        <td class="tips">px</td>
    </tr>
    <tr>
        <th>默认值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认值</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm" value="<?php echo ($setting["default"]); ?>" placeholder="请输入默认值" />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>允许HTML<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[allowhtml]" value="1" <?php if($setting['allowhtml'] == 1): ?>checked<?php endif; ?> />是
                </label>
                <label>
                    <input type="radio" name="setting[allowhtml]" value="0" <?php if($setting['allowhtml'] == 0): ?>checked<?php endif; ?> />否
                </label>
            </div>
        </td>
        <td class="tips"></td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="varchar" />