<?php if (!defined('THINK_PATH')) exit();?>    <tr>
        <th>编辑器样式<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[toolbar]" value="basic" <?php if($setting['toolbar'] == 'basic'): ?>checked<?php endif; ?> />简洁型
                </label>
                <label>
                    <input type="radio" name="setting[toolbar]" value="full" <?php if($setting['toolbar'] == 'full' || $setting['toolbar'] == ''): ?>checked<?php endif; ?> />标准型
                </label>
            </div>
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>默认值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认值</label>
            <textarea id="frmdefault" name="setting[default]" class="form-control input-sm" placeholder="请输入默认值" rows="3"><?php echo ($setting["default"]); ?></textarea>
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>默认高度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmheight" class="sr-only">默认高度</label>
            <input id="frmheight" type="text" name="setting[height]" class="form-control input-sm" value="<?php echo ((isset($setting["height"]) && ($setting["height"] !== ""))?($setting["height"]):200); ?>" placeholder="请输入默认高度（PX）" required />
        </td>
        <td class="tips">px</td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="text" />