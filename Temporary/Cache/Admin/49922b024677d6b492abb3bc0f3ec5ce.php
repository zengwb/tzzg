<?php if (!defined('THINK_PATH')) exit();?>    <tr>
        <th>接收参数<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmparam" class="sr-only">默认值</label>
            <input id="frmparam" type="text" name="setting[param]" class="form-control input-sm" value="<?php echo ($setting["param"]); ?>" placeholder="请输入接收参数" />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>默认栏目<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认栏目</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm" value="<?php echo ((isset($setting["default"]) && ($setting["default"] !== ""))?($setting["default"]):'0'); ?>" placeholder="请输入默认栏目" />
        </td>
        <td class="tips"></td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="smallint" />
    <input type="hidden" name="setting[unsigned]" value="1" />