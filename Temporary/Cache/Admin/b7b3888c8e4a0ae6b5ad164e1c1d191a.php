<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ModelActionView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>模型操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="info[table]" value="<?php echo ($data["table"]); ?>" />
<input type="hidden" name="info[name]" value="<?php echo ($data["name"]); ?>" />
<input type="hidden" name="info[modelid]" value="<?php echo ($data["modelid"]); ?>" />
<input type="hidden" name="info[module]" value="<?php echo ($data["module"]); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index', ['module' => $module]);?>');" class="btn btn-default btn-sm">模型管理</a>
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm"><?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>模型</a>
    <h3 class="btn btn-sm tips-head">您可以对【系统模型】进行管理，如增/删/改/查，显示排序、设置状态等操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="oper-title"><strong>基础信息</strong>（请填写模型的基本信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">所属模块<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="module" class="sr-only">所属模块</label>
            <select id="module" class="form-control input-sm cms-fleft" name="data[module]" required>
                <option value="">请选择所属模块</option>
                <?php if(is_array($apps)): $i = 0; $__LIST__ = $apps;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($row["module"]); ?>" <?php if($row['module'] == $data['module']): ?>selected<?php endif; ?>><?php echo ($row["name"]); ?>（<?php echo ($row["module"]); ?>）</option><?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模型名称<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="name" class="sr-only">模型名称</label>
            <input id="name" type="text" name="data[name]" class="form-control input-sm" value="<?php echo ((isset($data["name"]) && ($data["name"] !== ""))?($data["name"]):''); ?>" placeholder="请输入模型名称" required autofocus />
        </td>
        <td class="cms-note">
            <?php if(ACTION_NAME == 'add'): ?><a id="importmodel" href="javascript:void(0);">点击这里</a> <span>自动导入系统模型？</span><?php endif; ?>
        </td>
    </tr>
    <tr>
        <th>模型表名<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="table" class="sr-only">模型表名</label>
            <input id="table" type="text" name="data[table]" class="form-control input-sm" value="<?php echo ($data["table"]); ?>" placeholder="请输入模型表名" required />
        </td>
        <td class="cms-note">
            <?php if(ACTION_NAME == 'add'): ?><div class="checkbox input-sm">
                <label>
                    <input type="checkbox" name="data[data]" value="1" <?php if($data['data'] == 1): ?>checked<?php endif; ?> />创建副表（数据表）
                </label>
            </div><?php endif; ?>
        </td>
    </tr>
    <tr>
        <th>模型描述<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="description" class="sr-only">模型描述</label>
            <textarea id="description" name="data[description]" class="form-control input-sm" placeholder="请输入模型描述" rows="3"><?php echo ($data["description"]); ?></textarea>
        </td>
        <td class="cms-note"></td>
    </tr>
    <?php if(ACTION_NAME == 'add'): ?><tr id="importmodeloper" style="display:none;">
        <th>导入模型<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="model" class="sr-only">导入模型</label>
            <input id="model" type="file" name="model" style="float:left;" />
        </td>
        <td class="cms-note"></td>
    </tr><?php endif; ?>
    </tbody>
</table>
<div class="oper-title"><strong>模板设置</strong>（请选择模型的视图模板）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">风格主题<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="theme" class="sr-only">风格主题</label>
            <select id="theme" class="form-control input-sm cms-fleft" name="setting[theme]" required>
                <option value="">请选择风格主题</option>
                <?php if(is_array($skin)): $i = 0; $__LIST__ = $skin;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($row["dirname"]); ?>" <?php if($row['dirname'] == $setting['theme']): ?>selected<?php endif; ?>><?php echo ($row["name"]); ?>（<?php echo ($row["dirname"]); ?>）</option><?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>频道模板<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="categorytpl" class="sr-only">频道模板</label>
            <select id="categorytpl" class="form-control input-sm cms-fleft" name="setting[categorytpl]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>列表模板<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="listtpl" class="sr-only">列表模板</label>
            <select id="listtpl" class="form-control input-sm cms-fleft" name="setting[listtpl]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>详细模板<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="showtpl" class="sr-only">详细模板</label>
            <select id="showtpl" class="form-control input-sm cms-fleft" name="setting[showtpl]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">模型状态<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[disabled]" value="0" <?php if($data['disabled'] == 0): ?>checked<?php endif; ?> />启用模型
                </label>
                <label>
                    <input type="radio" name="data[disabled]" value="1" <?php if($data['disabled'] == 1): ?>checked<?php endif; ?> />禁用模型
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table oper-table-btns">
    <tbody>
    <tr>
        <td style="width:520px;">
            <input id="dosubmit" type="submit" name="dosubmit" value="<?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>模型" class="btn btn-danger btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <td style="width:520px;">
            <input type="button" name="doreturn" value="返回列表" onClick="javascript:history.go(-1);" class="btn btn-link btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    $("#theme").attr("disabled", true);
    
    $("#module").change(function() {
        $("#theme").val("");
        $("#categorytpl").val("");
        $("#listtpl").val("");
        $("#showtpl").val("");
        
        $("#theme").attr("disabled", true);
        $("#categorytpl").attr("disabled", true);
        $("#listtpl").attr("disabled", true);
        $("#showtpl").attr("disabled", true);
        
        if ($(this).val() != "") $("#theme").removeAttr("disabled");
    });
    
    $("#theme").change(function() {
        var theme = $(this).val(),
            module= $("#module").val();
        
        if (theme == "" || module == "") {
            $("#categorytpl").attr("disabled", true);
            $("#listtpl").attr("disabled", true);
            $("#showtpl").attr("disabled", true);
        } else {
            $("#categorytpl").removeAttr("disabled");
            $("#listtpl").removeAttr("disabled");
            $("#showtpl").removeAttr("disabled");
        }
        $.getJSON("<?php echo U('template/ajax');?>", {module: module, theme: theme, prefix: 'category'}, function(data) {
            var html = "";
            $.each(data, function(i, row) {
                var selected = "";
                if (row.file == "<?php echo ($setting['categorytpl']); ?>") {
                    selected = " selected";
                }
                html += "<option value=\""+row.file+"\" "+selected+">"+row.name+"（"+row.file+"）"+"</option>"
            });
            $("#categorytpl").html(html);
            if (html) $("#categorytpl").removeAttr("disabled");
            else $("#categorytpl").attr("disabled", true);
        });
        $.getJSON("<?php echo U('template/ajax');?>", {module: module, theme: theme, prefix: 'list'}, function(data) {
            var html = "";
            $.each(data, function(i, row) {
                var selected = "";
                if (row.file == "<?php echo ($setting['listtpl']); ?>") {
                    selected = " selected";
                }
                html += "<option value=\""+row.file+"\" "+selected+">"+row.name+"（"+row.file+"）"+"</option>"
            });
            $("#listtpl").html(html);
            if (html) $("#listtpl").removeAttr("disabled");
            else $("#listtpl").attr("disabled", true);
        });
        $.getJSON("<?php echo U('template/ajax');?>", {module: module, theme: theme, prefix: 'show'}, function(data) {
            var html = "";
            $.each(data, function(i, row) {
                var selected = "";
                if (row.file == "<?php echo ($setting['showtpl']); ?>") {
                    selected = " selected";
                }
                html += "<option value=\""+row.file+"\" "+selected+">"+row.name+"（"+row.file+"）"+"</option>"
            });
            $("#showtpl").html(html);
            if (html) $("#showtpl").removeAttr("disabled");
            else $("#showtpl").attr("disabled", true);
        });
    });
    
    if ($("#theme").val() != "") {
        $("#theme").removeAttr("disabled");
        $("#theme").trigger("change");
    }
    <?php if(ACTION_NAME == 'add'): ?>if ($("#module").val() != "") $("#module").trigger("change");<?php endif; ?>
    
    $("#importmodel").click(function() {
        if ($("#importmodeloper").css("display") == "none") {
            $("#importmodeloper").show();
            $("#importmodel").next("span").html("取消导入系统模型！");
            $("#model").attr("required", true);
            $(this).parent().parent().parent().find("tr:eq(2)>td:eq(1)>div").hide();
        } else {
            $("#importmodeloper").hide();
            $("#importmodel").next("span").html("自动导入系统模型！");
            $("#model").removeAttr("required");
            $(this).parent().parent().parent().find("tr:eq(2)>td:eq(1)>div").show();
        }
    });
    
    yhcms.common.dosubmit();
});
-->
</script>
</body>
</html>