<?php if (!defined('THINK_PATH')) exit();?>    <tr>
        <th>文本框长度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmlength" class="sr-only">文本框长度</label>
            <input id="frmlength" type="text" name="setting[length]" class="form-control input-sm" value="<?php echo ((isset($setting["length"]) && ($setting["length"] !== ""))?($setting["length"]):32); ?>" placeholder="请输入文本框长度" required autofocus />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>默认值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认值</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm" value="<?php echo ($setting["default"]); ?>" placeholder="请输入默认值" />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>为密码框<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[ispassword]" value="1" <?php if($setting['ispassword'] == 1): ?>checked<?php endif; ?> />是
                </label>
                <label>
                    <input type="radio" name="setting[ispassword]" value="0" <?php if($setting['ispassword'] == 0): ?>checked<?php endif; ?> />否
                </label>
            </div>
        </td>
        <td class="tips"></td>
    </tr>
    <input type="hidden" name="setting[fieldtype]" value="varchar" />