<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ModuleActionView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>模块操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="info[module]" value="<?php echo ($data["module"]); ?>" />
<input type="hidden" name="info[name]" value="<?php echo ($data["name"]); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-default btn-sm">模块管理</a>
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">
    <?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>模块
    </a>
    <h3 class="btn btn-sm tips-head">您可以对【系统模块】进行管理，如增/删/改/查，显示排序、模块配置及状态设置！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="oper-title"><strong>基础信息</strong>（请填写模块的基本信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;"><?php if($frmlist): ?>选择模块<?php else: ?>模块目录<?php endif; ?><i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <?php if($frmlist): ?><div class="radio input-sm">
                <?php if(is_array($frmlist)): $i = 0; $__LIST__ = $frmlist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><label>
                    <input type="radio" name="data[module]" value="<?php echo ($row["module"]); ?>" required /><?php echo ($row["name"]); ?>（<?php echo ($row["module"]); ?>）
                </label><br /><?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
            <?php else: ?>
            <label for="module" class="sr-only">模块目录</label>
            <input id="module" type="text" name="data[module]" class="form-control input-sm" value="<?php echo ($data["module"]); ?>" placeholder="请输入模块目录" required autofocus <?php if(ACTION_NAME == 'edit'): ?>disabled<?php endif; ?> /><?php endif; ?>
        </td>
        <td class="cms-note">
            <?php if($frmlist): ?><a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('add', ['checkup' => 0]);?>');">点击这里</a> 手动定义系统模块？
            <?php else: ?>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('add', ['checkup' => 1]);?>');">点击这里</a> 获取未知系统模块？<?php endif; ?>
        </td>
    </tr>
    <tr>
        <th>模块名称<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="name" class="sr-only">模块名称</label>
            <input id="name" type="text" name="data[name]" class="form-control input-sm" value="<?php echo ($data["name"]); ?>" placeholder="请输入模块名称" required />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模块描述<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="description" class="sr-only">模块描述</label>
            <textarea id="description" name="data[description]" class="form-control input-sm" placeholder="请输入模块描述" rows="3"><?php echo ($data["description"]); ?></textarea>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>模块属性</strong>（系统模块基础属性信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">模块标识<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="" class="sr-only">模块标识</label>
            <input id="module2" type="text" name="" class="form-control input-sm" value="<?php echo ($data["module"]); ?>" placeholder="请输入模块标识" disabled />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模块版本<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="version" class="sr-only">模块版本</label>
            <input id="version" type="text" name="data[version]" class="form-control input-sm" value="<?php echo ($data["version"]); ?>" placeholder="请输入模块版块" required />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模块属性<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[iscore]" value="1" <?php if($data['iscore'] == 1): ?>checked<?php endif; ?> <?php if($data['iscore'] == 1): ?>disabled<?php endif; ?> />核心模块
                </label>
                <label>
                    <input type="radio" name="data[iscore]" value="0" <?php if($data['iscore'] == 0): ?>checked<?php endif; ?> <?php if($data['iscore'] == 1): ?>disabled<?php endif; ?> />扩展模块
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模块模型<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[ismodel]" value="1" <?php if($data['ismodel'] == 1): ?>checked<?php endif; ?> />允许创建
                </label>
                <label>
                    <input type="radio" name="data[ismodel]" value="0" <?php if($data['ismodel'] == 0): ?>checked<?php endif; ?> />禁止创建
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>模块备注</strong>（系统模块开发团队信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">开发作者<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <label for="author" class="sr-only">开发作者</label>
            <input id="author" type="text" name="data[remarks][author]" class="form-control input-sm" value="<?php echo ($data["author"]); ?>" placeholder="请输入开发作者" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>模块网址<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="url" class="sr-only">模块网址</label>
            <input id="url" type="text" name="data[remarks][url]" class="form-control input-sm" value="<?php echo ($data["url"]); ?>" placeholder="请输入模块网址" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">模块状态<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[disabled]" value="0" <?php if($data['disabled'] == 0): ?>checked<?php endif; ?> <?php if($data['iscore'] == 1): ?>disabled<?php endif; ?> />启用模块
                </label>
                <label>
                    <input type="radio" name="data[disabled]" value="1" <?php if($data['disabled'] == 1): ?>checked<?php endif; ?> <?php if($data['iscore'] == 1): ?>disabled<?php endif; ?> />禁用模块
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table oper-table-btns">
    <tbody>
    <tr>
        <td style="width:520px;">
            <input id="dosubmit" type="submit" name="dosubmit" value="<?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>模块" class="btn btn-danger btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <td style="width:520px;">
            <input type="button" name="doreturn" value="返回列表" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-link btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var module = $("#module"), tag = $("#module2");
        
        module.change(function() {
            tag.val($(this).val());
        }).blur(function() {
            if ($(this).val() == "") return ;
            $.getJSON("<?php echo U('helper');?>", {module: $(this).val()}, function(data) {
                if (data) {
                    $("#name").val(data.name);
                    $("#description").val(data.description);
                    $("#version").val(data.version);
                    $("#author").val(data.remarks.author);
                    $("#url").val(data.remarks.url);
                    if (data.core) {
                        $("input[name='data[iscore]']:eq(0)").trigger("click");
                    } else {
                        $("input[name='data[iscore]']:eq(1)").trigger("click");
                    }
                    if (data.model) {
                        $("input[name='data[ismodel]']:eq(0)").trigger("click");
                    } else {
                        $("input[name='data[ismodel]']:eq(1)").trigger("click");
                    }
                } else {
                    $("#name").val("");
                    $("#description").val("");
                    $("#version").val("");
                    $("#author").val("");
                    $("#url").val("");
                    $("input[name='data[iscore]']:eq(0)").trigger("click");
                    $("input[name='data[ismodel]']:eq(0)").trigger("click");
                }
            });
        });
        
        $("input[name='data[module]']").click(function() {
            tag.val($(this).val());
            if ($(this).val() == "") return ;
            $.getJSON("<?php echo U('helper');?>", {module: $(this).val()}, function(data) {
                if (data) {
                    $("#name").val(data.name);
                    $("#description").val(data.description);
                    $("#version").val(data.version);
                    $("#author").val(data.remarks.author);
                    $("#url").val(data.remarks.url);
                    if (data.core) {
                        $("input[name='data[iscore]']:eq(0)").trigger("click");
                    } else {
                        $("input[name='data[iscore]']:eq(1)").trigger("click");
                    }
                    if (data.model) {
                        $("input[name='data[ismodel]']:eq(0)").trigger("click");
                    } else {
                        $("input[name='data[ismodel]']:eq(1)").trigger("click");
                    }
                } else {
                    $("#name").val("");
                    $("#description").val("");
                    $("#version").val("");
                    $("#author").val("");
                    $("#url").val("");
                    $("input[name='data[iscore]']:eq(0)").trigger("click");
                    $("input[name='data[ismodel]']:eq(0)").trigger("click");
                }
            });
        });
    yhcms.common.dosubmit();
});
-->
</script>
</body>
</html>