<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: AttachmentIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>附件管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="module" value="<?php echo ($module); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">附件管理</a>
    <h3 class="btn btn-sm tips-head">您可以对【内容附件】进行管理，如查看/下载、添加或删除操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">附件搜索</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-big">ID</th>
        <th style="width:120px;">所属模块</th>
        <th class="cms-tc" style="width:100px;">所属栏目</th>
        <th>附件名称</th>
        <th class="cms-tc" style="width:100px;">附件大小</th>
        <th class="cms-tc" style="width:140px;">上传时间</th>
        <th class="cms-tc" style="width:104px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="8">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th class="list-checkbox">
            <input class="checkchild" type="checkbox" name="info[attachedid][]" value="<?php echo ($row["attachedid"]); ?>" />
        </th>
        <td class="list-big"><?php echo ($row["attachedid"]); ?></td>
        <td><?php echo ($row["module"]); ?></td>
        <td class="cms-tc"><?php echo ((isset($row["catename"]) && ($row["catename"] !== ""))?($row["catename"]):"/"); ?></td>
        <td data-attachedid="<?php echo ($row["attachedid"]); ?>">
            <a style="float:left; margin-right:2px;"><?php echo ($row["name"]); ?></a>
            <?php if($row['isthumb'] == 1): ?><i class="icon-style icon-style-thumb" title="缩略图"></i><?php endif; ?>
            <?php if($row['temp'] == 1): ?><i class="icon-style icon-style-redundancy" title="冗余"></i><?php endif; ?>
        </td>
        <td class="cms-tc"><?php echo ($row["filesize"]); ?></td>
        <td class="cms-tc"><?php echo ($row["uploadtime"]); ?></td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('<?php echo U('shows', ['attachedid' => $row['attachedid']]);?>', '查看附件', 'AdminAttachmentIndex-640-480');" title="查看附件">查看</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['attachedid' => $row['attachedid']]);?>', '确认删除附件！');" title="删除附件">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的附件！');" class="btn btn-danger btn-sm">删除附件</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var attachedid = $(this).find("td:eq(3)").attr("data-attachedid"),
                url = "<?php echo U('shows', ['attachedid' => '']);?>" + attachedid;
            if (attachedid) {
                yhcms.dialog.window(url, "查看附件", "AdminAttachmentIndex-640-480");
            }
        });
    yhcms.common.checkall().dosubmit();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>