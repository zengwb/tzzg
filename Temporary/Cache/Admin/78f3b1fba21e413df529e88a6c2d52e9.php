<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: FieldIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>字段管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">字段管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('add', ['modelid' => $modelid]);?>');" class="btn btn-default btn-sm">添加字段</a>
    <h3 class="btn btn-sm tips-head">您可以对【<?php echo ($name); ?>】字段管理，如增/删/改/查，显示排序、设置状态等操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th style="width:120px;">字段名称</th>
        <th>字段别名</th>
        <th class="cms-tc" style="width:140px;">类型</th>
        <th class="list-big">系统</th>
        <th class="list-big">主表</th>
        <th class="list-big">基本</th>
        <th class="list-big">隐藏</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:102px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="12">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" type="checkbox" name="info[fieldid][]" value="<?php echo ($row["fieldid"]); ?>" />
        </th>
        <td class="list-small"><?php echo ($row[fieldid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[fieldid][]" value="<?php echo ($row[fieldid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td data-modelid='<?php echo ($row["modelid"]); ?>' data-fieldid='<?php echo ($row["fieldid"]); ?>'><a><?php echo ($row["field"]); ?></a></td>
        <td class="cms-c666"><?php echo ($row["alias"]); ?></td>
        <td class="cms-tc"><?php echo ($row["formtype"]); ?></td>
        <td class="cms-tc icon-color">
        <?php if($row['issystem']): ?><a class="list-operation" data-state="" href="javascript:void(0);" title="">
                <i class="iconfont icon-qiyong"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="" href="javascript:void(0);" title="">
                <i class="iconfont icon-qingchu cms-c999"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc icon-color">
        <?php if($row['ismain']): ?><a class="list-operation" data-state="" href="javascript:void(0);" title="">
                <i class="iconfont icon-qiyong"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="" href="javascript:void(0);" title="">
                <i class="iconfont icon-qingchu cms-cccc"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc icon-color">
        <?php if($row['isbase']): ?><a class="list-operation" data-state="" href="javascript:void(0);" title="">
                <i class="iconfont icon-qiyong"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="" href="javascript:void(0);" title="">
                <i class="iconfont icon-qingchu cms-cccc"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc icon-color">
        <?php if($row['display']): ?><a class="list-operation" data-state="<?php echo ($row[fieldid]); ?>" href="javascript:void(0);" title="点击显示字段">
                <i class="iconfont icon-qingchu cms-cccc"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[fieldid]); ?>" href="javascript:void(0);" title="点击隐藏字段">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc icon-color">
        <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[fieldid]); ?>" href="javascript:void(0);" title="点击启用字段">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[fieldid]); ?>" href="javascript:void(0);" title="点击禁用字段">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('edit', ['modelid' => $modelid, 'fieldid' => $row['fieldid']]);?>');" title="修改字段">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['fieldid' => $row['fieldid']]);?>', '确认删除『<?php echo ($row['field']); ?>』模型字段！');" title="删除字段">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的字段！');" class="btn btn-danger btn-sm">删除字段</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), fieldid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {fieldid: fieldid}, function(data) {
                if (data == 0) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击禁用字段");
                }
                if (data == 1) {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击启用字段");
                }
            });
        });
        list.find("td:eq(-3)>a").click(function() {
            var current = $(this), fieldid = $(this).attr("data-state");
            $.getJSON("<?php echo U('shows');?>", {fieldid: fieldid}, function(data) {
                if (data == 0) {
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击显示字段");
                }
                if (data == 1) {
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击隐藏字段");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var modelid = $(this).find("td:eq(2)").attr("data-modelid"),
                fieldid = $(this).find("td:eq(2)").attr("data-fieldid"),
                url     = "<?php echo U('edit', ['fieldid' => '']);?>" + fieldid;
            
            if (modelid) yhcms.common.linkurl(url);
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>