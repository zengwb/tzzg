<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: TemplateThemeView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>主题管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">模板主题</a>
    <h3 class="btn btn-sm tips-head">您可以对【模板主题】进行管理，如更新模板/删除主题、设置状态操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th style="width:240px;">模板主题</th>
        <th>主题描述</th>
        <th style="width:240px;">主题作者</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:104px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="7">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if(!$row['display']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" name="info[dirname][]" value="<?php echo ($row[dirname]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($key+1); ?></td>
        <td data-dirname='<?php echo ($row["dirname"]); ?>'><a><?php echo ($row["dirname"]); ?></a></td>
        <td><a><?php echo ($row["name"]); ?></a></td>
        <td><a href="<?php echo ($row["link"]); ?>" target="_blank"><?php echo ($row["author"]); ?></a></td>
        <td class="cms-tc icon-color">
        <?php if(!$row['display']): ?><a class="list-operation" data-state="<?php echo ($row[dirname]); ?>" href="javascript:void(0);" title="点击启用主题">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[dirname]); ?>" href="javascript:void(0);" title="点击禁用主题">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="yhcms.common.linkurl('<?php echo U('template', ['dirname' => $row['dirname']]);?>');" title="管理模板">管理</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['dirname' => $row['dirname']]);?>', '确认删除【<?php echo ($row['name']); ?>】模板主题！');" title="删除主题">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的主题！');" class="btn btn-danger btn-sm">删除主题</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), dirname = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {dirname: dirname}, function(data) {
                if (data == 1) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击启用主题");
                } else {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击禁用主题");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var dirname = $(this).find("td:eq(1)").attr("data-dirname"),
                url = "<?php echo U('template', ['dirname' => '']);?>" + dirname;
            
            if (dirname) { yhcms.common.linkurl(url); }
        });
    
    yhcms.common.dosubmit().checkall();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>