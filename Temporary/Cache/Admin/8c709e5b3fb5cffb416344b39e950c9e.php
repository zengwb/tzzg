<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: DatabaseIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>数据管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">数据管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('execute');?>');" class="btn btn-default btn-sm">执行SQL</a>
    <h3 class="btn btn-sm tips-head">您可以对【系统数据】进行管理，如数据优化/修复及查看数据结构！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th style="width:220px;">数据表名称</th>
        <th style="width:120px;">数据表注释</th>
        <th class="cms-tc" style="width:100px;">引擎</th>
        <th>字符集</th>
        <th class="cms-tc" style="width:80px;">记录数</th>
        <th class="cms-tc" style="width:100px;">占用空间</th>
        <th class="cms-tc" style="width:104px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="9">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th class="list-checkbox">
            <input class="checkchild" name="info[table][]" value="<?php echo ($row["name"]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($key+1); ?></td>
        <td data-table='<?php echo ($row["name"]); ?>'><a><?php echo ($row["name"]); ?></a></td>
        <td><?php echo ($row["comment"]); ?></td>
        <td class="cms-tc"><?php echo ($row["engine"]); ?></td>
        <td><?php echo (strtoupper($row["collation"])); ?></td>
        <td class="cms-tc"><?php echo ($row["rows"]); ?></td>
        <td class="cms-tc"><?php echo (cms_byte_unit($row["index_length"])); ?></td>
        <td>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('optimize', ['table' => $row['name']]);?>');" title="优化数据">优化</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('repair', ['table' => $row['name']]);?>');" title="修复数据">修复</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('<?php echo U('structure', ['table' => $row['name']]);?>', '查看【<?php echo ($row['name']); ?>】结构', 'AdminDatabaseIndex-640-360');" title="查看结构">结构</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('optimize');?>', '确认优化选中的数据库表！');" class="btn btn-danger btn-sm">批量优化</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('repair');?>', '确认修复选中的数据库表！')" class="btn btn-default btn-sm">批量修复</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var table = $(this).find("td:eq(1)").attr("data-table"),
                name  = $(this).find("td:eq(1)").find("a").html(),
                url   = "<?php echo U('structure', ['table' => '']);?>" + table;
            
            if (table) {
                yhcms.dialog.window(url, '查看【'+name+'】结构', 'AdminDatabaseIndex-640-360');
            }
        });
    
    yhcms.common.dosubmit().checkall();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>