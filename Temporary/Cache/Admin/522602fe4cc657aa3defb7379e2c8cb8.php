<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ConfigIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>配置管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">配置管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add');?>', '添加配置', 'AdminConfigAdd-0-540-222');" class="btn btn-default btn-sm">添加配置</a>
    <h3 class="btn btn-sm tips-head">您可以对【系统配置】进行管理，如增/删/改/查，显示排序、设置状态等操作！</h3>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('ConfigGroup/index');?>');" class="btn btn-link btn-sm">配置分组</a>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索配置</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th style="width:220px;">配置名称</th>
        <th style="width:100px;">所属模块</th>
        <th style="width:140px;">配置分组</th>
        <th style="width:140px;">配置参数</th>
        <th>配置描述</th>
        <th style="width:160px;">所属站点</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:136px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="11">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" type="checkbox" name="info[configid][]" value="<?php echo ($row["configid"]); ?>" />
        </th>
        <td class="list-small"><?php echo ($row[configid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[configid][]" value="<?php echo ($row[configid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td data-configid='<?php echo ($row["configid"]); ?>' title="<?php echo ($row["name"]); ?>"><?php echo ($row["title"]); ?><span class="cms-c999">（<?php echo ($row["configtype"]); ?>）</span></td>
        <td><a href="javascript:void(0);"><?php echo ($row["module"]); ?></a></td>
        <td><a href="javascript:void(0);"><?php echo ($row["groupname"]); ?></a>（<?php echo ($row["group"]); ?>）</td>
        <td><a href="javascript:void(0);"><?php echo ($row["params"]); ?></a></td>
        <td class="cms-c999"><?php echo ($row["description"]); ?></td>
        <td><?php echo ($row["sitename"]); ?></td>
        <td class="cms-tc icon-color">
        <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[configid]); ?>" href="javascript:void(0);" title="点击启用配置">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[configid]); ?>" href="javascript:void(0);" title="点击禁用配置">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('edit', ['configid' => $row['configid']]);?>', '修改【<?php echo ($row['name']); ?>】配置', 'AdminConfigEdit-0-540-222');" title="修改配置">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('type', ['configid' => $row['configid']]);?>', '设置【<?php echo ($row['name']); ?>】类型', 'AdminConfigType-0-560-320');" title="设置类型">设置</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('site', ['configid' => $row['configid']]);?>', '设置【<?php echo ($row['name']); ?>】站点', 'AdminConfigSite-0-540-222');" title="设置站点">站点</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['configid' => $row['configid']]);?>', '确认删除『<?php echo ($row['name']); ?>』模块配置！');" title="删除配置">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的配置！');" class="btn btn-danger btn-sm">删除配置</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), configid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {configid: configid}, function(data) {
                if (data == 0) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击禁用配置");
                }
                if (data == 1) {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击启用配置");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var configid = $(this).find("td:eq(0)").html(),
                name     = $(this).find("td:eq(2)").attr("title"),
                url      = "<?php echo U('edit', ['configid' => '']);?>" + configid;
            
            if (configid) {
                yhcms.dialog.topwin(url, '修改【'+name+'】配置', 'AdminConfigEdit-0-540-222');
            }
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>