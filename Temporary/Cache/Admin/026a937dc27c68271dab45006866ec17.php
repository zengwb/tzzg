<?php if (!defined('THINK_PATH')) exit();?>   <tr>
        <th>字段类型<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmfieldtype" class="sr-only">字段类型</label>
            <select id="frmfieldtype" class="form-control input-sm cms-fleft" name="setting[fieldtype]">
                <option value="varchar" <?php if($setting['fieldtype'] == 'varchar'): ?>selected<?php endif; ?>>字符 varchar</option>
                <option value="tinyint" <?php if($setting['fieldtype'] == 'tinyint'): ?>selected<?php endif; ?>>整数 tinyint(3)</option>
                <option value="smallint" <?php if($setting['fieldtype'] == 'smallint'): ?>selected<?php endif; ?>>整数 smallint(5)</option>
                <option value="mediumint" <?php if($setting['fieldtype'] == 'int'): ?>selected<?php endif; ?>>整数 mediumint(8)</option>
                <option value="int" <?php if($setting['fieldtype'] == 'int'): ?>selected<?php endif; ?>>整数 int(10)</option>
            </select>
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>选项类型<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[boxtype]" value="radio" <?php if($setting['boxtype'] == 'radio'): ?>checked<?php endif; ?> required />
                    单选按钮
                </label>
                <label>
                    <input type="radio" name="setting[boxtype]" value="checkbox" <?php if($setting['boxtype'] == 'checkbox'): ?>checked<?php endif; ?> required />
                    复选框
                </label>
                <label>
                    <input type="radio" name="setting[boxtype]" value="select" <?php if($setting['boxtype'] == 'select'): ?>checked<?php endif; ?> required />
                    下拉框
                </label>
                <label>
                    <input type="radio" name="setting[boxtype]" value="multiple" <?php if($setting['boxtype'] == 'multiple'): ?>checked<?php endif; ?> required />
                    多选列表框
                </label>
                <label>
                    <input type="radio" name="setting[boxtype]" value="linkage" <?php if($setting['boxtype'] == 'linkage'): ?>checked<?php endif; ?> required />
                    联动菜单
                </label>
            </div>
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>联动ＩＤ<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmmenuid" class="sr-only">联动ＩＤ</label>
            <input id="frmmenuid" type="text" name="setting[menuid]" class="form-control input-sm" value="<?php echo ((isset($setting["menuid"]) && ($setting["menuid"] !== ""))?($setting["menuid"]):0); ?>" placeholder="请输入联动ＩＤ" required />
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>每列宽度<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmwidth" class="sr-only">每列宽度</label>
            <input id="frmwidth" type="text" name="setting[width]" class="form-control input-sm" value="<?php echo ((isset($setting["width"]) && ($setting["width"] !== ""))?($setting["width"]):80); ?>" placeholder="请输入每列宽度" required />
        </td>
        <td class="tips">px</td>
    </tr>
    <tr>
        <th>输出格式<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="setting[outputtype]" value="0" <?php if($setting['outputtype'] == 0): ?>checked<?php endif; ?> required />
                    选项名称
                </label>
                <label>
                    <input type="radio" name="setting[outputtype]" value="1" <?php if($setting['outputtype'] == 1): ?>checked<?php endif; ?> required />
                    选项值
                </label>
            </div>
        </td>
        <td class="tips"></td>
    </tr>
    <tr>
        <th>默认值<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="frmdefault" class="sr-only">默认值</label>
            <input id="frmdefault" type="text" name="setting[default]" class="form-control input-sm"
                value="<?php echo ($setting["default"]); ?>" placeholder="请输入默认值" />
        </td>
        <td class="tips"></td>
    </tr>
    <!--<input type="hidden" name="setting[fieldtype]" value="text" />-->