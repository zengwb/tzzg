<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: LinkageListsView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>菜单管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <?php if($info['subsetid'] == 0): ?><a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-danger btn-sm">菜单管理</a>
    <?php else: ?>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('lists', ['subsetid' => $info['subsetid'], 'linkageid' => $info['linkageid']]);?>');" class="btn btn-danger btn-sm">菜单管理</a><?php endif; ?>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add', ['subsetid' => $info['subsetid'], 'parentid' => $info['linkageid']]);?>', '添加菜单', 'AdminLinkageAdd-0-540-152');" class="btn btn-default btn-sm">添加菜单</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('addAll', ['subsetid' => $info['subsetid'], 'parentid' => $info['linkageid']]);?>', '批量添加', 'AdminLinkageAddAll-0-540-152');" class="btn btn-default btn-sm">批量添加</a>
    <h3 class="btn btn-sm tips-head">
        <?php if($info['subsetid'] == 0): ?><a href="javascript:history.go(-1);" class="cms-cf30">返回上级菜单</a>，菜单目录：<span class="cms-c666"><?php echo ($list); ?></span><?php endif; ?>
    </h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索菜单</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th>联动菜单</th>
        <th>父级菜单</th>
        <th>菜单描述</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:230px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="8">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if(!$row['display']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" name="info[linkageid][]" value="<?php echo ($row[linkageid]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($row[linkageid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[linkageid][]" value="<?php echo ($row[linkageid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td data-linkageid="<?php echo ($row[linkageid]); ?>" data-name="<?php echo ($row[name]); ?>">
            <?php if($row['child']): ?><a href="javascript:void(0);" onClick="javascript:<?php echo ($row['_link']); ?>" title="管理子项"><?php echo ($row[name]); ?></a>
            <?php else: ?>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('edit', ['linkageid' => $row['linkageid']]);?>', '修改【<?php echo ($row['name']); ?>】菜单', 'AdminLinkageEdit-0-540-152');"><?php echo ($row[name]); ?></a><?php endif; ?>
        </td>
        <td class="cms-c999" style="width:120px; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;"><?php echo ($row["parent"]); ?></td>
        <td><?php echo ($row[description]); ?></td>
        <td class="cms-tc icon-color">
        <?php if(!$row['display']): ?><a class="list-operation" data-state="<?php echo ($row[linkageid]); ?>" href="javascript:void(0);" title="点击启用菜单">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[linkageid]); ?>" href="javascript:void(0);" title="点击禁用菜单">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <?php if($row['child']): ?><a href="javascript:void(0);" onClick="javascript:<?php echo ($row['_link']); ?>" title="管理菜单">管理</a>
            <?php else: ?>
            <a href="javascript:void(0);" class="cms-cccc">管理</a><?php endif; ?>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add', ['subsetid' => $row['linkageid'], 'parentid' => $row['linkageid']]);?>', '添加【<?php echo ($row['name']); ?>】子项', 'AdminLinkageAdd-0-540-152');" title="添加子项">添加</a>

            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('image', ['linkageid' => $row['linkageid'], 'parentid' => $row['parentid']]);?>', '添加【<?php echo ($row['name']); ?>】图片', 'AdminLinkageAdd-0-540-152');" title="介绍图片">图片</a>
            <a href="javascript:void(0);" role="button" onclick="javascript:yhcms.dialog.openx('/index.php?m=article&amp;c=content&amp;a=cityintroduce&amp;rand='+Math.random()+'&amp;linkageid='+<?php echo ($row['linkageid']); ?>, '添加内容');" title="介绍">介绍</a>

            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('edit', ['subsetid' => $info['subsetid'], 'linkageid' => $row['linkageid']]);?>', '修改【<?php echo ($row['name']); ?>】菜单', 'AdminLinkageEdit-0-540-152');" title="修改菜单">修改</a>
            <?php if($info['subsetid'] == 0): ?><a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('<?php echo U('lists', ['subsetid' => $row['linkageid'], 'linkageid' => $row['linkageid']]);?>', '菜单【<?php echo ($row['name']); ?>】子集', 'AdminSubset-900-442');">子集</a><?php endif; ?>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('move', ['subsetid' => $info['subsetid'], 'linkageid' => $row['linkageid']]);?>', '移动【<?php echo ($row['name']); ?>】菜单', 'AdminLinkageMove-0-540-132');" title="移动菜单">移动</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['subsetid' => $info['subsetid'], 'linkageid' => $row['linkageid']]);?>', '确认删除【<?php echo ($row['name']); ?>】联动菜单！');" title="删除菜单">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的菜单！');" class="btn btn-danger btn-sm">删除菜单</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), linkageid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {linkageid: linkageid}, function(data) {
                if (data.state == 1) {
                    $.each(data.linkageid, function(i, id) {
                        list.each(function() {
                            if ($(this).find("td:eq(2)").attr("data-linkageid") == id) {
                                $(this).find("td:eq(2)").parent().removeClass("display");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a>i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a").attr("title", "点击禁用菜单")
                            }
                        });
                    });
                } else {
                    $.each(data.linkageid, function(i, id) {
                        list.each(function() {
                            if ($(this).find("td:eq(2)").attr("data-linkageid") == id) {
                                $(this).find("td:eq(2)").parent().addClass("display");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a>i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a").attr("title", "点击禁用菜单")
                            }
                        });
                    });
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var linkageid = $(this).find("td:eq(2)").attr("data-linkageid"),
                name = $(this).find("td:eq(2)").find("a").html(),
                url = "<?php echo U('edit', ['subsetid' => $info['subsetid'], 'linkageid' => '']);?>" + linkageid;
            
            if (linkageid) {
                yhcms.dialog.topwin(url, '修改【'+name+'】菜单', 'AdminLinkageEdit-0-540-152');
            }
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>