<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: RoleIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>角色管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-danger btn-sm">角色管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('add', ['parentid' => $info['parentid']]);?>');" class="btn btn-default btn-sm">添加角色</a>
    <!--<a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add', ['parentid' => $info['parentid']]);?>', '添加角色', 'AdminRoleAdd-0-480-150');" class="btn btn-default btn-sm">添加角色</a>-->
    <h3 class="btn btn-sm tips-head">您可以对【用户角色】进行管理，如增/删/改/查，排序/权限/用户/移动，设置状态操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th style="width:260px;">系统用户组</th>
        <th>用户组描述</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:226px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="7">暂无内容！</td></tr><?php endif; ?>
<?php echo ($tree); ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的用户组！');" class="btn btn-danger btn-sm">删除角色</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), roleid = $(this).attr("data-state");
            $.getJSON("<?php echo U('stateAll');?>", {roleid: roleid}, function(data) {
            if (data.state == 2) {
                yhcms.dialog.tips("<?php echo U('index');?>", "超级管理角色态设置无效！");
            } else if (data.state == 1) {
                    $.each(data.roleid, function(i, id) {
                        list.each(function() {
                            if ($(this).find("td:eq(2)").attr("data-roleid") == id) {
                                $(this).find("td:eq(2)").parent().addClass("display");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a>i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a").attr("title", "点击启用角色")
                            }
                        });
                    });
                } else {
                    $.each(data.roleid, function(i, id) {
                        list.each(function() {
                            if ($(this).find("td:eq(2)").attr("data-roleid") == id) {
                                $(this).find("td:eq(2)").parent().removeClass("display");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a>i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                                $(this).find("td:eq(2)").parent().find("td:eq(-2)>a").attr("title", "点击禁用角色")
                            }
                        });
                    });
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var roleid = $(this).find("td:eq(2)").attr("data-roleid"),
                url = "<?php echo U('edit', ['roleid' => '']);?>" + roleid;
            
            if (roleid) { yhcms.common.linkurl(url); }
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>