<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: CategoryAction0View.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>栏目操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="info[catid]" value="<?php echo ($data["catid"]); ?>" />
<input type="hidden" name="info[catname]" value="<?php echo ($data["catname"]); ?>" />
<input type="hidden" name="info[catdir]" value="<?php echo ($data["catdir"]); ?>" />
<input type="hidden" name="data[type]" value="<?php echo ($cattype); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-default btn-sm">栏目管理</a>
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm"><?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>栏目</a>
    <h3 class="btn btn-sm tips-head">您可以对【栏目菜单】进行管理，如增/删/改/查，显示排序、设置状态等操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="oper-title"><strong>隶属关系</strong>（请选择栏目的隶属关系）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">上级栏目<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="parentid" class="sr-only">上级栏目</label>
            <select id="parentid" class="form-control input-sm cms-fleft" name="data[parentid]" required>
                <option value="0">作为顶级栏目</option>
                <?php echo ($cattree); ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>所属模块<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="module" class="sr-only">所属模块</label>
            <select id="module" class="form-control input-sm cms-fleft" name="data[module]" required>
                <option value="">请选择所属模块</option>
                <?php if(is_array($module)): $i = 0; $__LIST__ = $module;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($row["module"]); ?>" <?php if($row['module'] == $data['module']): ?>selected<?php endif; ?>><?php echo ($row["name"]); ?>（<?php echo ($row["module"]); ?>）</option><?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>所属模型<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="modelid" class="sr-only">所属模型</label>
            <select id="modelid" class="form-control input-sm cms-fleft" name="data[modelid]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <?php if(ACTION_NAME == 'add'): ?><tr>
        <th>添加方式<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <div class="radio input-sm">
            <label>
                <input type="radio" name="action" value="1" checked />单条添加
            </label>
            <label>
                <input type="radio" name="action" value="0" disabled />批量添加
            </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr><?php endif; ?>
    </tbody>
</table>
<div class="oper-title"><strong>基础信息</strong>（请填写栏目的基础信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">栏目名称<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="catname" class="sr-only">栏目名称</label>
            <input id="catname" type="text" name="data[catname]" class="form-control input-sm" value="<?php echo ($data["catname"]); ?>" placeholder="请输入栏目名称" required style="font-weight:<?php echo ($style["bold"]); ?>; color:<?php echo ($style["color"]); ?>;" />
        </td>
        <td class="cms-note">
            <input type="hidden" id="style_color" name="style[color]" value="<?php echo ($style["color"]); ?>" />
            <input type="hidden" id="style_font_weight" name="style[bold]" value="<?php echo ($style["bold"]); ?>" />
            <div style="float:left; position:relative; line-height:26px;">
                <i class="icon-style icon-style-lists" onclick="colorpicker('title_color_panel', 'set_title_color');" style="margin:5px 6px 0px 0px; cursor:pointer;"></i>
                <i class="icon-style icon-style-bold" onclick="input_font_bold()" style="margin:5px 6px 0px 0px; cursor:pointer;"></i>
                <span id="title_color_panel" style="position:absolute; top:-3px; left:20px; float:none; z-index:9999;" class="oper-color-panel"></span>
            </div>
        </td>
    </tr>
    <tr>
        <th>栏目缩写<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="name" class="sr-only">栏目缩写</label>
            <input id="name" type="text" name="data[name]" class="form-control input-sm" value="<?php echo ($data["name"]); ?>" placeholder="请输入栏目缩写" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>栏目目录<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="catdir" class="sr-only">栏目目录</label>
            <input id="catdir" type="text" name="data[catdir]" class="form-control input-sm" value="<?php echo ($data["catdir"]); ?>" placeholder="请输入栏目目录" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>栏目描述<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="description" class="sr-only">栏目描述</label>
            <textarea id="description" name="data[description]" class="form-control input-sm" placeholder="请输入栏目描述" rows="3"><?php echo ($data["description"]); ?></textarea>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>栏目图片<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="image" class="sr-only">栏目图片</label>
            <?php if($data['image']): ?><input type="hidden" name="data[image]" value="<?php echo ($data["image"]); ?>" />
            <img src="<?php echo ($data["image"]); ?>" alt="" style="margin:0px 6px 0px 0px; height:26px; border:solid 1px #f0f0f0;" />
            <a href="javascript:void(0);" onClick="yhcms.common.linkurl('<?php echo U('image', ['catid' => $data['catid']]);?>');">删除？</a>
            <?php else: ?>
            <input id="image" type="file" name="image" /><?php endif; ?>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>模板设置</strong>（请选择栏目的主题模板）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">风格主题<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="theme" class="sr-only">风格主题</label>
            <select id="theme" class="form-control input-sm cms-fleft" name="setting[theme]" disabled>
                <option value="">请选择风格主题</option>
                <?php if(is_array($skin)): $i = 0; $__LIST__ = $skin;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($row["dirname"]); ?>" <?php if($row['dirname'] == $setting['theme']): ?>selected<?php endif; ?>><?php echo ($row["name"]); ?>（<?php echo ($row["dirname"]); ?>）</option><?php endforeach; endif; else: echo "" ;endif; ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>频道模板<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="categorytpl" class="sr-only">频道模板</label>
            <select id="categorytpl" class="form-control input-sm cms-fleft" name="setting[categorytpl]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>列表模板<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="listtpl" class="sr-only">列表模板</label>
            <select id="listtpl" class="form-control input-sm cms-fleft" name="setting[listtpl]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>详细模板<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="showtpl" class="sr-only">详细模板</label>
            <select id="showtpl" class="form-control input-sm cms-fleft" name="setting[showtpl]" disabled>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>优化信息</strong>（请填写栏目的优化信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">网页标题<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <label for="settitle" class="sr-only">网页标题</label>
            <input id="settitle" type="text" name="setting[seo_title]" class="form-control input-sm" value="<?php echo ($setting["seo_title"]); ?>" placeholder="请输入网页标题" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>关键词<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="setkeywords" class="sr-only">关键词</label>
            <input id="setkeywords" type="text" name="setting[seo_keywords]" class="form-control input-sm" value="<?php echo ($setting["seo_keywords"]); ?>" placeholder="请输入网页关键词" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>网页描述<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="setdescription" class="sr-only">网页描述</label>
            <textarea id="setdescription" name="setting[seo_description]" class="form-control input-sm" placeholder="请输入网页描述" rows="3"><?php echo ($setting["seo_description"]); ?></textarea>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">栏目状态<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[display]" value="1" <?php if($data['display'] == 1): ?>checked<?php endif; ?> />前端显示
                </label>
                <label>
                    <input type="radio" name="data[display]" value="0" <?php if($data['display'] == 0): ?>checked<?php endif; ?> />前端隐藏
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table oper-table-btns">
    <tbody>
    <tr>
        <td style="width:520px;">
            <input id="dosubmit" type="submit" name="dosubmit" value="<?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>栏目" class="btn btn-danger btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <td style="width:520px;">
            <input type="button" name="doreturn" value="返回列表" onClick="javascript:history.go(-1);" class="btn btn-link btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/colorpicker.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var info = {}, config = {};
    $("#parentid").change(function() {
        $("#module>option").removeAttr("selected").eq(0).attr("selected", true);
        
        $("#modelid").html("");
        $("#modelid").attr("disabled", true);
        
        $("#theme").attr("disabled", true);
        $("#theme").removeAttr("required");
        
        $("#categorytpl").html("");
        $("#categorytpl").attr("disabled", true);
        
        $("#listtpl").html("");
        $("#listtpl").attr("disabled", true);
        
        $("#showtpl").html("");
        $("#showtpl").attr("disabled", true);
        
        if ($(this).val() == 0) return ;
        $.getJSON("<?php echo U('infos');?>", {catid: $(this).val()}, function(data) {
            if (!data) return ;
            info = data;
            $("#module").children("option").each(function() {
                if ($(this).val() != data.module) return ;
                $("#module>option").removeAttr("selected").eq($(this).index()).attr("selected", true);
                $("#module").trigger("change");
            });
            $("#modelid").removeAttr("disabled");
        });
    });
    $("#module").change(function() {
        $("#modelid").html("");
        $("#modelid").attr("disabled", true);
        
        $("#theme").attr("disabled", true);
        $("#theme").removeAttr("required");
        
        $("#categorytpl").html("");
        $("#categorytpl").attr("disabled", true);
        
        $("#listtpl").html("");
        $("#listtpl").attr("disabled", true);
        
        $("#showtpl").html("");
        $("#showtpl").attr("disabled", true);
        
        if ($(this).val() == "") return ;
        $.getJSON("<?php echo U('model');?>", {module: $(this).val()}, function(data) {
            if (!data) return ;
            var html  = "<option value=\"\">请选择所属模型</option>";
            
            $.each(data, function(i, row) {
                var selected = "";
                if (row.modelid == "<?php echo ($data['modelid']); ?>") {
                    selected = " selected";
                }
                html += "<option value=\""+row.modelid+"\" "+selected+">"+row.name+"（"+row.table+"）"+"</option>";
            });
            $("#modelid").html(html);
            $("#modelid").removeAttr("disabled");
            
            $("#modelid").children("option").each(function() {
                if ($(this).val() != info.modelid) return ;
                $("#modelid>option").removeAttr("selected").eq($(this).index()).attr("selected", true);
                $("#modelid").trigger("change");
            });
        });
    });
    $("#modelid").change(function() {
        $("#theme").attr("disabled", true);
        $("#theme").removeAttr("required");
        
        $("#categorytpl").html("");
        $("#categorytpl").attr("disabled", true);
        
        $("#listtpl").html("");
        $("#listtpl").attr("disabled", true);
        
        $("#showtpl").html("");
        $("#showtpl").attr("disabled", true);
        
        if ($(this).val() == "") return ;
        $.getJSON("<?php echo U('config');?>", {modelid: $(this).val()}, function(data) {
            //if (!data) return ;
            config = data;
            
            $("#theme").removeAttr("disabled");
            $("#theme").attr("required", true);
            $("#theme").children("option").each(function() {
                if ($(this).val() != data.theme) return ;
                <?php if(ACTION_NAME == 'add' || $parentid > 0): ?>$("#theme>option").removeAttr("selected").eq($(this).index()).attr("selected", true);<?php endif; ?>
                $("#theme").trigger("change");
            });
        });
    });
    $("#theme").change(function() {
        $("#categorytpl").html("");
        $("#categorytpl").attr("disabled", true);
        
        $("#listtpl").html("");
        $("#listtpl").attr("disabled", true);
        
        $("#showtpl").html("");
        $("#showtpl").attr("disabled", true);
        
        var theme = $(this).val(), module = $("#module").val();
        if (theme == "") return ;
        
        $.getJSON("<?php echo U('template/ajax');?>", {module: module, theme: theme, prefix: 'category'}, function(data) {
            var html = "";
            $.each(data, function(i, row) {
                var selected = "";
                if (row.file == "<?php echo ($setting['categorytpl']); ?>") {
                    selected = " selected";
                }
                if (row.file == config.categorytpl) {
                    selected = " selected";
                }
                html += "<option value=\""+row.file+"\" "+selected+">"+row.name+"（"+row.file+"）"+"</option>"
            });
            $("#categorytpl").html(html);
            if (html) $("#categorytpl").removeAttr("disabled");
            else $("#categorytpl").attr("disabled", true);
        });
        $.getJSON("<?php echo U('template/ajax');?>", {module: module, theme: theme, prefix: 'list'}, function(data) {
            var html = "";
            $.each(data, function(i, row) {
                var selected = "";
                if (row.file == "<?php echo ($setting['listtpl']); ?>") {
                    selected = " selected";
                }
                if (row.file == config.listtpl) {
                    selected = " selected";
                }
                html += "<option value=\""+row.file+"\" "+selected+">"+row.name+"（"+row.file+"）"+"</option>"
            });
            $("#listtpl").html(html);
            if (html) $("#listtpl").removeAttr("disabled");
            else $("#listtpl").attr("disabled", true);
        });
        $.getJSON("<?php echo U('template/ajax');?>", {module: module, theme: theme, prefix: 'show'}, function(data) {
            var html = "";
            $.each(data, function(i, row) {
                var selected = "";
                if (row.file == "<?php echo ($setting['showtpl']); ?>") {
                    selected = " selected";
                }
                if (row.file == config.showtpl) {
                    selected = " selected";
                }
                html += "<option value=\""+row.file+"\" "+selected+">"+row.name+"（"+row.file+"）"+"</option>"
            });
            $("#showtpl").html(html);
            if (html) $("#showtpl").removeAttr("disabled");
            else $("#showtpl").attr("disabled", true);
        });
    });
    
    <?php if(ACTION_NAME == 'edit' || $parentid > 0): ?>$("#module").trigger("change"); $("#modelid").trigger("change"); $("#theme").trigger("change");<?php endif; ?>
    
    yhcms.common.dosubmit();
});

function set_title_color(color) {
    $('#catname').css('color', color);
    $('#style_color').val(color);
}
function input_font_bold() {
	if ($('#catname').css('font-weight') == '700' || $('#catname').css('font-weight') == 'bold') {
		$('#catname').css('font-weight', 'normal');
		$('#style_font_weight').val('');
	} else {
		$('#catname').css('font-weight', 'bold');
		$('#style_font_weight').val('bold');
	}
}
-->
</script>
</body>
</html>