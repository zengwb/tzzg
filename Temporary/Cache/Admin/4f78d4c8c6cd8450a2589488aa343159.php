<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ConfigSettingView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>模块配置</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U('update');?>" method="post">
<input type="hidden" name="info[module]" value="<?php echo ($module); ?>" />
<input type="hidden" name="info[group]"  value="<?php echo ($group); ?>"  />
<div class="list-tips">
    <?php if(is_array($type)): $i = 0; $__LIST__ = $type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('setting', ['module' => $module, 'group' => $row['group']]);?>');" class="btn btn-<?php if($group == $row['group']): ?>primary<?php else: ?>default<?php endif; ?> btn-sm" style="margin-right:4px;"><?php echo ($row["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
<?php if($type): ?><h3 class="btn btn-sm tips-head">您可以对【<?php echo ($name); ?>】进行操作！</h3><?php else: echo ($module); ?>模块，暂无配置！<?php endif; ?>
    <hr />
</div>
<?php if($list): ?><div class="oper-title"><strong><?php echo ($name); ?></strong>（请填写模块配置信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th style="width:240px;"><?php echo ($row["name"]); ?></th>
        <td style="width:360px;">
            <?php echo ($row["element"]); ?>
        </td>
        <td class="cms-note"><?php echo ($row["description"]); ?></td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table oper-table-btns">
    <tbody>
    <tr>
        <td style="width:600px;">
            <input id="dosubmit" type="submit" name="dosubmit" value="更新配置" class="btn btn-danger btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table><?php endif; ?>
<?php if($type && !$list): ?><table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <td><?php echo ($module); ?>模块，<?php echo ($name); ?>，暂无配置！</td>
    </tr>
    </tbody>
</table><?php endif; ?>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() { yhcms.common.dosubmit(); });
-->
</script>
</body>
</html>