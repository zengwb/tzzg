<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: NavigationActionView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>导航操作</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="popup">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="info[navigationid]" value="<?php echo ($data["navigationid"]); ?>" />
<input type="hidden" name="info[name]" value="<?php echo ($data["name"]); ?>" />
<div class="popup-table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped popup-table-body">
    <tbody>
    <tr>
        <th>导航隶属<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="parentid" class="sr-only">导航隶属</label>
            <select id="parentid" class="form-control input-sm" name="data[parentid]">
                <option value="<?php echo ($info["navigationid"]); ?>">指定为顶级导航</option>
                <?php echo ($tree); ?>
            </select>
        </td>
        <td class="tips">选择顶级导航！</td>
    </tr>
    <tr>
        <th>导航名称<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <div class="cms-fl">
            <label for="name" class="sr-only">导航名称</label>
            <input id="name" type="text" name="data[name]" class="form-control input-sm" value="<?php echo ($data["name"]); ?>" placeholder="请输入导航名称" required autofocus style="font-weight:<?php echo ($style["bold"]); ?>; color:<?php echo ($style["color"]); ?>;" />
            <input type="hidden" id="style_color" name="style[color]" value="<?php echo ($style["color"]); ?>" />
            <input type="hidden" id="style_font_weight" name="style[bold]" value="<?php echo ($style["bold"]); ?>" />
            </div>
            <div style="float:left; position:relative; margin-left:10px; line-height:26px;">
                <i class="icon-style icon-style-lists" onclick="colorpicker('title_color_panel', 'set_title_color');" style="margin:5px 6px 0px 0px; cursor:pointer;"></i>
                <i class="icon-style icon-style-bold" onclick="input_font_bold()" style="margin:5px 6px 0px 0px; cursor:pointer;"></i>
                <span id="title_color_panel" style="position:absolute; top:-3px; left:20px; float:none; z-index:9999;" class="oper-color-panel"></span>
            </div>
        </td>
        <td class="tips">
            填写导航名称！
        </td>
    </tr>
    <tr>
        <th>导航修饰<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="icon" class="sr-only">导航修饰</label>
            <input id="icon" type="text" name="data[icon]" class="form-control input-sm" value="<?php echo ($data["icon"]); ?>" placeholder="请输入导航修饰" />
        </td>
        <td class="tips">
            填写导航样式！
        </td>
    </tr>
    <tr>
        <th>导航链接<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="linkurl" class="sr-only">导航链接</label>
            <input id="linkurl" type="text" name="data[linkurl]" class="form-control input-sm" value="<?php echo ($data["linkurl"]); ?>" placeholder="请输入导航链接" />
        </td>
        <td class="tips">填写导航链接</td>
    </tr>
    <tr>
        <th>导航描述<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="description" class="sr-only">导航描述</label>
            <textarea id="description" name="data[description]" class="form-control input-sm" placeholder="请输入导航描述" rows="3"><?php echo ($data["description"]); ?></textarea>
        </td>
        <td class="tips">填写导航描述！</td>
    </tr>
    </tbody>
</table>
</div>
<input id="dosubmit" type="submit" name="dosubmit" value="提交" class="popup-submit-dialog" />
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/colorpicker.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    
    yhcms.common.dosubmit();
});
function set_title_color(color) {
    $('#name').css('color', color);
    $('#style_color').val(color);
}
function input_font_bold() {
	if ($('#name').css('font-weight') == '700' || $('#name').css('font-weight') == 'bold') {
		$('#name').css('font-weight', 'normal');
		$('#style_font_weight').val('');
	} else {
		$('#name').css('font-weight', 'bold');
		$('#style_font_weight').val('bold');
	}
}
-->
</script>
</body>
</html>