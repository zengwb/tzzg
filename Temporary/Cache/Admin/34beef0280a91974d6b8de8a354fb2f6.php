<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: InstitutionUnitsView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>事业单位</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="info[linkageid]" value="<?php echo ($info["linkageid"]); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('units', ['linkageid' => $info['linkageid']]);?>');" class="btn btn-danger btn-sm">单位管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add', ['linkageid' => $info['linkageid'], 'parentid' => $info['unitid']]);?>', '添加单位', 'AdminInstitutionAdd-0-540-152');" class="btn btn-default btn-sm">添加单位</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('addAll', ['linkageid' => $info['linkageid'], 'parentid' => $info['unitid']]);?>', '批量添加', 'AdminInstitutionAddAll-0-540-152');" class="btn btn-default btn-sm">批量添加</a>
    <h3 class="btn btn-sm tips-head">
        <?php if($info['parentid']): ?><a href="javascript:history.go(-1);" class="cms-cf30">返回上级单位</a>，单位目录：<span class="cms-c666"><?php echo ($list); ?></span><?php endif; ?>
    </h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索单位</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('units', ['linkageid' => $info['linkageid']]);?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th>单位名称</th>
        <th>行政区域</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:230px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="7">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" name="info[unitid][]" value="<?php echo ($row[unitid]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($row[unitid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[unitid][]" value="<?php echo ($row[unitid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td data-unitid="<?php echo ($row[unitid]); ?>" data-name="<?php echo ($row[name]); ?>">
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('units', ['linkageid' => $info['linkageid'], 'parentid' => $row['unitid']]);?>');" title="管理子项"><?php echo ($row["title"]); ?></a>
        </td>
        <td class="cms-c999"><?php echo ($row["area"]); ?></td>
        <td class="cms-tc icon-color">
        <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[unitid]); ?>" href="javascript:void(0);" title="点击启用单位">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[unitid]); ?>" href="javascript:void(0);" title="点击锁定单位">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add', ['linkageid' => $info['linkageid'], 'parentid' => $row['unitid']]);?>', '添加单位', 'AdminInstitutionAdd-0-540-152');">添加</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('edit', ['linkageid' => $info['linkageid'], 'unitid' => $row['unitid']]);?>', '修改单位', 'AdminInstitutionEdit-0-540-152');">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('abouts', ['linkageid' => $info['linkageid'], 'unitid' => $row['unitid']]);?>', '『<?php echo ($row['name']); ?>』单位介绍', 'AdminInstitutionAbouts-0-900-408');" style="font-weight:600;">介绍</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['linkageid' => $info['linkageid'], 'unitid' => $row['unitid']]);?>', '确认删除『<?php echo ($row['name']); ?>』行政单位！');" title="删除单位">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的单位！');" class="btn btn-danger btn-sm">删除单位</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), unitid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {unitid: unitid}, function(data) {
                if (data == 0) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击锁定单位");
                } else if (data == 1) {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击启用单位");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var unitid = $(this).find("td:eq(0)").html(),
                name = $(this).find("td:eq(2)").attr("data-name"),
                url = "<?php echo U('edit', ['linkageid' => $info['linkageid'], 'unitid' => '']);?>" + unitid;
            
            if (unitid) {
                yhcms.dialog.topwin(url, '修改【'+name+'】单位', 'AdminInstitutionEdit-0-540-152');
            }
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>