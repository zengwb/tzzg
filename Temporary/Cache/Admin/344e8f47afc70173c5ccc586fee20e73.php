<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: NavigationListsView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>导航管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('index');?>');" class="btn btn-danger btn-sm">导航管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add', ['parentid' => $info['parentid']]);?>', '导航菜单', 'AdminNavigationAdd-0-548-220');" class="btn btn-default btn-sm">添加导航</a>
    <h3 class="btn btn-sm tips-head"><a href="javascript:history.go(-1);" class="cms-cf30">返回上级导航</a>，导航目录：<span class="cms-c666"><?php echo ($list); ?></span></h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索导航</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th style="width:260px;">导航名称</th>
        <th>URL地址</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:163px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="7">暂无内容！</td></tr><?php endif; ?>
<?php echo ($tree); ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的导航！');" class="btn btn-danger btn-sm">删除导航</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), navigationid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {navigationid: navigationid}, function(data) {
                if (data == 1) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击禁用导航");
                } else {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击启用导航");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var navigationid = $(this).find("td:eq(2)").attr("data-navigationid"),
                name = $(this).find("td:eq(2)").find("a").html(),
                url = "<?php echo U('edit', ['navigationid' => '']);?>" + navigationid;
            
            if (navigationid) {
                yhcms.dialog.topwin(url, '修改【'+name+'】导航', 'AdminNavigationEdit-0-548-220');
            }
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>