<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: TemplateEditView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>编辑模板</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<style type="text/css">
<!--
textarea.input-sm { height:auto; font-family:"Microsoft Yahei", Arial, Helvetica, sans-serif; font-size:14px; }
-->
</style>
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type='hidden' name='info[template]' value='<?php echo ($template); ?>' />
<div class="list-tips">
    <a href="javascript:void(0);" onClick="yhcms.common.linkurl('<?php echo U('index');?>');" role="button" class="btn btn-default btn-sm">模板主题</a>
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">模板管理</a>
    <h3 class="btn btn-sm tips-head">当前模板：<?php echo ($template); ?></h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">使用帮助</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmAct', '<?php echo C('CMS_ADMIN_HELPER');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<label for="content" class="sr-only">模板内容</label>
<textarea id="content" name="data[content]" class="form-control input-sm" placeholder="" rows="16" required><?php echo ($content); ?></textarea>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="javascript:history.go(-1);" class="btn btn-default btn-sm">返回列表</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmAct', '<?php echo U(ACTION_NAME);?>', '确认更新模板！');" class="btn btn-danger btn-sm">更新模板</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    yhcms.common.dosubmit();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>