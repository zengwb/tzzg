<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: AdminInfosView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>用户信息</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post" enctype="multipart/form-data">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" onClick="" class="btn btn-danger btn-sm">用户信息</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('privs');?>');" class="btn btn-default btn-sm">我的权限</a>
    <h3 class="btn btn-sm tips-head">您可以对【<?php echo ($data["username"]); ?>】进行管理，如更新用户信息及查看管理权限！</h3>
    <hr />
</div>
<div class="oper-title"><strong>基础信息</strong>（请填写用户的基本信息）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">角色隶属<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td style="width:360px;">
            <label for="roleid" class="sr-only">角色隶属</label>
            <select id="roleid" class="form-control input-sm" name="data[roleid]" disabled>
                <option value="">指定为顶级用户组</option>
                <?php echo ($tree); ?>
            </select>
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>用户帐号<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="username" class="sr-only">用户帐号</label>
            <input id="username" type="text" name="data[username]" class="form-control input-sm" value="<?php echo ($data["username"]); ?>" placeholder="请输入用户帐号" disabled />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>管理密码<i class="iconfont icon-xinghao cms-cf30"></i></th>
        <td>
            <label for="password" class="sr-only">管理密码</label>
            <input id="password" type="text" name="data[password]" class="form-control input-sm" value="<?php echo ($data["password"]); ?>" placeholder="请输入管理密码" disabled />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>联系方式</strong>（系统用户紧急联系方式）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">真实姓名<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <label for="realname" class="sr-only">真实姓名</label>
            <input id="realname" type="text" name="data[realname]" class="form-control input-sm" value="<?php echo ($data["realname"]); ?>" placeholder="请输入真实姓名" />
        </td>
        <td class="cms-note"></td>
    </tr>
    <tr>
        <th>联系方式<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="phone" class="sr-only">联系方式</label>
            <input id="phone" type="text" name="data[phone]" class="form-control input-sm" value="<?php echo ($data["phone"]); ?>" placeholder="请输入联系方式" />
        </td>
        <td class="cms-note">手机或常用座机号码！</td>
    </tr>
    <tr>
        <th>电子邮箱<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td>
            <label for="email" class="sr-only">电子邮箱</label>
            <input id="email" type="text" name="data[email]" class="form-control input-sm" value="<?php echo ($data["email"]); ?>" placeholder="请输入电子邮箱" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<div class="oper-title"><strong>扩展设置</strong>（这里指定角色所属站点）</div>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">用户头像<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <label for="avater" class="sr-only">用户头像</label>
            <?php if($data['avater']): ?><input type="hidden" name="data[avater]" value="<?php echo ($data["avater"]); ?>" />
            <img src="<?php echo ($data["avater"]); ?>" alt="" style="margin:0px 6px 0px 0px; height:26px; border:solid 1px #f0f0f0;" />
            <a href="<?php echo U('avater', ['username' => $data['username']]);?>">删除？</a>
            <?php else: ?>
            <input id="avater" type="file" name="avater" style="float:left;" /><?php endif; ?>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table">
    <tbody>
    <tr>
        <th style="width:160px;">用户状态<i class="iconfont icon-xinghao cms-tips"></i></th>
        <td style="width:360px;">
            <div class="radio input-sm">
                <label>
                    <input type="radio" name="data[disabled]" value="0" <?php if($data['disabled'] == 0): ?>checked<?php endif; ?> disabled />登陆开启
                </label>
                <label>
                    <input type="radio" name="data[disabled]" value="1" <?php if($data['disabled'] == 1): ?>checked<?php endif; ?> disabled />登陆锁定
                </label>
            </div>
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
<table class="table table-condensed table-bordered cms-table-form oper-table oper-table-btns">
    <tbody>
    <tr>
        <td style="width:520px;">
            <input id="dosubmit" type="submit" name="dosubmit" value="更新信息" class="btn btn-danger btn-sm btn-block" />
        </td>
        <td class="cms-note"></td>
    </tr>
    </tbody>
</table>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() { yhcms.common.dosubmit(); });
-->
</script>
</body>
</html>