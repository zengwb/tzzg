<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: IndexInitializeView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title><?php echo ($site["name"]); ?>内容管理系统</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="main-body">
<div class="main-head">
    <div class="main-head-logo">
        <img src="/Resources/Apps/Skin/Image/Admin/Common/logo.png" title="LOGO" alt="LOGO" />
    </div>
    <ul class="main-navi">
        <li data-link="<?php echo U('bootstrap');?>" class="current"><i class="iconfont icon-mokuai"></i></li>
        <?php if(is_array($navi)): $i = 0; $__LIST__ = $navi;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><li data-menu="<?php echo ($row["menuid"]); ?>" class="" title="<?php echo ($row["description"]); ?>"><?php echo ($row["name"]); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
    </ul>
    <div class="main-site">
        <div><a href="<?php echo ($site["domain"]); ?>" target="_blank" title="<?php echo ($site["domain"]); ?>"><?php echo ($site["name"]); ?></a></div>
        <ul data-ajax="<?php echo U('tabsite');?>">
            <ol>站点切换</ol>
            <?php if(is_array($sites)): $i = 0; $__LIST__ = $sites;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><li data-siteid="<?php echo ($row["siteid"]); ?>" <?php if($row['siteid'] == $site['siteid']): ?>class="current"<?php endif; ?> title="<?php echo ($row["domain"]); ?>"><?php echo ($row["name"]); ?></li><?php endforeach; endif; else: echo "" ;endif; ?>
        </ul>
    </div>
    <div class="main-user">
        <div>
            <p class="main-user-avatar">
                <img src="<?php if($data['avater'] && file_exists($data['avater'])): echo ($data["avater"]); else: ?>/Resources/Apps/Skin/Temp/Admin/avatar.png<?php endif; ?>" title="<?php echo ($data["username"]); ?>" alt="<?php echo ($data["username"]); ?>" />
            </p>
            <span>您好，<?php echo ($data["username"]); ?></span>
        </div>
        <ul class="main-user-handle">
            <li>
                <h2>授权信息<span><a href="javascript:void(0);">许可协议</a></span></h2>
                <p class="noa">
                    授权单位：<?php echo C('CMS_LICENSE');?><br />
                    证书编号：<?php echo C('CMS_CMSCODE');?>
                </p>
            </li>
            <li>
                <h2>登录信息<span><a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('Admin/password');?>', '修改密码', 'AdminAdminPassword-0-380-130');">修改密码</a></span></h2>
                <p class="noa">
                    登录时间：<?php echo (date("Y/m/d H:i:s", $data["logintime"])); ?><br />
                    登录ＩＰ：<?php echo ($data["loginip"]); ?>（<?php echo ($look["country"]); ?>·<?php echo ($look["city"]); ?>）
                </p>
            </li>
            <li>
                <h2>常规操作<span><a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('Panel/index');?>', '快捷导航', 'AdminPanelIndex-0-540-380');">添加菜单</a></span></h2>
                <p id="">
                    <a href="<?php echo ($site["domain"]); ?>" target="_blank" class="oper">网站首页</a>
                    <a href="javascript:void(0);" class="oper">商家管理</a>
                    <a href="<?php echo ($site["domain"]); echo U('Member/Index/index');?>" target="_blank" class="oper">用户管理</a>
                    <a href="<?php echo C('ADMIN_URL'); echo U('logout');?>" class="oper">注销登录</a>
                    <?php if(is_array($tabs)): $i = 0; $__LIST__ = $tabs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo ($row["link"]); ?>', 1);"><?php echo ($row["name"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
                </p>
            </li>
        </ul>
    </div>
</div>
<div class="main-menu">
    <div class="main-menu-tips"></div>
    <form id="frmCms" name="frmCms" action="<?php echo C('CMS_ADMIN_HELPER');?>" method="post" target="main" class="main-help">
        <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" autocomplete="off" placeholder="使用帮助" />
    </form>
    <?php if(is_array($menu)): $i = 0; $__LIST__ = $menu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><div id="main-menu-body" data-menu="<?php echo ($row["menuid"]); ?>" style="display:none;">
        <?php if(is_array($row["submenu"])): foreach($row["submenu"] as $key=>$val): ?><div class="main-menu-group">
            <span><i class="iconfont icon-liebiao"></i></span>
            <span><?php echo ($val["name"]); ?></span>
            <span><i class="iconfont icon-shousuo"></i></span>
        </div>
        <ul class="main-menu-list">
            <?php if(is_array($val["submenu"])): foreach($val["submenu"] as $key=>$act): preg_match('/(\[.*\])/', $act['url'], $url); $link = strtr($url[1], ['[' => '', ']' => '', '.' => '/']); if ($url[1]) { $act['url'] = strtr($act['url'], [$url[1] => U($link)]); } else { $act['url'] = $act['url']; } ?>
            <li data-link="<?php echo (cms_stripslashes($act["url"])); ?>">
                <i class="iconfont icon-jiantou"></i>
                <span><?php echo ($act["name"]); ?></span>
            </li><?php endforeach; endif; ?>
        </ul><?php endforeach; endif; ?>
    </div><?php endforeach; endif; else: echo "" ;endif; ?>
    <div class="main-menu-foot"></div>
</div>
<div class="main-oper">
    <div class="main-oper-tips">
        <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('index/initialize');?>');" target="_top">
            <i class="iconfont icon-zhuye"></i>
            <span>主页</span>
        </a>
        <a href="javascript:history.go(-1);">
            <i class="iconfont icon-fanhui"></i>
            <span>返回上一页</span>
        </a>
    </div>
    <iframe id="main" frameborder="0" src="<?php echo U('index/bootstrap');?>" name="main" class="main-frame"></iframe>
</div>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() { yhcms.admin.main().reszie(); });
$(window).resize(function() { yhcms.admin.main().reszie(); });
window.onload = function() {
    window.onresize = resize;
    function resize() { yhcms.admin.main().reszie(); }
};
-->
</script>
</body>
</html>