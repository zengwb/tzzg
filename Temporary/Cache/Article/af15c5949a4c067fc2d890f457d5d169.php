<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ContentActionView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title><?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>内容</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/colorpicker.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="winx-body">
<div class="winx-page">
    <div class="location">
        <i class="iconfont icon-zhuye"></i><span>主页</span>
        <i class="iconfont icon-fengefu"></i><span>内容管理</span>
        <i class="iconfont icon-fengefu"></i><span><?php echo ($name); ?></span>
        <i class="iconfont icon-fengefu"></i>
        <span><?php if(ACTION_NAME == 'add'): ?>添加<?php else: ?>修改<?php endif; ?>内容</span>
    </div>
    <form id="frmAct" name="frmAct" action="<?php echo U(ACTION_NAME);?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="info[catid]" value="<?php echo ($catid); ?>" />
        <input type="hidden" name="info[mid]" value="<?php echo ($main["mid"]); ?>" />
        <input type="hidden" name="info[title]" value="<?php echo ($main["title"]); ?>" />
        <div style="float:left; padding:8px 0px; width:724px; border:solid 1px #e0e0e0; border-top-color:#ccc; background:#fff;">
        <table class="table table-condensed table-bordered list-table-form winx-editor winx-content" style="position:relative">
            <tbody>
            <?php if(is_array($tagL)): $i = 0; $__LIST__ = $tagL;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
                <th style="width:114px;">
                    <i class="iconfont icon-xinghao <?php echo ($row["th2style"]); ?>"></i><?php echo ($row["name"]); ?>
                </th>
                <td <?php if($row['formtype'] == 'editor'): ?>class="editor"<?php endif; ?>><?php echo ($row["element"]); ?></td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        </div>
        <div style="float:right; padding:8px 0px 0px; width:244px; border:solid 1px #e0e0e0; border-top-color:#ccc; background:#fff;">
        <table class="table table-condensed table-bordered list-table-form winx-editor winx-content winx-content-right" style="position:relative">
            <tbody>
            <?php if(is_array($tagR)): $i = 0; $__LIST__ = $tagR;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
                <th style="text-align:left;">
                    <?php echo ($row["name"]); ?><i class="iconfont icon-xinghao <?php echo ($row["th2style"]); ?>"></i>
                </th>
            </tr>
            <tr>
                <td><?php echo ($row["element"]); ?></td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        </div>
        <div class="winx-bottom"></div>
        <div class="winx-bootom-fixed" <?php if($type == 9 ): ?>style="display:none"<?php endif; ?> >
            <div class="btn-group" role="group" aria-label="功能菜单">
                <input id="dosubmit" type="submit" name="dosubmit" class="btn btn-danger" value="保存后自动关闭" />
            </div>
        </div>
    </form>
</div>
</body>
</html>