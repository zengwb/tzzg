<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ContentIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>内容管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body>
<div class="main-category">
    <iframe id="category" frameborder="0" src="<?php echo U('category', ['modelid' => $modelid, 'catid' => $catid]);?>" name="category"></iframe>
</div>
<div class="main-content">
    <iframe id="content" frameborder="0" src="<?php echo U('lists', ['modelid' => $modelid, 'catid' => $catid]);?>" name="content"></iframe>
</div>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() { $("#category").css({"height": ($(window).outerHeight(true)-4)}); });
$(window).resize(function() { $("#category").css({"height": ($(window).outerHeight(true)-4)}); });
-->
</script>
</body>
</html>