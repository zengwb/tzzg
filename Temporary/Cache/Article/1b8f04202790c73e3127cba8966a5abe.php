<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ContentCategoryView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>栏目列表</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<style type="text/css">
<!--
.list-body { padding:6px 8px 12px; }
-->
</style>
</head>
<body class="list-body">
<ul class="list-category"><?php if($catetree): echo ($catetree); else: ?>暂无栏目！<?php endif; ?></ul>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    $("ul.list-category>li[class!='category']").click(function() {
        $("ul.list-category>li[class!='category']").removeClass("current");
        $(this).addClass("current");
        var link =$(this).find("a").attr("data-link");
        parent.frames["content"].location.href = link;
    });
});
-->
</script>
</body>
</html>