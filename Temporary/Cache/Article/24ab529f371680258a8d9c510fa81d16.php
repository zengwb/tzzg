<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: TypeIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>分类管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">分类管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('add');?>', '添加分类', 'ArticleTypeAdd-0-540-220');" class="btn btn-default btn-sm">添加分类</a>
    <h3 class="btn btn-sm tips-head">您可以对【内容分类】进行管理，如增/删/改/查，显示排序操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索分类</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th class="list-listorder">排序</th>
        <th style="width:160px;">分类名称</th>
        <th>分类描述</th>
        <th class="cms-tc" style="width:120px;">隶属栏目</th>
        <th class="cms-tc" style="width:120px;">隶属模型</th>
        <th class="cms-tc" style="width:120px;">隶属模块</th>
        <th class="cms-tc" style="width:86px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="9">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
        <th class="list-checkbox">
            <input class="checkchild" name="info[typeid][]" value="<?php echo ($row[typeid]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($row[typeid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[typeid][]" value="<?php echo ($row[typeid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td data-title="<?php echo ($row["name"]); ?>"><a><?php echo ($row["title"]); ?></a></td>
        <td class="cms-c999"><?php echo ($row["description"]); ?></td>
        <td class="cms-tc"><?php echo ($row["catname"]); ?></td>
        <td class="cms-tc"><?php echo ($row["model"]); ?></td>
        <td class="cms-tc"><?php echo ($row["module"]); ?></td>
        <td class="cms-tc">
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.topwin('<?php echo U('edit', ['typeid' => $row['typeid']]);?>', '修改『<?php echo ($row['name']); ?>』分类', 'ArticleTypeEdit-0-540-220');" title="修改分类">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['typeid' => $row['typeid']]);?>', '确认删除【<?php echo ($row['name']); ?>】内容分类！');" title="删除分类">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的分类！');" class="btn btn-danger btn-sm">删除分类</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        });
        list.dblclick(function() {
            var typeid = $(this).find("td:eq(0)").html(),
                name = $(this).find("td:eq(2)").attr("data-title"),
                link = "<?php echo U('edit', ['typeid' => '']);?>" + typeid;
            
            if (typeid) yhcms.dialog.topwin(link, "修改『"+name+"』分类", "ArticleTypeEdit-0-540-220");
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>