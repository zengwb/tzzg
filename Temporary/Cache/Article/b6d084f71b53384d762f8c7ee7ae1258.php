<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
    <meta name="author" content="$Id: ApproveIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
    <meta name="copyright" content="" />
    <title>用户管理</title>
    <link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
    <link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />
    
    <link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
    <div class="list-tips">
        <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">审核列表</a>
    <select id="model_type">
        <option value="all">全部</option>
        <?php if(is_array($mould_type)): $i = 0; $__LIST__ = $mould_type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($key); ?>" <?php if($table == $key): ?>selected<?php endif; ?>  ><?php echo ($row); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
    </select>
            <h3 class="btn btn-sm tips-head">选择对应模块进行审批操作，查看详情！</h3>
            <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">标题搜索</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U(ACTION_NAME,['table'=>'all']);?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
        <hr />
    </div>
    <div class="table-responsive">
        <table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
            <thead>
            <tr >
                <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
                <th class="list-small">申请ID</th>
                <th style="width:100px;">申请人</th>
                <th class="cms-tc" style="width:100px; text-align:center;">模板名称</th>
                <th>标题</th>
                <th class="cms-tc" style="width:140px;">创建时间</th>
                <th class="cms-tc" style="width: 100px">状态</th>
                <th class="cms-tc" style="width:150px;">管理操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!$data): endif; ?>
            <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if(!$row['state']): ?>class="display"<?php endif; ?>>
                <th class="list-checkbox">
                    <input class="checkchild" name="info[apply_id][]" value="<?php echo ($row[apply_id]); ?>" type="checkbox" <?php if($row['state'] != 1): ?>disabled<?php endif; ?> />
                </th>
                <td class="list-small"><?php echo ($row[apply_id]); ?></td>
                <td class="list-small"><?php echo ($row[proposer_name2]); ?></td>
                <td class="list-small"><?php echo ($row[mould_name]); ?></td>
                <td><?php echo ($row["title2"]); ?></td>
                <td class="cms-tc"><?php echo (date("Y-m-d H:i:s",$row["date_time"])); ?></td>
                <td class="cms-tc icon-color">
                    <?php if(!$row['state']): ?>已驳回<?php endif; ?>
                    <?php if($row['state'] == 1): ?>申请中<?php endif; ?>
                    <?php if($row['state'] == 2): ?>已通过<?php endif; ?>
                </td>
                <td>
                    <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('more', ['apply_id' => $row['apply_id']]);?>');" title="查看详情">查看详情</a>
                    <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('all_move', ['apply_id' => $row['apply_id'],'fruit' => 2]);?>', '确认【<?php echo ($row['title']); ?>】的申请直接通过审核！');" title="同意"   <?php if((!$row['fly_state'] and !$state) or $row['state'] != 1): ?>style="display: none"<?php endif; ?>  >同意</a>
                    <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('all_move', ['apply_id' => $row['apply_id'],'fruit' => 0]);?>', '确认申请人【<?php echo ($row['title']); ?>】的申请直接驳回！');" title="驳回"   <?php if((!$row['fly_state'] and !$state) or $row['state'] != 1): ?>style="display: none"<?php endif; ?>  >驳回</a>
                </td>
                </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
    </div>
    </div>
    <div class="list-foot" <?php if(!$state): ?>style="display: none"<?php endif; ?>>
            <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('all_move', ['fruit' => 2]);?>', '确认选中的用户通过审核！');" class="btn btn-danger btn-sm">一键同意</button>
    </div>

            <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('all_move', ['fruit' => 0]);?>', '确认选中的用户通过审核！');" class="btn btn-danger btn-sm">一键驳回</button>
    </div>
    <div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
    </div>
    
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">

    $(function() {
        var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), mould_id = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {mould_id: mould_id}, function(data) {
                if (data == 0) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "点击禁用帐号");
                } else if (data == 1) {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "点击启用帐号");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        }).dblclick(function() {
            var apply_id = $(this).find("td:eq(0)").html(),
                    name = $(this).find("td:eq(1)").find("a").html(),
                    url = "<?php echo U('more', ['apply_id' => '']);?>" + apply_id;

            if (apply_id) yhcms.common.linkurl(url);//双击跳转
        });


        yhcms.common.dosubmit().checkall();
        yhcms.admin.footnote();


        //下拉选择
        $('#model_type').change(function(){
            url = "<?php echo U('index', ['table' => '']);?>" + $("#model_type").val();
            yhcms.common.linkurl(url);
        })


    });

</script>
</body>
</html>