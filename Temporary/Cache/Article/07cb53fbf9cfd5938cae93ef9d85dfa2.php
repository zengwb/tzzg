<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ContentRelationView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>内容管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
<style type="text/css">
.list-body { padding:14px 16px; }
.dropdown-menu { height:240px; overflow:auto; }
.dropdown-menu > li { font-size:12px; cursor:pointer; }
.dropdown-menu > li > a { padding:2px 12px; }
</style>
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<input type="hidden" name="modelid" value="<?php echo ($modelid); ?>" />
<!--<input type="hidden" name="catid" value="<?php echo ($catid); ?>" />-->
<input type="hidden" name="id" value="<?php echo ($id); ?>" />
<div class="input-group">
    <div class="input-group-btn">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <a><?php echo ((isset($catname) && ($catname !== ""))?($catname):'选择栏目'); ?></a>
            <span class="caret"></span>
        </button>
        <input id="catid" type="hidden" name="catid" value="<?php echo ($catid); ?>" />
        <?php if ($cattree) { ?>
        <ul class="dropdown-menu"><?php echo ($cattree); ?></ul>
        <?php } ?>
    </div>
    <input id="frmKey" type="text" name="key" value="" class="form-control" placeholder="内容关键字！">
    <span class="input-group-btn">
        <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U(ACTION_NAME);?>');"
            class="btn btn-danger">
            搜索内容
        </button>
    </span>
</div>
<div class="table-responsive" style="margin-top:10px;">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
        <tr>
            <th class="list-checkbox">
                <input id="checkall" type="checkbox" name="checkall" value="off" disabled />
            </th>
            <th class="list-big">ID</th>
            <th>标题</th>
        </tr>
    </thead>
    <tbody>
        <?php if(!$data): ?><tr><td colspan="3">暂无内容！</td></tr><?php endif; ?>
        <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr>
            <th class='list-checkbox'>
                <input class='checkchild' type='checkbox' name='info[mid][]' value='<?php echo ($row["mid"]); ?>' disabled />
            </th>
            <td class='list-big'><?php echo ($row["mid"]); ?></td>
            <td data-catid="<?php echo ($row["catid"]); ?>" data-id="<?php echo ($row["mid"]); ?>">
                <a href="<?php echo ($row["links"]); ?>" title="<?php echo ($row["title"]); ?>" target="_blank" style="float:left; color:<?php echo ($row['style']['color']); ?>; font-weight:<?php echo ($row['style']['bold']); ?>;"><?php echo ($row["title2"]); ?></a>
                <?php if($row['thumb'] != ''): ?><i class="icon-style icon-style-image" title="图文"></i><?php endif; ?>
                <?php if($row['posiids'] != ''): ?><i class="icon-style icon-style-recommend" title="推荐"></i><?php endif; ?>
                <?php if($row['url'] != ''): ?><i class="icon-style icon-style-link" title="链接"></i><?php endif; ?>
            </td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script language="javascript" type="text/javascript">
<!--
$(function() {
    $("ul.dropdown-menu>li").click(function() {
        $("button.dropdown-toggle>a").html($(this).find("a>span").html());
        $("#catid").val($(this).attr("data-catid"));
    });
    
    var list = $("table.list-table-body>tbody>tr");
        list.click(function() {
            var style   = $(this).attr("class"),
                catid   = $(this).find("td:eq(1)").attr("data-catid"),
                mid     = $(this).find("td:eq(1)").attr("data-id"),
                title   = $(this).find("td:eq(1)>a").attr("title");
                
            var val     = $(window.parent.document).find("#relationVal"),
                box     = $(window.parent.document).find("#relationBox"),
                txt     = "<span data-id=\""+mid+"\">"+title+"<a href=\"javascript:void(0);\" onClick=\"javascript:delete_relation_mid("+mid+");\">删除</a></span>";
            
            if (title  == undefined) return ;
            if (style  == "current") {
                $(this).removeClass("current");
                //box.html(box.html().replace(txt, ""));
                box.find("span[data-id='"+mid+"']").remove();
                //val.val(val.val().replace(mid, "").replace(",,", ","));
                ids     = val.val();
                arr     = ids.split(",");
                arr.splice($.inArray(mid, arr), 1);
                val.val(arr.join(","));
            } else {
                $(this).addClass("current");
                box.append(txt);
                val.val(val.val() ? val.val() + "," + mid : mid);
            }
        });
        
    var value = $(window.parent.document).find("#relationVal").val();
        array = value.split(",");
        
        list.removeClass("current");
        $.each(array, function(i, mid) {
            if (!mid) return ;
            list.each(function() {
                var contentid = $(this).find("td").eq(1).attr("data-id");
                if (contentid == mid) $(this).addClass("current");
            });
        });
        
    yhcms.common.dosubmit();
});
-->
</script>
</body>
</html>