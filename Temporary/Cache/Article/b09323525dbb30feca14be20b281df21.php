<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: ContentListsView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>内容管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<?php if(!$catid): ?><style type="text/css">
.dropdown-menu { }
.dropdown-menu > li { cursor:pointer; font-size:12px; }
.dropdown-menu > li > a { padding:3px 6px; }
</style><?php endif; ?>
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U('lists');?>" method="post">
<input type="hidden" name="info[catid]" value="<?php echo ($catid); ?>" />
<input type="hidden" name="info[modelid]" value="<?php echo ($modelid); ?>" />
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm"><?php echo ($name); ?></a><!-- onClick="javascript:yhcms.common.linkurl('<?php echo U('lists', ['catid' => $catid]);?>');"-->
    <?php if($catid): ?><!--<?php if($modelid == 7): ?><a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.openx('<?php echo U('add', ['catid' => $catid, 'rand' => '']);?>'+Math.random(), '添加项目');" class="btn btn-default btn-sm">添加项目</a>
        <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('import', ['catid' => $catid]);?>', '导入项目', 'ArticleContentImport-0-480-180');" class="btn btn-default btn-sm">导入项目</a>
        <?php else: ?>
        <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.openx('<?php echo U('add', ['catid' => $catid, 'rand' => '']);?>'+Math.random(), '添加内容');" class="btn btn-default btn-sm">添加内容</a><?php endif; ?>-->
        <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.openx('<?php echo U('add', ['catid' => $catid, 'rand' => '']);?>'+Math.random(), '添加内容');" class="btn btn-default btn-sm">添加内容</a>
        <?php if($modelid == 37): ?><a href="javascript:void(0);" role="button" onClick="javascript:yhcms.dialog.topwin('<?php echo U('import_content37', ['catid' => $catid]);?>', '导入内容', 'ArticleContentImportContent-0-480-180');" class="btn btn-default btn-sm">导入内容</a><?php endif; ?>
    <?php else: ?>
    <a href="<?php echo ($links); ?>" role="button" class="btn btn-default btn-sm" target="_blank">访问站点</a><?php endif; ?>
    <h3 class="btn btn-sm tips-head">您可以对【<?php echo ($name); ?>】内容进行管理，如增/删/改/查，显示排序操作！</h3>
    <?php if($catid): ?><div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索内容</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('lists');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <?php else: ?>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <a><?php echo ((isset($catname) && ($catname !== ""))?($catname):'选择栏目'); ?></a>
                    <span class="caret"></span>
                </button>
                <input id="catid" type="hidden" name="catid" value="" required />
                <?php if ($cattree) { ?>
                <ul class="dropdown-menu"><?php echo ($cattree); ?></ul>
                <?php } ?>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('lists');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div><?php endif; ?>
    <hr />
</div>
<div class="table-responsive">
<?php if($modelid == 7): ?><table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
        <thead>
        <tr>
            <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
            <th class="list-big">ID</th>
            <th class="list-listorder">排序</th>
            <th>项目名称</th>
            <th class="cms-tl">业主单位</th>
            <th class="cms-tc" style="width:100px;">总投资额</th>
            <th class="cms-tc" style="width:80px;">发布人</th>
            <th class="cms-tc" style="width:140px;">发布时间</th>
            <th class="list-big">状态</th>
            <th class="cms-tc" style="width:112px;">管理操作</th>
        </tr>
        </thead>
        <tbody>
    <?php if(!$data): ?><tr><td colspan="10">暂无项目！</td></tr><?php endif; ?>
        <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
            <th class="list-checkbox">
                <input class="checkchild" name="info[mid][]" value="<?php echo ($row["mid"]); ?>" type="checkbox" />
            </th>
            <td class="list-big"><?php echo ($row["mid"]); ?></td>
            <td class="list-listorder">
                <input type="hidden" name="data[mid][]" value="<?php echo ($row[mid]); ?>" />
                <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
            </td>
            <td data-catid="<?php echo ($row["catid"]); ?>" data-id="<?php echo ($row["mid"]); ?>">
                <a href="<?php echo ($row["links"]); ?>" target="_blank" style="float:left; color:<?php echo ($row['style']['color']); ?>; font-weight:<?php echo ($row['style']['bold']); ?>;"><?php echo ($row["title2"]); ?></a>
                <?php if($row['thumb'] != ''): ?><i class="icon-style icon-style-image" title="图文"></i><?php endif; ?>
                <?php if($row['posiids'] != ''): ?><i class="icon-style icon-style-recommend" title="推荐"></i><?php endif; ?>
                <?php if($row['url'] != ''): ?><i class="icon-style icon-style-link" title="链接"></i><?php endif; ?>
            </td>
            <td class="cms-tl"><?php echo ($row["company"]); ?></td>
            <td class="cms-tc"><?php echo ($row["investment"]); ?> 亿元</td>
            <td class="cms-tc"><?php echo ($row["realname"]); ?></td>
            <td class="cms-tc"><?php echo (date("Y-m-d H:i:s",$row["inputtime"])); ?></td>
            <td class="cms-tc icon-color">
            <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[mid]); ?>" href="javascript:void(0);" title="点击审核内容">
                <i class="iconfont icon-qingchu"></i>
            </a>
            <?php else: ?>
                <a class="list-operation" data-state="<?php echo ($row[mid]); ?>" href="javascript:void(0);" title="点击初始内容">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
            </td>
            <td class="cms-tc">
                <!--<a href="<?php echo ($row["links"]); ?>" target="_blank" title="浏览内容">浏览</a>-->
                <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.openx('<?php echo U('edit', ['catid' => $row['catid'], 'id' => $row['mid']]);?>', '修改内容');" title="修改内容">修改</a>
                <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['catid' => $row['catid'], 'id' => $row['mid']]);?>', '确认删除：<?php echo ($row['title']); ?>？');" title="删除内容">删除</a>
            </td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table>
<?php else: ?>
    <table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
        <thead>
        <tr>
            <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
            <th class="list-big">ID</th>
            <th class="list-listorder">排序</th>
            <th>标题</th>
            <th class="cms-tc" style="width:100px;">访问量</th>
            <th class="cms-tc" style="width:80px;">发布人</th>
            <th class="cms-tc" style="width:140px;">更新时间</th>
            <th class="list-big">状态</th>
            <th class="cms-tc" style="width:112px;">管理操作</th>
        </tr>
        </thead>
        <tbody>
    <?php if(!$data): ?><tr><td colspan="9">暂无内容！</td></tr><?php endif; ?>
        <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
            <th class="list-checkbox">
                <input class="checkchild" name="info[mid][]" value="<?php echo ($row["mid"]); ?>" type="checkbox" />
            </th>
            <td class="list-big"><?php echo ($row["mid"]); ?></td>
            <td class="list-listorder">
                <input type="hidden" name="data[mid][]" value="<?php echo ($row[mid]); ?>" />
                <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
            </td>
            <td data-catid="<?php echo ($row["catid"]); ?>" data-id="<?php echo ($row["mid"]); ?>">
                <a href="<?php echo ($row["links"]); ?>" target="_blank" style="float:left; color:<?php echo ($row['style']['color']); ?>; font-weight:<?php echo ($row['style']['bold']); ?>;"><?php echo ($row["title2"]); ?></a>
                <?php if($row['thumb'] != ''): ?><i class="icon-style icon-style-image" title="图文"></i><?php endif; ?>
                <?php if($row['posiids'] != ''): ?><i class="icon-style icon-style-recommend" title="推荐"></i><?php endif; ?>
                <?php if($row['url'] != ''): ?><i class="icon-style icon-style-link" title="链接"></i><?php endif; ?>
            </td>
            <td class="cms-tc"><?php echo ($row["views"]); ?></td>
            <td class="cms-tc"><?php echo ($row["realname"]); ?></td>
            <td class="cms-tc"><?php echo (date("Y-m-d H:i:s",$row["updatetime"])); ?></td>
            <td class="cms-tc icon-color">
            <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[mid]); ?>" href="javascript:void(0);" title="点击审核内容">
                <i class="iconfont icon-qingchu"></i>
            </a>
            <?php else: ?>
                <a class="list-operation" data-state="<?php echo ($row[mid]); ?>" href="javascript:void(0);" title="点击初始内容">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
            </td>
            <td class="cms-tc">
                <a href="<?php echo ($row["links"]); ?>" target="_blank" title="浏览内容">浏览</a>
                <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.openx('<?php echo U('edit', ['catid' => $row['catid'], 'id' => $row['mid']]);?>', '修改内容');" title="修改内容">修改</a>
                <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['catid' => $row['catid'], 'id' => $row['mid']]);?>', '确认删除：<?php echo ($row['title']); ?>？');" title="删除内容">删除</a>
            </td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        </tbody>
    </table><?php endif; ?>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的内容！');" class="btn btn-danger btn-sm">删除内容</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <?php if($catid): ?><div class="btn-group" role="group" aria-label="功能菜单" style="width:156px;">
    <div class="input-group">
        <div class="input-group-addon addon-sm">推送至</div>
        <select id="posiid" class="form-control input-sm" name="data[posiid]">
            <option value="">选择推送！</option>
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><option value="<?php echo ($row["posiid"]); ?>"><?php echo ($row["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
        </select>
    </div>
    </div><?php endif; ?>
<!--    <h3 class="btn btn-sm tips-head">[note]</h3>-->
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    <?php if(!$catid): ?>$("ul.dropdown-menu>li").click(function() {
        $("button.dropdown-toggle>a").html($(this).find("a>span").html());
        $("#catid").val($(this).attr("data-catid"));
    });<?php endif; ?>
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), catid = $(this).parent().parent().find("td:eq(2)").attr("data-catid"),
                id = $(this).parent().parent().find("td:eq(2)").attr("data-id");
            
            $.getJSON("<?php echo U('state');?>", {catid: catid, id: id}, function(data) {
                if (data == 1) {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "单击审核内容");
                }
                if (data == 0) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "单击初始内容");
                }
            });
        });
        list.mousedown(function(e) {
            if (3 == e.which)
                $(this).find("th>input.checkchild").trigger("click");
        });
        list.dblclick(function() {
            var catid = $(this).find("td:eq(2)").attr("data-catid"),
                id    = $(this).find("td:eq(2)").attr("data-id"),
                links = "<?php echo U('edit', ['catid' => 'catidval', 'id' => 'idval', 'rand' => '']);?>"+Math.random();
            
            if (catid && id) {
                links = links.replace("catidval", catid).replace("idval", id);
                yhcms.dialog.openx(links, "编辑内容");
            }
        });
    
    $("#posiid").change(function() {
        if ($(this).val() == "") return ;
        yhcms.dialog.frmtips("#frmList", "<?php echo U('position');?>", "确认推送选中的内容！");
    });
    
    yhcms.common.dosubmit().checkall().listorder();
    // yhcms.admin.footnote();
});
-->
</script>
</body>
</html>