<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: PositionIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>推送管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">推荐管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('add');?>');" class="btn btn-default btn-sm">添加推送</a>
    <h3 class="btn btn-sm tips-head">您可以对【推送位置】进行管理，如增/删/改/查，显示排序、设置状态操作！</h3>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索推送</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-big">ID</th>
        <th class="list-listorder">排序</th>
        <th class="cms-tc" style="width:120px;">所属模块</th>
        <th class="cms-tc" style="width:120px;">所属模型</th>
        <th>推送名称（数量）</th>
        <th class="cms-tc" style="width:120px;">所属栏目</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:132px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="9">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if(!$row['status']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" name="info[posiid][]" value="<?php echo ($row[posiid]); ?>" type="checkbox" />
        </th>
        <td class="list-big"><?php echo ($row[posiid]); ?></td>
        <td class="list-listorder">
            <input type="hidden" name="data[posiid][]" value="<?php echo ($row[posiid]); ?>" />
            <input type="text" name="data[listorder][]" value="<?php echo ($row[listorder]); ?>" maxlength="4" autocomplete="off" class="form-control input-sm list-input-listorder" />
        </td>
        <td class="cms-tc"><?php echo ($row["module"]); ?></td>
        <td class="cms-tc"><?php echo ($row["model"]); ?></td>
        <td data-title="<?php echo ($row["name"]); ?>"><a><?php echo ($row["title"]); ?></a>（<?php echo ($row["count"]); ?>/<?php echo ($row["number"]); ?>）</td>
        <td class="cms-tc"><?php echo ($row["catname"]); ?></td>
        <td class="cms-tc icon-color">
        <?php if(!$row['status']): ?><a class="list-operation" data-state="<?php echo ($row[posiid]); ?>" href="javascript:void(0);" title="点击启用推送">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[posiid]); ?>" href="javascript:void(0);" title="点击禁用推送">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tr">
            <?php if($row['thumb'] != ''): ?><a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('<?php echo U('image', ['thumb' => $row['thumb']]);?>', '查看【<?php echo ($row['name']); ?>】图例', 'ArticlePositionThumb-640-480');" title="查看图例">图例</a><?php endif; ?>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('PositionData/index', ['posiid' => $row['posiid']]);?>');" title="推送内容">内容</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('edit', ['posiid' => $row['posiid']]);?>');" title="修改推送">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['posiid' => $row['posiid']]);?>', '确认删除【<?php echo ($row['name']); ?>】推送位置！');" title="删除推送">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的推送！');" class="btn btn-danger btn-sm">删除推送</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('listorder');?>', '确认更新排序！')" class="btn btn-default btn-sm">显示排序</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页"><?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), posiid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {posiid: posiid}, function(data) {
                if (data == 1) {
                    current.parent().parent().removeClass("display");
                    current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                    current.attr("title", "单击禁用推送");
                }
                if (data == 0) {
                    current.parent().parent().addClass("display");
                    current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                    current.attr("title", "单击启用推送");
                }
            });
        });
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        });
        list.dblclick(function() {
            var posiid = $(this).find("td:eq(0)").html(),
                link = "<?php echo U('edit', ['posiid' => '']);?>" + posiid;
            
            if (posiid) yhcms.common.linkurl(link);
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>