<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html lang="zh-cn">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, Chrome=1" />
<meta name="author" content="$Id: SpaceIndexView.html 8 2018-01-31 11:11:01Z z.weibing $" />
<meta name="copyright" content="" />
<title>位置管理</title>
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/bootstrap-3.3.0/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Plug-in/dialog/dialog.css" />
<link rel="stylesheet" type="text/css" href="/Resources/Apps/Skin/Css/yhcms.min.css" />

<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
</head>
<body class="list-body">
<form id="frmList" name="frmList" action="<?php echo U(ACTION_NAME);?>" method="post">
<div class="list-tips">
    <a href="javascript:void(0);" role="button" class="btn btn-danger btn-sm">位置管理</a>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('add');?>');" class="btn btn-default btn-sm">添加位置</a>
    <h3 class="btn btn-sm tips-head">您可以对【广告位置】进行管理，如增/删/改/查，设置状态操作！</h3>
    <a href="javascript:void(0);" role="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('Template/index');?>');" class="btn btn-link btn-sm">模板管理</a>
    <div class="tips-help">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle btn-sm">搜索位置</button>
            </div>
            <span>
                <input id="frmKey" type="text" name="key" value="" class="form-control input-sm" placeholder="关键字！">
            </span>
            <span class="input-group-btn">
                <button id="frmSubmit" type="button" onClick="javascript:yhcms.common.submit('#frmList', '<?php echo U('index');?>', 'post');" class="btn btn-danger btn-sm">搜索</button>
            </span>
        </div>
    </div>
    <hr />
</div>
<div class="table-responsive">
<table class="table table-condensed table-bordered table-hover table-striped list-table-form list-table-body">
    <thead>
    <tr>
        <th class="list-checkbox"><input id="checkall" type="checkbox" name="checkall" value="off" /></th>
        <th class="list-small">ID</th>
        <th style="width:180px;">位置名称</th>
        <th>TAG位置描述</th>
        <th class="cms-tc" style="width:120px;">位置模板</th>
        <th class="cms-tc" style="width:100px;">广告尺寸</th>
        <th class="cms-tc" style="width:100px;">广告统计</th>
        <th class="list-big">状态</th>
        <th class="cms-tc" style="width:226px;">管理操作</th>
    </tr>
    </thead>
    <tbody>
<?php if(!$data): ?><tr><td colspan="9">暂无内容！</td></tr><?php endif; ?>
    <?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$row): $mod = ($i % 2 );++$i;?><tr <?php if($row['disabled']): ?>class="display"<?php endif; ?>>
        <th class="list-checkbox">
            <input class="checkchild" name="info[spaceid][]" value="<?php echo ($row[spaceid]); ?>" type="checkbox" />
        </th>
        <td class="list-small"><?php echo ($row[spaceid]); ?></td>
        <td data-title="<?php echo ($row["name"]); ?>">
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('advert/index', ['spaceid' => $row['spaceid']]);?>')" style="float:left;"><?php echo ($row["title"]); ?></a>
            <?php if(!$row['adcount']): ?><i class="icon-style icon-style-null" title="无广告"></i><?php endif; ?>
        </td>
        <td class="cms-c999"><?php echo ($row["description"]); ?></td>
        <td class="cms-tc"><?php echo ($row["template"]); ?></td>
        <td class="cms-tc"><?php echo ($row["size"]); ?></td>
        <td class="cms-tc" title="有效广告/显示数量"><?php echo ($row["adcount"]); ?>/<?php echo ($row["number"]); ?></td>
        <td class="cms-tc icon-color">
        <?php if($row['disabled']): ?><a class="list-operation" data-state="<?php echo ($row[spaceid]); ?>" href="javascript:void(0);" title="点击显示位置">
                <i class="iconfont icon-qingchu"></i>
            </a>
        <?php else: ?>
            <a class="list-operation" data-state="<?php echo ($row[spaceid]); ?>" href="javascript:void(0);" title="点击隐藏位置">
                <i class="iconfont icon-qiyong"></i>
            </a><?php endif; ?>
        </td>
        <td class="cms-tc">
            <a href="<?php echo U('views', ['spaceid' => $row['spaceid']]);?>" target="_blank" title="浏览广告">浏览</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('<?php echo U('call', ['spaceid' => $row['spaceid']]);?>', '调用代码', 'AdvertSpaceCall-520-146');" title="调用代码">调用</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('Advert/add', ['spaceid' => $row['spaceid']]);?>');" title="添加广告">添加</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('Advert/index', ['spaceid' => $row['spaceid']]);?>');" title="广告管理">管理</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('edit', ['spaceid' => $row['spaceid']]);?>');" title="修改位置">修改</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.common.linkurl('<?php echo U('setting', ['spaceid' => $row['spaceid']]);?>');" title="位置配置">配置</a>
            <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.tips('<?php echo U('delete', ['spaceid' => $row['spaceid']]);?>', '确认删除【<?php echo ($row['name']); ?>】广告位置！');" title="删除位置">删除</a>
        </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
    </tbody>
</table>
</div>
<div class="list-foot">
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="yhcms.dialog.frmtips('#frmList', '<?php echo U('delete');?>', '确认删除选中的位置！');" class="btn btn-danger btn-sm">删除位置</button>
    </div>
    <div class="btn-group" role="group" aria-label="功能菜单">
        <button type="button" onClick="javascript:yhcms.common.linkurl('<?php echo U('Template/index');?>');" class="btn btn-default btn-sm">模板管理</button>
    </div>
    <h3 class="btn btn-sm tips-head">[note]</h3>
<div class="btn-group list-page" role="group" aria-label="数据分页">共计：<span class="cms-bold"><?php echo ($nums); ?></span>个广告位　<?php echo ($page); ?></div>
</div>
</form>
<script type="text/javascript" src="/Resources/Plug-in/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/bootstrap-3.3.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/Resources/Plug-in/dialog/dialog.js"></script>
<script type="text/javascript" src="/Resources/Apps/Skin/Js/yhcms.min.js"></script>

<script type="text/javascript" language="javascript">
<!--
$(function() {
    var list = $("table.list-table-body>tbody>tr");
        list.mousedown(function(e) {
            if (e.which == 3) $(this).find("th>input.checkchild").trigger("click");
        });
        list.dblclick(function() {
            var spaceid = $(this).find("td:eq(0)").html(),
                name = $(this).find("td:eq(2)").attr("data-title"),
                link = "<?php echo U('edit', ['spaceid' => '']);?>" + spaceid;
            
            if (spaceid) yhcms.common.linkurl(link);
        });
        list.find("td:eq(-2)>a").click(function() {
            var current = $(this), spaceid = $(this).attr("data-state");
            $.getJSON("<?php echo U('state');?>", {spaceid: spaceid}, function(data) {
                if (data == 0) {
                current.parent().parent().removeClass("display");
                current.find("i").removeClass("icon-qingchu cms-cccc").addClass("icon-qiyong");
                current.attr("title", "点击显示位置");
                } else {
                current.parent().parent().addClass("display");
                current.find("i").removeClass("icon-qiyong").addClass("icon-qingchu cms-cccc");
                current.attr("title", "点击隐藏位置");
                }
            });
        });
    
    yhcms.common.dosubmit().checkall().listorder();
    yhcms.admin.footnote();
});
-->
</script>
</body>
</html>