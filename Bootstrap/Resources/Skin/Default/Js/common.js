
;
$(function() { mjcms.initialize.bind(); });

var mjcms = { initialize: {}, common:{}, index: {}, list: {}, show: {}, page: {}, search: {} };

mjcms.initialize.bind = function() {
    var _browser =  navigator.appVersion,
        _ie8     =  false, // _browser.search(/MSIE 8/i) != -1,
        _ie7     = _browser.search(/MSIE 7/i) != -1,
        _ie6     = _browser.search(/MSIE 6/i) != -1;
    
    if (_ie8 || _ie7 || _ie6) {
        window.location.href = "/ieupdate.html"
    }
    // $(document).bind("contextmenu", function(e) { return false});
    mjcms.common.catid = 0;
    
    $("div.advert").append("<span class=\"note\"></span>");
    
    var itop = $("div.itop>div.container ul");
        itop.find("li").hover(function() {
            // $(this).addClass("current").find("div.box").show();
            $(this).addClass("current").find("div.box").slideDown(100);
        }, function() {
            $(this).removeClass("current").find("div.box").hide();
        });
        
    var top2 = $("a.to-top");
        top2.click(function() { $("html, body").animate({"scrollTop": 0}, 600); });
        
    var note = $("div.advert>span.note");
        note.click(function() {
            return "";
        }).hover(function() {
            var name = $(this).parent().attr("data-space");
            $(this).attr("title", name);
        }, function() {});
        
    var form = $("#frmSearch>input[type='button']");
        form.click(function() {
        var url = $("#frmSearch").attr("action");
        var key = $.trim($("#frmSearch>input[name='key']").val());
        if (key == "") {
            $("#frmSearch>input[name='key']").focus();
            return ;
        }
        switch ($(this).attr("name")) {
            case "btnBaidu":
                url = "http://www.baidu.com/s?wd=";
                break;
            case "btn360":
                url = "http://www.so.com/s?q=";
                break;
            default:
                break;
        }
        if (url != undefined) window.open(url+encodeURIComponent(key));
        });
        
        mjcms.common.search().vote();
        
    return this;
}

mjcms.common.search = function() {
    var frmkey = $("#frmSearch>input#key"),
        prompt = $("ul.search-prompt");
        
    frmkey.hover(function() { $(this).focus(); }, function() {}).focus(function() {
        $(this).addClass("key-focus");
        if ($.trim($(this).val()) == "") return ;
        prompt.find("li").remove();
        prompt.append(mjcms.common.keyword($(this).val()));
        prompt.slideDown(300);
    }).keydown(function() {}).keyup(function() {
        if ($.trim($(this).val())) {
            prompt.find("li").remove();
            prompt.append(mjcms.common.keyword($(this).val()));
            prompt.slideDown(300);
        } else {
            prompt.slideUp(300);
        }
    }).blur(function() {
        $(this).removeClass("key-focus");
        prompt.slideUp(300);
    });
    
    return this;
};

mjcms.common.keyword = function(key) {
    var html  = "";
    
    if ($.trim(key) == "") return html;
    $.ajax({
        type: "get",
        url: "/index.php?m=home&c=index&a=ajax2", // "Resources/Skin/Default/temp/temp-data-0.js",
        data: {catid: 0, key: key},
        dataType: "json",
        async: false,
        success: function(data) {
            $.each(data, function(i, row) {
            html += "<li>";
            html += "<a href=\""+row.link+"\" target=\"_blank\" title=\""+row.title+"\">"+row.title.replace(key,"<font class=\"cf30 bold\">"+key+"</font>")+"</a>";
            html += "</li>";
            });
        },
        error: function(e) {},
    });
    
    return html;
};

mjcms.common.position = function(t_side, t_main, t_menu, tbside) {
    var o_side = $("body>div.main>div.side"),
        o_main = $("body>div.main>div.body"),
        obside = $("body>div.main>div.body>div.side"),
        o_foot = $("div#footer"),
        o_head = $("a.to-top"),
        
        o_menu = $("body>div.main>div.body>div.main>div.main-menu"),
        o_menu_fill = $("body>div.main>div.body>div.main>div.main-menu-fill"),
        
        o_container = $("body>div.main"),
        t_container = o_container.offset().top,
        
        h_side = o_side.outerHeight(true),
        hbside = obside.outerHeight(true),
        h_main = o_main.outerHeight(true),
        h_page = h_side > hbside ? h_side : hbside,
        
        t_side = (t_side || o_side.offset().top) + o_side.outerHeight(true),
        t_main = (t_main || o_main.offset().top) + o_main.outerHeight(true),
        tbside = (tbside || obside.offset().top) + obside.outerHeight(true),
        t_menu = (t_menu || o_menu.offset().top),
        
        w_page = $(window).width(),
        h_page = $(window).height(),
        
        t_roll = 0,
        l_roll = 0,
        t_foot = 0;
        
    $(window).scroll(function() {
        t_foot = o_foot.offset().top - h_page;
        
        t_roll = $(this).scrollTop();
        l_roll = $(this).scrollLeft();
        
        if (t_roll > t_container) {
            o_head.fadeIn(1000).css({"bottom": ((h_page-o_head.outerHeight(true))/2)+"px"});
        } else {
            o_head.fadeOut(800);
        }
        
        if (t_roll > (t_side - h_page)) {
            o_side.css({"position": "fixed", "bottom": "0px"});
            if (t_roll > t_foot) {
                o_side.css({"bottom": (t_roll-t_foot)+"px"});
            }
            if (w_page > 1240) o_side.css({"margin-left": "0px"});
            else o_side.css({"margin-left": (-l_roll)+"px"});
        } else {
            o_side.css({"position": "relative"});
        }
        
        if (t_roll > t_menu) {
            o_menu.css({"position": "fixed", "top": "0px"});
            o_menu_fill.show();
            
            if (w_page > 1240) o_menu.css({"margin-left": "0px"});
            else o_menu.css({"margin-left": (-l_roll)+"px"});
        } else {
            o_menu.css({"position": "relative", "margin-left": "0px"});
            o_menu_fill.hide();
        }
        if (t_roll >= t_foot) o_menu.fadeOut(600);
        else o_menu.fadeIn(600);
        
        if (t_roll > (tbside - h_page)) {
            obside.css({"position": "fixed", "bottom": "0px"});
            if (t_roll > t_foot) {
                obside.css({"bottom": (t_roll-t_foot)+"px"});
            }
            if (w_page > 1240) obside.css({"margin-left": "680px"});
            else obside.css({"margin-left": (680-l_roll)+"px"});
        } else {
            obside.css({"position": "relative", "margin-left": "0px"});
        }
    });
    
    return this;
};

mjcms.common.slide = function(lable, control, time) {
    var sidebar = $(lable),
        control = sidebar.find(control),
        sidebox = sidebar.find("ul"),
        
        prev    = true,
        next    = true,
        timer   = null,
        time    = time || 8000,
        
        count   = sidebox.find("li").length,
        width   = sidebox.find("li").outerWidth(true),
        
        index   = 0,
        left    = 0,
        
        topval  = (sidebar.outerHeight(true)-control.outerHeight(true))/2;
    
    control.css({"top": (topval)+"px"});
    sidebox.css({"width": (count*width)+"px"});
    
    sidebar.hover(function() {
        clearInterval(timer);
        control.fadeIn();
    }, function() {
        timer = setInterval(function() {
            control.find("i:last").trigger("click");
        }, time);
        control.fadeOut();
    }).trigger("mouseleave");
    
    control.find("i:first").click(function() {
        if (!prev) return ;
        prev = false;
        sidebox.find("li:last").clone(true).prependTo(sidebox);
        sidebox.css({"left": (-width)+"px"});
        sidebox.stop(true, false).animate(
            {"left": left+"px"},
            300,
            function() {
                sidebox.find("li:last").remove();
                prev = true;
            }
        );
    });
    control.find("i:last").click(function() {
        if (!next) return ;
        next = false;
        sidebox.stop(true, false).animate(
            {"left": (left-width)+"px"},
            300,
            function() {
                sidebox.find("li:first").clone(true).appendTo(sidebox);
                sidebox.find("li:first").remove();
                sidebox.css({"left": left});
                next = true;
            }
        );
    });
    
    return this;
};

mjcms.common.focus = function() {
    var sidebar = $("div.focus>div"),
        control = sidebar.find("div.control"),
        sidebox = sidebar.find("ul"),
        
        prev    = true,
        next    = true,
        timer   = null,
        time    = 8000,
        
        count   = sidebox.find("li").length,
        width   = sidebox.find("li").outerWidth(true),
        
        index   = 0,
        left    = 0,
        topval  = (sidebar.outerHeight(true)-control.outerHeight(true))/2;
        
    control.css({"top": (topval)+"px"});
    sidebox.css({"margin-left": (-width)+"px", "width": (count*width)+"px"});
    sidebox.find("li:last").clone(true).prependTo(sidebox);
    sidebox.find("li:last").remove();
    
    sidebar.parent().hover(function() {
        clearInterval(timer);
        control.fadeIn();
    }, function() {
        timer = setInterval(function() {
            control.find("i:last").trigger("click");
        }, time);
        control.fadeOut();
    }).trigger("mouseleave");
    
    control.find("i:first").click(function() {
        if (!prev) return ;
        prev = false;
        sidebox.find("li:last").clone(true).prependTo(sidebox);
        sidebox.css({"left": (-width)+"px"});
        sidebox.stop(true, false).animate(
            {"left": left+"px"},
            300,
            function() {
                sidebox.find("li:last").remove();
                prev = true;
            }
        );
    });
    control.find("i:last").click(function() {
        if (!next) return ;
        next = false;
        sidebox.stop(true, false).animate(
            {"left": (left-width)+"px"},
            300,
            function() {
                sidebox.find("li:first").clone(true).appendTo(sidebox);
                sidebox.find("li:first").remove();
                sidebox.css({"left": left});
                next = true;
            }
        );
    });
    
    return this;
};

mjcms.common.vote = function() {
    var frmVote  = $("#frmVote"),
        url      = frmVote.attr("action"),
        voteid   = frmVote.find("input[name='voteid']").val(),
        option   = frmVote.find("input[name='option']"),
        val      = [];
        
    frmVote.find("input[type='button']").click(function() {
        val      = [];
        frmVote.find("input:checked").each(function(i) {
            val[i] = $(this).val();
        });
        if (val.length == 0) {
            alert("请选择投票！");
            return ;
        }
        $.getJSON(url+"vote", {voteid: voteid, option: val}, function(data) {
            if (data) alert("感谢您的参与！");
        });
    });
    option.parent().click(function() {
        var current = $(this);
        
        $.getJSON(url+"number", {voteid: voteid}, function(number) {
            var cnt = frmVote.find("input:checked").length;
            if (cnt > number) {
                current.find("input[name='option']").removeAttr("checked");
                alert("最多支持 "+number+" 项投票！");
            } else {
                current.find("input[name='option']").attr("checked", true);
            }
        });
    });
    
    return this;
};

mjcms.index.category = function() {
    var showmenu = $("div.main-menu>ul>li[class!='list']"),
        moremenu = $("div.main-menu>ul>li[class ='list']"),
        
        hidemenu = moremenu.find("dl"),
        bodymain = $("div.body>div.main"),
        
        mainlist = $(" ul.main-list"),
        mainmore = $("div.main-more"),
        
        l_side   = $("div.main>div.side"),
        r_side   = $("div.main>div.body>div.side"),
        
        tl_side  = l_side.offset().top,
        tr_side  = r_side.offset().top,
        tb_main  = bodymain.offset().top,
        tm_menu  = $("div.main>div.body>div.main>div.main-menu").offset().top,
        
        number   = 16,
        index    =  0,
        count    =  0,
        timer    = null;
        
    showmenu.parent().on("mouseover", "li[class!='list']", function() {
        var showmenu = $("div.main-menu>ul>li[class!='list']");
            if ($(this).attr("class") == "current") { return ; }
            
            showmenu.removeClass("current");
            $(this).addClass("current");
            
            index = $(this).index();
            mjcms.common.catid = $(this).attr("data-catid");
            
            mainmore.attr("data-catid", mjcms.common.catid);
            mainmore.removeClass("main-more-none");
            mainmore.show();
            
            mainlist.find("li").remove();
            mainlist.append(mjcms.index.mainlist(mjcms.common.catid, number));
            
            if (mainlist.find("li").length == 0) {
            mainmore.hide();
            /*mainlist.append(mjcms.index.mainlist(0, number));*/
            }
    });
    showmenu.parent().on("click", "li[class!='list']", function() {
        // alert($(this).attr("data-catid")); // 单击跳转
    });
    moremenu.click(function() {
        if (hidemenu.css("display") != "none") return ;
        var showmenu = $("div.main-menu>ul>li[class!='list']");
            mjcms.common.catid = showmenu.parent().find("li[class!='current']").attr("data-catid");
            showmenu.removeClass("current");
            hidemenu.show();
    });
    
    moremenu.parent().parent().hover(function() {}, function() {
        var showmenu = $("div.main-menu>ul>li[class!='list']");
            showmenu.each(function() {
                if ($(this).attr("data-catid") != mjcms.common.catid) return ;
                showmenu.removeClass("current");
                $(this).addClass("current");
            });
            hidemenu.hide();
    });
    hidemenu.find("dd").click(function() {
        var showmenu = $("div.main-menu>ul>li[class!='list']");
        
        if ($(this).find("i").length > 0) {
            var text = $(this).text();
            $(this).removeClass("select").find("i").remove();
            
            showmenu.each(function() {
				if ($(this).text() !== text) return ;
                if ($(this).attr("class") != undefined) {
                    showmenu.removeClass("current").eq(0).addClass("current");
                }
                index = 0;
                mjcms.common.catid = 0;
                $(this).remove();
                mainlist.html(mjcms.index.mainlist(mjcms.common.catid, number));
			});
        } else {
            if (showmenu.length > 7) return ;
            $(this).addClass("select").append("<i class=\"iconfont icon-xuanzhong\"></i>");
            
            var html  = "<li data-catid=\""+$(this).attr("data-catid")+"\">";
                html += $(this).text();
                html += "</li>";
            showmenu.parent().append(html);
        }
    });
    mainmore.click(function() {
        bodymain.css({"height": "auto"});
        if (mjcms.common.catid == $(this).attr("data-catid")) {
            count++;
        }
        if (count >= 5) return ;
        
        mainhtml = mjcms.index.mainlist(mjcms.common.catid, number);
        if (mainhtml == "") return ;
        
        mainlist.append(mainhtml);
        if (count == 4) {
            $(this).addClass("main-more-none").hide();
            count = 0;
        }
        mjcms.common.position(tl_side, tb_main, tm_menu, tr_side);
        
        l_side.stop(true, false).animate({"bottom": "0px"}, 500);
        r_side.stop(true, false).animate({"bottom": "0px"}, 500);
    });
    
    return this;
};

mjcms.index.mainlist = function(catid, number) {
    var lists = $("ul.main-list"),
        count = number || 16,
        limit = lists.find("li").length+","+count,
        
        //file  = catid % 2 == 0 ? 0 : 1,
        html  = "";
        
    $.ajax({
        type: "get",
        url: "/index.php?m=home&c=index&a=ajax2", // "Resources/Skin/Default/temp/temp-data-"+file+".js",
        data: {catid: catid, limit: limit},
        dataType: "json",
        async: false,
        success: function(data) {
            $.each(data, function(key, row) {
                html += mjcms.index.itemlist(row);
            });
        },
        error: function(e) {},
    });
    
    return html;
};

mjcms.index.itemlist = function(data) {
    var keyword = data.keywords,
        keyarr  = keyword.split(","),
        images  = data.images,
        imgarr  = images ? images.split(",") : [],
        string  = "";
        
        string += "<li>";
        string += "<h2>";
        string += "<a href=\""+data.link+"\" target=\"_blank\" title=\""+data.title+"\">"+data.title+"</a>";
        string += "</h2>";
        
    if (images) {
        $.each(imgarr, function(i, img) {
        string += "<div class=\"imgs\">";
        string += "<a href=\""+data.link+"\" target=\"_blank\" title=\""+data.title+"\">";
        string += "<img src=\""+img+"\" title=\""+data.title+"\" alt=\""+data.title+"\" />";
        string += "</a>";
        string += "</div>";
        });
        string += "<div class=\"more\">";
        string += "<a href=\""+data.link+"\" target=\"_blank\" title=\""+data.title+"\">";
        string += "<i class=\"iconfont icon-youjiantou\" title=\"详细\"></i>";
        string += "</a>";
        string += "</div>";
    } else {
        string += "<div class=\"imgs\">";
        string += "<a href=\""+data.link+"\" target=\"_blank\" title=\""+data.title+"\">";
        string += "<img src=\""+data.thumb+"\" title=\""+data.title+"\" alt=\""+data.title+"\" />";
        string += "</a>";
        string += "</div>";
        string += "<div class=\"text\">";
        string += "<a href=\""+data.link+"\" target=\"_blank\">"+data.description+"</a>";
        string += "<a href=\""+data.link+"\" target=\"_blank\">【详细】 </a>";
        string += "</div>";
    }
        string += "<div class=\"help\">";
        string += "<div class=\"tags\">";
        $.each(keyarr, function(i, key) {
        string += "<a href=\""+data.search+"catid="+data.catid+"&key="+key+"\" target=\"_blank\">"+key+"</a>";
        });
        string += "</div>";
        string += "<div class=\"date\">"+data.inputtime+"</div>";
        string += "</div>";
        string += "</li>";
    
    return string;
};

mjcms.index.linkage = function() {
    var sidebar = $("div.tablinkage"),
        title   = sidebar.find("ul.title-b>li[class!='description']"),
        linkage = sidebar.find("ul.linkage>li");
        
        linkage.hide().eq(0).show();
        title.hover(function() {
            title.removeClass("current").eq($(this).index()).addClass("current");
            linkage.hide().eq($(this).index()).show();
        }, function() {});
    
    return this;
};

mjcms.list.mainmore = function() {
    var mainlist = $(" ul.main-list"),
        mainmore = $("div.main-more"),
        pageside = $("div#page_side").offset().top,
        
        l_side   = $("body>div.main>div.side>div:first-child"),
        r_side   = $("div#page_side"),
        
        number   = 16,
        count    =  0;
    
    mainmore.click(function() {
        count++;
        if (count >= 8) return ;
        mainhtml = mjcms.index.mainlist($(this).attr("data-catid"), number);
        if (mainhtml == "") return ;
        if (count == 7) {
            $(this).addClass("main-more-none").hide();
        }
        mainlist.append(mainhtml);
        
        mjcms.page.position(pageside);
        l_side.stop(true, false).animate({"top":   "0px"}, 500);
        r_side.stop(true, false).animate({"top": "-20px"}, 500);
    });
};

mjcms.show.ranking = function() {
    var tabs = $("ul.title-tab280>li"),
        wrap = $("ul.list-rank");
        
        wrap.hide().eq(0).show();
        tabs.hover(function() {
            tabs.removeClass("current").eq($(this).index()).addClass("current");
            wrap.hide().eq($(this).index()).show();
        }, function() {});
        
    return this;
};

mjcms.page.position = function(tsside) {
    var olside = $("body>div.main>div.side>div:first-child"),
        o_main = $("body>div.main>div.body>div.main"),
        orside = $("body>div.main>div.body>div.side"),
        osside = $("div#page_side"),
        o_foot = $("div#footer"),
        o_head = $("a.to-top"),
        
        o_container = $("body>div.main"),
        t_container = o_container.offset().top,
        
        hlside = olside.outerHeight(true),
        hrside = orside.outerHeight(true),
        hsside = osside.outerHeight(true),
        h_main = o_main.outerHeight(true),
        
        tlside = olside.offset().top + hlside,
        tsside = tsside || osside.offset().top,
        t_main = o_main.offset().top + h_main,
        
        w_page = $(window).width(),
        h_page = $(window).height(),
        
        t_roll = 0,
        l_roll = 0,
        t_foot = 0;
        
    $(window).scroll(function() {
        t_foot = (o_foot.offset().top - h_page);
        
        t_roll = $(this).scrollTop();
        l_roll = $(this).scrollLeft();
        
        if (t_roll > t_container) {
            o_head.fadeIn(1000).css({"bottom": ((h_page-o_head.outerHeight(true))/2)+"px"});
        } else {
            o_head.fadeOut(800);
        }
        
        if (w_page > 1240) {
            olside.css({"margin-left": "0px"});
            osside.css({"margin-left": "0px"});
        } else {
            olside.css({"margin-left": (-l_roll)+"px"});
            osside.css({"margin-left": (-l_roll)+"px"});
        }
        
        if (t_roll > (t_container + 40)) {
            olside.css({"position": "fixed", "top": "0px", "bottom": ""});
            
            if (t_roll > (t_foot + (h_page - hlside))) {
                olside.css({"top": "", "bottom": (t_roll - t_foot)+"px"});
            }
        } else {
            olside.css({"position": "relative"});
        }
        
        if (t_roll > (t_main - h_page) && (hrside > h_main)) {
            o_main.css({"position": "fixed", "bottom": "0px"});
            if (t_roll > t_foot) {
                o_main.css({"bottom": (t_roll - t_foot)+"px"});
            }
        } else {
            o_main.css({"position": "relative", "bottom": ""});
        }
        
        if (t_roll > tsside && (h_main > hrside)) {
            osside.css({"position": "fixed", "top": "-20px", "bottom": "auto"});
            
            if (t_roll > t_foot + (h_page - hsside)) {
                osside.css({"top": "auto", "bottom": (t_roll - t_foot)+"px"});
            }
        } else {
            osside.css({"position": "relative", "top": "", "bottom": ""});
        }
    });
};

mjcms.page.favorite = function(url, title) {
    try {
        window.external.addFavorite(url, title);
    } catch(e) {
        try {
            window.sidebar.addPanel(title, url, "");
        } catch(e) {
            alert("加入收藏失败，请使用Ctrl+D进行添加");
        }
    }
};

mjcms.page.sethome = function(obj, val) {
    if (window.sidebar) {
        try {
            netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        } catch(e) {
            alert("此操作被浏览器拒绝！n请在浏览器地址栏输入“about:config”并回车n然后将[signed.applets.codebase_principal_support]设置为true");
        }
        var prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
            prefs.setCharPref("browser.startup.homepage", val);
    } else if (document.all) {
        document.body.style.behavior="url(#default#homepage)";
        document.body.setHomePage(val);
    } else {
    }
};

mjcms.search.itemmore = function() {
    var mainlist = $(" ul.main-list"),
        mainmore = $("div.main-more"),
        pageside = $("div#page_side").offset().top,
        
        l_side   = $("body>div.main>div.side>div:first-child"),
        r_side   = $("div#page_side"),
        
        number   = 16,
        count    =  0;
    
    mainmore.click(function() {
        count++;
        if (count >= 8) return ;
        itemhtml = mjcms.search.itemlist($(this).attr("data-catid"), $(this).attr("data-key"), $(this).attr("data-link"), number);
        if (itemhtml == "") return ;
        if (count == 7) {
            $(this).addClass("main-more-none").hide();
        }
        mainlist.append(itemhtml);
        
        mjcms.page.position(pageside);
        l_side.stop(true, false).animate({"top":   "0px"}, 500);
        r_side.stop(true, false).animate({"top": "-20px"}, 500);
    });
};

mjcms.search.itemlist = function(catid, key, link, number) {
    var lists = $("ul.main-list"),
        count = number || 16,
        limit = lists.find("li").length+","+count,
        html  = "";
        
    $.ajax({
        type: "get",
        url: link, // "Resources/Skin/Default/temp/temp-data-"+file+".js",
        data: {catid: catid, key: key, limit: limit},
        dataType: "json",
        async: false,
        success: function(data) {
            $.each(data, function(key, row) {
                html += mjcms.index.itemlist(row);
            });
        },
        error: function(e) {},
    });
    
    return html;
};
