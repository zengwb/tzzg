
$(function(){
    // 页面加载获取数据
    var data = getParams();
    ajaxGetNews(data);

    //获取默认介绍信息
    getBuweiInfo($("div.goverment-text>span.goverment-on").attr("linkid"));
    /**
     * 加载显示部委已选择项的描述信息和图片
     */
    $("#govermentRegion>span").each(function(){
        if($(this).hasClass("goverment-on")){
            $(".bw-info").html($("#govermentRegion>div").html());
            var image = $(this).next("div").data("img").length > 0  ? $(this).next("div").data("img"):"/Resources/Skin/2019/image/zw-bw.gif";

            $(".bw-info").prev("img").attr("src", image);
        }
    });
    /**
     * 部委绑定点击事件
     */
    $("#govermentRegion>span").click(function(){
        $("#govermentRegion>span").removeClass("goverment-on");
        $(this).addClass("goverment-on");

        //重置tab
        resetTab();

        //显示描述信息
        var linkageid = $(this).attr("linkid");
        getBuweiInfo(linkageid);


        //显示图片
        var image = $(this).next("div").data("img").length > 0  ? $(this).next("div").data("img"):"/Resources/Skin/2019/image/zw-bw.gif";

        $(".bw-info").prev("img").attr("src", image);
        //重置分类菜单
        $(".goverment-nav>ul>li>a").each(function(){
            $(this).removeClass("red");
        });
        $(".goverment-nav>ul>li>a:first").addClass("red");
        //清除数据
        $("#zwbz_list").html('');
        //加载新闻列表 ajax请求 重新加载数据
        var data = getParams();
        data.typeid = $(".goverment-nav>ul>li>a:first").parent('li').attr('typeid');
        data.linkageid = linkageid;
        ajaxGetNews(data);

    });



    /**
     * 选择分类
     */
    $(".goverment-nav>ul>li").on("click", "a", function(){
        //获取分类id
        var typeid = $(this).parent("li").attr("typeid");

        $(".goverment-nav>ul>li>a").removeClass("red");
        $(this).addClass("red");
        //清除数据
        $("#zwbz_list").html('');

        //加载新闻列表 ajax请求 重新加载数据
        var data = getParams();
        data.typeid = typeid;
        if(typeid != 4030){
            data['type'] = 'news';
        }else{
            data['type'] = 'project';
        }
        ajaxGetNews(data);

    });

    /**
     * 点击搜索按钮
     */
    $("input[type='button']").click(function(){
        //清除数据
        $("#zwbz_list").html('');

        //加载新闻列表 ajax请求  重新加载数据
        var data = getParams();
        data.keywords = $(this).prev("input").val();
        ajaxGetNews(data);
        //点击搜索之后，清除关键词
        $(this).prev("input").val('');
    });


});

/**
 * 获取部委介绍信息
 */
function getBuweiInfo(unitid){
    $.ajax({
        url:"/index.php?m=home&c=index&a=buweiinfo",
        method:'get',
        data:{'unitid':unitid},
        success:function(res){
            if(res.code == 200){
                var image = res.data.image != null?res.data.image:"/Resources/Skin/2019/image/zw-bw.gif";
                var introduce = res.data.introduce != null ?res.data.introduce:"暂无介绍信息";
                $("#agencyinfo>img").attr("src",image);
                $("#agencyinfo>div.jgwz").html(introduce);
            }
        }
    })
}

/**
 * 重置tab切换
 */
function resetTab(){
    $("ul.jgdt>li").removeClass("t-active");
    $("#agencydata").hide();
    $("ul.jgdt>li:first").addClass("t-active");
    $("#agencyinfo").show();

}


/**
 * 获取参数
 */
function getParams(){
    var linkageid,typeid,keywords;
    //获取部委id
    $("#govermentRegion>span").each(function(){
        if($(this).hasClass("goverment-on")){
            linkageid = $(this).attr("linkid");
        }
    });
    //获取分类id
    $(".goverment-nav>ul>li").each(function(){
        if($(this).hasClass("red")){
            typeid = $(this).attr("typeid");
        }
    });

    //获取搜索关键词
    keywords = $("input[type='text']").val();
    //获取分页页数
    var curPage = 1;
    var pageSize = 5;
    var data = {
        "linkageid": linkageid,
        "typeid": typeid,
        "keywords":keywords,
        "pageSize":pageSize,
        "curPage":curPage
    };

    if(typeid != 4030){
        data['type'] = 'news';
    }else{
        data['type'] = 'project';
    }
    return data;
}

/**
 * 获取数据
 */
var total,pageSize,curPage,totalPage;
function ajaxGetNews(data){


    $.ajax({
        'url':'/index.php?m=home&c=index&a=ajaxGetBuweiList',
        'type':'get',
        'data':data,
        success:function(res){
            var html='';
            $("#zwbz_list").html('');
            if(res.type == "news"){
                if(res.data.length == 0){
                    html += "<li><div class='commissions-list-img lf'></div><div class='commissions-list-text lf wid6'><h5>暂无数据</h5><p></p><p><a></a></p></div></li>";
                }else{
                    for(var i=0; i<res.data.length; i++){

                        if(res.data[i].thumb.length > 0 ){
                            var thumb = res.data[i].thumb;
                        }
                        var title = res.data[i].title;
                        var description = res.data[i].description;
                        var time = res.data[i].inputtime;
                        var mid = res.data[i].mid;
                        var title = res.data[i].title;
                        var typeid = res.data[i].typeid;
                        var group = res.data[i].group;
                        html += "<li><div class='commissions-list-img lf'><img style='width:220px;height:160px' src='"+thumb+"' alt='"+title+"'></div><div class='commissions-list-text lf wid6'><h5>"+title+"<span>"+time+"</span></h5><p>"+description+"</p><p><a target='_blank' mid="+mid+" href='/index.php?m=home&c=index&a=shows&catid=189&id="+res.data[i].mid+"'>&lt;详情&gt;</a></p></div></li>";
                        ///index.php?m=home&c=index&a=shows&catid=198&id="+res.data[i].mid+"
                        // /index.php?m=Home&c=Index&a=shows&catid=189&typeid="+typeid+"&group="+group+"&id="+mid+"
                    }

                    html += "<div class='commissions-page'></div>";
                }
            }else{
                if(res.data.length == 0){
                    html += "<li><div class='commissions-list-img lf'></div><div class='commissions-list-text lf wid6'><h5>暂无数据</h5><p></p><p><a></a></p></div></li>";
                }else{

                    for(var i=0; i < res.data.length; i++){
                        var title = res.data[i].title;
                        var quasi_amount = res.data[i].quasi_amount;
                        var cityid = res.data[i].cityid;
                        var industryid = res.data[i].industryid;
                        var finacing_style = res.data[i].finacing_style;
                        var contact_name = res.data[i].contact_name;
                        var company = res.data[i].company;
                        var mid = res.data[i].mid;
                        var group = res.data[i].group;
                        ///index.php?m=Home&c=Index&a=showBuweiProject&catid=189&mid="+mid+"&group="+group+"
                        html += "<li><h3 class='list-title'><span> </span><a target='_blank' href='/index.php?m=home&c=index&a=shows&catid=200&id="+mid+"'>"+title+"</a></h3>";
                        html += "<div class='list-left lf'>";
                        html += "<p>融资资金：<span class='money'>"+quasi_amount+"亿元</span></p>";
                        html += "<p>所属行业：<span >"+industryid+"</span></p>";
                        html += "<p>所在地区："+cityid+"</p>";
                        html += "<p>项目融资方式："+finacing_style+"</p>";
                        html += "</div><div class='list-center lf'>";
                        html += "<i class='icon iconfont icon-icondingdanxiangqingxiansheng'></i>"+contact_name+"<br />"+company;
                        html += "</div><div class='list-right' style='float: right;margin-right: 70px;'><button>约谈资方</button></div></li>";
                    }

                    html += "<div class='commissions-page'></div>";
                }
            }

            //添加分页
            //需补充分页信息
            total = res.pageinfo.total; //总记录数
            pageSize = res.pageinfo.pageSize; //每页显示条数
            curPage = res.pageinfo.curPage; //当前页
            totalPage = res.pageinfo.totalPage; //总页数

            $("#zwbz_list").append(html);
            getPageBar();

        },
        complete:function(res){ //生成分页条

        },
        error:function(){
            alert("数据加载失败");
        }
    });
}



//获取分页条（分页按钮栏的规则和样式根据自己的需要来设置）
function getPageBar(){
    if(curPage > totalPage) {
        curPage = totalPage;
    }
    if(curPage < 1) {
        curPage = 1;
    }
    pageBar = "";
    pageBar +="<span  style='margin-right:25px;'>当前共<b style='color:red'> "+totalPage+"</b> 页</span>";
    //如果不是第一页
    if(curPage != 1){
        pageBar += "<span ><a href='javascript:turnPage(1)'>首页</a></span>";
        pageBar += "<span ><a href='javascript:turnPage("+(curPage-1)+")'><<</a></span>";
    }
    //显示的页码按钮(5个)
    var start,end;
    if(totalPage <= 5) {
        start = 1;
        end = totalPage;
    } else {
        if(curPage-2 <= 0) {
            start = 1;
            end = 5;
        } else {
            if(totalPage-curPage < 2) {
                start = totalPage - 4;
                end = totalPage;
            } else {
                start = curPage - 2;
                end = parseInt(curPage) + 2;
            }
        }
    }
    for(var i=start;i<=end;i++) {
        if(i == curPage) {
            pageBar += "<span class='page-on'><a href='javascript:turnPage("+i+")'>"+i+"</a></span>";
        } else {
            pageBar += "<span ><a href='javascript:turnPage("+i+")'>"+i+"</a></span>";
        }
    }
    //如果不是最后页
    if(curPage != totalPage){
        pageBar += "<span ><a href='javascript:turnPage("+(parseInt(curPage)+1)+")'>>></a></span>";
        pageBar += "<span ><a href='javascript:turnPage("+totalPage+")'>尾页</a></span>";
    }

    $(".commissions-page").html(pageBar);
}

//点击页码获取数据
function turnPage(curPage){
    var data = getParams();
    data.curPage = curPage;
    ajaxGetNews(data);
}

