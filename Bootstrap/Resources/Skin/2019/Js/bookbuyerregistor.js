$(function(){
    var phone;
    var flag = false;
    //获取手机号码，验证获取短信验证码
    $("#getverify").click(function() {
        phone = $("input[name='phone']").val();
        if(!verifyPhone(phone)){
            console.log("手机号码不符合规则");
            return false;
        }
        //调用短信接口发送短信验证码 成功倒计时，不成功返回错误信息
		$.ajax({
			'url':"/index.php?m=home&c=index&a=send_verify",
            'type':'get',
            'data':{'phone':phone},
            success:function(res){
			    console.log(res);
			    if(res.code == 200){
                    flag = true;
                    var time = 60;
                    if(flag === true){
                        //倒计时
                        $("#getverify").attr("disabled",true);
                        $("#getverify").css('background','gray');

                        var t = setInterval(function(){
                            time--;
                            // console.log(time);
                            $("#getverify").html(time+"秒");
                            if (time == 0) {
                                clearInterval(t);
                                $("#getverify").html("重新获取");
                                flag = true;
                                $("#getverify").attr("disabled",false);
                                $("#getverify").css('background',"#b80000");
                            }
                        },1000);
                    }
                }else if(res.code == 2){
			        //信息填写不完整
                    alert("您已注册过，确定跳转完页面完善信息。");
                    window.location.href = "/index.php?m=home&c=index&a=registor_two&id=" + res.data.id;
                    return false;
                }else{
                    alert(res.message);
                    return false;
                }
			}
		});
    });

    $("#next").click(function () {
        //后台验证手机号码是否一致，短信验证码是否一致
        var phone1 = $("input[name='phone']").val();
        if(phone === undefined){
            console.log("请先获取验证码");
            return false;
        }

        if(phone != phone1){
            console.log("手机号码跟获取短信验证码的手机号不一致");
            return false;
        }
        var verify = $("input[name='verify']").val();
        if(verify === ''){
            console.log("请填写验证码");
            return false;
        }
        var password = $("input[name='password']").val();
        if(password === ''){
            console.log("请填写密码");
            return false;
        }

        var data = {
            'phone': phone,
            'verify':verify,
            'password':password
        };
		$.ajax({
            'url':"/index.php?m=home&c=index&a=check_info",
            'type':'post',
            'data':data,
            success:function(res){
                if(res.code == 200 || res.code == 2){
                    window.location.href = "/index.php?m=home&c=index&a=registor_two&id=" + res.data.id;
                    // window.location.href = "{:U('home/index/registor_two')}";
                }else{
                    alert(res.message);
                    return false;
                }
            }
		});
    });

});


//验证手机号码
function verifyPhone(phone){
    var telReg = !!phone.match(/^1[345789]\d{9}$/);
    //如果手机号码不能通过验证
    if(telReg == false){
        return false;
    }
    return true;
}