
CREATE TABLE `{$data_table}` (
  `mid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'ID',
  `content` text COMMENT '内容',
  `views` int(8) DEFAULT '0' COMMENT '阅读',
  `template` varchar(16) DEFAULT '' COMMENT '模板',
  `relation` varchar(128) DEFAULT '' COMMENT '相关内容'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='{$comment}';

INSERT INTO `{$db_prefix}model_field` (`modelid`, `field`, `alias`, `tips`, `style`, `minlength`, `maxlength`, `pattern`, `errortips`, `formtype`, `setting`, `issystem`, `ismain`, `isbase`, `isposition`, `issearch`, `listorder`, `display`, `usergroupids`, `roleids`, `disabled`) VALUES ('{$modelid}', 'content', '内容', '', '', 0, 9999, '', '', 'editor', 'a:4:{s:7:\"toolbar\";s:5:\"basic\";s:7:\"default\";s:0:\"\";s:6:\"height\";s:3:\"320\";s:9:\"fieldtype\";s:4:\"text\";}', 1, 0, 1, 0, 0, 29, 1, NULL, NULL, 0);
INSERT INTO `{$db_prefix}model_field` (`modelid`, `field`, `alias`, `tips`, `style`, `minlength`, `maxlength`, `pattern`, `errortips`, `formtype`, `setting`, `issystem`, `ismain`, `isbase`, `isposition`, `issearch`, `listorder`, `display`, `usergroupids`, `roleids`, `disabled`) VALUES ('{$modelid}', 'views', '阅读', '', '', 0, 8, '', '', 'number', 'a:8:{s:9:\"minnumber\";s:1:\"0\";s:9:\"maxnumber\";s:5:\"99999\";s:13:\"decimaldigits\";s:1:\"0\";s:4:\"size\";s:0:\"\";s:7:\"default\";s:1:\"0\";s:9:\"minrandom\";s:2:\"11\";s:9:\"maxrandom\";s:2:\"99\";s:9:\"fieldtype\";s:6:\"double\";}', 1, 0, 0, 0, 0, 99, 1, NULL, NULL, 0);
INSERT INTO `{$db_prefix}model_field` (`modelid`, `field`, `alias`, `tips`, `style`, `minlength`, `maxlength`, `pattern`, `errortips`, `formtype`, `setting`, `issystem`, `ismain`, `isbase`, `isposition`, `issearch`, `listorder`, `display`, `usergroupids`, `roleids`, `disabled`) VALUES ('{$modelid}', 'template', '模板', '', '', 0, 16, '', '', 'template', 'a:2:{s:7:\"default\";s:0:\"\";s:9:\"fieldtype\";s:7:\"varchar\";}', 1, 0, 0, 0, 0, 54, 1, NULL, NULL, 0);
INSERT INTO `{$db_prefix}model_field` (`modelid`, `field`, `alias`, `tips`, `style`, `minlength`, `maxlength`, `pattern`, `errortips`, `formtype`, `setting`, `issystem`, `ismain`, `isbase`, `isposition`, `issearch`, `listorder`, `display`, `usergroupids`, `roleids`, `disabled`) VALUES ('{$modelid}', 'relation', '相关内容', '', '', 0, 128, '', '', 'relation', 'a:5:{s:6:\"module\";s:6:\"module\";s:7:\"modelid\";s:7:\"modelid\";s:5:\"catid\";s:5:\"catid\";s:2:\"id\";s:2:\"id\";s:9:\"fieldtype\";s:7:\"varchar\";}', 1, 0, 0, 0, 0, 53, 1, NULL, NULL, 0);
