<?php

return array (
  'dirname' => '2017',
  'name' => '2017模板',
  'author' => 'YhCMS项目团队',
  'link' => 'http://',
  'display' => 1,
  'remarks' => 
  array (
    'Views/Article/category.html' => '频道模板',
    'Views/Article/index.html' => '主页模板',
    'Views/Article/list.html' => '列表模板',
    'Views/Article/show.html' => '详情模板',
    'Views/Article' => '文章模块',
    'Views/Goods' => '商品模块',
    'Views/Mall' => '商城模块',
    'Views/Shop' => '商铺模块',
    'Block' => '块模板',
    'config.php' => '',
    'Touch' => '触摸版',
    'Views' => '电脑版',
  ),
);
