function dialog(options){
	if(options.type == "modal"){
		$.dialog({
			title:false,
			lock:true,
			width:options.width,
			height:options.height,
			id:options.id,
			padding:"0px",
			show:function(){
				var $this = this;
				$this.$content.load(options.url,function(){
					$("body").on("click",".dele_Icon",function(){
						$this.hide()
					});
				});
			},
			hide:function(){
				this.$content.empty();
			}
		})
	}else if(options.type == "confirm"){
		$.dialog({
			id:options.id,
			title:"提醒",
			lock:true,
			width:options.width,
			height:options.height,
			content:options.content,
			btn:{
				ok: {                     //按钮的key值，下次可用个btn方法从新设置
			      val: '确定',            //按钮显示的文本
			      type: 'green',          //样式可选参数green, blue, red, yellow .默认白色。
			      //disabled: true,       //是否可操作
			      click: function(btn) {
			        options.callback1();
			      }
			    },
			    cancle: {                 //第四个按钮
			      val: '取消'
			    }
			}
		})
	}else if(options.type == "tips"){
		$.dialog({
			title:"温馨提示",
			lock:true,
			width:options.width,
			height:options.height,
			content:options.content,
			id:options.id,
			time:2000
		})
	}else if(options.type == "cancelbox"){
		$.dialog({
			title:"警告",
			lock:true,
			width:options.width,
			height:options.height,
			content:options.content,
			id:options.id,
			btn:{
				ok: {                     //按钮的key值，下次可用个btn方法从新设置
			      val: '确定',            //按钮显示的文本
			      type: 'green',          //样式可选参数green, blue, red, yellow .默认白色。
			      //disabled: true,       //是否可操作
			      click: function(btn) {
			        options.callback1();
			      }
			    },
			    cancle: {                 //第四个按钮
			      val: '取消',
			      click:function(){
			      	options.callback2();
			      }
			    }
			}
		})
	}else if(options.type == "domModal"){
		$.dialog({
			title:options.title,
			lock:true,
			width:options.width,
			height:options.height,
			id:options.id,
			show:function(){
				var $this = this;
				$("body").on("click",".dele_Icon",function(){
					$this.hide()
				});
			},
			content:options.content
		})
	}
}