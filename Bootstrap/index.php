<?php

if (FALSE != version_compare(PHP_VERSION, '5.4', 'le')) {
    try {
        throw new Exception('REQUIRE PHP > 5.4');
    } catch (Exception $e) {
        exit($e->getMessage()); // @todo: 输出异常提示
    }
}
define('APP_STATUS', TRUE); // 开启应用程序状态（即激活应用）

define('CMS_PATH', dirname(__DIR__).DIRECTORY_SEPARATOR);
define('CMS_DIRS', DIRECTORY_SEPARATOR);

define('APP_PATH', CMS_PATH.'Application'.CMS_DIRS);
define('RES_PATH', CMS_PATH.'Bootstrap'.CMS_DIRS.'Resources'.CMS_DIRS);

define('RUNTIME_PATH', CMS_PATH.'Temporary'.CMS_DIRS);

define('APP_DEBUG', TRUE); // @todo: 开启调试模式？
define('BUILD_DIR_SECURE', FALSE);

header('Content-Type: text/html; charset=utf-8');

include_once (CMS_PATH.'Bootstrap/Webroots/Base.php');
include_once (CMS_PATH.'Framework/ThinkPHP.php');
