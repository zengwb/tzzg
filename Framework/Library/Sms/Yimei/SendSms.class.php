<?php
namespace Sms\Yimei;

class SendSms
{
    /**
     * 发送请求
     * @param $url
     * @param null $data
     * @return \stdClass
     */
    public final function http_request($url, $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, true);
        $header[] = "appId: ".C('YM_SMS_APPID');
        if (true == C('EN_GZIP'))   $header[] = "gzip: on";
        //print_r($header);echo C('END');

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($curl, CURLOPT_HEADER, true);
        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($curl);

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        curl_close($curl);

        $header = substr($res, 0, $headerSize);

        //echo "HEADER=".$header.END;
        //echo "URL=".$url.END;

        $outobj = new \stdClass();

        $lines = explode("\r\n",$header);
        foreach($lines as $line)
        {
            $items = explode(": ",$line);
            if(isset($items[0]) and !empty($items[0]) and
                isset($items[1]) and !empty($items[1]))
                $outobj->$items[0] = $items[1];
        }

        $outobj->ciphertext = substr($res, $headerSize);

        return $outobj;
    }

    /**
     * 获取微秒时间
     * @return float
     */
    public final function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
    }

    /**
     * 发送单条短信
     * @param $mobile
     * @param $content
     * @param string $timerTime
     * @param string $customSmsId
     * @param string $extendedCode
     * @param int $validPeriodtime
     * @return \stdClass
     */
    public final function SendSingleSMS($mobile, $content,
                     $timerTime = "", $customSmsId = "",
                     $extendedCode = "",
                     $validPeriodtime= 180)
    {

        // 如果您的系统环境不是UTF-8，内容需要转码到UTF-8。如下：从gb2312转到了UTF-8
        // $content = mb_convert_encoding( $content,"UTF-8","gb2312");

        $item = new \stdClass();
        $item->mobile   = $mobile;
        $item->content  = $content;

        /* 选填内容 */
        if("" != $timerTime)    $item->timerTime    = $timerTime;
        if("" != $customSmsId)  $item->customSmsId  = $customSmsId;
        if("" != $extendedCode) $item->extendedCode = $extendedCode;

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        //echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('YM_SMS_SEND_URI');
        $resobj = $this->http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }

    /**
     * 批量发送短息(自定义SMSID)
     * @param $mobiles
     * @param $content
     * @param string $timerTime
     * @param string $customSmsId
     * @param string $extendedCode
     * @param int $validPeriodtime
     * @return string
     */
    function SendBatchSMS($mobiles, $content,
                          $timerTime = "", $customSmsId = "",
                          $extendedCode = "",
                          $validPeriodtime= 120)
    {
        $item = new \stdClass();

        $smses = array();
        foreach($mobiles as $mobile)    $smses[] = $mobile;

        $item->smses   = $smses;

        // 如果您的系统环境不是UTF-8，内容需要转码到UTF-8。如下：从gb2312转到了UTF-8
        // $content = mb_convert_encoding( $content,"UTF-8","gb2312");

        $item->content  = $content;
        /* 选填内容 */
        if("" != $timerTime)    $item->timerTime    = $timerTime;
        if("" != $customSmsId)  $item->customSmsId  = $customSmsId;
        if("" != $extendedCode) $item->extendedCode = $extendedCode;

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('YM_SMS_SEND_BATCH_URI');
        $resobj = $this->http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }

    /**
     * 批量发发送短信（非自定义SMSID）
     * @param $mobiles
     * @param $content
     * @param string $timerTime
     * @param string $customSmsId
     * @param string $extendedCode
     * @param int $validPeriodtime
     * @return string
     */
    public final function sendBatchOnlySMS($mobiles, $content,
                              $timerTime = "", $customSmsId = "",
                              $extendedCode = "",
                              $validPeriodtime= 120)
    {

        // 如果您的系统环境不是UTF-8，内容需要转码到UTF-8。如下：从gb2312转到了UTF-8
        // $content = mb_convert_encoding( $content,"UTF-8","gb2312");

        $item = new \stdClass();

        $item->mobiles  = $mobiles;
        $item->content  = $content;
        /* 选填内容 */
        if("" != $timerTime)    $item->timerTime    = $timerTime;
        if("" != $extendedCode) $item->extendedCode = $extendedCode;

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('YM_SMS_SEND_BATCHONLY_SMS_URI');
        $resobj = $this->http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }

    /**
     * 发送个性短信
     * @param $mobiles
     * @param string $timerTime
     * @param string $customSmsId
     * @param string $extendedCode
     * @param int $validPeriodtime
     * @return string
     */
     public final function sendPersonalitySMS($mobiles,
                                $timerTime = "", $customSmsId = "",
                                $extendedCode = "",
                                $validPeriodtime= 120)
    {

        // 如果您的系统环境不是UTF-8，内容需要转码到UTF-8。如下：从gb2312转到了UTF-8
        // $content = mb_convert_encoding( $content,"UTF-8","gb2312");

        $item = new \stdClass();
        $smses = array();
        foreach($mobiles as $mobile)    $smses[] = $mobile;

        $item->smses   = $smses;

        /* 选填内容 */
        if("" != $timerTime)    $item->timerTime    = $timerTime;
        if("" != $customSmsId)  $item->customSmsId  = $customSmsId;
        if("" != $extendedCode) $item->extendedCode = $extendedCode;

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('M_SMS_SEND_PERSONALITY_SMS_URI');
        $resobj = $this->http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }

    /**
     * 获取状态报告
     * @param int $number
     * @param int $validPeriodtime
     * @return string
     */
    public final function getReport($number = 0, $validPeriodtime= 120)
    {


        $item = new \stdClass();
        /* 选填内容 */
        if(0 != $number)    $item->number    = $number;

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('YM_SMS_GETREPORT_URI');
        $resobj = http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }

    /**
     * 获取上行
     * @param int $number
     * @param int $validPeriodtime
     * @return \stdClass
     */
    public final function getMo($number = 0, $validPeriodtime= 120)
    {
        $item = new \stdClass();
        /* 选填内容 */
        if(0 != $number)    $item->number    = $number;

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('YM_SMS_GETMO_URI');
        $resobj = $this->http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }


    public final function getBalance($validPeriodtime= 120)
    {
        $item = new \stdClass();

        $item->requestTime = $this->getMillisecond();
        $item->requestValidPeriod = $validPeriodtime;

        $json_data = json_encode($item, JSON_UNESCAPED_UNICODE);

        echo "SendJson=".$json_data.C('END');

        $encryptObj = new MagicCrypt();
        $senddata = $encryptObj->encrypt($json_data);//加密结果

        $url = C('YM_SMS_ADDR').C('YM_SMS_GETBALANCE_URI');
        $resobj = http_request($url, $senddata);
        $resobj->plaintext = $encryptObj->decrypt($resobj->ciphertext);

        return $resobj;
    }
}