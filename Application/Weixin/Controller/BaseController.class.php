<?php

namespace Weixin\Controller;

use \Common\Controller\iBaseController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 微信模块基础控制器类：配置或实例化应用模块
 * 
 * @author T-01
 */
abstract class BaseController extends iBaseController {
    
    protected   $roleid     = 0;
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iBaseController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
        $this->roleid = session('CMS_RoleID');
    }
    
}
