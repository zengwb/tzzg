<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 输出矩形横幅广告
 * 
 * @param array     $args       必填。广告配置
 * 
 * @return string
 */
function cms_advert_banner($args) {
    return cms_advert_files($args); // @todo: cms_advert_files
}

/**
 * 输出固定位置广告
 * 
 * @param array     $args       必填。广告配置
 * 
 * @return string
 */
function cms_advert_fixure($args) {
    list($t, $r, $b, $l) = explode(',', $args['position']);
    $args['style'] = ' position:fixed; ';
    
    $args['style'].= $l ? 'left: '  .$l.'px; ' : '';
    $args['style'].= $r ? 'right: ' .$r.'px; ' : '';
    
    $args['style'].= $t ? 'top: '   .$t.'px; ' : '';
    $args['style'].= $b ? 'bottom: '.$b.'px; ' : '';
    
    return cms_advert_files($args);
}

/**
 * 输出文字/代码广告
 * 
 * @param array     $args       必填。广告配置
 * 
 * @return string
 */
function cms_advert_text($args) {
    $html = '';
    if ($args['type'] == 0) {
        $html .= '<span>'.$args['text'].'</span>';
    }
    if ($args['type'] == 1) {
        $html .= htmlspecialchars_decode($args['code']);
    }
    return $html;
}

/**
 * 输出列表广告
 * 
 * @param array     $args       必填。广告配置
 * 
 * @return string
 */
function cms_advert_lists($args) {
    return '<li>'.cms_advert_files($args).'</li>';
}

/**
 * 输出文件类型广告
 * 
 * @param array     $args       必填。广告配置
 * 
 * @return string
 */
function cms_advert_files($args) {
    list($w, $h) = explode(',', $args['size']);
    
    $file = strtr($args['fileurl'], ['./' => '/']);
    $imgs = strtr($args['imageurl'],['./' => '/']);
    
    $temp = [];
    $temp['advertid']   = $args['advertid'];
    $temp['linkurl']    = $args['linkurl'];
    $site = substr(cms_site_info()['domain'], 0, -1);
    $link = $site.U('Plugin/Count/advert', $temp);
    
    $html = '';
    if ($args['type'] == 3) {
    $html .= '<object classid="'.time().'" ';
    $html .= 'codebase="';
    $html .= 'http://download.macromedia.com/pub/shockwave/cabs/flash/';
    $html .= 'swflash.cab#version=6,0,29,0" ';
    $html .= 'width="'.$w.'" height="'.$h.'" ';
    $html .= 'style="'.$args['style'].'">';
    $html .= '<param name="movie" value="'.$file.'">';
    $html .= '<param name="quality" value="high">';
    $html .= '<param name="wmode" value="transparent">';
    $html .= '<embed wmode="transparent" ';
    $html .= 'src="'.$file.'" ';
    $html .= 'quality="high" type="application/x-shockwave-flash" ';
    $html .= 'pluginspage="';
    $html .= 'http://www.macromedia.com/go/getflashplayer" ';
    $html .= 'width="'.$w.'" height="'.$h.'">';
    $html .= '</embed>';
    $html .= '</object>';
    }
    if ($args['type'] == 2) {
    $html .= '<a href="'.$link.'" target="_blank" ';
    $html .= 'title="'.$args['alt'].'">';
    $html .= '<img src="'.$site.$imgs.'" alt="'.$args['alt'].'" ';
    $html .= 'style="width:'.$w.'px; height:'.$h.'px;';
    $html .= $args['style'].'" />';
    $html .= '</a>';
    }
    return $html;
}

/**
 * 幻灯广告
 * 
 * @param array     $args       必填。广告配置
 * 
 * @return string
 */
function cms_advert_slide($args) {
    list($w, $h) = explode(',', $args['size']);
    
    $file  = strtr($args['fileurl'], ['./' => '/']);
    $imgs  = strtr($args['imageurl'],['./' => '/']);
    
    $temp  = [];
    $temp['advertid']   = $args['advertid'];
    $temp['linkurl']    = $args['linkurl'];
    $site  = substr(cms_site_info()['domain'], 0, -1);
    $link  = $site.U('Plugin/Count/advert', $temp);
    
    $html  = '';
    $html .= '<li>';
    $html .= '<a href="'.$link.'" target="_blank" ';
    $html .= 'title="'.$args['alt'].'">';
    $html .= '<img src="'.$site.$imgs.'" alt="'.$args['alt'].'" ';
    $html .= 'style="width:'.$w.'px; height:'.$h.'px;';
    $html .= $args['style'].'" />';
    $html .= '</a>';
    $html .= '<p>';
    $html .= '<a href="'.$link.'" target="_blank">'.$args['alt'].'</a>';
    $html .= '</p>';
    $html .= '<p class="bg"></p>';
    $html .= '</li>';
    return $html;
}
