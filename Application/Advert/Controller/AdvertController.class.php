<?php

namespace Advert\Controller;

use Think\Upload;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 广告模块内容管理控制器类：呈现广告CURD常规管理
 * 
 * @author T-01
 */
final class AdvertController extends BaseController {
    
    public      $action     = [];
    
    private     $template   = [],
                $space      = [],
                $advert     = [],
                
                $upload     = null,
                $images     = null;
    
    /**
     * {@inheritDoc}
     * @see \Advert\Controller\BaseController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
        
        $this->upload = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Advert/';
        
        $this->upload->subName  = null;
        $this->upload->saveName = date('YmdHis', time());
        
        $this->upload->exts = ['jpg','jpeg','gif','png'];
        
        $this->space        = D('AdvertSpace');
        $this->template     = D('AdvertTemplate');
        $this->advert       = D('Advert');
    }
    
    /**
     * 广告管理
     */
    public final function index() {
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        
        $ress = $this->advert->where($where)->order(
            '`listorder` ASC, `advertid` DESC'
        )->select();
        
        foreach ($ress as $key => $row) {
        $where              = [];
        $where['spaceid']   = $row['spaceid'];
        $space = $this->space->where($where)->find();
        
        $row['space'] = $space['name'];
        $row['type'] = C('TYPE_DESC')[$row['type']].'广告';
        
        $row['end'] = time() >= $row['endtime'] ? 1 : 0;
        $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('spaceid', $info['spaceid']);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['advertid'] as $key => $advertid) {
        $listorder = $this->advert->where([
            'advertid' => $advertid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->advert->where(['advertid' => $advertid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $advertid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('更新广告排序成功！', $list ? : null);
    }
    
    /**
     * 添加广告
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $temp = [];
        
        $setting = I('post.setting', []);
        if ($data['type']   == '') {
        } else if ($data['type'] == 0) {
        $temp['text']       = $setting['text'];
        } else if ($data['type'] == 1) {
        $temp['code']       = $setting['code'];
        } else if ($data['type'] == 2) {
        $temp['imageurl']   = $setting['imageurl'];
        $temp['linkurl']    = $setting['linkurl'];
        $temp['alt']        = $setting['alt'];
        } else if ($data['type'] == 3) {
        $temp['fileurl']    = $setting['fileurl'];
        $temp['linkurl']    = $setting['linkurl'];
        }
        if (empty($_FILES['adimage']['error']) &&
            isset($_FILES['adimage'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('图片上传失败！');
        
        $temp['imageurl']   = $this->upload->rootPath;
        $temp['imageurl']  .= $file['adimage']['savepath'];
        $temp['imageurl']  .= $file['adimage']['savename'];
        
        $text               = [];
        $text['filename']   = $file['adimage']['name'];
        $text['filesize']   = $file['adimage']['size'];
        $text['fileext']    = $file['adimage']['ext'];
        $text['isimage']    = 1;
        $text['filepath']   = $temp['imageurl'];
        cms_attachment($text);
        }
        
        if (empty($_FILES['adfile']['error']) &&
            isset($_FILES['adfile'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('文件上传失败！');
        
        $temp['fileurl']    = $this->upload->rootPath;
        $temp['fileurl']   .= $file['adfile']['savepath'];
        $temp['fileurl']   .= $file['adfile']['savename'];
        
        $text               = [];
        $text['filename']   = $file['adfile']['name'];
        $text['filesize']   = $file['adfile']['size'];
        $text['fileext']    = $file['adfile']['ext'];
        $text['isimage']    = 1;
        $text['filepath']   = $temp['fileurl'];
        cms_attachment($text);
        }
        $data['setting']    = serialize($temp);
        $data['spaceid']    = $info['spaceid'];
        
        $data['starttime']  = strtotime($data['starttime']);
        $data['endtime']    = strtotime($data['endtime']);
        $data['addtime']    = time();
        
        $tips = '添加广告『'.$data['name'].'』信息';
        $ress = $this->advert->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        $inid = $this->advert->getLastInsID();
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $this->space->where($where)->setInc('adcount', 1);
        
        $this->advert->where(['advertid' => $inid])->save([
            'listorder' => $inid
        ]);
        $args = ['spaceid' => $info['spaceid']];
        $this->success($tips.'成功！', '', U('index', $args));
        } // @todo: 
        
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $space = $this->space->where($where)->find();
        
        $where              = [];
        $where['template']  = $space['template'];
        $template = $this->template->where($where)->find();
        
        $setting = unserialize($space['setting']);
        $typearr = explode(',', $setting['type']);
        
        foreach ($typearr as $i => $val) {
        $type[$i]['key']    = $val;
        $type[$i]['val']    = C('TYPE_DESC')[$val];
        }
        $data               = [];
        $data['type']       = reset($typearr);
        $data['starttime']  = date('Y-m-d H:i:s', time());
        $data['endtime']    = date('Y-m-d H:i:s', time()+(3600*24*30));
        $data['spaceid']    = $info['spaceid'];
        
        $this->assign('spacename', $space['name']);
        $this->assign('spacetype', $template['name']);
        
        $this->assign('type', $type);
        $this->assign('data', $data);
        
//         $this->assign('spaceid', $info['spaceid']);
        $this->display('action');
    }
    
    /**
     * 修改广告
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $temp = [];
        
        $setting = I('post.setting', []);
        if ($data['type']   == '') {
        } else if ($data['type'] == 0) {
        $temp['text']       = $setting['text'];
        } else if ($data['type'] == 1) {
        $temp['code']       = $setting['code'];
        } else if ($data['type'] == 2) {
        $temp['imageurl']   = $setting['imageurl'];
        $temp['linkurl']    = $setting['linkurl'];
        $temp['alt']        = $setting['alt'];
        } else if ($data['type'] == 3) {
        $temp['fileurl']    = $setting['fileurl'];
        $temp['linkurl']    = $setting['linkurl'];
        }
        if (empty($_FILES['adimage']['error']) &&
            isset($_FILES['adimage'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('图片上传失败！');
        
        $temp['imageurl']   = $this->upload->rootPath;
        $temp['imageurl']  .= $file['adimage']['savepath'];
        $temp['imageurl']  .= $file['adimage']['savename'];
        
        $text               = [];
        $text['filename']   = $file['adimage']['name'];
        $text['filesize']   = $file['adimage']['size'];
        $text['fileext']    = $file['adimage']['ext'];
        $text['isimage']    = 1;
        $text['filepath']   = $temp['imageurl'];
        cms_attachment($text);
        }
        
        if (empty($_FILES['adfile']['error']) &&
            isset($_FILES['adfile'])) {
            $file = $this->upload->upload();
        if (!$file) $this->error('文件上传失败！');
        
        $temp['fileurl']    = $this->upload->rootPath;
        $temp['fileurl']   .= $file['adfile']['savepath'];
        $temp['fileurl']   .= $file['adfile']['savename'];
        
        $text               = [];
        $text['filename']   = $file['adfile']['name'];
        $text['filesize']   = $file['adfile']['size'];
        $text['fileext']    = $file['adfile']['ext'];
        $text['isimage']    = 1;
        $text['filepath']   = $temp['fileurl'];
        cms_attachment($text);
        }
        $data['setting']    = serialize($temp);
        
        $data['starttime']  = strtotime($data['starttime']);
        $data['endtime']    = strtotime($data['endtime']);
        
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $where              = [];
        $where['advertid']  = $info['advertid'];
        $where['spaceid']   = $info['spaceid'];
        
        $tips = '修改广告『'.$name.'』内容';
        $ress = $this->advert->where($where)->save($data);
        $args = ['spaceid' => $info['spaceid']];
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index', $args));
        } // @todo:
        
        $advertid = I('get.advertid', 0, 'intval');
        $info               = [];
        $info['advertid']   = $advertid;
        
        $where              = [];
        $where['advertid']  = $info['advertid'];
        $data = $this->advert->where($where)->find();
        
        $info['spaceid']    = $data['spaceid'];
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $space = $this->space->where($where)->find();
        
        $where              = [];
        $where['template']  = $space['template'];
        $template = $this->template->where($where)->find();
        
        $setting = unserialize($space['setting']);
        $typearr = explode(',', $setting['type']);
        
        foreach ($typearr as $i => $val) {
        $type[$i]['key']    = $val;
        $type[$i]['val']    = C('TYPE_DESC')[$val];
        }
        $data['starttime']  = date('Y-m-d H:i:s', $data['starttime']);
        $data['endtime']    = date('Y-m-d H:i:s', $data['endtime']);
        
        $this->assign('spacename', $space['name']);
        $this->assign('spacetype', $template['name']);
        
        $this->assign('type', $type);
        $this->assign('data', $data);
        $this->assign('setting', unserialize($data['setting']));
        
//         $this->assign('spaceid', $info['spaceid']);
        $this->display('action');
    }
    
    /**
     * 删除广告
     */
    public final function delete() {
        $advertid = I('get.advertid', 0, 'intval');
        $info = I('post.info', []);
        $info = $advertid ? [$advertid] : $info['advertid'];
        $data = [];
        if (empty($info)) $this->error('请选择广告内容！');
        
        foreach ($info as $key => $advertid) {
        $data['advertid'][] = $advertid;
        $where              = [];
        $where['advertid']  = $advertid;
        $temp = $this->advert->where($where)->find();
        
        $setting = unserialize($temp['setting']);
        $imgs = $setting['imageurl'];
        $file = $setting['fileurl'];
        
        if ($temp['type'] == 2 && file_exists($imgs))
            @unlink($imgs);
        if ($temp['type'] == 3 && file_exists($file))
            @unlink($file);
        $where              = [];
        $where['spaceid']   = $temp['spaceid'];
        $this->space->where($where)->setDec('adcount', 1);
        }
        $where              = [];
        $where['advertid']  = ['IN', $data['advertid']];
        $ress = $this->advert->where($where)->delete();
        $temp = 'advertid:'.implode(',', $data['advertid']);
        
        if (!$ress) $this->error('删除广告内容失败！', $temp);
        else $this->success('删除广告内容成功！', $temp);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $advertid = I('get.advertid', 0, 'intval');
        $info               = [];
        $info['advertid']   = $advertid;
        
        $where              = [];
        $where['advertid']  = $info['advertid'];
        $ress2 = $this->advert->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where              = [];
        $where['advertid']  = $info['advertid'];
        $ress2 = $this->advert->where($where)->save([
            'disabled'=>$value
        ]);
        $where              = [];
        $where['advertid']  = $info['advertid'];
        $data2 = $this->advert->where($where)->find();
        $oper2 = $data2['disabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'广告『'.$data2['name'].'』内容';
        $tips2.= $state;
        cms_writelog($tips2, ['advertid' => $info['advertid']]);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 文件操作
     */
    public final function files() {
        $advertid = I('get.advertid', 0, 'intval');
        $info               = [];
        $info['advertid']   = $advertid;
        
        $temp               = [];
        $temp['advertid']   = $info['advertid'];
        
        $where              = [];
        $where['advertid']  = $info['advertid'];
        $data = $this->advert->where($where)->find();
        
        $setting = unserialize($data['setting']);
        $imgs = $setting['imageurl'];
        $file = $setting['fileurl'];
        
        if ($data['type'] == 2) {
        if (file_exists($imgs)) @unlink($imgs);
        $setting['imageurl'] = '';
        $temp['imageurl'] = $imgs;
        }
        if ($data['type'] == 3) {
        if (file_exists($file)) @unlink($file);
        $setting['fileurl'] = '';
        $temp['fileurl'] = $file;
        }
        $where              = [];
        $where['advertid']  = $info['advertid'];
        
        $data               = [];
        $data['setting'] = serialize($setting);
        $ress = $this->advert->where($where)->save($data);
        
        if (!$ress) $this->error('删除文件失败！', $temp);
        else $this->success('删除文件成功！', $temp);
    }
    
}
