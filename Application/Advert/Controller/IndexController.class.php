<?php

namespace Advert\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 广告模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    private     $space      = [],
                $advert     = [];
    
    /**
     * {@inheritDoc}
     * @see \Advert\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->space        = D('AdvertSpace');
        $this->advert       = D('Advert');
    }
    
    /**
     * 打印输出
     */
    public final function poster() {
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['disabled']  =  0;
        
        $space = $this->space->where($where)->find();
        if (!$space) return ;
        
        $template = $space['template'];
        $setting  = unserialize($space['setting']);
        
        $position = $setting['position'];
        $adsize = $setting['size'];
        $number = $setting['number'];
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $where['starttime'] = ['ELT', time()];
        $where['endtime']   = ['EGT', time()];
        $where['disabled']  =  0;
        
        $limit = '0,'.$number;
        $lists = $this->advert->where($where)->limit($limit)->order(
            '`listorder` ASC, `advertid` DESC'
        )->select();
        
        $html = '';
        foreach ($lists as $key => $row) {
        $args = unserialize($row['setting']);
        $args['type']       = $row['type'];
        $args['size']       = $adsize;
        $args['advertid']   = $row['advertid'];
        $args['position']   = $position;
        
        $func = 'cms_advert_'.$template;
        $html.= $func($args);
        }
        echo cms_string2script($html);
        exit();
    }
    
}
