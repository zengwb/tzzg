<?php

namespace Advert\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 广告模块模板管理控制器类：呈现模板CURD常规管理
 * 
 * @author T-01
 */
final class TemplateController extends BaseController {
    
    public      $action     = [];
    
    private     $template   = [];
    
    /**
     * {@inheritDoc}
     * @see \Advert\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->template     = D('AdvertTemplate');
    }
    
    /**
     * 模板管理
     */
    public final function index() {
        $data = [];
        $ress = $this->template->where()->order(
            '`listorder` ASC, `tplid` ASC'
        )->select();
        foreach ($ress as $key => $row) {
        $array              = [];
        $setting            = unserialize($row['setting']);
        $types              = explode(',', $setting['type']);
        foreach ($types as $i) {
        if (empty(C('TYPE_DESC')[$i])) continue;
        $array[$i]          = C('TYPE_DESC')[$i];
        }
        $row['type']        = implode(' / ', $array);
        $data[$key]         = $row;
        }
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['tplid'] as $key => $tplid) {
        $listorder = $this->template->where([
            'tplid' => $tplid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->template->where(['tplid' => $tplid])->save([
            'listorder'=>$data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $tplid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('更新模板排序成功！', $list ? : null);
    }
    
    /**
     * 添加模块
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $type = I('post.type', []);
        $type = implode(',', $type['type']);
        
        $data = I('post.data', [], 'cms_addslashes');
        $data['setting'] = serialize(['type' => $type]);
        
        $where              = [];
        $where['template']  = trim($data['template']);
        
        $ress = $this->template->where($where)->find();
        $tips = '模板标识『'.$data['template'].'』已经存在！';
        if ($ress) $this->error($tips);
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $tips = '添加模板『'.$data['template'].'』标识';
        $ress = $this->template->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        $inid = $this->template->getLastInsID();
        
        $this->template->where(['tplid' => $inid])->save([
            'listorder'=>$inid
        ]);
        $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $this->assign('data', ['status' => 1]);
        $this->assign('type', C('TYPE_DESC'));
        $this->display('action');
    }
    
    /**
     * 修改模板
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $type = I('post.type', []);
        $type = implode(',', $type['type']);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $temp = $this->template->where($where)->find();
        $setting = unserialize($temp['setting']);
        $setting['type'] = $type;
        $data['setting'] = serialize($setting);
        
        $where              = [];
        $where['template']  = trim($data['template']);
        
        $ress = $this->template->where($where)->find();
        $tips = '模板标识『'.$data['template'].'』已经存在！';
        if ($ress && $info['template'] != $data['template']) {
            $this->error($tips);
        }
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $where              = [];
        $where['tplid']     = $info['tplid'];
        
        $ress = $this->template->where($where)->save($data);
        $tips = '修改模板『'.$name.'』标识';
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $tplid = I('get.tplid', 0, 'intval');
        $info               = [];
        $info['tplid']      = $tplid;
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $data = $this->template->where($where)->find();
        
        $setting = unserialize($data['setting']);
        $data['type'] = explode(',', $setting['type']);
        
        $this->assign('data', $data);
        $this->assign('type', C('TYPE_DESC'));
        
        $this->display('action');
    }
    
    /**
     * 删除模板
     */
    public final function delete() {
        $tplid = I('get.tplid', 0, 'intval');
        $info = I('post.info', []);
        $info = $tplid ? [$tplid] : $info['tplid'];
        $data = [];
        if (empty($info)) $this->error('请选择广告模板！');
        
        foreach ($info as $key => $tplid) {
        $data['tplid'][]    = $tplid;
        }
        $where              = [];
        $where['tplid']     = ['IN', $data['tplid']];
        $ress = $this->template->where($where)->delete();
        
        if (!$ress) $this->error('删除广告模板失败！', $data);
        else $this->success('删除广告模板成功！', $data);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $tplid = I('get.tplid', 0, 'intval');
        $info               = [];
        $info['tplid']      = $tplid;
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $ress2 = $this->template->where($where)->find();
        $value = $ress2['status'] ? 0 : 1;
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $ress2 = $this->template->where($where)->save([
            'status' => $value
        ]);
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $data2 = $this->template->where($where)->find();
        $oper2 = $data2['status'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'广告『'.$data2['name'].'』模板';
        $tips2.= $state;
        cms_writelog($tips2, ['tplid' => $info['tplid']]);
        
        echo json_encode($data2['status']);
    }
    
    /**
     * 配置模板
     */
    public final function setting() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $setting = I('post.setting', [], 'cms_addslashes');
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $data = $this->template->where($where)->find();
        
        $temp = unserialize($data['setting']);
        $setting['type'] = $temp['type'];
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        $ress = $this->template->where($where)->save([
            'setting'=>serialize($setting)
        ]);
        $tips = '配置广告『'.$info['name'].'』模板';
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $tplid = I('get.tplid', 0, 'intval');
        $info               = [];
        $info['tplid']      = $tplid;
        
        $where              = [];
        $where['tplid']     = $info['tplid'];
        
        $data = $this->template->where($where)->find();
        $info['template']   = $data['template'];
        
        $view = APP_PATH.MODULE_NAME.'/View/Template/';
        $view.= $info['template'].'View.html';
        
        $this->assign('data', $data);
        $this->assign('view', $view);
        
        $this->assign('setting', unserialize($data['setting']));
        $this->display();
    }
    
}
