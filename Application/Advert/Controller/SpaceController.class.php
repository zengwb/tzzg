<?php

namespace Advert\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 广告模块位置管理控制器类：呈现位置CURD常规管理
 * 
 * @author T-01
 */
final class SpaceController extends BaseController {
    
    public      $action     = [
        'index', 'add', 'edit', 'delete', 'state', 'setting',
        'views', 'call'
    ];
    
    private     $template   = [],
                $space      = [],
                $advert     = [],
                $siteModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Advert\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->space        = D('AdvertSpace');
        $this->template     = D('AdvertTemplate');
        $this->advert       = D('Advert');
        
        $this->siteModel    = D('Admin/Website');
    }
    
    /**
     * 位置管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $order = '`name` DESC, `spaceid` ASC';
        $where              = [];
        $where['name']      = ['LIKE', '%'.$keyword.'%'];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        $nums = $this->space->where($where)->count();
        $rows = 200; // C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->space->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title'] = $row['name'];
        else $row['title'] = strtr($row['name'], $keys);
        
        $where              = [];
        $where['template']  = $row['template'];
        $view = $this->template->where($where)->find();
        $row['template']    = $view['name'];
        
        $setting = unserialize($row['setting']);
        list($w, $h) = explode(',', $setting['size']);
        $row['size'] = $setting['size'] ? $w.' × '.$h : '/';
        
        $where              = [];
        $where['spaceid']   = $row['spaceid'];
        
        $where['starttime'] = ['ELT', time()];
        $where['endtime']   = ['EGT', time()];
        $where['disabled']  =  0;
        $count = $this->advert->where($where)->count();
        
        $row['adcount'] = $count;
        $row['number']  = $setting['number'];
        
        $data[$key]  = $row;
        }
        $this->assign('data', $data);
        $this->assign('nums', $nums);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 添加广告
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['name']      = trim($data['name']);
        
        $ress = $this->space->where($where)->find();
        $tips = '广告位置『'.$data['name'].'』已经存在！';
        if ($ress) $this->error($tips, $data);
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where              = [];
        $where['template']  = $data['template'];
        $ress = $this->template->where($where)->find();
        $data['setting']    = $ress['setting'];
        
        $tips = '添加广告『'.$data['name'].'』位置';
        $ress = $this->space->add($data);
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $where              = [];
        $where['status']    =  1;
        $view = $this->template->where($where)->order(
            '`listorder` ASC, `tplid` ASC'
        )->select();
        
        $this->assign('site', $site);
        $this->assign('view', $view);
        
        $this->display('action');
    }
    
    /**
     * 修改配置
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['name']      = trim($data['name']);
        
        $ress = $this->space->where($where)->find();
        $tips = '广告位置『'.$data['name'].'』已经存在！';
        if ($ress && $info['name'] != $data['name']) {
            $this->error($tips, $data);
        }
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        
        $ress = $this->space->where($where)->save($data);
        $tips = '修改广告『'.$name.'』位置';
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $data = $this->space->where($where)->find();
        
        $where              = [];
        $where['display']   =  1;
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $where              = [];
        $where['status']    =  1;
        $view = $this->template->where($where)->order(
            '`listorder` ASC, `tplid` ASC'
        )->select();
        
        $this->assign('site', $site);
        $this->assign('view', $view);
        $this->assign('data', $data);
        
        $this->display('action');
    }
    
    /**
     * 删除位置
     */
    public final function delete() {
        $spaceid = I('get.spaceid', 0, 'intval');
        $info = I('post.info', []);
        $info = $spaceid ? [$spaceid] : $info['spaceid'];
        $data = [];
        if (empty($info)) $this->error('请选择广告位置！');
        
        foreach ($info as $key => $spaceid) {
        $data['spaceid'][]  = $spaceid;
        $where              = [];
        $where['spaceid']   = $spaceid;
        $ress = $this->advert->where($where)->select();
        
        foreach ($ress as $i => $row) {
        $setting = unserialize($row['setting']);
        $imgs = $setting['imageurl'];
        $file = $setting['linkurl'];
        
        if ($row['type'] == 2 && file_exists($imgs)) 
            @unlink($imgs);
        if ($row['type'] == 3 && file_exists($file))
            @unlink($file);
        }
        $this->advert->where($where)->delete();
        }
        $where              = [];
        $where['spaceid']   = ['IN', $data['spaceid']];
        $ress = $this->space->where($where)->delete();
        $temp = 'spaceid:'.implode(',', $data['spaceid']);
        
        if (!$ress) $this->error('删除广告位置失败！', $temp);
        else $this->success('删除广告位置成功！', $temp);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $ress2 = $this->space->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $ress2 = $this->space->where($where)->save([
            'disabled'=>$value
        ]);
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $data2 = $this->space->where($where)->find();
        $oper2 = $data2['disabled'] ? '隐藏' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'广告『'.$data2['name'].'』位置';
        $tips2.= $state;
        cms_writelog($tips2, ['spaceid' => $info['spaceid']]);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 位置配置
     */
    public final function setting () {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $setting = I('post.setting', [], 'cms_addslashes');
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $data = $this->space->where($where)->find();
        
        $temp = unserialize($data['setting']);
        $setting['type'] = $temp['type'];
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $ress = $this->space->where($where)->save([
            'setting'=>serialize($setting)
        ]);
        $tips = '配置广告『'.$info['name'].'』位置';
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $data = $this->space->where($where)->find();
        $info['template']   = $data['template'];
        
        $view = APP_PATH.MODULE_NAME.'/View/Template/';
        $view.= $info['template'].'View.html';
        
        $this->assign('data', $data);
        $this->assign('view', $view);
        
        $this->assign('setting', unserialize($data['setting']));
        $this->display();
    }
    
    /**
     * 广告调用
     */
    public final function call() {
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $data = $this->space->where($where)->find();
        
        $link               = '';
        $args               = [];
        $args['spaceid']    = $info['spaceid'];
        $code = '<script language="javascript" src="';
        $code.= $link . U('index/poster', $args).'">';
        $code.= '</script>';
        
        $this->assign('code', $code);
        $this->assign('name', $data['name']);
        $this->display();
    }
    
    /**
     * 浏览广告
     */
    public final function views() {
        $spaceid = I('get.spaceid', 0, 'intval');
        $info               = [];
        $info['spaceid']    = $spaceid;
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        $data = $this->space->where($where)->find();
        
        $view = $data['template'];
        $setting = unserialize($data['setting']);
        
        $limit              = $setting['number'];
        
        $where              = [];
        $where['spaceid']   = $info['spaceid'];
        
        $where['starttime'] = ['ELT', time()];
        $where['endtime']   = ['EGT', time()];
        $where['disabled']  =  0;
        
        $ress = $this->advert->where($where)->limit($limit)->order(
            '`listorder` ASC, `advertid` DESC'
        )->select();
        $data = [];
        foreach ($ress as $key => $row) {
        $args = unserialize($row['setting']);
        $args['size'] = $setting['size'];
        $args['position'] = $setting['position'];
        $args['type'] = $row['type'];
        
        $func = 'cms_advert_'.$view;
        $data[$key]['key'] = $key;
        $data[$key]['val'] = $func($args);
        }
        $this->assign('data', $data);
        $this->display();
    }
    
}
