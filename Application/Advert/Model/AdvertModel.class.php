<?php

namespace Advert\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 广告模块内容数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class AdvertModel extends BaseModel {}
