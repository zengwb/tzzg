<?php

namespace Advert\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 广告模块位置数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class AdvertSpaceModel extends BaseModel {}
