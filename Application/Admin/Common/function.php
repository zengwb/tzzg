<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 获取指定类属方法的注释信息（名称空间/控制器名、操作事件）
 * 
 * @param string    $namespace  必填。名称空间
 * @param string    $action     必填。操作事件
 * 
 * @return string
 */
function cms_method_notes($namespace, $action) {
    $class = new \ReflectionMethod($namespace, $action);
    $methodcomment = $class->getDocComment();
    
    preg_match('|/\*.*?\*/|is', $methodcomment, $array);
    
    $notes = trim(strtr($array[0], ['/' => '', '*' => '']));
    return ['namespace' => $namespace, 'method' => $action,
            'note' => $notes
    ];
}
