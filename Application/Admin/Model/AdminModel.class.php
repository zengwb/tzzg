<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块用户数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class AdminModel extends BaseModel {
    
    /**
     * 获取满足条件的系统用户记录信息
     * 
     * @param array     $data       必填。用户数据（数组）
     * 
     * @return array
     */
    public final function oauth2($data) {
        $encrypt2 = C('OAUTH_CMSKEY');
        $password = cms_password($data['password'], $encrypt2);
        
        $where = [];
        $where['username'] = $data['username'];
        $where['password'] = $password;
        return $this->where($where)->find();
    }
    
    /**
     * 更新满足条件的系统用户信息（登录信息）
     * 
     * @param string    $username   必填。用户帐号
     */
    public final function uplogin($username) {
        $username = $username ? : session('CMS_UserName');
        
        $where = [];
        $where['username'] = $username;
        $ress1 = $this->where($where)->setInc('number');
        $ress2 = $this->where($where)->save([
            'logintime' => time(),
            'loginip'   => get_client_ip()
        ]);

        return ($ress1 && $ress2) ? true :false;
    }
    
    /**
     * 更新满足条件的系统用户信息（修改资料）
     * 
     * @param array     $data       必填。表单数据（数组）
     * 
     * @return boolean
     */
    public final function upinfo($data) {
        if (empty($data)) return false;
        
        $where = [];
        $where['username'] = session('CMS_UserName');
        return $this->where($where)->save($data);
    }
    
}
