<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块菜单数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class MenuModel extends BaseModel {
    
    /**
     * 返回创建菜单树形结构数据（数组）
     * 
     * @param array     $where      必填。条件
     * @param string    $field      选填。字段
     * @param string    $order      选填。排序
     * 
     * @return array
     */
    public final function treeList($where, $field = '', $order = '') {
        $field = $field ? : 'menuid,name,parentid';
        $order = $order ? : '`listorder` ASC, `menuid` ASC';
        
        $array = [];
        $ress2 = $this->where($where)->field($field)->order($order)
                      ->select();
        foreach ($ress2 as $key => $row) {
            $array[$row['menuid']] = $row;
        }
        return $array;
    }
    
    /**
     * 获取所有父级菜单ID
     * 
     * @param integer   $menuid     必填。菜单ID
     * 
     * @return string
     */
    public final function parentid($menuid) {
        $parentids  = '';
        $where      = ['menuid' => $menuid];
        
        $parentid   = $this->where($where)->find()['parentid'];
        $parentids  = $parentid;
        
        if ($parentid != 0) {
            $parentids.= ','.self::parentid($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子菜单ID（含当前菜单ID，ID间以逗号分隔）
     * 
     * @param integer   $menuid     必填。菜单ID
     * 
     * @return unknown|string|unknown
     */
    public final function childid($menuid) {
        $ids .= $menuid;
        $ress = $this->where(['parentid' => $menuid])->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) {
            $ids .= ','.$this->childid($row['menuid']);
        }
        return $ids;
    }
    
    /**
     * 整合菜单URL地址（生成完整的菜单URL链接地址）
     * 
     * @param array     $data       必填。菜单数据（数组）
     */
    public final function linkurl($data) {
        $link = '';
        $args = [];
        $temp = ['module', 'controller', 'action', 'params'];
        $data = cms_array_merge($temp, $data);
        
        $m = $data['module'] ? : '';
        $c = $data['controller'] ? : '';
        $a = $data['action'] ? : '';
        
        if (!empty($data['params'])) {
            $data['params'] = htmlspecialchars_decode($data['params']);
            $params = explode('&', $data['params']);
            
            foreach ($params as $key => $val) {
                list($param, $value) = explode('=', $val);
                $args[$param] = $value;
            }
        }
        if (!empty($a)) $link = strtolower(U($m.'/'.$c.'/'.$a, $args));
        return $link ? : '/';
    }
    
    /**
     * 获取菜单目录（所有上级菜单，不含父ID为0的菜单）
     * 
     * @param integer   $menuid    必填。菜单ID
     * 
     * @return array
     */
    public final function catalog($menuid) {
        $menures = [];
        $menustr = $menuid.','.$this->parentid($menuid);
        
        $menuarr = explode(',',$menustr);
        krsort($menuarr);
        
        foreach ($menuarr as $key => $menuid) {
            if (!$menuid) continue;
            $where = ['menuid' => $menuid];
            
            $menures['name'] .= $this->where($where)->find()['name'];
            $menures['name'] .= $key ? '_' : '';
            
            $menures['menuid'][] = $menuid;
        }
        return $menures;
    }
    
    /**
     * 获取指定条件的子菜单数据（数组）
     * 
     * @param integer   $parentid   选填。父菜单ID
     * @param integer   $isattr     选 填。菜单属性
     * @param boolean   $allmenu    选填。所有菜单（当前属性）
     * 
     * @return array
     */
    public final function menuList($parentid = 0, $isattr = 1,
        $allmenu = false) {
        $where              = [];
        $where['parentid']  = $parentid;
        $where['isattr']    = $isattr;
        $where['display']   =  1;
        
        $ress = $this->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        if (!$allmenu) return $ress ? : [];
        
        $data =  $ress;
        foreach ($ress as $key => $row) {
            if (!$row) continue;
            
            if (!$row['linkurl']) $row['url'] = self::linkurl($row);
            else $row['url'] = $row['linkurl'];
            
            $sub2 = self::menuList($row['menuid'], $isattr, $allmenu);
            $data[$key]['submenu'] = $sub2;
            $data[$key]['url'] = $row['url'];
        }
        return $data;
    }
    
}
