<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块角色数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class AdminRoleModel extends BaseModel {
    
    /**
     * 返回创建角色树形结构数据（数组）
     * 
     * @param array     $where      必填。条件
     * @param string    $field      选填。字段
     * @param string    $order      选填。排序
     * 
     * @return array
     */
    public final function treeList($where, $field = '', $order = '') {
        $field = $field ? : 'roleid,name,parentid';
        $order = $order ? : '`listorder` ASC, `roleid` ASC';
        
        $array = [];
        $ress2 = $this->where($where)->field($field)->order($order)
                      ->select();
        foreach ($ress2 as $key => $row) {
            $array[$row['roleid']] = $row;
        }
        return $array;
    }
    
    /**
     * 获取所有父级角色ID
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return string
     */
    public final function parentid($roleid) {
        $parentids  = '';
        $where      = ['roleid' => $roleid];
        
        $parentid   = $this->where($where)->find()['parentid'];
        $parentids  = $parentid;
        
        if ($parentid != 0) {
            $parentids.= ','.self::parentid($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子角色ID（含当前角色ID，ID间以逗号分隔）
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return unknown|string|unknown
     */
    public final function childid($roleid) {
        $ids .= $roleid;
        $ress = $this->where(['parentid' => $roleid])->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) {
            $ids .= ','.$this->childid($row['roleid']);
        }
        return $ids;
    }
    
    /**
     * 获取角色目录（所有上级角色，不含父ID为0的角色）
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function catalog($roleid) {
        $roleres = [];
        $rolestr = $roleid.','.$this->parentid($roleid);
        
        $rolearr = explode(',', $rolestr);
        krsort($rolearr);
        
        foreach ($rolearr as $key => $roleid) {
        if (!$roleid) continue;
        $where = ['roleid' => $roleid];
        
        $roleres['name'] .= $this->where($where)->find()['name'];
        $roleres['name'] .= $key ? '_' : '';
        
        $roleres['roleid'][] = $roleid;
        }
        return $roleres;
    }
    
}
