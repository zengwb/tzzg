<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块数据应用模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class DatabaseModel extends BaseModel {
    
    protected $autoCheckFields = false; // 关闭字段自动检测
    
    /**
     * 获取系统数据库表（数组）
     * 
     * @return mixed
     */
    public final function tables() {
        return $this->query('SHOW TABLES');
    }
    
    /**
     * 优化或修复系统数据库表
     * 
     * @param string    $operation  必填。操作类型
     * @param string    $table      必填。数据表名
     * 
     * @return mixed
     */
    public final function optimize($operation, $table) {
        $sql = strtoupper($operation).' TABLE '.$table;
        return $this->query($sql);
    }
    
    /**
     * 获取系统数据库表基本信息
     * 
     * @param string    $table      必填。数据表名
     * 
     * @return mixed
     */
    public final function status($table) {
        return $this->query('SHOW TABLE STATUS LIKE "'.$table.'"');
    }
    
    /**
     * 获取系统数据库表结构
     * 
     * @param string    $table      必填。数据表名
     */
    public final function structure($table) {
        return $this->query('SHOW CREATE TABLE '.$table);
    }
    
    /**
     * 生成SQL语句（插入语句）
     * 
     * @param string    $table      必填。数据表名
     * 
     * @return string
     */
    public final function insertsql($table) {
        $ress = $this->table($table)->select();
        $keys = $vals = $sqlv = '';
        
        foreach ($ress as $key => $row) {
//         $vals = array_map('cms_addslashes', array_values($row));
        $vals = array_values($row);
        $vals = '\''.join('\', \'', $vals).'\'';
        
        $sqlv.= 'INSERT INTO `'.$table.'` VALUES('.$vals.')';
        $sqlv.= ";\r\n";
        }
        return $sqlv;
    }
    
}
