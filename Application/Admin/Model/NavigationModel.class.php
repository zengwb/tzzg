<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块导航数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class NavigationModel extends BaseModel {
    
    /**
     * 返回创建导航树形结构数据（数组）
     * 
     * @param array     $where      必填。条件
     * @param string    $field      选填。字段
     * @param string    $order      选填。排序
     * 
     * @return array
     */
    public final function treeList($where, $field = '', $order = '') {
        $field = $field ? : 'navigationid,name,parentid';
        $order = $order ? : '`listorder` ASC, `navigationid` ASC';
        
        $array = [];
        $ress2 = $this->where($where)->field($field)->order($order)
                      ->select();
        foreach ($ress2 as $key => $row) {
            $array[$row['navigationid']] = $row;
        }
        return $array;
    }
    
    /**
     * 获取所有父级导航ID
     * 
     * @param integer   $navigationid   必填。导航ID
     */
    public final function parentid($navigationid) {
        $parentids  = '';
        $where      = ['navigationid' => $navigationid];
        
        $parentid   = $this->where($where)->find()['parentid'];
        $parentids .= $parentid;
        
        if ($parentid != 0) {
            $parentids.= ','.self::parentid($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子导航ID（含当前导航ID，ID间以逗号分隔）
     * 
     * @param integer   $navigationid   必填。导航ID
     * 
     * @return string
     */
    public final function childid($navigationid) {
        $ids .= $navigationid;
        $ress = $this->where(['parentid' => $navigationid])->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) {
            $ids .= ','.$this->childid($row['navigationid']);
        }
        return $ids;
    }
    
    /**
     * 获取导航目录（所有上级导航，不含父ID为0的导航）
     * 
     * @param integer   $navigationid   必填。导航ID
     * 
     * @return array
     */
    public final function catalog($navigationid) {
        $navigationres = [];
        $navigationstr = $navigationid.','.$this->parentid($navigationid);
        
        $navigationarr = explode(',', $navigationstr);
        krsort($navigationarr);
        
        foreach ($navigationarr as $key => $navigationid) {
            if (!$navigationid) continue;
            $where = ['navigationid' => $navigationid];
            
            $navigationres['name'] .= $this->where($where)->find()['name'];
            $navigationres['name'] .= $key ? '_' : '';
            
            $navigationres['navigationid'][] = $navigationid;
        }
        return $navigationres;
    }
    
}
