<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块权限数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class AdminRolePrivModel extends BaseModel {
    
    /**
     * 获取菜单ID
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function menuid($roleid) {
        $array = [];
        $ress2 = $this->where(['roleid' => $roleid])->select();
        
        foreach ($ress2 as $key => $row) {
            $array[] = $row['menuid'];
        }
        return $array;
    }
    
    /**
     * 获取权限（菜单ID）
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function privs($roleid) {
        $menu2 = D('Admin/Menu');
        $role2 = D('Admin/AdminRole');
        
        $where = [];
        $where['roleid'] = $roleid;
        
        $data2 = [];
        $ress2 = $this->where($where)->select();
        foreach ($ress2 as $key => $row) {
            $data2[$key] = $row['menuid'];
        }
        if ($data2) return $data2;
        
        $ress2 = $role2->where($where)->find();
        if ($ress2['parentid']) {
            return $this->privs($ress2['parentid']);
        }
    }
    
}
