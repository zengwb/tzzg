<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块菜单数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class LinkageModel extends BaseModel {
    
    /**
     * 返回创建 树形结构数据（数组）
     * 
     * @param array     $where      必填。条件
     * @param string    $field      选填。字段
     * @param string    $order      选填。排序
     * 
     * @return array
     */
    public final function treeList($where, $field = '', $order = '') {
        $field = $field ? : 'linkageid,name,parentid';
        $order = $order ? : '`listorder` ASC, `linkageid` ASC';
        
        $index = explode(',', $field)[0];
        
        $array = [];
        $ress2 = $this->where($where)->field($field)->order($order)
                      ->select();
        foreach ($ress2 as $key => $row) {
            $array[$row[$index]] = $row;
        }
        return $array;
    }
    
    /**
     * 获取所有父级菜单ID
     * 
     * @param integer   $linkageid  必填。菜单ID
     * 
     * @return string
     */
    public final function parentid($linkageid) {
        $parentids  = '';
        $where      = ['linkageid' => $linkageid];
        
        $parentid   = $this->where($where)->find()['parentid'];
        $parentids  = $parentid;
        
        if ($parentid != 0) {
            $parentids.= ','.self::parentid($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子菜单ID（含当前菜单ID，ID间以逗号分隔）
     * 
     * @param integer   $linkageid  必填。菜单ID
     * 
     * @return string
     */
    public final function childid($linkageid) {
        $ids .= $linkageid;
        $ress = $this->where(['parentid' => $linkageid])->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) {
            $ids .= ','.$this->childid($row['linkageid']);
        }
        return $ids;
    }
    
    /**
     * 获取当前子菜单ID（ID间以逗号分隔）
     * 
     * @param integer   $linkageid  必填。菜单ID
     * @param integer   $subsetid   选填。子集ID
     * 
     * @return unknown|string|unknown
     */
    public final function thischildid($linkageid, $subsetid = 0) {
        $ids .= $linkageid;
        $where = [];
//         $where['parentid'] = $linkageid;
        $where['parentid'] = ['IN', $linkageid];
        $where['display']  =  1;
        if ($subsetid) $where['subsetid'] = $subsetid;
        
        $ress = $this->where($where)->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) $ids .= ','.$row['linkageid'];
        return $ids;
    }
    
    /**
     * 获取菜单目录（所有上级菜单，不含父ID为0的菜单）
     * 
     * @param integer   $linkageid  必填。菜单ID
     * 
     * @return array
     */
    public final function catalog($linkageid) {
        $linkres = [];
        $linkstr = $linkageid.','.$this->parentid($linkageid);
        
        $linkarr = explode(',', $linkstr);
        krsort($linkarr);
        
        foreach ($linkarr as $key => $linkageid) {
            if (!$linkageid) continue;
            $where = ['linkageid' => $linkageid];
            
            $linkres['name'] .= $this->where($where)->find()['name'];
            $linkres['name'] .= $key ? '_' : '';
            
            $linkres['linkageid'][] = $linkageid;
        }
        return $linkres;
    }
    
    /**
     * 获取联动菜单ID
     * 
     * @param string    $key        必填。联动菜单名称（部分）
     * @param number    $linkageid  选填。联动菜单ID
     */
    public final function get_attribute_id($key, $linkageid = 0) {
        $where = [];
        $where['name'] = ['LIKE', $key.'%'];
        if ($linkageid) $where['parentid'] = $linkageid;
        
        return $this->where($where)->find()['linkageid'];
    }
    
    /**
     * 获取联动菜单父ID
     * 
     * @param number    $linkageid  必填。联动菜单ID
     */
    public final function get_attribute_parentid($linkageid) {
        return $this->where(['linkageid' => $linkageid])->find()['parentid'];
    }
}
