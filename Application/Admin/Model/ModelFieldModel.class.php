<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块字段管理模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class ModelFieldModel extends BaseModel {
    
    /**
     * 判断已有模型主（从）数据表字段是否存在
     * 
     * @param string    $table      必填。模型数据库表
     * @param string    $field      必填。待验证字段项
     * 
     * @return boolean
     */
    public final function fieldchk($table, $field) {
        $fress = [];
        $table = C('DB_PREFIX').$table;
        $array = $this->query('SHOW COLUMNS FROM `'.$table.'`');
        
        foreach ($array as $key => $row) {
            $fress[] = $row['field'];
        }
        $model = D('Admin/Model');
        if ($model->tablechk($table, [C('DB_PREFIX') => ''])) {
        $array = $this->query('SHOW COLUMNS FROM `'.$table.'`');
        
        foreach ($array as $key => $row) {
            $fress[] = $row['field'];
        }
        }
        if (in_array($field, $fress)) return true;
        else return false;
    }
    
    /**
     * 插入模型字段
     * 
     * @param string    $table      必填。模型数据库表
     * @param string    $field      必填。待添加的字段
     * @param array     $setting    必填。模型字段配置
     */
    public final function insert($table, $field, $setting) {
        $table = C('DB_PREFIX').$table;
        $ftype = $setting['fieldtype'];
        $setting['decimaldigits'] = $setting['decimaldigits'] ? : '0';
        
        if ($ftype == 'double' || $ftype  == 'float') {
            if ($setting['decimaldigits'] == 0) {
                $setting['fieldtype'] = 'int';
                $length = '('.$setting['length'].')';
            } else {
                $length = '('.$setting['length'].',';
                $length.= $setting['decimaldigits'].')';
            }
        } else if ($ftype == 'text') {
            $length = '';
        } else {
            $length = '('.$setting['length'].')';
        }
        $default = $setting['default'];
        if ($default || $default == '0' || $default == 0) {
            $default = " DEFAULT '{$default}'";
        }
        if ($ftype == 'text') unset($default);
        
        $query  = 'ALTER TABLE `'. $table .'` ADD `'.$field.'` ';
        $query .= $setting['fieldtype'] . $length;
        $query .= $setting['unsigned'] ? ' UNSIGNED ' : '';
        $query .= $setting['isnull'] ? '' : ' NOT NULL ';
        $query .= $default.' ';
        $query .= "COMMENT '{$setting['comment']}'";
        
        $this->execute($query);
    }
    
    /**
     * 修改模型字段
     * 
     * @param string    $table      必填。模型数据库表
     * @param string    $field      必填。待修改的字段
     * @param string    $newfield   必填。新的模型字段
     * @param array     $setting    必填。模型字段配置
     */
    public final function update($table, $field, $newfield, $setting) {
        $table = C('DB_PREFIX').$table;
        $ftype = $setting['fieldtype'];
        $setting['decimaldigits'] = $setting['decimaldigits'] ? : '0';
        
        if ($field == $newfield) {
            $field = ' MODIFY `'.$field.'` ';
        } else {
            $field = ' CHANGE `'.$field.'` `'.$newfield.'` ';
        }
        if ($ftype == 'double' || $ftype == 'float') {
            if ($setting['decimaldigits'] == 0) {
                $setting['fieldtype'] = 'int';
                $length = '('.$setting['length'].')';
            } else {
                $length = '('.$setting['length'].',';
                $length.= $setting['decimaldigits'].')';
            }
        } else if ($ftype == 'text') {
            $length = '';
        } else {
            $length = '('.$setting['length'].')';
        }
        $default = $setting['default'];
        if ($default || $default == '0' || $default == 0) {
            $default = " DEFAULT '{$default}'";
        }
        if ($ftype == 'text') unset($default);
        
        $query  = 'ALTER TABLE `'. $table .'` '.$field;
        $query .= $setting['fieldtype'] . $length;
        $query .= $setting['unsigned'] ? ' UNSIGNED ' : '';
        $query .= $setting['isnull'] ? '' : ' NOT NULL ';
        $query .= $default.' ';
        $query .= "COMMENT '{$setting['comment']}'";
        
        $this->execute($query);
    }
    
    /**
     * 删除模型字段
     * 
     * @param string    $table      必填。模型数据库表
     * @param string    $field      必填。待删除字段项
     */
    public final function drop($table, $field) {
        if (!$this->fieldchk($table, $field)) return ;
        $table = C('DB_PREFIX') .$table;
        
        $query = 'ALTER TABLE `'.$table.'` DROP `'.$field.'`';
        $this->execute($query);
    }
    
}
