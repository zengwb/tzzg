<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块单页数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class PagesModel extends BaseModel {}
