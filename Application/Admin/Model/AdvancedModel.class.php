<?php

namespace Admin\Model;

use \Common\Model\AbstractAdvancedModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块高级数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
abstract class AdvancedModel extends AbstractAdvancedModel {}
