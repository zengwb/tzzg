<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块表单元素模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class FormsModel extends BaseModel {}
