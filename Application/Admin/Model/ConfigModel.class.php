<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块配置数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class ConfigModel extends BaseModel {
    
    /**
     * 获取模块配置参数（数组）
     * 
     * @param string    $module     必填。模块
     * @param string    $group      选填。分组
     */
    public final function parameter($module, $group = '') {
        $where = [];
        $where['disabled']  = 0;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['module']    = $module ? : MODULE_NAME;
        
        if (!empty($group)) $where['group'] = $group;
        $order = '`listorder` ASC';
        
        return $this->where($where)->order($order)->select();
    }
    
}
