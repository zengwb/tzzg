<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块事业单位数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class InstitutionModel extends BaseModel {
    
    /**
     * 返回创建树形结构数组
     * 
     * @param string|array $where   必填。查询条件
     * @param string    $field      选填。显示字段
     * @param string    $order      选填。显示排序
     * 
     * @return unknown[]
     */
    public final function get_unit_tree($where, $field = '', $order = '') {
        $field = $field ?: 'unitid,name,parentid';
        $order = $order ?: 'listorder ASC, unitid ASC';
        
        $index = explode(',', $field)[0];
        $ress2 = $this->where($where)->field($field)->order($order)->select();
        $array = [];
        foreach ($ress2 as $key => $row) {
            $array[$row[$index]] = $row;
        }
        return $array;
    }
    
    /**
     * 获取所有父级单位ID
     * 
     * @param number    $unitid     必填。单位ID
     */
    public final function get_unit_parentid($unitid) {
        $parentids  = '';
        $where      = ['unitid' => $unitid];
        
        $parentid   = $this->where($where)->find()['parentid'];
        $parentids  = $parentid;
        
        if ($parentid) {
            $parentids .= ','.self::get_unit_parentid($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子单位ID（含当前单位ID，ID间以逗号分隔）
     * 
     * @param number    $unitid     必填。单位ID
     * @param number    $linkageid  选填。菜单ID
     * 
     * @return unknown|string|unknown
     */
    public final function get_unit_childid($unitid, $linkageid = 0) {
        $ids .= $unitid;
        $ress = $this->where(['parentid' => $unitid, 'linkageid' => $linkageid])->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) {
//             $ids .= ','.$this->get_unit_childid($row['unitid'], $linkageid);
            $ids .= ','.$row['unitid'];
        }
        return $ids;
    }
    
    /**
     * 获取单位目录（所有上级单位，不含父ID为的单位
     * 
     * @param number    $unitid     必填。单位ID
     */
    public final function get_unit_catalog($unitid) {
        $unitres = [];
        $unitstr = $unitid.','.$this->get_unit_parentid($unitid);
        
        $unitarr = explode(',', $unitstr);
        krsort($unitarr);
        
        foreach ($unitarr as $key => $unitid) {
            if (!$unitid) continue;
            $where = ['unitid' => $unitid];
            
            $unitres['name'] .= $this->where($where)->find()['name'];
            $unitres['name'] .= $key ? '_' : '';
            
            $unitres['unitid'][] = $unitid;
        }
        return $unitres;
    }
    
}
