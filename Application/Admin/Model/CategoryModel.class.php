<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块栏目数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class CategoryModel extends BaseModel {
    
    /**
     * 返回创建栏目树形结构数据（数组）
     * 
     * @param array     $where      必填。条件
     * @param string    $field      选填。字段
     * @param string    $order      选填。排序
     * 
     * @return array
     */
    public final function treeList($where, $field = '', $order = '') {
        $field = $field ? : 'catid,catname,parentid';
        $order = $order ? : '`listorder` ASC, `catid` ASC';
        
        $type2 = ['内部栏目', '外部链接', '单独页面'];
        $array = [];
        $ress2 = $this->where($where)->field($field)->order($order)
                      ->select();
        foreach ($ress2 as $key => $row) {
            $row['type'] = $type2[$row['type']];
            $array[$row['catid']] = $row;
        }
        return $array;
    }
    
    /**
     * 获取所有父栏目ID
     * 
     * @param integer   $catid      选填。栏目ID
     * 
     * @return string
     */
    public final function parentids($catid) {
        $where              = [];
        $where['siteid']    = cms_siteid();
        $where['catid']     = $catid;
        $categorys = $this->where($where)->find();
        
        $parentids = '';
        $parentids.= $parentid = $categorys['parentid'];
        
        if ($parentid != 0) {
            $parentids.= ','.self::parentids($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子栏目ID（含当前栏目ID，ID间以逗号分隔）
     * 
     * @param integer   $catid      选填。栏目ID
     * @param integer   $siteid     选填。站点ID
     * 
     * @return string
     */
    public final function childid($catid = 0, $siteid = 0) {
        $arrid = '';
        $where = ['catid' => $catid, 'siteid' => $siteid];
        
        if ($siteid == 0) unset($where['siteid']);
        
        $child = $this->where($where)->find();
//         if ($child) $arrid .= $catid;
        $arrid .= $catid;
        
        $where = ['parentid' => $catid, 'siteid' => $siteid];
        if ($siteid == 0) unset($where['siteid']);
        
        $array = $this->where($where)->select();
        if (!$array) return $arrid;
        
        foreach ($array as $key => $row) {
            $arrid .= ','.$this->childid($row['catid'], $siteid);
        }
        return $arrid;
    }
    
    /**
     * 获取栏目目录（所有上级栏目，不含父ID为0的栏目）
     * 
     * @param integer   $catid      选填。栏目ID
     * 
     * @return array
     */
    public final function catalog($catid) {
        $catres = [];
        $catstr = $catid.','.$this->parentids($catid);
        
        $caturl = cms_config_array('Article', 'base')['cat_link'];
        $catarr = explode(',', $catstr);
        krsort($catarr);
        
        foreach ($catarr as $key => $catid) {
        if (!$catid) continue;
        $where = ['catid' => $catid];
        $ress = $this->where($where)->find();
        
        if ($caturl) {
            $catres['link'].= $ress['catdir'].'/';
        } else {
            $catres['link'] = 'index.php';
            $catres['link'].= '?m=home&c=index&a=lists&catid=';
            $catres['link'].= $catid;
        }
        $catres['catid'][]= $catid;
        }
        return $catres;
    }
    
}
