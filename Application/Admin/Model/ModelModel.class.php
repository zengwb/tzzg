<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块系统应用模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class ModelModel extends BaseModel {
    
    /**
     * 判断数据库表是否存在
     * 
     * @param string    $table      必填上。待判断的数据库表
     * 
     * @return boolean
     */
    public final function tablechk($table) {
        $db_table = $this->query('SHOW TABLES');
        foreach ($db_table as $key => $val) {
            $tables[] = $val['tables_in_'.C('DB_NAME')];
        }
        if (in_array(C('DB_PREFIX').$table, $tables)) return true;
        else return false;
    }
    
    /**
     * 创建模型数据表
     * 
     * @param string    $module     必填。模块名称（标识）
     * @param integer   $modelid    必填。数据模型ID
     * @param string    $table      必填。待创建的数据表
     * @param string    $name       必填。待创建的表名称
     * @param integer   $data       选填。是否创建副表（数据）
     * 
     * @return boolean
     */
    public final function create_model($module, $modelid,
        $table, $name = '', $data = 0) {
        $array = [];
        $array['{$base_table}'] = C('DB_PREFIX').$table;
        
        $array['{$modelid}']    = $modelid;
        $array['{$db_prefix}']  = C('DB_PREFIX');
        $array['{$comment}']    = $name;
        
        $model = C('MODEL_DBPATH').$module.'_base_model.sql';
        if (!file_exists($model))  $module = 'temp';
        $model = C('MODEL_DBPATH').$module.'_base_model.sql';
        
        $query = file_get_contents($model);
        $this->execute(strtr($query, $array));
        
        if (!$data) return true;
        $array['{$data_table}'] = C('DB_PREFIX').$table.'_data';
        
        $model = C('MODEL_DBPATH').$module.'_data_model.sql';
        $query = file_get_contents($model);
        $this->execute(strtr($query, $array));
    }
    
    /**
     * 导入模型数据表
     * 
     * @param integer   $modelid    必填。数据模型ID
     * @param array     $model      必填。模型数据（数组）
     * 
     * @return boolean
     */
    public final function import_model($modelid, $model = []) {
        if (empty($model)) return false;
        $fieldModel = D('Admin/ModelField');
        $modelTable = C('DB_PREFIX').'model_field';
        
        $where = ['modelid' => $modelid];
        $table = $this->where($where)->find()['table'];
        
        $query = $keys = $vals = '';
        foreach ($model as $key => $row) {
            
        if ($row['maxlength'] == 0) $length = $row['minlength'];
        else $length = $row['maxlength'];
        
        $param = [];
        $param['fieldtype'] = $row['setting']['fieldtype'];
        $param['length'] = $length ? : 1;
        $param['decimaldigits'] = $row['setting']['decimaldigits'];
        $param['isnull'] = $row['minlength'] ? false : true;
        $param['default'] = $row['setting']['default'];
        $param['unsigned'] = $row['setting']['unsigned'];
        $param['comment']= $row['alias'];
        
        $table = $row['ismain'] ? $table : $table.'_data';
        $check = $fieldModel->fieldchk($table, $row['field']);
        
        if ($check)
        $fieldModel->update($table, $row['field'], $row['field'], $param);
        else
        $fieldModel->insert($table, $row['field'], $param);
        
        $row['modelid'] = $modelid;
        $row['setting'] = serialize($row['setting']);
        unset($row['fieldid']);
        
        $keys = array_map('cms_addslashes', array_keys($row));
        $keys = '`'.join('`,`', $keys).'`';
//         $vals = array_map('cms_addslashes', array_values($row));
        $vals = array_values($row);
        $vals = '\''.join('\', \'', $vals).'\'';
        
        $query .= 'INSERT INTO `'.$modelTable.'`('.$keys.') ';
        $query .= 'VALUES('.$vals.')'.";\r\n";
        }
        $this->execute($query);
    }
    
    /**
     * 修改模型数据表
     * 
     * @param string    $table      必填。原数据库表
     * @param string    $toTable    必填。新数据库表
     * @param integer   $data       选填。是否更新副表
     * 
     * @return boolean
     */
    public final function update_model($table, $toTable, $data = 0) {
        if ($table == $toTable) return false;
        
        $table      = C('DB_PREFIX').$table;
        $toTable    = C('DB_PREFIX').$toTable;
        
        $this->execute('ALTER TABLE `'.$table.'` RENAME `'.$toTable.'`');
        if (!$data) return true;
        
        $table     .= '_data';
        $toTable   .= '_data';
        if (!$this->tablechk(strtr($table, [C('DB_PREFIX') => '']))) {
            return false;
        }
        $this->execute('ALTER TABLE `'.$table.'` RENAME `'.$toTable.'`');
    }
    
    /**
     * 删除模型数据表
     * 
     * @param string    $table      必填。待删除的数据库表
     * 
     * @return boolean
     */
    public final function delete_model($table) {
        $table  = C('DB_PREFIX').$table;
        $this->execute('DROP TABLE IF EXISTS `'.$table.'`');
        
        $table .= '_data';
        if (!$this->tablechk(strtr($table, [C('DB_PREFIX') => '']))) {
            return false;
        }
        $this->execute('DROP TABLE IF EXISTS `'.$table.'`');
    }
    
}
