<?php

namespace Admin\Model;

use \Common\Model\AbstractModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块基础数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
abstract class BaseModel extends AbstractModel {}
