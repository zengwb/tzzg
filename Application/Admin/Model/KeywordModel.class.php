<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块敏感数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class KeywordModel extends BaseModel {
    
    /**
     * 获取敏感词汇数据（数组）
     * 
     * @return array
     */
    public final function lists() {
        $keywords = $this->order('`inputtime` DESC')->select();
        
        foreach ($keywords as $key => $row) {
        if ($row['level']) $row['replace'] = '**';
        unset($row['keyid'], $row['level'], $row['inputtime']);
        $array[$row['keyword']] = $row['replace'];
        }
        return $array;
    }
    
}
