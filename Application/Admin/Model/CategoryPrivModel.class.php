<?php

namespace Admin\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块栏目权限模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class CategoryPrivModel extends BaseModel {
    
    /**
     * 获取栏目ID
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function catid($roleid) {
        $array = [];
        $ress2 = $this->where(['roleid' => $roleid])->select();
        
        foreach ($ress2 as $key => $row) {
            $array[] = $row['catid'];
        }
        return $array;
    }
    
    /**
     * 获取栏目（导航ID）
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function privs($roleid) {
        $cate2 = D('Admin/Category');
        $role2 = D('Admin/AdminRole');
        
        $where = [];
        $where['roleid'] = $roleid;
        
        $data2 = [];
        $ress2 = $this->where($where)->select();
        foreach ($ress2 as $key => $row) {
            $data2[$key] = $row['catid'];
        }
        if ($data2) return $data2;
        
        $ress2 = $role2->where($where)->find();
        if ($ress2['parentid']) {
            return $this->privs($role2['parentid']);
        }
    }
    
}
