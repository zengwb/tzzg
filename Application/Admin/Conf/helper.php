<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Admin',
    'version'               => 'v1.0.01',
    
    'name'                  => 'Admin',
    'description'           => '管理模块',
    'core'                  => true,
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
];
