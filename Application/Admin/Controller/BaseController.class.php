<?php

namespace Admin\Controller;

use \Common\Controller\iBaseController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块助手控制器类：配置或实例化应用模块
 * 
 * @author T-01
 */
abstract class BaseController extends iBaseController {
    
    public      $action     = [];
    
    protected   $username   = '',
                $userid     =  0,
                $roleid     =  0,
                $menuid     =  0,
                
                $oauth2     = [],
                $domain     = '';
    
    private     $menuModel  = [],
                $roleModel  = [],
                $privModel  = [],
                
                $userModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iBaseController::_initialize()
     */
    public function _initialize() {
        $this->username     = session('CMS_UserName');
        $this->userid       = session('CMS_UserID');
        $this->roleid       = session('CMS_RoleID');
        
        $actionArray2       = session('CMS_ActionArray');
        if (!empty($actionArray2)) $this->action = $actionArray2;
        
        $this->userModel    = D('Admin/Admin');
        $this->roleModel    = D('Admin/AdminRole');
        
        $this->privModel    = D('Admin/AdminRolePriv');
        $this->menuModel    = D('Admin/Menu');
        
        $safety = cms_config_array('Admin', 'safety');
        $domain = $_SERVER['SERVER_NAME'];
        $prompt = '域名地址无效，没有管理权限！';
        if ($domain != $safety['admin_url'] && !empty($safety['admin_url']))
        $this->error($prompt, '', 'javascript:void(0);', 9999);
        
        $adminu = $safety['admin_url'] ? : $domain;
        C('ADMIN_URL', 'http://'.$adminu);
        $this->domain       = substr(cms_site_info()['domain'], 0, -1);
        
        $username           = $this->username;
        $userid             = $this->userid;
        $roleid             = $this->roleid;
        
        if (empty($username) && empty($userid) && empty($roleid))
        if (MODULE_NAME == C('ADMIN_MODULE')) {
        $this->error('会话已过期，请重新登陆！', '', U('login/index'));
//         echo '
//         <script language="javascript" type="text/javascript">
//             top.location.href="'.U('login/index').'";
//         </script>'; exit();
        } else {
        $this->error('会话已过期，请重新登陆！', '', U('admin/login/index'));
//         echo '
//         <script language="javascript" type="text/javascript">
//             top.location.href="'.U('admin/login/index').'";
//         </script>'; exit();
        }
        $where              = [];
        $where['roleid']    = $this->roleid;
        $ress = $this->roleModel->where($where)->find();
        $this->menuid = $ress['menuid'];
        
        $this->action($this->action);
        $this->dialog(MODULE_NAME.CONTROLLER_NAME.ucwords(ACTION_NAME));
        
        $this->oauth2 = (new   \Common\ORG\OAuth2());
        $this->oauth2->_initialize( $this->username);
        $ress = $this->oauth2->action($this->action);
        
        if (empty($this->action)) return ; // @todo:
        
        $role = !in_array($this->roleid, C('SUPER_ROLEID'));
        $user = !in_array($this->userid, C('SUPER_USERID'));
        
        if (!empty($ress) && ($role || $user)) {
            $this->error($ress['tips'],$ress['data'],'',99);
        }
    }
    
}
