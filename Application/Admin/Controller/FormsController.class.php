<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块表单元素控制器类：呈现元素CURD常规管理
 * 
 * @author T-01
 */
final class FormsController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'setting',
        'state',
    ];
    
    protected   $formModel  =   [],
                $form       = null;
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->formModel    = D('Forms');
        $this->form = (new \Common\Widget\FormsWidget());
    }
    
    /**
     * 表单元素
     */
    public final function index() {
        $where = [];
        $lists = $this->formModel->where($where)->order(
            '`listorder` ASC, `elementid` ASC'
        )->select();
        
        $this->assign('data', $lists);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['elementid'] as $key => $elementid) {
        $listorder = $this->formModel->where([
            'elementid' => $elementid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->formModel->where([
            'elementid' => $elementid
        ])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $elementid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('表单元素排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加元素
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['alias'] = trim($data['alias']);
        
        if ($data['alias'] && empty($data['description'])) {
            $data['description'] = $data['alias'];
        }
        // 判断元素名称
        $temp = ['elements' => $data['elements']];
        $ress = $this->formModel->where($temp)->find();
        
        $tips = '表单元素『'.$data['alias'].'』已经存在！';
        if ( $ress) $this->error($tips);
        
        $inid = $this->formModel->add($data);
        $tips = '添加表单『'.$data['alias'].'』元素';
        if (!$inid) $this->error($tips.'失败！');
        
        $this->formModel->where(['elementid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', '', U('index'));
        } // @todo:
        
        $checkup = I('get.checkup', 0, 'intval');
        $frmlist = [];
        $i = 0;
        
        foreach ($this->form->elements as $element => $alias) {
        $tmp = ['elements' => $element];
        $res = $this->formModel->where($tmp)->find();
        if ($res) continue;
        
        $frmlist[$i]['elements'] = $element;
        $frmlist[$i]['alias'] = $alias;
        $i++;
        }
        
        $this->assign('frmlist', $checkup ? $frmlist : []);
        $this->assign('data', ['disabled' => 0]);
        $this->display('action');
    }
    
    /**
     * 编辑元素
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['alias'] = trim($data['alias']);
        
        if ($data['alias'] && empty($data['description'])) {
            $data['description'] = $data['alias'];
        }
        if ($info['alias'] == $data['alias']) {
            $alias = $info['alias'];
        } else {
            $alias = $info['alias'].'→'.$data['alias'];
        }
        $where = ['elements' => $info['elements']];
        $ress = $this->formModel->where($where)->save($data);
        
        $tips = '更新表单『'.$alias.'』元素';
        $info = ['elements' => $info['elements']];
        
        if (!$ress) $this->error($tips.'失败！', $info);
        else $this->success($tips.'成功！', $info, U('index'));
        } // @todo: 
        
        $elementid = I('get.elementid', 0);
        
        $temp = ['elementid' => $elementid];
        $data = $this->formModel->where($temp)->find();
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 删除元素
     */
    public final function delete() {
        $elementid = I('get.elementid', 0, 'intval');
        $info = I('post.info', []);
        $info = $elementid ? [$elementid] : $info['elementid'];
        $data = [];
        if (empty($info)) $this->error('请选择表单元素！');
        
        foreach ($info as $key => $elementid) {
            $data['elementid'][] = $elementid;
        }
        $elementid = implode(',', $data['elementid']);
        
        $where = ['elementid' => ['IN', $elementid]];
        $ress = $this->formModel->where($where)->delete();
        $temp = 'elementid: ' . $elementid;
        
        if (!$ress) $this->error('删除表单元素失败！', $temp);
        else $this->success('删除表单元素成功！', $temp);
    }
    
    /**
     * 配置元素
     */
    public final function setting() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.setting', [], 'cms_addslashes');
        
        $where = ['elementid' => $info['elementid']];
        $ress = $this->formModel->where($where)->save([
            'setting'=>serialize($data)
        ]);
        $tips = '配置表单『'.$info['alias'].'』元素';
        
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo:
        
        $elementid = I('get.elementid', 0, 'intval');
        $info = [];
        $info['elementid'] = $elementid;
        
        $where = ['elementid' => $info['elementid']];
        $data = $this->formModel->where($where)->find();
        $setting = unserialize($data['setting']);
        
        $this->assign('data', $data);
        $this->assign('setting', $setting);
        
        $this->display();
    }
    
    /**
     * 元素状态
     */
    public final function state() {
        $elementid = I('get.elementid', 0, 'intval');
        $info = [];
        $info['elementid'] = $elementid;
        
        $where = ['elementid' => $info['elementid']];
        $ress2 = $this->formModel->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where = ['elementid' => $info['elementid']];
        $ress2 = $this->formModel->where($where)->save([
            'disabled'=>$value
        ]);
        $where = ['elementid' => $info['elementid']];
        $data2 = $this->formModel->where($where)->find();
        $oper2 = $data2['disabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'表单『'.$data2['alias'].'』元素'.$state;
        cms_writelog($tips2, ['elementid'=>$info['elementid']]);
        
        echo json_encode($data2['disabled']);
    }
    
}
