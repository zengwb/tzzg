<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块模型字段控制器类：呈现字段CURD常规管理
 * 
 * @author T-01
 */
final class FieldController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state',
        'shows', 'forms',
    ];
    
    private     $modelModel = [],
                $fieldModel = [],
                
                $formModel  = [],
                $userRoleModel = [],
                $roleModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->modelModel   = D('Admin/Model');
        $this->fieldModel   = D('Admin/ModelField');
        
        $this->formModel    = D('Admin/Forms');
        
//         $this->userRoleModel= D('Member/MemberRole');
        $this->roleModel    = D('Admin/AdminRole');
    }
    
    /**
     * 字段管理
     */
    public final function index() {
        $modelid = I('get.modelid', 0, 'intval');
        
        $where = ['modelid' => $modelid];
        $model = $this->modelModel->where($where)->find();
        
        $where = ['modelid' => $modelid];
        $field = $this->fieldModel->where($where)->order(
            '`listorder` ASC, `fieldid` ASC'
        )->select();

        $this->assign('modelid', $modelid);
        
        $this->assign('name', $model['name']);
        $this->assign('data', $field);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['fieldid'] as $key => $fieldid) {
        $listorder = $this->fieldModel->where([
            'fieldid' => $fieldid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->fieldModel->where(['fieldid' => $fieldid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $fieldid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('模型字段排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加字段
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', '', 'cms_addslashes');
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['setting'] = serialize($setting);
        $data['modelid'] = $info['modelid'];
        
        $data['usergroupids'] = implode(',', $data['usergroupids']);
        $data['roleids'] = implode(',', $data['roleids']);
        
        $issystem = isset($data['issystem']) && $data['issystem'] == 1;
        $data['issystem'] = $issystem ? 1 : 0;
        $ismain = isset($data['ismain']) && $data['ismain'] == 1;
        $data['ismain'] = $ismain ? 1 : 0;
        
        $isbase = isset($data['isbase']) && $data['isbase'] == 1;
        $data['isbase'] = $isbase ? 1 : 0;
        $isposition = isset($data['isposition']) && $data['isposition'] == 1;
        $data['isposition'] = $isposition ? 1 : 0;
        $issearch = isset($data['issearch']) && $data['issearch'] == 1;
        $data['issearch'] = $issearch ? 1 : 0;
        
        $where = [];
        $where['modelid']   = $info['modelid'];
        $where['field']     = $data['field'];
        $field = $this->fieldModel->where($where)->find();
        
        $where = [];
        $where['modelid']   = $info['modelid'];
        $model = $this->modelModel->where($where)->find();
        
        $check = $this->fieldModel->fieldchk(
            $model['table'], $data['field']
        );
        $tips2 = '系统『'.$model['name'].'』字段『'.$data['field'].'』已存在！';
        if ($field || $check) $this->error($tips2);
        
        $table = $model['table'];
        if ($data['maxlength'] == 0) $length = $data['minlength'];
        else $length = $data['maxlength'];
        if ($data['ismain'] == 0) $table .= '_data';
        
        $param = [];
        $param['fieldtype'] = $setting['fieldtype'];
        $param['length'] = $length ? : 1;
        $param['decimaldigits'] = $setting['decimaldigits'];
        
        $param['default']= $setting['default'];
        $param['isnull'] = $setting['minlength'] ? false : true;
        
        $param['unsigned'] = $setting['unsigned'];
        $param['comment']= $data['alias'];
        
        $this->fieldModel->insert($table, $data['field'], $param);
        
        $tips2 = '添加『'.$model['name'].'』字段『'.$data['field'].'』';
        $inid2 = $this->fieldModel->add($data);
        
        $this->fieldModel->where(['fieldid' => $inid2])->save([
            'listorder' => $inid2
        ]);
        $link2 = U('index', ['modelid' => $info['modelid']]);
        if (!$inid2) $this->error($tips2.'失败！');
        else $this->success($tips2.'成功！', $data, $link2);
        } // @todo: 
        
        $modelid = I('get.modelid', 0, 'intval');
        
        $where = ['modelid' => $modelid];
        $model = $this->modelModel->where($where)->find();
        
        $where = ['disabled' => 0];
        $forms = $this->formModel->where($where)->order(
            '`listorder` ASC, `elementid` ASC'
        )->select();
        
        // 用户角色
        $usergroupids = [];
        
        $where = [];
        $where['disabled']  = 0;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $roles = $this->roleModel->where($where)->order(
            '`listorder` ASC, `roleid` ASC'
        )->select();
        
        $data = [];
        $data['modelid']    = $modelid;
        $data['fieldid']    = 0;
        $data['issystem']   = 1;
        $data['ismain']     = 1;
        $data['isbase']     = 1;
        $data['isposition'] = 0;
        $data['issearch']   = 0;
        $data['display']    = 0;
        $data['disabled']   = 0;
        $data['usergroupids'] = [];
        $data['roleids']    = [];
        
        $this->assign('modelname', $model['name']);
        $this->assign('modeldata', $model['data']);
        
        $this->assign('modelid', $modelid);
        $this->assign('data', $data);
        
        $this->assign('forms', $forms);
        $this->assign('usergroupids', $usergroupids);
        $this->assign('roleids', $roles);
        
        $this->display('action');
    }
    
    /**
     * 修改字段
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', '', 'cms_addslashes');
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['setting'] = serialize($setting);
        $data['modelid'] = $info['modelid'];
        
        $data['usergroupids'] = implode(',', $data['usergroupids']);
        $data['roleids'] = implode(',', $data['roleids']);
        
        $issystem = isset($data['issystem']) && $data['issystem'] == 1;
        $data['issystem'] = $issystem ? 1 : 0;
        $ismain = isset($data['ismain']) && $data['ismain'] == 1;
        $data['ismain'] = $ismain ? 1 : 0;
        
        $isbase = isset($data['isbase']) && $data['isbase'] == 1;
        $data['isbase'] = $isbase ? 1 : 0;
        $isposition = isset($data['isposition']) && $data['isposition'] == 1;
        $data['isposition'] = $isposition ? 1 : 0;
        $issearch = isset($data['issearch']) && $data['issearch'] == 1;
        $data['issearch'] = $issearch ? 1 : 0;
        
        $where = [];
        $where['modelid']   = $info['modelid'];
        $where['field']     = $data['field'];
        $field = $this->fieldModel->where($where)->find();
        
        $where = [];
        $where['modelid']   = $info['modelid'];
        $model = $this->modelModel->where($where)->find();
        
        $check = $this->fieldModel->fieldchk(
            $model['table'], $data['field']
        );
        $tips2 = '系统『'.$model['name'].'』字段『'.$data['field'].'』已存在！';
        if ($info['field'] != $data['field'] &&
           ($field || $check)) $this->error($tips2);
        
        $table = $model['table'];
        if ($data['maxlength'] == 0) $length = $data['minlength'];
        else $length = $data['maxlength'];
        if ($data['ismain'] == 0) $table .= '_data';
        
        $param = [];
        $param['fieldtype'] = $setting['fieldtype'];
        $param['length'] = $length ? : 1;
        $param['decimaldigits'] = $setting['decimaldigits'];
        
        $param['default']= $setting['default'];
        $param['isnull'] = $setting['minlength'] ? false : true;
        
        $param['unsigned'] = $setting['unsigned'];
        $param['comment']= $data['alias'];
        $this->fieldModel->update(
            $table, $info['field'], $data['field'], $param
        );
        
        if ($info['field'] == $data['field']) $name = $info['field'];
        else $name = $info['field'].'→'.$data['field'];
        
        $where = [];
        $where['modelid'] = $info['modelid'];
        $where['fieldid'] = $info['fieldid'];
        $ress2 = $this->fieldModel->where($where)->save($data);
        
        $tips2 = '修改『'.$model['name'].'』字段『'.$name.'』';
        $link2 = U('index', ['modelid' => $info['modelid']]);
        
        if (!$ress2) $this->error($tips2.'失败！');
        else $this->success($tips2.'成功！', $data, $link2);
        } // @todo:
        
        $modelid = I('get.modelid', 0, 'intval');
        $fieldid = I('get.fieldid', 0, 'intval');
        
        $where = [];
        $where['fieldid'] = $fieldid;
        
        $data = $this->fieldModel->where($where)->find();
        $data['usergroupids'] = explode(',', $data['usergroupids']);
        $data['roleids'] = explode(',', $data['roleids']);
        $modelid = $modelid ? : $data['modelid'];
        
        $where = [];
        $where['modelid'] = $modelid;
        $model = $this->modelModel->where($where)->find();
        
        $where = ['disabled' => 0];
        $forms = $this->formModel->where($where)->order(
            '`listorder` ASC, `elementid` ASC'
        )->select();
        
        // 用户角色
        $usergroupids = [];
        
        $where = [];
        $where['disabled']  = 0;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $roles = $this->roleModel->where($where)->order(
            '`listorder` ASC, `roleid` ASC'
        )->select();
        
        $this->assign('modelname', $model['name']);
        $this->assign('modeldata', $model['data']);
        
        $this->assign('modelid', $modelid);
        $this->assign('data', $data);
        
        $this->assign('forms', $forms);
        $this->assign('usergroupids', $usergroupids);
        $this->assign('roleids', $roles);
        
        $this->display('action');
    }
    
    /**
     * 删除字段
     */
    public final function delete() {
        $fieldid = I('get.fieldid', 0, 'intval');
        $info = I('post.info', []);
        $info = $fieldid ? [$fieldid] : $info['fieldid'];
        $data = [];
        if (empty($info)) $this->error('请选择模型字段！');
        
        foreach ($info as $key => $fieldid) {
        $data['fieldid'][]  =  $fieldid;
        $where = ['fieldid' => $fieldid];
        $field = $this->fieldModel->where($where)->find();
        
        $where = ['modelid' => $field['modelid']];
        $model = $this->modelModel->where($where)->find();
        
        $table = $model['table'];
        if (!$field['ismain']) $table .= '_data';
        $this->fieldModel->drop($table, $field['field']);
        }
        $fieldid = implode(',', $data['fieldid']);
        
        $where = ['fieldid' => ['IN', $fieldid]];
        $fress = $this->fieldModel->where($where)->delete();
        
        $ftemp = ['fieldid' => $fieldid];
        if (!$fress) $this->error('删除模型字段失败！', $ftemp);
        else $this->success('删除模型字段成功！', $ftemp);
    }
    
    /**
     * 字段状态
     */
    public final function state() {
        $fieldid = I('get.fieldid', 0, 'intval');
        $info = [];
        $info['fieldid'] = $fieldid;
        
        $where = ['fieldid' => $info['fieldid']];
        $field = $this->fieldModel->where($where)->find();
        $value = $field['disabled'] ? 0 : 1;
        
        $where = ['fieldid' => $info['fieldid']];
        $fress = $this->fieldModel->where($where)->save([
            'disabled'=>$value
        ]);
        $where = ['fieldid' => $info['fieldid']];
        $field = $this->fieldModel->where($where)->find();
        $foper = $field['disabled'] ? '禁用' : '启用';
        
        if (!$fress) $state = '失败！';
        else $state = '成功！';
        $where = ['modelid' => $field['modelid']];
        $model = $this->modelModel->where($where)->find();
        
        $ftips = $foper.'系统『'.$model['name'].'』模块『';
        $ftips.= $field['alias'].'』字段'.$state;
        cms_writelog($ftips, ['fieldid' => $info['fieldid']]);
        
        echo json_encode($field['disabled']);
    }
    
    /**
     * 显示状态
     */
    public final function shows() {
        $fieldid = I('get.fieldid', 0, 'intval');
        $info = [];
        $info['fieldid'] = $fieldid;
        
        $where = ['fieldid' => $info['fieldid']];
        $field = $this->fieldModel->where($where)->find();
        $value = $field['display'] ? 0 : 1;
        
        $where = ['fieldid' => $info['fieldid']];
        $fress = $this->fieldModel->where($where)->save([
            'display' => $value
        ]);
        
        $where = ['fieldid' => $info['fieldid']];
        $field = $this->fieldModel->where($where)->find();
        $foper = $field['display'] ? '显示' : '隐藏';
        
        if (!$fress) $state = '失败！';
        else $state = '成功！';
        $where = ['modelid' => $field['modelid']];
        $model = $this->modelModel->where($where)->find();
        
        $ftips = $foper.'系统『'.$model['name'].'』模块『';
        $ftips.= $field['alias'].'』字段'.$state;
        cms_writelog($ftips, ['fieldid' => $info['fieldid']]);
        
        echo json_encode($field['display']);
    }
    
    /**
     * 表单元素（Ajax）
     */
    public final function forms() {
        $fieldid = I('get.fieldid', 0, 'intval');
        $element = I('get.formtype', '');
        
        $where = ['fieldid' => $fieldid];
        $field = $this->fieldModel->where($where)->find();
        
        $where = ['elements'=> $element];
        $forms = $this->formModel->where($where)->find();
        $setting = $field['setting'] ?  :
                   $forms['setting'] ;
        
        $this->assign('setting', unserialize($setting));
        W('Common/Forms/element',['label' => $element]);
    }
    
}
