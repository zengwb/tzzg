<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块主控制器类：呈现主界面、欢迎界面及注销管理
 * 
 * @author T-01
 */
final class IndexController extends BaseController {
    
    public      $action     = [
        'index', 'initialize', 'bootstrap', 'logout',
    ];
    private     $userModel  = [],
                $roleModel  = [],
                
                $menuModel  = [],
                $privModel  = [],
                
                $siteModel  = [],
                $tabsModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iBaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->userModel    = D('Admin/Admin');
        $this->roleModel    = D('Admin/AdminRole');
        
        $this->menuModel    = D('Admin/Menu');
        $this->privModel    = D('Admin/AdminRolePriv');
        
        $this->siteModel    = D('Admin/Website');
        $this->tabsModel    = D('Admin/AdminPanel');
    }
    
    /**
     * 管理主界面
     */
    public final function index() { $this->redirect('initialize'); }
    /**
     * 初始化界面
     */
    public final function initialize() {
        $priv = $this->oauth2->operPriv();
        if (empty($priv)) {
            $this->username = $this->username ? : 'unknown';
            
            $tips = '当前用户『'.$this->username.'』没有权限。请联系管理员！';
            $data = ['username' => $this->username];
            $this->error($tips, $data, U('login/index'));
        }
        $where              =   [];
        $where['username']  = $this->username;
        $data = $this->userModel->where($where)->find();
        
        $where              =   [];
        $where['parentid']  = $this->menuid;
        $where['isattr']    =    1;
        $where['display']   =    1;
        $where['menuid']    = ['IN', $priv];
        $navi = $this->menuModel->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        
        $priv = explode(',', $priv);
        $ress = $this->menuModel->menuList($this->menuid, 1, true);
        
        $menu = [];
        foreach ($ress as $k1 => $v1) {
            if (!in_array($v1['menuid'], $priv)) continue;
            foreach ($v1['submenu'] as $k2 => $v2) {
                if (!in_array($v2['menuid'], $priv))
                     unset($v1['submenu'][$k2]);
                foreach ($v2['submenu'] as $k3 => $v3)
                    if (!in_array($v3['menuid'], $priv))
                         unset($v1['submenu'][$k2]['submenu'][$k3]);
            }
            $menu[$k1] = $v1;
        }
        $data['username'] = $data['realname'] ?: $data['username'];
        
        $where              = [];
        $where['userid']    = $this->userid;
        $panel = $this->tabsModel->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        
        $where              = [];
        $where['roleid']    = $this->roleid;
        $sites = $this->roleModel->where($where)->find()['siteids'];
        
        $where              = [];
        $where['display']   =  1;
        if ($sites) $where['siteid'] = ['IN', $sites];
        
        $sites = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $this->assign('data', $data);
        $this->assign('navi', $navi);
        $this->assign('menu', $menu);
        $this->assign('look', cms_iplookup());
        $this->assign('tabs', $panel);
        $this->assign('site', cms_site_info());
        $this->assign('sites',$sites);
        $this->display();
    }
    
    /**
     * 切换站点
     */
    public final function tabsite() {
        $siteid = I('get.siteid', 0, 'intval');
        
        $where              = [];
        $where['siteid']    = $siteid;
        $array = $this->siteModel->where($where)->find();
        
        if ($array) session('CMS_SiteID', $array['siteid']);
        echo json_encode($array);
    }
    
    /**
     * 引导页面（系统导航）
     */
    public final function bootstrap() {
        $this->assign('site', cms_site_info()); // 获取站点信息
        $this->display();
    }
    
    /**
     * 注销登录（安全退出）
     */
    public final function logout() {
        $username = ucwords($this->username);
//         $username = $this->username;
        cms_writelog(
            $username.'，注销登录！', ['username' => $username],
            2
        );
        session('[destroy]');
        session_unset();    // 释放系统所有SESSION变量
        session_destroy();  // 销毁系统所有SESSION会话
        
        $this->success(
            $username.'，安全退出！', '', U('login/index'), 5,
            false, 2
        ); // @todo: 
    }
    
}
