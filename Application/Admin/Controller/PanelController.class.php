<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块面板管理控制器类：呈现面板CURD常规管理
 * 
 * @author T-01
 */
final class PanelController extends BaseController {
    
    public      $action     = [];
    
    private     $menuModel  = [],
                $tabsModel  = [],
                $tree       = null,
                $field      = ['menuid', 'name', 'parentid'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->tabsModel    = D('Admin/AdminPanel');
        $this->menuModel    = D('Admin/Menu');
    }
    
    /**
     * 面板管理
     */
    public final function index() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', []);
        
        $where              = [];
        $where['userid']    = $info['userid'];
        $this->tabsModel->where($where)->delete();
        
        foreach ($data['menuid'] as $key => $menuid) {
        $where              = [];
        $where['menuid']    = $menuid;
        
        $menu = $this->menuModel->where($where)->find();
        $link = $this->menuModel->linkurl($menu);
        
        $this->tabsModel->add([
            'menuid'        => $menu['menuid'],
            'userid'        => $info['userid'],
            'name'          => $menu['name'],
            'link'          => $link,
            'listorder'     => $menu['listorder'],
            'addtime'       => time()
        ]);
        }
        $this->popup(true, '设置快捷导航');
        } // @todo: 
        
        $oauth = $this->oauth2->operPriv();
        $where              = [];
        $where['display']   =  1;
        
        if (empty($oauth)) $where['menuid'] = 0;
        else $where['menuid'] = ['IN', $oauth];
        
        $menus = $this->menuModel->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        $array = [];
        foreach ($menus as $key => $row) {
        $links = $this->menuModel->linkurl($row);
        $links = $row['linkurl'] ? : $links;
        
        $row['links'] = cms_stripslashes($links);
        $row['links'] = substr($row['links'], 0, 42).'...';
        $row['links'] = strtr($row['links'], ['/...' => '/']);
        $row['state'] = $row['links'] !=  '/' ? : ' disabled';
        $array[$row['menuid']] = $row;
        }
        $where              = [];
        $where['userid']    = $this->userid;
        $ress = $this->tabsModel->where($where)->select();
        foreach ($ress as $key => $row) $sarr[] = $row['menuid'];
        
        $text = "<tr>
        <th class='list-checkbox'>
        <input class='checkchild' name='data[menuid][]'
        value='\$menuid' type='checkbox' \$checked \$state />
        </th>
        <td class='list-small'>\$menuid</td>
        <td data-menuid='\$menuid'>
        \$spacer\$name
        <span class='cms-cccc'>　\$description</span>
        </td>
        <td class='cms-tc'>后台菜单</td>
        </tr>";
        $tree = cms_tree_menu(
            $array, 1001,
            $text, '', $sarr, ['field' => $this->field]
        );
        $this->assign('info', ['userid' => $this->userid]);
        $this->assign('tree', $tree);
        $this->display('select');
    }
    
}
