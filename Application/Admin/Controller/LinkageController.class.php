<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块联动菜单控制器类：呈现菜单CURD常规管理
 * 
 * @author T-01
 */
final class LinkageController extends BaseController {
    
    public      $action     = [
        'index', 'lists', 'listorder', 'add', 'addAll', 'edit',
        'delete', 'state', 'move',
    ];
    
    private     $siteModel  =   [],
                $menuModel  =   [],
                $tree       = null,
                $field      = ['linkageid', 'name', 'parentid'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->siteModel = D('Admin/Website');
        $this->menuModel = D('Admin/Linkage');
        
        session('CMS_POPUP', null);
    }
    
    /**
     * 菜单管理
     */
    public final function index() {
        $keyword    = I('request.key', null, 'cms_addslashes');
        $keyword    = I('get.key', $keyword, 'cms_addslashes');
        $keyword    = I('post.key',$keyword, 'cms_addslashes');
        $keyword    = trim($keyword);
        
        $where      = [];
        $where['subsetid'] = 0;
        $order      = '`listorder` ASC, `linkageid` ASC';
        if (!empty($keyword)) {
            $where['name']      = ['LIKE', '%'.$keyword.'%'];
        } else {
            $where['parentid']  = 0;
        }
        $where['siteids'] = ['IN', [0, cms_siteid()]];
        
        $nums = $this->menuModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->menuModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key  => $row) {
            if ($keyword) {
                $row['title'] = strtr($row['name'], $keys);
            } else {
                $row['title'] = $row['name'];
            }
            $data[$key]       = $row;
            
//             $childid = $this->menuModel->childid($row['linkageid']);
//             $childid = strtr($childid, [$row['linkageid'] => null]);
            
//             if ($childid) {
//                 $link = U('lists', ['parentid' => $row['linkageid']]);
//                 $link = 'yhcms.common.linkurl(\''.$link.'\');';
                
//                 $data[$key]['_child']   =  true;
//                 $data[$key]['_link']    = $link;
//             } else {
//                 $link = '';
//                 $data[$key]['_child']   = false;
//                 $data[$key]['_link']    = 'void(0);';
//             }
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 子项管理
     */
    public final function lists() {
        $linkageid = I('get.parentid', 0, 'intval');
        $subsetid = I('get.subsetid', 0, 'intval');
        
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['subsetid']   = $subsetid;
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['subsetid']  = $info['subsetid'];
        
        $ress = $this->menuModel->where($where)->find();
        $name = $ress['name'];
        
        $where              = [];
        $where['parentid']  = $info['linkageid'];
        $where['subsetid']  = $info['subsetid'];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        if (in_array($this->roleid, C('SUPER_ROLEID'))) {
            unset($where['siteid']);
        }
        $ress = $this->menuModel->where($where)->order(
            '`listorder` ASC, `linkageid` ASC'
        )->select();
        $data = [];
        foreach ($ress as $key => $row) {
            $where = [];
            $where['linkageid'] = $row['parentid'];
            $where['subsetid']  = $info['subsetid'];
            $ress  = $this->menuModel->where($where)->find();
            $row['parent'] = $ress['name'] ?: '顶级菜单';
            
//             $linkid = $this->menuModel->childid($row['linkageid']);
//             $count  = count(explode(',', $linkid));
            
//             if ($count) {
                $link = U('lists', ['subsetid' => $info['subsetid'], 'parentid' => $row['linkageid']]);
                $link = 'yhcms.common.linkurl(\''.$link.'\');';
                $row['_link'] = $link;
//             } else {
//                 $link = '';
//                 $row['_link'] = 'void(0);';
//             }
            $row['child'] = 1; // $count > 1 ? : 0;
            $data[$key]   = $row;
        }
        $list = $this->menuModel->catalog($info['linkageid'])['name'];

        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('name', $name);
        $this->assign('list', $list);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], '');
        $list = [];
        
        foreach ($data['linkageid'] as $i => $linkageid) {
        $where = ['linkageid' => $linkageid];
        $ress2 = $this->menuModel->where($where)->find();
        
        $listorder = $ress2['listorder'];
        if ($listorder == $data['listorder'][$i]) {
            continue;
        }
        $ress2 = $this->menuModel->where($where)->save([
            'listorder'=> $data['listorder'][$i]
        ]);
        $listorder = $linkageid.'='.$data['listorder'][$i];
        $list['listorder'][] = $listorder;
        }
        $listorder = $list ? : 'null';
        self::success('菜单排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加菜单
     */
    public final function add() {
        session('CMS_POPUP', "AdminSubset");
        
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        $data['subsetid'] = $info['subsetid'];
        
        $tips = '联动菜单『'.$data['name'].'』同名，当前操作';
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['subsetid']  = $data['subsetid'];
        $where['name']      = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($ress) $this->popup(false, $tips, $data);;
        
        if ( empty($data['description']) &&
            !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $inid = $this->menuModel->add($data);
        
        $tips = '添加联动『'.$data['name'].'』菜单';
        if (!$inid) $this->popup(false, $tips, $data);
        
        $this->menuModel->where(['linkageid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->popup(true, $tips, $data, null,  3);
        } // @todo:
        
        $parentid = I('get.parentid', 0, 'intval');
        $subsetid = I('get.subsetid', 0, 'intval');
        
        $info               = [];
        $info['parentid']   = $parentid;
        $info['subsetid']   = $subsetid;
        $info['linkageid']  = $info['parentid'];
        
        $data               = [];
        $data['parentid']   = $info['parentid'];
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['subsetid']  = $info['subsetid'];
        $where['siteids']   = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        
        if (in_array($this->roleid, C('SUPER_ROLEID'))) {
            unset($where['siteids']);
        }
        if (!empty($info['parentid'])) {
            $linkageid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $linkageid];
        }
        $temp = $this->menuModel->treeList($where);
        
        $text = "<option value='\$linkageid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [], ['field' => $this->field]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display('action');
    }
    
    /**
     * 批量添加
     */
    public final function addAll() {
        session('CMS_POPUP', "AdminSubset");
        
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $info = I('post.info', []);
        
        $array = explode("\n", $data['nameall']);
        unset($data['nameall']);
        
        $tips = '批量添加联动菜单';
        foreach ($array as $key => $name) {
        if (empty($name)) continue;
        $data['name'] = $name;
        $data['subsetid'] = $info['subsetid'];
        
        $where = [];
        $where['parentid'] = $data['parentid'];
        $where['subsetid'] = $data['subsetid'];
        $where['name'] = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($ress) continue;
        $data['description'] = $data['name'];
        
        $inid = $this->menuModel->add($data);
        
        $this->menuModel->where(['linkageid' => $inid])->save([
            'listorder' => $inid
        ]);
        }
        $this->popup(true, $tips, $data, null,  3);
        } // 请求结束
        
        $parentid = I('get.parentid', 0, 'intval');
        $subsetid = I('get.subsetid', 0, 'intval');
        $info = [];
        $info['parentid'] = $parentid;
        $info['subsetid'] = $subsetid;
        
        $where = [];
        $where['parentid'] = $info['parentid'];
        $where['subsetid'] = $info['subsetid'];
        $where['siteids'] = ['IN', [0, cms_siteid()]];
        $where['display'] = 1;
        
        if (in_array($this->roleid, C('SUPER_ROLEID'))) {
            unset($where['siteids']);
        }
        if (!empty($info['parentid'])) {
            $linkageid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $linkageid];
        }
        $temp = $this->menuModel->treeList($where);
        
        $text = "<option value='\$linkageid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [], ['field' => $this->field]
        );
        
        $this->assign('info', $info);
        $this->assign('tree', $tree);
        $this->display();
    }
    
    /**
     * 编辑菜单
     */
    public final function edit() {
        session('CMS_POPUP', "AdminSubset");
        
        if (IS_POST && I('post.dosubmit')) {
        // 请求开始
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        $data['subsetid'] = $info['subsetid'];
        
        $tips = '联动菜单『'.$data['name'].'』同名，当前操作';
        
        $where = [];
        $where['parentid'] = $data['parentid'];
        $where['subsetid'] = $data['subsetid'];
        $where['name'] = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($info['name'] != $data['name'] &&
            $ress) $this->popup(false, $tips, $data);
        
        if ($info['name'] != $data['name']) {
            $name = $info['name'].'→'.$data['name'];
        } else {
            $name = $data['name'];
        }
        $where = [];
        $where['linkageid'] = $info['linkageid'];
        $ress = $this->menuModel->where($where)->save($data);
        
        $tips = '更新联动『'.$name.'』菜单';
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, '', 3);
        } // @todo: 
        
        $linkageid = I('get.linkageid', 0, 'intval');
        $subsetid = I('get.subsetid', 0, 'intval');
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['subsetid']   = $subsetid;
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['subsetid']  = $info['subsetid'];
        
        $link = $this->menuModel->catalog($info['linkageid'])['linkageid'];
        $data = [];
        $data = $this->menuModel->where($where)->find();
        $data = cms_stripslashes($data);
        
        $temp               = [];
        $temp['linkageid']  = $info['linkageid'];
        $info['parentid']   = $data['parentid'] ? $link[0] : 0;
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['subsetid']  = $info['subsetid'];
        
        $where['linkageid'] = ['NEQ', $info['linkageid']];
        $where['_logic']    =  'AND';
        $where['linkageid'] = ['NEQ', $temp['linkageid']];
        
        $where['display']   =  1;
        
        if (in_array($this->roleid, C('SUPER_ROLEID'))) {
            unset($where['siteids']);
        }
        if (!empty($info['parentid'])) {
            $linkageid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $linkageid];
        }
        $temp = $this->menuModel->treeList($where);
        
        $text = "<option value='\$linkageid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [$data['parentid']], ['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display('action');
    }
    
    /**
     * 删除菜单
     */
    public final function delete() {
        $linkageid = I('get.linkageid', 0, 'intval');
        $subsetid  = I('get.subsetid' , 0, 'intval');
        
        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';
        
        $info = $linkageid ? [$linkageid] : $info['linkageid'];
        if (empty($info)) $this->error('请选择菜单！');
        
        foreach ($info as $key => $linkageid) {
            $temp .= $this->menuModel->childid($linkageid).',';
        }
        $data['linkageid'] = explode(',', $temp);
        
        $temp = ['linkageid' => ['IN', $temp]];
        $ress = $this->menuModel->where($temp)->delete();
        
        if (!$ress) $this->error('删除菜单失败！', $data);
        else $this->success('删除菜单成功！', $data);
    }
    
    /**
     * 更新状态
     */
    public final function state() {
        $linkageid = I('get.linkageid', 0, 'intval');
        
        $where = ['linkageid' => $linkageid];
        $data2 = $this->menuModel->where($where)->find();
        $state = $data2['display'] ? 0 : 1;
        
        $child = $this->menuModel->childid($linkageid);
        $array = explode(',', $child);
        
        foreach ($array as $key => $linkageid) {
            $this->menuModel->where(['linkageid' => $linkageid])
                 ->save(['display' => $state]);
        }
        $data2 = $this->menuModel->where($where)->find();
        $oper2 = $data2['display'] ? '启用' : '禁用';
        
        $tips2 = $oper2.'联动『'.$data2['name'].'』菜单';
        cms_writelog($tips2.'成功！', ['linkageid' => $child]);
        
        $array = ['linkageid' => $array, 'state' => $state];
        echo json_encode($array);
    }
    
    /**
     * 移动菜单
     */
    public final function move() {
        session('CMS_POPUP', "AdminSubset");
        
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['subsetid'] = $info['subsetid'];
        
        $where              = [];
        $where['linkageid'] = $data['parentid'];
        $where['subsetid']  = $info['subsetid'];
        $name = $this->menuModel->where($where)->find()['name'];
        $name = $name ? : '顶级菜单';
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['subsetid']  = $info['subsetid'];
        $temp = $this->menuModel->where($where)->find();
        
        $tips = '移动联动『'.$temp['name'].'』到『'.$name.'』菜单';
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['subsetid']  = $info['subsetid'];
        $ress = $this->menuModel->where($where)->save($data);
        
        $data               = [];
        $data['parentid']   = $temp['parentid'].'='.$ress['parentid'];
        $data['linkageid']  = $info['linkageid'];
        
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, null, 3);
        } // @todo: 
        
        $linkageid = I('get.linkageid', 0, 'intval');
        $subsetid = I('get.subsetid', 0, 'intval');
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['subsetid']   = $subsetid;
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['subsetid']  = $info['subsetid'];
        
        $link = $this->menuModel->catalog($info['linkageid'])['linkageid'];
        $data = [];
        $data = $this->menuModel->where($where)->find();
        
        $data = cms_stripslashes($data);
        $info['parentid']   = $data['parentid'] ? $link[0] : 0;
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['linkageid'] = ['NEQ', $info['linkageid']];
        $where['subsetid']  = $info['subsetid'];
        $where['siteids']   = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        
        if (in_array($this->roleid, C('SUPER_ROLEID'))) {
            unset($where['siteids']);
        }
        if (!empty($info['parentid'])) {
            $linkageid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $linkageid];
        }
        $ress = $this->menuModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $key => $row) {
            $temp[$row['linkageid']] = $row;
        }
        $text = "<option value='\$linkageid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [$data['parentid']], ['field' => $this->field]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }

    /**
     * 添加国家部委联动介绍图，在display页面（LinkageListsView.html）绑定了linkageid=3773
     */
    public final function image(){
        session('CMS_POPUP', "AdminSubset");
        if(IS_POST){

            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize   =     3145728 ;// 设置附件上传大小
            $upload->subName = array('date','Ymd');
            $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            $upload->rootPath  =     './Temporary/Attachment/Units/'; // 设置附件上传根目录
            // 上传单个文件
            $info   =   $upload->uploadOne($_FILES['photo']);

            if(!$info) {// 上传错误提示错误信息
                $this->popup(true, "图片上传失败", null, null, 3);
            }else{// 上传成功
                $path = $upload->rootPath.$info['savepath'].$info['savename'];
                $data['image'] = $path;
                $save = D("Admin/Linkage")->where(I('get.'))->save($data);

                if($save){
                    $this->popup(true, "图片上传", null, null, 3);
                }else{
                    $this->popup(false, "图片上传失败", null, null, 3);
                }
            }
        } // @todo:

        $where = [
            'linkageid' => I('get.')['linkageid'],
            'parentid' => I('get.')['parentid']
        ];

        $data = D("Admin/Linkage")->where($where)->find();

        $image = $data['image'] ? : "未上传图片";
        $this->assign('data', I('get.'));
        $this->assign('image',$image);
        $this->display();
    }
}
