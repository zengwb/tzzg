<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块角色管理控制器类：呈现角色CURD常规管理
 * 
 * @author T-01
 */
final class RoleController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state',
        'stateAll', 'move',
    ];
    
    protected   $userModel  = [],
                $siteModel  = [],
                
                $roleModel  = [],
                $privModel  = [],
                $menuModel  = [],
                
                $tree       = null,
                $field      = ['roleid', 'name', 'parentid'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->roleModel    = D('AdminRole');
        $this->privModel    = D('AdminRolePriv');
        $this->menuModel    = D('Menu');
        
        $this->userModel    = D('Admin');
        $this->siteModel    = D('Website');
    }
    
    /**
     * 角色管理
     */
    public final function index() {
        $parentid = I('get.parentid', 0, 'intval');
        
        $data = $temp = [];
        $info = ['parentid' => $parentid];
        
        $role = $this->roleModel->childid($parentid);
        
        $where              = [];
        $where['parentid']  = ['IN', $role];
        
        $ress = $this->roleModel->treeList($where, '*');
        foreach ($ress as $key => $row) {
            $tmp2 = in_array($row['roleid'], C('SUPER_ROLEID'));
            
            $row['_disabled'] = $tmp2 ? 'disabled' : '';
            $css2 = $row['disabled'] ? 'display'  : '';
            $row['_css2'] = $css2;
            
            if ($row['disabled']) {
            $row['_state'] = '<a class="list-operation"
            data-state="'.$row['roleid'].'"
            href="javascript:void(0);" title="点击启用角色">
            <i class="iconfont icon-qingchu"></i></a>';
            } else {
            $row['_state'] = '<a class="list-operation"
            data-state="'.$row['roleid'].'"
            href="javascript:void(0);" title="点击禁用角色">
            <i class="iconfont icon-qiyong"></i></a>';
            }
            
            $url = U('add', ['parentid' => $row['roleid']]);
            $row['_oper']  = '<a href="javascript:void(0);"
            onClick="javascript:yhcms.common.linkurl(\''.
            $url.'\');" title="添加子项">添加</a> ' ;
            
            $url = U('edit', ['roleid' => $row['roleid']]);
            $row['_oper'] .= '<a href="javascript:void(0);"
            onClick="javascript:yhcms.common.linkurl(\''.
            $url.'\');" title="修改角色">修改</a> ';
            
            $url = U('priv/rolePriv', ['roleid' => $row['roleid']]);
            $row['_oper'] .= $tmp2 ?
            '<a href="javascript:void(0);" class="cms-cccc">权限</a> ' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(
            \''.$url.'\', \'设置【'.$row['name'].'】权限\',
            \'AdminPrivRolePriv-0-640-480\');"
            title="权限设置">权限</a> ';
            
            $url = U('priv/roleMenu', ['roleid' => $row['roleid']]);
            $row['_oper'] .= $tmp2 ?
            '<a href="javascript:void(0);" class="cms-cccc">栏目</a> ' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(
            \''.$url.'\', \'设置【'.$row['name'].'】栏目\',
            \'AdminPrivRoleMenu-0-640-480\');"
            title="栏目权限">栏目</a> ';
            
            $url = U('admin/index', ['roleid' => $row['roleid']]);
            $row['_oper'] .= '<a href="javascript:void(0);"
            onClick="javascript:yhcms.common.linkurl(
            \''.$url.'\');" title="用户管理">用户</a> ';
            
            $url = U('move', ['roleid' => $row['roleid']]);
            $row['_oper'] .= $tmp2 ?
            '<a href="javascript:void(0);" class="cms-cccc">移动</a> ' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(\''.$url.'\',
            \'移动【'.$row['name'].'】角色\',
            \'AdminRoleMove-0-480-132\');" title="移动角色">移动</a> ';
            
            $url = U('delete', ['roleid' => $row['roleid']]);
            $row['_oper'] .= $tmp2 ?
            '<a href="javascript:void(0);" class="cms-cccc">删除</a>' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.tips(\''.$url.'\',
            \'确认删除【'.$row['name'].'】用户角色！\');"
            title="删除角色">删除</a> ';
            
            $temp[$row['roleid']] = $row; // 获取角色记录
        }
        $text = "<tr class='\$_css2'>
            <th class='list-checkbox'>
            <input class='checkchild' name='info[roleid][]'
            value='\$roleid' type='checkbox' \$_disabled />
            </th>
            <td class='list-small'>\$roleid</td>
            <td class='list-listorder'>
            <input type='hidden' name='data[roleid][]'
            value='\$roleid' />
            <input type='text' name='data[listorder][]'
            value='\$listorder' maxlength='4' autocomplete='off'
            class='form-control input-sm list-input-listorder' />
            </td>
            <td data-roleid='\$roleid'>
            \$spacer<a>\$name</a></td>
            <td class='cms-c999'>\$description</td>
            <td class='cms-tc icon-color'>\$_state</td>
            <td class='cms-tc'>\$_oper</td>
        </tr>";

        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [], ['field' => $this->field]
        );

        $this->assign('info', $info);
        $this->assign('data', $temp);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['roleid'] as $i => $roleid) {
            $where = ['roleid' => $roleid];
            $ress2 = $this->roleModel->where($where)->find();
            
            $listorder = $ress2['listorder'];
            if ($listorder == $data['listorder'][$i]) {
                continue;
            }
            $ress2 = $this->roleModel->where($where)->save([
                'listorder' => $data['listorder'][$i]
            ]);
            $listorder = $roleid.'='.$data['listorder'][$i];
            $list['listorder'][] = $listorder;
        }
        $listorder = $list ? : 'null';
        self::success('角色排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加角色
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        $data['siteids'] = implode(',', $data['siteids']);
        $data['siteids'] = $data['siteids'] ? : '0';
        $tips = '用户角色『'.$data['name'].'』同名，当前操作';
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->roleModel->where($where)->find();
        if ($ress) $this->popup(false, $tips, $data);
        
        if ( empty($data['description']) &&
            !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $inid = $this->roleModel->add($data);
        
        $tips = '添加用户『'.$data['name'].'』角色';
        if (!$inid) $this->error($tips.'失败！', $data);
        
        $this->roleModel->where(['roleid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $parentid = I('get.parentid', 0, 'intval');
        
        $info               = [];
        $info['parentid']   = $parentid;
        $info['roleid']     = $info['parentid'];
        
        $where              = [];
//         $where['siteid']    = ['IN', [0]];
        $where['display']   =  1;
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $role = $this->roleModel->childid(0); // $info['parentid']
        
        $data               = [];
        $data['parentid']   = $info['parentid'];
        $data['siteids']    = [0];
        
        $where              = [];
        $where['parentid']  =  0;
        $where['isattr']    =  1;
        $where['display']   =  1;
        $menu = $this->menuModel->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        
        $where              = [];
        $where['parentid']  = ['IN', $role];
        $where['disabled']  =  0;
        
        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        
        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [$info['roleid']], ['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->assign('site', $site);
        $this->assign('menu', $menu);
        $this->assign('test', [0]);
        
        $this->display('action');
    }
    
    /**
     * 编辑角色
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        $data['siteids'] = implode(',', $data['siteids']);
        $tips = '用户角色『'.$data['name'].'』同名，当前操作';
        
        if ( empty($data['description']) &&
            !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->roleModel->where($where)->find();
        if ($info['name']  != $data['name'] &&
            $ress) $this->popup(false, $tips, $data);
        
        if ($info['name']  != $data['name']) {
            $name = $info['name'].'→'.$data['name'];
        } else {
            $name = $data['name'];
        }
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress = $this->roleModel->where($where)->save($data);
        
        $tips = '更新用户『'.$name.'』角色';
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $parentid = I('get.parentid', 0, 'intval');
        $roleid = I('get.roleid', 0, 'intval');
        
        $info               = [];
        $info['parentid']   = $parentid;
        $info['roleid']     = $roleid;
        
        $where              = [];
        $where['display']   =  1;
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        
        $data = [];
        $data = $this->roleModel->where($where)->find();
        $data['siteids'] = explode(',', $data['siteids']);
        
        $where              = [];
        $where['roleid']    = $data['parentid'];
        $ress = $this->roleModel->where($where)->find();
        $test = explode(',', $ress['siteids']);
        
        $info['name']       = $data['name'];
        $role = $this->roleModel->childid($info['parentid']);
        
        $where              = [];
        $where['parentid']  =  0;
        $where['isattr']    =  1;
        $where['display']   =  1;
        $menu = $this->menuModel->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        
        $where              = [];
        $where['roleid']    = ['NEQ', $info['roleid']];
        
        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [$data['parentid']], ['field' => $this->tree]
        );
        $disabled = in_array($info['roleid'],C('SUPER_ROLEID'));
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->assign('site', $site);
        $this->assign('menu', $menu);
        $this->assign('test', $test);
        $this->assign('disabled', $disabled);
        
        $this->display('action');
    }
    
    /**
     * 删除角色
     */
    public final function delete() {
        $roleid = I('get.roleid', 0, 'intval');
        
        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';
        
        $info = $roleid ? [$roleid] : $info['roleid'];
        if (empty($info)) $this->error('请选择角色！');
        
        foreach ($info as $key => $roleid) {
            if (in_array($roleid, C('SUPER_ROLEID')))
                continue;
            $temp .= $this->roleModel->childid($roleid).',';
            
            $where = ['roleid' => $roleid];
            $this->privModel->where($where)->delete();
            // @todo: 删除用户（暂时保留）
        }
        $data['roleid'] = explode(',', $temp);
        
        $temp = ['roleid' => ['IN', $temp]];
        $ress = $this->roleModel->where($temp)->delete();
        
        if (!$ress) $this->error('删除角色失败！', $data);
        else $this->success('删除角色成功！', $data);
    }
    
    /**
     * 更新角色（单一操作）
     */
    public final function state() {}
    
    /**
     * 更新角色（批量操作）
     */
    public final function stateAll() {
        $roleid = I('get.roleid', 0, 'intval');
        
        $where = ['roleid' => $roleid];
        $data2 = $this->roleModel->where($where)->find();
        $state = $data2['disabled'] ? 0 : 1;
        
        $child = $this->roleModel->childid($roleid);
        $array = explode(',', $child);
        
        if (in_array($roleid, C('SUPER_ROLEID'))) {
            $tips = '设置用户'.'『'.$data2['name'].'』角色无效！';
            cms_writelog($tips, ['roleid' => $roleid]);
            
            echo json_encode(['roleid' => [], 'state' => 2]);
            return false;
        }
        
        foreach ($array as $key => $roleid) {
            $this->roleModel->where(['roleid' => $roleid])
                 ->save(['disabled' => $state]);
        }
        $data2 = $this->roleModel->where($where)->find();
        $oper2 = $data2['disabled'] ? '启用' : '禁用';
        
        $tips2 = $oper2.'用户'.'『'.$data2['name'].'』角色';
        cms_writelog($tips2.'成功！', ['roleid' => $child]);
        
        $array = ['roleid' => $array, 'state' => $state];
        echo json_encode($array);
    }
    
    /**
     * 移动角色
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['roleid']    = $data['parentid'];
        $name = $this->roleModel->where($where)->find()['name'];
        $name = $name ? : '顶级角色';
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $temp = $this->roleModel->where($where)->find();
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress = $this->roleModel->where($where)->save($data);
        
        $data               = [];
        $data['parentid']   = $temp['parentid'].'='.$ress['parentid'];
        $data['roleid']     = $info['roleid'];
        
        $tips = '移动当前『'.$temp['name'].'』到『'.$name.'』角色';
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, null, 3);
        } // @todo: 
        
        $roleid = I('get.roleid', 0, 'intval');
        $info               = [];
        $info['roleid']     = $roleid;
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        
        $data = [];
        $data = $this->roleModel->where($where)->find();
        
        $data = cms_stripslashes($data);
        $info['parentid']   = 0;
        $role = $this->roleModel->childid($info['parentid']);
        
        $where              = [];
        $where['parentid']  = ['IN',  $role];
        $where['roleid']    = ['NEQ', $info['roleid']];
        $where['disabled']  =  0;
        
        $ress = $this->roleModel->treeList($where);
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [$data['parentid']], ['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
}
