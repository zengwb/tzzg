<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块配置分组控制器类：呈现配置CURD常规管理
 * 
 * @author T-01
 */
final class ConfigGroupController extends BaseController {
    
    public      $action     = [
        'index','listorder','add','edit','delete','check','state',
    ];
    
    private     $appsModel  = [],
                $confModel  = [],
                $typeModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->appsModel    = D('Module');
        $this->confModel    = D('Config');
        $this->typeModel    = D('ConfigGroup');
    }
    
    /**
     * 分组管理
     */
    public final function index() {
        $data = [];
        $ress = $this->typeModel->where()->order(
            '`listorder` ASC, `groupid` ASC'
        )->select();
        
        foreach ($ress as $key => $row) {
        $where = ['module' => $row['module']];
        $ress2 = $this->appsModel->where($where)->find();
        $row['module'] = $ress2['name'];
        $data[$key] = $row;
        }
        
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['groupid'] as $key => $groupid) {
        $listorder = $this->typeModel->where([
            'groupid' => $groupid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->typeModel->where([
            'groupid'   => $groupid
        ])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $groupid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('配置分组排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加分组
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where = [];
        $where['module'] = $data['module'];
        $where['name']   = $data['name'];
        
        $ress = $this->typeModel->where($where)->find();
        if ($ress) $this->popup(false, '配置分组已存在，添加操作');
        
        $where = [];
        $where['module'] = $data['module'];
        
        $apps = $this->appsModel->where($where)->find();
        $module = $apps['name'];
        
        $tips = '添加系统『'.$module.'』模块配置『'.$data['name'].'』分组';
        $inid = $this->typeModel->add($data);
        
        if (!$inid) $this->popup(false, $tips, $data);
        $this->typeModel->where(['groupid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->popup(true, $tips, $data, U('index'));
        } // @todo: 
        
        $where = ['disabled' => 0];
        $apps2 = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('module', $apps2);
        $this->display('action');
    }
    
    /**
     * 修改分组
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where = [];
        $where['module'] = $data['module'];
        $where['name'] = $data['name'];
        
        $ress = $this->typeModel->where($where)->find();
        if ($ress && $info['name'] != $data['name']) {
            $this->popup(false, '配置分组已存在，修正操作');
        }
        $where = ['module' => $data['module']];
        $ress = $this->appsModel->where($where)->find();
        $module = $ress['name'];
        
        $where = [];
        $where['module'] = $data['module'];
        $where['groupid']= $info['groupid'];
        $ress = $this->typeModel->where($where)->save($data);
        
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $tips = '更新系统『'.$module.'』模块配置『'.$name.'』分组';
        $info = ['groupid' => $info['groupid']];
        
        if (!$ress) $this->popup(false, $tips, $info);
        else $this->popup(true, $tips, $info, U('index'));
        } // @todo: 
        
        $groupid = I('get.groupid', 0);
        
        $where = ['disabled' => 0];
        $apps = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $where = ['groupid' => $groupid];
        $data = $this->typeModel->where($where)->find();
        
        $this->assign('module', $apps);
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 删除分组
     */
    public final function delete() {
        $groupid = I('get.groupid', 0, 'intval');
        $info = I('post.info', []);
        $info = $groupid ? [$groupid] : $info['groupid'];
        $data = [];
        if (empty($info)) $this->error('请选择配置分组！');
        
        foreach ($info as $key => $groupid) {
            $data['groupid'][] =  $groupid;
        }
        $groupid = implode(',', $data['groupid']);
        
        $where = ['groupid' => ['IN', $groupid]];
        $ress = $this->typeModel->where($where)->delete();
        $temp = 'groupid: '.$groupid;
        
        if (!$ress) $this->error('删除配置分组失败！', $temp);
        else $this->success('删除配置分组成功！', $temp);
    }
    
    /**
     * 删除分组
     */
    public final function check() {
        $module = I('get.module', '');
        $group = I('get.group', '');
        
        $where = [];
        $where = ['module' => $module, 'group' => $group];
        
        $check = $this->typeModel->where($where)->count();
        echo json_encode($check);
    }
    
    /**
     * 状态设置
     */
    public final function state() {
        $groupid = I('get.groupid', 0, 'intval');
        $info = [];
        $info['groupid'] = $groupid;
        
        $where = ['groupid' => $info['groupid']];
        $ress2 = $this->typeModel->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where = ['groupid' => $info['groupid']];
        $ress2 = $this->typeModel->where($where)->save([
            'disabled'=>$value
        ]);
        $where = ['groupid' => $info['groupid']];
        $data2 = $this->typeModel->where($where)->find();
        $oper2 = $data2['disabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'配置『'.$data2['name'].'』'.$state;
        cms_writelog($tips2, ['groupid' => $info['groupid']]);
        
        echo json_encode($data2['disabled']);
    }
    
}
