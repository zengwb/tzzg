<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块联动子集控制器类，呈现子集CURD常规管理
 * 
 * @author T-01
 */
final class SubsetController extends BaseController {
    
    private     $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state', 'move'
    ];
    
    private     $siteModel  =   [],
                $menuModel  =   [],
                $tree       = null,
                $treeField  = ['linkageid', 'name', 'parentid'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->siteModel    = D('Admin/Website');
        $this->menuModel    = D('Admin/Linkage');
    }
    
    /**
     * 子集管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $linkageid = I('get.key', 0, 'intval');
        
        $where = [];
        $where['subsetid']  = $linkageid;
        $order = 'listorder ASC, linkageid ASC';
        if (!empty($keyword)) {
            $where['name'] = ['LIKE', '%'.$keyword.'%'];
        } else {
            $where['parentid'] = 0;
        }
        $where['siteids'] = ['IN', [0, cms_siteid()]];
        
        $nums = $this->menuModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->menuModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
            if ($keyword) {
                $row['title'] = strtr($row['name'], $keys);
            } else {
                $row['title'] = $row['name'];
            }
            $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
}