<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块后台菜单控制器类：呈现菜单CURD常规管理
 * 
 * @author T-01
 */
final class MenuController extends BaseController {
    
    public      $action     = [
        'index', 'lists', 'listorder', 'method', 'add', 'edit',
        'delete','state', 'stateAll', 'move',
    ];
    
    private     $attribute  = [
        0 => '用户', 1 => '管理' // @todo: 这里可以扩展菜单类型
    ];
    private     $menuModel  =   [],
                $tree       = null,
                $field      = ['menuid', 'name', 'parentid'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->menuModel = D('Admin/Menu');
    }
    
    /**
     * 菜单管理
     */
    public final function index() {
        $isattr = I('get.isattr', 1, 'intval');
        
        $keyword = I('post.key', '', 'cms_addslashes');
        $keyword = trim($keyword);
        $where   = [];
        $order   = '`listorder` ASC, `menuid` ASC';
        
        if (IS_POST && !empty($keyword)) { // @todo:
            $where['name'] = ['LIKE', '%'.$keyword.'%'];
        } else {
            $where['parentid'] = 0;
        }
        if (!$isattr) $where['isattr'] = $isattr;
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->menuModel->where($where)->order($order)
                     ->select();
        foreach ($ress as $key  => $row) {
            if ($keyword) $row['title'] = strtr($row['name'], $keys);
            else $row['title']  = $row['name'];
            
            $data[$key]         =  $row;
            
            if ($row['linkurl'] && empty($row['action'])) {
            $data[$key]['_add'] = false;
            $data[$key]['_url'] = $row['linkurl'];
            $data[$key]['_link']= 'void(0);';
            } else {
            $link = U('lists', ['parentid' => $row['menuid']]);
            $data[$key]['_add'] = true;
            $data[$key]['_url'] = $this->menuModel->linkurl($row);
            $data[$key]['_link']= 'yhcms.common.linkurl(\''.$link.'\');';
            }
            $childid = $this->menuModel->childid($row['menuid']);
            $childid = strtr($childid, [$row['menuid'] => null]);
            
            if ($childid) $data[$key]['_child'] = true;
            else $data[$key]['_child'] = false;
            
            $data[$key]['_attr']= $this->attribute[$row['isattr']];
        }
        $this->assign('data', $data);
        $this->assign('isattr', $isattr);
        $this->display();
    }
    
    /**
     * 子项管理
     */
    public final function lists() {
        $parentid = I('get.parentid', 0, 'intval');
        
        $data = $temp = [];
        $info = ['parentid' => $parentid];
        
        $where = [];
        if (!empty($info['parentid'])) {
            $menuid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $menuid];
        }
        $ress = $this->menuModel->treeList($where, '*');
        foreach ($ress as $key  => $row) {
            $attr = $this->attribute[$row['isattr']].'菜单';
            $row['_attr'] = $attr;
            
            $link = $this->menuModel->linkurl($row);
            $link = $row['linkurl'] ? : $link;
            $link = cms_stripslashes($link);
            $row['_link'] = $link;
            
            $css2 = $row['display'] ? '' : 'display';
            $row['_css2'] = $css2;
            
            if ($row['display']) {
            $row['_state'] = '<a class="list-operation" 
            data-state="'.$row['menuid'].'" 
            href="javascript:void(0);" title="点击禁用菜单">
            <i class="iconfont icon-qiyong"></i></a>';
            } else {
            $row['_state'] = '<a class="list-operation"
            data-state="'.$row['menuid'].'"
            href="javascript:void(0);" title="点击启用菜单">
            <i class="iconfont icon-qingchu"></i></a>';
            }
            $childid = $this->menuModel->childid($row['menuid']);
            $childid = strtr($childid, [$row['menuid'] => null]);
            
            $_u2 = U('lists', ['parentid' => $row['menuid']]);
            $_r1 = $row['linkurl'] ?
            'void(0);' : 'yhcms.common.linkurl(\''.$_u2.'\');';
            $_r2 = U('edit', ['menuid' => $row['menuid']]);
            
            $row['_name'] = $childid ?
            '<a href="javascript:void(0);"
            onClick="javascript:'.$_r1.'" title="管理子项">'.
            $row['name'].'</a>' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(\''.$_r2.'\',
            \'修改【'.$row['name'].'】菜单\',
            \'AdminMenuEdit-0-540-415\');">'.$row['name'].'</a>';
            
            $row['_oper']  = $childid ?
            '<a href="javascript:void(0);"
            onClick="javascript:'.$_r1.'" title="管理菜单">管理</a> ' :
            '<a href="javascript:void(0);"
            class="cms-cccc">管理</a> ';
            
            $url = U('method', ['menuid' => $row['menuid']]);
            $row['_oper'] .= $row['controller'] &&
            $row['action'] == 'index' && empty($row['linkurl']) &&
            empty($row['params']) ?
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(
            \''.$url.'\', \'指定【'.$row['name'].'】方法\',
            \'AdminMenuMethod-0-640-360\');"
            title="配置方法">方法</a> ' :
            '<a href="javascript:void(0);" class="cms-cccc">方法</a> ';
            
            $url = U('add', ['parentid' => $row['menuid']]);
            $row['_oper'] .= empty($row['linkurl']) &&
            empty($row['action']) ?
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(
            \''.$url.'\', \'添加【'.$row['name'].'】子项\',
            \'AdminMenuAdd-0-540-415\');"
            title="添加子项">添加</a> ' :
            '<a href="javascript:void(0);" class="cms-cccc">添加</a> ';
            
            $url = U('edit', ['menuid' => $row['menuid']]);
            $row['_oper'] .= '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(\''.$url.'\',
            \'修改【'.$row['name'].'】菜单\',
            \'AdminMenuEdit-0-540-415\');" title="修改菜单">修改</a> ';
            
            $url = U('move', ['menuid' => $row['menuid']]);
            $row['_oper'] .= '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(\''.$url.'\',
            \'移动【'.$row['name'].'】菜单\',
            \'AdminMenuMove-0-540-132\');" title="移动菜单">移动</a> ';
            
            $url = U('delete', ['menuid' => $row['menuid']]);
            $row['_oper'] .= '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.tips(\''.$url.'\',
            \'确认删除【'.$row['name'].'】'.$row['_attr'].'！\');"
            title="删除菜单">删除</a> ';
            
            $temp[$row['menuid']] = $row; // 获取菜单记录
        }
        $text = "<tr class='\$_css2'>
            <th class='list-checkbox'>
            <input class='checkchild' name='info[menuid][]'
            value='\$menuid' type='checkbox' />
            </th>
            <td class='list-small'>\$menuid</td>
            <td class='list-listorder'>
            <input type='hidden' name='data[menuid][]'
            value='\$menuid' />
            <input type='text' name='data[listorder][]'
            value='\$listorder' maxlength='4' autocomplete='off'
            class='form-control input-sm list-input-listorder' />
            </td>
            <td data-menuid='\$menuid'>
            \$spacer\$_name
            <span class='cms-cccc'>　\$description</span>
            </td>
            <td class='cms-tc'>\$_attr</td>
            <td class='cms-c999'>\$_link</td>
            <td class='cms-tc icon-color'>\$_state</td>
            <td class='cms-tc'>\$_oper</td>
        </tr>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],$text,'',[],['field'=>$this->field]
        );
        $list = $this->menuModel->catalog($info['parentid'])['name'];
        
        $this->assign('info', $info);
        $this->assign('data', $temp);
        $this->assign('tree', $tree);
        $this->assign('list', $list);
        
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['menuid'] as $i => $menuid) {
        $where = ['menuid' => $menuid];
        $ress2 = $this->menuModel->where($where)->find();
        
        $listorder = $ress2['listorder'];
        if ($listorder == $data['listorder'][$i]) {
            continue;
        }
        $ress2 = $this->menuModel->where($where)->save([
            'listorder'=> $data['listorder'][$i]
        ]);
        $listorder = $menuid.'='.$data['listorder'][$i];
        $list['listorder'][] = $listorder;
        }
        $listorder = $list ? : 'null';
        self::success('菜单排序成功！', $listorder, '', 3);
    }
    
    /**
     * 可用方法
     */
    public final function method() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $method = $data['method'] ?: [];
        $method = implode(',', $method);
        
        $where              = [];
        $where['menuid']    = $info['menuid'];
        
        $data = ['method'   => $method];
        $ress = $this->menuModel->where($where)->save($data);
        
        $data['menuid']     = $info['menuid'];
        $tips = '配置菜单『'.$info['name'].'』方法';
        
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, '', 3);
        } // @todo: 
        
        $menuid = I('get.menuid', 0, 'intval');
        $info               = [];
        $info['menuid']     = $menuid;
        
        $where              = [];
        $where['menuid']    = $info['menuid'];
        $menu = $this->menuModel->where($where)->find();
        
        $space  = '';
        $space .= $menu['module'] . '\\Controller' .'\\';
        $space .= $menu['controller'].'Controller';
        $class  = class_exists($space) ? new $space : [];
        
        $data = [];
        foreach ($class->action as $key => $action) {
        if (!method_exists($class, $action)) continue ;
        
        $data[$key]['namespace'] = $space;
        $data[$key]['name'] = $action;
        
        $method = cms_method_notes($space, $action);
        $data[$key]['note'] = $method['note'];
        }
        unset($class);
        $info['name']       = $menu['name'];
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('method', explode(',',$menu['method']));
        
        $this->display();
    }
    
    /**
     * 添加菜单
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        $type = $this->attribute[$data['isattr']];
        $tips = $type.'菜单『'.$data['name'].'』同名，当前操作';
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($ress) $this->popup(false, $tips, $data);
        
        if ( empty($data['description']) &&
            !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        if ( empty($data['module']) && empty($data['linkurl'])) {
            $data['module'] = MODULE_NAME;
        }
        $inid = $this->menuModel->add($data);
        
        $tips = '添加'.$type.'『'.$data['name'].'』菜单';
        if (!$inid) $this->popup(false, $tips, $data);
        
        $this->menuModel->where(['menuid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->popup(true, $tips, $data, null,  3);
        } // @todo: 
        
        $parentid = I('get.parentid', 0, 'intval');
        
        $info               = [];
        $info['parentid']   = $parentid;
        $info['menuid']     = $info['parentid'];
        
        $data               = [];
        $data['parentid']   = $info['parentid'];
        $data['type']       =  0;
        $data['isattr']     =  1;
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['linkurl']   = '';
        $where['action']    = '';
        $where['display']   =  1;
        
        if (!empty($info['parentid'])) {
            $menuid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $menuid];
        }
        $temp = $this->menuModel->treeList($where);
        
        $text = "<option value='\$menuid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [], ['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->assign('attr', $this->attribute);
        
        $this->display('action');
    }
    
    /**
     * 编辑菜单
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        $type = $this->attribute[$data['isattr']];
        $tips = $type.'菜单『'.$data['name'].'』同名，当前操作';
        
        if ( empty($data['description']) &&
            !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($info['name']  != $data['name'] &&
            $ress) $this->popup(false, $tips, $data);
        
        if ($info['name']  != $data['name']) {
            $name = $info['name'].'→'.$data['name'];
        } else {
            $name = $data['name'];
        }
        $where              = [];
        $where['menuid']    = $info['menuid'];
        $ress = $this->menuModel->where($where)->save($data);
        
        $tips = '更新'.$type.'『'.$name.'』菜单';
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, '', 3);
        } // @todo: 
        
        $menuid = I('get.menuid', 0, 'intval');
        $info               = [];
        $info['menuid']     = $menuid;
        
        $where              = [];
        $where['menuid']    = $info['menuid'];
        
        $menu = $this->menuModel->catalog($info['menuid'])['menuid'];
        $data = [];
        $data = $this->menuModel->where($where)->find();
        $data = cms_stripslashes($data);
        $data['type'] = $data['linkurl'] ? 1 : 0;
        
        $temp               = [];
        $temp['menuid']     = $info['menuid'];
        
        $info['parentid']   = $data['parentid'] ? $menu[0] : 0;
        $info['menuid']     = $info['parentid'];
        
        $info['name']       = $data['name'];
        $info['isattr']     = $data['isattr'];
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        
        $where['menuid']    = ['NEQ', $info['menuid']];
        $where['_logic']    =  'AND';
        $where['menuid']    = ['NEQ', $temp['menuid']];
        
        $where['linkurl']   = '';
        $where['action']    = '';
        $where['display']   =  1;
        
        if (!empty($info['parentid'])) {
            $menuid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $menuid];
        }
        $ress = $this->menuModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['menuid']] = $row;
        }
        $text = "<option value='\$menuid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text,'',[$data['parentid']],['field' => $this->field]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->assign('attr', $this->attribute);
        
        $this->display('action');
    }
    
    /**
     * 删除菜单
     */
    public final function delete() {
        $menuid = I('get.menuid', 0, 'intval');
        
        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';
        
        $info = $menuid ? [$menuid] : $info['menuid'];
        if (empty($info)) $this->error('请选择菜单！');
        
        foreach ($info as $key => $menuid) {
            $temp .= $this->menuModel->childid($menuid).',';
        }
        $data['menuid'] = explode(',', $temp);
        
        $temp = ['menuid' => ['IN', $temp]];
        $ress = $this->menuModel->where($temp)->delete();
        
        if (!$ress) $this->error('删除菜单失败！', $data);
        else $this->success('删除菜单成功！', $data);
    }
    
    /**
     * 更新状态（单一操作）
     */
    public final function state() {
        $menuid = I('get.menuid', 0, 'intval');
        
        $where = ['menuid' => $menuid];
        $data2 = $this->menuModel->where($where)->find();
        
        $state = $data2['display'];
        $value = $state ? 0 : 1;
        
        $ress2 = $this->menuModel->where($where)->save([
            'display' => $value
        ]);
        $data2 = $this->menuModel->where($where)->find();
        
        $oper2 = $data2['display'] ? '启用' : '禁用';
        $oper2.= $this->attribute[$data2['isattr']];
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        
        $tips2 = $oper2.'『'.$data2['name'].'』菜单';
        cms_writelog($tips2.$state, ['menuid'=>$menuid]);
        
        echo json_encode($data2['display']);
    }
    
    /**
     * 更新状态（批量操作）
     */
    public final function stateAll() {
        $menuid = I('get.menuid', 0, 'intval');
        
        // @todo: 向下操作有效，向上操作待开发
//         $parent = $this->menuModel->parentid($menuid);
//         print_r($parent);
        
        $where = ['menuid' => $menuid];
        $data2 = $this->menuModel->where($where)->find();
        $state = $data2['display'] ? 0 : 1;
        
        $child = $this->menuModel->childid($menuid);
        $array = explode(',', $child);
        
        foreach ($array as $key => $menuid) {
            $this->menuModel->where(['menuid' => $menuid])
                 ->save(['display' => $state]);
        }
        $data2 = $this->menuModel->where($where)->find();
        
        $oper2 = $data2['display'] ? '启用' : '禁用';
        $oper2.= $this->attribute[$data2['isattr']];
        
        $tips2 = $oper2.'『'.$data2['name'].'』菜单';
        cms_writelog($tips2.'成功！', ['menuid' => $child]);
        
        $array = ['menuid' => $array, 'state' => $state];
        echo json_encode($array);
    }
    
    /**
     * 移动菜单
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['menuid']    = $data['parentid'];
        $name = $this->menuModel->where($where)->find()['name'];
        $name = $name ? : '顶级菜单';
        
        $where              = [];
        $where['menuid']    = $info['menuid'];
        $temp = $this->menuModel->where($where)->find();
        
        $type = $this->attribute[$temp['isattr']];
        $tips = '移动'.$type.'『'.$temp['name'].'』到『'.$name.'』菜单';
        
        $where              = [];
        $where['menuid']    = $info['menuid'];
        $ress = $this->menuModel->where($where)->save($data);
        
        $data               = [];
        $data['parentid']   = $temp['parentid'].'='.$ress['parentid'];
        $data['menuid']     = $info['menuid'];
        
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, null, 3);
        } // @todo: 
        
        $menuid = I('get.menuid', 0, 'intval');
        $info               = [];
        $info['menuid']     = $menuid;
        
        $where              = [];
        $where['menuid']    = $info['menuid'];
        
        $menu = $this->menuModel->catalog($info['menuid'])['menuid'];
        $data = [];
        $data = $this->menuModel->where($where)->find();
        
        $data = cms_stripslashes($data);
        $info['parentid']   = $data['parentid'] ? $menu[0] : 0;
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['menuid']    = ['NEQ', $info['menuid']];
        $where['linkurl']   = '';
        $where['action']    = '';
        $where['display']   =  1;
        
        if (!empty($info['parentid'])) {
            $menuid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $menuid];
        }
        $ress = $this->menuModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['menuid']] = $row;
        }
        $text = "<option value='\$menuid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [$data['parentid']], ['field' => $this->field]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
}
