<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块附件控制器类：呈现附件CURD常规管理
 * 
 * @author T-01
 */
final class AttachmentController extends BaseController {
    
    public      $action     = [
        'index','upload','shows','download','delete',
    ];
    
    private     $attcModel  = [],
                $cateModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->attcModel    = D('Common/Attachment');
        $this->cateModel    = D('Admin/Category');
    }
    
    /**
     * 附件管理
     */
    public final function index() {
        $modules    = I('request.module', null);
        $modules    = I('get.module', $modules);
        $modules    = I('post.module',$modules);
        
        $keyword    = I('request.key', null, 'cms_addslashes');
        $keyword    = I('get.key', $keyword, 'cms_addslashes');
        $keyword    = I('post.key',$keyword, 'cms_addslashes');
        $keyword    = trim($keyword);
        
        $where      = [];
        $order      = '`uploadtime` DESC';
        if (!empty($keyword)) {
            $where['filename'] = ['LIKE', '%'.$keyword.'%'];
        } else {
        }
        if (!empty($modules)) $where['module'] = $modules;
        $where['siteid'] = ['IN', [0, cms_siteid()]];
        
        $nums = $this->attcModel->submeter()->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $ress = $this->attcModel->submeter()->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $data = [];
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['name'] = $row['filename'];
        else $row['name'] = strtr($row['filename'], $keys);
        
        $where              = [];
        $where['catid']     = $row['catid'];
        $cates = $this->cateModel->where($where)->find();
        $row['catename']    = $cates['catname'];
        
        $row['filesize'] = cms_byte_unit(filesize($row['filepath']));
        $row['uploadtime'] = date('Y-m-d H:i:s', $row['uploadtime']);
        
        $row['temp'] = !file_exists($row['filepath']) ? 1 : 0;
        $data[$key] = $row;
        }
        
        $this->assign('module', $modules);
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 上传附件
     */
    public final function upload() {}
    
    /**
     * 查看附件
     */
    public final function shows() {
        $attachedid = I('get.attachedid', 0, 'intval');
        
        $where = ['attachedid' => $attachedid];
        $files = $this->attcModel->submeter()->where($where)->find();
        
        $this->assign('filepath', $files['filepath']);
        $this->assign('filename', $files['filename']);
        $this->assign('isimage',  $files['isimage'] );
        $this->display();
    }
    
    /**
     * 下载附件
     */
    public final function download() {
        $attachedid = I('get.attachedid', 0, 'intval');
        
        $where = ['attachedid' => $attachedid];
        $files = $this->attcModel->submeter()->where($where)->find();
        
        $filename = $files['filename'];
        $filepath = $files['filepath'];
        
        $fp = fopen($filepath, 'r');
        
        header('Content-type: application/octet-stream');
        header('Accept-Ranges: bytes');
        header('Accept-Length: '.filesize($filepath));
        header('Content-Disposition: attachment; filename='.$filename);
        
        echo fread($fp, filesize($filepath));
        fclose($fp); exit();
    }
    
    /**
     * 删除附件
     */
    public final function delete() {
        $attachedid = I('get.attachedid', 0, 'intval');
        
        $info = I('post.info', []);
        $info = $attachedid ? [$attachedid] : $info['attachedid'];
        $data = [];
        if (empty($info)) $this->error('请选择附件！');
        
        foreach ($info as $key => $attachedid) {
            $data['attachedid'][] = $attachedid;
            
            $where = ['attachedid' => $attachedid];
            $files = $this->attcModel->submeter($where)->where($where)
                          ->find();
            $ress2 = $this->attcModel->submeter($where)->where($where)
                          ->delete();
            
            @unlink($files['filepath']);
        }
        $info = ['attachedid: '.implode(',', $data['attachedid'])];
        $this->success('删除内容附件成功！', $info);
    }
    
}
