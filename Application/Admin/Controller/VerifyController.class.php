<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块验证信息控制器类：呈现验证CURD常规操作
 * 
 * @author T-01
 */
final class VerifyController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state'
    ];
    
    private     $dataModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->dataModel    = D('Verifyid');
    }
    
    /**
     * 验证管理
     */
    public final function index() {
        $keys = I('request.key', '', 'cms_addslashes');
        $keys = I('get.key' , '', 'cms_addslashes') ? : $keys;
        $keys = I('post.key', '', 'cms_addslashes') ? : $keys;
        $keys = trim($keys);
        
        $where              = [];
        $where['verify']    = ['LIKE', '%'.$keys.'%'];
        
        $nums = $this->dataModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] =  $keys;
        
        $ress = $this->dataModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order(
            '`listorder` DESC, `verifyid` DESC'
        )->select();
        
        $keyr = [
            $keys => '<span class="cms-cf30">'.$keys.'</span>'
        ];
        foreach ($ress as $key => $row) {
            if (!$keys) $row['verify2'] = $row['verify'];
            else $row['verify2'] =  strtr($row['verify'], $keyr);
            $data[$key] = $row;
        }
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['verifyid'] as $i => $verifyid) {
        $where              = [];
        $where['verifyid']  = $verifyid;
        $ress = $this->dataModel->where($where)->find();
        
        $listorder = $ress['listorder'];
        if ($listorder == $data['listorder'][$i]) {
            continue;
        }
        $ress = $this->dataModel->where($where)->save([
            'listorder'=> $data['listorder'][$i]
        ]);
        $listorder = $verifyid.'='.$data['listorder'][$i];
        $list['listorder'][]= $listorder;
        }
        $listorder = $list ? : 'null';
        self::success('验证排序成功！', $listorder);
    }
    
    /**
     * 添加验证
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['verify']    = $data['verify'];
        $ress = $this->dataModel->where($where)->find();
        
        $tips = '验证问题『'.$data['verify'].'』已经存在！';
        if ($ress) $this->error($tips, $data);
        
        $tips = '添加验证『'.$data['verify'].'』问题';
        $inid = $this->dataModel->add($data);
        
        $where              = [];
        $where['verifyid']  = $inid;
        $ress = $this->dataModel->where($where)->save([
            'listorder'     => $inid
        ]);
        
        if (!$inid) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $this->assign('data', ['display' => 1]);
        $this->display('action');
    }
    
    /**
     * 编辑验证
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['verify']    = $data['verify'];
        $ress = $this->dataModel->where($where)->find();
        
        $tips = '验证问题『'.$data['verify'].'』已经存在！';
        if ($info['verify'] != $data['verify'] && $ress) {
            $this->error($tips, $data);
        }
        $where              = [];
        $where['verifyid']  = $info['verifyid'];
        $ress = $this->dataModel->where($where)->save($data);
        
        if ($info['verify'] == $data['verify']) {
            $name = $info['verify'];
        } else {
            $name = $info['verify'].'→'.$data['verify'];
        }
        $tips = '更新验证『'.$name.'』问题';
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $verifyid = I('get.verifyid', 0, 'intval');
        
        $where              = [];
        $where['verifyid']  = $verifyid;
        $data = $this->dataModel->where($where)->find();
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 删除验证
     */
    public final function delete() {
        $verifyid = I('get.verifyid', 0, 'intval');
        
        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = [];
        
        $info = $verifyid ? [$verifyid] : $info['verifyid'];
        if (empty($info)) $this->error('请选择问题！');
        
        foreach ($info as $key => $verifyid) {
            $data['verifyid'][]=  $verifyid;
        }
        $temp['verifyid'] = implode(',', $data['verifyid']);
        
        $where              = [];
        $where['verifyid']  = ['IN', $data['verifyid']];
        $ress = $this->dataModel->where($where)->delete();
        
        if (!$ress) $this->error('删除验证问题失败！', $temp);
        else $this->success('删除验证问题成功！', $temp, U('index'));
    }
    
    /**
     * 更新状态
     */
    public final function state() {
        $verifyid = I('get.verifyid', 0, 'intval');
        
        $info               = [];
        $info['verifyid']   = $verifyid;
        
        $where              = [];
        $where['verifyid']  = $info['verifyid'];
        $ress = $this->dataModel->where($where)->find();
        
        $state = $ress['display'];
        $value = $state ? 0 : 1;
        
        $where              = [];
        $where['verifyid']  = $info['verifyid'];
        $ress = $this->dataModel->where($where)->save([
            'display' => $value
        ]);
        $data = $this->dataModel->where($where)->find();
        $oper = $data['display'] ? '启用' : '隐藏';
        
        if (!$ress) $state = '失败！';
        else $state = '成功！';
        
        $tips = $oper.'验证『'.$data['verify'].'』问题';
        cms_writelog($tips.$state, $info);
        
        echo json_encode($data['display']);
    }
    
}
