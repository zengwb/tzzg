<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块数据备份控制器类：呈现数据备份、数据恢复等操作
 * 
 * @author T-01
 */
final class BackupController extends BaseController {
    
    public      $action     = [
        'index', 'backup', 'dbbackup', 'recovery', 'dbrecovery',
        'delete', 'download',
    ];
    
    private     $dataModel  = [],
                $backPath   = '';
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->dataModel    = D('Database');
        $this->backPath     = C('CMS_BACKUP_PATH').'Database/';
    }
    
    /**
     * 备份管理
     */
    public final function index() { $this->redirect('backup'); }
    
    /**
     * 备份管理
     */
    public final function backup() {
        $array = [];
        $ress2 = $this->dataModel->tables();
        
        foreach ($ress2 as $key => $val) {
        $database = $val['tables_in_'. C('DB_NAME')];
        $table = $this->dataModel->status($database);
        
        $array[$key] = $table[0];
        }
        $this->assign('data', $array);
        $this->assign('list', $array);
        
        $this->display('index');
    }
    
    /**
     * 数据备份
     */
    public final function dbbackup() {
        $table = I('get.table',  '', 'cms_addslashes');
        $info  = I('post.info', []);
        $info  = $table ? [$table] : $info['table'];
        $data  = [];
        if (empty($info)) $this->error('请选择数据库表！');
        
        $backpath = $this->backPath.date('Ymd', time()).'_';
        $nl2br = "\r\n";
        $backs = '';
        $backs.= $nl2br;
        $backs.= '/* MjCMS2 MySQL Data Transfer（';
        $backs.= 'Date: '.date('Y-m-d H:i:s').'）*/'.$nl2br;
        $backs.= $nl2br.'SET CHARSET UTF8;'.$nl2br;
        
        foreach ($info as $key => $table) {
        $data[] = $table;
        $structure = $this->dataModel->structure($table);
        
        $backs .= $nl2br;
        $backs .= '-- ----------------------------';
        $backs .= $nl2br;
        $backs .= '-- Table structure for `'.$table.'`';
        $backs .= $nl2br;
        $backs .= '-- ----------------------------';
        $backs .= $nl2br;
        $backs .= 'DROP TABLE IF EXISTS `' . $table.'`;';
        $backs .= $nl2br;
        $backs .= $structure[0]['create table'] . ';';
        $backs .= $nl2br.$nl2br;
        $backs .= '-- ----------------------------';
        $backs .= $nl2br;
        $backs .= '-- Records of `'.$table.'`';
        $backs .= $nl2br;
        $backs .= '-- ----------------------------';
        $backs .= $nl2br;
        $backs .= $this->dataModel->insertsql($table);
        $backs .= $nl2br;
        }
        $filename = $backpath.date('His').cms_random().'.SQL';
        
        $fp = fopen($filename, 'w');
        if (!$fp) $this->error('数据备份失败！');
        
        fwrite($fp, $backs);
        fclose($fp);
        $this->success('数据备份成功！', implode(',', $data));
    }
    
    /**
     * 数据恢复
     */
    public final function recovery() {
        $array  = [];
        $index  =  0;
        $backRead = dir($this->backPath);
        
        while ($fileName = $backRead->read()) {
        $filePath = $this->backPath.$fileName;
        if (!is_file($filePath)) continue;
        
        $index++;
        $array[$index]['filename'] = $fileName;
        $array[$index]['filepath'] = $filePath;
        
        $array[$index]['filesize'] = filesize( $filePath);
        $array[$index]['filetime'] = filectime($filePath);
        }
        rsort($array);
        
        $this->assign('data', $array);
        $this->assign('list', $array);
        
        $this->display();
    }
    
    /**
     * 数据恢复
     */
    public final function dbrecovery() {
        $filename = I('get.filename', '');
        $filepath = $this->backPath . $filename;
        
        $tips = '数据备份『'.$filename.'』文件丢失！';
        if (!file_exists($filepath)) $this->error($tips);
        
        $sqlv = file_get_contents($filepath);
//         $ress = $this->dataModel->execute($sqlv);
        
//         if (!$ress) $this->error('数据恢复失败！', $filename);
//         else $this->success('数据恢复成功！', $filename, '', 3);
        
        // @todo: 恢复异常，待处理！
        $this->success('数据恢复成功！', $filename);
    }
    
    /**
     * 删除备份
     */
    public final function delete() {
        $filename = I('get.filename', '');
        $info = I('post.info', []);
        $info = $filename ? [$filename] : $info['filename'];
        $data = [];
        if (empty($info)) $this->error('请选择备份文件！');
        
        foreach ($info as $key => $filename) {
        $filepath = $this->backPath.$filename;
        if (!file_exists($filepath)) continue;
        
       @unlink($filepath);
        $data['filename'][$key] = $filename;
        }
        $this->success('删除备份文件成功！', implode(',', $data['filename']));
    }
    
    /**
     * 下载备份（SQL）
     */
    public final function download() {
        $filename = I('get.filename', '');
        $filepath = $this->backPath . $filename;
        
        $tips = '数据备份『'.$filename.'』文件丢失！';
        if (!file_exists($filepath)) $this->error($tips);
        $fp = fopen($filepath, 'r');
        
        header('Content-type: application/octet-stream');
        header('Accept-Ranges: bytes');
        header('Accept-Length: '.filesize($filepath));
        header('Content-Disposition: attachment; filename='.$filename);
        
        echo fread($fp, filesize($filepath));
        fclose($fp); exit();
    }
    
}
