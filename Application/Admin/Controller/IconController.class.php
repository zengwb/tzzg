<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块ICON图标控制器类：呈现图标CURD常规管理
 * 
 * @author T-01
 */
final class IconController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state'
    ];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    public final function index() {}
    
    public final function listorder() {}
    
    public final function add() {}
    
    public final function edit() {}
    
    public final function delete() {}
    
    public final function state() {}
    
}
