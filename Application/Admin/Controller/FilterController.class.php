<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块敏感词汇控制器类：呈现词汇CURD常规操作
 * 
 * @author T-01
 */
final class FilterController extends BaseController {
    
    public      $action     = [
        'index', 'add', 'import', 'edit', 'export', 'delete'
    ];
    private     $keysModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->keysModel    = D('Keyword');
    }
    
    /**
     * 词汇管理
     */
    public final function index() {
        $keys = I('request.key', '', 'cms_addslashes');
        $keys = I('get.key' , '', 'cms_addslashes') ? : $keys;
        $keys = I('post.key', '', 'cms_addslashes') ? : $keys;
        $keys = trim($keys);
        
        $where              = [];
        $where['keyword']   = ['LIKE', '%'.$keys.'%'];
        
        $nums = $this->keysModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] =  $keys;
        
        $ress = $this->keysModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order(
            '`inputtime` DESC, `keysid` DESC'
        )->select();
        
        $keyr = [
            $keys => '<span class="cms-cf30">'.$keys.'</span>'
        ];
        foreach ($ress as $key => $row) {
            if (!$keys) $row['keyword2'] = $row['keyword'];
            else $row['keyword2'] =  strtr($row['keyword'], $keyr);
            
            $row['level'] = $row['level'] ? '危险' : '一般';
            $data[$key] = $row;
        }
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 添加词汇
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['keyword']    = trim($data['keyword']);
        $data['replace']    = trim($data['replace']);
        $data['inputtime']  = time();
        
        $where              = [];
        $where['keyword']   = $data['keyword'];
        
        $ress = $this->keysModel->where($where)->find();
        $tips = '敏感词汇『'.$data['keyword'].'』已经存在！';
        if ($ress) $this->error($tips, $data);
        
        $temp               = [];
        $temp['keyword']    = $data['keyword'];
        $temp['replace']    = $data['replace'];
        $temp['level']      = $data['level'];
        
        $tips = '添加敏感『'.$data['keyword'].'』词汇';
        $ress = $this->keysModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo: 
        
        $this->assign('data', ['level' => 0, 'replace' => '**']);
        $this->display('action');
    }
    
    /**
     * 导入词汇
     */
    public final function import() {
        if (IS_POST && I('post.dosubmit')) {
        $data = [];
        $data['inputtime']  = time();
        
        if (empty($_FILES['keywords']['error']) &&
            isset($_FILES['keywords'])) {
            $temp2 = file_get_contents($_FILES['keywords']['tmp_name']);
        }
        $array = explode("\n", $temp2);
        
        foreach ($array as $key => $val) {
        list($keyword, $replace, $level) = explode('|', $val);
        $data['keyword']    = $keyword;
        $data['replace']    = $replace;
        $data['level']      = $level;
        
        $where              = [];
        $where['keyword']   = $keyword;
        
        $ress = $this->keysModel->where($where)->find();
        if ($ress) continue;
        $this->keysModel->where($where)->add($data);
        }
        $this->success('导入敏感词汇成功！', '', U('index'));
        } // @todo:
        $this->display();
    }
    
    /**
     * 编辑词汇
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['keyword']    = trim($data['keyword']);
        $data['replace']    = trim($data['replace']);
        
        $where              = [];
        $where['keyword']   = $data['keyword'];
        
        $ress = $this->keysModel->where($where)->find();
        $tips = '敏感词汇『'.$data['keyword'].'』已经存在！';
        if ($info['keyword'] != $data['keyword'] && $ress) {
            $this->error($tips, $data);
        }
        $where              = [];
        $where['keysid']    = $info['keysid'];
        
        $ress = $this->keysModel->where($where)->save($data);
        if ($info['keyword'] == $data['keyword']) {
            $name = $info['keyword'];
        } else {
            $name = $info['keyword'].'→'.$data['keyword'];
        }
        $tips = '更新敏感『'.$data['keyword'].'』词汇';
        $temp               = [];
        $temp['keyword']    = $data['keyword'];
        $temp['replace']    = $data['replace'];
        $temp['level']      = $data['level'];
        
        if (!$ress) $this->error($tips.'失败！', $temp);
        else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo:
        
        $keysid = I('get.keysid', 0, 'intval');
        
        $where              = [];
        $where['keysid']    = $keysid;
        $data = $this->keysModel->where($where)->find();
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 导出词汇
     */
    public final function export() {
        $data = [];
        $ress = $this->keysModel->where()->order(
            '`inputtime` DESC, `keysid` DESC'
        )->select();
        foreach ($ress as $key => $row) {
            $keyword  = $row['keyword'].'|';
            $keyword .= $row['replace'].'|'.$row['level'];
            
            $data[$key] = $keyword;
        }
        header('Content-type: application/octet-stream');
        header('Accept-Ranges: bytes');
        
        header('Content-Disposition: attachment; filename=keyword.txt');
        header('Expires: 0');
        
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        echo implode("\n", $data);
        exit();
    }
    
    /**
     * 删除词汇
     */
    public final function delete() {
        $keysid = I('get.keysid', 0, 'intval');
        
        $info = I('post.info', []);
        $info = $keysid ? [$keysid] : $info['keysid'];
        $data = [];
        if (empty($info)) $this->error('请选择词汇！');
        
        foreach ($info as $key => $keysid) {
            $data['keysid'][] = $keysid;
        }
        $where              = [];
        $where['keysid']    = ['IN', $data['keysid']];
        
        $ress = $this->keysModel->where($where)->delete();
        $temp = [];
        $temp['keysid'] = implode(',', $data['keysid']);
        
        if (!$ress) $this->error('删除敏感词汇失败！', $temp);
        else $this->success('删除敏感词汇成功！', $temp);
    }
    
}
