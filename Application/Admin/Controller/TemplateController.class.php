<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块模板管理控制器类：呈现模板CURD常规管理
 * 
 * @author T-01
 */
final class TemplateController extends BaseController {
    
    public      $action     = [
        'index', 'template', 'edit', 'state', 'delete', 'update',
    ];
    
    private     $viewPath   = '',
                $skinPath   = '';
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->viewPath = RES_PATH.'Template/';
        $this->skinPath = RES_PATH.'Skin/';
    }
    
    /**
     * 模板管理
     */
    public final function index() {
        $list = [];
        $view = cms_theme_config($this->viewPath, true);
        
        foreach ($view as $key => $theme) {
            if (empty($theme)) continue;
            $list[$key] = $theme;
        }
        $this->assign('data', $list);
        $this->display('theme');
    }
    
    /**
     * 模板列表
     */
    public final function template() {
        $dirname = I('get.dirname', '');
        
        $theme = explode('/', $dirname)[0];
        $dirv2 = strtr($dirname, [$theme.'/' => '', $theme => '']);
        
        $array = explode('/', $dirname);
        array_pop($array);
        $path2 = implode('/', $array);
        
        $lists = [];
        $views = cms_template($this->viewPath.'/'.$dirname);
        
        $config = [];
        $tarray = cms_theme_config($this->viewPath, true);
        $themename = $theme;
        
        foreach ($tarray as $key => $val) {
            if ($val['dirname'] == $theme) {
                $config  = $val;
                $themename = $val['name'];
                $remarks = $val['remarks'];
                break;
            }
        }
        foreach ($views as $key => $file) {
            if ($file == '.' || $file == 'index.htm') continue;
            foreach ($remarks as $param => $value) {
                if ($param == $file) {
                    $note   = $value;
                    continue;
                }
            }
            $desc = $note ? : $remarks[$dirv2.'/'.$views[$key]];
            $name = $file == '..' ? '返回上一级目录' : $desc;
            
            $lists[$key]['dirs'] = $file;
            $lists[$key]['name'] = $name;
            
            $lists[$key]['dirname'] = $dirname.'/'.$file;
            $lists[$key]['parent'] = $path2 ? : $dirname;
            
            $isfile = $this->viewPath.$lists[$key]['dirname'];
            $lists[$key]['isfile'] = $isfile;
        }
        
        $this->assign('dirname',    $dirname);
        $this->assign('theme',      $theme);
        $this->assign('themename',  $themename);
        $this->assign('lists',      $lists);
        $this->display('index');
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $dirsname = I('get.dirname', '');
        $filepath = $this->viewPath.$dirsname.'/config.php';
        $sitetemp = load_config($filepath);
        
        $display  = $sitetemp['display'] ? 0 : 1;
        $sitetemp['display'] = $display;
        
        if (is_writable($filepath)) {
            $content = "<?php\nreturn ".var_export($sitetemp, true).';';
            file_put_contents($filepath, $content);
        }
        $sitetemp = load_config($filepath);
        
        $oper = $sitetemp['display'] ? '启用' : '禁用';
        $tips = $oper.'模板『'.$sitetemp['name'].'』主题成功！';
        cms_writelog($tips, ['dirname' => $dirsname]);
        
        echo json_encode($sitetemp['display']);
    }
    
    /**
     * 删除模板
     */
    public final function delete() {
        $dirsname = I('get.dirname', '');
        
        $info = I('post.info', []);
        $info = $dirsname ? [$dirsname] : $info['dirname'];
        $data = [];
        $skin = [];
        if (empty($info)) $this->error('请选择主题模板！');
        
        foreach ($info as $key => $dirname) {
            $data['dirname'][] =  $dirname;
            cms_delete_directory($this->viewPath.$dirname);
            cms_delete_directory($this->skinPath.$dirname);
        }
        $this->success('删除模板主题成功！', $data);
    }
    
    /**
     * 编辑模板
     */
    public final function edit() {
        if (IS_POST) {
        $info = I('post.info', []);
        $data = I('post.data', [], '');
        if (empty($data['content'])) $this->error('模板内容不能为空！');
        
        $data = $data['content'];
        $path = $this->viewPath.$info['template'];
        if (file_exists($path)) file_put_contents($path, $data);
        
        $temp = explode('/', $info['template']);
        array_pop($temp);
        $param = ['dirname' => implode('/', $temp)];
        
        $this->success(
            '更新模板成功！', $info['template'], U('template', $param)
            );
        } // @todo: 请求结束
        
        $template = I('get.template', '');
        $theme = explode('/', $template)[0];
        
        $config = [];
        $tarray = cms_theme_config($this->viewPath, true);
        $themename = $theme;
        
        foreach ($tarray as $key => $val) {
            if ($val['dirname'] == $theme) {
                $config  = $val;
                $themename = $val['name'];
                $remarks = $val['remarks'];
                break;
            }
        }
        if (file_exists($this->viewPath.$template)) {
            $content = file_get_contents($this->viewPath.$template);
        }
        $this->assign('content', $content);
        $this->assign('themename', $themename);
        $this->assign('template', $template);
        
        $this->display('edit');
    }
    
    /**
     * 更新描述
     */
    public final function update() {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $dirarrs = explode('/', $info['dirname']);
        $dirname = $dirarrs[0];
        $dstring = '';
        $remarks = [];
        
        foreach ($dirarrs as $key => $dirs) {
            if ($key == 0) continue;
            $dstring .= $dirs . '/';
        }
        $filepath = $this->viewPath.$dirname.'/config.php';
        $sitetpl = load_config($filepath);
        
        foreach ($data['dirs'] as $key => $dirs) {
            $dirs = $dstring. $dirs;
            $remarks[$dirs] = $data['name'][$key];
        }
        foreach ($sitetpl['remarks'] as $param => $desc) {
            if ($remarks[$param]) continue;
            $remarks[$param] = $desc;
        }
        $sitetpl['remarks'] = $remarks;
        
        if (is_writable($filepath)) {
            $data = "<?php\n\nreturn ".var_export($sitetpl, true).";\n";
            file_put_contents($filepath, $data);
        }
        $this->success('更新模板描述成功！', null);
    }
    
    /**
     * Ajax请求
     */
    public final function ajax() {
        $module = I('get.module', '', 'cms_addslashes');
        $theme  = I('get.theme' , '', 'cms_addslashes');
        $prefix = I('get.prefix', '', 'cms_addslashes');
        $views  = $this->viewPath;
        
        $tplarr = cms_theme_views($views, $theme, $module, $prefix);
        echo json_encode($tplarr);
    }
    
}
