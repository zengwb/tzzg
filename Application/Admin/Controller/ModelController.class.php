<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块系统模型控制器类：呈现模型CURD常规管理
 * 
 * @author T-01
 */
final class ModelController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'export',
        'state', 'update',
    ];
    
    private     $modelModel = [],
                $fieldModel = [],
                
                $appsModel  = [],
                $siteModel  = [],
                
                $skin       = null;
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->skin = cms_theme_config(RES_PATH.'Template/');
        
        $this->modelModel   = D('Admin/Model');
        $this->fieldModel   = D('Admin/ModelField');
        
        $this->appsModel    = D('Admin/Module');
        $this->siteModel    = D('Admin/Website');
    }
    
    /**
     * 模型管理
     */
    public final function index() {
        $module = I('get.module', '', 'cms_addslashes');
        
        $where              = [];
        $where['module']    = $module;
        $where['siteid']    = cms_siteid();
        if (empty($module)) unset($where['module']);
        
        $ress = $this->modelModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->select();
        
        foreach ($ress as $key => $row) {
        if (!$this->modelModel->tablechk($row['table'])) {
            continue;
        }
        $where              = [];
        $where['module']    = $row['module'];
        $app = $this->appsModel->where($where)->find();
        $row['module']= $app['name'];
        
        $row['count'] = D($row['table'])->count();
        $list[$key] = $row;
        }
        
        $this->assign('data',   $list);
        $this->assign('module', $module);
        
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['modelid'] as $key => $modelid) {
        $listorder = $this->modelModel->where([
            'modelid' => $modelid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->modelModel->where(['modelid' => $modelid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $modelid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('系统模型排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加模型
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', []);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['setting'] = serialize($setting);
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $data['inputtime']  = time();
        $data['siteid']     = cms_siteid();
        
        if (empty($_FILES['model']['error']) &&
            isset($_FILES['model'])) {
        $model = file_get_contents($_FILES['model']['tmp_name']);
        $model = unserialize($model);
        }
        $tchk = $this->modelModel->tablechk($data['table']);
        if ($tchk) $this->error('模型表已存在！');
        
        $where              = [];
        $where['module']    = $data['module'];
        $apps = $this->appsModel->where($where)->find();
        $apps = $apps['name'];
        
        $tips = !empty($model) ? '导入' : '添加';
        $tips.= '应用『'.$apps.'』模块『'.$data['name'].'』模型';
        $inid = $this->modelModel->add($data);
        
        if (!$inid) $this->error($tips.'失败！', $data);
        $this->modelModel->where(['modelid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->modelModel->create_model(
            $data['module'], $inid,
            $data['table'],  $data['name'].'表', $data['data']
        );
        if (!empty($model)) {
            $this->modelModel->import_model($inid, $model);
        }
        $link = U('index', ['module' => $info['module']]);
        if ($inid) $this->success($tips.'成功！', $data, $link);
        } // @todo:
        
        $import = I('get.import', 0, 'intval');
        $module = I('get.module', '', 'cms_addslashes');
        $module = ucwords($module);
        
        $where              = [];
        $where['ismodel']   =  1;
        $where['disabled']  =  0;
        $where['module']    = $module;
        if (empty($module)) unset($where['module']);
        
        $apps = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('import', $import);
        $this->assign('module', $module);
        
        $this->assign('apps', $apps);
        $this->assign('skin', $this->skin);
        
        $this->assign('data', ['module' => $module, 'data' => 1]);
        $this->display('action');
    }
    
    /**
     * 修改模型
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', []);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['setting'] = serialize($setting);
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $tchk = $this->modelModel->tablechk($data['table']);
        if ($tchk && $info['table'] != $data['table']) {
            $this->error('模型表已存在！');
        }
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $where              = [];
        $where['module']    = $data['module'];
        $apps = $this->appsModel->where($where)->find();
        $apps = $apps['name'];
        
        $where              = [];
        $where['modelid']   = $info['modelid'];
        $ress = $this->modelModel->where($where)->save($data);
        $temp = $this->modelModel->where($where)->find();
        
        if (!$temp['data']) $up = 0;
        else $up = 1;
        $tips = '修改应用『'.$apps.'』模块『'.$data['name'].'』模型';
        
        $this->modelModel->update_model(
            $info['table'], $data['table'], $up
        );
        $link = U('index', ['module' => $info['module']]);
        
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, $link);
        } // @todo: 
        
        $modelid = I('get.modelid', 0, 'intval');
        
        $module = I('get.module', '', 'cms_addslashes');
        $module = ucwords($module);
        
        $where              = [];
        $where['modelid']   = $modelid;
        $data = $this->modelModel->where($where)->find();
        
        $where              = [];
        $where['ismodel']   =  1;
        $where['disabled']  =  0;
        $where['module']    = $module;
        if (empty($module)) unset($where['module']);
        
        $apps = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('apps', $apps);
        $this->assign('skin', $this->skin);
        $this->assign('data', $data);
        
        $this->assign('setting', unserialize($data['setting']));
        $this->display('action');
    }
    
    /**
     * 删除模型
     */
    public final function delete() {
        $modelid = I('get.modelid', 0, 'intval');
        $info = I('post.info', []);
        $info = $modelid ? [$modelid] : $info['modelid'];
        $data = [];
        if (empty($info)) $this->error('请选择系统模型！');
        
        foreach ($info as $key => $modelid) {
        $data['modelid'][]  =  $modelid;
        $where = ['modelid' => $modelid];
        $model = $this->modelModel->where($where)->find();
        
        $this->modelModel->delete_model($model['table']);
        $this->fieldModel->where($where)->delete();
        }
        $modelid = implode(',', $data['modelid']);
        
        $where = ['modelid' => ['IN', $modelid]];
        $mress = $this->modelModel->where($where)->delete();
        
        $mtemp = ['modelid' => $modelid];
        if (!$mress) $this->error('删除系统模型失败！', $mtemp);
        else $this->success('删除系统模型成功！', $mtemp);
    }
    
    /**
     * 导出模型
     */
    public final function export() {
        $modelid = I('get.modelid', 0, 'intval');
        $info = [];
        $info['modelid'] = $modelid;
        $data = [];
        
        $array = [];
        $where = ['modelid' => $info['modelid']];
        $model = $this->modelModel->where($where)->find();
        $field = $this->fieldModel->where($where)->order(
            '`listorder` ASC, `fieldid` ASC'
        )->select();
        
        foreach ($field as $key => $row) {
        $row['setting'] = unserialize($row['setting']);
        $array[$key] = $row;
        }
        $data = serialize($array);
        $file = C('DB_PREFIX').$model['table'].'.model';
        
        header('Content-Disposition:attachment; filename="'.$file.'"');
        echo $data; exit();
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $modelid = I('get.modelid', 0, 'intval');
        $info = [];
        $info['modelid'] = $modelid;
        
        $where = ['modelid' => $info['modelid']];
        $ress2 = $this->modelModel->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where = ['modelid' => $info['modelid']];
        $ress2 = $this->modelModel->where($where)->save([
            'disabled'=>$value
        ]);
        $where = ['modelid' => $info['modelid']];
        $data2 = $this->modelModel->where($where)->find();
        $oper2 = $data2['disabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $where = ['module' => $data2['module']];
        $apps2 = $this->appsModel->where($where)->find();
        
        $tips2 = $oper2.'应用『'.$apps2['name'].'』模块『';
        $tips2.= $data2['name'].'』模型'.$state;
        cms_writelog($tips2, ['modelid' => $info['modelid']]);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 更新模型
     */
    public final function update() {
        $folder = RUNTIME_PATH;
        $folder.= 'Data'.CMS_DIRS.'_fields'.CMS_DIRS;
        
        $ress = cms_delete_directory($folder);
        if (!$ress) $this->error('更新系统模型失败！');
        else $this->success('更新系统模型成功！');
    }
    
}
