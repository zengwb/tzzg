<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块角色权限控制器类：呈现权限CURD常规管理
 * 
 * @author T-01
 */
final class PrivController extends BaseController {
    
    public      $action     = [
        'index', 'roleMenu', 'role', 'rolePriv',
        'userMenu', 'user', 'userPriv'
    ];
    
    private     $menuModel  = [],
                $tree       = null,
                $menuField  = ['menuid', 'name', 'parentid'],
                
                $userModel  = [],
                $cateModel  = [],
                $catePrivModel = [],
                $cateField  = ['catid' , 'name', 'parentid'],
                
                $roleModel  = [],
                $roleField  = ['roleid', 'name', 'parentid'],
                $privModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->userModel    = D('Admin/Admin');
        $this->menuModel    = D('Admin/Menu');
        
        $this->privModel    = D('Admin/AdminRolePriv');
        $this->roleModel    = D('Admin/AdminRole');
        
        $this->cateModel    = D('Admin/Category');
        $this->catePrivModel= D('Admin/CategoryPriv');
        
        $where              = [];
        $where['roleid']    = I('get.roleid', 1001, 'intval');
        $ress = $this->roleModel->where($where)->find();
        $this->menuid = $ress['menuid'];
    }
    
    /**
     * 角色列表
     */
    public final function index() { $this->redirect('role'); }
    
    /**
     * 角色菜单
     */
    public final function roleMenu() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', []);
        $priv = [];
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $this->catePrivModel->where($where)->delete();
        
        foreach ($data['catid'] as $key => $catid) {
        $where              = [];
        $where['catid']     = $catid;
        $cate = $this->cateModel->where($where)->find();
        
        $this->catePrivModel->add([
            'roleid'        => $info['roleid'],
            'catid'         => $cate['catid'],
            'siteid'        => $cate['siteid']
        ]);
        }
        $priv['roleid'][$data['roleid']] = $data['roleid'];
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress = $this->roleModel->where($where)->find();
        $name = $ress['name'];
        
        $this->popup(true, '设置角色『'.$name.'』导航', $priv);
        } // @todo:
        
        $roleid = I('get.roleid', 0, 'intval');
        
        $info               = [];
        $info['roleid']     = $roleid;
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        
        $ress = $this->roleModel->where($where)->find();
        $info['parentid']   = $ress['parentid'];
        
        $array = $this->cateModel->childid(0);
        $cate2 = $this->catePrivModel->catid($info['parentid']);
        $cate2 = implode(',', $cate2);
        
        $where              = [];
        $where['parentid']  = ['IN', $array];
        $where['display']   = 1;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $cate  = $this->cateModel->where($where)->select();
        
        if ($info['parentid']) $where['catid'] = ['IN', $cate2];
        $data  = $this->cateModel->treeList($where, '*');
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress  = $this->catePrivModel->where($where)->select();
        if (in_array($info['roleid'], C('SUPER_ROLEID')))
            $ress = $cate;
        foreach ($ress as $key => $row) $sarr[] = $row['catid'];
        
        $text = "<tr>
            <th class='list-checkbox'>
            <input class='checkchild' name='data[catid][]'
            value='\$catid' type='checkbox' \$checked />
            </th>
            <td class='list-small'>\$catid</td>
            <td data-catid='\$catid'>
            \$spacer\$catname
            <span class='cms-cccc'>　\$description</span>
            </td>
            <td class='cms-tc'>\$type</td>
        </tr>";
        $tree  = cms_tree_menu(
            $data,  0,
            $text, '', $sarr, ['field' => $this->cateField]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 角色列表
     */
    public final function role() {
        $parentid = I('get.parentid', 0, 'intval');
        
        $data = $temp = [];
        $info = ['parentid' => $parentid];
        
        $role = $this->roleModel->childid($info['parentid']);
        
        $where              = [];
        $where['parentid']  = ['IN', $role];
        
        $ress = $this->roleModel->treeList($where, '*');
        foreach ($ress as $key => $row) {
            $tmp2 = in_array($row['roleid'], C('SUPER_ROLEID'));
            
            $url = U('priv/rolePriv', ['roleid' => $row['roleid']]);
            $row['_oper']  = $tmp2 ?
            '<a href="javascript:void(0);" class="cms-cccc">权限设置</a> ' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(
            \''.$url.'\', \'设置【'.$row['name'].'】权限\',
            \'AdminPrivRolePriv-0-640-480\');"
            title="权限设置">权限设置</a> ';
            
            $url = U('priv/roleMenu', ['roleid' => $row['roleid']]);
            $row['_oper'] .= $tmp2 ?
            '<a href="javascript:void(0);" class="cms-cccc">栏目设置</a>' :
            '<a href="javascript:void(0);"
            onClick="javascript:yhcms.dialog.topwin(
            \''.$url.'\', \'设置【'.$row['name'].'】栏目\',
            \'AdminPrivRoleMenu-0-640-480\');"
            title="栏目设置">栏目设置</a>';
            
            $temp[$row['roleid']] = $row; // 获取角色记录
        }
        $text = "<tr>
        <th class='list-checkbox'>
        <input class='checkchild' name='info[roleid][]'
        value='\$roleid' type='checkbox' disabled />
        </th>
        <td class='list-small'>\$roleid</td>
        <td data-roleid='\$roleid'>
        \$spacer<a>\$name</a></td>
        <td class='cms-c999'>\$description</td>
        <td class='cms-tc'>\$_oper</td>
        </tr>";
        
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [], ['field' => $this->roleField]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $temp);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 角色权限
     */
    public final function rolePriv() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', []);
        $priv = [];
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $this->privModel->where($where)->delete();
        
        foreach ($data['menuid'] as $key => $menuid) {
        $where              = [];
        $where['menuid']    = $menuid;
        $menu = $this->menuModel->where($where)->find();
        
        $this->privModel->add([
            'roleid'        => $info['roleid'],
            'menuid'        => $menu['menuid'],
            'module'        => $menu['module'],
            'controller'    => $menu['controller'],
            'action'        => $menu['action'],
            'params'        => $menu['params'],
            'method'        => $menu['method'],
            'linkurl'       => $menu['linkurl'],
        ]);
        }
        $priv['roleid'][$data['roleid']] = $data['roleid'];
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress = $this->roleModel->where($where)->find();
        $name = $ress['name'];
        
        $this->popup(true, '设置角色『'.$name.'』权限', $priv);
        } // @todo: 
        
        $roleid = I('get.roleid', 0, 'intval');
        
        $info               = [];
        $info['roleid']     = $roleid;
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        
        $ress = $this->roleModel->where($where)->find();
        $info['parentid']   = $ress['parentid'];
        
        $array = $this->menuModel->childid($this->menuid);
        $menu2 = $this->privModel->menuid($info['parentid']);
        $menu2 = implode(',', $menu2);
        
        $where              = [];
        $where['parentid']  = ['IN', $array];
        $where['isattr']    =  1;
        $where['display']   =  1;
        $menu = $this->menuModel->where($where)->select();
        
        if ($info['parentid']) $where['menuid'] = ['IN', $menu2];
        $data = $this->menuModel->treeList($where, '*');
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress = $this->privModel->where($where)->select();
        if (in_array($info['roleid'], C('SUPER_ROLEID')))
            $ress = $menu;
        foreach ($ress as $key => $row) $sarr[] = $row['menuid'];
        
        $text = "<tr>
        <th class='list-checkbox'>
        <input class='checkchild' name='data[menuid][]'
        value='\$menuid' type='checkbox' \$checked />
        </th>
        <td class='list-small'>\$menuid</td>
        <td data-menuid='\$menuid'>
        \$spacer\$name
        <span class='cms-cccc'>　\$description</span>
        </td>
        <td class='cms-tc'>后台菜单</td>
        </tr>";
        $tree = cms_tree_menu(
            $data, $this->menuid,
            $text, '', $sarr, ['field' => $this->menuField]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 用户菜单
     */
    public final function userMenu() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', []);
        $menu = [];
        
        $where              = [];
        $where['userid']    = $info['userid'];
        $user = $this->userModel->where($where)->find();
        $temp = unserialize($user['setting']);
        $temp['menu'] = implode(',', $data['catid']);
        
        $info['username']   = $user['username'];
        $ress = $this->userModel->where($where)->save([
            'setting'=>serialize($temp)
        ]);
        $tips = '设置用户『'.$info['username'].'』导航';
        $temp               = [];
        $temp['userid']     = $info['userid'];
        $temp['catid']      = $data['catid'];
        
        if (!$ress) $this->popup(false, $tips, $temp);
        else $this->popup(true, $tips, $temp);
        } // @todo: 
        
        $userid = I('get.userid', 0, 'intval');
        
        $info               = [];
        $info['userid']     = $userid;
        
        $where              = [];
        $where['userid']    = $info['userid'];
        
        $user = $this->userModel->where($where)->find();
        $info['roleid']     = $user['roleid'];
        $info['setting']    = unserialize($user['setting']);
        
        $array = $this->cateModel->childid(0);
        $where              = [];
        $where['parentid']  = ['IN', $array];
        $where['display']   = 1;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $cate = $this->cateModel->where($where)->select();
        
        $menu = $this->catePrivModel->catid($info['roleid']);
        
        $where              = [];
        $where['catid']     = ['IN', $menu];
        $ress = $this->cateModel->treeList($where, '*');
        
        if (in_array($info['roleid'], C('SUPER_ROLEID')))
            $ress = $cate;
        
        $text = "<tr>
            <th class='list-checkbox'>
            <input class='checkchild' name='data[catid][]'
            value='\$catid' type='checkbox' \$checked />
            </th>
            <td class='list-small'>\$catid</td>
            <td data-catid='\$catid'>
            \$spacer\$catname
            <span class='cms-cccc'>　\$description</span>
            </td>
            <td class='cms-tc'>\$type</td>
        </tr>";
        $tree  = cms_tree_menu(
            $ress,  0,
            $text, '', explode(',', $info['setting']['menu']),
            ['field' => $this->cateField]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $ress);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 用户列表
     */
    public final function user() {
        $roleid = I('get.roleid', 0, 'intval');
        
        $info               = [];
        $info['roleid']     = $roleid;
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        if (!$info['roleid']) $where = [];
        
        $data = [];
        $ress = $this->userModel->where($where)->select();
        foreach ($ress as $key => $row) {
        $where              = [];
        $where['roleid']    = $row['roleid'];
        
        $role = $this->roleModel->where($where)->find();
        $row['rolename']    = $role['name'];
        
        $user = in_array($row['userid'], C('SUPER_USERID'));
        $role = in_array($row['roleid'], C('SUPER_ROLEID'));
        
        $temp = $role['userid'] == session('CMS_UserID');
        $row['disabled'] = $user || $role || $temp;
        
        $data[$key]         = $row; // 获取用户记录
        }
        $this->assign('data', $data);
        
        $this->display();
    }
    
    /**
     * 用户权限
     */
    public final function userPriv() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', []);
        $priv = [];
        
        $where              = [];
        $where['userid']    = $info['userid'];
        $ress = $this->userModel->where($where)->find();
        $temp = unserialize($ress['setting']);
        $temp['priv'] = implode(',', $data['menuid']);
        
        $info['username']   = $ress['username'];
        $ress = $this->userModel->where($where)->save([
            'setting'=>serialize($temp)
        ]);
        $tips = '设置用户『'.$info['username'].'』权限';
        $temp = [];
        $temp['userid']     = $info['userid'];
        $temp['menuid']     = $data['menuid'];
        
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data);
        } // @todo: 
        
        $userid = I('get.userid', 0, 'intval');
        
        $info               = [];
        $info['userid']     = $userid;
        
        $where              = [];
        $where['userid']    = $info['userid'];
        
        $ress = $this->userModel->where($where)->find();
        $temp = unserialize($ress['setting']);
        
        $info['roleid']     = $ress['roleid'];
        $info['priv']       = explode(',', $temp['priv']);
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        $ress = $this->roleModel->where($where)->find();
        $this->menuid = $ress['menuid'];
        
        $priv = $this->privModel->privs($info['roleid']);
        $priv = $priv ?: $this->menuModel->childid($this->menuid);
        
        $where              = [];
        $where['menuid']    = ['IN', $priv];
        $where['isattr']    =  1;
        $where['display']   =  1;
        $data = $this->menuModel->treeList($where, '*');
        
        $text = "<tr>
        <th class='list-checkbox'>
        <input class='checkchild' name='data[menuid][]'
        value='\$menuid' type='checkbox' \$checked />
        </th>
        <td class='list-small'>\$menuid</td>
        <td data-menuid='\$menuid'>
        \$spacer\$name
        <span class='cms-cccc'>　\$description</span>
        </td>
        <td class='cms-tc'>后台菜单</td>
        </tr>";
        $tree = cms_tree_menu(
            $data, $this->menuid,
            $text, '', $info['priv'], ['field' => $this->menuField]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
}
