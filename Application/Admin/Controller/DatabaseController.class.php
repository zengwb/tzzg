<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块数据管理控制器类：呈现数据管理、优化/修复操作
 * 
 * @author T-01
 */
final class DatabaseController extends BaseController {
    
    public      $action     = [
        'index', 'optimize', 'repair', 'execute', 'structure',
    ];
    
    private     $dataModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->dataModel    = D('Database');
    }
    
    /**
     * 数据管理
     */
    public final function index() {
        $array = [];
        $ress2 = $this->dataModel->tables();
        
        foreach ($ress2 as $key => $val) {
            $database = $val['tables_in_'. C('DB_NAME')];
            $table = $this->dataModel->status($database);
            
            $array[$key] = $table[0];
        }
        $this->assign('data', $array);
        $this->assign('list', $array);
        
        $this->display();
    }
    
    /**
     * 数据优化
     */
    public final function optimize() {
        $table = I('get.table',  '', 'cms_addslashes');
        $info  = I('post.info', []);
        $info  = $table ? [$table] : $info['table'];
        $data  = [];
        if (empty($info)) $this->error('请选择数据库表！');
        
        foreach ($info as $key => $table) {
            if (empty($table)) continue;
            $ress = $this->dataModel->optimize('optimize', $table);
            
            if ($ress[0]['msg_text'] == 'OK') {
                $data['ok'][] = $table;
            } else {
                $data['no'][] = $table;
            }
        }
        $tipok = implode(',', $data['ok']);
        $tipno = implode(',', $data['no']);
        
        if ($data['ok']) cms_writelog('数据优化成功！', $tipok);
        if ($data['no']) cms_writelog('数据优化失败！', $tipno);
        
        $this->success('数据优化完成！', '', '', 3, false);
    }
    
    /**
     * 数据修复
     */
    public final function repair() {
        $table = I('get.table',  '', 'cms_addslashes');
        $info  = I('post.info', []);
        $info  = $table ? [$table] : $info['table'];
        $data  = [];
        if (empty($info)) $this->error('请选择数据库表！');
        
        foreach ($info as $key => $table) {
            if (empty($table)) continue;
            $ress = $this->dataModel->optimize('repair', $table);
            
            if ($ress[0]['msg_text'] == 'OK') {
                $data['ok'][] = $table;
            } else {
                $data['no'][] = $table;
            }
        }
        $tipok = implode(',', $data['ok']);
        $tipno = implode(',', $data['no']);
        
        if ($data['ok']) cms_writelog('数据修复成功！', $tipok);
        if ($data['no']) cms_writelog('数据修复失败！', $tipno);
        
        $this->success('数据修复完成！', '', '', 3, false);
    }
    
    /**
     * 查看结构
     */
    public final function structure() {
        $table = I('get.table', '', 'cms_addslashes');
        $structure = $this->dataModel->structure($table);
        
        $this->assign('structure', $structure[0]['create table']);
        $this->display();
    }
    
    /**
     * 执行SQL
     */
    public final function execute() {
        if (IS_POST) {
        $data = I('post.data', []);
        if (empty($data['sql'])) $this->error('请输入SQL语句！');
        
        $sqlarr = explode("\n", $data['sql']);
        foreach ($sqlarr as $key => $sql) {
            if (!is_string($sql)) continue;
            $this->dataModel->execute($sql);
        }
        $data = implode(',', $sqlarr);
        $this->success('SQL执行完成！', $data, '', 3);
        } // @todo: 
        
        $this->display();
    }
    
}
