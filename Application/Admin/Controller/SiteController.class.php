<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块站点管理控制器类：呈现站点CURD常规管理
 * 
 * @author T-01
 */
final class SiteController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state',
        'setting', 'logo',
    ];
    
    private     $pin2Model  = [],
                $siteModel  = [],
                $skin       = [],
                
                $upload     = null;
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->pin2Model    = D('Common/Pinyin');
        $this->siteModel    = D('Admin/Website');
        
        $this->skin = cms_theme_config(RES_PATH.'Template/', true);
        $this->upload = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
    }
    
    /**
     * 站点管理
     */
    public final function index() {
        $site = $this->siteModel->where()->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $this->assign('data', $site);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['siteid'] as $key => $siteid) {
        $listorder = $this->siteModel->where([
            'siteid' => $siteid
        ])->find();
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->siteModel->where(['siteid' => $siteid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $siteid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('应用站点排序成功！', $listorder, '');
    }
    
    /**
     * 添加站点
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        
        if (empty($data['dirname'])) {
            $data['dirname'] = $this->pin2Model->output($data['name']);
        }
        if ($data['dirname'] == '/') $data['dirname'] = '';
        $data['setting'] = serialize($data['setting']);
        
        $str2 = ["\r" => '|'];
        $data['domainarr'] = strtr($data['domainarr'], $str2);
        
        $where              = [];
        $where['dirname']   = $data['dirname'];
        $where['name']      = $data['name'];
        $ress = $this->siteModel->where($where)->find();
        
        $tips = '应用站点『'.$data['name'].'』已经存在！';
        if ($ress) $this->error($tips, $data['name']);
        
        $dirs = C('CMS_WEBDIR_PATH').$data['dirname'];
        if (!is_dir($dirs)) @mkdir($dirs, 0755, true);
        
        $tips = '添加应用『'.$data['name'].'』站点';
        $inid = $this->siteModel->add($data);
        
        $this->siteModel->where(['siteid' => $inid])->save([
            'listorder' => $inid, 'token' => cms_token($inid, $inid)
        ]);
        if (!$inid) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $skin = [];
        foreach ($this->skin as $key => $val) {
            if (!$val['display']) continue;
            
            $skin[$key]['dirs'] = $val['dirname'];
            $skin[$key]['name'] = $val['name'];
        }
        
        $this->assign('skin', $skin);
        $this->assign('data', ['dirname' => '/', 'display' => 1]);
        $this->display('action');
    }
    
    /**
     * 修改站点
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        $site = $this->siteModel->where($where)->find();
        $setting = unserialize($site['setting']);
        
        if (empty($data['dirname'])) {
            $data['dirname'] = $this->pin2Model->output($data['name']);
        }
        if ($data['dirname'] == '/') $data['dirname'] = '';
        
        $data['setting'] = array_merge($setting, $data['setting']);
        $data['setting'] = serialize($data['setting']);
        
        $str2 = ["\r" => '|'];
        $data['domainarr'] = strtr($data['domainarr'], $str2);
        
        $sitedir = C('CMS_WEBDIR_PATH').$data['dirname'];
        $oldsite = C('CMS_WEBDIR_PATH').$info['dirname'];
        
        if ($info['dirname'] != $data['dirname'] &&
            $data['dirname'] != '/' && is_dir($oldsite)) {
            @rename($oldsite, $sitedir);
        }
        if (!is_dir($sitedir)) @mkdir($sitedir, 0755, true);
            
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $where = ['siteid' => $info['siteid']];
        $ress = $this->siteModel->where($where)->save($data);
        
        $tips = '更新应用『'.$name.'』站点';
        $info = ['siteid' => $info['siteid']];
        
        if (!$ress) $this->error($tips.'失败！', $info);
        else $this->success($tips.'成功！', $info, U('index'));
        } // @todo: 
        
        $siteid = I('get.siteid', 0, 'intval');
        $info = [];
        $info['siteid']     = $siteid;
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        $data = $this->siteModel->where($where)->find();
        $setting = unserialize($data['setting']);
        
        $str2 = ['|' => "\r"];
        $data['domainarr']  = strtr($data['domainarr'], $str2);
        
        $skin = [];
        foreach ($this->skin as $key => $val) {
            if (!$val['display']) continue;
            
            $skin[$key]['dirs'] = $val['dirname'];
            $skin[$key]['name'] = $val['name'];
        }
        $this->assign('skin', $skin);
        $this->assign('data', $data);
        
        $this->assign('theme',$setting['themes']);
        $this->display('action');
    }
    
    /**
     * 删除站点
     */
    public final function delete() {
        $siteid = I('get.siteid', 0, 'intval');
        $info = I('post.info', []);
        $info = $siteid ? [$siteid] : $info['siteid'];
        $data = [];
        if (empty($info)) $this->error('请选择应用站点！');
        
        foreach ($info as $key => $siteid) {
        $data['siteid'][] = $siteid;
        
        $where = ['siteid' => $siteid];
        $ress2 = $this->siteModel->where($where)->find();
        
        $setting = unserialize($ress2['setting']);
        // 删除水印
        $wmimage = $setting['wmimage'];
        if (file_exists($wmimage)) @unlink($wmimage);
        // 删除LOGO
        $weblogo = $setting['logo'];
        if (file_exists($weblogo)) @unlink($weblogo);
        // 删除目录
        $dirname = $ress2['dirname'];
        if (empty($dirname) || $dirname == '/') continue;
        $dirs[] = dirname(APP_PATH).'/Bootstrap/Webroots/'.$dirname;
        }
        $siteid = implode(',', $data['siteid']);
        
        $where = ['siteid' => ['IN', $siteid]];
        $ress2 = $this->siteModel->where($where)->delete();
        $temp2 = ['siteid' => $siteid];
        
        if ( $ress2)
        foreach ($dirs as $key => $dir) {
            cms_delete_directory($dir);
        }
        if (!$ress2) $this->error('删除应用站点失败！', $temp2);
        else $this->success('删除应用站点成功！', $temp2);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $siteid = I('get.siteid', 0, 'intval');
        $info = [];
        $info['siteid'] = $siteid;
        
        $where = ['siteid' => $info['siteid']];
        $ress2 = $this->siteModel->where($where)->find();
        $value = $ress2['display'] ? 0 : 1;
        
        $where = ['siteid' => $info['siteid']];
        $ress2 = $this->siteModel->where($where)->save([
            'display' => $value
        ]);
        $where = ['siteid' => $info['siteid']];
        $data2 = $this->siteModel->where($where)->find();
        $oper2 = $data2['display'] ? '关闭' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'应用『'.$data2['name'].'』站点'.$state;
        cms_writelog($tips2, ['siteid' => $info['siteid']]);
        
        echo json_encode($data2['display']);
    }
    
    /**
     * 设置站点
     */
    public final function setting() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        $site = $this->siteModel->where($where)->find();
        $setting = unserialize($site['setting']);
        
        if (empty($_FILES['logo']['error']) &&
            isset($_FILES['logo'])) {
            
        $this->upload->rootPath.= 'Statics/Logo/';
        $this->upload->subName  = null;
        $this->upload->saveName = date('YmdHis', time());
        $this->upload->ext      = ['png','gif','jpg','jpeg'];
        
        $file = $this->upload->uploadOne($_FILES['logo']);
        if (!$file) $this->error('LOGO上传失败！');
        
        $data['logo']   = $this->upload->rootPath;
        $data['logo']  .= $file['savepath'];
        $data['logo']  .= $file['savename'];
        
        $temp = [];
        $temp['filename']   = $file['name'];
        $temp['filesize']   = $file['size'];
        $temp['fileext']    = $file['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['logo'];
        cms_attachment($temp); // @todo: 写入附件
        }
        $data = array_merge($setting, $data);
        $data = ['setting' => serialize($data)];
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        $ress = $this->siteModel->where($where)->save($data);
        
        $tips = '设置应用『'.$info['name'].'』站点';
        $info = ['siteid' => $info['siteid']];
        
        if (!$ress) $this->error($tips.'失败！', $info);
        else $this->success($tips.'成功！', $info, U('index'));
        } // @todo: 
        
        $siteid = I('get.siteid', 0, 'intval');
        $info = ['siteid' => $siteid];
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        
        $data = $this->siteModel->where($where)->find();
        $setting = unserialize($data['setting']);
        $setting = cms_stripslashes($setting);
        
        $str2 = ['|' => "\r"];
        $data['domainarr']  = strtr($data['domainarr'], $str2);
        
        $this->assign('data', $data);
        $this->assign('setting', $setting);
        $this->display();
    }
    
    /**
     * 默认主题（皮肤）
     */
    public final function skin() {
        $array = I('get.array', []);
        $theme = [];
        $i = 0;
        foreach ($this->skin as $key => $val) {
        if (!in_array($val['dirname'], $array)) continue;
        $theme[$i]['dirs'] = $val['dirname'];
        $theme[$i]['name'] = $val['name'];
        $i++;
        }
        echo json_encode($theme);
    }
    
    /**
     * 删除LOGO
     */
    public final function logo() {
        $siteid = I('get.siteid', 0, 'intval');
        $info = ['siteid' => $siteid];
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        
        $ress = $this->siteModel->where($where)->find();
        $data = unserialize($ress['setting']);
        
        $logo = $data['logo'];
        if (file_exists($logo)) @unlink($logo);
        
        $data['logo'] = '';
        $data = ['setting' => serialize($data)];
        
        $where              = [];
        $where['siteid']    = $info['siteid'];
        
        $ress = $this->siteModel->where($where)->save($data);
        if (!$ress) $this->error('删除LOGO失败！', $info);
        else $this->success('删除LOGO成功！', $info, null);
    }
    
}
