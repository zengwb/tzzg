<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块应用管理控制器类：呈现模块CURD常规管理
 * 
 * @author T-01
 */
final class ModuleController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete',
        'state', 'setting', 'helper',
    ];
    private     $appsModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->appsModel    = D('Module');
    }
    
    /**
     * 模块管理
     */
    public final function index() {
        $where = [];
        $lists = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('data', $lists);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['moduleid'] as $key => $moduleid) {
        $listorder = $this->appsModel->where([
            'moduleid' => $moduleid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->appsModel->where([
            'moduleid'  => $moduleid
        ])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $moduleid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('系统模块排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加模块
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        if ($data['name'] && empty($data['description'])) {
            $data['description'] = $data['name'];
        }
        $remarks = serialize($data['remarks']);
        $data['remarks'] = $remarks;
        
        $where = ['module' => $data['module']];
        $ress = $this->appsModel->where($where)->find();
        $deny = in_array($data['module'], C('MODULE_DENY_LIST'));
        
        if ($ress || $deny) {
            $tips = '系统模块『'.$data['module'].'』目录存在！';
            $this->error($tips);
        }
        if ($data['iscore']) $attr = '核心';
        else $attr = '扩展';
        $inid = $this->appsModel->add($data);
        
        $tips = '添加系统『'.$data['name'].'』模块';
        if (!$inid) $this->error($tips.'失败！');
        
        $this->appsModel->where(['moduleid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $checkup = I('get.checkup', 0, 'intval');
        $frmlist = [];
        $applist = cms_directory(APP_PATH);
        $i = 0;
        
        foreach ($applist as $key => $module) {
        if (in_array($module, C('MODULE_DENY_LIST'))) {
            continue;
        }
        $where = ['module' => $module];
        $ress2 = $this->appsModel->where($where)->find();
        if ($ress2) continue;
        
        $config = cms_module_config(APP_PATH.$module)[1];
        if (!$config) continue;
        
        $frmlist[$key]['module'] = $module;
        $frmlist[$key]['name'] = $config['name'];
        }
        $data = ['iscore' => 0, 'ismodel' => 0, 'disabled' => 0];
        
        $this->assign('data', $data);
        $this->assign('frmlist', $checkup ? $frmlist : []);
        
        $this->display('action');
    }
    
    /**
     * 编辑模块
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        if ($data['name'] && empty($data['description'])) {
            $data['description'] = $data['name'];
        }
        $remarks = serialize($data['remarks']);
        $data['remarks'] = $remarks;
        
        $where = ['module' => $data['module']];
        $ress = $this->appsModel->where($where)->find();
        $deny = in_array($data['module'], C('MODULE_DENY_LIST'));
        
        $chk2 = $info['module'] != $data['module'];
        if ($chk2 && ($ress || $deny)) {
            $tips = '系统模块『'.$data['module'].'』目录存在！';
            $this->error($tips);
        }
        if ($data['iscore']) $attr = '核心';
        else $attr = '扩展';
        
        $where = ['module' => $info['module']];
        $ress = $this->appsModel->where($where)->save($data);
        
        $tips = '更新'.$attr.'『'.$info['module'].'』模块';
        $temp = ['module' => $info['module']];
        
        if (!$ress) $this->error($tips.'失败！', $temp);
        else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo:
        
        $info = [];
        $info['module'] = I('get.module', '');
        
        $data = [];
        $temp = ['module' => $info['module']];
        $data = $this->appsModel->where($temp)->find();
        
        $data['remarks'] = unserialize($data['remarks']);
        $data['url'] = $data['remarks']['url'];
        $data['author'] = $data['remarks']['author'];
        
        $this->assign('data', $data);
        $this->assign('disabled', $data['module'] == MODULE_NAME);
        
        $this->display('action');
    }
    
    /**
     * 删除模块
     */
    public final function delete() {
        $moduleid = I('get.moduleid', '');
        $info = I('post.info', []);
        $info = $moduleid ? [$moduleid] : $info['moduleid'];
        $data = [];
        if (empty($info)) $this->error('请选择系统模块！');
        
        foreach ($info as $key => $moduleid) {
        $data['moduleid'][] = $moduleid;
        }
        $moduleid = implode(',', $data['moduleid']);
        
        $where = ['moduleid' => ['IN', $moduleid]];
        $ress = $this->appsModel->where($where)->delete();
        $temp = 'moduleid: '.$moduleid;
        
        if (!$ress) $this->error('删除系统模块失败！', $temp);
        else $this->success('删除系统模块成功！', $temp);
    }
    
    /**
     * 配置模块
     */
    public final function setting() {}
    
    /**
     * 模块状态
     */
    public final function state() {
        $info2 = [];
        $info2['module'] = I('get.module',  '');
        
        $where = ['module' => $info2['module']];
        $ress2 = $this->appsModel->where($where)->find();
        $state = $ress2['disabled'] ? 0 : 1;
        
        if ($ress2['iscore'] || $ress2['module'] == MODULE_NAME) {
        if ($ress2['iscore']) $attr = '核心';
        else $attr = '扩展';
        
        $tips2 = '设置'.$attr.'『'.$ress2['name'].'』模块无效！';
        cms_writelog($tips2, ['module' => $info2['module']]);
        
        echo json_encode(2); return false;
        }
        $where = [];
        $where['module'] = $info2['module'];
        $ress2 = $this->appsModel->where($where)->save([
            'disabled'=>$state
        ]);
        $data2 = $this->appsModel->where($where)->find();
        $oper2 = $data2['disabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        if ( $data2) $attr2 = '核心';
        else $attr2 = '扩展';
        
        $tips2 = $oper2.$attr2.'『'.$data2['module'].'』模块'.$state;
        cms_writelog($tips2, ['module' => $info2['module']]);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 模块助手（Ajax请求）
     */
    public final function helper() {
        $module = I('get.module', '');
        $config = cms_module_config(APP_PATH.$module)[1];
        
        if (in_array($module, C('MODULE_DENY_LIST'))) $config = null;
        echo json_encode($config);
    }
    
}
