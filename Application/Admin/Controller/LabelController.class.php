<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块标签管理控制器类：呈现标签CURD常规管理
 * 
 * @author T-01
 */
final class LabelController extends BaseController {
    
    public      $action     = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    public final function index() {
        echo __METHOD__;
    }
    
}
