<?php

namespace Admin\Controller;
use Think\Upload;
defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块事业单位控制器类：呈现机构CURD常规管理
 * 
 * 
 * @author T-01
 */
final class InstitutionController extends BaseController {
    
    public  $action = ['index', 'listorder', 'add', 'addAll', 'edit', 'delete', 'state', 'move', 'units', 'abouts'];
    
    private $menuModel      = [],
            $unitModel      = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->menuModel    = D('Admin/Linkage');
        $this->unitModel    = D('Admin/Institution');
        
        session('CMS_POPUP', null);
    }
    
    /**
     * 联动列表
     */
    public final function index() {
        $parentid = I('get.parentid', 0, 'intval');
        $info = ['parentid' => $parentid];
        
        $where = [];
        $where['parentid']  = ['IN', [1, 3285]];
        if ($parentid) $where['parentid'] = $parentid;
        $where['display']   = 1;
        
        $order = [];
        $order['listorder'] = 'DESC';
        $order['linkageid'] = 'ASC';
        $ress = $this->menuModel->where($where)->order($order)->select();
        $list = $this->menuModel->catalog($info['parentid']);
        $list = $list ? $list['name'] : '';

        $this->assign('info', $info);
        $this->assign('data', $ress);
        $this->assign('list', $list);
        $this->display();
    }
    
    /**
     * 事业单位
     */
    public final function units() {
        if (IS_POST) {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        $linkageid = I('get.linkageid', 0, 'intval');
        
        $keyword = trim($keyword);
        $this->redirect('units', ['linkageid' => $linkageid, 'key' => $keyword]);
        }
        
        $keyword    = I('get.key', '', 'cms_addslashes');
        $linkageid  = I('get.linkageid', 0, 'intval');
        $unitid     = I('get.parentid', 0, 'intval');
        
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['parentid']   = $unitid;
        $info['unitid']     = $unitid;
        $info['keys']       = $keyword;
        
        $where              = [];
        $where['linkageid'] = $info['unitid'];
        
        $ress = $this->unitModel->where($where)->find();
        $name = $ress['name'];
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['parentid']  = $info['unitid'];
        if ($info['keys']) {
            $where['name']  = ['LIKE', '%'.$info['keys'].'%'];
            unset($where['parentid']);
        }
        $order              = [];
        $order['listorder'] = 'ASC';
        $order['unitid']    = 'ASC';
        
        $nums = $this->unitModel->where($where)->count();
        $rows = 10;
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $info['keys'];
        
        $keys = [
            $info['keys'] => '<span class="cms-cf60">'.$info['keys'].'</span>'
        ];
        $ress = $this->unitModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();

        $data = [];
        foreach ($ress as $key => $row) {
        if ($info['keys']) {
            $row['title'] = strtr($row['name'], $keys);
        } else {
            $row['title'] = $row['name'];
        }
        $area = $this->menuModel->where(['linkageid' => $row['linkageid']])->find();
        $row['area'] = $area['name'] ?: '';
        $data[$key] = $row;
        }
        $list = $this->unitModel->get_unit_catalog($info['unitid'])['name'];

        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->assign('list', $list);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['unitid'] as $key => $unitid) {
        $where = ['unitid' => $unitid];
        $ress2 = $this->unitModel->where($where)->find();
        
        $listorder = $ress2['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $ress2 = $this->unitModel->where($where)->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $unitid.'='.$data['listorder'][$key];
        $list['listorder'][] = $listorder;
        }
        $listorder = $list ?: 'null';
        self::success('行政单位排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加单位
     */
    public final function add() {
        session('CMS_POPUP', "AdminInstitutionUnits");
        
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        $data['linkageid']  = $info['linkageid'];
        
        $tips = '行政单位『'.$data['name'].'』同名，当前操作';
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->unitModel->where($where)->find();
        if ($ress) $this->popup(false, $tips, $data);
        
        $data['description'] = $data['description'] ?: $data['name'];
        
        $inid = $this->unitModel->add($data);
        $tips = '添加行政『'.$data['name'].'』单位';
        if (!$inid) $this->popup(false, $tips, $data);
        
        $this->unitModel->where(['unitid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->popup(true, $tips, $data, null, 3);
        } // @todo:
        
        $linkageid  = I('get.linkageid', 0, 'intval');
        $parentid   = I('get.parentid' , 0, 'intval');
        
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['parentid']   = $parentid;
        
        $data               = [];
        $data['parentid']   = $info['parentid'];
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['linkageid'] = $info['linkageid'];
        $where['disabled']  =  0;
        
        if ($info['parentid']) {
            $childid = $this->unitModel->get_unit_childid($info['parentid']);
            $where['parentid'] = ['IN', $childid];
        }
        $temp = $this->unitModel->get_unit_tree($where);
        $text = <<<echo
        <option value='\$unitid' \$selected>\$spacer\$name</option>
echo;
        $tree = cms_tree_menu(
            $temp, $info['parentid'], $text, '',
            ['field' => ['unitid', 'name', 'parentid']]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->display('action');
    }
    
    /**
     * 批量添加
     */
    public final function addAll() {
        session('CMS_POPUP', "AdminInstitutionUnits");
        
        if (IS_POST) {
        $data = I('post.data', [], 'cms_addslashes');
        $info = I('post.info', []);
        
        $array = explode("\n", $data['nameall']);
        unset($data['nameall']);
        
        $tips = '批量添加行政单位';
        foreach ($array as $key => $name) {
        $name = strtr($name, ['\n' => '', '\r' => '', '\t' => '']);
        if (empty($name)) continue;
        $data['name'] = trim($name);
        $data['description'] = $data['name'];
        $data['linkageid']  = $info['linkageid'];
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['linkageid'] = $info['linkageid'];
        $where['name'] = $data['name'];
        $ress = $this->unitModel->where($where)->find();
        
        $inid = $this->unitModel->add($data);
        $this->unitModel->where(['unitid' => $inid])->save([
            'listorder' => $inid
        ]);
        }
        $this->popup(true, $tips, $data, null, 3);
        } // @todo:
        
        $linkageid  = I('get.linkageid', 0, 'intval');
        $parentid   = I('get.parentid' , 0, 'intval');
        
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['parentid']   = $parentid;
        
        $data               = [];
        $data['parentid']   = $info['parentid'];
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['linkageid'] = $info['linkageid'];
        $where['disabled']  =  0;
        
        if ($info['parentid']) {
            $childid = $this->unitModel->get_unit_childid($info['parentid']);
            $where['parentid'] = ['IN', $childid];
        }
        $temp = $this->unitModel->get_unit_tree($where);
        $text = <<<echo
        <option value='\$unitid' \$selected>\$spacer\$name</option>
echo;
        $tree = cms_tree_menu(
            $temp, $info['parentid'], $text, '',
            ['field' => ['unitid', 'name', 'parentid']]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->display();
    }
    
    /**
     * 修改单位
     */
    public final function edit() {
        session('CMS_POPUP', "AdminInstitutionUnits");
        
        if (IS_POST) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        $tips = '行政单位『'.$data['name'].'』同名，当前操作';
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->unitModel->where($where)->find();
        if ($info['name'] != $data['name'] && $ress) {
            $this->popup(false, $tips, $data);
        }
        if ($info['name'] != $data['name']) {
            $name = $info['name'].'→'.$data['name'];
        } else {
            $name = $data['name'];
        }
        $data['description'] = $data['description'] ?: $data['name'];
        
        $where              = [];
        $where['unitid']    = $info['unitid'];
        $ress = $this->unitModel->where($where)->save($data);
        
        $tips = '更新行政『'.$name.'』单位';
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, '', 3);
        } // @todo:
        
        $linkageid  = I('get.linkageid', 0, 'intval');
        $unitid     = I('get.unitid', 0, 'intval');
        
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['unitid']     = $unitid;
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['unitid']    = $info['unitid'];
        
        $data = $this->unitModel->where($where)->find();
        $info['parentid']   = $data['parentid'];
        $info['name']       = $data['name'];
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['linkageid'] = $info['linkageid'];
        $where['unitid']    = ['NEQ', $info['unitid']];
        $where['disabled']  =  0;
        if ($info['parentid']) {
            $childid = $this->unitModel->get_unit_childid($info['parentid']);
            $where['parentid'] = ['IN', $childid];
        }
        $temp = $this->unitModel->get_unit_tree($where);
        $text = <<<echo
        <option value='\$unitid' \$selected>\$spacer\$name</option>
echo;
        $tree = cms_tree_menu(
            $temp, $info['parentid'], $text, '',
            ['field' => ['unitid', 'name', 'parentid']]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->display('action');
    }
    
    /**
     * 删除单位
     */
    public final function delete() {
        $unitid = I('get.unitid', 0, 'intval');
        $linkageid = I('get.linkageid', 0, 'intval');
        
        $info = I('post.info', []);
        
        $data = [];
        $temp = '';
        
        $info['unitid'] = $unitid ? [$unitid] : $info['unitid'];
        $info['linkageid'] = $linkageid ?: $info['linkageid'];
        
        if (empty($info['unitid'])) $this->error('请选择待删除的行政单位！');
        
        foreach ($info['unitid'] as $key => $unitid) {
            $temp .= $this->unitModel->get_unit_childid($unitid).',';
        }
        $data['unitid'] = explode(',', $temp);
        
        $temp = ['unitid' => ['IN', $temp]];
        $ress = $this->unitModel->where($temp)->delete();
        
        if (!$ress) $this->error('删除行政单位失败！', $data);
        else $this->success('删除行政单位成功！', $data);
    }
    
    /**
     * 更新状态
     */
    public final function state() {
        $unitid = I('get.unitid', 0, 'intval');
        $where = ['unitid' => $unitid];
        
        $ress2 = $this->unitModel->where($where)->find();
        $state = $ress2['disabled'] ? 0 : 1;
        
        $ress2 = $this->unitModel->where($where)->save([
            'disabled'=> $state
        ]);
        $ress2 = $this->unitModel->where($where)->find();
        $oper2 = $ress2['disabled'] ? '锁定' : '开启';
        
        if ($ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'行政『'.$ress2['name'].'』单位'.$state;
        cms_writelog($tips2, ['unitid' => $unitid]);
        
        echo json_encode($ress2['disabled']);
    }
    
    /**
     * 单位介绍
     */
    public final function abouts() {
        session('CMS_POPUP', "AdminInstitutionUnits");
        
        if (IS_POST) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['unitid']    = $info['unitid'];
        $ress = $this->unitModel->where($where)->save($data);
        
        $tips = '更新行政单位『'.$info['name'].'』介绍';
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, '', 3);
        } // @todo: 
        
        $linkageid  = I('get.linkageid', 0, 'intval');
        $unitid     = I('get.unitid', 0, 'intval');
        
        $info               = [];
        $info['linkageid']  = $linkageid;
        $info['unitid']     = $unitid;
        
        $where              = [];
        $where['linkageid'] = $info['linkageid'];
        $where['unitid']    = $info['unitid'];
        $data = $this->unitModel->where($where)->find();
        $info['name']       = $data['name'];
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->display();
    }

    /*
     * 单位图片
     */
    public final function image(){
        session('CMS_POPUP', "AdminInstitutionUnits");
        if(IS_POST){
            $upload = new \Think\Upload();// 实例化上传类
            $upload->maxSize   =     3145728 ;// 设置附件上传大小
            $upload->subName = array('date','Ymd');
            $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            $upload->rootPath  =     './Temporary/Attachment/Units/'; // 设置附件上传根目录
            // 上传单个文件
            $info   =   $upload->uploadOne($_FILES['photo']);

            if(!$info) {// 上传错误提示错误信息
                $this->popup(true, "图片上传失败", null, null, 3);
            }else{// 上传成功
                $path = $upload->rootPath.$info['savepath'].$info['savename'];
                $data['image'] = $path;
                $save = $this->unitModel->where(I('get.'))->save($data);
                if($save){
                    $this->popup(true, "图片上传", null, null, 3);
                }else{
                    $this->popup(true, "图片上传失败", null, null, 3);
                }
            }
        } // @todo:

        $where = [
            'linkageid' => I('get.')['linkageid'],
            'unitid' => I('get.')['parentid']
        ];
        $data = $this->unitModel->where($where)->find();
        $image = $data['image'] ? : "未上传图片";
        $this->assign('data', I('get.'));
        $this->assign('image',$image);
        $this->display();
    }
    
}