<?php

namespace Admin\Controller;

use Think\Upload;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块栏目管理控制器类：呈现栏目CURD常规管理
 * 
 * @author T-01
 */
final class CategoryController extends BaseController {
    
    public      $action     = [
        'index','listorder','add','edit','delete','state','move',
        'image','model','config','infos','output',
    ];
    
    private     $type       = ['内部栏目', '外部链接', '单独页面'],
                
                $appsModel  = [],
                $modeModel  = [],
                
                $cateModel  = [],
                $pageModel  = [],
                
                $treeField  = ['catid', 'catname', 'parentid'],
                $siteTheme  = [],
                
                $pinyin     = [],
                $upload     = null;
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->appsModel    = D('Admin/Module');
        $this->modeModel    = D('Admin/Model');
        
        $this->cateModel    = D('Admin/Category');
        $this->pageModel    = D('Admin/Pages');
        
        $this->siteTheme = cms_theme_config(RES_PATH.'Template/');
        $this->pinyin = (new \Common\Model\PinyinModel());
        
        $this->upload = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Category/';
        
        $this->upload->subName  = null;
        $this->upload->ext  = ['jpg', 'jpeg', 'gif', 'png'];
    }
    
    /**
     * 栏目管理
     */
    public final function index() {
        $parentid = I('get.parentid', 0, 'intval');
        $info = ['parentid' => $parentid];
        
        $where              = [];
        $where['siteid']    = ['In', [0, cms_siteid()]];
        
        $cates = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        foreach ($cates as $key => $row) {
        $where = ['modelid' => $row['modelid']];
        $model = $this->modeModel->where($where)->find();
        $table = $model['table'];
        
        $catid = $this->cateModel->childid($row['catid']);
        $count = '';
        if ($table) {
            $where = ['catid' => ['IN', $catid]];
            $count = D($table)->where($where)->count();
        }
        $row['number'] = $count;
        
        $title = $row['type'] == 1 ? '外部链接' : '单独页面';
        $row['model']  = $model['name'] ? : $title;
        
        $row['typename'] = $this->type[$row['type']];
        
        if ($row['display']) {
        $row['_state']  = '<a data-catid="'.$row['catid'].
        '" href="javascript:void(0);" '.
        'class="list-operation" title="单击前端隐藏">';
        $row['_state'] .= '<i class="iconfont icon-qiyong"></i>';
        $row['_state'] .= '</a>';
        $row['_trcss']  = '';
        } else {
        $row['_state']  = '<a data-catid="'.$row['catid'].
        '" href="javascript:void(0);" '.
        'class="list-operation" title="单击前端显示">';
        $row['_state'] .= '<i class="iconfont icon-qingchu"></i>';
        $row['_state'] .= '</a>';
        $row['_trcss']  = ' class="display"';
        }
        $row['_manage']  = '';
        $param = ['parentid'=>$row['catid'], 'cattype'=>$row['type']];
        if ($row['type'] == 0) {
        $row['_manage'] .= '<a href="
        javascript:void(0);" onClick="yhcms.common.linkurl(\''.
        U('add', $param).'\');" title="添加子栏目">添加</a> ';
        }
        $param = ['catid' => $row['catid'], 'cattype' => $row['type']];
        $row['_manage'] .= '<a href="
        javascript:void(0)"; onClick="yhcms.common.linkurl(\''.
        U('edit', $param).'\');" title="修改栏目">修改</a> ';
        $row['_manage'] .= '<a href="
        javascript:void(0);" onClick="yhcms.dialog.topwin(\''.
        U('move', ['catid' => $row['catid']]).
        '\', \'移动栏目\', \'AdminCategoryMove-0-540-56\');"
        title="移动栏目">移动 </a> ';
        $row['_manage'] .= '<a href="
        javascript:void(0);" onClick="yhcms.dialog.tips(\''.
        U('delete', ['catid' => $row['catid']]).
        '\', \'确认删除『' .$row['catname'] . '』栏目导航！\');"
        title="删除栏目">删除</a>';
        if ($row['linkage']) {
        $row['_link'] = $row['linkage'];
        } else {
        $param = ['catid'=>$row['catid']];
        $row['_link'] = $this->domain.U('home/index/lists', $param);
        }
        $row['style'] = unserialize($row['style']);
        $data[$key] = $row;
        }
        $trbr = "<tr\$_trcss>
        <th class='list-checkbox'>
        <input class='checkchild' type='checkbox'
        name='info[catid][]' value='\$catid' />
        </th>
        <td class='list-small'>\$catid</td>
        <td class='list-listorder'>
        <input type='hidden' name='data[catid][]'
        value='\$catid' />
        <input type='text'
        name='data[listorder][]' value='\$listorder'
        class='form-control input-sm list-input-listorder'
        maxlength='4' autocomplete='off' />
        </td>
        <td data-catid='\$catid' data-cattype='\$type'>
        \$spacer
        <a href='\$_link' class='cms-c333' target='_blank'
        style='font-weight:\$style[bold]; color:\$style[color]'>
        \$catname</a></td>
        <td class='cms-tc'>\$typename</td>
        <td class='cms-c999'>\$description</td>
        <td class='cms-tc'>\$model</td>
        <td class='cms-tc'>\$number</td>
        <td id='state' class='cms-tc icon-color'>\$_state</td>
        <td class='cms-tr'>\$_manage</td>
        </tr>";
        $cattree = cms_tree_menu(
            $data, 0,
            $trbr, '', [], ['field' => $this->treeField]
        );
        
        $this->assign('data', $data);
        $this->assign('cattree', $cattree);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['catid'] as $key => $catid) {
        $listorder = $this->cateModel->where([
            'catid'=>$catid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->cateModel->where(['catid' => $catid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $catid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('栏目导航排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加栏目
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', []);
        $style   = I('post.style',   []);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        if ($data['catname'] && empty($data['description'])) {
            $data['description'] = $data['catname'];
        }
        if ($data['catname'] && empty($data['name'])) {
            $data['name'] = $data['catname'];
        }
        $data['catdir'] = $data['catdir'] ? : '';
        
        $dirs = $this->pinyin->output($data['name']);
        $data['catdir']     = $data['catdir'] ? : $dirs;
        
        $data['style']      = serialize($style);
        $data['setting']    = serialize($setting);
        $data['siteid']     = cms_siteid();
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['type']      = $data['type'];
        $where['catdir']    = $data['catdir'];
        $cates = $this->cateModel->where($where)->find();
        if ($cates) $this->error('目录已经存在！');
        
        if (empty($_FILES['image']['error']) &&
            isset($_FILES['image'])) {
        $this->upload->saveName = $data['type'].'-';
        $this->upload->saveName.= time();
        
        $file = $this->upload->upload();
        if (!$file) $this->error('图片上传失败！');
        
        $data['image']  = $this->upload->rootPath;
        $data['image'] .= $file['image']['savepath'];
        $data['image'] .= $file['image']['savename'];
        }
        if ($data['linkage']) unset($data['catdir']);
        
        $tips = '添加栏目『'.$data['catname'].'』导航';
        $ress = $this->cateModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        $inid = $this->cateModel->getLastInsID();
        
        if ($file && $inid) {
        $temp = [];
        $temp['filename']   = $file['image']['name'];
        $temp['filesize']   = $file['image']['size'];
        $temp['fileext']    = $file['image']['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['image'];
        $temp['catid']      = $inid;
        cms_attachment($temp); // @todo: 写入附件
        }
        $this->cateModel->where(['catid' => $inid])->save([
            'listorder' => $inid
        ]);
        if ($data['type'] == 2) {
        $page               = [];
        $page['catid']      = $inid;
        $page['pagetitle']  = $setting['seo_title'];
        $page['keywords']   = $setting['seo_keywords'];
        $page['description']= $setting['seo_description'];
        $page['template']   = $setting['pagetpl'];
        $page['updatetime'] = time();
        $this->pageModel->add($page);
        }
        $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $parentid = I('get.parentid', 0, 'intval');
        $cattype  = I('get.cattype' , 0, 'intval');
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['type']      =  0;
        $where['display']   =  1;
        
        $category = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        $options  = "<option value='\$catid' \$selected>";
        $options .= "\$spacer\$catname</option>";
        
        $cattree  = cms_tree_menu(
            $category, 0, $options, '',
            [$parentid], ['field' => $this->treeField]
        );
        $where    = ['catid' => $parentid];
        $category = $this->cateModel->where($where)->find();
        
        $data = [];
        $data['display']    =  1;
        $data['module']     = $category['module'];
        $data['modelid']    = $category['modelid'];
        $data['theme']      = unserialize($category['setting'])['theme'];
        
        $where              = [];
        $where['ismodel']   =  1;
        $where['disabled']  =  0;
        $modules  = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        foreach ($modules as $key => $row) {
            $module[$key] = $row;
        }
        $this->assign('parentid',   $parentid);
        
        $this->assign('data',       $data);
        $this->assign('skin',       $this->siteTheme);
        
        $this->assign('cattree',    $cattree);
        $this->assign('cattype',    $cattype);
        
        $this->assign( 'module',    $module );
        $this->display('action'.    $cattype);
    }
    
    /**
     * 修改栏目
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', []);
        $style   = I('post.style',   []);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        if ($data['catname'] && empty($data['description'])) {
            $data['description'] = $data['catname'];
        }
        if ($data['catname'] && empty($data['name'])) {
            $data['name'] = $data['catname'];
        }
        $data['catdir'] = $data['catdir'] ? : '';
        
        $dirs = $this->pinyin->output($data['name']);
        $data['catdir']     = $data['catdir'] ? : $dirs;
        
        $data['style']      = serialize($style);
        $data['setting']    = serialize($setting);
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['type']      = $data['type'];
        $where['catdir']    = $data['catdir'];
        $cates = $this->cateModel->where($where)->find();
        if ($cates && $info['catdir'] != $data['catdir']) {
            $this->error('目录已经存在！');
        }
        if (empty($_FILES['image']['error']) &&
            isset($_FILES['image'])) {
        $this->upload->saveName = $data['type'].'-';
        $this->upload->saveName.= time();
        
        $file = $this->upload->upload();
        if (!$file) $this->error('图片上传失败！');
        
        $data['image']  = $this->upload->rootPath;
        $data['image'] .= $file['image']['savepath'];
        $data['image'] .= $file['image']['savename'];
        }
        if ($data['linkage']) unset($data['catdir']);
        
        if ($file) {
        $temp = [];
        $temp['filename']   = $file['image']['name'];
        $temp['filesize']   = $file['image']['size'];
        $temp['fileext']    = $file['image']['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['image'];
        $temp['catid']      = $info['catid'];
        cms_attachment($temp); // @todo: 写入附件
        }
        if ($info['catname'] == $data['catname']) {
            $name = $info['catname'];
        } else {
            $name = $info['catname'].'→'.$data['catname'];
        }
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->save($data);
        if ($data['type'] == 2) {
        $page = [];
        $page['pagetitle']  = $setting['seo_title'];
        $page['keywords']   = $setting['seo_keywords'];
        $page['description']= $setting['seo_description'];
        $page['template']   = $setting['pagetpl'];
        $page['updatetime'] = time();
        $this->pageModel->where($where)->save($page);
        }
        $tips = '修改栏目『'.$name.'』导航';
        if (!$cates) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $catid = I('get.catid', 0, 'intval');
        $where = ['catid' => $catid];
        $data  = $this->cateModel->where($where)->find();
        
        $parentid = $data['parentid'];
        $cattype  = $data['type'];
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['type']      =  0;
//         $where['display']   =  1;
        
        $category = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        $options  = "<option value='\$catid' \$selected>";
        $options .= "\$spacer\$catname</option>";
        
        $cattree  = cms_tree_menu(
            $category, 0, $options, '',
            [$parentid], ['field' => $this->treeField]
        );
        $where              = [];
        $where['ismodel']   =  1;
        $where['disabled']  =  0;
        $modules  = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        foreach ($modules as $key => $row) {
            $module[$key] = $row;
        }
        $data['style'] = unserialize($data['style']);
        $data['setting'] = unserialize($data['setting']);
        $data['theme'] = $data['setting']['theme'];
        
        $this->assign('parentid',   $parentid);
        
        $this->assign('data',       $data);
        $this->assign('skin',       $this->siteTheme);
        
        $this->assign('setting',    $data['setting']);
        $this->assign('style',      $data['style']);
        
        $this->assign('cattree',    $cattree);
        $this->assign('cattype',    $cattype);
        
        $this->assign( 'module',    $module );
        $this->display('action'.    $cattype);
    }
    
    /**
     * 删除栏目
     */
    public final function delete() {
        $catid = I('get.catid', 0, 'intval');
        $info  = I('post.info', []);
        $info  = $catid ? [$catid] : $info['catid'];
        $data  = [];
        if (empty($info)) $this->error('请选择栏目导航！');
        
        foreach ($info as $key => $catid) {
        $catids = $this->cateModel->childid($catid);
        $data['catid'][] = $catids;
        }
        $catid = implode(',', $data['catid']);
        $array = explode(',', $catid);
        $array = array_unique($array);
        
        foreach ($array as $key => $catid) {
        $where = ['catid' => $catid];
        $cates = $this->cateModel->where($where)->find();
        $image = $cates['image'];
        if (file_exists($image)) @unlink($image);
        // @todo: 删除内容
        }
        $where = ['catid' => ['IN', $array]];
        $cates = $this->cateModel->where($where)->delete();
        $pages = $this->pageModel->where($where)->delete();
        $param = ['catid' => $array];
        
        if (!$cates) $this->error('删除栏目导航失败！', $param);
        else $this->success('删除栏目导航成功！', $param);
    }
    
    /**
     * 栏目状态
     */
    public final function state() {
        $catid = I('get.catid', 0, 'intval');
        $info  = [];
        $info['catid'] = $catid;
        
        $where = ['catid' => $info['catid']];
        $ress2 = $this->cateModel->where($where)->find();
        $value = $ress2['display'] ? 0 : 1;
        
        $ress2 = $this->cateModel->where($where)->save([
            'display' => $value
        ]);
        $data2 = $this->cateModel->where($where)->find();
        $oper2 = $data2['display'] ? '启用' : '禁用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'栏目『'.$data2['catname'].'』导航'.$state;
        cms_writelog($tips2, ['catid' => $info['catid']]);
        
        echo json_encode($data2['display']);
    }
    
    /**
     * 移动栏目
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', []);
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $cate = $this->cateModel->where($where)->find();
        
        $name = $cate['name'] ? : '顶级栏目';
        $tips = '移动栏目『'.$info['catname'].'』到『'.$name.'』操作';
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $cate = $this->cateModel->where($where)->save($data);
        $data['catid']      = $info['catid'];
        
        if (!$cate) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data);
        } // @todo: 
        
        $catid = I('get.catid', 0, 'intval');
        $where              = [];
        $where['catid']     = $catid;
        $cates = $this->cateModel->where($where)->find();
        
        $where              = [];
        $where['catid']     = ['NEQ', $catid];
        $where['type']      = 0;
        
        $trees = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        $options = "<option value='\$catid' \$selected>";
        $options.= "\$spacer\$catname</option>";
        
        $cattree = cms_tree_menu(
            $trees, 0,
            $options, '', [], ['field' => $this->treeField]
        );
        
        $this->assign('data', $cates);
        $this->assign('cattree', $cattree);
        $this->display();
    }
    
    /**
     * 删除图片
     */
    public final function image() {
        $catid = I('get.catid', 0, 'intval');
        $where = ['catid' => $catid];
        $cates = $this->cateModel->where($where)->find();
        
        $image = $cates['image'];
        if (file_exists($image)) @unlink($image);
        
        $cates = $this->cateModel->where($where)->save([
            'image' => null
        ]);
        if (!$cates) $this->error('删除栏目图片失败！', $image);
        else $this->success('删除栏目图片成功！', $image);
    }
    
    /**
     * 获取模型
     */
    public final function model() {
        $module = I('get.module', '', 'cms_addslashes');
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['module']    = $module;
        $where['disabled']  =  0;
        $model = $this->modeModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->field(
            'modelid,name,module,table,listorder'
        )->select();
        
        echo json_encode($model);
    }
    
    /**
     * 获取模型（配置信息）
     */
    public final function config() {
        $modelid = I('get.modelid', 0, 'intval');
        
        $where = ['modelid' => $modelid];
        $model = $this->modeModel->where($where)->find();
        $array = unserialize($model['setting']);
        
        echo json_encode($array);
    }
    
    /**
     * 栏目信息（输出）
     */
    public final function infos() {
        $catid = I('get.catid', 0, 'intval');
        $where = ['catid' => $catid];
        $cates = $this->cateModel->where($where)->find();
        
        echo json_encode($cates);
    }
    
    /**
     * 输出栏目
     */
    public final function output() {
        $module  = I('get.module', '', 'cms_addslashes');
        $modelid = I('get.modelid', 0, 'intval');
        $catid   = I('get.catid',   0, 'intval');
        
        $where              = [];
        $where['display']   =  1;
        $where['module']    = $module ;
        $where['modelid']   = $modelid;
        
        $array = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        $options = "<option value='\$catid' \$selected>";
        $options.= "\$spacer\$catname</option>";
        
        $cattree = cms_tree_menu(
            $array, 0, $options,
            null, [$catid], ['field' => $this->treeField]
        );
        echo json_encode($cattree);
    }
    
}
