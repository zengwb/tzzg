<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块导航菜单控制器类：呈现菜单CURD常规管理
 * 
 * @author T-01
 */
final class NavigationController extends BaseController {
    
    public      $action     = [
        'index', 'lists', 'listorder', 'add', 'edit',
        'delete', 'state', 'move',
    ];
    
    private     $siteModel  =   [],
                $menuModel  =   [],
                $tree       = null,
                $field      = ['navigationid', 'name', 'parentid'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->siteModel = D('Admin/Website');
        $this->menuModel = D('Admin/Navigation');
    }
    
    /**
     * 菜单管理
     */
    public final function index() {
        $keyword    = I('request.key', null, 'cms_addslashes');
        $keyword    = I('get.key', $keyword, 'cms_addslashes');
        $keyword    = I('post.key',$keyword, 'cms_addslashes');
        $keyword    = trim($keyword);
        
        $where      = [];
        $order      = '`listorder` ASC, `navigationid` ASC';
        if (!empty($keyword)) {
            $where['name']      = ['LIKE', '%'.$keyword.'%'];
        } else {
            $where['parentid']  = 0;
        }
        $where['siteid'] = ['IN', [0, cms_siteid()]];
        
        $nums = $this->menuModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->menuModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if ($keyword) {
            $row['title'] = strtr($row['name'], $keys);
        } else {
            $row['title'] = $row['name'];
        }
        $data[$key]       = $row;
        
        if ($row['linkurl']) {
        $data[$key]['_add'] = false;
        $data[$key]['_link']= 'void(0);';
        } else {
        $link = U('lists', ['parentid' => $row['navigationid']]);
        $data[$key]['_add'] = true;
        $data[$key]['_link']= 'yhcms.common.linkurl(\''.$link.'\');';
        }
        $childid = $this->menuModel->childid($row['navigationid']);
        $childid = strtr($childid, [$row['navigationid'] => null]);
        
        if ($childid) $data[$key]['_child'] = true;
        else $data[$key]['_child'] = false;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 子项管理
     */
    public final function lists() {
        $parentid = I('get.parentid', 0, 'intval');
        
        $data = $temp = [];
        $info = ['parentid' => $parentid];
        
        $where = [];
        if (!empty($info['parentid'])) {
            $navigationid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $navigationid];
        }
        $where['siteid'] = ['IN', [0, cms_siteid()]];
        $ress = $this->menuModel->treeList($where, '*');
        
        foreach ($ress as $key => $row) {
        $row['_link'] = cms_stripslashes($row['linkurl']);
        $row['_css2'] = $row['display'] ? '' : 'display';
        
        if ($row['display']) {
        $row['_state'] = '<a class="list-operation"
        data-state="'.$row['navigationid'].'"
        href="javascript:void(0);" title="点击禁用导航">
        <i class="iconfont icon-qiyong"></i></a>';
        } else {
        $row['_state'] = '<a class="list-operation"
        data-state="'.$row['navigationid'].'"
        href="javascript:void(0);" title="点击启用导航">
        <i class="iconfont icon-qingchu"></i></a>';
        }
        $childid = $this->menuModel->childid($row['navigationid']);
        $childid = strtr($childid, [$row['navigationid'] => null]);
        
        $_u2 = U('lists', ['parentid' => $row['navigationid']]);
        $_r1 = $row['linkurl'] ?
        'void(0);' : 'yhcms.common.linkurl(\''.$_u2.'\');';
        $_r2 = U('edit', ['navigationid' => $row['navigationid']]);
        
        $row['_name'] = $childid ?
        '<a href="javascript:void(0);"
        onClick="javascript:'.$_r1.'" title="管理子项">'.
        $row['name'].'</a>' :
        '<a href="javascript:void(0);"
        onClick="javascript:yhcms.dialog.topwin(\''.$_r2.'\',
        \'修改【'.$row['name'].'】导航\',
        \'AdminNavigationEdit-0-548-220\');">'.$row['name'].'</a>';
        
        $row['_oper']  = $childid ?
        '<a href="javascript:void(0);"
        onClick="javascript:'.$_r1.'" title="管理导航">管理</a> ' :
        '<a href="javascript:void(0);"
        class="cms-cccc">管理</a> ';
        
        $url = U('add', ['parentid' => $row['navigationid']]);
        $row['_oper'] .= empty($row['linkurl']) &&
        empty($row['action']) ?
        '<a href="javascript:void(0);"
        onClick="javascript:yhcms.dialog.topwin(
        \''.$url.'\', \'添加【'.$row['name'].'】子项\',
        \'AdminNavigationAdd-0-548-220\');"
        title="添加子项">添加</a> ' :
        '<a href="javascript:void(0);" class="cms-cccc">添加</a> ';
        
        $url = U('edit', ['navigationid' => $row['navigationid']]);
        $row['_oper'] .= '<a href="javascript:void(0);"
        onClick="javascript:yhcms.dialog.topwin(\''.$url.'\',
        \'修改【'.$row['name'].'】导航\',
        \'AdminNavigationEdit-0-548-220\');" title="修改导航">修改</a> ';
        
        $url = U('move', ['navigationid' => $row['navigationid']]);
        $row['_oper'] .= '<a href="javascript:void(0);"
        onClick="javascript:yhcms.dialog.topwin(\''.$url.'\',
        \'移动【'.$row['name'].'】导航\',
        \'AdminNavigationMove-0-540-132\');" title="移动导航">移动</a> ';
        
        $url = U('delete', ['navigationid' => $row['navigationid']]);
        $row['_oper'] .= '<a href="javascript:void(0);"
        onClick="javascript:yhcms.dialog.tips(\''.$url.'\',
        \'确认删除【'.$row['name'].'】导航菜单！\');"
        title="删除导航">删除</a> ';
        
        $temp[$row['navigationid']] = $row;
        }
        $text = "<tr class='\$_css2'>
        <th class='list-checkbox'>
        <input class='checkchild' name='info[navigationid][]'
        value='\$navigationid' type='checkbox' />
        </th>
        <td class='list-small'>\$navigationid</td>
        <td class='list-listorder'>
        <input type='hidden' name='data[navigationid][]'
        value='\$navigationid' />
        <input type='text' name='data[listorder][]'
        value='\$listorder' maxlength='4' autocomplete='off'
        class='form-control input-sm list-input-listorder' />
        </td>
        <td data-navigationid='\$navigationid'>
        \$spacer\$_name
        <span class='cms-cccc'>　\$description</span>
        </td>
        <td class='cms-c999'>\$_link</td>
        <td class='cms-tc icon-color'>\$_state</td>
        <td class='cms-tc'>\$_oper</td>
        </tr>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],$text,'',[],['field'=>$this->field]
        );
        $list = $this->menuModel->catalog($info['parentid'])['name'];
        
        $this->assign('info', $info);
        $this->assign('data', $temp);
        $this->assign('tree', $tree);
        $this->assign('list', $list);
        
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['navigationid'] as $i => $navigationid) {
        $where = ['navigationid' => $navigationid];
        $ress2 = $this->menuModel->where($where)->find();
        
        $listorder = $ress2['listorder'];
        if ($listorder == $data['listorder'][$i]) {
            continue;
        }
        $ress2 = $this->menuModel->where($where)->save([
            'listorder'=> $data['listorder'][$i]
        ]);
        $listorder = $navigationid.'='.$data['listorder'][$i];
        $list['listorder'][] = $listorder;
        }
        $listorder = $list ? : 'null';
        self::success('导航排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加菜单
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $style= I('post.style',[]);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        if ($data['name'] && empty($data['description'])) {
            $data['description'] = $data['name'];
        }
        $data['style']      = serialize($style);
        $data['siteid']     = cms_siteid();
        
        $tips = '导航菜单『'.$data['name'].'』同名，当前操作';
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($ress) $this->popup(false, $tips, $data);
        
        $inid = $this->menuModel->add($data);
        
        $tips = '添加导航『'.$data['name'].'』菜单';
        if (!$inid) $this->popup(false, $tips, $data);
        
        $this->menuModel->where(['navigationid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->popup(true, $tips, $data, null, 3);
        } // @todo: 
        
        $parentid = I('get.parentid', 0, 'intval');
        
        $info               = [];
        $info['parentid']   = $parentid;
        $info['navigationid'] = $info['parentid'];
        
        $data               = [];
        $data['parentid']   = $info['parentid'];
        
        $where              = [];
        $where['parentid']  = $info['parentid'];
        $where['linkurl']   = '';
        $where['display']   =  1;
        
        if (!empty($info['parentid'])) {
            $navigationid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $navigationid];
        }
        $temp = $this->menuModel->treeList($where);
        
        $text = "<option value='\$navigationid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [], ['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display('action');
    }
    
    /**
     * 修改菜单
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $style= I('post.style',[]);
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['name'] = trim($data['name']);
        
        if ($data['name'] && empty($data['description'])) {
            $data['description'] = $data['name'];
        }
        $data['style']      = serialize($style);
        
        $tips = '导航菜单『'.$data['name'].'』同名，当前操作';
        
        $where              = [];
        $where['parentid']  = $data['parentid'];
        $where['name']      = $data['name'];
        
        $ress = $this->menuModel->where($where)->find();
        if ($info['name']  != $data['name'] &&
            $ress) $this->popup(false, $tips, $data);
        
        if ($info['name']  != $data['name']) {
            $name = $info['name'].'→'.$data['name'];
        } else {
            $name = $data['name'];
        }
        $where                  = [];
        $where['navigationid']  = $info['navigationid'];
        $ress = $this->menuModel->where($where)->save($data);
        
        $tips = '更新导航『'.$name.'』菜单';
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, '', 3);
        } // @todo:
        
        $navigationid = I('get.navigationid', 0, 'intval');
        $info                   = [];
        $info['navigationid']   = $navigationid;
        
        $where                  = [];
        $where['navigationid']  = $info['navigationid'];
        
        $navi = $this->menuModel->catalog($info['navigationid'])['navigationid'];
        $data = [];
        $data = $this->menuModel->where($where)->find();
        $data = cms_stripslashes($data);
        
        $temp                   = [];
        $temp['navigationid']   = $info['navigationid'];
        
        $info['parentid']       = $data['parentid'] ? $navi[0] : 0;
        $info['navigationid']   = $info['parentid'];
        $info['name']           = $data['name'];
        
        $where                  = [];
        $where['parentid']      = $info['parentid'];
        
        $where['navigationid']  = ['NEQ', $info['navigationid']];
        $where['_logic']        = 'AND';
        $where['navigationid']  = ['NEQ', $temp['navigationid']];
        
        $where['linkurl']       = '';
        $where['display']       =  1;
        
        if (!empty($info['parentid'])) {
            $navigationid = $this->menuModel->childid($info['parentid']);
            $where['parentid']  = ['IN', $navigationid];
        }
        $ress = $this->menuModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['navigationid']] = $row;
        }
        $text = "<option value='\$navigationid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text,'',[$data['parentid']],['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        $this->assign('style', unserialize($data['style']));
        
        $this->display('action');
    }
    
    /**
     * 删除菜单
     */
    public final function delete() {
        $navigationid = I('get.navigationid', 0, 'intval');
        
        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';
        
        $info = $navigationid ? [$navigationid] : $info['navigationid'];
        if (empty($info)) $this->error('请选择导航！');
        
        foreach ($info as $key => $navigationid) {
            $temp .= $this->menuModel->childid($navigationid).',';
        }
        $data['navigationid'] = explode(',', $temp);
        
        $temp = ['navigationid' => ['IN', $temp]];
        $ress = $this->menuModel->where($temp)->delete();
        
        if (!$ress) $this->error('删除导航失败！', $data);
        else $this->success('删除导航成功！', $data);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $navigationid = I('get.navigationid', 0, 'intval');
        
        $where = ['navigationid' => $navigationid];
        $data2 = $this->menuModel->where($where)->find();
        
        $state = $data2['display'];
        $value = $state ? 0 : 1;
        
        $ress2 = $this->menuModel->where($where)->save([
            'display' => $value
        ]);
        $data2 = $this->menuModel->where($where)->find();
        $oper2 = $data2['display'] ? '启用' : '禁用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        
        $tips2 = $oper2.'导航『'.$data2['name'].'』菜单';
        cms_writelog($tips2.$state, ['navigationid' => $navigationid]);
        
        echo json_encode($data2['display']);
    }
    
    /**
     * 移动菜单
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where                  = [];
        $where['navigationid']  = $data['parentid'];
        $name = $this->menuModel->where($where)->find()['name'];
        $name = $name ? : '顶级菜单';
        
        $where                  = [];
        $where['navigationid']  = $info['navigationid'];
        $temp = $this->menuModel->where($where)->find();
        
        $tips = '移动导航『'.$temp['name'].'』到『'.$name.'』菜单';
        
        $where                  = [];
        $where['navigationid']  = $info['navigationid'];
        $ress = $this->menuModel->where($where)->save($data);
        
        $data                   = [];
        $data['parentid']       = $temp['parentid'].'='.$ress['parentid'];
        $data['navigationid']   = $info['navigationid'];
        
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data, null, 3);
        } // @todo: 
        
        $navigationid = I('get.navigationid', 0, 'intval');
        $info                   = [];
        $info['navigationid']   = $navigationid;
        
        $where                  = [];
        $where['navigationid']  = $info['navigationid'];
        
        $navi = $this->menuModel->catalog($info['navigationid'])['navigationid'];
        $data = [];
        $data = $this->menuModel->where($where)->find();
        
        $data = cms_stripslashes($data);
        $info['parentid']       = $data['parentid'] ? $navi[0] : 0;
        
        $where                  = [];
        $where['parentid']      = $info['parentid'];
        $where['navigationid']  = ['NEQ', $info['navigationid']];
        $where['linkurl']       = '';
        $where['display']       =  1;
        
        if (!empty($info['parentid'])) {
            $navigationid = $this->menuModel->childid($info['parentid']);
            $where['parentid'] = ['IN', $navigationid];
        }
        $ress = $this->menuModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['navigationid']] = $row;
        }
        $text = "<option value='\$navigationid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp,
            $info['parentid'],
            $text, '', [$data['parentid']], ['field' => $this->field]
        );
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
}
