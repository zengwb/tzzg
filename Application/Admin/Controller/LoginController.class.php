<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块登录控制器类：呈现登录、验证或重置密码
 * 
 * @author T-01
 */
final class LoginController extends PageController {
    
    public      $action     = ['index', 'check', 'verify'];
    
    private     $roleModel  = [],
                $userModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\PageController::_initialize()
     */
    public final function _initialize() {
//         parent::_initialize();
        $safety = cms_config_array('Admin', 'safety');
        $domain = $_SERVER['SERVER_NAME'];
        $prompt = '域名地址无效，没有管理权限！';
        if ($domain != $safety['admin_url'] && !empty($safety['admin_url']))
        $this->error($prompt, '', 'javascript:void(0);', 9999);
        
        $this->roleModel    = D('AdminRole');
        $this->userModel    = D('Admin');
    }
    
    /**
     * 登录界面
     */
    public final function index() {
        $data = ['username' => 'admin', 'password' => 'admin888'];
        $data = [];
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 登录验证
     */
    public final function check() {
        if (!IS_POST && !I('post.dosubmit')) $this->redirect('index');
        
        $data = I('post.data', [], 'cms_addslashes');
        $code = trim($data['verify']);
        
        $data['username'] = trim($data['username']);
        $data['password'] = trim($data['password']);
        
        if (!cms_verify_check($code)) {
            $this->error('验证码错误！', $data, '', 3, true, 1);
        }
        $user = $this->userModel->oauth2($data);
        if (!$user) {
            $this->error('管理帐号或密码错误！', $data, '', 3, true, 1);
        }
        $where = [];
        $where['roleid'] = $user['roleid'];
        
        $role = $this->roleModel->where($where)->find();
        $tips = '当前用户组『{rolename}』已被锁定！';
        
        $regx = ['{rolename}' => $role['name']];
        $tips = strtr($tips, $regx);
        
        if ($role['disabled']) {
            $this->error($tips, $data, '', 3, true, 1);
        }
        $tips = '当前用户『{username}』禁止登录！';
        $regx = ['{username}' => $user['username']];
        $tips = strtr($tips, $regx);
        
        if ($user['disabled']) {
            $this->error($tips, $data, '', 3, true, 1);
        }
        $siteid = $role['siteids'];
        $siteid = explode(',', $siteid)[0];
        if (in_array($user['roleid'], C('SUPER_ROLEID')))
        $siteid = cms_siteid();
        
        session('CMS_UserID',   $user['userid']);
        session('CMS_RoleID',   $user['roleid']);
        session('CMS_UserName', $user['username']);
        session('CMS_SiteID',   $siteid);
        
        $this->userModel->uplogin(session('CMS_UserName'));
        $this->success(
            ucwords(session('CMS_UserName')).'，登录成功！',
            ['status' => 1],U('index/index'), 3, true, 1
        );
    }
    
    /**
     * 获取并打印登录验证码字符串（引用生成图形验证码函数）
     */
    public final function verify() {
        echo cms_verify_code(4, [249, 249, 249], 24); // @todo: 
    }
    
}
