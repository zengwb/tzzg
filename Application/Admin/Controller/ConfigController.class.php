<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块基础配置控制器类：呈现配置CURD常规管理
 * 
 * @author T-01
 */
final class ConfigController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state',
        'update', 'setting', 'check', 'ajax', 'group', 'site', 'type',
    ];
    
    private     $roleModel  = [],
                $appsModel  = [],
                
                $confModel  = [],
                $typeModel  = [],
                
                $siteModel  = [],
                $formModel  = [],
                $form       = null;
    
    private     $type       = ['text', 'textarea', 'box', 'number'];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->form = (new \Common\Widget\FormsWidget());
        
        $this->appsModel    = D('Module');
        $this->confModel    = D('Config');
        $this->roleModel    = D('AdminRole');
        
        $this->formModel    = D('Forms');
        $this->typeModel    = D('ConfigGroup');
        
        $this->siteModel    = D('Website');
    }
    
    /**
     * 配置管理
     */
    public final function index() {
        $keyword    = I('request.key', null, 'cms_addslashes');
        $keyword    = I('get.key', $keyword, 'cms_addslashes');
        $keyword    = I('post.key',$keyword, 'cms_addslashes');
        $keyword    = trim($keyword);
        
        $where      = [];
        $order      = '`listorder` ASC, `configid` ASC';
        if (!empty($keyword)) {
            $where['name']      = ['LIKE', '%'.$keyword.'%'];
        }
        $where['siteid'] = ['IN', [0, cms_siteid()]];
        if (in_array($this->roleid, C('SUPER_ROLEID'))) {
            unset($where['siteid']);
        }
        $nums = $this->confModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->confModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if ($keyword) {
            $row['title'] = strtr($row['name'], $keys);
        } else {
            $row['title'] =  $row['name'];
        }
        $where = ['group' => $row['group']];
        $group = $this->typeModel->where($where)->find();
        $row['groupname'] =  $group['name'];
        
        $where = ['siteid'=> $row['siteid']];
        $site2 = $this->siteModel->where($where)->find();
        $row['sitename']  =  $site2['name'] ? : '全部站点';
        
        $row['configtype']= $row['configtype'] ?: 'NULL';
        $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['configid'] as $key => $configid) {
        $listorder = $this->confModel->where([
            'configid' => $configid
        ])->find();
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->confModel->where([
            'configid'  => $configid]
        )->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $configid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('模块配置排序成功！', $listorder, '', 3);
    }
    
    /**
     * 添加配置
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['siteid'] = cms_siteid();
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where = [];
        $where['module'] = $data['module'];
        $ress2 = $this->appsModel->where($where)->find();
        $module= $ress2['name'];
        
        $where['group']  = $data['group'];
        $ress2 = $this->typeModel->where($where)->find();
        $group = $ress2['name'];
        
        $where['params'] = $data['params'];
        $ress2 = $this->confModel->where($where)->find();
        
        $tips2 = '添加系统『'.$module.'』模块『'.$group.'』';
        $tips2.= '组『'.$data['name'].'』配置';
        if ($ress2) $this->popup(false, $tips2);
        
        $inid2 = $this->confModel->add($data);
        if (!$inid2) $this->popup(false, $tips2, $data);
        
        $this->confModel->where(['configid' => $inid2])->save([
            'listorder' => $inid2
        ]);
        $this->popup(true, $tips2, $data);
        } // @todo: 
        
        $where = ['disabled' => 0];
        $apps2 = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('module', $apps2);
        $this->display('action');
    }
    
    /**
     * 修改配置
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $where              = [];
        $where['module']    = $data['module'];
        $ress2 = $this->appsModel->where($where)->find();
        $module= $ress2['name'];
        
        $where              = [];
        $where['group']     = $data['group'];
        $ress2 = $this->typeModel->where($where)->find();
        $group = $ress2['name'];
        
        $where              = [];
        $where['configid']  = $info['configid'];
        $ress2 = $this->confModel->where($where)->save($data);
        
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
//         $tips2 = '更新系统『'.$module.'』模块『'.$group.'』';
//         $tips2.= '组『'.$name.'』配置';
        $tips2 = '更新模块『'.$name.'』配置';
        
        if (!$ress2) $this->popup(false, $tips2, $data);
        else $this->popup(true, $tips2, $data);
        } // @todo: 
        
        $configid = I('get.configid', 0, 'intval');
        
        $where = ['configid' => $configid];
        $data2 = $this->confModel->where($where)->find();
        
        $where = ['disabled' => 0];
        $apps2 = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('data', $data2);
        $this->assign('module', $apps2);
        
        $this->display('action');
    }
    
    /**
     * 删除配置
     */
    public final function delete() {
        $configid = I('get.configid', 0, 'intval');
        $info = I('post.info', []);
        $info = $configid ? [$configid] : $info['configid'];
        $data = [];
        if (empty($info)) $this->error('请选择模块配置！');
        
        foreach ($info as $key => $configid) {
            $data['configid'][]=  $configid;
        }
        $configid = implode(',', $data['configid']);
        
        $where = ['configid' => ['IN', $configid]];
        $ress2 = $this->confModel->where($where)->delete();
        $temp2 = 'configid: '.$configid;
        
        if (!$ress2) $this->error('删除模块配置失败！', $temp2);
        else $this->success('删除模块配置成功！', $temp2);
    }
    
    /**
     * 更新状态
     */
    public final function state() {
        $configid = I('get.configid', 0, 'intval');
        $info = [];
        $info['configid'] = $configid;
        
        $where = ['configid' => $info['configid']];
        $ress2 = $this->confModel->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where = ['configid' => $info['configid']];
        $ress2 = $this->confModel->where($where)->save([
            'disabled'=>$value
        ]);
        $where = ['configid' => $info['configid']];
        $data2 = $this->confModel->where($where)->find();
        $oper2 = $data2['disabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'模块『'.$data2['name'].'』配置'.$state;
        cms_writelog($tips2, ['configid' => $info['configid']]);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 更新配置
     */
    public final function update() {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        foreach ($data as $param => $value) {
        $where              = [];
        $where['module']    = $info['module'];
        $where['group']     = $info['group'];
        $where['params']    = $param;
        
        if (is_array($value)) $value = implode(',', $value);
        $this->confModel->where($where)->save([
            'value' => $value
        ]);
        }
        $this->success('更新配置成功！');
    }
    
    /**
     * 配置设置
     */
    public final function setting() {
        $module = I('get.module', MODULE_NAME);
        $group  = I('get.group', '');
        $where  = ['group' => $group, 'disabled' => 0];
        
        $typeress = $this->typeModel->where($where)->find();
        $typename = $typeress['name'];
        
        if (!$typeress) {
        unset($where['disabled']);
        $type = $this->typeModel->where($where)->find();
        $this->error(
            '该模块『'.$type.'』模块组，已被管理员禁用！',
            '', 'javascript:void(0);', 3600
        );
        }
        $where  = [];
        $where['module']    = ucwords($module);
        $where['disabled']  = 0;
        $typeress = $this->typeModel->where($where)->order(
            '`listorder` ASC, `groupid` ASC'
        )->select();
        
        $where  = [];
        $where['module']    = ucwords($module);
        $where['group']     = $group;
        $where['configtype']= ['neq', ''];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['disabled']  = 0;
        $ress = $this->confModel->where($where)->order(
            '`listorder` ASC, `configid` ASC'
        )->select();
        
        $list = [];
        foreach ($ress as $key => $row) {
        if (method_exists($this->form, $row['configtype'])) {
        $params             = [];
        $params['field']    = $row['params'];
        $params['value']    = $row['value'];
        $params['description'] = $row['description'];
        $params['name']     = $row['name'];
        $params['setting']  = $row['setting'];
        
        $method = [$this->form, $row['configtype']];
        $row['element'] = call_user_func($method, $params);
        } else {
        $row['element'] = null;
        }
        $list[$key] = $row;
        }
        $this->assign('list',   $list);
        $this->assign('module', $module);
        $this->assign('group',  $group);
        
        $this->assign('type', $typeress);
        $this->assign('name', $typename);
        $this->display();
    }
    
    /**
     * 检查参数
     */
    public final function check() {
        $module = I('get.module', '');
        $group  = I('get.group' , '');
        $params = I('get.params', '');
        
        $where  = [];
        $where['module']    = $module;
        $where['group']     = $group;
        $where['params']    = $params;
        
        $check  = $this->confModel->where($where)->count();
        echo json_encode($check);
    }
    
    /**
     * Ajax（配置元素）
     */
    public final function ajax() {
        $configid   = I('get.configid', 0, 'intval');
        $configtype = I('get.configtype' , '');
        
        $where = [];
        $where['configid']  = $configid;
        $conf2 = $this->confModel->where($where)->find();
        
        $where = [];
        $where['configtype']= $configtype;
        $form2 = $this->formModel->where($where)->find();
        
        $setting = $conf2['setting'] ?: $form2['setting'];
        $setting = unserialize($setting);
        
        $setting['default'] = $conf2['value'] ?: $setting['default'];
        
        $this->assign('setting', $setting);
        W('Common/Forms/element', ['label' => $configtype]);
    }
    
    /**
     * 获取分组
     */
    public final function group() {
        $module = I('get.module', '');
        
        $where = ['module' => $module];
        $group = $this->typeModel->where($where)->order(
            '`listorder` ASC, `groupid` ASC'
        )->select();
        
        echo json_encode($group);
    }
    
    /**
     * 设置站点
     */
    public final function site() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['module']    = $info['module'];
        $ress2 = $this->appsModel->where($where)->find();
        $module= $ress2['name'];
        
        $where              = [];
        $where['group']     = $info['group'];
        $ress2 = $this->typeModel->where($where)->find();
        $group = $ress2['name'];
        
        $where              = [];
        $where['configid']  = $info['configid'];
        $ress2 = $this->confModel->where($where)->save($data);
        
        $tips2 = '系统『'.$module.'』模块『'.$group.'』';
        $tips2.= '组『'.$info['params'].'』配置，设置站点';
        
        $temp2              = [];
        $temp2['configid']  = $info['configid'];
        $temp2['siteid']    = $data['siteid'];
        
        if (!$ress2) $this->popup(false, $tips2, $temp2);
        else $this->popup(true, $tips2, $temp2);
        } // @todo: 
        
        $configid = I('get.configid', 0, 'intval');
        
        $where = [];
        $where = ['configid' => $configid];
        $data2 = $this->confModel->where($where)->find();
        
        $where              = [];
        $where['roleid']    = $this->roleid;
        $sites = $this->roleModel->where($where)->find()['siteids'];
        
        $where = [];
        $where['siteid'] = ['IN', $sites];
        $where['display']= 1;
        if (!$sites || in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $site2 = $this->siteModel->where($where)->order(
            '`listorder` ASC'
        )->select();
        
        $this->assign('site', $site2);
        $this->assign('data', $data2);
        $this->display();
    }
    
    /**
     * 配置类型
     */
    public final function type() {
        if (IS_POST && I('post.dosubmit')) {
        $setting = I('post.setting', [], 'cms_addslashes');
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['setting'] = serialize($setting);
        
        $where              = [];
        $where['module']    = $info['module'];
        $ress2 = $this->appsModel->where($where)->find();
        $module= $ress2['name'];
        
        $where              = [];
        $where['group']     = $info['group'];
        $ress2 = $this->typeModel->where($where)->find();
        $group = $ress2['name'];
        
        $where              = [];
        $where['configid']  = $info['configid'];
        $ress2 = $this->confModel->where($where)->save($data);
        
        $tips2 = '系统『'.$module.'』模块『'.$group.'』';
        $tips2.= '组『'.$info['params'].'』配置，设置类型';
        
        $temp2              = [];
        $temp2['configid']  = $info['configid'];
        $temp2['setting']   = $data['setting'];
        
        if (!$ress2) $this->popup(false, $tips2, $temp2);
        else $this->popup(true, $tips2, $temp2);
        } // @todo: 
        
        $configid = I('get.configid', 0, 'intval');
        
        $where = [];
        $where['disabled']  = 0;
        $where['elements']  = ['IN', $this->type];
        $type2 = $this->formModel->where($where)->order(
            '`listorder` ASC, `elementid` ASC'
        )->select();
        
        $where = [];
        $where['configid']  = $configid;
        $data2 = $this->confModel->where($where)->find();
        
        $this->assign('type', $type2);
        $this->assign('data', $data2);
        $this->display();
    }
    
}
