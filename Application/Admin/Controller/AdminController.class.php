<?php

namespace Admin\Controller;

use Think\Upload;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块系统用户控制器类：呈现系统用户CRUD常规管理
 * 
 * @author T-01
 */
final class AdminController extends BaseController {
    
    public      $action     = [
        'index', 'avater', 'add', 'edit', 'delete', 'state',
        'move', 'infos', 'password', 'menus', 'privs', 'cityinfo',
        'unitinfo','governmentinfo',
    ];
    
    private     $upload     = null,
                $menuField  = [],
                $roleField  = [],
                
                $userModel  = [],
                $roleModel  = [],
                
                $menuModel  = [],
                $privModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->menuModel    = D('Menu');
        $this->menuField    = ['menuid', 'name', 'parentid'];
        $this->roleField    = ['roleid', 'name', 'parentid'];
        
        $this->userModel    = D('Admin');
        $this->roleModel    = D('AdminRole');
        $this->privModel    = D('AdminRolePriv');
        
        $this->upload       = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Statics/Avatar/';
        
        $this->upload->subName  = null;
        $this->upload->ext      = ['png', 'jpg'];
    }
    
    /**
     * 用户管理
     */
    public final function index() {
        $roleid = I('get.roleid', 0, 'intval');
        $keyWord = I('post.key') ? I('post.key') : '';//搜索

        $info               = [];
        $info['roleid']     = $roleid;
        
        $where  = $info['roleid'] ? ['roleid' => $info['roleid']] : '';
        $where              = [];
        $where['roleid']    = $info['roleid'];
        if (!$roleid) $where = [];
        $where['username'] = ['like',"%$keyWord%"];//只搜索字段 username
        
        $data   = [];
        $ress   = $this->userModel->where($where)->select();
        foreach ($ress as $key => $row) {
        $where  = ['roleid'  => $row['roleid']];
        $role   = $this->roleModel->where($where)->find();
        $temp   = ['0.0.0.0' => null];
        
        $row['rolename']    = $role['name'];
        $row['loginip']     = strtr($row['loginip'], $temp);
        
        $user   = in_array($row['userid'], C('SUPER_USERID'));
        $role   = in_array($row['roleid'], C('SUPER_ROLEID'));
        
        $temp   = $row['userid'] == session('CMS_UserID');
        $row['showmove']    = $user || $role || $temp;
        
        $data[$key]         = $row; // 获取用户记录
        }
        
        $where              = [];
        $where['disabled']  =  0;
        
        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        
        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        foreach($data as $k => $v){
            $data[$k]['username2'] = str_replace($keyWord,'<font style=\'color: red\'>'.$keyWord.'</font>',$v['username']);
        }

        $this->assign('tree', $tree);
        $this->assign('data', $data);
        
        $this->display();
    }
    
    /**
     * 用户头像
     */
    public final function avater() {
        $username = I('get.username', '');
        
        $info = ['username' => $username];
        $temp = ['username' => $info['username']];
        
        $data = $this->userModel->where($temp)->find();
        if (file_exists($data['avater'])) {
           @unlink($data['avater']);
        }
        $data['avater'] = '';
        $ress = $this->userModel->where($temp)->save($data);
        
        if (!$ress) $this->error('删除头像失败！', $info);
        else $this->success('删除头像成功！', $info, '', 3);
    }
    
    /**
     * 添加用户
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $tips = '系统用户『'.$data['username'].'』已经存在！';
        
        $where              = [];
        $where['username']  = $data['username'];
        
        $user = $this->userModel->where($where)->find();
        if ($user) $this->error($tips);
        
        if (empty($_FILES['avater']['error']) &&
        isset($_FILES['avater'])) {
        $this->upload->saveName = $data['username'];
        
        $file = $this->upload->upload();
        if (!$file) $this->error('头像上传失败！');
        
        $data['avater']  = $this->upload->rootPath;
        $data['avater'] .= $file['avater']['savepath'];
        $data['avater'] .= $file['avater']['savename'];
        
        $temp = [];
        $temp['filename'] = $file['avater']['name'];
        $temp['filesize'] = $file['avater']['size'];
        $temp['fileext']  = $file['avater']['ext'];
        $temp['isimage']  = 1;
        $temp['filepath'] = $data['avater'];
        cms_attachment($temp); // @todo: 写入附件
        }
        $temp = C('OAUTH_CMSKEY');
        $data['passprot'] = $data['password'];
        $data['password'] = cms_password($data['passprot'], $temp);
        
        $tips = '添加系统『'.$data['username'].'』用户';
        $ress = $this->userModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $where              = [];
        $where['disabled']  = 0;
        
        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        
        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );
        $this->assign('tree', $tree);
        $this->assign('data', ['disabled' => 0]);
        
        $this->display('action');
    }
    
    /**
     * 编辑用户
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['username']  = $data['username'];
        
        $user = $this->userModel->where($where)->find();
        $tips = '系统用户『'.$data['username'].'』已经存在！';
        
        if ($info['username'] != $data['username'] &&
            $user) {
            $this->error($tips);
        }
        if ($info['username'] == $data['username']) {
            $name = $data['username'];
        } else {
            $name = $info['username'].'→'.$data['username'];
        }
        
        if (empty($_FILES['avater']['error']) &&
            isset($_FILES['avater'])) {
        $this->upload->saveName = $data['username'];
        
        $file = $this->upload->upload();
        if (!$file) $this->error('头像上传失败！');
        
        $data['avater']  = $this->upload->rootPath;
        $data['avater'] .= $file['avater']['savepath'];
        $data['avater'] .= $file['avater']['savename'];
        
        $temp = [];
        $temp['filename'] = $file['avater']['name'];
        $temp['filesize'] = $file['avater']['size'];
        $temp['fileext']  = $file['avater']['ext'];
        $temp['isimage']  = 1;
        $temp['filepath'] = $data['avater'];
        cms_attachment($temp); // @todo: 写入附件
        }
        $temp = C('OAUTH_CMSKEY');
        $data['passprot'] = $data['password'];
        $data['password'] = cms_password($data['passprot'], $temp);
        
        $where              = [];
        $where['userid']    = $info['userid'];
        
        $tips = '修改系统『'.$name.'』用户';
        $ress = $this->userModel->where($where)->save($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $userid = I('get.userid', 0, 'intval');
        
        $info               = [];
        $info['userid']     = $userid;
        
        $where              = [];
        $where['userid']    = $info['userid'];
        
        $data = $this->userModel->where($where)->find();
        $data['password']   = $data['passprot'];
        
        $user = in_array($data['userid'], C('SUPER_USERID'));
        $role = in_array($data['roleid'], C('SUPER_ROLEID'));
        
        $temp = $data['userid'] == session('CMS_UserID');
        $disabled = $user || $role || $temp;
        
        $where              = [];
        $where['disabled']  = 0;
        
        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        
        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );
        $this->assign('tree', $tree);
        $this->assign('data', $data);
        $this->assign('disabled', $disabled);
        
        $this->display('action');
    }
    
    /**
     * 删除用户
     */
    public final function delete() {
        $userid = I('get.userid', 0, 'intval');
        
        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';
        
        $info = $userid ? [$userid] : $info['userid'];
        if (empty($info)) $this->error('请选择用户！');
        
        foreach ($info as $key => $userid) {
        if ($userid  == session('CMS_UserID')  ||
            in_array($userid, C('SUPER_USERID')))
            continue;
        $where          = [];
        $where['userid']= $userid;
        
        $ress = $this->userModel->where($where)->find();
        if (file_exists($ress['avater'])) {
           @unlink($ress['avater']);
        }
        $data['userid'][] = $userid;
        }
        if (empty($data['userid'])) $this->error('请选择用户！');
        
        $userid = implode(',', $data['userid']);
        $where = ['userid' => ['IN', $userid]];
        
        $temp = 'userid: '.$userid;
        $ress = $this->userModel->where($where)->delete();
        if (!$ress) $this->error('删除用户失败！', $temp);
        else $this->success('删除用户成功！', $temp, U('index'));
    }
    
    /**
     * 更新状态
     */
    public final function state() {
        $userid = I('get.userid', 0, 'intval');
        
        $info               = [];
        $info['userid']     = $userid;
        
        $where              = [];
        $where['userid']    = $info['userid'];
        $ress = $this->userModel->where($where)->find();
        
        $info['roleid']     = $ress['roleid'];
        $info['username']   = $ress['username'];
        
        $user = in_array($info['userid'], C('SUPER_USERID'));
        $role = in_array($info['roleid'], C('SUPER_ROLEID'));
        
        $usertemp = $info['userid'] == session('CMS_UserID');
        if ($user || $role || $usertemp) {
            $tips = '设置用户『' .$ress['username']. '』状态无效！';
            cms_writelog($tips, ['userid'=>$info['userid']]);
            
            echo json_encode(2); return false;
        }
        $state              = $ress['disabled'];
        $value              = $state ? 0 : 1;
        
        $where              = [];
        $where['userid']    = $info['userid'];
        $where['roleid']    = $info['roleid'];
        
        $ress = $this->userModel->where($where)->save([
            'disabled'      => $value
        ]);
        $data = $this->userModel->where($where)->find();
        $oper = $data['disabled'] ? '启用系统' : '禁用系统';
        
        if (!$ress) $state = '失败！';
        else $state = '成功！';
        
        $tips = $oper.'『'.$data['username'].'』用户'.$state;
        cms_writelog($tips, ['userid' => $info['userid']]);
        
        echo json_encode($data['disabled']);
    }
    
    /**
     * 移动用户
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit') || I('get.dosubmit')) {
        $roleid = I('get.roleid', 0, 'intval');
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $data['roleid'] = $roleid ?: $data['roleid'];

        if (!is_array($info['userid'])) {
            $info['userid'] = [$info['userid']];
        }
        if (empty($info['userid'])) $this->error('请选择用户！');
        
        foreach ($info['userid'] as $key => $userid) {
        $where = ['userid' => $userid];
        $ress = $this->userModel->where($where)->find();
        
        $user = in_array($ress['userid'], C('SUPER_USERID'));
        $role = in_array($ress['roleid'], C('SUPER_ROLEID'));

        $temp = $ress['userid'] == session('CMS_UserID');
        if ($user || $role || $temp || empty($userid)) continue;
        
        // 重置权限
        $setting = unserialize($ress['setting']);
        $setting['priv'] = '';
        $setting['menu'] = '';
        
        $setting = serialize($setting);
        $this->userModel->where($where)->save(['setting'=>$setting]);
        $data['userid'][]   = $userid;
        }
        if (empty($data['userid'])) $this->error('请选择用户！');
        
        $where              = [];
        $where['userid']    = ['IN', $data['userid']];
        
        $temp = ['roleid' => $data['roleid']];
        $ress = $this->userModel->where($where)->save($temp);
        
        $where              = [];
        $where['roleid']    = $data['roleid'];
        
        $name = $this->roleModel->where($where)->find()['name'];
        $tips = '移动用户到『'.$name.'』用户组';
        
        $temp               = [];
        $temp['roleid']     = $data['roleid'];
        $temp['userid']     = implode(',', $data['userid']);
        
        if (IS_POST && I('post.dosubmit')) // @todo: 弹窗
        if (!$ress) $this->popup(false, $tips, $temp);
        else $this->popup(true, $tips, $temp);
        
        else // @todo: 普通页面
        if (!$ress) $this->error($tips.'失败！', $temp);
        else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo: 
        
        $userid = I('get.userid', 0, 'intval');
        
        $info               = [];
        $info['userid']     = $userid;
        
        $where              = [];
        $where['userid']    = $info['userid'];
        
        $data = [];
        $data = $this->userModel->where($where)->find();
        
        $data = cms_stripslashes($data);
        $info['roleid']     = $data['roleid'];
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        
        $role = $this->roleModel->where($where)->find();
        
        $where              = [];
        $where['disabled']  =  0;
        $ress = $this->roleModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 修改密码
     */
    public final function password() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', []);
        $data['password']   = trim($data['password']);
        
        $encrypt2 = C('OAUTH_CMSKEY');
        $password = cms_password($data['password'], $encrypt2);
        
        $data['passprot']   = $data['password'];
        $data['password']   = $password;
        
        $where              = [];
        $where['username']  = $this->username;
        $ress = $this->userModel->where($where)->save($data);
        $tips = '修改用户『'.$this->username.'』密码';
        
        if (!$ress) $this->popup(false, $tips, $data);
        else $this->popup(true, $tips, $data);
        } // @todo: 
        
        $where              = [];
        $where['username']  = $this->username;
        
        $data = $this->userModel->where($where)->find();
        $this->assign('data', $data);
        
        $this->display('pass2');
    }
    
    /**
     * 用户信息
     */
    public final function infos() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', []);
        
        if (empty($_FILES['avater']['error']) &&
            isset($_FILES['avater'])) {
        $this->upload->saveName = $this->username;
        
        $file = $this->upload->upload();
        if (!$file) $this->error('头像上传失败！');
        
        $data['avater']     = $this->upload->rootPath;
        $data['avater']    .= $file['avater']['savepath'];
        $data['avater']    .= $file['avater']['savename'];
        
        $temp = [];
        $temp['filename']   = $file['avater']['name'];
        $temp['filesize']   = $file['avater']['size'];
        $temp['fileext']    = $file['avater']['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['avater'];
        cms_attachment($temp); // @todo: 写入附件
        }
        $where = [];
        $where['username']  = $this->username;
        $ress = $this->userModel->where($where)->save($data);
        
        $tips = '更新用户『'.$this->username.'』信息';
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, '', 3);
        } // @todo: 
        
        $where              = [];
        $where['username']  = $this->username;
        
        $data = $this->userModel->where($where)->find();
        $data['password']   = $data['passprot'];
        
        $where              = [];
        $where['disabled']  =  0;
        $ress = $this->roleModel->treeList($where);
        
        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );
        
        $this->assign('data', $data);
        $this->assign('tree', $tree);
        
        $this->display();
    }
    
    /**
     * 我的权限
     */
    public final function privs() {
        $oauth = $this->oauth2->operPriv();
        $where              = [];
        $where['display']   =  1;
        
        if (empty($oauth)) $where['menuid'] = 0;
        else $where['menuid'] = ['IN', $oauth];
        
        $menus = $this->menuModel->where($where)->order(
            '`listorder` ASC, `menuid` ASC'
        )->select();
        $array = [];
        foreach ($menus as $key => $row) {
        $links = $this->menuModel->linkurl($row);
        $links = $row['linkurl'] ? : $links;
        $row['links'] = cms_stripslashes($links);
        $array[$row['menuid']] = $row;
        }
        $text = "<tr>
        <th class='cms-checkbox'>
        <input class='checkchild' type='checkbox' \$checked
        name='data[menuid][]' value='\$menuid' disabled />
        </th>
        <td class='cms-small'>\$menuid</td>
        <td>
        \$spacer<a>\$name</a>　
        <span class='cms-c999'>\$description</span>
        </td>
        <td><span class='cms-c999'>\$links</span></td>
        </tr>";
        $tree = cms_tree_menu(
            $array, $this->menuid, 
            $text, '', [], ['field' => $this->menuField]
        );
        $this->assign('menutree', $tree);
        $this->assign('username', $this->username);
        $this->display();
    }

    /**
     * 省市介绍信息（包含图片）
     */
    public final function cityinfo(){
        if(IS_POST){
            $cityid = I('post.main')['cityid'];
            if($cityid[count($cityid) -1]){
                $linkageid = $cityid[count($cityid) - 1];
            }else if($cityid[count($cityid) - 2]){
                $linkageid = $cityid[count($cityid) - 2];
            }else{
                $this->error("请选择地区!");
            }
            //内容
            $content = I('post.data')['content'];
            // 将特殊的 HTML 实体转换回普通字符
            $content = htmlspecialchars_decode($content);
            $data['introduce'] = $content;

            if($_FILES['photo']['size']) {
                //上传图片
                $upload = new \Think\Upload();// 实例化上传类
                $upload->maxSize = 3145728;// 设置附件上传大小
                $upload->subName = array('date', 'Ymd');
                $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
                $upload->rootPath = './Temporary/Attachment/Units/'; // 设置附件上传根目录
                // 上传单个文件
                $info = $upload->uploadOne($_FILES['photo']);

                if (!$info) {// 上传错误提示错误信息
                    //上传失败
                    $this->error("图片上传失败！支持格式:jpg,gif,png,jpeg");
                } else {
                    $path = $upload->rootPath . $info['savepath'] . $info['savename'];
                    $data['image'] = $path;
                }
            }
            $save = D('Admin/Linkage')->where(['linkageid' => $linkageid])->save($data);

            if ($save) {
                $this->success("上传成功");
            } else {
                $this->error("上传失败");
            }
            die;
        }

        $this->display();
    }

    /**
     * 事业单位介绍(包含图片)
     */
    public final function unitinfo(){
        if(IS_POST){
            $params = I('post.');
            //事业单位id
            $units = $params['units'];
            if($units[count($units) - 1]){
                $unitid = $units[count($units) - 1];
            }else if($units[count($units) - 2]){
                $unitid = $units[count($units) - 2];
            }else{
                $this->error("未选择事业单位!");
            }

            //联动菜单id
            $linkageids = $params['main']['cityid'];
            if($linkageids[count($linkageids) - 1]){
                $linkageid = $linkageids[count($linkageids) - 1];
            }else if($linkageids[count($linkageids) - 2]){
                $linkageid = $linkageids[count($linkageids) - 2];
            }else{
                $this->error("未选择省市区!");
            }

            //内容
            $content = $params['data']['content'];
            // 将特殊的 HTML 实体转换回普通字符
            $content = htmlspecialchars_decode($content);
            $data['abouts'] = $content;

            //图片
            if($_FILES['photo']['size']) {
                //上传图片
                $upload = new \Think\Upload();// 实例化上传类
                $upload->maxSize = 3145728;// 设置附件上传大小
                $upload->subName = array('date', 'Ymd');
                $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
                $upload->rootPath = './Temporary/Attachment/Units/'; // 设置附件上传根目录
                // 上传单个文件
                $info = $upload->uploadOne($_FILES['photo']);

                if (!$info) {// 上传错误提示错误信息
                    //上传失败
                    $this->error("图片上传失败！支持格式:jpg,gif,png,jpeg");
                } else {
                    $path = $upload->rootPath . $info['savepath'] . $info['savename'];
                    $data['image'] = $path;
                }
            }

            $_where = [];
            $_where['unitid'] = $unitid;
            $_where['linkageid'] = $linkageid;
            $save = D('Admin/Institution')->where($_where)->save($data);

            if ($save) {
                $this->success("上传成功");
            } else {
                $this->error("上传失败");
            }
            die;
        }

        $this->display();
    }

    /**
     * 国家部委信息
     */
    public final function governmentinfo(){
        if(IS_POST){
            $params = I('post.');
            $linkageid = $params['linkageid'];
            if(!$linkageid){
                $this->error("未选择部委机构！");
            }

            $data['introduce'] = htmlspecialchars_decode($params['content']);

            if($_FILES['photo']['size']) {

                $upload = new \Think\Upload();// 实例化上传类
                $upload->maxSize = 3145728;// 设置附件上传大小
                $upload->subName = array('date', 'Ymd');
                $upload->exts = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
                $upload->rootPath = './Temporary/Attachment/Units/'; // 设置附件上传根目录
                // 上传单个文件
                $info = $upload->uploadOne($_FILES['photo']);

                if (!$info) {// 上传错误提示错误信息
                    //上传失败
                    $this->error("图片上传失败！支持格式:jpg,gif,png,jpeg");
                } else {
                    $path = $upload->rootPath . $info['savepath'] . $info['savename'];
                    $data['image'] = $path;
                }
            }

            $save = D('Admin/Linkage')->where(['linkageid' => $linkageid])->save($data);

            if ($save) {
                $this->success("上传成功");
            } else {
                $this->error("上传失败");
            }
            die;
        }

        $this->display();
    }
}
