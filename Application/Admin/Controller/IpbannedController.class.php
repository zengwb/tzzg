<?php

namespace Admin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块IP禁用控制器类：呈现IP禁用常规管理操作
 * 
 * @author T-01
 */
final class IpbannedController extends BaseController {
    
    public      $action     = ['index', 'add', 'delete'];
    
    private     $dataModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->dataModel    = D('Admin/Ipbanned');
    }
    
    /**
     * IP管理
     */
    public final function index() {
        $nums = $this->dataModel->where()->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        
        $data = $this->dataModel->where()->limit(
            $page->firstRow.','.$page->listRows
        )->order('`ipid` DESC')->select();
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 添加IP
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['time'] = strtotime($data['time']);
        if (empty($data['end'])) $data['end'] = $data['address'];
        
        $where              = [];
        $where['address']   = $data['address'];
        $ress = $this->dataModel->where($where)->find();
        
        $tips = 'IP地址『'.$data['address'].'』已经存在！';
        if ($ress) $this->error($tips, $data);
        
        $tips = '添加IP『'.$data['address'].'』地址';
        $ress = $this->dataModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！', $data);
        else $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $this->display('action');
    }
    
    /**
     * 删除IP
     */
    public final function delete() {
        $ipid = I('get.ipid', 0, 'intval');
        
        $info = I('post.info', []);
        $info = $ipid ? [$ipid] : $info['ipid'];
        $data = [];
        if (empty($info)) $this->error('请选择IP！');
        
        foreach ($info as $key => $ipid) {
            $data['ipid'][]   = $ipid;
        }
        $where              = [];
        $where['ipid']      = ['IN', $data['ipid']];
        
        $ress = $this->dataModel->where($where)->delete();
        $temp = [];
        $temp['ipid'] = implode(',', $data['ipid']);
        
        if (!$ress) $this->error('删除禁用IP失败！', $temp);
        else $this->success('删除禁用IP成功！', $temp);
    }
    
}
