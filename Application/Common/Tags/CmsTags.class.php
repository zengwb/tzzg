<?php

namespace Common\Tags;

use \Think\Template\TagLib;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块自定义内容标签解析类
 * 
 * @author T-01
 */
final class CmsTags extends TagLib {
    
    protected   $tags       = [],
                $site       = '',
                $link       = '';
    
    public function __construct() {
        parent::__construct();
        $this->tags     =  [
            'keywords'  => ['attr' => ['content,linkurl']],
            'relation'  => [
                'attr'  => 'catid,id,number,keywords,data,order,siteid,empty',
                'close' => 1,
                'level' => 3
            ],
            'ranking'   => [
                'attr'  => 'catid,number,order,siteid',
                'close' => 1,
                'level' => 3
            ],
            'voteitem'  => [
                'attr'  => 'voteid,data,order,siteid',
                'close' => 1,
                'level' => 3
            ],
            'position'  => [
                'attr'  => 'positionid,catid,number,data,order,siteid',
                'close' => 1,
                'level' => 3
            ],
            'linkage'   => [
                'attr'  => 'typeid,number,data,order,empty,siteid',
                'close' => 1,
                'level' => 3
            ],
            'content'   => [
                'attr'  => 'field,catid,typeid,number,where,keyword,data,order,siteid,page,cityid',
                'close' => 1,
                'level' => 3
            ],
            'navigation'=> [
                'attr'  => 'navigationid,number,data,order,siteid',
                'close' => 1,
                'level' => 3
            ],
            'catename'  => ['attr'  => 'catid,siteid', 'level' => 3],
            'category'  => [
                'attr'  => 'field,catid,number,data,order,siteid',
                'close' => 1,
                'level' => 3
            ],
            //获取联动菜单，根据父级id获取
            'getlinkage'=>[
                'attr'   => 'parentid,siteid,number,order,data',
                'closer' => 1,
                'level'  => 3
            ],
            //国家战略
            'gettype'=>[
                'attr' => 'catid,siteid,order,number',
            ],
            //获取省级单位
            'getsheng'=>[
                'attr'=>'linkageid,parentid,number,siteid,order,class',
            ],
            //获取省级单位
            'getsheng1'=>[
                'attr'=>'linkageid,number,siteid,order,class',
            ],
            //获取项目
            'getsheng2'=>[
                'attr'=>'cityid,number,siteid,order,class',
            ],
        ];
        $this->site     = cms_site_info()['domain'];
        $this->link     = cms_config_array('Article', 'base')['cat_link'];
    }

    public final function _keywords($attr, $content) {
        $url1   = $this->link ? 'search.html?' : substr(U('home/search/lists'),1).'&';
        $url2   = $attr['linkurl'] ? : $this->site.$url1;
        $html   = '<?php ';
        
        $html  .= '$_link = \''.$this->site.'\';';
        $html  .= '$_keys = strtr('.$attr['content'].', [\'，\' => \',\']);';
        $html  .= '$_array = explode(\',\', $_keys);';
        
        $html  .= 'foreach ($_array as $_key => $_text) :';
        $html  .= '$row[\'text\'] = trim($_text);';
        $html  .= '$row[\'link\'] = \''.$url2.'\';';
        
        $html  .= '?>';
        $html  .= $this->tpl->parse($content);
        $html  .= '<?php endforeach; ?>';
        return $html;
    }
    
    public final function _relation($attr, $content) {
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_catid = '.($attr['catid'] ? : 0).';';
        
        $html  .= '$_data = \''.($attr['data'] ? : 'row').'\';';
        $html  .= '$_count = '.($attr['number'] ? : 5).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        $html  .= '$_where[\'catid\'] = $_catid;';
        $html  .= '$_category = D(\'Admin/Category\')->where($_where)->find();';
        
        $html  .= '$_where = [\'modelid\' => $_category[\'modelid\']];';
        $html  .= '$_model = D(\'Admin/Model\')->where($_where)->find();';
        $html  .= '$_table = $_model[\'table\'];';
        
        $html  .= 'if (!empty($_table)) :';
        $html  .= '$_where = [\'mid\' => '.$attr['id'].'];';
        $html  .= '$_ress = D($_table.\'_data\')->where($_where)->find();';
        
        $html  .= '$_relation = $_ress[\'relation\'];';
        $html  .= '$_array = explode(\',\', $_relation);';
        $html  .= '$_array = $_relation ? $_array : [];';
        
        $html  .= 'if (count($_array) > 0) :';
        $html  .= '$_info = [];';
        
        $html  .= 'foreach ($_array as $_key => $_id) :';
        $html  .= 'if (empty($_id)) continue;';
        $html  .= '$_where = ["mid" => $_id];';
        $html  .= '$_data = D($_table)->where($_where)->find();';
        
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $_data[\'catid\'];';
        $html  .= '$_args[\'id\'] = $_data[\'mid\'];';
        $html  .= '$_link = U(\'home/index/shows\', $_args);';
        
        $html  .= '$_html = \'show-\'.$_data[\'catid\'].\'-\';';
        $html  .= '$_html.= $_data[\'mid\'].\'.html\';';
        
        $html  .= '$_link = '.($this->link ? '$_html' : '$_link').';';
        $html  .= '$_data[\'link\'] = $_data[\'url\'] ?: $_link;';
        
        $html  .= '$_info[$key] = $_data;';
        $html  .= 'endforeach;';
        $html  .= 'endif;'; // $array
        
        $html  .= 'if (count($_array) < 1) :';
        $html  .= '$_catids = D(\'Admin/category\')->childid($_catid, $_siteid);';
        $html  .= '$_where = [];';
        $html  .= '$_where[\'catid\'] = [\'IN\', $_catids];';
        $html  .= '$_where[\'mid\'] = [\'NEQ\', '.$attr['id'].'];';
        
        $html  .= '$_info = D($_table)->where($_where)';
        $html  .= '->limit(\'0,\'.$_count.\'\')';
        $html  .= '->order($_order)->select();';
        $html  .= 'endif;'; // $array
        
        $html  .= '$_ress = $_info;';
        $html  .= 'foreach ($_ress as $key => $row) :';
        
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $row[\'catid\'];';
        $html  .= '$_args[\'id\'] = $row[\'mid\'];';
        $html  .= '$_link = U(\'home/index/shows\', $_args);';
        
        $html  .= '$_html = \'show-\'.$row[\'catid\'].\'-\';';
        $html  .= '$_html.= $row[\'mid\'].\'.html\';';
        
        $html  .= '$_link = '.($this->link ? '$_html' : '$_link').';';
        $html  .= '$row[\'link\'] = $row[\'url\'] ?: $_link;';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html  .= '<?php endforeach; endif; // $table ?>';
        $html  .= '<?php if (!$_ress) { ';
        $html  .= 'echo "<div class=\"cms-empty\">'.$attr['empty'].'</div>";';
        $html  .= '} ?>';
        return $html;
    }
    
    public final function _ranking($attr, $content) {
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_catid = '.($attr['catid'] ? : 0).';';
        
        $html  .= '$_count = '.($attr['number'] ? : 5).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        
        $html  .= '$_catid = D(\'Admin/Category\')->childid($_catid, $_siteid);';
        $html  .= '$_where = [];';
        $html  .= '$_where[\'catid\'] = [\'IN\', $_catid];';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        
        $html  .= '$_ress = D(\'Plugin/Hits\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count.\'\')';
        $html  .= '->order($_order)->select();';
        
        $html  .= 'foreach ($_ress as $_row) :';
        $html  .= 'list($_module, $_catid, $_id) = explode(\'-\', $_row[\'hitid\']);';
        
        $html  .= '$_where = [\'catid\' => $_catid];';
        $html  .= '$_category = D(\'Admin/Category\')->where($_where)->find();';
        $html  .= '$_modelid = $_category[\'modelid\'];';
        
        $html  .= '$_where = [\'modelid\' => $_modelid];';
        $html  .= '$_model = D(\'Admin/Model\')->where($_where)->find();';
        $html  .= '$_table = $_model[\'table\'];';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'catid\'] = [\'IN\', $_catid];';
        $html  .= '$_where[\'mid\'] = $_id;';
        $html  .= '$_data = D($_table)->where($_where)->find();';
        
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $_data[\'catid\'];';
        $html  .= '$_args[\'id\'] = $_data[\'mid\'];';
        $html  .= '$_link = U(\'home/index/shows\', $_args);';
        
        $html  .= '$_html = \'show-\'.$_data[\'catid\'].\'-\';';
        $html  .= '$_html.= $_data[\'mid\'].\'.html\';';
        
        $html  .= '$_link = '.($this->link ? '$_html' : '$_link').';';
        $html  .= '$link = $_data[\'url\'] ?: $_link;';
        
        $html  .= '$thumb = strtr($_data[\'thumb\'], [\'./\' => \'/\']);';
        $html  .= '$title = $_data[\'title\'];';
        $html  .= '$description = $_data[\'description\'];';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html  .= '<?php endforeach; ?>';
        return $html;
    }
    
    public final function _voteitem($attr, $content) {
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'voteid\'] = '.$attr['voteid'].';';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        
        $html  .= '$_ress = D(\'Vote/Vote\')->where($_where)->find();';
//         $html  .= '$type = $_ress[\'maxval\'] <= 1 ? \'radio\' : \'checkbox\';';
        $html  .= '$type = $_ress[\'ischeckbox\'] ? \'checkbox\' : \'radio\';';
        $html  .= '$subject = $_ress[\'subject\'];';
        $html  .= '$voteid = $_ress[\'voteid\'];';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'voteid\'] = '.$attr['voteid'].';';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        $html  .= '$_where[\'enabled\'] = 1;';
        
        $html  .= '$'.($attr['data'] ?: 'row').' = D(\'Vote/VoteOption\')';
        $html  .= '->where($_where)';
        $html  .= '->limit(\'0,\'.$_ress[\'option\'].\'\')';
        $html  .= '->order($_order)->select();';
        $html  .= '?>';
        
        $html .= $this->tpl->parse($content);
        return $html;
    }
    
    public final function _position($attr, $content) {
        $data   = '$'.($attr['data']?:'row');
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_order  = \''.($attr['order']? : '').'\';';
        $html  .= '$_positionid = \''.($attr['positionid'] ?: 0).'\';';
        $html  .= '$_count  = \''.($attr['number'] ? : 0).'\';';
        
        $html  .= '$_catid  = '.($attr['catid']  ? :  0).';';
        $html  .= '$_catids = D(\'Admin/category\')->childid($_catid, $_siteid);';
        
        $html  .= '$_where  = [];';
        $html  .= '$_where[\'posiid\'] = $_positionid;';
        $html  .= '$_where[\'catid\']  = [\'IN\', $_catids];';
        $html  .= '$_where[\'status\'] = 1;';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        
        $html  .= '$_ress = D(\'Article/Position\')->where($_where)->find();';
        $html  .= '$_nums = $_count ? : $_ress[\'number\'];';
        $html  .= 'unset($_where[\'status\']);';
        
        $html  .= '$_ress = D(\'Article/PositionData\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_nums)';
        $html  .= '->order($_order)->select();';
        
        $html  .= 'foreach ($_ress as $_key => $_val) :';
        $html  .= '$_where = [\'catid\' => $_val[\'catid\']];';
        $html  .= '$_catrs = D(\'Admin/category\')->where($_where)->find();';
        
        $html  .= $data.' = unserialize($_val[\'content\']);';
        $html  .= $data.'[\'thumb\'] = strtr('.$data.'[\'thumb\'], [\'./\' => \'/\']);';
        $html  .= $data.'[\'catname\'] = $_catrs[\'catname\'];';
        
        $html  .= $data.'[\'style\'] = unserialize('.$data.'[\'style\']);';
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $_val[\'catid\'];';
        $html  .= '$_args[\'id\'] = $_val[\'contentid\'];';
        $html  .= '$_link = U(\'home/index/shows\', $_args);';
        
        $html  .= '$_html = \'show-\'.$_val[\'catid\'].\'-\';';
        $html  .= '$_html.= $_val[\'contentid\'].\'.html\';';
        
        $html  .= '$_link = '.($this->link ? '$_html' : '$_link').';';
        $html  .= $data.'[\'link\'] = '.$data.'[\'url\'] ?: $_link;';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html  .= '<?php endforeach; ?>';
        return $html;
    }
    
    public final function _linkage($attr, $content) {
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_typeid = '.($attr['typeid'] ? : 0).';';
        $html  .= '$_order  = \''.($attr['order']? : '').'\';';
        $html  .= '$_count  = '.($attr['number'] ? : 0).';';
        
        $html  .= '$_where  = [];';
        $html  .= '$_where[\'typeid\'] = $_typeid;';
        $html  .= '$_where[\'siteid\'] = [\'IN\', [0, $_siteid]];';
        
        $html  .= '$_ress = D(\'Linkage/Group\')->where($_where)->find();';
        $html  .= '$_temp = unserialize($_ress[\'setting\']);';
        $html  .= '$_size = $_temp[\'size\'];';
        
        $html  .= '$_ress = D(\'Linkage/Link\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';
        
        $html  .= 'foreach ($_ress as $_key => $row) :';
        $html  .= '$row[\'style\'] = unserialize($row[\'style\']);';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html  .= '<?php endforeach; ?>';
        
        $html .= '<?php if (!$_ress) {';
        $html .= 'echo "<div class=\"cms-empty\">'.$attr['empty'].'</div>";';
        $html .= '} ?>';
        return $html;
    }
    
    public final function _content($attr, $content) {

        $data   = ($attr['data']) ? : 'row';
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_catid = '.($attr['catid'] ? : 0).';';
        $html  .= '$_modelid = '.($attr['modelid'] ?:0).';';
        $html  .= '$_typeid = '.($attr['typeid'] ? : 0).';';
        
        $html  .= '$_keyword = \''.($attr['keyword'] ? : '').'\';';
        $html  .= '$_field = \''.($attr['field'] ? : '*').'\';';
        
        $html  .= '$_count = '.($attr['number'] ? : 5).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        $html  .= '$_where[\'catid\'] = $_catid;';
        $html  .= '$_category = D(\'Admin/Category\')->where($_where)->find();';
        $html  .= '$_modelid = $_modelid ? : $_category[\'modelid\'];';
        
        $html  .= '$_where = [\'modelid\' => $_modelid];';
        $html  .= '$_model = D(\'Admin/Model\')->where($_where)->find();';
        $html  .= '$_table = $_model[\'table\'];';
        $html  .= '$_table_data = $_model[\'data\'] ? $_table.\'_data\' : null;';
        
        $html  .= 'if (!empty($_table)) :';
        $html  .= '$_catids = D(\'Admin/Category\')->childid($_catid, $_siteid);';
        $html  .= '$_where = [];';
        $html  .= '$_where[\'catid\'] = [\'IN\', $_catids];';
        $html  .= '$_where[\'disabled\'] = 0;';
        $html  .= 'if ($_typeid) $_where[\'typeid\'] = $_typeid;';
        
        $html  .= 'if (!empty($_keyword)) :';
        $html  .= '$_keys = \''.$attr['keyword'].'\' ? : \'\';';
        $html  .= '$_keyr = [$_keys => \'<font class="cf30">\'.$_keys.\'</font>\'];';
        
        $html  .= '$_temp[\'title\'] = [\'LIKE\', "%$_keyword%"];';
        $html  .= '$_temp[\'keywords\'] = [\'LIKE\', "%$_keyword%"];';
        $html  .= '$_temp[\'_logic\'] = \'OR\';';
        $html  .= '$_where[\'_complex\'] = $_temp;';
        $html  .= 'endif;';
        
        $html  .= '$_nums = D($_table)->where($_where)->count();';
        $html  .= '$_rows = $_count;';
        $html  .= '$_page = cms_page($_nums, $_rows);';
        
//         $html  .= '// @todo: 分页';
        
        $html  .= '$_limit = $_page->firstRow.\',\'.$_page->listRows;';
        $html  .= '$_ress = D($_table)->where($_where)->limit($_limit)';
        $html  .= '->order($_order)->field($_field)->select();';
        $html  .= 'foreach ($_ress as $key => $'.$data.') :';
        $html  .= '$_where = [\'catid\' => $'.$data.'[\'catid\']];';
        $html  .= '$_catrs = D(\'Admin/category\')->where($_where)->find();';
        
        $html  .= '$'.$data.'[\'catname\'] = $_catrs[\'catname\'];';
        $html  .= '$'.$data.'[\'title\'] = $'.$data.'[\'title\'] ? :';
        $html  .= '$'.$data.'[\'title\'];';
        
        $html  .= 'if ($_table_data) :';
        $html  .= '$_where = [\'mid\' => $'.$data.'[\'mid\']];';
        $html  .= '$'.$data.'_data = D($_table_data)->where($_where)->find();';
        $html  .= '$'.$data.' = array_merge($'.$data.', $'.$data.'_data);';
        $html  .= 'endif;';
        
        $html  .= 'if (!$_keyr) ';
        $html  .= '$'.$data.'[\'title2\'] = $'.$data.'[\'title\'];';
        $html  .= 'else ';
        $html  .= '$'.$data.'[\'title2\'] = strtr($'.$data.'[\'title\'], $_keyr);';
        $html  .= '$'.$data.'[\'style\'] = unserialize($'.$data.'[\'style\']);';
        $html  .= '$'.$data.'[\'thumb\'] = strtr($'.$data.'[\'thumb\'], ["./" => "/"]);';
        
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $'.$data.'[\'catid\'];';
        $html  .= '$_args[\'id\'] = $'.$data.'[\'mid\'];';
        $html  .= '$_link = U(\'home/index/shows\', $_args);';
        
        $html  .= '$_html = \'show-\'.$'.$data.'[\'catid\'].\'-\';';
        $html  .= '$_html.= $'.$data.'[\'mid\'].\'.html\';';
        
        $html  .= '$_link = '.($this->link ? '$_html' : '$_link').';';
        $html  .= '$'.$data.'[\'link\'] = $'.$data.'[\'url\'] ?: $_link;';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html  .= '<?php endforeach; endif; ?>';
        
        $html .= '<?php if (!$_ress) {';
        $html .= 'echo "<div class=\"cms-empty\">'.$attr['empty'].'</div>";';
        $html .= '} ?>';
        return $html;
    }
    
    public final function _navigation($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_navigationid = '.($attr['navigationid'] ?: 0).';';
        
        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'parentid\'] = $_navigationid;';
        $html  .= '$_where[\'display\'] = 1;';
        
        $html  .= '$_ress = D(\'Admin/Navigation\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';
        $html  .= 'foreach ($_ress as $key => $'.$data.') :';
        $html  .= '$'.$data.'[\'style\'] = unserialize($'.$data.'[\'style\']);';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';
        return $html;
    }
    
    public final function _catename($attr, $content) {
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_catid = '.($attr['catid'] ?: 0).';';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'catid\'] = $_catid;';
        $html  .= '$_where[\'display\'] = 1;';
        $html  .= '$_where[\'siteid\'] = $_siteid;';
        
        $html  .= '$data = D(\'Admin/Category\')->where($_where)->find();';
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $data[\'catid\'];';
        $html  .= '$_link = U(\'home/index/lists\', $_args);';
        
        $html  .= '$_dirs = \''.($this->site).'\';';
        $html  .= '$_dirs.= D(\'Admin/Category\')';
        $html  .= '->catalog($data[\'catid\'])[\'link\'];';
        
        $html  .= '$_link = '.($this->link ? '$_dirs' : '$_link').';';
        $html  .= '$data[\'link\'] = $data[\'linkage\'] ?: $_link;';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        return $html;
    }
    
    public final function _category($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_catid = '.($attr['catid'] ?: 0).';';
        
        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        
        $html  .= '$_where = [];';
        $html  .= '$_where[\'parentid\'] = $_catid;';
        $html  .= '$_where[\'display\'] = 1;';
        
        $html  .= '$_ress = D(\'Admin/Category\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';
        $html  .= 'foreach ($_ress as $key => $'.$data.') :';
        
        $html  .= '$_args = [];';
        $html  .= '$_args[\'catid\'] = $'.$data.'[\'catid\'];';
        $html  .= '$_link = U(\'home/index/lists\', $_args);';
        
        $html  .= '$_dirs = \''.($this->site).'\';';
        $html  .= '$_dirs.= D(\'Admin/Category\')';
        $html  .= '->catalog($'.$data.'[\'catid\'])[\'link\'];';
        
        $html  .= '$_link = '.($this->link ? '$_dirs' : '$_link').';';
        $html  .= '$'.$data.'[\'link\'] = $'.$data.'[\'linkage\'] ?: $_link;';
        $html  .= '?>';
        
        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';
        return $html;
    }


    public final function _getlinkage($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        $html   = '<?php ';
        $html  .= '$_parentid = '.($attr['parentid'] ? : 0 ).';';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';

        $html  .= '$_where = [];';
        $html  .= '$_where[\'parentid\'] = $_parentid;';
        $html  .= '$_where[\'display\'] = 1;';

        $html  .= '$_ress = D(\'Admin/Linkage\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';

        $html  .= 'foreach ($_ress as $key => $'.$data.') :';

        $html  .= '?>';

        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';
        return $html;
    }
    //获取省级部门
    public final function _getsheng($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        $html   = '<?php ';
        $html  .= '$_parentid = '.($attr['parentid'] ? : 0 ).';';
        $html  .= '$_linkageid = '.($attr['linkageid'] ? : 0 ).';';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        $html  .= '$_where = [];';
        $html  .= '$_where[\'parentid\'] = $_parentid;';
        $html  .= '$_where[\'linkageid\'] = $_linkageid;';
        $html  .= '$_where[\'disabled\'] = 0;';
        $html  .= '$_ress = D(\'Admin/Institution\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';
        $html  .= 'foreach ($_ress as $key => $'.$data.') :';
        //dump($html);
        $html  .= '?>';

        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';
        return $html;
    }
    //获取省级部门
    public final function _getsheng1($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        //dump($attr['linkageid']);
        $html   = '<?php ';
        $html  .= '$_linkageid = '.($attr['linkageid'] ? : 0 ).';';
        $html  .= '$_parentid = '.($attr['parentid'] ? : 15 ).';';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        $html  .= '$_where = [];';
        $html  .= '$_where[\'linkageid\'] = $_linkageid;';
        $html  .= '$_where[\'disabled\'] = 0;';
//        $html  .= '$_where[\'parentid\'] = [\'gt\',0];';
        $html  .= '$_where[\'parentid\'] = $_parentid;';
        $html  .= '$_ress = D(\'Admin/Institution\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';
        //dump($html);die;
        $html  .= 'foreach ($_ress as $key => $'.$data.') :';
        $html  .= '?>';
        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';
        //dump($html);die;
        return $html;
    }
    //获取项目
    public final function _getsheng2($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        $attr['cityid']= $attr['cityid'] ? ['like','%'.$attr['cityid'].'%']: 0 ;
        //dump($attr['city']);die;
        $html   = '<?php ';
        $html  .= '$_city = $attr[\'cityid\'];';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';
        $html  .= '$_where = [];';
        $html  .= '$_where[\'cityid\'] = $_city;';
        $html  .= '$_ress = D(\'Article/Project\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';
        $html  .= 'foreach ($_ress as $key => $'.$data.') : ';
        // $html  .= '$'.$data.'[\'cityid\'] = explode(\',\', $'.$data.'[\'cityid\']);';
        // $html  .= '$where[\'linkageid\'] = [\'in\',$'.$data.'[\'cityid\']];';
        // $html  .= '$row[\'city\'] = implode(\',\',array_column(D(\'Admin/Linkage\')->field(\'name\')->where($where)->select(),\'name\'));';
        $html  .= '?>';
        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';
        //dump($html);die;
        return $html;
    }

    /**
     * 国家战略 v1_type
     */
    public final function _gettype($attr, $content) {
        $data   = ($attr['data']) ? : 'row';
        $html   = '<?php ';
        $html  .= '$_siteid = '.($attr['siteid'] ? : cms_siteid()).';';
        $html  .= '$_catid = '.($attr['catid'] ?: 0).';';

        $html  .= '$_count = '.($attr['number'] ? : 99).';';
        $html  .= '$_order = \''.($attr['order'] ? : '').'\';';

        $html  .= '$_where = [];';
        $html  .= '$_where[\'catid\'] = $_catid;';
        $html  .= '$_where[\'siteid\'] = $_siteid;';

        $html  .= '$_ress = D(\'Article/Type\')->where($_where)';
        $html  .= '->limit(\'0,\'.$_count)';
        $html  .= '->order($_order)->select();';

        $html  .= 'foreach ($_ress as $key => $'.$data.') :';
        $html  .= '?>';

        $html  .= $this->tpl->parse($content);
        $html .= '<?php endforeach; ?>';

        return $html;
    }
}
