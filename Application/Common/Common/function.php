<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 预定义字符转换函数（避免预定义字符未经转义导致显示或存储异常）
 * 
 * @param string|array $string  必填。待转换的字符串或数组
 * 
 * @return string|unknown[]
 */
function cms_addslashes($string) {
    $array = [];
    if (!is_array($string)) return addslashes($string);
    
    foreach (array_keys($string) as $index) {
        $value = cms_addslashes($string[$index]);
        $array[cms_addslashes($index)] = $value;
    }
    return $array;
}

/**
 * 将预定义字符转换为HTML实体
 * 
 * @param string|array $string  必填。待转换的字符串或数组
 * @param string    $flags      选填。转换单引号或双引号条件
 * @param string    $encoding   选填。字符集；默认：UTF-8
 * 
 * @return string|unknown[]
 */
function cms_htmlspecialchars($string, $flags = '', $encoding = '') {
    $array = [];
    $encoding = $encoding ? : 'utf-8';
    
//     ENT_COMPAT   仅编码双引号
//     ENT_QUOTES   编码双引号和单引号
//     ENT_NOQUOTES 不编码任何引号
    $flags = $flags ? : ENT_COMPAT;
    
    if (!is_array($string)) {
        return htmlspecialchars($string, $flags, $encoding);
    }
    foreach (array_keys($string) as $index) {
        $func2 = 'cms_htmlspecialchars';
        
        $index = $func2($index, $flags, $encoding);
        $value = $func2($string[$index], $flags, $encoding);
        
        $array[$index] = $value;
    }
    return $array;
}

/**
 * 删除由cms_addslashes()添加的反斜线
 * 
 * @param string|array $string  必填。待转换的字符串或数组
 * 
 * @return string|unknown[]
 */
function cms_stripslashes($string) {
    $array = [];
    if (!is_array($string)) return stripslashes($string);
    
    foreach (array_keys($string) as $index) {
        $value = cms_stripslashes($string[$index]);
        $array[cms_stripslashes($index)] = $value;
    }
    return $array;
}

/**
 * 生成图形验证码（ThinkPHP内置方法）
 * 
 * @param integer   $len        选填。字符长度；默认长度：4
 * @param array     $bg         选填。图片背景；默认：白色
 * @param integer   $size       选填。字体大小；默认：24PX
 * @param integer   $id         选填。验证标识
 */
function cms_verify_code($len = 4, $bg = ['255, 255, 255'],
    $size = 24, $id = 1) {
    (new \Think\Verify([
        'length' => $len, 'bg' => $bg, 'fontSize' => $size
    ]))->entry($id);
}

/**
 * 检查图形验证结果
 * 
 * @param string    $code       必填。验证字符
 * @param integer   $id         选填。验证标识
 * 
 * @return boolean
 */
function cms_verify_check($code, $id = 1) {
    return (new \Think\Verify())->check($code, $id);
}

/**
 * 将字符串转换为JS格式输出（字符串）
 * 
 * @param string    $string     必填。待转换的字符串
 * @param boolean   $javascript 选填。JS输出条件
 * 
 * @return string|unknown[]
 */
function cms_string2script($string, $javascript = true) {
    $string = strtr($string, ['\r' => '', '\n' => '', '\t' => '']);
    $string = cms_addslashes($string);
    
    return $javascript ? 'document.write(\''.$string.'\')' : $string;
}

/**
 * 将字符串转换为数组（JSON格式）
 * 
 * @param string    $string     必填。待转换的字符串
 * 
 * @return mixed
 */
function cms_string2array($string) {
    if ($string == '' || empty($string)) $string = [];
    return json_decode(cms_stripslashes($string), true);
}

/**
 * 将数组转换为字符串（JSON格式）
 * 
 * @param array     $array      必填。待转换的数组
 * @param boolean   $format     选填。字符格式化（cms_addslashes）
 * 
 * @return string|string|unknown[]
 */
function cms_array2string($array, $format = true) {
    if (empty($array)) return '';
    
    if ($format) $array = cms_stripslashes($array);
    return cms_addslashes(json_encode($array, JSON_FORCE_OBJECT));
}

/**
 * 转换文件字节为计算机单位
 * 
 * @param integer   $bytesize   必填。文件字节大小
 * 
 * @return string
 */
function cms_byte_unit($bytesize) {
    $units = [
        'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB', 'BB'
    ];
    for ($i = 0; $bytesize >= 1024 && $i < count($units); $i++) {
        $bytesize /= 1024;
    }
    return round($bytesize, 2).' '.$units[$i];
}

/**
 * 生成随机字符串函数
 * 
 * @param integer   $length     选填。字符串长度；默认：4
 * @param string    $chars      选填。随机字符范围；默认：0123456789
 * 
 * @return string
 */
function cms_random($length = 4, $chars = '0123456789') {
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= $chars[mt_rand(0, strlen($chars) - 1)];
    }
    return $string;
}

/**
 * 获取文件后缀（类型）函数
 * 
 * @param string    $filename   必填。文件路径（或文件名称）
 * 
 * @return string
 */
function cms_fileext($filename) {
    return strtolower(trim(substr(strchr($filename, '.'), 1, 10)));
}

/**
 * 合并数组
 * 
 * @param array     $params     必填。
 * @param array|string $context 必填。
 * 
 * @return array
 */
function cms_array_merge($params, $context) {
    $array = [];
    if (!is_array($context)) $context = [$context];
    $parameter = array_diff_key($params, $context);
    
    foreach ($parameter as $k => $v) {
        if (!is_numeric($k)) continue;
        $array[$v] = $context[$v] ?  : $params[$v];
    }
    return $array;
}

/**
 * 自定义MD5加密函数
 * 
 * @param string|mixed $password 必填。待处理的明文密码字符
 * @param string    $encrypt    必填。密钥（默认：4位随机数字）
 * 
 * @return mixed|string[]
 */
function cms_password($password, $encrypt = '') {
    $array = [];
    $array['encrypt'] = $encrypt ? : cms_random();
    $array['password']= md5(md5(trim($password)).$array['encrypt']);
    return $encrypt ? $array['password'] : $array;
}

/**
 * 时间戳格式化函数，输出：时:分:秒 或 分:秒
 * 
 * @param integer   $time       必填。时间戳（单位：秒）
 * 
 * @return string
 */
function cms_date_format($time) {
    $hours2 = floor($time / 3600     );
    $minite = floor($time % 3600 / 60);
    $secend = floor($time % 3600 % 60);
    
    $hours2 = $hours2 < 10 ? '0'.$hours2 : $hours2;
    $minite = $minite < 10 ? '0'.$minite : $minite;
    $secend = $secend < 10 ? '0'.$secend : $secend;
    
    if ($time >= 3600) return $hours2.':'.$minite.':'.$secend;
    else return $minite.':'.$secend;
}

/**
 * 树形结构函数（公共模块树形结构扩展类）
 * 
 * @param array     $array      必填。树形结构数据（数组）
 * @param integer   $id         必填。条件ID
 * @param string    $text       必填。输出字符串
 * @param string    $adds       选填。修饰字符
 * @param array     $select     选填。选中记录（数组）
 * @param array     $attr       选填。重置树形类属性
 * 
 * @return string
 */
function cms_tree_menu($array, $id, $text, $adds = '', $select = [],
    $attr = []) {
    $tree = (new \Common\ORG\Tree());
    $tree->initialize($array);

    if (isset($attr['field'])) $tree->treeField = $attr['field' ];
    if (isset($attr['prefix']))$tree->treePrefix= $attr['prefix'];
    if (isset($attr['space'])) $tree->treeSpace = $attr['space' ];

    return $tree->build($id, $text, $adds, $select);
}

/**
 * 
 * @param unknown $array
 * @param unknown $action
 */
function cms_oauth($array, $action) {
    return (new \Common\ORG\OAuth2())->action($array, $action);
}

/**
 * 写入日志
 * 
 * @param string    $tips       选填。提示信息
 * @param string    $text       选填。内容参数
 * @param integer   $type       选填。日志类型（1登录，2操作，3异常）
 */
function cms_writelog($tips = '', $text = '', $type = 2) {
    $array = ['login', 'oper', 'error'];
    $model = ucwords($array[$type-1]).'Log';
    D('Common/'.$model)->writelog($tips, $text, $type);
}

/**
 * 获取配置参数（数组）
 * 
 * @param string    $module     选填。模块
 * @param string    $group      选填。分组
 * 
 * @return array
 */
function cms_config_array($module = '', $group = '') {
    $value = [];
    $array = D('Common/Config')->configArray($module, $group);
    foreach ($array as $key => $row) {
    $setting = unserialize($row['setting']);
    $row['value'] = $row['value'] ? : $setting['default'];
    
    if (empty($group))
    $value[$row['group']][$row['params']] = $row['value'];
    else
    $value[$row['params']] = $row['value'];
    }
    return $value;
}

/**
 * 获取当前（默认）站点ID
 * 
 * @return integer
 */
function cms_siteid() {
    $site = D('Admin/Website')->where(['display' => 1])->order(
        '`listorder` ASC, `siteid` ASC'
    )->find();
    
    return session('CMS_SiteID') ?: $site['siteid'];
}

/**
 * 生成站点SEO
 * 
 * @param integer   $catid      选填。栏目ID
 * @param array     $array      选填。SEO数组
 * 
 * @return array
 */
function cms_site_seo($catid = 0, $array = []) {
    $title          = $array['title'];
    $description    = $array['description'];
    $keyword        = $array['keywords'];
    
    $title      = $title ? strip_tags($title) : '';
    $description= $description ? strip_tags($description) : '';
    $keyword    = $keyword ? strtr(strip_tags($keyword), [' ' => ',']) : '';
    
    $site       = cms_site_info();
    $category   = [];
    
    if ($catid) {
    $where = ['catid' => $catid];
    $category = D('Admin/Category')->where($where)->find();
    $category['setting'] = unserialize($category['setting']);
    }
    $seo_title  = $category['setting']['seo_title'];
    $seo_title  = $seo_title ? $seo_title.' - ' : '';
    $seo_title  = $seo_title ? : $category['catname'].' - ';
    $seo_title  = $title ? $title.' - ' : $seo_title;
    $seo_title  = $catid || $title ? $seo_title : '';
    
    $seo_keyword     = $category['setting']['seo_keywords'];
    $seo_keyword     = $seo_keyword ? : '';
    $seo_keyword     = $keyword ? : $seo_keyword;
    
    $seo_description = $category['setting']['$seo_description'];
    $seo_description = $seo_description ? : '';
    $seo_description = $description ? : $seo_description;
    
    $seo['title']    = $site['title'] ? : $site['name'];
    $seo['title']    = $seo_title.$seo['title'];
    
    $seo['description'] = $seo_description ? : $site['description'];
    $seo['keywords'] = $seo_keyword ? : $site['keywords'];
    
    return $seo;
}

/**
 * 获取当前（默认）站点基础信息（引用站点数据模型类）
 * 
 * @return array
 */
function cms_site_info() {
    $site = D('Admin/Website');
    
    $info = $site->where(['siteid' => cms_siteid()])->find();
    $info['setting'] = unserialize($info['setting']);
    
    $data = $site->where(['display' => 1])->order(
        '`listorder` ASC, `siteid` ASC'
    )->find();
    $data['setting'] = unserialize($$data['setting']);
    
    return cms_stripslashes($info ? : $data);
}

/**
 * CMS加密令牌（引用MD5加密函数）
 * 
 * @param string|mixed $string  必填。令牌明文字符串
 * @param string    $encrypt    可选。密钥（默认：4位随机数字）
 * 
 * @return mixed|string[]
 */
function cms_token($string, $encrypt = '') {
    return cms_password($string, $encrypt); // @todo: 
}

/**
 * 写入附件数据（信息）
 * 
 * @param array     $files      必填。附件数据（数组）
 * 
 * @return boolean|mixed
 */
function cms_attachment($files) {
    $files['module']    = $files['module'] ? : MODULE_NAME;
    $files['catid']     = $files['catid']  ? : 0;
    
//     $files['uploadtime']= time();
//     $files['uploadip']  = get_client_ip();
    
    $files['siteid']    = $files['siteid'] ? : cms_siteid();
    
    return D('Common/Attachment')->insert($files);
}

/**
 * 系统分页函数
 * 
 * @param integer   $count      必填。数据总和
 * @param integer   $rows       选填。显示数量，默认24条数据
 */
function cms_page($count, $rows = 24) {
    return (new \Think\Page($count, $rows));
}

/**
 * 获取指定目录下的目录数据（数组格式）
 * 
 * @param string    $path       必填。目录路径
 * 
 * @return string[]
 */
function cms_directory($path) {
    $array = [];
    if ($handle = opendir($path)) {
        $i = 0;
        while ($file = readdir($handle)) {
            if ($file != '.' && $file != '..' && !strpos($file, '.')) {
                $array[$i] = $file;
                $i++;
            }
        }
    }
    return $array;
}

/**
 * 获取当前（默认）站点主题的视图模板配置信息（数组）
 * 
 * @param string    $path       必填。主题目录
 * @param boolean   $all        选填。全部模板
 * 
 * @return void[]
 */
function cms_theme_config($path, $all = false) {
    $files = [];
    $files = cms_directory($path);
    $theme = cms_site_info()['setting']['themes'];
    
    $array = [];
    foreach ($files as $key => $val) {
        if (!$all && !in_array($val, $theme)) continue;
        $array[$key] = load_config($path.$val.'/config.php');
    }
    return $array;
}

/**
 * 获取主题目录下满足条件的模板文件（数组）
 * 
 * @param string    $path       必填。模板目录
 * @param string    $theme      必填。风格主题
 * @param string    $module     必填。模块
 * @param string    $prefix     选填。模板前缀
 * 
 * @return array
 */
function cms_theme_views($path, $theme, $module, $prefix = '') {
    $subdirs = 'Views/'.$module.'/';
    $tplpath = $path.$theme.'/'.$subdirs;
    
    $type = empty($prefix) ? '*' : '{'.$prefix.'}*';
    $tplconfig = cms_theme_config($path);
    
    foreach ($tplconfig as $key => $val) {
        if ($val['dirname'] == ucwords($theme)) {
            $remarks = $val['remarks'];
            break;
        }
    }
    $files = glob($tplpath.$type, GLOB_BRACE);
    $array = [];
    foreach ($files as $key => $file) {
        $file = strtr($file, [$path.$theme.'/' => '']);
        $array[$key]['file'] = strtr($file, [$subdirs => '']);
        $array[$key]['name'] = $remarks[$file];
    }
    return $array;
}

/**
 * 获取模板目录及模板文件列表（数组）
 * 
 * @param string    $path       必填。目录路径
 * 
 * @return array;
 */
function cms_template($path) {
    $ress = $list = [];
    $ress = opendir($path);
    if ($ress) {
        while (false !== ($item = readdir($ress))) $list[] = $item;
        closedir($ress);
    }
    return $list ? : [];
}

/**
 * 删除文件夹及其文件夹下所有文件
 * 
 * @param string    $path       必填。目录路径
 * 
 * @return boolean
 */
function cms_delete_directory($path) {
    $handle = opendir($path);
    
    while ($file = readdir($handle)) {
        if ($file != '.' && $file != '..') {
            $fullpath = $path.'/'.$file;
            if (is_dir($fullpath)) cms_delete_directory($fullpath);
            else unlink($fullpath);
        }
    }
    closedir($handle);
    if (rmdir($path)) return true;
    else return false;
}

/**
 * 获取应用程序模块备注（配置）信息（数组）
 * 
 * @param string    $path       必填。应用程序目录
 * 
 * @return array
 */
function cms_module_config($path) {
    $array = [];
    $files = [];
    $files = cms_directory($path);
    
    foreach ($files as $key => $directory) {
        $filepath = $path.'/'. $directory .'/helper.php';
        if (!file_exists($filepath)) continue;
        
        $array[$key] = load_config($filepath);
    }
    return $array;
}

/**
 * 过滤敏感关键字
 * 
 * @param string|array $data    必填。待过滤字符（或数组）
 * @param string    $text       选填。返回结果
 * 
 * @return string|NULL[]
 */
function cms_keyword_filter($data, $text = '') {
    $config = cms_config_array('Admin', 'safety');
    $result = D('Admin/Keyword');
    
    if (!is_array($data)) {
        return strtr($data, $result->lists());
    }
    $filter = [];
    foreach ($data as $key => $val) {
        $filter[$key] = cms_keyword_filter($val, $text);
    }
    return $filter;
}

/**
 * 获取IP信息
 * 
 * @param string    $ip         选填。IP地址
 */
function cms_iplookup($ip = '') {
    if (empty($ip)) $ip = get_client_ip();
    
    $url = 'http://int.dpool.sina.com.cn/iplookup/';
    $url.= 'iplookup.php?format=js&ip=';
    $res = @file_get_contents($url);
    
    if (empty($res)) return false;
    $arr = [];
    preg_match('/\{.+?\}/', $res, $arr);
    if (!isset($arr[0])) return false;
    
    $json = json_decode($arr[0], true);
    if (isset($json['ret']) && $json['ret'] == 1) {
        $json['ip'] = $ip;
        unset($json['ret']);
    } else {
        return false;
    }
    return $json;
}

/**
 * 获取当前栏目（位置）
 * 
 * @param integer   $catid      选填。栏目ID
 * 
 * @return string
 */
function cms_location($catid = 0) {
    $catids = D('Admin/Category')->parentids($catid);
    $catarr = explode(',', $catids.','.$catid);

    $site = cms_site_info();
    $text = '';
    $text.= '<a href="'.$site['domain'].'">'.$site['name'].'</a>';
    
    sort($catarr);
    foreach ($catarr as $catid) {
    if (!$catid) continue;
    $where                  = [];
    $where['siteid']        = cms_siteid();
    $where['catid']         = $catid;
    $ress = D('Admin/Category')->where($where)->find();
    $text.= ' &gt; '.$ress['catname'];
    }

    return $text;
}

function cms_advert($spaceid, $js = false) {
    $info               = [];
    $info['spaceid']    = $spaceid ? : 0;
    
    $where              = [];
    $where['spaceid']   = $info['spaceid'];
    $where['siteid']    = ['IN', [0, cms_siteid()]];
    $where['disabled']  =  0;
    
    $space = D('Advert/AdvertSpace')->where($where)->find();
    if (!$space) return ;
    
    $template = $space['template'];
    $setting  = unserialize($space['setting']);
    
    $position = $setting['position'];
    $adsize = $setting['size'];
    $number = $setting['number'];
    
    $where              = [];
    $where['spaceid']   = $info['spaceid'];
    $where['starttime'] = ['ELT', time()];
    $where['endtime']   = ['EGT', time()];
    $where['disabled']  =  0;
    
    $limit = '0,'.$number;
    $lists = D('Advert/Advert')->where($where)->limit($limit)->order(
        '`listorder` ASC, `advertid` DESC'
    )->select();
    
    foreach ($lists as $key => $row) {
    $args = unserialize($row['setting']);
    $args['type']       = $row['type'];
    $args['size']       = $adsize;
    $args['advertid']   = $row['advertid'];
    $args['position']   = $position;
    
    list($w, $h) = explode(',', $args['size']);
    
    $file  = strtr($args['fileurl'], ['./' => '/']);
    $imgs  = strtr($args['imageurl'],['./' => '/']);
    
    $temp  = [];
    $temp['advertid']   = $args['advertid'];
    $temp['linkurl']    = $args['linkurl'];
    $site  = substr(cms_site_info()['domain'], 0, -1);
    $link  = $site.U('Plugin/Count/advert', $temp);
    
    $html  = '';
    $html .= '<li>';
    $html .= '<a href="'.$link.'" target="_blank" ';
    $html .= 'title="'.$args['alt'].'">';
    $html .= '<img src="'.$site.$imgs.'" alt="'.$args['alt'].'" ';
    $html .= 'style="width:'.$w.'px; height:'.$h.'px;';
    $html .= $args['style'].'" />';
    $html .= '</a>';
    $html .= '<p>';
    $html .= '<a href="'.$link.'" target="_blank">'.$args['alt'].'</a>';
    $html .= '</p>';
    $html .= '<p class="bg"></p>';
    $html .= '</li>';
    echo $html;
    }
}

/**
 * 内容模块字段内容过滤函数（自定义转换为字符串）
 * 
 * @param array     $data       必填。数据内容
 * 
 * @return unknown
 */
function cms_article_field_content_filter_string($data) {
    $data['cooperation'] = implode(',', cms_article_field_content_filter_string_helper($data['cooperation'])); // 项目模型，合作方式
    $data['city'] = implode(',', cms_article_field_content_filter_string_helper($data['city'])); // 项目模型，所在城市
    $data['industry_typeid'] = implode(',', cms_article_field_content_filter_string_helper($data['industry_typeid'])); // 项目模型，产业类别
    
//     $data['hot135'] = implode(',', cms_article_field_content_filter_string_helper($data['hot135'])); // 十三五热点
//     $data['hot_industry'] = implode(',', cms_article_field_content_filter_string_helper($data['hot_industry'])); // 热点产业
    
    $data['unitid_linkage'] = implode(',', cms_article_field_content_filter_string_helper($data['unitid_linkage']));
    $data['unitid'] = $data['unitid_linkage'].'|'.implode(',', cms_article_field_content_filter_string_helper($data['unitid'])); // 共享单位
    unset($data['unitid_linkage']);
    
    $data['maturity'] = implode(',', cms_article_field_content_filter_string_helper($data['maturity'])); // 高新技术，技术成熟度
    $data['areaid'] = implode(',', cms_article_field_content_filter_string_helper($data['areaid'])); // 高新技术，所在地区、国家战略项目
    
    $data['industry'] = implode(',', cms_article_field_content_filter_string_helper($data['industry'])); // 高新需求/高新技术，应用行业
    $data['scale'] = implode(',', cms_article_field_content_filter_string_helper($data['scale'])); // 高新需求，企业规模
    $data['price'] = implode(',', cms_article_field_content_filter_string_helper($data['price'])); // 高新需求/高新技术，合作价格

    $data['invest_type'] = implode(',', cms_article_field_content_filter_string_helper($data['invest_type'])); // 重大项目》项目融资》融资方式
    $data['invest_maturity'] = implode(',', cms_article_field_content_filter_string_helper($data['invest_maturity'])); // 重大项目》项目融资》投资阶段
    $data['invest_trade'] = implode(',', cms_article_field_content_filter_string_helper($data['invest_trade'])); // 重大项目》项目融资、国家战略项目》融资行业
    $data['invest_price'] = implode(',', cms_article_field_content_filter_string_helper($data['invest_price'])); // 重大项目》项目融资、国家战略项目》总投资金额
    $data['attract_pirce'] = implode(',', cms_article_field_content_filter_string_helper($data['attract_pirce'])); // 重大项目》项目融资》总引资金额

    $data['deal_class'] = implode(',', cms_article_field_content_filter_string_helper($data['deal_class'])); // 重大项目》资产交易》交易类别
    $data['deal_type'] = implode(',', cms_article_field_content_filter_string_helper($data['deal_type'])); // 重大项目》资产交易》交易方式
    $data['value_price'] = implode(',', cms_article_field_content_filter_string_helper($data['value_price'])); // 重大项目》资产交易》估值价格
    $data['make_price'] = implode(',', cms_article_field_content_filter_string_helper($data['make_price'])); // 重大项目》资产交易》转让价格

    $data['project_type'] = implode(',', cms_article_field_content_filter_string_helper($data['project_type'])); // 重大项目》国家战略项目》项目类型
    
    
    // @todo: 2018/11/13 修正
    $data['industryid'] = implode(',', cms_article_field_content_filter_string_helper($data['industryid'])); // 产业类别
    $data['cityid'] = implode(',', cms_article_field_content_filter_string_helper($data['cityid'])); // 所在城市

    //2018-11-26 新闻模型 添加group字段
    $data['group'] = implode(',', cms_article_field_content_filter_string_helper($data['group']));
    $data['transaction_type'] = implode(',', cms_article_field_content_filter_string_helper($data['transaction_type']));
    $data['enter_type'] = implode(',', cms_article_field_content_filter_string_helper($data['enter_type']));
    $data['suit_company'] = implode(',', cms_article_field_content_filter_string_helper($data['suit_company']));
    $data['finacing_style'] = implode(',', cms_article_field_content_filter_string_helper($data['finacing_style']));
    //精准投资 》 资金类型
    $data['funds_type'] = implode(',', cms_article_field_content_filter_string_helper($data['funds_type']));
    //精准投资 》 投资地区
    $data['invest_cityid'] = implode(',', cms_article_field_content_filter_string_helper($data['invest_cityid']));
    //精准投资 》 投资阶段
    $data['invest_stage'] = implode(',', cms_article_field_content_filter_string_helper($data['invest_stage']));
    $data['units'] = implode(',', cms_article_field_content_filter_string_helper($data['units']));
    //精准服务-政务服务
    $data['service_type'] = implode(',', cms_article_field_content_filter_string_helper($data['service_type']));
    $data['introduce'] = implode(',', cms_article_field_content_filter_string_helper($data['introduce']));

    $data['unit_group'] = implode(',', cms_article_field_content_filter_string_helper($data['unit_group']));
    $data['be_induid'] = implode(',', cms_article_field_content_filter_string_helper($data['be_induid']));

    $data['typeid'] = is_array($data['typeid']) ? implode(',', cms_article_field_content_filter_string_helper($data['typeid'])) : $data['typeid'];

    return $data;
}
/**
 * 内容模块字段内容过滤函数（自定义转换为数组）
 * 
 * @param array     $data       必填。数据内容
 * 
 * @return unknown
 */
function cms_article_field_content_filter_array($data) {
    $data['cooperation'] = explode(',', $data['cooperation']); // 项目模型，合作方式
    $data['city'] = explode(',', $data['city']); // 项目模型，所在城市
    $data['industry_typeid'] = explode(',', $data['industry_typeid']); // 项目模型，产业类别
    
//     $data['hot135'] = explode(',', $data['hot135']); // 十三五热点
//     $data['hot_industry'] = explode(',', $data['hot_industry']); // 热点产业
    
    $data['unitid'] = explode(',', $data['unitid']); // 共享单位
    
    $data['maturity'] = explode(',', $data['maturity']); // 高新技术，技术成熟度
    $data['areaid'] = explode(',', $data['areaid']); // 高新技术，所在地区、国家战略项目
    
    $data['industry'] = explode(',', $data['industry']); // 高新需求/高新技术，应用行业
    $data['scale'] = explode(',', $data['scale']); // 高新需求，企业规模
    $data['price'] = explode(',', $data['price']); // 高新需求/高新技术，合作价格

    $data['invest_type'] = explode(',', $data['invest_type']); // 重大项目》项目融资》融资方式
    $data['invest_maturity'] = explode(',', $data['invest_maturity']); // 重大项目》项目融资》投资阶段
    $data['invest_trade'] = explode(',', $data['invest_trade']); // 重大项目》项目融资、国家战略项目》融资行业
    $data['invest_price'] = explode(',', $data['invest_price']); // 重大项目》项目融资、国家战略项目》总投资金额
    $data['attract_pirce'] = explode(',', $data['attract_pirce']); // 重大项目》项目融资》总引资金额

    $data['deal_class'] = explode(',', $data['deal_class']); // 重大项目》资产交易》交易类别
    $data['deal_type'] = explode(',', $data['deal_type']); // 重大项目》资产交易》交易方式
    $data['value_price'] = explode(',', $data['value_price']); // 重大项目》资产交易》估值价格
    $data['make_price'] = explode(',', $data['make_price']); // 重大项目》资产交易》转让价格

    $data['project_type'] = explode(',', $data['project_type']); // 重大项目》国家战略项目》项目类型

    
    // @todo: 2018/11/13 修正
    $data['industryid'] = explode(',', $data['industryid']); // 产业类别
    $data['cityid'] = explode(',', $data['cityid']); // 所在城市
    $data['typeid'] = explode(',', $data['typeid']);
    $data['be_induid'] = explode(',', $data['be_induid']);

    return $data;
}
function cms_article_field_content_filter_string_helper($data) {
    $text = [];
    if(is_array($data)){
        foreach ($data as $key => $val) {
            if (empty($val)) continue;
            $text[$key] = $val;
        }
    }else{
        $text = $data;
    }
    return $text;
}


/** 查询v1_model中所有已有的模型，并按需求返回格式
 * @return array
 */
function model_all(){
    //查询已有模型
    $table_where = [
        'disabled' => 0 //状态
    ];
    $table_arr = M('Model')->field('`table`,name')->where($table_where)->select();
    $mould_type = [];
    foreach($table_arr as $v){
        $mould_type['name'][$v['table']] = $v['name'];
        $mould_type['table'][] = $v['table'];
    }
    return $mould_type;
}


// @todo: 以下为自定义的函数，主要操作联动菜单（取值）
function cms_get_linkage_value($data, $all = true) {
    $array = explode(',', $data);
    $value = '';
    if($all){
        foreach ($array as $key => $linkageid) {
            $ress = D('Admin/Linkage')->where(['linkageid' => $linkageid])->find();
            $value .= $ress['name'];
            $value .= count($array) == $key ?: ' ';
        }
    }else{
        $ress = D('Admin/Linkage')->where(['linkageid' => $array[count($array)-1]])->find();
        $value = $ress['name'];
//        $value .= count($array) == $key ?: ' ';
    }

    return trim($value);
}

/**
 * 处理总引资单位换算(万 to 亿)(当前用于政府招商页面中的“总引资额”)
 */
function cms_handle_allyinzi($string){
    if(strpos($string, "—")){
        $array = explode("—",$string);
        $nums = array();

        foreach ($array as $k => $v) {
            if(strpos($v, '万')){
                $nums[$k] = explode("万", $v)[0] / 10000;
            }elseif(strpos($v, '亿')){
                $nums[$k] = explode("亿", $v)[0];
            }
        }
        return implode("-",$nums);
    }else if(strpos($string, "不限")){
        return 0;
    }else if(strpos($string, "亿以上")){
        return explode("亿", $string)[0];
    }else{
        return (float)$string/10000;
    }
}


/**
 * 判断请求来自移动端还是pc端
 */
function checkmobile() {
    $mobile = array();
    static $mobilebrowser_list =array('iphone', 'android', 'phone', 'mobile', 'wap', 'netfront', 'java', 'opera mobi', 'opera mini',
        'ucweb', 'windows ce', 'symbian', 'series', 'webos', 'sony', 'blackberry', 'dopod', 'nokia', 'samsung',
        'palmsource', 'xda', 'pieplus', 'meizu', 'midp', 'cldc', 'motorola', 'foma', 'docomo', 'up.browser',
        'up.link', 'blazer', 'helio', 'hosin', 'huawei', 'novarra', 'coolpad', 'webos', 'techfaith', 'palmsource',
        'alcatel', 'amoi', 'ktouch', 'nexian', 'ericsson', 'philips', 'sagem', 'wellcom', 'bunjalloo', 'maui', 'smartphone',
        'iemobile', 'spice', 'bird', 'zte-', 'longcos', 'pantech', 'gionee', 'portalmmm', 'jig browser', 'hiptop',
        'benq', 'haier', '^lct', '320x320', '240x320', '176x220');
    $useragent = strtolower($_SERVER['HTTP_USER_AGENT']);
    if( dstrpos($useragent, $mobilebrowser_list, true)) {
        return true;
    }
    return false;
}

/**
 * 判断请求来自移动端还是pc端  辅助方法
 */
function dstrpos($string, &$arr, $returnvalue = false) {
    if(empty($string)) return false;
    foreach((array)$arr as $v) {
        if(strpos($string, $v) !== false) {
            $return = $returnvalue ? $v : true;
            return $return;
        }
    }
    return false;
}

/**
 * 单位换算  万 => 亿  保留2微小数
 */
function unit_conversion($num){
    return  (float)$num/10000;
}

/**
 * 截取指定长度的字符串，多余的用省略号代替
 * @param $text
 * @param $length
 * @return string
 */
function subtext($text, $length)
{
    if (mb_strlen($text, 'utf8') > $length) {
        return mb_substr($text, 0, $length, 'utf8') . '......';
    } else {
        return $text;
    }

}

/**
 * 一周内、一月内、一季度内、半年内、一年内、一年以上
 * 返回查询数组
 */
function switchtime($tpye){
    $str = array();
    $time = time(); //当前时间的时间戳
    switch ($tpye){
        case "inoneweek":
            // 一周内
            return ['egt', strtotime('-7 days')];
            break;
        case "inonemonth";
            // 一月内
            return ['egt', strtotime('-1 month')];
            break;
        case "inonequarter";
            //一季度内
            return ['egt', strtotime('-1 month')];
            break;
        case "inhalfyear";
            //半年内
            return ['egt', strtotime('-6 month')];
            break;
        case "inoneyear";
            //一年内
            return ['egt', strtotime('-1 year')];
            break;
        case "overoneyear";
            return ['lt', strtotime('-1 year')];
            break;
        default;
    }
}


/**
 * 获取事业单位名称
 */
function get_unit_name($unitid){
    if(strstr($unitid, ',' ) !== false){
        $array = explode(',', $unitid);
        $value = '';
        $ress = D('Admin/Institution')->where(['unitid' => $array[count($array)-1]])->find();
        $value = $ress['name'];

    }else{
        $ress = D('Admin/Institution')->where(['unitid' => $unitid])->find();
        $p = D('Admin/linkage')->where(['linkageid'=>$ress['linkageid']])->find();

        if($p['parentid'] != 1){
            $p1 = D('Admin/linkage')->where(['linkageid'=>$p['parentid']])->find();
            $p1_value = $p1['name'];
        }
        $value = $p1_value.' '.$p['name'].' '.$ress['name'];
    }


    return trim($value);
}

function getRegionName($id){

    return D("Admin/linkage")->where(['linkageid'=>$id, "display"=>1])->find();

}

