<?php

namespace Common\Controller;

use \Admin\Controller\BaseController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块助手控制器类：配置或实例化系统模块
 * 
 * @author T-01
 */
abstract class iBaseController extends AbstractController {
    
    public      $action         = [];
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\AbstractController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
        
        session('CMS_ActionArray', $this->action);
        BaseController::_initialize(); // @todo: 管理模块基础控制器初始化方法
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Controller::__destruct()
     */
    public function __destruct() {
        session('CMS_ActionArray', NULL); // @todo: 清除
        session('CMS_Dialog2', NULL);
    }
    
}
