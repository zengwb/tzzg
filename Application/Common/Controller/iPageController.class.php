<?php

namespace Common\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块页面控制器类：配置或实例化系统模块
 * 
 * @author T-01
 */
abstract class iPageController extends AbstractController {
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\AbstractController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
        
        $where              = [];
        $where['domainarr'] = ['LIKE', '%'.$_SERVER['SERVER_NAME'].'%'];
        $where['domain']    = ['LIKE', '%'.$_SERVER['SERVER_NAME'].'%'];
        $where['display']   =  1;
        
        $ress = D('Admin/Website')->where($where)->find();
        $tips = '当前站点被系统关闭或找不到！';
        if (!$ress) $this->error($tips, '', 'javascript:void(0);', 9999);
        
        session('CMS_SiteID', $ress['siteid']);
    }
    
}
