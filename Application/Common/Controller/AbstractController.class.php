<?php

namespace Common\Controller;

use \Think\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块抽象控制器类：配置或实例化系统模块
 * 
 * @author T-01
 */
abstract class AbstractController extends Controller {
    
    /**
     * 初始化系统基础配置方法，检查域名配置、站点环境等
     */
    public function _initialize() {
        if (file_exists(RUNTIME_PATH.'install.lock')) {
        } else {
            header('location: index.php?m=install&c=index&a=install');
            exit();
        }
        $where = ['module' => MODULE_NAME];
        $array = D('Admin/Module')->where($where)->find();
        
        if ($array) {
        } else {
            $tips = '模块『'.MODULE_NAME.'』未安装，请联系管理员！';
            $link = 'javascript:void(0);';
            $this->error($tips, '', $link, 1);
        }
        if ($array['disabled']) {
            $tips = $array['iscore'] ? '核心' : '扩展';
            $tips.= '模块『'.$array['name'].'』已被禁用！';
            $link = 'javascript:void(0);';
            $this->error($tips, '', $link, 1);
        }
    }
    
    /**
     * 操作事件提示跳转方法（CMS自定义扩展方法）
     * 
     * @param string    $state      选填。操作状态
     * @param string    $tips       选填。写入内容
     * @param string    $data       选填。数据参数
     * @param string    $jump       选填。跳转地址
     * @param boolean|integer $ajax 选填。是否为Ajax方法，当数字时指定跳转时间
     * @param boolean   $write      选填。写入日志
     * @param integer   $type       选填。日志类型
     * 
     * {@inheritDoc}
     * @see \Think\Controller::success()
     * @see \Think\Controller::error()
     */
    protected final function popup($state = true, $tips = '', $data = '',
        $jump = '', $ajax = false, $write = true, $type = 2) {
        
        $tips .= $state ? '成功！' : '失败！';
        if ($write) self::writelog($tips, $data,  $type);
        
        C('TMPL_ACTION_ERROR',   C('TMPL_ACTION_POPUP'));
        C('TMPL_ACTION_SUCCESS', C('TMPL_ACTION_POPUP'));
        
        if ($state) parent::success($tips, $jump, $ajax);
        else parent::error( $tips, $jump, $ajax); exit();
    }
    
    /**
     * @param string    $tips       选填。写入内容
     * @param string    $data       选填。数据参数
     * @param string    $jump       选填。跳转地址
     * @param boolean|integer $ajax 选填。是否为Ajax方法，当数字时指定跳转时间
     * @param boolean   $write      选填。写入日志
     * @param integer   $type       选填。日志类型
     * 
     * {@inheritDoc}
     * @see \Think\Controller::success()
     */
    protected final function success($tips = '', $data = '',
        $jump ='', $ajax = false, $write = true, $type = 2) {
        
        if ($write)  self::writelog($tips, $data, $type);
        parent::success($tips, $jump, $ajax); exit();
    }
    
    /**
     * @param string    $tips       选填。写入内容
     * @param string    $data       选填。数据参数
     * @param string    $jump       选填。跳转地址
     * @param boolean|integer $ajax 选填。是否为Ajax方法，当数字时指定跳转时间
     * @param boolean   $write      选填。写入日志
     * @param integer   $type       选填。日志类型
     * 
     * {@inheritDoc}
     * @see \Think\Controller::error()
     */
    protected final function error($tips = '', $data = '',
        $jump='', $ajax= false, $write = true, $type = 2) {
        
        if ($write)  self::writelog($tips, $data, $type);
        parent::error($tips, $jump, $ajax); exit();
    }
    
    /**
     * @param string    $tpl        选填。模板文件
     * @param string    $charset    选填。输出编码
     * @param string    $type       选填。输出类型
     * @param string    $content    选填。输出内容
     * @param string    $prefix     选填。缓存前缀
     * 
     * {@inheritDoc}
     * @see \Think\Controller::display()
     */
    protected final function display($tpl = '', $charset = '', $type = '',
        $content = '', $prefix = '') {
        
        $tpl = $tpl ? : ACTION_NAME;
        $tpl = ucwords($tpl);
        
        parent::display($tpl, $charset, $type, $content, $prefix);
    }
    
    /**
     * 写入日志
     * 
     * @param string    $tips       选填。提示信息
     * @param string    $text       选填。内容参数
     * @param integer   $type       选填。日志类型（1登录，2操作，3异常）
     */
    protected final function writelog($tips = '', $text = '', $type = 2) {
        cms_writelog($tips, $text, $type);
    }
    
    /**
     * 设置系统权限（操作事件）
     * 
     * @param array     $array      选填。操作事件（数组）
     */
    protected final function action($array = []) {
        session('CMS_ActionArray2', $array ? $array : []) ;
    }
    
    /**
     * 设置系统模块弹出容器标识（名称），管理部分会经常用到该方法
     * 
     * @param string    $label      选填。标识（名称）
     */
    protected final function dialog($label = ACTION_NAME) {
        session('CMS_Dialog2', $label ? ucwords($label) : 'CMS');
    }
    
}
