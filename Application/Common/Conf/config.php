<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [

    'APP_DOMAIN_SUFFIX'     => '',          // 部署项目主域名
    'APP_SUB_DOMAIN_DEPLOY' => FALSE,       // 开启子域名部署
    'APP_SUB_DOMAIN_RULES'  => [],
    'APP_SUB_DOMAIN_DENY'   => [],          // 子域名禁用列表

    'ADMIN_MODULE'          => 'Admin',     // 指定管理模块
    'ADMIN_CONTROLLER'      => 'Index',     // 指定管理控制器

    'SUPER_ROLEID'          => [101],       // 超级管理角色（用户组）ID
    'SUPER_USERID'          => [101],       // 超级管理员（用户）ID

    'TMPL_ACTION_SUCCESS'   => RES_PATH.'Apps/View/prompt.html',
    'TMPL_ACTION_POPUP'     => RES_PATH.'Apps/View/popup.html',
    'TMPL_ACTION_ERROR'     => RES_PATH.'Apps/View/prompt.html',

    'URL_CASE_INSENSITIVE'  => TRUE,        // URL不区分大小写
    'URL_MODEL'             =>  0,          // URL访问模式
    'URL_HTML_SUFFIX'       => 'shtml',     // URL伪静态后缀

    'DEFAULT_CHARSET'       => 'utf-8',     // 默认输出编码
    'DEFAULT_MODULE'        => 'Home',      // 默认模块
    'DEFAULT_FILTER'        => 'htmlspecialchars',

    'SHOW_PAGE_TRACE'       => APP_DEBUG ? TRUE : FALSE, // TRACE
    'TMPL_PARSE_STRING'     => [
        '__ROOT__'          => CMS_ROOT,
        '__PLUG__'          => CMS_RESS.'Plug-in/',
        '__SKIN__'          => CMS_SKIN,
        '__APPS__'          => CMS_RESS.'Apps/',
    ],
    'TMPL_TEMPLATE_SUFFIX'  => 'View.html', // 模板后缀（自定）
    'TMPL_FILE_DEPR'        => NULL,        // 模板分割符

    'TMPL_EXCEPTION_FILE'   => RES_PATH.'Apps/View/exception.html',
    'TMPL_CACHE_ON'         => FALSE,

    'SESSION_OPTIONS'       => [
        'expire'            => 3600 * 24
    ],
    'LOAD_EXT_CONFIG'       => 'database,version,helper',

    'PAGES_NUMBER'          => 24,          // 列表分页
    'CMS_ADMIN_HELPER'      => 'index.php?m=helper&c=index&a=search',

    'TAGLIB_LOAD'           => TRUE,
    'TAGLIB_PRE_LOAD'       => 'Common\Tags\CmsTags',
    'TAGLIB_BUILD_IN'       => 'Common\Tags\CmsTags,cx',

    'FILE_UPLOAD_TYPE'      => 'Local',     // 文件上传方式，Local/Ftp
    'UPLOAD_TYPE_CONFIG'    => [
        'host'              => '127.0.0.1', // FTP服务器
        'port'              => 21,          // FTP端口
        'username'          => 'yhcms',     // FTP帐号
        'password'          => '123',       // FTP密码
        'timeout'           => 90,          // 超时时间
    ],
    'DATA_CRYPT_TYPE'       => 'MjCMS',     // 数据加密方式

    'CMS_ATTACH_PATH'       => './Temporary/Attachment/',
    'CMS_BACKUP_PATH'       => './Temporary/Backup/',

    'CMS_WEBDIR_PATH'       => dirname(APP_PATH).'/Bootstrap/Webroots/',

    /**短信配置--start**/
    'YM_SMS_ADDR'                       => "http://bjmtn.b2m.cn:80",        /*接口地址,请联系销售获取*/
    'YM_SMS_SEND_URI'                   => "/inter/sendSingleSMS",      /*发送单条短信接口*/
    'YM_SMS_SEND_BATCH_URI'             => "/inter/sendBatchSMS",       /*发送批次短信接口*/
    'YM_SMS_SEND_BATCHONLY_SMS_URI'     => "/inter/sendBatchOnlySMS",   /*发送批次[不支持自定义smsid]短信接口*/
    'YM_SMS_SEND_PERSONALITY_SMS_URI'   => "/inter/sendPersonalitySMS", /*发送个性短信接口*/
    'YM_SMS_GETREPORT_URI'              => "/inter/getReport",          /*获取状态报告接口*/
    'YM_SMS_GETMO_URI'                  => "/inter/getMo",              /*获取上行接口*/
    'YM_SMS_GETBALANCE_URI'             => "/inter/getBalance",         /*获取余额接口*/
    'YM_SMS_APPID'                      => "EUCP-EMY-SMS0-KCRST",       /*APPID,请联系销售或者在页面获取*/
    'YM_SMS_AESPWD'                     => "4069550320570869",          /*密钥，请联系销售或者在页面获取*/
    'EN_GZIP'                           => true,                        /*是否开启GZIP*/
    'END'                               => "\n",
    /**短信配--end**/

];

