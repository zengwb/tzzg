<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    'CMS_VERSION'           => '1.0.01',    // 系统版本
    'CMS_RELEASE'           => '181112',    // 发行时间
    'CMS_COMPILE'           => preg_replace('/\D/', NULL, '$Rev: 8 $'),
    'CMS_CHARSET'           => 'UTF8',      // 字符编码
	
    'CMS_CMSNAME'           => '英煌CMS内容管理系统（YhCMS）',
    'CMS_LICENSE'           => '北京英煌科技有限公司',
    'CMS_CMSCODE'           => 'YH17-P001-1212-'.cms_random(4),
];
