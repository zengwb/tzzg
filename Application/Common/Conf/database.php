<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'DB_DEPLOY_TYPE'        =>  0,          // 部署方式：0集中式，1分布式
    // 分布式部署：数据库类型务必相同
    // 服务器地址、数据库名、用户帐户、用户密码、服务端口，以英文状态下逗号分隔
    'DB_RW_SEPARATE'        => FALSE,       // 数据读写分离（分布式有效）
    
    'DB_MASTER_NUM'         =>  1,          // 主服务器数量
    'DB_SLAVE_NO'           => '',          // 指定从服务器序号
    'DB_SUBMETER'           => FALSE,       // 数据分表？
    
    'DB_DEBUG'              => FALSE,       // 调试模式，开启后可以记录SQL日志
    'DB_FIELDS_CACHE'       => FALSE,		// 启用字段缓存
    
    'DB_TYPE'               => 'mysql',     // 数据库类型
    'DB_HOST'               => '192.168.101.109', // '112.74.185.184', // 服务器地址
    'DB_PORT'               => '3306',      // 数据服务器端口
    
    'DB_NAME'               => 'tzzg2018',     // 数据库名
    'DB_USER'               => 'root',     // 用户帐户
    'DB_PWD'                => '123', // 'tzzg201829', // 用户密码

    'DB_PREFIX'             => 'v1_',       // 数据表前缀
    'DB_PARAMS'             => [],          // 数据库连接参数
    'DB_CHARSET'            => 'utf8',      // 数据库编码默认采用 UTF8
    
];
