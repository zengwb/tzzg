<?php

namespace Common\ORG;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块操作权限控制类
 * 
 * @author T-01
 */
final class OAuth2 {
    
    private     $userName2  = '',
                
                $menuModel  = [],
                $roleModel  = [],
                
                $userModel  = [],
                $privModel  = [];
    
    public final function __construct() { $this->_initialize(); }
    
    public final function _initialize($username) {
        $this->menuModel    = D('Admin/Menu');
        $this->roleModel    = D('Admin/AdminRole');
        
        $this->userModel    = D('Admin/Admin');
        $this->privModel    = D('Admin/AdminRolePriv');
        
        $this->userName2    = $username;
    }
    
    /**
     * 操作事件验证
     * 
     * @param array     $action     必填。操作事件数组
     * 
     * @return array
     */
    public final function action($action) {
        $where              = [];
//         $where['menuid']    = ['IN', $this->operPriv()];
        $where['module']    = MODULE_NAME;
        $where['controller']= CONTROLLER_NAME;
        $where['action']    = ['NEQ', ''];
        $where['method']    = ['NEQ', ''];
        $where['isattr']    =  1;
        $where['display']   =  1;
        
        $ress17 = $this->menuModel->where($where)->find();
        $method = explode(',', $ress17['method']);
        
        $module     = MODULE_NAME     == C('ADMIN_MODULE');
        $controller = CONTROLLER_NAME == C('ADMIN_CONTROLLER');
        $admin2     = $module && $controller;
        
        if ($admin2 || in_array(ACTION_NAME, $method)) {
            return [];
        }
        
        $this->userName2 = $this->userName2 ?: 'unknown';
        $tips = '当前操作『'.__ACTION__.'』没有权限。请联系管理员！';
        
        $data = [];
        $data['username']   = $this->userName2;
        $data['action']     = __ACTION__;
        
        return ['tips' => $tips, 'data' => $data];
    }
    
    /**
     * 获取操作权限（菜单ID）
     */
    public final function operPriv() {
        $where              = [];
        $where['username']  = $this->userName2;
        $where['disabled']  =  0;
        
        $userress = $this->userModel->where($where)->find();
        $settings = unserialize($userress['setting']);
        $userpriv = $settings['priv'];
        $rolepriv = $this->rolePriv($userress['roleid']);
        
        if (in_array($userress['roleid'], C('SUPER_ROLEID')))
            return $this->menuModel->childid(1001);
        else
            return $userpriv ? $userpriv : $rolepriv;
    }
    
    /**
     * 角色权限
     */
    private final function rolePriv($roleid) {
        $where              = [];
        $where['roleid']    = $roleid;
        
        $data = [];
        $ress = $this->privModel->where($where)->select();
        
        foreach ($ress as $key => $row) {
            $data[$key] = $row['menuid'];
        }
        if ($ress || !$roleid) return implode(',',$data);
        
        $where              = [];
        $where['roleid']    = $roleid;
        $where['disabled']  =  0;
        
        $ress2 = $this->roleModel->where($where)->find();
        return $this->rolePriv($ress2['parentid']);
    }
    
}
