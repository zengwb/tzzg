<?php

namespace Common\ORG;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块树形结构扩展类：定义管理/联动菜单树形方法
 * 
 * @author T-01
 */
final class Tree {
    
    public  $treeSpace      = '　',
            $treePrefix     = ['│ ','├ ','└ '],
            
            $treeField      = ['id', 'name', 'parentid'];
            
    private $treeReturn     = '',
            $treeArray      = [];
    
    /**
     * 初始化树形结构扩展类属性，判断并获取数据或返回初始状态
     * 
     * @param array     $array      选填。树形结构数据（数组）
     * 
     * @return boolean
     */
    public final function initialize($array = []) {
        $this->treeArray = is_array($array) ? $array : [];
        $this->treeReturn = null;
        return is_array($array);
    }
    
    /**
     * 获取父级树形结构数据（数组格式）
     * 
     * @param integer   $id         必填。当前ID
     * 
     * @return boolean|array[]
     */
    public final function parentid($id) {
        $array = [];
        if (!isset($this->treeArray[$id])) return false;
        $id = $this->treeArray[$id][$this->treeField[2]];
        
        if (!isset($this->treeArray[$id])) return false;
        $id = $this->treeArray[$id][$this->treeField[2]];
        
        if (is_array($this->treeArray)) {
            foreach ($this->treeArray as $k => $v) {
                if ($v[$this->treeField[2]] == $id) $array[$k] = $v;
            }
        }
        return $array;
    }
    
    /**
     * 获取子级树形结构数据（数组格式）
     * 
     * @param integer   $id         必填。当前ID
     * 
     * @return array[]
     */
    public final function childid($id) {
        $array = [];
        if (is_array($this->treeArray)) {
            foreach ($this->treeArray as $k => $v) {
                if ($v[$this->treeField[2]] == $id) $array[$k] = $v;
            }
        }
        return $array;
    }
    
    /**
     * 建立树形结构方法
     * 
     * @param integer   $id         必填。当前ID
     * @param string    $text       必填。输出内容
     * @param string    $adds       选填。修饰字符
     * @param array     $array      选填。选中记录
     * 
     * @return string
     */
    public final function build($id, $text, $adds = '', $array = []) {
        $count = 1;
        $child = $this->childid($id);

        if (!is_array($child)) return $this->treeReturn;
        if (!is_array($array)) $array = [$array];

        foreach ($child as $key => $val) {
            $j  = $k = '';
            if ($count == count($child)) {
            $j .= $this->treePrefix[2];
            $k  = $adds ? $this->treeSpace     : '';
            } else {
            $j .= $this->treePrefix[1];
            $k  = $adds ? $this->treePrefix[0] : '';
            }
            if (in_array($val[$this->treeField[0]], $array)) {
            $checked  = 'checked';
            $selected = 'selected';
            } else {
            $checked  = '';
            $selected = '';
            }
            $spacer = $adds ? $adds.$j.$this->treeSpace : '';
            
           @extract($val);
            eval("\$return = \"$text\";");
            $this->treeReturn  .= $return;
            
            $space   = $this->treeSpace;
            $id = $val[$this->treeField[0]];

            $this->build($id, $text, $adds.$k.$space, $array);
            $count++;
        }

        return $this->treeReturn;
    }
    
}
