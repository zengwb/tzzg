<?php

namespace Common\Widget;

use \Common\Controller\AbstractController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块助手组件类：配置或实例化应用组件
 * 
 * @author T-01
 */
abstract class iHelperWidget extends AbstractController {
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iHelperController::_initialize()
     */
    public function _initialize() { parent::_initialize(); }
    
}
