<?php

namespace Common\Widget;

use Admin\Model\LinkageModel;
use Admin\Model\InstitutionModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块表单组件类：自定义表单元素方法
 * 
 * @author T-01
 */
final class FormsWidget extends iHelperWidget {
    
    // 预定义表单元素
    public  $elements           =  [
        'text'                  => '单行文本',
        'textarea'              => '多行文本',
        'editor'                => '编辑器',
        'catid'                 => '栏目',
        'title'                 => '标题',
        'box'                   => '选项',
        'image'                 => '图片',
        'images'                => '图组',
        'number'                => '数字',
        'datetime'              => '日期和时间',
        'posiid'                => '推荐位',
        'keyword'               => '关键字',
        'author'                => '作者',
        'copyfrom'              => '来源',
        'groupid'               => '会员组',
        'islink'                => '转向链接',
        'template'              => '模板',
        'pages'                 => '分页选择',
        'typeid'                => '类别',
        'readpoint'             => '积分 、点数',
        'linkage'               => '联动菜单',
        'subset'                => '联动子集',
        'downfile'              => '镜像下载',
        'upfiles'               => '多文件上传',
        'map'                   => '地图字段',
        'omnipotent'            => '万能字段',
        'video'                 => '视频库',
        'relation'              => '相关内容',
        'units'                 => '共享单位',
        'company' => '企业',
    ];
    
    /**
     * {@inheritDoc}
     * @see \Common\Widget\iHelperWidget::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    /**
     * 表单元素配置方法
     * 
     * @param string    $label      必填。元素标签
     * 
     * @example W('Common/Forms/element', ['label' => 'text'])
     */
    public final function element($label) {
        $view = T('Common@Form/'.ucwords($label).'/');
        if (file_exists($view)) $this->display($view);
    }
    
    /**
     * 自定义单行文本元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function text($args) {
        $setting = unserialize($args['setting']);
        
        $name  = $this->field($args);
        $type  = $setting['ispassword'] ? 'password' : 'text';
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        if ($args['description']) {
            $description = '请输入'.$args['description'].'！';
        } else {
            $description = '';
        }
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="'  .$args['field'].'" ';
        $html .= 'type="'.$type.'" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size. '" ';
        $html .= 'placeholder="'.$description.'" '.$required.' />';
        
        return $html;
    }
    
    /**
     * 自定义多行文本元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function textarea($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $pregr = preg_replace("/<([a-zA-Z]+)[^>]*>/", "<\\1>", $value);
        $value = $setting['allowhtml'] ? $pregr : $value;
        
        if ($args['description']) {
            $description = '请输入'.$args['description'].'！';
        } else {
            $description = '';
        }
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<textarea id="'   .$args['field']. '" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="width:'.$setting['width'].'%; height:';
        $html .= $setting['height'].'px;  '.$args['style'].'" ';
        $html .= 'placeholder="' . $description . '" '.$required.'>';
        $html .= $value.'</textarea>';
        
        return $html;
    }
    
    /**
     * 自定义编辑器元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function editor($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        $value = cms_stripslashes($value);
        
        if ($setting['toolbar'] == 'basic') {
        $toolbars = "
        'fullscreen', 'source', '|', 'undo', 'redo', '|',
        'bold', 'italic', 'underline', 'fontborder', 'strikethrough',
        'superscript', 'subscript', 'removeformat',
        'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
        'forecolor', 'backcolor', 'insertorderedlist',
        'insertunorderedlist', 'selectall', 'cleardoc', '|',
        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
        'paragraph', 'fontfamily', 'fontsize', '|',
        'justifyleft', 'justifycenter', 'justifyright',
        'justifyjustify', '|',
        'link', 'unlink', 'anchor', '|', 'imagenone', '|',
        'simpleupload', 'insertimage', 'emotion', 'scrawl',
        'insertvideo', 'music', 'attachment', 'map', 'gmap',
        'insertcode', 'pagebreak', 'background', '|',
        'horizontal', 'date', 'time', 'spechars', '|', 'help'
        ";
        } else {
        $toolbars = "";
        }
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="' .$args['field'].'" type="hidden" ';
        $html .= 'name="'.$name.'['.$args['field'].']" value="" />';
        
        $html .= '<script id="'.$args['field'].'_editor" type="text/plain" ';
        $html .= 'style="width:100%; height:'.$setting['height'].'px;">';
        $html .= htmlspecialchars_decode($value);
        $html .= '</script>';
        
        $html .= '<script type="text/javascript">';
        if ($toolbars) {
        $html .= 'var ue_'.$args['field'].' = ';
        $html .= 'UE.getEditor("'.$args['field'].'_editor", ';
        $html .= '{toolbars: [['.$toolbars.']]});';
        } else {
        $html .= 'var ue_'.$args['field'].' = ';
        $html .= 'UE.getEditor("'.$args['field'].'_editor");';
        }
        $html .= '$("form").submit(function() { ';
        $html .= '$("#'.$args['field'].'").val(ue_content.getContent());';
        $html .= '});';
        $html .= '</script>';
        
        return $html;
    }
    
    /**
     * 自定义栏目元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function catid($args) {
        $setting =  unserialize($args['setting']);
        $name  = $this->field($args);
        
        if (isset($setting['param']) && !empty($setting['param'])) {
            $catid = $_GET[$setting['param']];
        } else {
            $catid = isset($args['value']) ?: $setting['default'];
        }
        $where = [];
        $where['catid'] = $catid;
        $ress2 = D('Admin/Category')->where($where)->find();
        
        $html  = '';
        $html .= '<span id="catid">'.$ress2['catname'].'</span>';
//         $html .= '<span id="catid">栏目</span>';
        
        $html .= '<input type="hidden" name="'.$name;
        $html .= '['.$args['field'].']" value="'.$catid.'" />';
        
        return $html;
    }
    
    /**
     * 自定义标题元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function title($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $ttcss = ['color' => '', 'bold' => ''];
        $style = $args['ttcss'] ? unserialize($args['ttcss']) : $ttcss;
        
        if ($args['description']) {
            $description = '请输入'.$args['description'].'！';
        } else {
            $description = '';
        }
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="' .$args['field'].'" type="text" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size. '" ';
        $html .= 'placeholder="'.$description.'" '.$required.' />';
        
        // @todo: 标题样式
        $html .= '<input type="hidden" ';
        $html .= 'id="style_'.$args['field'].'_color" ';
        $html .= 'name="style['.$args['field'].'_style][color]" ';
        $html .= 'value="'.$style['color'].'" />';
        $html .= '<input type="hidden" ';
        $html .= 'id="style_'.$args['field'].'_font_weight" ';
        $html .= 'name="style['.$args['field'].'_style][bold]" ';
        $html .= 'value="'.$style['bold'].'" />';
        
        $html .= '<div style="float:left; position:relative; ';
        $html .= 'line-height:26px;">';
        
        $html .= '<i class="icon-style icon-style-lists" ';
        $html .= 'onClick="';
        $html .= 'colorpicker(\''.$args['field'].'_color_panel\', ';
        $html .= '\'set_'.$args['field'].'_color\');" ';
        $html .= 'style="margin:5px 6px 0px 6px; cursor:pointer;"></i>';
        
        $html .= '<i class="icon-style icon-style-bold" ';
        $html .= 'onClick="set_'.$args['field'].'_font_bold();" ';
        $html .= 'style="margin:5px 6px 0px 0px; cursor:pointer;"></i>';
        
        $html .= '<span>';
        $html .= '标题请控制在 '.($args['maxlength']/2).' 个汉字以内！';
        $html .= '</span>';
        
        $html .= '<span id="'.$args['field'].'_color_panel" ';
        $html .= 'style="position:absolute; top:-3px; left:20px; ';
        $html .= 'float:none; z-index:9999;" class="oper-color-panel">';
        $html .= '</span></div>';
        
        $html .= '<script type="text/javascript" language="javascript">';
        $html .= 'function set_'.$args['field'].'_color(color) {';
        $html .= '$(\'#'.$args['field'].'\').css(\'color\', color);';
        $html .= '$(\'#style_'.$args['field'].'_color\').val(color);';
        $html .= '}';
        $html .= 'function set_'.$args['field'].'_font_bold() {';
        $html .= 'if ($(\'#'.$args['field'].'\').css(\'font-weight\') ';
        $html .= '== \'700\' || $(\'#catname\').css(\'font-weight\') ';
        $html .= '== \'bold\') {';
        $html .= '$(\'#'.$args['field'].'\').css(\'font-weight\', ';
        $html .= '\'normal\');';
        $html .= '$(\'#style_'.$args['field'].'_font_weight\').val(\'\');';
        $html .= '} else {';
        $html .= '$(\'#'.$args['field'].'\').css(\'font-weight\', ';
        $html .= '\'bold\');';
        $html .= '$(\'#style_'.$args['field'].'_font_weight\').val(';
        $html .= '\'bold\');';
        $html .= '}';
        $html .= '}';
        $html .= '$(function() {';
        $html .= '$(\'#'.$args['field'].'\').css(\'font-weight\', \'';
        $html .= $style['bold'].'\');';
        $html .= '$(\'#'.$args['field'].'\').css(\'color\', \'';
        $html .= $style['color'].'\');';
        $html .= '});';
        $html .= '</script>';
        
        return $html;
    }
    
    /**
     * 自定义选项元素（单选/复选，下拉单选/多选）
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function box($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $options = explode("\n", $setting['options']);
        
        $html  = '';
        switch ($setting['boxtype']) {
        case 'radio':
        $html .= '<div class="radio input-sm">';
        
        foreach ($options as $key => $option) {
        if (empty($option)) continue;
        list($keys, $vals) = explode('|', $option);
        
        $vals  = $setting['outputtype'] ? $vals : $keys;
        $vals  = trim($vals);
        $checked = $value == $vals ? 'checked' : '';
        
        $html .= '<label>';
        $html .= '<input type="radio" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'value="'.$vals.'" '.$checked.' />';
        $html .= $keys.'</label>';
        }
        $html .= '</div>';
        break;
        
        case 'checkbox':
        $html .= '<div class="checkbox input-sm">';
        
        foreach ($options as $key => $option) {
        if (empty($option)) continue;
        list($keys, $vals) = explode('|', $option);
        if (!is_array($value)) $array = explode(',', $value);
        else $array = $value;
        
        $vals  = $setting['outputtype'] ? $vals : $keys;
        $vals  = trim($vals);
        $checked = in_array($vals, $array) ? 'checked' : '';
        
        $html .= '<label>';
        $html .= '<input type="checkbox" ';
        $html .= 'name="'.$name.'['.$args['field'].'][]" ';
        $html .= 'value="'.$vals.'" '.$checked.' />';
        $html .= $keys.'</label>';
        }
        $html .= '</div>';
        break;
        
        case 'select':
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'].'</label>';
        
        $html .= '<select id="'.$args['field'].'" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'name="'.$name.'['.$args['field'].']">';
        
        foreach ($options as $key => $option) {
        if (empty($option)) continue;
        list($keys, $vals) = explode('|', $option);
        
        $vals  = $setting['outputtype'] ? $vals : $keys;
        $vals  = trim($vals);
        $selected = $value == $vals ? 'selected' : '';
        
        $html .= '<option value="'.$vals.'" '.$selected.' >';
        $html .= $keys.'</option>';
        }
        $html .= '</select>';
        $html .= '';
        break;
        
        case 'multiple':
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'].'</label>';
        
        $html .= '<select multiple id="'.$args['field'].'" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'name="'.$name.'['.$args['field'].'][]">';
        
        foreach ($options as $key => $option) {
        if (empty($option)) continue;
        list($keys, $vals) = explode('|', $option);
        $array = explode(',', $value);
        
        $vals  = $setting['outputtype'] ? $vals : $keys;
        $vals  = trim($vals);
        $selected = in_array($vals, $array) ? 'selected' : '';
        
        $html .= '<option value="'.$vals.'" '.$selected.' >';
        $html .= $keys.'</option>';
        }
        $html .= '</select>';
        break;
        }
        return $html;
    }
    
    /**
     * 自定义图片元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function image($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        list($width, $height) = explode(',', $setting['size']);
        $width = $width  ? : 120;
        $height= $height ? :  80;
        
        $width = $width  < 220 ? $width  : 220;
        $height= $height < 220 ? $height : 142;
        
        $nlbr  = "\n";
        $html  = '';
        
        $html .= '<script language="javascript" type="text/plain" ';
        $html .= 'id="'.$args['field'].'_ueditor_upload" ';
        $html .= 'style="height:5px; display:none;"></script>'.$nlbr;
        
        $html .= '<div id="'.$args['field'].'" class="cms-thumb-body">';
        $html .= '</div>'.$nlbr;
        
        $html .= '<a id="'.$args['field'].'btn" href="javascript:void(0);" ';
        $html .= 'onClick="cms_'.$args['field'].'();" ';
        $html .= 'class="cms-thumb">上传图片</a>'.$nlbr;
        
        $html .= '<input type="hidden" id="'.$args['field'].'val" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'value="'.$value.'" />'.$nlbr;
        
        $html .= '<script language="javascript" type="text/javascript">';
        $html .= $nlbr;
        
        if ($value) {
        $html .= '$("#'.$args['field'].'").show();'.$nlbr;
        $html .= '$("#'.$args['field'].'btn").hide();'.$nlbr;
        
        $html .= 'var '.$args['field'].'val = "";'.$nlbr;
        $html .= $args['field'].'val += "<div>";'.$nlbr;
        
        $html .= $args['field'].'val += "<img src=\"'.$value.'\" ';
        $html .= 'alt=\"#\" />";'.$nlbr;
        
        $html .= $args['field'].'val += "<a href=\"javascript:cms_del';
        $html .= $args['field'].'(\''.$value.'\')\" ';
        $html .= 'alt=\"清除\">清除</a>";'.$nlbr;
        $html .= $args['field'].'val += "</div>";'.$nlbr;
        
        $html .= '$("#'.$args['field'].'").html('.$args['field'].'val);';
        $html .= $nlbr;
        } else {
        $html .= '$("#'.$args['field'].'").hide();'.$nlbr;
        $html .= '$("#'.$args['field'].'btn").show();'.$nlbr;
        }
        $html .= 'var '.$args['field'].'_ueditor_upload = ';
        $html .= 'UE.getEditor("'.$args['field'].'_ueditor_upload", ';
        $html .= '{autoHeightEnabled: false});'.$nlbr;
        
        $html .= $args['field'].'_ueditor_upload.ready(function() {'.$nlbr;
        $html .= $args['field'].'_ueditor_upload.hide();'.$nlbr;
        
        $html .= $args['field'].'_ueditor_upload.addListener("';
        $html .= 'beforeInsertImage", function(t, arg) {'.$nlbr;
        
        $html .= 'var imgv = "";'.$nlbr;
        $html .= '$.each(arg, function(i,row) {'.$nlbr;
        $html .= 'var imgs = "";'.$nlbr;
        
        $html .= 'imgs += "<div>";'.$nlbr;
        $html .= 'imgs += "<img src="+row.src+" alt="+row.alt+" />"'.$nlbr;
        $html .= 'imgs += "<a href=\"javascript:cms_del';
        $html .= $args['field'].'(\'"+row.src+"\');\" ';
        $html .= 'alt=\"清除\">清除</a>";'.$nlbr;
        $html .= 'imgs += "</div>";'.$nlbr;
        $html .= '$("#'.$args['field'].'").html(imgs); imgv = row.src;';
        $html .= $nlbr;
        
        $html .= '});'.$nlbr;
        $html .= '$("#'.$args['field'].'val").val(imgv);'.$nlbr;
        $html .= '$("#'.$args['field'].'").show();'.$nlbr;
        $html .= 'var thumb = $("#thumb"),'.$nlbr;
        
        $html .= 'thumbBoxH = thumb.find("div").outerHeight(true),'.$nlbr;
        $html .= 'thumbImgH = thumb.find("div>img").height();'.$nlbr;
        $html .= 'thumb.find("div>img").';
        $html .= 'css("margin-top", 0);'.$nlbr;
        $html .= 'thumb.find("div>img").';
        $html .= 'css("margin-top", (thumbBoxH-thumbImgH-4)/2);'.$nlbr;
        
        $html .= 'thumbBoxW = thumb.find("div").outerWidth(true),'.$nlbr;
        $html .= 'thumbImgW = thumb.find("div>img").width();'.$nlbr;
        $html .= 'thumb.find("div>img").';
        $html .= 'css("margin-left", 0);'.$nlbr;
        $html .= 'thumb.find("div>img").';
        $html .= 'css("margin-left", (thumbBoxW-thumbImgW-4)/2);'.$nlbr;
        
        $html .= '$("#'.$args['field'].'btn").hide();'.$nlbr;
        $html .= '});'.$nlbr;
        $html .= '});'.$nlbr;
        
        $html .= 'function cms_'.$args['field'].'() {'.$nlbr;
        $html .= 'var '.$args['field'].' = '.$args['field'];
        $html .= '_ueditor_upload.';
        $html .= 'getDialog("insertimage");'.$nlbr;
        $html .= $args['field'].'.open();'.$nlbr;
        $html .= '}'.$nlbr;
        
        $html .= 'function cms_del'.$args['field'].'(src) {'.$nlbr;
        $html .= '$("#'.$args['field'].'").hide();';
        $html .= '$("#'.$args['field'].'btn").show();';
        $html .= '$("#'.$args['field'].'val").val(';
        $html .= '$("#'.$args['field'].'val").val().replace(src, ""));';
        $html .= $nlbr.'}'.$nlbr.'</script>'.$nlbr;
        return $html;
    }
    
    /**
     * 自定义图组元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function images($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        $array = explode(',', $value);
        
        $html  = <<<echo
<script language="javascript" type="text/plain" id="{$args['field']}_ueditor_upload" style="height:5px; display:none;">
</script>
<div id="{$args['field']}" class="cms-upimage-box"></div>
<a href="javascript:void(0);" onClick="cms_up{$args['field']}();" class="cms-upimage">上传图片</a>
<input type="hidden" id="{$args['field']}val" name="{$name}[{$args['field']}]" value="{$value}" />
<script language="javascript" type="text/javascript">
    var imgs  = "";
        
echo;
        foreach ($array as $key => $img) {
            if (empty($img)) continue;
            $html .= <<<echo
        imgs += "<div>";
        imgs += "<img src=\"{$img}\" alt=\"#\" />";
        imgs += "<a href=\"javascript:cms_del{$args['field']}('{$img}')\" alt=\"删除\"";
        imgs += "<i class=\"iconfont icon-qingchu\"></i></a>";
        imgs += "</div>";
        
echo;
        }
        $html .= <<<echo
        $("#{$args['field']}").append(imgs);
    
    var {$args['field']}_ueditor_upload = UE.getEditor("{$args['field']}_ueditor_upload", {autoHeightEnabled: false});
        
        {$args['field']}_ueditor_upload.ready(function() {
            {$args['field']}_ueditor_upload.hide();
            {$args['field']}_ueditor_upload.addListener("beforeInsertImage", function(t, arg) {
                var imgv = [];
                $.each(arg, function(i, row) {
                    var imgs  = "";
                        imgs += "<div>";
                        imgs += "<img src=\""+row.src+"\" alt=\""+row.alt+"\" />";
                        imgs += "<a href=\"javascript:cms_del{$args['field']}('"+row.src+"')\" alt=\"删除\"";
                        imgs += "<i class=\"iconfont icon-qingchu\"></i></a>";
                        imgs += "</div>";
                    $("#{$args['field']}").append(imgs);
                    imgv[i] = row.src;
                });
                $("#{$args['field']}val").val($("#{$args['field']}").val()+","+imgv);
                $("#{$args['field']}").show();
            });
        });
        function cms_up{$args['field']}() {
            var {$args['field']} = {$args['field']}_ueditor_upload.getDialog("insertimage");
            {$args['field']}.open();
        }
        function cms_del{$args['field']}(src) {
            $("#{$args['field']}").find("div").each(function() {
                var img = $(this).find("img").attr("src");
                if (src == img) $(this).remove();
            });
            $("#{$args['field']}val").val($("#{$args['field']}val").val().replace(src, ""));
        }
</script>
echo;
        return $html;
    }
    
    /**
     * 自定义数字元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function number($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $minrandom = $setting['minrandom']?:0;
        $maxrandom = $setting['maxrandom']?:0;
        $value = $value ?: rand($minrandom, $maxrandom);
        
        if ($args['description']) {
            $description = '请输入'.$args['description'].'！';
        } else {
            $description = '';
        }
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="'.$args['field'].'" ';
        $html .= 'type="type" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size.'" ';
        $html .= 'placeholder="'.$description.'" '.$required.' />';
        
        return $html;
    }
    
    /**
     * 自定义日期和时间元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function datetime($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $args['style'] = $args['style'] ?: 'float:left; width:186px;';
        
        if ($setting['datetype'] == 'datetime') {
            $fmts = 'yyyy-MM-dd HH:mm:ss';
        } else if ($setting['datetype'] == 'date') {
            $fmts = 'yyyy-MM-dd';
        }
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="'.$args['field'].'" type="text" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size.'" ';
        
        $html .= 'onClick="WdatePicker({dateFmt:\''.$fmts.'\'});" ';
        $html .= 'placeholder="请选择'.$args['name'].'！" '.$required.' />';
        
        $html .= '<script type="text/javascript" ';
        $html .= 'src="'.C('TMPL_PARSE_STRING')['__PLUG__'];
        $html .= 'datepicker/wdatepicker.js">';
        $html .= '</script>';
        
        return $html;
    }
    
    /**
     * 自定义推荐位元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function posiid($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        if (is_numeric($setting['module'])) {
            $module = $setting['module'];
        } else {
            $module = $_GET[$setting['module']];
        }
        if (is_numeric($setting['modelid'])) {
            $modelid= $setting['modelid'];
        } else {
            $modelid= $_GET[$setting['modelid']];
        }
        if (is_numeric($setting['catid'])) {
            $catid  = $setting['catid'];
        } else {
            $catid  = $_GET[$setting['catid']];
        }
        $module     = $module   ?: MODULE_NAME;
        $modelid    = $modelid  ?: $args['modelid'];
        $catid      = $catid    ?: $args['catid'];
        
        $where              = [];
        $where['module']    = $module;
        $where['modelid']   = ['IN', [0, $modelid]];
        $where['catid']     = ['IN', [0, $catid]];
        $where['status']    = 1;
        
        $array = D('Article/Position')->where($where)->order(
            '`listorder` ASC, `posiid` ASC'
        )->select();
        
        $value = '';
        foreach ($array as $key => $row) {
        if ($key < count($array) - 1) $n = "\n";
        else $n = '';
        $value .= $row['name'].'|'.$row['posiid'].$n;
        }
        $setting['boxtype'] = 'checkbox';
        $setting['options'] = $value;
        $setting['outputtype'] = 1;
        $setting['default'] = '';
        
        $args['setting'] = serialize($setting);
        return $this->box($args);
    }
    
    /**
     * 自定义关键字元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function keyword($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="' .$args['field'].'" type="text" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size. '" ';
        $html .= 'placeholder="请输入'.$args['name'].'！" '.$required.' />';
        
        $html .= '<span style="padding-left:8px;">';
        $html .= '多个关键字间以英文状态下逗号分隔！';
        $html .= '</span>';
        
        return $html;
    }
    
    /**
     * 自定义作者元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function author($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="'.$args['field'].'" type="text" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size. '" ';
        $html .= 'placeholder="请输入'.$args['name'].'！" '.$required.' />';
        
        // @todo: 来源选择（扩展）
        return $html;
    }
    
    /**
     * 自定义来源元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function copyfrom($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        $size  = $setting['length'] ? : '';
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $required = $args['minlength'] ? ' required' : '';
        
        $html  = '';
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'];
        $html .= '</label>';
        
        $html .= '<input id="'.$args['field'].'" type="text" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'value="'.$value.'" size="'.$size. '" ';
        $html .= 'placeholder="请输入'.$args['name'].'！" '.$required.' />';
        
        // @todo: 来源选择（扩展）
        return $html;
    }
    
    /**
     * 自定义模板元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function template($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $where              = [];
        $where['modelid']   = $args['modelid'];
        $model = D('Admin/Model')->where($where)->find();
        
        $setting = serialize($model['setting']);
        $default = $args['value'] ? : $setting['showtpl'];
        
        $array = cms_theme_views(
            RES_PATH.'Template/', cms_site_info()['theme'],
            MODULE_NAME, 'show'
        );
        $options = '';
        foreach ($array as $key => $val) {
        if ($key < count($array) - 1) $n = "\n";
        else $n = '';
        $options .= $val['name'].'（'.$val['file'].'）|';
        $options .= $val['file'].$n;
        }
        $setting = [];
        $setting['boxtype'] = 'select';
        $setting['options'] = $options;
        $setting['outputtype'] = 1;
        $setting['default'] = $default;
        
        $args['setting'] = serialize($setting);
        $args['value']   = $default;
        return $this->box($args);
    }
    
    /**
     * 自定义类别元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function typeid($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        if (empty($setting['module']))  $module  = '';
        else $module  = $_GET[$setting['module']];
        if (empty($setting['modelid'])) $modelid = '';
        else $modelid = $_GET[$setting['modelid']];
        if (empty($setting['catid']))   $catid   = '';
        else $catid   = $_GET[$setting['catid']];
        
        $where              = [];
        $where['module']    = $module;
        $where['modelid']   = ['IN', [0, $modelid]];
        $where['catid']     = ['IN', [0, $catid]];
        
        if (empty($module)) unset($where['module']);
        if (empty($modelid))unset($where['modelid']);
        if (empty($catid))  unset($where['catid']);
        
        $array = D('Article/Type')->where($where)->order(
            '`listorder` ASC, `typeid` ASC'
        )->select();
        
        $value = '';
        foreach ($array as $key => $row) {
        if ($key < count($array) - 1) $n = "\n";
        else $n = "";
        $value .= $row['name'].'|'.$row['typeid'].$n;
        }
        $setting['boxtype'] = 'radio';
        $setting['options'] = $value;
        $setting['outputtype'] = 1;
        $setting['default'] = '';
        
        $args['setting'] = serialize($setting);
        return $this->box($args);
    }
    
    /**
     * 联动菜单
     * 
     * @see 引用自定义选项元素
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function linkage($args) {
        $setting = unserialize($args['setting']);
        $menuid = $setting['menuid'];
        
        $linkageModel = new LinkageModel();
//         $linkageid = $linkageModel->childid($menuid);
        $linkageid = $linkageModel->thischildid($menuid);
        $linkageidarr = explode(',', $linkageid);
        array_shift($linkageidarr);
        
        $where = [];
        $where['linkageid'] = ['IN', $linkageidarr];
        $where['display'] = 1;
        
        $order = [];
        $order['listorder'] = 'ASC';
        $order['linkageid'] = 'ASC';
        
        $linkagearray = $linkageModel->where($where)->order($order)->select();
        foreach ($linkagearray as $key => $row) {
            $setting['options'] .= $row['name'].'|'.$row['linkageid']."\n";
        }
        $args['setting'] = serialize($setting);
        $html = $this->box($args);
        
        if ($setting['boxtype'] == 'linkage') {
        $name  = $this->field($args);
        $options = explode("\n", $setting['options']);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'].'</label>';
        
        $html .= '<select id="'.$args['field'].'" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'name="'.$name.'['.$args['field'].'][]" onChange="javascript:yhcms.admin.linkage(\''.U('extend/linkage/lists').'\', \''.$name.'\', \''.$args['field'].'\', this.value, \'margin-left:6px; '.$args['style'].'\', \''.$value.'\', 0)">';
        $html .= '<option value="0">请选择</option>';
        
        $value = explode(',', $value);
        
        foreach ($options as $key => $option) {
        if (empty($option)) continue;
        list($keys, $vals) = explode('|', $option);
        
        $vals  = $setting['outputtype'] ? $vals : $keys;
        $vals  = trim($vals);
        $selected = in_array($vals, $value) ? ' selected' : '';
        
        $html .= '<option value="'.$vals.'" '.$selected.' >';
        $html .= $keys.'</option>';
        }
        $html .= '</select>';
        $html .= '<div id="'.$args['field'].'_box"></div>';
        $html .= '<script language="javascript">';
        $html .= '$(function() {';
        $html .= 'if ($("#'.$args['field'].'").val()) { $("#'.$args['field'].'").trigger("change"); }';
        $html .= '});';
        $html .= '</script>';
        }
        return $html;
    }
    
    /**
     * 相关文章
     * 
     * @param array     $args       必填。元素参数
     * 
     * @return string
     */
    public final function relation($args) {
        $setting = unserialize($args['setting']);
        $name  = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        $array = explode(',', $value);
        
        if (empty($setting['module']))  $module  = '';
        else $module  = $_GET[$setting['module']];
        if (empty($setting['modelid'])) $modelid = '';
        else $modelid = $_GET[$setting['modelid']];
        if (empty($setting['catid']))   $catid   = '';
        else $catid   = $_GET[$setting['catid']];
        if (empty($setting['id']))      $id      = '';
        else $id      = $_GET[$setting['id']];
        
        $where              = [];
        $where['catid']     = $catid;
        $cates = D('Admin/Category')->where($where)->find();
        $modelid = $modelid ? : $cates['modelid'];
        
        $where              = [];
        $where['modelid']   = $modelid;
        $model = D('Admin/Model')->where($where)->find();
        $table = $model['table'];
        
        $text  = '';
        foreach ($array as $key => $mid) {
        if (!$mid) continue;
        $where              = [];
        $where['mid']       = $mid;
        $title = D($table)->where($where)->find()['title'];
        
        $text .= '<span data-id="'.$mid.'">';
        $text .= $title;
        $text .= '<a href="javascript:void(0);" onClick="';
        $text .= 'javascript:delete_'.$args['field'].'_mid('.$mid.');">';
        $text .= '移除</a>';
        $text .= '</span>';
        }
        $html  = '';
        $html .= '<input id="'.$args['field'].'Val" type="hidden" ';
        $html .= 'name="'.$name.'['.$args['field'].']" ';
        $html .= 'value="'.$value.'" />';
        $html .= '<div id="'.$args['field'].'Box" class="cms-relation">';
        $html .= $text;
        $html .= '</div>';
        
        $link  = ['modelid' => $modelid, 'catid' => $catid, 'mid' => $id];
        $html .= '<a href="javascript:void(0);" onClick="javascript:';
        $html .= 'yhcms.dialog.window(\'';
        $html .= U('Article/Content/relation', $link);
        $html .= '\', \'添加相关内容\')" class="cms-cf30">添加？</a>';
        
        $html .= '<script language="javascript" type="text/javascript">';
        $html .= 'function delete_'.$args['field'].'_mid(mid) {';
        $html .= '$("#'.$args['field'].'Box").find("span")';
        $html .= '.each(function() {';
        $html .= 'if ($(this).attr("data-id") == mid) {';
        $html .= 'var val = $("#'.$args['field'].'Val").val(),';
        $html .= 'arr = val.split(",");';
        $html .= 'arr.splice($.inArray(mid, arr), 1);';
        $html .= '$("#'.$args['field'].'Val").val(arr.join(","));';
//         $html .= '$("#'.$args['field'].'Val").val(';
//         $html .= '$("#'.$args['field'].'Val").val().replace(mid, ""));';
        $html .= '$(this).remove();';
        $html .= '}';
        $html .= '});';
        $html .= '}';
        $html .= '</script>';
        return $html;
    }
    
    public final function subset($args) {
        $setting = unserialize($args['setting']);
        
        $args['setting'] = serialize($setting);
        $html = $this->box($args);
        if ($setting['boxtype'] == 'subset') {
        $name = $this->field($args);
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $html .= '<label for="'.$args['field'].'" class="sr-only">';
        $html .= $args['name'].'</label>';
        
        $html .= '<select id="'.$args['field'].'" ';
        $html .= 'class="form-control input-sm" ';
        $html .= 'style="'.$args['style'].'" ';
        $html .= 'name="'.$name.'['.$args['field'].']">';
        $html .= '<option value="0">请选择'.$setting['subset'].'</option>';
        $html .= '</select>';
        
        $z_linkage = $setting['field'];
        $z_linkage_box = $z_linkage.'_box';
        
        $z_linkage1 = $z_linkage.'1';
        $z_linkage2 = $z_linkage.'2';
        
        $z_field = $args['field'];
        $z_ajax = U('extend/linkage/subset');
        $z_subset = $setting['subset'];
        
        $html .= <<<echo
<script language="javascript">
$(function() {
    var $z_field = $("#$z_field");
    
    $("#$z_linkage").change(function() {
        if ($(this).val() == "0" || $(this).val() == "") {
            $z_field.html("<option value='0'>请选择$z_subset</option>");
            return ;
        }
        var name = $(this).find("option:selected").text();
        $.getJSON("$z_ajax", {subset: "$z_subset", linkageid: $(this).val()}, function(data) {
            $z_field.html("");
            if (!data) return ;
            var html  = "", selected = "";
                html += "<option value='0'>请选择"+name+"$z_subset</option>";
            $.each(data, function(i, row) {
                selected = ""
                if ("$value" == row.linkageid) selected = "selected";
                
                html += "<option value='"+row.linkageid+"' "+selected+">"+row.name+"</option>";
            });
            $z_field.html(html);
        });
    });
    $("#$z_linkage_box").on("change", "#sub_$z_linkage1", function() {
        if ($(this).val() == "0" || $(this).val() == "") {
            $z_field.html("<option value='0'>请选择$z_subset</option>");
            return ;
        }
        var name = $(this).find("option:selected").text();
        $.getJSON("$z_ajax", {subset: "$z_subset", linkageid: $(this).val()}, function(data) {
            $z_field.html("");
            if (!data) return ;
            var html  = "", selected = "";
                html += "<option value='0'>请选择"+name+"$z_subset</option>";
            $.each(data, function(i, row) {
                selected = ""
                if ("$value" == row.linkageid) selected = "selected";
                
                html += "<option value='"+row.linkageid+"' "+selected+">"+row.name+"</option>";
            });
            $z_field.html(html);
        });
    });
    $("#$z_linkage_box").on("change", "#sub_$z_linkage2", function() {
        if ($(this).val() == "0" || $(this).val() == "") {
            $z_field.html("<option value='0'>请选择$z_subset</option>");
            return ;
        }
        var name = $(this).find("option:selected").text();
        $.getJSON("$z_ajax", {subset: "$z_subset", linkageid: $(this).val()}, function(data) {
            $z_field.html("");
            if (!data) return ;
            var html  = "", selected = "";
                html += "<option value='0'>请选择"+name+"$z_subset</option>";
            $.each(data, function(i, row) {
                selected = ""
                if ("$value" == row.linkageid) selected = "selected";
                
                html += "<option value='"+row.linkageid+"' "+selected+">"+row.name+"</option>";
            });
            $z_field.html(html);
        });
    });
    if ($("#$z_linkage").val() != 0) $("#$z_linkage").trigger("change");
    if ($("#sub_$z_linkage1").val() != 0) $("#sub_$z_linkage1").trigger("change");
    if ($("#sub_$z_linkage2").val() != 0) $("#sub_$z_linkage2").trigger("change");
});
</script>
echo;
        }
        // yhcms.admin.subset
        return $html;
    }
    
    public final function units($args) {
        $setting = unserialize($args['setting']);
        $linkageid = $setting['linkageid'];
        
        $linkModel = new LinkageModel();
        $unitModel = new InstitutionModel();
        
        $linkageid = $linkModel->thischildid($linkageid);
        $linkageidarr = explode(',', $linkageid);
        array_shift($linkageidarr);
        array_shift($linkageidarr);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        $array = explode('|', $value);
        $value = $array[0]; // explode(',', $array[0]);
        
        $where = [];
        $where['linkageid'] = ['IN', $linkageidarr];
        $where['display']   = 1;
        
        $order = [];
        $order['listorder'] = 'ASC';
        $order['linkageid'] = 'ASC';
        
        $linkageidarrary = $linkModel->where($where)->order($order)->select();
        foreach ($linkageidarrary as $key => $row) {
            $setting['options'] .= $row['name'].'|'.$row['linkageid']."\n";
        }
        $args['setting'] = serialize($setting);
        
        $html = '';
        if ($setting['boxtype'] != 'units')  return $html;
        
        $name = $this->field($args);
        $options = explode("\n", $setting['options']);
        
        $link = U('extend/linkage/lists');
        $html = <<<echo
        <label for="{$args['field']}_linkage" class="sr-only">{$args['name']}</label>
        <select id="{$args['field']}_linkage" class="form-control input-sm" style="{$args['style']}" name="{$name}[{$args['field']}_linkage][]" onChange="javascript:yhcms.admin.linkage('{$link}', '{$name}', '{$args['field']}_linkage', this.value, 'margin-left:6px; {$args['style']}', '{$value}', 0);">
        <option value="0">请选择</option>
echo;
        foreach ($options as $key => $option) {
        if (empty($option)) continue;
        list($keys, $vals) = explode('|', $option);
        $vals = $setting['outputtype'] ? $vals : $keys;
        $value = explode(',', $array[0]);
        $selected = in_array($vals, $value) ? ' selected' : '';
        $html.= <<<echo
        <option value="{$vals}"{$selected}>{$keys}</option>
echo;
        }
        $html.= <<<echo
        </select>
echo;
        $html.= <<<echo
        <div id="{$args['field']}_linkage_box"></div>
echo;
        
        $value = explode(',', $array[1]);
        $value[0] = $value[0] ?: 0;
        
        $link = U('extend/institution/lists');
        $unit = U('extend/institution/units');
        $html.= <<<echo
        <br />
        <label for="{$args['field']}" class="sr-only">{$args['name']}</label>
        <select id="{$args['field']}" class="form-control input-sm" style="" name="{$name}[{$args['field']}][]">
        <option value="0">请选择共享单位！</option>
echo;
        $html.= <<<echo
        </select>
echo;
        $html.= <<<echo
        <div id="{$args['field']}_unit"></div>
        <script language="javascript">
        $(function() {
            if ($("#{$args['field']}_linkage").val()) { $("#{$args['field']}_linkage").trigger("change"); }
            
            var linkageid = 0;
            $("#{$args['field']}_linkage").change(function() {
                if ($(this).val() == "0" || $(this).val() == "") {
//                     $("#{$args['field']}").html("<option value='0'>请选择共享单位！</option>");
                    $("#{$args['field']}_unit").html("");
                    return ;
                }
                linkageid = $(this).val();
                $.getJSON("{$link}", {linkageid: $(this).val(), unitid: 0}, function(data) {
                    $("#{$args['field']}").html("");
                    $("#{$args['field']}_unit").html("");
                    if (data == [] || data == "[]") return ;
                    var html = "",
                        selected = "";
                    html += "<option value='0'>请选择共享单位！</option>";
                    $.each(data, function(i, row) {
                        selected = "";
                        if ("{$value[0]}" == row.unitid) selected = "selected";
                        html += "<option value='"+row.unitid+"' "+selected+">"+row.name+"</option>";
                    });
                    $("#{$args['field']}").html(html);
                });
            });
            $("#{$args['field']}_linkage_box").on("change", "#sub_{$args['field']}_linkage1", function() {
                if ($(this).val() == "0" || $(this).val() == "") {
//                     $("#{$args['field']}").html("<option value='0'>请选择共享单位！</option>");
                    $("#{$args['field']}_unit").html("");
                    return ;
                }
                linkageid = $(this).val();
                $.getJSON("{$link}", {linkageid: linkageid, unitid: 0}, function(data) {
                    $("#{$args['field']}").html("");
                    $("#{$args['field']}_unit").html("");
                    if (data == [] || data == "[]") return ;
                    var html = "",
                        selected = "";
                    html += "<option value='0'>请选择共享单位！</option>";
                    $.each(data, function(i, row) {
                        selected = "";
                        if ("{$value[0]}" == row.unitid) selected = "selected";
                        html += "<option value='"+row.unitid+"' "+selected+">"+row.name+"</option>";
                    });
                    $("#{$args['field']}").html(html);
                });
            });
            $("#{$args['field']}_linkage_box").on("change", "#sub_{$args['field']}_linkage2", function() {
                if ($(this).val() == "0" || $(this).val() == "") {
//                     $("#{$args['field']}").html("<option value='0'>请选择共享单位！</option>");
                    $("#{$args['field']}_unit").html("");
                    return ;
                }
                linkageid = $(this).val();
                $.getJSON("{$link}", {linkageid: linkageid, unitid: 0}, function(data) {
                    $("#{$args['field']}").html("");
                    $("#{$args['field']}_unit").html("");
                    if (data == [] || data == "[]") return ;
                    var html = "",
                        selected = "";
                    html += "<option value=''>请选择共享单位！</option>";
                    $.each(data, function(i, row) {
                        selected = "";
                        if ("{$value[0]}" == row.unitid) selected = "selected";
                        html += "<option value='"+row.unitid+"' "+selected+">"+row.name+"</option>";
                    });
                    $("#{$args['field']}").html(html);
                });
            });
            
            $("#{$args['field']}").change(function() {
                if ($(this).val() == "0" || $(this).val() == "") {
                    $("#{$args['field']}_unit").html("");
                    return ;
                }
                $.getJSON("{$link}", {linkageid: linkageid, unitid: $(this).val()}, function(data) {
                    $("#{$args['field']}_unit").html("");
                    if (data == [] || data == "[]") return ;
                    var html = "";
                    html += "<select id='sub_{$args['field']}' class='form-control input-sm' name='{$name}[{$args['field']}][]'>";
                    html += "<option value=''>请选择共享单位！</option>";
                    $.each(data, function(i, row) {
                        selected = "";
                        if ("{$value[1]}" == row.unitid) selected = "selected";
                        html += "<option value='"+row.unitid+"' "+selected+">"+row.name+"</option>";
                    });
                    html += "</select>";
                    $("#{$args['field']}_unit").html(html);
                });
            });
            if ("{$value[0]}" != "") {
                $.getJSON("{$unit}", {unitid: {$value[0]}}, function(units) {
                    
                    $.getJSON("{$link}", {linkageid: units.linkageid, unitid: 0}, function(data) {
                        $("#{$args['field']}").html("");
                        $("#{$args['field']}_unit").html("");
                        if (data == [] || data == "[]") return ;
                        var html = "",
                            selected = "";
                        html += "<option value=''>请选择共享单位！</option>";
                        $.each(data, function(i, row) {
                            selected = "";
                            if ("{$value[0]}" == row.unitid) selected = "selected";
                            html += "<option value='"+row.unitid+"' "+selected+">"+row.name+"</option>";
                        });
                        $("#{$args['field']}").html(html);
                    });
                    $.getJSON("{$link}", {linkageid: units.linkageid, unitid: {$value[0]}}, function(data) {
                        $("#{$args['field']}_unit").html("");
                        if (data == [] || data == "[]") return ;
                        var html = "";
                        html += "<select id='sub_{$args['field']}' class='form-control input-sm' name='{$name}[{$args['field']}][]'>";
                        html += "<option value=''>请选择共享单位！</option>";
                        $.each(data, function(i, row) {
                            selected = "";
                            if ("{$value[1]}" == row.unitid) selected = "selected";
                            html += "<option value='"+row.unitid+"' "+selected+">"+row.name+"</option>";
                        });
                        html += "</select>";
                        $("#{$args['field']}_unit").html(html);
                    });
                });
            }
        });
        </script>
echo;
        return $html;
    }
    
    /**
     * 共享企业
     * 
     * @param unknown $args
     */
    public final function company($args) {
        $setting = unserialize($args['setting']);
        $name = $this->field($args);
        
        $check = isset($args['value']) && !empty($args['value']);
        $value = $check ? $args['value'] : $setting['default'];
        
        $where = [];
        $where['company_id'] = $value;
        $where['disabled'] = 0;
        $ress = D('Company')->where($where)->find();
        
        $text = <<<echo
        <span data-id="{$ress['company_id']}">
            {$ress['name']}
            <a href="javascript:void(0);" onClick="javascript:delete_{$args['field']}_mid('{$ress['company_id']}');">移除</a>
        </span>
echo;
        $link = U('Extend/Company/popup2');
        $temp = <<<echo
        <a href="javascript:void(0);" onClick="javascript:yhcms.dialog.window('{$link}', '添加企业');">添加？</a>
echo;
        $text = $ress ? $text : $temp;
        
        $html = <<<echo
        <input id="{$args['field']}" type="hidden" name="{$name}[{$args['field']}]" value="{$value}" />
        <div id="{$args['field']}Box" class="cms-relation">
            {$text}
        </div>
echo;
        $temp = cms_addslashes($temp);
        $html.= <<<echo
        <script language="javascript" type="text/javascript">
            function delete_{$args['field']}_mid(mid) {
                var crrid = $("#{$args['field']}Box").find("span").attr("data-id");
                if (mid == crrid) {
                    $("#{$args['field']}Box").find("span").remove();
                    $("#{$args['field']}Box").html("{$temp}");
                    $("#{$args['field']}").val("");
                }
            }
        </script>
echo;
        return $html;
    }
    
    /**
     * 判断获取表单元素名称（标签名称）
     * 
     * @param array     $content    必填。环境参数
     * 
     * @return string
     */
    private final function field($content) {
        $form = $content['ismain'] ? 'main' : 'data';
        return !isset($content['ismain']) ? 'data' : $form;
    }
    
}
