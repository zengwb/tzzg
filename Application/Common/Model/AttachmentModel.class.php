<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块附件管理数据模型类：配置或实例化附件数据模型
 * 
 * @author T-01
 */
final class AttachmentModel extends AbstractAdvancedModel {
    
    protected   $autoCheckFields = false,
                $partition  = [
                    'type'  => 'id', 'field' => 'attachedid',
                    'expr'  => 200000, 'num' => 5
                ];
    
    private     $field      = null;
    
    /**
     * {@inheritDoc}
     * @see \Common\Model\AbstractAdvancedModel::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    /**
     * 插入附件数据（信息）
     * 
     * @param array     $files      必填。附件数据（数组）
     * 
     * @return boolean|mixed
     */
    public final function insert($files) {
        $username = $files['username'];
        $username = $username ? : $_SESSION['CMS_UserName'];
        
        $files['username']  = $username;
        $files['module']    = $files['module'] ? : MODULE_NAME;
        $files['catid']     = $files['catid']  ? : 0;
        
        $files['uploadtime']= time();
        $files['uploadip']  = get_client_ip();
        
        $files['attachedid']= $this->submeter()->max('attachedid')+1;
        $files['siteid']    = $files['siteid'] ? : 0; // cms_siteid();
        
        if (!$this->create($files)) return false;
        else return $this->submeter($files)->add();
    }
    
}
