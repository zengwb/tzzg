<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块错误日志数据模型类：配置或实例化错误数据模型
 * 
 * @author T-01
 */
final class ErrorLogModel extends AbstractAdvancedModel {
    
    protected   $autoCheckFields = false,
                $partition  = [
                    'type' => 'id', 'field' => 'logid', 'num' => 2,
                    'expr' => 100000
                ];
    private     $field      = null;
    
    /**
     * {@inheritDoc}
     * @see \Common\Model\AbstractAdvancedModel::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    /**
     * 写入日志
     * 
     * @param string    $tips       选填。提示信息
     * @param string    $text       选填。内容参数
     * 
     * @return boolean
     */
    public final function writelog($tips = '', $text = '') {
        $username = session('CMS_UserName');
        $username = $username ? : 'unknown';
        
        $usernoid = session('CMS_UserID');
        $usernoid = $usernoid ? : 0;
        
        $data               = [];
        $data['logid']      = $this->submeter()->max('logid')+1;
        
        $data['message']    = $tips;
        $data['file']       = $_SERVER['HTTP_REFERER'];
        
        $data['description']= serialize($text);
        $data['client']     = $_SERVER['HTTP_USER_AGENT'];
        
        $data['userid']     = $usernoid;
        $data['username']   = $username;
        
        $data['operip']     = get_client_ip();
        $data['inputtime']  = time();
        
        if (!$this->create($data))  return false ;
        else return $this->submeter($data)->add();
    }
    
}
