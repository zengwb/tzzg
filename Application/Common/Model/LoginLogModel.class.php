<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块登陆日志数据模型类：配置或实例化登陆数据模型
 * 
 * @author T-01
 */
final class LoginLogModel extends AbstractAdvancedModel {
    
    protected   $autoCheckFields = false,
                $partition  = [
                    'type' => 'id', 'field' => 'logid', 'num' => 2,
                    'expr' => 100000
                ];
    private     $field      = null;
    
    /**
     * {@inheritDoc}
     * @see \Common\Model\AbstractAdvancedModel::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    /**
     * 写入日志
     * 
     * @param string    $tips       选填。提示信息
     * @param array     $text       选填。内容参数
     * 
     * @return boolean
     */
    public final function writelog($tips = '', $text = []) {
        $username = IS_POST ? 
                    I('post.data')['username'] : 'unknown';
        
        $password = cms_password($text['password']);
        $password = $text['status'] ? $password['password'] :
                    $text['password'];
        
        $data               = [];
        $data['logid']      = $this->submeter()->max('logid')+1;
        
        $data['username']   = $username;
        $data['password']   = $password;
        
        $data['client']     = $_SERVER['HTTP_USER_AGENT'];
        $data['message']    = $tips;
        
        $data['loginip']    = get_client_ip();
        $data['logintime']  = time();
        
        $data['status']     = $text['status'] ? 1 : 0;
        
        if (!$this->create($data))  return false ;
        else return $this->submeter($data)->add();  // @todo:
    }
    
}
