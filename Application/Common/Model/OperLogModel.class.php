<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块操作日志数据模型类：配置或实例化操作数据模型
 * 
 * @author T-01
 */
final class OperLogModel extends AbstractAdvancedModel {
    
    protected   $autoCheckFields = false,
                $partition  = [
                    'type' => 'id', 'field' => 'logid', 'num' => 5,
                    'expr' => 200000
                ];
    
    private     $field      = null;
    
    /**
     * {@inheritDoc}
     * @see \Common\Model\AbstractAdvancedModel::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    /**
     * 写入日志
     * 
     * @param string    $tips       选填。提示信息
     * @param string    $text       选填。内容参数
     * 
     * @return boolean
     */
    public final function writelog($tips = '', $text = '') {
        $username = session('CMS_UserName');
        $username = $username ? : 'unknown';

        $usernoid = session('CMS_UserID');
        $usernoid = $usernoid ? : 0;

        $data               = [];
        $data['logid']      = $this->submeter()->max('logid')+1;
        
        $data['module']     = MODULE_NAME;
        $data['controller'] = CONTROLLER_NAME;
        $data['action']     = ACTION_NAME;
        
        $data['params']     = $_SERVER['QUERY_STRING'];
        $data['url']        = $_SERVER['REQUEST_URI'];
        
        $data['message']    = $tips;
        $data['data']       = serialize($text);
        
        $data['userid']     = $usernoid;
        $data['username']   = $username;
        
        $data['operip']     = get_client_ip();
        $data['opertime']   = time();
        
        if (!$this->create($data))  return false ;
        else return $this->submeter($data)->add();
    }
    
    /**
     * 数据列表
     * 
     * @param array     $where      必填。查询条件
     * 
     * @return array
     */
    public final function getData($where) {
        return $this->submeter($where)->where($where)->select();
    }
    
}
