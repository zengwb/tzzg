<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块系统配置数据模型类：配置或实例化配置数据型
 * 
 * @author T-01
 */
final class ConfigModel extends iPageModel {
    
    /**
     * 获取模块配置参数（数组）
     * 
     * @param string    $module     必填。模块
     * @param string    $group      选填。分组
     * 
     * @return array
     */
    public final function configArray($module, $group = '') {
        $where              = [];
        $where['disabled']  =  0;
        $where['module']    = $module ? : MODULE_NAME;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        if (!empty($group)) $where['group'] = $group;
        
        $order = '`listorder` DESC';
        $ress2 = $this->where($where)->order($order)->select();
        return $ress2;
    }
    
}
