<?php

namespace Common\Model;

use \Think\Model\AdvModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块高级数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
abstract class AbstractAdvancedModel extends AdvModel {
    
    protected $partition = ['type' => 'id', 'expr' => 200000];
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::_initialize()
     */
    public function _initialize() {
        $this->field = $this->partition['field'];
    }
    
    /**
     * 获取数据库分表序号（名称）
     * 
     * @param array     $data       选填。数据
     * 
     * @return string
     */
    public final function submeter($data = []) {
        $data = empty($data) ? $_POST : $data;
        $name = $this->getPartitionTableName($data);

        return $this->table($name);
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::limit()
     */
    public final function limit($offset, $length = null) {
        parent::limit($offset, $length);
        return $this;
    }
    
}
