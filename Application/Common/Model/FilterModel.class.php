<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块敏感数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class FilterModel extends iPageModel {
    
    protected   $tableName  = 'keyword';
    
}
