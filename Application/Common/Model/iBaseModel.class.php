<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块基础数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
abstract class iBaseModel extends AbstractModel {}
