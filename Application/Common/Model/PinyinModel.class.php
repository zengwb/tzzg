<?php

namespace Common\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 公共模块拼音模型类：将汉字转化成拼音输出
 * 
 * @author T-01
 */
final class PinyinModel extends AbstractModel {
    
    protected   $autoCheckFields = false;
    
    private     $ascii      = [];
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::_initialize()
     */
    public final function _initialize() {
        $pinyin = file_get_contents(RES_PATH.'Data/pinyin.bat');
        $this->ascii = json_decode($pinyin, true);
    }
    
    /**
     * 将汉字转换并输出拼音
     * 
     * @param string    $string     必填。待转换的汉字
     * @param boolean   $utf8       选填。汉字字符编码
     * 
     * @return string
     */
    public final function output($string, $utf8 = true) {
        if (empty($string)) return '';
        if ($utf8) $string = $this->iconvstr('utf-8', 'gbk', $string);
        
        $number = strlen($string);
        $pinyin = '';
        for ($i = 0; $i < $number; $i++) {
            $temp = ord(substr($string, $i, 1));
            if ($temp > 160) {
                $tmp2 = ord(substr($string, ++$i, 1));
                $temp = $temp * 256 + $tmp2 - 65536;
            }
            $pinyin .= $this->num2str($temp);
        }
        return $utf8 ? $this->iconvstr('gbk', 'utf-8', $pinyin) :
               $pinyin;
    }
    
    /**
     * 将ASCII编码转换为字符串
     * 
     * @param integer   $ascii      必填。ASCII码
     * 
     * @return string
     */
    private final function num2str($ascii) {
        if (!$this->ascii) return  $ascii;
        if ($ascii > 0 && $ascii < 160) {
            return chr($ascii);
        } else if ($ascii <- 20319 || $ascii >- 10247) {
            return '';
        } else {
            $total = sizeof($this->ascii) - 1;
            for ($i = $total; $i >= 0; $i--) {
                if ($this->ascii[$i][1] <= $ascii) break;
            }
            return $this->ascii[$i][0];
        }
    }
    
    /**
     * 字符编码转换
     * 
     * @param string    $from       必填。原始编码
     * @param string    $to         必填。目标编码
     * @param string    $string     必填。待转换字符串
     * 
     * @return string
     */
    private final function iconvstr($from, $to, $string) {
        if (!is_string($string)) return ;
        if (function_exists('mb_convert_encoding')) {
            return mb_convert_encoding($string, $to, $from);
        } else if (function_exists('iconv')) {
            return iconv($from, $to, $string);
        } else {
            return $string;
        }
    }
    
    public final function __destruct() {
        if (isset($this->ascii)) unset($this->ascii);
    }
    
}
