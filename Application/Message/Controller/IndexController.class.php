<?php

namespace Message\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 留言模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    private     $message    = [],
                $reply      = [];
    
    /**
     * {@inheritDoc}
     * @see \Message\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->message      = D('Message');
        $this->reply        = D('MessageReply');
    }
    
    /**
     * 留言测试（示例）
     */
    public final function test() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['msgtime']    = time();
        $data['msgip']      = get_client_ip();
        $data['siteid']     = cms_siteid();
        
        $ress = $this->message->add($data);
        
        if (!$ress) $this->error('留言失败！');
        else $this->success('留言成功！');
        } // @todo:
        
        $type = cms_config_array('message', 'base')['type'];
        
        $this->assign('type', explode('|', $type));
        $this->display();
    }
    
}
