<?php

namespace Message\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 留言模块内容管理控制器类：呈现留言CURD常规管理
 * 
 * @author T-01
 */
final class ContentController extends BaseController {
    
    public      $action     = [
        'index', 'state', 'detail', 'delete', 'reply'
    ];
    
    private     $message    = [],
                $reply      = [],
                
                $siteModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Message\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->message      = D('Message');
        $this->reply        = D('MessageReply');
        
        $this->siteModel    = D('Admin/Website');
    }
    
    /**
     * 留言管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $order = '`msgtime` DESC';
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        $nums = $this->message->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->message->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        $row['content'] = $row['title'] ?: $row['content'];
        if ($keyword) $row['content'] = strtr($row['title'], $keys);
        
        $where              = [];
        $where['siteid']    = $row['siteid'];
        $site = $this->siteModel->where($where)->find();
        
        $where              = [];
        $where['msgid']     = $row['msgid'];
        $count = $this->reply->where($where)->count();
        
        $row['reply'] = $count;
        $row['site']= $site['name'] ?: '全部站点';
        $data[$key] = $row;
        }
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 留言审核
     */
    public final function state() {
        $msgid = I('get.msgid', 0, 'intval');
        $info               = [];
        $info['msgid']      = $msgid;
        
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $ress2 = $this->message->where($where)->find();
        $value = $ress2['disabled'] ? 0 : 1;
        
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $ress2 = $this->message->where($where)->save([
            'disabled'=>$value
        ]);
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $data2 = $this->message->where($where)->find();
        
        $oper2 = $data2['disabled'] ? '关闭' : '审核';
        $title = $data2['title'] ? : $data2['content'];
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'留言『'.$title.'』信息';
        $tips2.= $state;
        cms_writelog($tips2, ['msgid' => $info['msgid']]);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 留言查看
     */
    public final function detail() {
        $msgid = I('get.msgid', 0, 'intval');
        $info               = [];
        $info['msgid']      = $msgid;
        
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $data = $this->message->where($where)->find();
        
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 留言删除
     */
    public final function delete() {
        $msgid = I('get.msgid', 0, 'intval');
        $info = I('post.info', []);
        $info = $msgid ? [$msgid] : $info['msgid'];
        $data = [];
        if (empty($info)) $this->error('请选择留言信息！');
        
        foreach ($info as $key => $msgid) {
        $data['msgid'][]    = $msgid;
        $where              = [];
        $where['msgid']     = $msgid;
        $this->reply->where($where)->delete();
        }
        $where              = [];
        $where['msgid']     = ['IN', $data['msgid']];
        $ress = $this->message->where($where)->delete();
        $temp = 'msgid:'.implode(',', $data['msgid']);
        
        if (!$ress) $this->error('删除留言信息失败！', $temp);
        else $this->success('删除留言信息成功！', $temp);
    }
    
    /**
     * 留言回复
     */
    public final function reply() {
        if (IS_POST) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['username']   = session('CMS_UserName');
        $data['replytime']  = time();
        $data['msgid']      = $info['msgid'];
        
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $check = $this->reply->where($where)->find();
        
        if (!$check) $ress = $this->reply->add($data);
        else $ress = $this->reply->where($where)->save($data);
        
        $tips = '回复留言『'.$info['title'].'』信息';
        if (!$ress) $this->popup(false, $tips);
        else $this->popup(true, $tips);
        } // @todo: 
        
        $msgid = I('get.msgid', 0, 'intval');
        $info               = [];
        $info['msgid']      = $msgid;
        
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $info = $this->message->where($where)->find();
        
        $where              = [];
        $where['msgid']     = $info['msgid'];
        $data = $this->reply->where($where)->find();
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->display();
    }
    
}
