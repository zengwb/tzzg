<?php

namespace Message\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 留言模块回复数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class MessageReplyModel extends BaseModel {}
