<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Extend',
    'version'               => 'v1.0.01',
    
    'name'                  => 'Extend',
    'description'           => '扩展模块',
    'core'                  => false,
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
];
