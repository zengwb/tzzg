<?php

namespace Extend\Model;

use \Common\Model\iBaseModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块基础数据模型类：配置或实例化数据模型
 * 
 *
 */
abstract class BaseModel extends iBaseModel {}
