<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块菜单AJAX控制器类
 * 
 * @author T-01
 */
final class MenuController extends PageController {
    
    private     $menuModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Extend\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        $this->menuModel    = D('Admin/Menu');
    }
    
    /**
     * 菜单属性
     */
    public final function attribute() {
        $menuid = I('get.menuid', 0, 'intval');
        
        $where              = [];
        $where['menuid']    = $menuid;
        $where['display']   = 1;
        
        echo $this->menuModel->where($where)->find()['isattr'];
    }
    
}
