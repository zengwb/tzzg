<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

final class InstitutionController extends PageController {
    
    private $unitModel = [];
    
    public final function _initialize() {
        $this->unitModel = D('Admin/Institution');
    }
    
    public final function lists() {
        $unitid = I('get.unitid', 0, 'intval');
        $linkageid = I('get.linkageid', 0, 'intval');
        
        $unitids = $this->unitModel->get_unit_childid($unitid, $linkageid);
        $unitidarr = explode(',', $unitids);
        array_shift($unitidarr);
        if (empty($unitidarr)) { echo json_encode([]); return ; }
        
        $where = [];
        $where['unitid'] = ['IN', $unitidarr];
        $where['disabled'] = 0;
        
        $order = [];
        $order['listorder'] = 'ASC';
        $order['unitid'] = 'ASC';
        
        $option = [];
        $unitarray = $this->unitModel->where($where)->order($order)->select();
        foreach ($unitarray as $key => $row) {
            $option[$key]['name'] = $row['name'];
            $option[$key]['unitid'] = $row['unitid'];
        }
        echo json_encode($option);
    }
    
    public final function units() {
        $unitid = I('get.unitid', 0, 'intval');
        $ress = $this->unitModel->where(['unitid' => $unitid])->find();
        echo json_encode($ress);
    }
    
}