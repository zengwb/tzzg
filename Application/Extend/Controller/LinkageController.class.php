<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块联动AJAX控制器类
 * 
 * @author T-01
 */
final class LinkageController extends PageController {
    
    private $linkage = [];
    
    /**
     * {@inheritDoc}
     * @see \Extend\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        $this->linkage = D('Admin/Linkage');
    }
    
    /**
     * 联动列表
     * 
     * http://www.yhcms.com/index.php?m=extend&c=linkage&a=lists&linkageid=1
     */
    public final function lists() {
        $linkageid = I('get.linkageid', 0);
        if (empty($linkageid)) echo json_encode("");
        
        $linkageids = $this->linkage->thischildid($linkageid);
        $linkageidarr = explode(',', $linkageids);
        array_shift($linkageidarr);
        if (empty($linkageidarr)) return ;
        
        $where = [];
        $where['linkageid'] = ['IN', $linkageidarr];
        $where['display'] = 1;
        
        $order = [];
        $order['listorder'] = 'ASC';
        $order['linkageid'] = 'ASC';
        
        $option = [];
        $linkagearray = $this->linkage->where($where)->order($order)->select();
        foreach ($linkagearray as $key => $row) {
            $option[$key]['name'] = $row['name'];
            $option[$key]['linkageid'] = $row['linkageid'];
        }
//        print_r($option); exit();
        echo json_encode($option);
    }
    
    /**
     * 联动子集
     */
    public final function subset() {
        $linkageid = I('get.linkageid', 0, 'intval');
        $subset = I('get.subset', '', 'cms_addslashes');
        
        $where                  = [];
        $where['name']          = ['LIKE', $subset.'%'];
        $where['subsetid']      = $linkageid;
        
        $linkageid = $this->linkage->where($where)->find()['linkageid'];
        $data = [];
        $ress = $this->linkage->where(['parentid' => $linkageid])->order([
            'listorder'         => 'ASC',
            'linkageid'         => 'ASC'
        ])->select();
        foreach ($ress as $key  => $row) {
            $data[$key]['name'] =  $row['name'];
            $data[$key]['linkageid'] = $row['linkageid'];
        }
        echo json_encode($data);
    }
    
}
