<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

final class CompanyController extends BaseController {
    
    public final function popup2() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        $keyword = trim($keyword);
        
        $where = [];
        $where['name'] = ['LIKE', '%'.$keyword.'%'];
        $where['disabled'] = 0;
        if (empty($keyword)) unset($where['name']);
        
        $nums = D('Company')->where($where)->count();
        $rows = 12;
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $ress = D('Company')->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order('`listorder` ASC, `company_id` DESC')->select();
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $data = [];
        foreach ($ress as $key => $row) {
            if (!$keyword) $row['title2'] = $row['name'];
            else $row['title2'] = strtr($row['name'], $keys);
            $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
}
