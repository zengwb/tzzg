<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块内容AJAX控制器类
 * 
 * @author T-01
 */
final class ArticleController extends PageController {
    
    private $modelModel     = [],
            $catesModel     = [],
            
            $linksModel     = [];
    
    /**
     * {@inheritDoc}
     * @see \Extend\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->modelModel   = D('Admin/Model');
        $this->catesModel   = D('Admin/Category');
        
        $this->linksModel   = D('Admin/Linkage');
    }
    
    /**
     * 项目列表
     */
    public final function project_lists() {
        
    }
    
    public final function test() {
        $p = I('get.p');
        
        $positionModel = D('Article/Position');
        $modelModel = D('Admin/Model');
        
        $where = [];
        $where['posiid'] = $p;
        $where['status'] = 1;
        $ress = $positionModel->where($where)->find();
        
        $catid = $ress['catid'];
        $modelid = $ress['modelid'];
        $number = $ress['number'];
        
        $where = [];
        $where['modelid'] = $modelid;
        $where['disabled'] = 0;
        $ress = $modelModel->where($where)->find();
        $table = $ress['table'];
        
        $where = [];
        $where['catid'] = $catid;
        $where['disabled'] = 0;
        
        $ress = D($table)->where($where)->limit($number)->order('listorder ASC, mid ASC')->select();
        $data = [];
        foreach ($ress as $key => $row) {
            $data[$key]['title'] = $row['title'];
            $data[$key]['thumb'] = $row['thumb'];
            $data[$key]['inputtime'] = date('Y-m-d H:i:s', $row['inputtime']);
        }
        print_r($data);
    }
    
    /**
     * 项目信息
     * 
     * http://www.yhcms.com/index.php?m=extend&c=article&a=project&catid=150&id=1
     */
    public final function project() {
        $catid = I('get.catid', 0, 'intval');
        $id = I('get.id', 0, 'intval');
        
        $cates = $this->_get_cates($catid);
        
        $modelid = $cates['modelid'];
//         $childid = $this->catesModel->childid($catid);
        
        $model = $this->_get_model($modelid);
        $table = $model['table'];
        
        $where = [];
        $where['catid'] = $catid;
        $where['id'] = $id;
        $where['disabled'] = 0;
        $field = 'mid,title,thumb,investment,attract_investment,cooperation,company,contact,city,address,industry_typeid';
        
        $base = D($table)->where($where)->field($field)->find();
        
        $cooperationarr = explode(',', $base['cooperation']);
        $cooperationtxt = [];
        foreach ($cooperationarr as $key => $val) {
            $coop = $this->_get_linkage($val)['name'];
            $cooperationtxt[$key] = $coop;
        }
        $base['cooperation'] = implode('、', $cooperationtxt);
        
        $cityarr = explode(',', $base['city']);
        $citytxt = [];
        foreach ($cityarr as $key => $val) {
            $text = $this->_get_linkage($val)['name'];
            $citytxt[$key] = $text;
        }
//         $base['city'] = implode('|', $citytxt);
        $base['province'] = $citytxt[0];
        $base['city'] = $citytxt[1];
        $base['county'] = $citytxt[2];
        
        $industryarr = explode(',', $base['industry_typeid']);
        $industrytxt = [];
        foreach ($industryarr as $key => $val) {
            $text = $this->_get_linkage($val)['name'];
            $industrytxt[$key] = $text;
        }
        $base['industry_type'] = implode('》', $industrytxt);
        unset($base['industry_typeid']);
        
        $data = D($table.'_data')->where($where)->field('content,relation')->find();
        
        $json = array_merge($base, $data);
        print_r($json);
//         echo json_encode($json);
    }
    
    /**
     * 获取联动信息
     * 
     * @param number    $linkageid  选填。联动ID
     */
    private final function _get_linkage($linkageid = 0) {
        return $this->linksModel->where(['linkageid' => $linkageid])->find();
    }
    
    /**
     * 获取栏目信息
     * 
     * @param number    $catid      选填。栏目ID
     */
    private final function _get_cates($catid = 0) {
        return $this->catesModel->where(['catid' => $catid])->find();
    }
    
    /**
     * 获取模型信息
     * 
     * @param number    $modelid    选填。模型ID
     */
    private final function _get_model($modelid = 0) {
        return $this->modelModel->where(['modelid' => $modelid])->find();
    }
    
}
