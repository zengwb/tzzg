<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块ICON图标控制器类
 * 
 * @author T-01
 */
final class IconController extends BaseController {
    
    /**
     * {@inheritDoc}
     * @see \Extend\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
//         parent::_initialize();
    }
    
    /**
     * 选择ICON
     */
    public final function select() {
        echo __METHOD__;
    }
    
}
