<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模型角色AJAX控制器类
 * 
 * @author T-01
 */
final class RoleController extends PageController {
    
    private     $roleModel  = [],
                $siteModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Extend\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        $this->roleModel    = D('Admin/AdminRole');
        $this->siteModel    = D('Admin/Website');
    }
    
    /**
     * 隶属站点
     */
    public final function website() {
        $roleid = I('get.roleid', 0, 'intval');
        
        $info = [];
        $info['roleid']     = $roleid;
        
        $where              = [];
        $where['roleid']    = $info['roleid'];
        
        $ress = $this->roleModel->where($where)->find();
        $data = explode(',', $ress['siteids']);
        
        echo json_encode($data);
    }
    
}
