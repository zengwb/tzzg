<?php

namespace Extend\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块广告AJAX控制器类
 * 
 * @author T-01
 */
final class AdvertController extends PageController {
    
    private $spaceModel     = [],
            $advertModel    = [];
    
    /**
     * {@inheritDoc}
     * @see \Extend\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        $this->spaceModel   = D('Advert/AdvertSpace');
        $this->advertModel  = D('Advert/Advert');
    }
    
    /**
     * 
     */
    public final function space() {
        $spaceid = I('get.spaceid', 0);
        
        $data = [];
        
        $where              = [];
        $where['spaceid']   = $spaceid;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['disabled']  =  0;
        
        $space = $this->spaceModel->where($where)->find();
        $setting = unserialize($space['setting']);
        
        $data['size'] = $setting['size'];
        
        $where              = [];
        $where['spaceid']   = $spaceid;
        $where['starttime'] = ['ELT', time()];
        $where['endtime']   = ['EGT', time()];
        $where['disabled']  =  0;
        
        $limit = '0,'.$setting['number'];
        $advert = $this->advertModel->where($where)->limit($advert)->order('listorder ASC')->select();
        foreach ($advert as $key => $row) {
            $data['advert'][$key] = unserialize($row['setting']);
            $data['advert'][$key]['name'] = $row['name'];
        }
        echo json_encode($data);
    }
    
}
