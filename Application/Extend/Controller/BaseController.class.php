<?php

namespace Extend\Controller;

use \Common\Controller\iBaseController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 扩展模块基础控制器类：配置或实例化系统模块
 * 
 * @author T-01
 */
abstract class BaseController extends iBaseController {
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iBaseController::_initialize()
     */
    public function _initialize() { parent::_initialize(); }
    
}
