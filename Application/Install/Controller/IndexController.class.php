<?php

namespace Install\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 安装模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    /**
     * {@inheritDoc}
     * @see \Install\Controller\PageController::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    public final function index() { $this->install(); }
    
    public final function install() {
        $file = RUNTIME_PATH.'install.lock';
        $data = 'mjcms lock file.';
        if (!file_exists($file)) {
            file_put_contents($file, $data);
            $this->install(); // @todo: 下一步
        } else {
            header('location: /'); exit('');
        }
    }
    
}
