<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Install',
    'version'               => 'v1.0.01',
    
    'name'                  => '安装模块',
    'description'           => '',
    'core'                  => false,
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
];
