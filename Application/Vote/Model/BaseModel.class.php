<?php

namespace Vote\Model;

use \Common\Model\iBaseModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 投票模块基础数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
abstract class BaseModel extends iBaseModel {}
