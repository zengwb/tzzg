<?php

namespace Vote\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 投票模块选项数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class VoteOptionModel extends BaseModel {}
