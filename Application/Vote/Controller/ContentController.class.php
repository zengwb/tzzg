<?php

namespace Vote\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 投票模块内容管理控制器类：呈现主题CURD常规管理
 * 
 * @author T-01
 */
final class ContentController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'setting',
        'state', 'skin', 'chart'
    ];
    
    private     $siteTheme  = [],
                $siteModel  = [],
                
                $voteModel  = [],
                $tabsModel  = [],
                $dataModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Vote\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->siteTheme    = cms_theme_config(RES_PATH.'Template/', true);
        $this->siteModel    = D('Admin/Website');
        
        $this->voteModel    = D('Vote');
        $this->tabsModel    = D('VoteOption');
        $this->dataModel    = D('VoteData');
    }
    
    /**
     * 主题管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $order = '`listorder` ASC, `voteid` ASC';
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        $nums = $this->voteModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->voteModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title'] = $row['subject'];
        else $row['title'] = strtr($row['subject'], $keys);
        $row['end'] = time() >= $row['totime'] ? 1 : 0;
        $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['voteid'] as $key => $voteid) {
        $listorder = $this->voteModel->where([
            'voteid' => $voteid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->voteModel->where(['voteid' => $voteid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $voteid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('投票主题排序成功！', $listorder);
    }
    
    /**
     * 添加主题
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['addtime']    = time();
        $data['fromtime']   = strtotime($data['fromtime']);
        $data['totime']     = strtotime($data['totime']);
        
        $tips = '添加投票『'.$data['subject'].'』主题';
        $ress = $this->voteModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        $inid = $this->voteModel->getLastInsID();
        
        $this->voteModel->where(['voteid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $data               = [];
        $data['ischeckbox'] =  1;
        $data['fromtime']   = date('Y-m-d H:i:s', time());
        $data['totime']     = date('Y-m-d H:i:s', time()+(3600*24*30));
        $data['enabled']    =  1;
        
        $this->assign('site', $site);
        $this->assign('skin', $this->siteTheme);
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 修改主题
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['fromtime']   = strtotime($data['fromtime']);
        $data['totime']     = strtotime($data['totime']);
        
        if ($info['subject'] == $data['subject']) {
            $name = $info['subject'];
        } else {
            $name = $info['subject'].'→'.$data['subject'];
        }
        $where              = [];
        $where['voteid']    = $info['voteid'];
        
        $tips = '修改投票『'.$name.'』主题';
        $ress = $this->voteModel->where($where)->save($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo:
        
        $voteid = I('get.voteid', 0, 'intval');
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $where              = [];
        $where['voteid']    = $voteid;
        $data = $this->voteModel->where($where)->find();
        
        $data['fromtime']   = date('Y-m-d H:i:s', $data['fromtime']);
        $data['totime']     = date('Y-m-d H:i:s', $data['totime']);
        
        $this->assign('site', $site);
        $this->assign('skin', $this->siteTheme);
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 删除主题
     */
    public final function delete() {
        $voteid = I('get.voteid', 0, 'intval');
        $info = I('post.info', []);
        $info = $voteid ? [$voteid] : $info['voteid'];
        $data = [];
        if (empty($info)) $this->error('请选择投票主题！');
        
        foreach ($info as $key => $voteid) {
        $data['voteid'][]   = $voteid;
        $where              = [];
        $where['voteid']    = $voteid;
        $ress = $this->tabsModel->where($where)->delete();
        }
        $where              = [];
        $where['voteid']    = ['IN', $data['voteid']];
        $ress = $this->voteModel->where($where)->delete();
        
        if (!$ress) $this->error('删除投票失败！', $data);
        else $this->success('删除投票成功！', $data);
    }
    
    /**
     * 主题状态
     */
    public final function state() {
        $voteid = I('get.voteid', 0, 'intval');
        $info               = [];
        $info['voteid']     = $voteid;
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $ress2 = $this->voteModel->where($where)->find();
        $value = $ress2['enabled'] ? 0 : 1;
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $ress2 = $this->voteModel->where($where)->save([
            'enabled' => $value
        ]);
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $data2 = $this->voteModel->where($where)->find();
        $oper2 = $data2['enabled'] ? '关闭' : '开启';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'投票『'.$data2['subject'].'』主题';
        $tips2.= $state;
        cms_writelog($tips2, ['voteid' => $info['voteid']]);
        
        echo json_encode($data2['enabled']);
    }
    
    /**
     * 配置主题
     */
    public final function setting() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['maxval'] = $data['maxval'] ? : $data['option'];
        $tips = '配置投票『'.$info['subject'].'』主题';
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $ress = $this->voteModel->where($where)->save($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $voteid = I('get.voteid', 0, 'intval');
        
        $where              = [];
        $where['voteid']    = $voteid;
        $data = $this->voteModel->where($where)->find();
        
        $data['fromtime']   = date('Y-m-d H:i:s', $data['fromtime']);
        $data['totime']     = date('Y-m-d H:i:s', $data['totime']);
        
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 获取皮肤（主题）
     */
    public final function skin() {
        $siteid = I('get.siteid', 0, 'intval');
        if ($siteid == 0) {
            echo json_encode($this->siteTheme);
            return ;
        }
        $array              = [];
        $where              = [];
        $where['siteid']    = $siteid;
        $theme = $this->siteModel->where($where)->find()['theme'];
        
        foreach ($this->siteTheme as $key => $val) {
            if ($theme != $val['dirname']) continue;
            $array[] = $val;
        }
        echo json_encode($array);
    }
    
    /**
     * 投票图表
     */
    public final function chart() {}
    
}
