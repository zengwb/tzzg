<?php

namespace Vote\Controller;

use \Common\Controller\iPageController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 投票模块页面控制器类：配置或实例化应用模块
 * 
 * @author T-01
 */
abstract class PageController extends iPageController {
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iPageController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
    }
    
}
