<?php

namespace Vote\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 投票模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    protected   $vote       = [],
                $data       = [];
    
    /**
     * {@inheritDoc}
     * @see \Vote\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->vote         = D('Vote/Vote');
        $this->data         = D('Vote/VoteData');
    }
    
    public final function index() {
        echo __METHOD__;
    }
    
    public final function number() {
        $voteid = I('get.voteid', 0, 'intval');
        $where = ['voteid' => $voteid];
        echo $this->vote->where($where)->find()['maxval'];
    }
    
    public final function vote() {
        $voteid = I('get.voteid', 0, 'intval');
        $option = I('get.option', []);
        
        foreach ($option as $optionid) {
        $data = [];
        $data['voteid'] = $voteid;
        $data['optionid'] = $optionid;
        $data['user']   = 'unknown'; // @todo: 用户登陆后的SESSON
        $data['inputtime'] = time();
        $data['ip']     = get_client_ip();
        $data['userinfo'] = $_SERVER['HTTP_USER_AGENT'];
        $this->data->add($data);
            
        $where = [];
        $where['voteid'] = $voteid;
        $this->vote->where($where)->setInc('number');
        }
        
        echo 1;
//         echo json_encode($voteid.'-'.$option);
    }
    
}
