<?php

namespace Vote\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 投票模块选项管理控制器类：呈现选项CURD常规管理
 * 
 * @author T-01
 */
final class OptionController extends BaseController {
    
    public      $action     = [];
    
    private     $voteModel  = [],
                $tabsModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Vote\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->voteModel    = D('Vote');
        $this->tabsModel    = D('VoteOption');
    }
    
    /**
     * 选项管理
     */
    public final function index() {
        $voteid = I('get.voteid', 0, 'intval');
        $info               = [];
        $info['voteid']     = $voteid;
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $ress = $this->voteModel->where($where)->find();
        
        $info['subject']    = $ress['subject'];
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $ress = $this->tabsModel->where($where)->order(
            '`listorder` DESC, `optionid` DESC'
        )->select();
        foreach ($ress as $key => $row) $data[$key] = $row;
        
        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['optionid'] as $key => $optionid) {
        $listorder = $this->tabsModel->where([
            'optionid' => $optionid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->tabsModel->where(['optionid' => $optionid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $optionid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('投票选项排序成功！', $listorder);
    }
    
    /**
     * 添加选项
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['voteid']    = $info['voteid'];
        $vote = $this->voteModel->where($where)->find();
        
        $data['voteid']     = $info['voteid'];
        $data['siteid']     = $vote['siteid'];
        
        $nums = $this->tabsModel->where($where)->count();
        $tips = '投票主题『'.$vote['subject'].'』选项已达最大值！';
        if ($nums >= $vote['option']) $this->error($tips);
        
        $tips = '添加投票『'.$data['option'].'』选项';
        $ress = $this->tabsModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        $inid = $this->tabsModel->getLastInsID();
        
        $this->tabsModel->where(['optionid' => $inid])->save([
            'listorder' => $inid
        ]);
        $args = ['voteid' => $info['voteid']];
        $this->success($tips.'成功！', '', U('index', $args));
        } // @todo: 
        
        $voteid = I('get.voteid', 0, 'intval');
        
        $where              = [];
        $where['voteid']    = $voteid;
        $vote = $this->voteModel->where($where)->find();
        
        $data               = [];
        $data['enabled']    =  1;
        $data['subject']    = $vote['subject'];
        $data['voteid']     = $vote['voteid'];
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 修改选项
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        if ($info['option'] == $data['option']) {
            $name = $info['option'];
        } else {
            $name = $info['option'].'→'.$data['option'];
        }
        $where              = [];
        $where['optionid']  = $info['optionid'];
        
        $tips = '修改投票『'.$name.'』选项';
        $ress = $this->tabsModel->where($where)->save($data);
        $args = ['voteid' => $info['voteid']];
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index', $args));
        } // @todo: 
        
        $optionid = I('get.optionid', 0, 'intval');
        
        $where              = [];
        $where['optionid']  = $optionid;
        $data = $this->tabsModel->where($where)->find();
        
        $where              = [];
        $where['voteid']    = $data['voteid'];
        $ress = $this->voteModel->where($where)->find();
        
        $data['subject']    = $ress['subject'];
        
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 删除选项
     */
    public final function delete() {
        $optionid = I('get.optionid', 0, 'intval');
        $info = I('post.info', []);
        $info = $optionid ? [$optionid] : $info['optionid'];
        $data = [];
        if (empty($info)) $this->error('请选择投票选项！');
        
        foreach ($info as $key => $optionid) {
        $data['optionid'][] = $optionid;
        }
        $where              = [];
        $where['optionid']  = ['IN', $data['optionid']];
        $ress = $this->tabsModel->where($where)->delete();
        
        if (!$ress) $this->error('删除投票选项失败！', $data);
        else $this->success('删除投票选项成功！', $data);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $optionid = I('get.optionid', 0, 'intval');
        $info               = [];
        $info['optionid']   = $optionid;
        
        $where              = [];
        $where['optionid']  = $info['optionid'];
        $ress2 = $this->tabsModel->where($where)->find();
        $value = $ress2['enabled'] ? 0 : 1;
        
        $where              = [];
        $where['optionid']  = $info['optionid'];
        $ress2 = $this->tabsModel->where($where)->save([
            'enabled' => $value
        ]);
        
        $where              = [];
        $where['optionid']  = $info['optionid'];
        $data2 = $this->tabsModel->where($where)->find();
        $oper2 = $data2['enabled'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'投票『'.$data2['option'].'』选项';
        $tips2.= $state;
        cms_writelog($tips2, ['optionid' => $info['optionid']]);
        
        echo json_encode($data2['enabled']);
    }
    
}
