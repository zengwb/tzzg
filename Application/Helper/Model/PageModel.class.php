<?php

namespace Helper\Model;

use \Common\Model\iPageModel;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 助手模块页面数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
abstract class PageModel extends iPageModel {}
