<?php

namespace Helper\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 助手模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    /**
     * {@inheritDoc}
     * @see \Helper\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    public final function index() {
        echo __METHOD__;
    }
    
    public final function search() {
        $key = I('post.key', '', 'cms_addslashes');
        echo __METHOD__ . '   ' . $key;
    }
    
}
