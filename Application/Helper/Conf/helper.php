<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Helper',
    'version'               => 'v1.0.01',
    
    'name'                  => 'Helper',
    'description'           => '扩展模块',
    'core'                  => false,
    'model'                 => true,
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
    
];
