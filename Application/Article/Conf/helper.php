<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Article',
    'version'               => 'v1.0.01',
    
    'name'                  => '内容模块',
    'description'           => '内容模块',
    'core'                  => true,
    'model'                 => true,
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
    
];
