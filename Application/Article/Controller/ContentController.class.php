<?php

namespace Article\Controller;

use Think\Upload;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块内容管理控制器类：呈现内容CURD常规管理
 * 
 * @author T-01
 */
final class ContentController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'lists', 'page', 'category', 'position',
        'add', 'edit', 'delete', 'state', 'relation', 'import'
    ];
    
    private     $image      = null,
                $upload     = null,
                $forms      = null,
                $catid      = [],
                
                $modelModel = [],
                $fieldModel = [],
                
                $cateModel  = [],
                $privModel  = [],
                $pageModel  = [],
                
                $treeField  = ['catid', 'catname', 'parentid'],
                
                $userModel  = [],
                $siteModel  = [],
                
                $formModel  = [],
                $posiModel  = [],
                
                $linkModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->dialog('ArticleAction2');
        
        $this->modelModel   = D('Admin/Model');
        $this->fieldModel   = D('Admin/ModelField');
        
        $this->cateModel    = D('Admin/Category');
        $this->privModel    = D('Admin/CategoryPriv');
        $this->pageModel    = D('Admin/Pages');
        
        $this->formModel    = D('Admin/Forms');
        $this->posiModel    = D('Article/Position');
        
        $this->siteModel    = cms_theme_config(RES_PATH.'Template/');
        $this->upload       = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Article/';
        
        $this->image        = (new \Think\Image());
        $this->forms        = (new \Common\Widget\FormsWidget());

        $this->userModel    = D('Admin/Admin');
        
        $where              = [];
        $where['username']  = session('CMS_UserName');
        $user = $this->userModel->where($where)->find();
        $temp = unserialize($user['setting']);
        $menu = $temp['menu'] ? explode(',', $temp['menu']) : [];
        $priv = $this->privModel->catid(session('CMS_RoleID'));
        $this->catid = $menu ? : $priv;
        
        $this->linkModel    = D('Admin/Linkage');
    }
    
    /**
     * 内容管理
     */
    public final function index() {
        $modelid = I('get.modelid', 0, 'intval');
        $catid = I('get.catid', 0, 'intval');
        
        $this->assign('modelid', $modelid);
        $this->assign('catid', $catid);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->find();
        if($cates){
            $where              = [];
            $where['modelid']   = $cates['modelid'];
            $model = $this->modelModel->where($where)->find();
            $table = $model['table'];
            if($table){
                foreach ($data['mid'] as $key => $mid) {
                $where              = [];
                $where['mid']       = $mid;
                $listorder = D($table)->where($where)->find()['listorder'];
                if ($listorder == $data['listorder'][$key]) {
                    continue;
                }
                D($table)->where($where)->save([
                    'listorder' => $data['listorder'][$key]
                ]);
                $listorder = $mid.'='.$data['listorder'][$key];
                $list['listorder'][] = $listorder;
                }
            }
        }
        $this->success('内容排序成功！', $list ?: 'null');
    }
    
    /**
     * 内容列表
     */
    public final function lists() {
        if (IS_POST) {
        $info = I('post.info', ['modelid' => 0, 'catid' => 0]);
        
        $modelid = I('request.modelid',    0, 'intval');
        $modelid = I('get.modelid', $modelid, 'intval');
        $modelid = I('post.modelid',$modelid, 'intval');
        $modelid = $modelid ?: $info['modelid'];
        
        $catid = I('request.catid',  0, 'intval');
        $catid = I('get.catid', $catid, 'intval');
        $catid = I('post.catid',$catid, 'intval');
        $catid = $catid ?: $info['catid'];
        
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        $keyword = trim($keyword);
        
        $args = [];
        $args['modelid'] = $modelid;
        $args['catid'] = $catid;
        $args['key'] = $keyword;
        $this->redirect('lists', $args);
        exit();
        }
        $modelid = I('get.modelid',  0, 'intval');
        $catid = I('request.catid',  0, 'intval');
        $catid = I('get.catid', $catid, 'intval');
        $catid = I('post.catid',$catid, 'intval');
        
        $info = I('post.info', []);
        $info['catid'] = $catid ? : $info['catid'];
        
        $where              = [];
        $where['catid']     = $info['catid'];
        
        $ress = $this->cateModel->where($where)->find();
        $type = $ress['type'];
        $name = $ress['catname'];
        
        $this->assign('catid', $info['catid']);
        $this->assign('name', $name ? : '内容搜索');
        
        if ($info['catid'] == 0) {
        $where              = [];
        $where['display']   =  1;
        $where['type']      =  0;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['catid']     = ['IN', $this->catid];
        
        $where['modelid']   = $modelid;
        if (!$modelid) unset($where['modelid']);
        
        if (empty($this->catid)) unset($where['catid']);
        $category = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        $options  = "<li data-catid='\$catid'>";
        $options .= "<a>\$spacer<span>\$catname</span></a>";
        $options .= "</li>";
        
        $cattree  = cms_tree_menu(
            $category, 0,
            $options, '', null, ['field' => $this->treeField]
        );
        $where              = [];
        $where['module']    = MODULE_NAME;
        $where['disabled']  = 0;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        $model = $this->modelModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->find();
        $modelid = I('request.modelid',    0, 'intval');
        $modelid = I('get.modelid', $modelid, 'intval');
        $modelid = I('post.modelid',$modelid, 'intval');
        $modelid = $modelid ? : $model['modelid'];
        
        $where              = [];
        $where['modelid']   = $modelid;
        $table = $this->modelModel->where($where)->find()['table'];
        $table = $table ? : $model['table'];
        if (empty($table)) exit(); // @todo: 终止脚本
        
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        $keyword = trim($keyword);
        
        $catidss = $this->cateModel->childid(0);
        
        $where              = [];
        $where['title']     = ['LIKE', '%'.$keyword.'%'];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        if (empty($keyword)) unset($where['title']);
        if (empty($keyword)) $where['catid']= ['IN', $catidss];
        
        $nums = D($table)->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $ress = D($table)->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order('`listorder` DESC, `inputtime` DESC')->select();
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title2'] = $row['title'];
        else $row['title2'] = strtr($row['title'], $keys);
        $row['realname'] = $this->userModel->where(['username' => $row['username']])->find()['realname'] ?: $row['username'];
        
        $where              = [];
        $where['mid']       = $row['mid'];
        $views = D($table.'_data')->where($where)->find();
        $row['views'] = $views['views'] ?: 0;
        
        $param = ['catid' => $row['catid'], 'id' => $row['mid']];
        $links = $this->domain.U('Home/Index/shows', $param);
        $row['links'] = $row['url'] ?: $links ;
        
        $row['style'] = unserialize($row['title_style']);
        $data[$key] = $row;
        }
        
        $where              = [];
        $where['module']    = MODULE_NAME;
        $where['modelid']   = ['IN', [0, $modelid]];
        $where['catid']     = ['IN', [0, $catidss]];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['status']    = 1;
        
        $ress = $this->posiModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->select();
        foreach ($ress as $key => $row) $position[$key] = $row;
            
        $this->assign('cattree', $cattree);
        $this->assign('links', cms_site_info()['domain']);
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->assign('list', $position);
        $this->display();
        }
        if ($type == 0) {
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->find();
        $modelid = $cates['modelid'];
        
        $where              = [];
        $where['modelid']   = $modelid;
        $model = $this->modelModel->where($where)->find();
        
        $where              = [];
        $where['module']    = MODULE_NAME;
        $where['modelid']   = ['IN', [0, $modelid]];
        $where['catid']     = ['IN', [0, $info['catid']]];
        $where['status']    = 1;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        $ress2 = $this->posiModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->select();
        foreach ($ress2 as $key => $row) $posids[$key] = $row;
        
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        $keyword = trim($keyword);
        
        $catid = $this->cateModel->childid($info['catid']);
        
        $table = $model['table'] ? : exit();
        $where              = [];
        $where['title']     = ['LIKE', '%'.$keyword.'%'];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        if (empty($keyword)) unset($where['title']);
        
        $where['catid'] = ['IN', $catid];
//         if (empty($keyword)) $where['catid'] = $info['catid'];
//         else $where['catid']= ['IN', $catid];
        
        $nums = D($table)->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $ress = D($table)->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order('`listorder` DESC, `inputtime` DESC')->select();
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title2'] = $row['title'];
        else $row['title2'] = strtr($row['title'], $keys);
        
        $where              = [];
        $where['mid']       = $row['mid'];
        $views = D($table.'_data')->where($where)->find();
        $row['views'] = $views['views'] ?: 0;
        $row['realname'] = $this->userModel->where(['username' => $row['username']])->find()['realname'] ?: $row['username'];
        
        $param = ['catid' => $row['catid'], 'id' => $row['mid']];
        $links = $this->domain.U('Home/Index/shows', $param);
        $row['links'] = $row['url'] ?: $links ;
        
        $row['style'] = unserialize($row['title_style']);
        $data[$key] = $row;
        }

        $where              = [];
        $where['module']    = MODULE_NAME;
        $where['modelid']   = ['IN', [0, $modelid]];
        $where['catid']     = ['IN', [0, $info['catid']]];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['status']    = 1;
        
        $ress = $this->posiModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->select();
        foreach ($ress as $key => $row) $position[$key] = $row;
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->assign('list', $position);

        $this->assign('modelid', $modelid); // @todo: 模型ID
        $this->display();
        } else {
        $where              = [];
        $where['catid']     = $info['catid'];
        $data = $this->pageModel->where($where)->find();
        $info = [];
        $info['catid']      = $data['catid'];
        $info['pagetitle']  = $data['pagetitle'];

        $link               = [];
        $link['catid']      = $info['catid'];
        $site = substr(cms_site_info()['domain'], 1, -1);
        
        $this->assign('data', $data);
        $this->assign('info', $info);
        $this->assign('style', unserialize($data['style']));
        $this->assign('links', $site.U('Home/Index/lists',$link));
        $this->display('page');
        }
    }
    
    /**
     * 单页管理
     */
    public final function page() {
        if (!IS_POST && !I('post.dosubmit')) return ;
        
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $data['style'] = serialize(I('post.style', []));
        $data['updatetime'] = time();
        
        // 写入附件
        $files = $_SESSION['CMS_FILES'];
        if ($files) {
        foreach ($files as $key => $file) {
        $param = [];
        $param['module']    = MODULE_NAME;
        $param['catid']     = $info['catid'];
        
        $param['filename']  = $file['original'];
        $param['filesize']  = $file['size'];
        $param['fileext']   = $file['type'];
        
        $param['isimage']   = 1;
        $param['filepath']  = $file['url'];
        cms_attachment($param); // @todo: 
        unset($param);
        }
        unset($_SESSION['CMS_FILES']);
        }
        if ($info['pagetitle'] == $data['pagetitle']) {
        $title = $data['pagetitle'];
        } else {
        $title = $info['pagetitle'];
        $title = $title ? $title.'→'.$data['pagetitle'] : '';
        }
        $title = $title ? '『'.$title.'』' : '';
        $where              = [];
        $where['catid']     = $info['catid'];
        $page = $this->pageModel->where($where)->save($data);
        
        $tips = '更新单页'.$title.'内容';
        
        if (!$page) $this->error($tips.'失败！');
        else $this->success($tips.'成功！');
    }
    
    /**
     * 栏目列表
     */
    public final function category() {
        // @todo: 以下脚本重置栏目显示条件
        $modelid = I('get.modelid', 0, 'intval');
        $catid = I('get.catid', 0, 'intval');
        
        if ($catid) $catids = $this->cateModel->childid($catid);
        else $catids = '';
        $catidarr = explode(',', $catids);
        
        $incatids = [];
        foreach ($this->catid as $key => $val) {
            if ($catid && in_array($val, $catidarr))
                $incatids[] = $val;
        }
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['type']      = ['NEQ', 1];
//         $where['display']   =  1;
        $where['catid']     = ['IN', $incatids]; // @todo: 原取 $this->catid
        
        if (empty($this->catid)) unset($where['catid']);
        if ($modelid) $where['modelid'] = $modelid;
        $where['display']   =  1;
        
        $array = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        foreach ($array as $key => $row) {
        $param = ['catid' => $row['catid']];
        $row['linkage'] = U('Content/lists', $param);
        $row['style'] = '';
        
        $where              = [];
        $where['parentid']  = $row['catid'];
        $where['display']   =  1;
        $cates = $this->cateModel->where($where)->find();
        if ($cates) {
        $row['style'] = 'category';
        $count = $this->cateModel->where($where)->count();
        $row['catname'].= '（'.$count.'）';
        $row['linkage'] = 'javascript:void(0);';
        }
        $lists[$key] = $row;
        }
        $text = "<li class='\$style'>\$spacer<a ";
        $text.= "href='javascript:void(0);' data-link='\$linkage' ";
        $text.= "class='cms-c666' ";
        $text.= "target='content'>\$catname</a></li>";
        
        $category = cms_tree_menu(
            $lists, 0,
            $text, '', [], ['field' => $this->treeField]
        );
        $this->assign('catetree', $category);
        $this->display();
    }
    
    /**
     * 内容推送
     */
    public final function position() {
        $info = I('post.info', []);
        $data = I('post.data', []);
        
        $where              = [];
        $where['catid']     = $info['catid'];
//         $where['catid']     = ['IN', $this->catid];
//         if (empty($this->catid)) unset($where['catid']);
        $cates = $this->cateModel->where($where)->find();
//         print_r($where); print_r($cates); exit();

        $where              = [];
        $where['modelid']   = $cates['modelid'];
        $model = $this->modelModel->where($where)->find();
        $table = $model['table'];
        
        foreach ($info['mid'] as $key => $mid) {
        $where              = [];
        $where['mid']       = $mid;
        $ress2 = D($table)->where($where)->find();
        
        $posiidarr = explode(',', $ress2['posiids']);
        $array = [];
        foreach ($posiidarr as $i => $posiid) {
        if ($posiid == $data['posiid'] || empty($posiid)) continue;
        $array[$i] = $posiid;
        }
        $array[] = $data['posiid'];
        $save2 = ['posiids' => implode(',', $array)];
        D($table)->where($where)->save($save2);
        
        $main = D($table)->where($where)->find();
        $temp               = [];
        $temp['contentid']  = $mid;
        $temp['catid']      = $info['catid'];
        $temp['posiid']     = $data['posiid'];
        $temp['listorder']  = $main['listorder'] ?: 0;
        
        $text               = [];
        $text['title']      = $main['title'];
        $text['description']= $main['description'];
        $text['thumb']      = $main['thumb'];
        $text['style']      = $main['title_style'];
        $text['subtitle']   = $main['subtitle'];
        $text['inputtime']  = time();
        
        D('Article/PositionData')->insert($temp, $text);
        }
        $where              = [];
        $where['posiid']    = $data['posiid'];
        $ress2 = $this->posiModel->where($where)->find();
        $this->success('推送内容到『'.$ress2['name'].'』位置成功！');
    }
    
    /**
     * 添加内容
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $main = I('post.main', [], 'cms_addslashes');
        $data = I('post.data', [], 'cms_addslashes');
        
        $tcss = I('post.style',[]);
        foreach ($tcss as $field => $style) {
        $main[$field]       = serialize($style);
        }
        $main['inputtime']  = time();
        $main['username']   = session('CMS_UserName');
        
        $main['relation']   = implode(',', $main['relation']);
        $main['updatetime'] = time();
        
        $main['posiids']    = implode(',', $main['posiids']);
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->find();
        $modelid = $cates['modelid'];
        
        if ($main['thumb']) {
        $main['thumb'] = strtr($main['thumb'], ['/./' => './']);
        
        $where              = [];
        $where['modelid']   = $modelid;
        $where['field']     = 'thumb';
        $field = $this->fieldModel->where($where)->find();
        
        $setting = unserialize($field['setting']);
        $width = $setting['width'];
        $height= $setting['height'];
        
        $this->image->open($main['thumb']);
        $this->image->thumb($width, $height, 2)->save(
            $main['thumb'], null, 100
        );
        }
        $where              = [];
        $where['modelid']   = $modelid;
        $model = $this->modelModel->where($where)->find();
        $table = $model['table'];
        
        $main = cms_article_field_content_filter_string($main);
//         print_r($main); exit();

        $tips = '添加内容『'.$main['title'].'』信息';
        $ress = D($table)->add($main); // @todo: 添加主表

        if (!$ress) $this->error($tips.'失败！');
        $inid = D($table)->getLastInsID();

            //>>>添加审批申请 返回array(code=> boolean,message=>string)
            $approve_model = A('Article/Approve');
            $message = $approve_model->add($table,$ress);
        
//         if (empty($main['url'])) {
//         $link = ['url' => $link];
//         D($table)->where(['mid' => $inid])->save($link);
//         }
        // 写入附件
        $files = $_SESSION['CMS_FILES'];
        if ($files) {
        foreach ($files as $key => $file) {
        if (empty($file['url'])) continue;
        $param = [];
        $param['module']    = MODULE_NAME;
        $param['catid']     = $info['catid'];
        
        $param['filename']  = $file['original'];
        $param['filesize']  = $file['size'];
        $param['fileext']   = $file['type'];
        
        $param['isimage']   = 1;
        $param['filepath']  = $file['url'];
        if ($file['filepath'] == $main['thumb'])
            $file['isthumb']= 1;
        cms_attachment($param); // @todo:
        unset($param);
        }
        unset($_SESSION['CMS_FILES']);
        }
        D($table)->where(['mid' => $inid])->save(['listorder' => $inid]);
        $data['mid']        = $inid;
        
        $data = cms_article_field_content_filter_string($data);
        D($table.'_data')->add($data);
        
        // 判断推送
        $posiidarr = explode(',', $main['posiids']);
        foreach ($posiidarr as $key => $posiid) {
        if (empty($posiid)) continue;
        $data               = [];
        $data['contentid']  = $inid;
        $data['catid']      = $info['catid'];
        $data['posiid']     = $posiid;
        $data['listorder']  = $inid;
        
        $text               = [];
        $text['title']      = $main['title'];
        $text['description']= $main['description'];
        $text['thumb']      = $main['thumb'];
        $text['style']      = $main['title_style'];
        $text['subtitle']   = $main['subtitle'];
        $text['inputtime']  = time();
        D('Article/PositionData')->insert($data, $text);
        }
            $tips .= '|'.$message['message'];
        $this->success($tips.'成功！');
        } // @todo: 
        
        $catid = I('get.catid', 0, 'intval');
        $info = ['catid' => $catid];
        
        $where              = [];
        $where['catid']     = $info['catid'];
        
        $ress = $this->cateModel->where($where)->find();
        $name = $ress['catname'];

        $where              = [];
        $where['modelid']   = $ress['modelid'];
        $where['display']   =  1;
        $where['disabled']  =  0;
        
        $field = $this->fieldModel->where($where)->order(
            '`listorder` ASC, `fieldid` ASC'
        )->select();
        foreach ($field as $key => $row) {
        if ($row['minlength']) {
        $row['required']    = ' required';
        $row['th2style']    = ' cms-cf30';
        } else {
        $row['required']    = '';
        $row['th2style']    = ' cms-cccc';
        }
        $where              = [];
        $where['elements']  = $row['formtype'];
        $form = $this->formModel->where($where)->find();

        $row['setting']     = $row['setting'] ?: $form['setting'];
        $row['name']        = $row['alias'];
        $setting = unserialize($row['setting']);

        if (method_exists($this->forms, $row['formtype'])) {
        $param              = [];
        $param['minlength'] = $row['minlength'];
        $param['maxlength'] = $row['maxlength'];

        $param['ismain']    = $row['ismain'];
        $param['field']     = $row['field'];
        $param['value']     = $setting['default'];

        $param['name']      = $row['alias'];
        $param['description'] = $row['name'];

        $param['setting']   = $row['setting'];
        $param['style']     = $row['style'];
        $param['modelid']   = $row['modelid'];

        $method = [$this->forms, $row['formtype']];
        $row['element']     = call_user_func($method, $param);
        } else {
        $row['element']     = '';
        }
        if (!$row['isbase']) $tagR[$key] = $row;
        else $tagL[$key]    = $row;
        }

        $this->assign('name', $name);
        $this->assign('tagL', $tagL);
        $this->assign('tagR', $tagR);
        
        $this->assign('catid', $info['catid']);
        $this->display('action');
    }
    
    /**
     * 修改内容
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $main = I('post.main', [], 'cms_addslashes');
        $data = I('post.data', [], 'cms_addslashes');
        
        $tcss = I('post.style',[]);
        foreach ($tcss as $field => $style) {
        $main[$field]       = serialize($style);
        }
        $main['updatetime'] = time();
//         $main['username']   = session('CMS_UserName');
        
        $main['relation']   = implode(',', $main['relation']);
        $main['posiids']    = implode(',', $main['posiids']);
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->find();
        $modelid = $cates['modelid'];
        
        if ($main['thumb']) {
        $main['thumb'] = strtr($main['thumb'], ['/./' => './']);
        
        $where              = [];
        $where['modelid']   = $modelid;
        $where['field']     = 'thumb';
        $field = $this->fieldModel->where($where)->find();
        
        $setting = unserialize($field['setting']);
        $width = $setting['width'];
        $height= $setting['height'];
        
        $this->image->open($main['thumb']);
        $this->image->thumb($width, $height, 2)->save(
            $main['thumb'], null, 100
        );
        }
        $where              = [];
        $where['modelid']   = $modelid;
        $model = $this->modelModel->where($where)->find();
        $table = $model['table'];
        
        if ($info['title'] == $main['title']) $title = $info['title'];
        else $title = $info['title'].'→'.$main['title'];
        
        $where              = [];
        $where['mid']       = $info['mid'];

        $main = cms_article_field_content_filter_string($main);
        $data = cms_article_field_content_filter_string($data);

//         print_r($main);
//         print_r($data);
//         exit();
        $tips = '修改内容『'.$title.'』信息';
        $ress = D($table)->where($where)->save($main);
        D($table.'_data')->where($where)->save($data);
        
        // 写入附件
        $files = $_SESSION['CMS_FILES'];
        if ($files) {
        foreach ($files as $key => $file) {
        if (empty($file['url'])) continue;
        $param = [];
        $param['module']    = MODULE_NAME;
        $param['catid']     = $info['catid'];
        
        $param['filename']  = $file['original'];
        $param['filesize']  = $file['size'];
        $param['fileext']   = $file['type'];
        
        $param['isimage']   = 1;
        $param['filepath']  = $file['url'];
        if ($file['filepath']  == $main['thumb'])
            $file['isthumb']= 1;
        cms_attachment($param); // @todo:
        unset($param);
        }
        unset($_SESSION['CMS_FILES']);
        }
        // 判断推送
        $posiidarr = explode(',', $main['posiids']);
        foreach ($posiidarr as $key => $posiid) {
        if (empty($posiid)) continue;
        $data               = [];
        $data['contentid']  = $info['mid'];
        $data['catid']      = $info['catid'];
        $data['posiid']     = $posiid;
        
        $text               = [];
        $text['title']      = $main['title'];
        $text['description']= $main['description'];
        $text['thumb']      = $main['thumb'];
        $text['style']      = $main['title_style'];
        $text['subtitle']   = $main['subtitle'];
        $text['inputtime']  = time();
        
        D('Article/PositionData')->update($data, $text);
        }
        if (empty($main['posiids'])) {
        $where              = [];
        $where['catid']     = $info['catid'];
        $where['contentid'] = $info['mid'];
        D('Article/PositionData')->clear($where);
        }
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！');
        } // @todo: 
        
        $catid  = I('get.catid', 0, 'intval');
        $id     = I('get.id',    0, 'intval');

        $type     = I('get.type',    0, 'intval');//预览  type=9

        $info               = [];
        $info['catid']      = $catid;
        $info['mid']        = $id;
        
        $where              = [];
        $where['catid']     = $info['catid'];
        
        $ress = $this->cateModel->where($where)->find();
        $name = $ress['catname'];
        
        $where              = [];
        $where['modelid']   = $ress['modelid'];
        $model = $this->modelModel->where($where)->find();
        $table = $model['table'];
        
        $where              = [];
        $where['mid']       = $info['mid'];
        $main = D($table)->where($where)->find();
        $data = D($table.'_data')->where($where)->find();
        
        $where              = [];
        $where['modelid']   = $ress['modelid'];
        $where['display']   =  1;
        $where['disabled']  =  0;
        
        $field = $this->fieldModel->where($where)->order(
            '`listorder` ASC, `fieldid` ASC'
        )->select();
        foreach ($field as $key => $row) {
        if ($row['minlength']) {
        $row['required']    = ' required';
        $row['th2style']    = ' cms-cf30';
        } else {
        $row['required']    = '';
        $row['th2style']    = ' cms-cccc';
        }
        $where              = [];
        $where['elements']  = $row['formtype'];
        $form = $this->formModel->where($where)->find();
        
        $row['setting']     = $row['setting'] ?: $form['setting'];
        $row['name']        = $row['alias'];
        $setting = unserialize($row['setting']);
        
        if (method_exists($this->forms, $row['formtype'])) {
        $param              = [];
        $param['minlength'] = $row['minlength'];
        $param['maxlength'] = $row['maxlength'];
        
        $param['ismain']    = $row['ismain'];
        $param['field']     = $row['field'];
        
        if ($row['ismain']) {
        $param['value']     = $main[$row['field']];
        } else {
        $param['value']     = $data[$row['field']];
        }
        $param['value']     = $param['value'] ?: $setting['default'];
        
        $param['name']      = $row['alias'];
        $param['description'] = $row['name'];
        
        $param['setting']   = $row['setting'];
        $param['style']     = $main['style'] ?: $row['style'];
        
        $param['ttcss']     = $main[$row['field'].'_style'];
        $param['modelid']   = $row['modelid'];
        
        $method = [$this->forms, $row['formtype']];
        $row['element']     = call_user_func($method, $param);
        } else {
        $row['element']     = '';
        }
        if (!$row['isbase']) $tagR[$key] = $row;
        else $tagL[$key] = $row;
        }
        $this->assign('name', $name);
        
        $main = cms_article_field_content_filter_array($main);
        $data = cms_article_field_content_filter_array($data);
        
        $this->assign('main', $main);
        $this->assign('data', $data);
        
        $this->assign('tagL', $tagL);
        $this->assign('tagR', $tagR);

        $this->assign('type', $type);//get参数 type=9  预览

        $this->assign('catid', $info['catid']);
        $this->display('action');
    }
    
    /**
     * 删除内容
     */
    public final function delete() {
        $catid = I('get.catid', 0, 'intval');
        $id    = I('get.id',    0, 'intval');
        $info  = I('post.info', []);
        
        if ($catid && $id) $info = ['catid' => $catid, 'mid' => [$id]];
        if (empty($info['mid'])) $this->error('请选择栏目内容！');
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->find();
        $modelid = $cates['modelid'];
        
        $where              = [];
        $where['modelid']   = $modelid;
        $model = $this->modelModel->where($where)->find();
        $table = $model['table'];
        
        foreach ($info['mid'] as $key => $mid) {
        // 删除缩略图
        $where              = [];
        $where['mid']       = $mid;
        $ress2 = D($table)->where($where)->find();
        
        $thumb = $ress2['thumb'];
        $thumb = strtr($thumb, ['/./' => './']);
        if (file_exists($thumb)) @unlink($thumb);
        }
        $where              = [];
        $where['catid']     = $info['catid'];
        $where['mid']       = ['IN', $info['mid']];
        
        $ress = D($table)->where($where)->delete();
        D($table.'_data')->where($where)->delete();
        
        $where              = [];
        $where['contentid'] = ['IN', $info['mid']];
        $where['catid']     = $info['catid'];
        D('Article/PositionData')->where($where)->delete();
        
        if (!$ress) $this->error('删除内容失败！', $info);
        else $this->success('删除内容成功！', $info);
    }
    
    /**
     * 显示状态
     */
    public final function state() {
        $catid = I('get.catid', 0, 'intval');
        $id     = I('get.id',    0, 'intval');
        
        $info               = [];
        $info['catid']      = $catid;
        $info['mid']        = $id;
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $cates = $this->cateModel->where($where)->find();
        
        $where              = [];
        $where['modelid']   = $cates['modelid'];
        $model = $this->modelModel->where($where)->find();
        $table = $model['table'];
        
        $where              = [];
        $where['catid']     = $info['catid'];
        $where['mid']       = $info['mid'];
        
        $state = D($table)->where($where)->find()['disabled'];
        $value = $state ? 0 : 1;
        
        $ress2 = D($table)->where($where)->save(['disabled' => $value]);
        $data2 = D($table)->where($where)->find();
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $oper2 = $data2['disabled'] ? '审核' : '初始';
        
        $tips2 = $oper2.'『'.$data2['title'].'』';
        cms_writelog($tips2.$state, $info);
        
        echo json_encode($data2['disabled']);
    }
    
    /**
     * 相关内容
     */
    public final function relation() {
        $modelid = I('request.modelid',    0, 'intval');
        $modelid = I('get.modelid', $modelid, 'intval');
        $modelid = I('post.modelid',$modelid, 'intval');
        
        $catid   = I('request.catid',      0, 'intval');
        $catid   = I('get.catid',     $catid, 'intval');
        $catid   = I('post.catid',    $catid, 'intval');
        
        $mid     = I('request.mid',        0, 'intval');
        $mid     = I('get.mid',         $mid, 'intval');
        $mid     = I('post.mid',        $mid, 'intval');
        
        $where              = [];
        $where['modelid']   = $modelid;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $model = $this->modelModel->where($where)->find();
        if (empty($model['table'])) exit();
        
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        $keyword = trim($keyword);
        
        $where              = [];
        $where['title']     = ['LIKE', '%'.$keyword.'%'];
        $where['disabled']  =  0;
        if (empty($keyword)) unset($where['title']);
        if ($mid) $where['mid'] = ['NEQ', $mid];
        
        $catids = D('Admin/Category')->childid($catid);
        if ($catid) $where['catid'] = ['IN', $catids];
        
        $nums = D($model['table'])->where($where)->count();
        $rows = 12; //C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        $page->parameter['modelid'] = $modelid;
        $page->parameter['catid'] = $catid;
        $page->parameter['mid'] = $mid;
        
        $ress = D($model['table'])->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order('`inputtime` DESC, `mid` DESC')->select();
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title2'] = $row['title'];
        else $row['title2'] = strtr($row['title'], $keys);
        
        $param = ['catid' => $row['catid'], 'id' => $row['mid']];
        $links = U('Home/Index/shows', $param);
        $row['links'] = $row['url'] ?: $links ;
        
        $row['style'] = unserialize($row['title_style']);
        $data[$key] = $row;
        }
        
        $where              = [];
        $where['modelid']   = $modelid;
        $where['display']   =  1;
        $where['type']      =  0;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['catid']     = ['IN', $this->catid];
        if (empty($this->catid)) unset($where['catid']);
        
        $category = $this->cateModel->where($where)->order(
            '`listorder` ASC, `catid` ASC'
        )->select();
        
        $options  = "<li data-catid='\$catid'>";
        $options .= "<a>\$spacer<span>\$catname</span></a>";
        $options .= "</li>";
        
        $cattree  = cms_tree_menu(
            $category, 0,
            $options, '', null, ['field' => $this->treeField]
        );
        
        $where              = [];
        $where['catid']     = $catid;
        $cates = $this->cateModel->where($where)->find();
        
        $this->assign('modelid', $modelid);
        $this->assign('catid', $catid);
        $this->assign('mid', $mid);
        
        $this->assign('catname', $cates['catname']);
        $this->assign('cattree', $cattree);
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 导入内容37模型
     */
    public final function import_content37() {
        $this->dialog('ArticleContentImportContent');
        if (IS_POST) {
        $info = I('post.info', []);
        $catid = $info['catid'];
        $cates = $this->cateModel->where(['catid' => $catid])->find();
        $model = $this->modelModel->where(['modelid' => $cates['modelid']])->find();
        
        $table = $model['table'];
        
        $this->upload->rootPath .= 'ProjectTtemplate/';
        
        if (empty($_FILES['filename']['error']) &&
            isset($_FILES['filename'])) {
        $this->upload->saveName = 'data_'.time();
        
        $file = $this->upload->upload();
        if (!$file) $this->popup(false, $this->upload->getError().'上传内容模板');
        $filename = $this->upload->rootPath.$file['filename']['savepath'].$file['filename']['savename'];
        }
        vendor('PHPExcel.PHPExcel');
        
        $reader = $this->reader($filename);
        $phpexcel = $reader->load($filename, 'utf-8');
        $sheet = $phpexcel->getSheet(0); // 取第一张工作薄
        
        $totals = $sheet->getHighestRow();      // 总行数
        $column = $sheet->getHighestColumn();   // 总列表
        
        $ok_count = 0;    // 成功记录数
        $no_conut = 0;    // 失败记录数
        
        $to_count = 0;    // 记录总条数
        
        $t_base = [];   // 基础表
        $t_data = [];   // 数据表
        
        for ($key = 3; $key < $totals; $key++) {
        $to_count++;
        
        $t_base['catid'] = $catid;
        $t_base['number']= $phpexcel->getActiveSheet()->getCell("A".$key)->getValue();
        $t_base['title'] = $phpexcel->getActiveSheet()->getCell('B'.$key)->getValue();
        if (empty($t_base['title'])) continue;
        
        // 市州
        $sz = $phpexcel->getActiveSheet()->getCell('C'.$key)->getValue();
        $sz = $this->linkModel->get_attribute_id($sz) ?: 0;
        // 区县
        $qx = $phpexcel->getActiveSheet()->getCell('D'.$key)->getValue();
        $qx = $this->linkModel->get_attribute_id($qx) ?: 0;
        // 省市
        $ss = $this->linkModel->get_attribute_parentid($sz);
        
        $ss = $ss ?: '';
        $sz = $sz ? ','.$sz : '';
        $qx = $qx ? ','.$qx : '';
        
        $t_base['cityid'] = $ss.$sz.$qx;
        $t_base['address']= $phpexcel->getActiveSheet()->getCell('D'.$key)->getValue();
        
        $industry = $phpexcel->getActiveSheet()->getCell('E'.$key)->getValue();
        $industryid = $this->linkModel->get_attribute_id($industry) ?: 0;
        
        $t_base['industryid'] = $industryid;
        
        $t_base['international_type'] = $phpexcel->getActiveSheet()->getCell('F'.$key)->getValue();
        $t_base['city_type'] = $phpexcel->getActiveSheet()->getCell('G'.$key)->getValue();
        $t_base['province_type'] = $phpexcel->getActiveSheet()->getCell('H'.$key)->getValue();
        
        $t_base['content'] = $phpexcel->getActiveSheet()->getCell('I'.$key)->getValue();
        
        $t_base['total_amount'] = $phpexcel->getActiveSheet()->getCell('J'.$key)->getValue();
        $t_base['investment'] = $phpexcel->getActiveSheet()->getCell('K'.$key)->getValue();
        
        $cooper = $phpexcel->getActiveSheet()->getCell('L'.$key)->getValue();
        $cooperarray = explode('、', $cooper);
        $cooper = [];
        foreach ($cooperarray as $i => $val) {
            if (empty($val)) continue;
            $cooper[$i] = $this->linkModel->get_attribute_id($val) ?: 0;
        }
        $t_base['cooperation'] = implode(',', $cooper);
        
        $t_base['company'] = $phpexcel->getActiveSheet()->getCell('M'.$key)->getValue();
        $t_base['contact1'] = $phpexcel->getActiveSheet()->getCell('N'.$key)->getValue();
        $t_base['contact2'] = $phpexcel->getActiveSheet()->getCell('O'.$key)->getValue();
        
        $t_base['inputtime'] = time();
        $t_base['updatetime'] = time();
        $t_base['username'] = session('CMS_UserName');
        
        $inid = D($table)->add($t_base);
        D($table)->where(['mid' => $inid])->save(['listorder' => $inid]);
        if ($inid) $ok_count++;
        else $no_conut++;
        
        $t_data['mid'] = $inid;
        D($table.'_data')->add($t_data);
        }
        $this->popup(true, $cates['name'].'内容导入');
        }
        $this->assign('catid', I('get.catid', 0, 'intval'));
        $this->display('import');
    }
    
    /**
     * 导入项目
     */
    public final function import() {
        $this->dialog('ArticleContentImport');
        if (IS_POST) {
        $info = I('post.info', []);
        $catid = $info['catid'];
        $cates = $this->cateModel->where(['catid' => $catid])->find();
        $model = $this->modelModel->where(['modelid' => $cates['modelid']])->find();
        
        $table = $model['table'];
        
        $this->upload->rootPath .= 'ProjectTtemplate/'; // 设置项目模板上传目录
        
        if (empty($_FILES['filename']['error']) &&
            isset($_FILES['filename'])) {
        $this->upload->saveName = 'project_'.time();
        
        $file = $this->upload->upload();
        if (!$file) $this->popup(false, $this->upload->getError().'上传项目模板');
        $filename = $this->upload->rootPath.$file['filename']['savepath'].$file['filename']['savename'];
        }
        vendor("PHPExcel.PHPExcel");
        $reader = $this->reader($filename);
        $phpexcel = $reader->load($filename, 'utf-8');
        $sheet = $phpexcel->getSheet(0); // 取第一张工作薄
        
        $totals = $sheet->getHighestRow();      // 总行数
        $column = $sheet->getHighestColumn();   // 总列表
        
        $ok_count = 0;    // 成功记录数
        $no_conut = 0;    // 失败记录数
        
        $to_count = 0;    // 记录总条数
        
        $t_base = [];   // 基础表
        $t_data = [];   // 数据表
        
        for ($key = 2; $key < $totals; $key++) {
        $to_count++;
        
        $t_base['catid'] = $catid;
        $t_base['title'] = $phpexcel->getActiveSheet()->getCell('B'.$key)->getValue();
        if (empty($t_base['title'])) continue;
        
        $city = $phpexcel->getActiveSheet()->getCell('C'.$key)->getValue();
        $city = $this->linkModel->get_attribute_id($city) ?: 0;
        
        $county = $phpexcel->getActiveSheet()->getCell('D'.$key)->getValue();
        $t_base['address'] = $county;
        
        $county = $this->linkModel->get_attribute_id($county) ?: 0;
        $province = $this->linkModel->get_attribute_parentid($city) ?: 0;
        $t_base['city'] = $province.','.$city.','.$county;
        
        $typeid1 = $phpexcel->getActiveSheet()->getCell('E'.$key)->getValue();
        $typeid1 = $this->linkModel->get_attribute_id($typeid1, 3288) ?:0 ;
        $typeid2 = $phpexcel->getActiveSheet()->getCell('F'.$key)->getValue();
        $typeid2 = $this->linkModel->get_attribute_id($typeid2, $typeid1) ?: 0;
        $t_base['industry_typeid'] = $typeid1.','.$typeid2;
        
        $t_base['investment'] = $phpexcel->getActiveSheet()->getCell('H'.$key)->getValue();
        $t_base['attract_investment'] = $phpexcel->getActiveSheet()->getCell('I'.$key)->getValue();
        
        $cooper = $phpexcel->getActiveSheet()->getCell('J'.$key)->getValue();
        $cooperarray = explode('、', $cooper);
        $cooper = [];
        foreach ($cooperarray as $i => $val) {
            if (empty($val)) continue;
            $cooper[$i] = $this->linkModel->get_attribute_id($val) ?: 0;
        }
        $t_base['cooperation'] = implode(',', $cooper);
        
        $t_base['company'] = $phpexcel->getActiveSheet()->getCell('K'.$key)->getValue();
        
        $contact = $phpexcel->getActiveSheet()->getCell('L'.$key)->getValue();
        $contactarray = explode("\n", $contact);
        $contact = [];
        $contact_index = 0;
        for ($i = 0; $i < count($contactarray); $i++) {
            if ($i % 2 != 0 || !$contactarray[$i]) continue;
            $contact_index++;
            $contact[$contact_index] = $contactarray[$i].' '.$contactarray[$i+1];
        }
        $t_base['contact'] = $contact;
        $t_base['inputtime'] = time();
        $t_base['updatetime'] = time();
        $t_base['username'] = session('CMS_UserName');
        
        $inid = D($table)->add($t_base);
        D($table)->where(['mid' => $inid])->save(['listorder' => $inid]);
        if ($inid) $ok_count++;
        else $no_conut++;
        
        $t_data['mid'] = $inid;
        $t_data['content'] = $phpexcel->getActiveSheet()->getCell('G'.$key)->getValue();
        
        D($table.'_data')->add($t_data);
        }
        $this->popup(true, $cates['name'].'项目导入');
        } // @todo: POST请求
        
        $this->assign("catid", I("get.catid", 0, 'intval'));
        $this->display();
    }
    
    /**
     * Excel版本兼容
     * 
     * @param string    $file       必填。文件
     * 
     * @return \PHPExcel_Reader_CSV
     */
    public final function reader($file) {
        if (!file_exists($file)) die('没有找到文件！');
        $extension = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        
        switch ($extension) {
            case 'xlsx':
                $reader = new \PHPExcel_Reader_Excel2007();
                break;
            case 'xls':
                $reader = new \PHPExcel_Reader_Excel5();
                break;
            default :
                $reader = new \PHPExcel_Reader_CSV();
                break;
        }
        return $reader;
    }

    /**
     * 添加城市介绍
     */
    public final function cityintroduce(){
        if(IS_POST){
            $data['introduce']  = htmlspecialchars_decode(I('post.data')['content']);

            $data['linkageid']  = I('post.linkageid');

            $res = D("Article/Linkage")->save($data);
            if($res){
                $this->success('成功！');
            }else{
                $this->success("失败");
            }
        }else if(IS_GET){
            $data = D("Article/Linkage")->where(['display'=>1,'linkageid'=>I('get.linkageid')])->field('introduce')->find();
            $this->assign('linkageid',I('get.linkageid'));
            $this->assign("content",$data);
            $this->display();
        }
    }

    /**
     * 导入61模型内容（金融机构）
     */
    public final function import_content_jrjg(){
        $this->dialog('ArticleContentImportContent');
        if (IS_POST) {
            $info = I('post.info', []);
            $catid = $info['catid'];
            $cates = $this->cateModel->where(['catid' => $catid])->find();
            $model = $this->modelModel->where(['modelid' => $cates['modelid']])->find();

            $table = $model['table'];
            $this->upload->rootPath .= 'ProjectTtemplate/'; // 设置项目模板上传目录


            if (empty($_FILES['filename']['error']) && isset($_FILES['filename'])) {
                $this->upload->saveName = 'project_'.time();

                $file = $this->upload->upload();
                if (!$file) $this->popup(false, $this->upload->getError().'上传金融机构模板');
                $filename = $this->upload->rootPath.$file['filename']['savepath'].$file['filename']['savename'];
            }

            vendor("PHPExcel.PHPExcel");
            $reader = $this->reader($filename);
            $phpexcel = $reader->load($filename, 'utf-8');
            $sheet = $phpexcel->getSheet(0); // 取第一张工作薄

            $totals = $sheet->getHighestRow();      // 总行数
            $column = $sheet->getHighestColumn();   // 总列表

            $ok_count = 0;    // 成功记录数
            $no_conut = 0;    // 失败记录数

            $to_count = 0;    // 记录总条数

            $t_base = [];   // 基础表

            $typeids = D('Admin/linkage')->where(['parentid'=>4081,'display'=>1])->select();

            for ($key = 2; $key <= $totals; $key++) {
                $to_count++;

                $t_base[$key]['catid'] = $catid;
                $t_base[$key]['title'] = $phpexcel->getActiveSheet()->getCell('A' . $key)->getValue();
                $val = $phpexcel->getActiveSheet()->getCell('B' . $key)->getValue();
                foreach ($typeids as $k => $v){
                    if(trim($val) == trim($v['name'])){
                        $t_base[$key]['typeid'] = $typeids[$k]['linkageid'];
                    }
                }

                $t_base[$key]['inputtime'] = time();
                $t_base[$key]['updatetime'] = time();
                $t_base[$key]['username'] = session('CMS_UserName');
                $t_base[$key]['listorder'] = $key-1;
                if ($inid) $ok_count++;
                else $no_conut++;
            }

            $t_base = array_values($t_base);
            $inid = D('Admin/tzzgJrjg')->addAll($t_base);
            $this->popup(true, $cates['name'].'导入内容');

        }

        $this->assign("catid", I("get.catid", 0, 'intval'));
        $this->display('import');
    }

    /**
     * 导入金控平台 基金数据(暂行)
     */
    public final function import_content_fund(){
        $this->dialog('ArticleContentImportContent');
        if(IS_POST){
            $info = I('post.info', []);
            $catid = $info['catid'];
            $cates = $this->cateModel->where(['catid' => $catid])->find();
            $model = $this->modelModel->where(['modelid' => $cates['modelid']])->find();

            $table = $model['table'];
            $this->upload->rootPath .= 'ProjectTtemplate/'; // 设置项目模板上传目录


            if (empty($_FILES['filename']['error']) && isset($_FILES['filename'])) {
                $this->upload->saveName = 'project_'.time();

                $file = $this->upload->upload();
                if (!$file) $this->popup(false, $this->upload->getError().'上传金融基金模板');
                $filename = $this->upload->rootPath.$file['filename']['savepath'].$file['filename']['savename'];
            }

            vendor("PHPExcel.PHPExcel");
            $reader = $this->reader($filename);
            $phpexcel = $reader->load($filename, 'utf-8');
            $sheet = $phpexcel->getSheet(0); // 取第一张工作薄

            $totals = $sheet->getHighestRow();      // 总行数
            $column = $sheet->getHighestColumn();   // 总列表

            $ok_count = 0;    // 成功记录数
            $no_conut = 0;    // 失败记录数

            $to_count = 0;    // 记录总条数

            $t_base = [];   // 基础表


            $data = [];
            $typeids = D("Article/Linkage")->where(["parentid" => 4100])->select();

            for($key = 2; $key <= $totals; $key++){
                $t_base[$key]['title'] =$phpexcel->getActiveSheet()->getCell('A'.$key)->getValue();

                $val = $phpexcel->getActiveSheet()->getCell('B'.$key)->getValue();
                foreach ($typeids as $k => $v){
                    if(trim($val) == trim($v['name'])){
                        $t_base[$key]['typeid'] = $typeids[$k]['linkageid'];
                    }
                }
                $t_base[$key]['company'] = $phpexcel->getActiveSheet()->getCell('C'.$key)->getValue() ? :'';
                $t_base[$key]['catid'] = 218;
                $t_base[$key]['inputtime'] = time();
                $t_base[$key]['updatetime'] = time();
                $t_base[$key]['username'] = session('CMS_UserName');
                $t_base[$key]['listorder'] = $key-1;
            }

            //去重
            $t_base = array_unique($t_base, SORT_REGULAR);
            //键值重新排序
            $t_base = array_values($t_base);

            $inid = D('Article/tzzgFund')->addAll($t_base);
            $inid == true ? $this->popup(true, $cates['name'].'导入内容') : $this->popup(false, $cates['name'].'导入内容');

        }

        $this->assign("catid", I("get.catid", 0, 'intval'));
        $this->display('import');
    }
}
