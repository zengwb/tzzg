<?php

namespace Article\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块推送内容控制器类：呈现内容CURD常规管理
 * 
 * @author T-01
 */
final class PositionDataController extends BaseController {
    
    public      $action     = [];
    
    private     $appsModel  = [],
                $modeModel  = [],
                
                $cateModel  = [],
                $posiModel  = [],
                $dataModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->appsModel    = D('Admin/Module');
        $this->modeModel    = D('Admin/Model');
        
        $this->cateModel    = D('Admin/Category');
        $this->posiModel    = D('Position');
        $this->dataModel    = D('PositionData');
    }
    
    /**
     * 内容管理
     */
    public final function index() {
        $posiid = I('get.posiid', 0, 'intval');
        
        $where              = [];
        $where['posiid']    = $posiid;
        $position = $this->posiModel->where($where)->find();
        
        $ress = $this->dataModel->where($where)->order(
            '`listorder` ASC, `dataid` ASC'
        )->select();
        
        foreach ($ress as $key => $row) {
        $row['content'] = unserialize($row['content']);
        
        $where              = [];
        $where['posiid']    = $row['posiid'];
        $posii = $this->posiModel->where($where)->find();
        
        $where              = [];
        $where['module']    = $posii['module'];
        $apps2 = $this->appsModel->where($where)->find();
        $row['module']      = $apps2['name'];
        
        $where              = [];
        $where['modelid']   = $posii['modelid'];
        $model = $this->modeModel->where($where)->find();
        $row['model']       = $model['name'];
        
        $data[$key]         = $row;
        }
        $this->assign('position', $position);
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['dataid'] as $key => $dataid) {
        $listorder = $this->dataModel->where([
            'dataid' => $dataid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->dataModel->where(['dataid' => $dataid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $dataid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('推送内容排序成功！', $listorder);
    }
    
    /**
     * 移除内容
     */
    public final function remove() {
        $dataid = I('get.dataid', 0, 'intval');
        $posiid = I('get.posiid', 0, 'intval');
        
        $info   = I('post.info', []);
        $data = [];
        
        $info['dataid'] = $dataid ? [$dataid] : $info['dataid'];
        $info['posiid'] = $posiid ? : $info['posiid'];
        
        if (empty($info['dataid'])) $this->error('请选择推送内容！');
        
        foreach ($info['dataid'] as $key => $dataid) {
        $where              = [];
        $where['dataid']    = $dataid;
        $ress2 = $this->dataModel->where($where)->find();
        
        $where              = [];
        $where['catid']     = $ress2['catid'];
        $cates = $this->cateModel->where($where)->find();
        
        $where              = [];
        $where['modelid']   = $cates['modelid'];
        $model = $this->modeModel->where($where)->find();
        $table = $model['table'];
        
        // 更新内容
        $where              = [];
        $where['mid']       = $ress2['contentid'];
        $posii = D($table)->where($where)->find()['posiids'];
        
        $array = explode(',', $posii);
        $iival = [];
        foreach ($array as $i => $val) {
            if (empty($val) || $val == $info['posiid']) continue;
            $iival[] = $val;
        }
        $posii = implode(',', $iival);
        D($table)->where($where)->save(['posiids' => $posii]);
        
        $data['dataid'][] = $dataid;
        }
        $where              = [];
        $where['dataid']    = ['IN', $data['dataid']];
        $where['posiid']    = $info['posiid'];
        $ress2 = $this->dataModel->where($where)->delete();
        
        if (!$ress2) $this->error('移除推送内容失败！', $data);
        else $this->success('移除推送内容成功！', $data);
    }
    
}
