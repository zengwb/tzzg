<?php

namespace Article\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块专题管理控制器类：呈现专题CURD常规管理
 * 
 * @author T-01
 */
final class SpecialController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state',
    ];
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    /**
     * 专题管理
     */
    public final function index() {
        echo __METHOD__;
    }
    public final function listorder() {}
    public final function add() {}
    public final function edit() {}
    public final function delete() {}
    public final function state() {}
    
}
