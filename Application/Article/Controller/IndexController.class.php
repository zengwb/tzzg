<?php

namespace Article\Controller;

use function PHPSTORM_META\type;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 内容模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
     private     $visit;
    /**
     * {@inheritDoc}
     * @see \Article\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->visit = (
            new \Home\Controller\VisitController()
        );
        $this->assign('search', substr($this->search, 1));
    }
    
    public final function index() {
        if(checkmobile()){
            $this->redirect('Home/index/bookbuyerRegistor');
        }
        $this->assign('site', $this->site);
        $this->assign('seo', cms_site_seo());
        $this->display('Article:index.html');
    }
    
    public final function lists() {
        $catid  = I('get.catid', 0, 'intval');
        $cates = I('get.catdir', '');
        $area_id = I('get.area_id',24,'intval');

        $where              = [];
        $where['siteid']    = cms_siteid();
        $where['catdir']    = $cates;
        $ress = $this->category->where($where)->find();
        $catid = $catid ? : $ress['catid'];
        $where              = [];
        $where['siteid']    = cms_siteid();
        $where['catid']     = $catid;
        $ress = $this->category->where($where)->find();
        if ($ress['linkage']) {
            header('location: '.$ress['linkage']);
        }
        $tips = '找不到您要访问的栏目，请检查或联系网站管理员！';
        if (!$ress) $this->error($tips, '', U('index'));
        extract($ress);
        //记录页面访问次数
        //$this->visit->index($catid);
        $setting = unserialize($setting);
        $catesub = $this->category->childid($catid, cms_siteid());
        
        $catetpl = $setting['categorytpl'] ? : 'category.html';
        $listtpl = $setting['listtpl'] ? : 'list.html';
        $pagetpl = $setting['pagetpl'] ? : 'page.html';

        $page = I('get.page', 0, 'intval');
        if ($type == 0) {
        $template = is_numeric($catesub) ? $listtpl : $catetpl;
        $array = explode(',', $catesub);
        
        $this->assign('tag', count($array) > 2 ? '_频道' : '_列表');
        
        
        $this->assign('catdir', $cates);
        $this->assign('parentid', $parentid);
        
        $this->assign('site', $this->site);
        $this->assign('seo', cms_site_seo($catid));

        } else {
        $this->page->where(['catid' => $catid])->setInc('views');
        $data = $this->page->where(['catid' => $catid])->find();
        
        $content  = $data['content'];
        $template = $data['template'] ? : $pagetpl;
        $seo = cms_site_seo($catid, [
            'title'         => $data['pagetitle'],
            'description'   => $data['description'],
            'keywords'      => $data['keywords'],
        ]);
        $this->assign('tag', '_单页');
        $this->assign('catdir', $cates);
        $this->assign('site', $this->site);
        $this->assign('seo', $seo);
        
        $this->assign('title', $data['pagetitle']);
        $this->assign('content', htmlspecialchars_decode($content));
        $this->assign('time', $data['updatetime']);
        $this->assign('views', $data['views']);
//         $this->assign('data', $data);
        }
//        dump($template);die;
        $this->assign('area_id', $area_id);
        $this->assign('catid', $catid);
        $this->display('Article:'.$template);
    }
    
    public final function shows() {
        $catid  = I('get.catid', 0, 'intval');
        $cates = I('get.catdir', '');
        $id     = I('get.id', 0, 'intval');
        $acc = I('get.acc');
        $where              = [];
        $where['siteid']    = cms_siteid();
        $where['catdir']    = $cates;
        $ress = $this->category->where($where)->find();

        $catid = $catid ? : $ress['catid'];

        $where              = [];
        $where['siteid']    = cms_siteid();
        $where['catid']     = $catid;
        $ress = $this->category->where($where)->find();

        $tips = '找不到您要访问的栏目，请检查或联系网站管理员！';
        if (!$ress) $this->error($tips, '', U('index'));
        extract($ress);

        $where              = [];
        $where['modelid']   = $modelid;
        $table = $this->apps->where($where)->find()['table'];
        //dump($table);die;
        $where              = [];
        $where['mid']       = $id;
        $bases = D($table)->where($where)->find();
        $datas = D($table.'_data')->where($where)->find();

        $params = I("get.");

        $_where = [];
        if(isset($params['units'])){
            $_where['units'] = $params['units'];
        }
        if(isset($params['typeid'])){
            $_where['typeid'] = $params['typeid'];
        }

        $privs = D($table)->where($_where)->where(['mid' => ['lt', $id]])->where(array('catid'=>$catid))->find();
        $nexts = D($table)->where($_where)->where(['mid' => ['gt', $id]])->where(array('catid'=>$catid))->find();

        if ($bases['url']) header('location: '.$bases['url']);
        $data = $datas ? array_merge($bases, $datas) : $bases;

        $tips = '找不到您要访问的信息，请检查或联系网站管理员！';
        if (!$data) $this->error($tips, '', U('index'));
        
        $setting = unserialize($setting);
        extract($data);
        
        $template = $template ? : $setting['showtpl'];
        $seo = cms_site_seo($catid, [
            'title'         => $title,
            'description'   => $description,
            'keywords'      => $keywords,
        ]);
        $where              = [];
        $where['username']  = $username;
        $realname = D('Admin/admin')->where($where)->find()['realname'];

        $caturl  = $this->site['domain'];
        $caturl .= $this->category->catalog($catid)['link'];
        
        $data['catname']    = $ress['catname'];
        $data['realname']   = $realname;
        $data['views']      = $data['views']; // ? : rand(1, 6);
        $data['content']    = cms_stripslashes($data['content']);
        if($table=='tzzg_jzfu_xm'){
            if(isset($data['cityid'])){
                $data['cityid'] = $this->city($data['cityid']);
            }
            if(isset($data['linkageid'])){
                $data['linkageid'] = $this->city($data['linkageid']);
            }
            if(isset($data['hzfs'])){
                $data['hzfs'] = $this->city($data['hzfs']);
            }
            if(isset($data['jscsd'])){
                $data['jscsd'] = $this->city($data['jscsd']);
            }
            if(isset($data['zrj'])){
                $data['zrj'] = $this->city($data['zrj']);
            }
            if(isset($data['zrdx'])){
                $data['zrdx'] = $this->city($data['zrdx']);
            }
            if(isset($data['fwlx'])){
                $data['fwlx'] = $this->city($data['fwlx']);
            }
            if(isset($data['linkageid_bu'])){
                $data['linkageid_bu'] = $this->city($data['linkageid_bu']);
            }
            if($acc){
                $this->assign('acc', $acc);  
            }
        }
        if($table=='tzzg_fpxm'){
            $data['is_helpproject'] = 1;
            $data['industryid'] = $data['linkageid'];
            $data['finacing_style'] = $data['hzfs'];
            $data['total_amount'] = $data['price_all'];
            
        }
        //dump($data);die;
        if(isset($data['leader'])){
            $arr = [];
            if(strpos($data['leader'], ';')){
                $leaders = explode(';', $data['leader']);
                foreach ($leaders as $k=>$v){
                    if(strpos($v, '/')){
                        $ins = explode('/', $v);
                        $arr[$k] = implode("&nbsp;&nbsp;", $ins);
                    }
                }
            }
            $data['leader'] = $arr;
        }
        $this->assign('catid', $catid);
        $this->assign('catdir', $ress['catdir']);
        
        $this->assign('content', $data);
        $this->assign('privs', $privs);
        $this->assign('nexts', $nexts);
        $this->assign('site', $this->site);
        $this->assign('tag', '_阅读');
//        dump($privs);
//        dump($nexts);
//        die;
        $this->assign('seo', $seo);
        $this->assign('caturl', $caturl);
        $this->assign('id', $id);
        $this->display('Article:'.$template);
    }
    public final function city($str)
    {
        if(strstr($str,',')){
            $arr['linkageid'] = ['in',explode(',', $str)];
            //dump($arr['linkageid']);die;
        }else{
            $arr['linkageid'] = $str;
        }
        $data = D('linkage')->field('name')->where($arr);
        if(strstr($str,',')){
            $re = $data->select();
            //dump($re);die;
            $re = implode(',', array_column($re, 'name'));
        }else{
            $re = $data->find()['name'];
        }
        return $re;
    }
    
    public final function demo2() {
        $this->assign('site', $this->site);
        $this->display('Article:demo.html');
    }
    
    public final function ajax2() {
        header("Access-Control-Allow-Origin:*");
        
        $ishtml = cms_config_array('Article', 'base')['cat_link'];
        $search = $ishtml ? 'search.html?' : substr(U('home/search/lists'),1).'&';
        
        $catid = I('get.catid', 0, 'intval');
        $key   = I('get.key', '', 'cms_addslashes');
        $limit = I('get.limit', '') ? : 16;
        
        $catids= $this->category->childid($catid);
        
        $where              = [];
        $where['catid']     = $catid;
        $ress = $this->category->where($where)->find();
        
        $where              = [];
        $where['module']    = 'Article';
        $where['disabled']  =  0;
        $where['siteid']    = cms_siteid();
        $order              = '`listorder` ASC';
        
        $ress = $this->apps->where($where)->order($order)->find();
        $modelid = $ress['modelid'];
        $table = $ress['table'];
        
        $where              = [];
        $where['catid']     = ['IN', $catids];
        $where['title']     = ['LIKE', '%'.$key.'%'];
        if (empty($key)) unset($where['title']);
        
        $ress = D($table)->where($where)->limit($limit)->order(
            '`inputtime` DESC'
        )->select();
        
        $list =[];
        foreach ($ress as $key => $row) {
        $row['id'] = $row['mid'];
        $row['keywords'] = strtr($row['keywords'], ['，' => ',']);
//         $row['keywords'] = explode(',', $row['keywords']);
        $row['inputtime'] = date('Y年m月d日', $row['inputtime']);
        $row['style'] = unserialize($row['style']);
        $row['thumb'] = strtr($row['thumb'], ['./' => '/']);
        $row['description'] = mb_substr($row['description'], 0, 72, 'utf8');
        
        $args               = [];
        $args['catid']      = $row['catid'];
        $args['id']         = $row['mid'];
        $link = U('home/index/shows', $args);
        $html = 'show-'.$row['catid'].'-'.$row['mid'].'.html';
        $link = $ishtml ? $html : $link;
        $row['link'] = $row['url'] ? : $link;
        $row['search'] = $this->site['domain'].$search;
        $list[$key] = $row;
        }
        echo json_encode($list);
    }

    /**
     * [ajaxLinkage 根据父级id获取子级菜单]
     * @return [type] [description]
     */
    public final function ajaxLinkage($parentid = 0) {

        if(I('get.pid')){
            $parentid = I('get.pid');
        }

        $where = [];
        $where['parentid'] = $parentid;
        $where['display'] = 1;
        $ress = D('Admin/Linkage')->where($where)->select();

        $this->ajaxReturn($ress);
    }

    /**
     * ajax获取政府招商项目列表
     */
    public final function ajaxGetProList(){
        $params = I('get.');

        foreach ($params as $key => $v) {
            if(!$v){
                unset($params[$key]);
            }elseif ($key == "cityid") {
                if(strpos($v, '%')){
                    $params[$key] = array('like', $v);
                }
            }elseif ($key == "cooperation") {
                $params[$key] = array('like', "%" . $v . "%");
            }elseif ($key == "total_amount") {
                if(strpos($v, "-")){
                    $a = explode("-",$v);
                    $params['total_amount'] = array(array('GT', (double)$a[0]),array('ELT', (double)$a[1]), 'AND');
                }else if($params[$key] != 0) {
                    $params['total_amount'] = array('GT',(double)$v);
                }else {
                    unset($params['total_amount']);
                }
            }elseif ($key == "keyWords") {
                $params['title'] = array('like', "%" . $v . "%");
                unset($params[$key]);
            }
        }
        //处理一级产业
        if(isset($params['industryid1'])){
            $where = [];
            $where['parentid'] = $params['industryid1'];
            $where['display'] = 1;

            $ress = D('Admin/Linkage')->where($where)->select();
            $ins = [];
            foreach ($ress as $k => $v){
                $ins[] = $ress[$k]['linkageid'];
            }
            $params['industryid'] = array("IN",$ins);
            unset($params['industryid1']);
        }

        if($params['gjzl'] == '0'){
            unset($params['gjzl']);
        }

        $page = $params['page'];
        $pageNum = 5;
        unset($params['page']);

        $params['disabled'] = 0;
        $order = $params['order'];
        unset($params['order']);
        $count = D('Article/Project')->where($params)->count();
        if($order == "`total_amount` DESC"){
            $order = "`total_amount`*1 DESC";
        }

        $_ress = D('Article/Project')->where($params)->order($order)->limit(($page-1)*$pageNum,$pageNum)->select();
        $pageInfo = [
            'total' => $count,
            'pageSize'=>$pageNum,
            'curPage'=>$page,
            'totalPage'=>ceil($count/5)
        ];
        foreach ($_ress  as $key => $value) {
            $_ress[$key]['title_style'] = unserialize($_ress[$key]['title_style']);
            $_ress[$key]['regionName'] = cms_get_linkage_value($_ress[$key]['cityid']);
            $_ress[$key]['cooperation'] = cms_get_linkage_value($_ress[$key]['cooperation']);
        }

        $data = array('pro'=>$_ress,'count'=>$count, 'pageinfo'=>$pageInfo);
        $this->ajaxReturn($data);
    }
    //获取省下的部门,城市,和项目
    public final function ajaxshen(){
        $params = I('get.');
        //dump($params);die;
        $bumentype = [];
        $_where['parentid'] = $params['linkageid'];
        //获取城市
        $bumentype['city'] = D('Admin/Linkage')->field('linkageid,name')->where($_where)->order('listorder ASC')->select();
        //获取部门
        $bumen = $this->bumen($_where['parentid']);
        foreach($bumen as $k=>$v){
            if($v['parentid']==0){
                $bumentype['bumen'][] = $v;
            }else{
                $bumentype['bumen1'][] = $v;
            }
        }
        //获取项目
        $bumentype['project'] = $this->xiangmu($_where['parentid'],1,1);
        $this->ajaxReturn($bumentype);
        //dump(D()->getLastSql());die;
    }
    //获取省部门
    public final function bumen($linkageid,$parentid='a',$b=''){
        $where['linkageid'] = $linkageid;
        if($parentid != 'a'){
            $where['parentid'] = $parentid;
        }
        $where['disabled'] = 0;
        $bumen = D('Admin/Institution')->field('parentid,linkageid,name,unitid')->where($where)->order('listorder ASC')->select();
        if($b){
            $this->ajaxReturn($bumen);
        }else{
            return $bumen;
        }
    }
    //获取项目
    public final function xiangmu($city,$page,$a=''){
        $where['cityid'] = ['like','%'.$city.'%'];
        $pageNum = 5;
        $project['content'] = D('Article/Project')->where($where)->order('inputtime DESC')->limit(($page-1)*$pageNum,$pageNum)->select();
       foreach ($project['content'] as $key => $data){
            $data['cityid'] = explode(',', $data['cityid']);
            $where['linkageid'] = ['in',$data['cityid']];
            $project['content'][$key]['city'] = implode(',',array_column(D('Admin/Linkage')->field('name')->where($where)->select(),'name'));
            $data['cooperation'] = explode(',',$data['cooperation']);
            $where = [];
            $where['linkageid'] = ['in', $data['cooperation']];
            $project['content'][$key]['cooperation'] = implode(',',array_column(D('Admin/Linkage')->field('name')->where($where)->select(),'name'));
        }
        $project['page']['total'] = D('Article/Project')->where($where)->count();
        $project['page']['totalPage'] = ceil($project['page']['total']/$pageNum);
        $project['page']['curPage'] = $page;
        $project['page'][' pageSize'] = $pageNum;                                    
        if($a){
            return $project;
        }else{
            $this->ajaxReturn($project);
        }
        
    }


    /**
     * 获取部委新闻列表
     */
    public final function ajaxGetBuweiList(){
        $params = I('get.');

        $curPage = $params['curPage'];
        $pageSize = $params['pageSize'];
        $order = "`inputtime` DESC";
        $where = [];
        $where['group'] = $params['linkageid'];
        $where['typeid'] = $params['typeid'];
        $where['disabled'] = 0;

        if(strlen(trim($params['keywords'])) > 0){
            $where['title'] = array("like", "%" . $params['keywords'] ."%");
        }

        if($params['type'] == "news"){

            $count = D('Article/tzzgNews')->where($where)->count();
            $feild = "mid,catid,typeid,group,title,title_style,subtitle,thumb,description,inputtime";
            $_ress = D("Article/tzzgNews")->where($where)->order($order)->field($feild)->limit(($curPage-1)*$pageSize,$pageSize)->select();

            $pageInfo = [
                'total' => $count,
                'pageSize'=>$pageSize,
                'curPage'=>$curPage,
                'totalPage'=>ceil($count/$pageSize)
            ];

            foreach($_ress as $k => $v){
                $_ress[$k]['inputtime'] = date('Y-m-d', $v['inputtime']);
                $_ress[$k]['title'] = subtext($v['title'],28);
                $_ress[$k]['description'] = subtext($v['description'],130);
            }

            $this->ajaxReturn(array('params'=>$where, 'data'=>$_ress, 'pageinfo'=>$pageInfo, 'type'=>'news'));
        }else{

            $count = D('Article/tzzgZwproject')->where($where)->count();
            $feild = "mid,catid,group,title,inputtime,quasi_amount,industryid,cityid,finacing_style,contact_name,company";
            $_ress = D("Article/tzzgZwproject")->where($where)->order($order)->field($feild)->limit(($curPage-1)*$pageSize,$pageSize)->select();

            $pageInfo = [
                'total' => $count,
                'pageSize'=>$pageSize,
                'curPage'=>$curPage,
                'totalPage'=>ceil($count/$pageSize)
            ];

            foreach ($_ress as $k => $v){
                $_ress[$k]['inputtime'] = date('Y-m-d', $v['inputtime']);
                $_ress[$k]['quasi_amount'] = (float)$v['quasi_amount']/10000;
                $_ress[$k]['cityid'] = cms_get_linkage_value($v['cityid']);
                $_ress[$k]['industryid'] = cms_get_linkage_value($v['industryid'], false);
                $_ress[$k]['finacing_style'] = cms_get_linkage_value($v['finacing_style']);
                $_ress[$k]['title'] = subtext($v['title'],28);
            }

            $this->ajaxReturn(array('params'=>$where, 'data'=>$_ress, 'pageinfo'=>$pageInfo, 'type'=>'project'));
        }
    }

    /**
     * ajax获取国家部委新闻详细
     */
    public final function ajaxGetBuweiDetail(){
        $params = I("get.");
        $catid  = I('get.catid');
        $group = I('get.group');
        $typeid     = I('get.typeid');
        $mid     = I('get.mid');

        if($mid){
            $where = array(
                'catid' =>  $catid,
                'group' =>  $group,
                'typeid' => $typeid,
                'mid'   =>  $mid,
                'disabled' => 0
            );
        }else{
            $where = array(
                'catid' =>  $catid,
                'group' =>  $group,
                'typeid' => $typeid,
                'disabled' => 0
            );
        }
        $_ress = D("Article/tzzgNews")->where($where)->find();

        $_content = D("Article/tzzgNewsData")->where(['mid'=>$_ress['mid']])->find();
        $_ress['inputtime'] = date('Y-m-d', $_ress['inputtime']);
        $_ress = array_merge($_ress, $_content);
        if($_ress['content']){
            $_ress['content'] = cms_stripslashes($_ress['content']);
        }
        $p  = array(
            'catid' =>  $catid,
            'group' =>  $group,
            'typeid' => $typeid,
            'disabled' => 0
        );

        $privs = D("Article/tzzgNews")->where(['mid' => ['lt', $_ress['mid']]])->where($p)->order("inputtime DESC")->find();

        $nexts = D("Article/tzzgNews")->where(['mid' => ['gt', $_ress['mid']]])->where($p)->order("inputtime ASC")->find();

        $result = array(
            'data'  => $_ress,
            'privs' => $privs,
            'nexts' => $nexts
        );
        $this->ajaxReturn($result);
    }

    /**
     * 获取项目融资列表数据
     */
    public final function get_financing_project(){
        $params = I('get.');

        foreach ($params as $key => $v) {
            if(!$v){
                unset($params[$key]);
            }elseif ($key == "cityid") {
                if(strpos($v, '%')){
                    $params[$key] = array('like', $v);
                }
            }elseif ($key == "total_amount") {
                if(strpos($v, "-")){
                    $a = explode("-",$v);
                    $params['total_amount'] = array(array('GT', (double)$a[0]*10000),array('ELT', (double)$a[1]*10000), 'AND');

                }else if($params[$key] != 0) {
                    $params['total_amount'] = array('GT',(double)$v);
                }else {
                    unset($params['total_amount']);
                }
            }elseif ($key == "keyWords") {
                $params['title'] = array('like', "%" . $v . "%");
                unset($params[$key]);
            }
        }
        //处理一级产业
        if(isset($params['industryid1'])){
            $params['industryid'] = array("like", $params['industryid1'] . ",%");
            unset($params['industryid1']);
        }

        $page = $params['page'];
        $pageNum = 5;
        unset($params['page']);

        $params['disabled'] = 0;
        $order = $params['order'];
        unset($params['order']);
        $count = D('Article/tzzgFinancing')->where($params)->count();
        if($order == "`total_amount` DESC"){
            $order = "`total_amount`*1 DESC";
        }
        $field = "catid,cityid,contacts_name,contacts_tel,industryid,inputtime,mid,quasi_amount,stage,title,title_style,total_amount,company,finacing_style";
        $_ress = D('Article/tzzgFinancing')->where($params)->field($field)->order($order)->limit(($page-1)*$pageNum,$pageNum)->select();

        $pageInfo = [
            'total' => $count,
            'pageSize'=>$pageNum,
            'curPage'=>$page,
            'totalPage'=>ceil($count/5)
        ];

        foreach ($_ress  as $key => $value) {
            $_ress[$key]['title_style'] = unserialize($_ress[$key]['title_style']);
            $_ress[$key]['regionName'] = cms_get_linkage_value($_ress[$key]['cityid']);
            $_ress[$key]['finacing_style'] = cms_get_linkage_value($_ress[$key]['finacing_style']) != " "?cms_get_linkage_value($_ress[$key]['finacing_style']):"不详";
            $_ress[$key]['total_amount'] = (float)$_ress[$key]['total_amount']/10000;
            $_ress[$key]['company'] = $_ress[$key]['company'] == " "?"不详":$_ress[$key]['company'];
            $_ress[$key]['contacts_tel'] = $_ress[$key]['contacts_tel'] == " "?"不详":$_ress[$key]['contacts_tel'];
            $_ress[$key]['industryid'] = cms_get_linkage_value($_ress[$key]['industryid'],false);
            if(strpos($_ress[$key]['industryid'], " ")){
                $_ress[$key]['industryid'] = explode(" ",$_ress[$key]['industryid'])[1];
            }
        }
//        print_r($_ress);die;
        $data = array('pro'=>$_ress,'count'=>$count, 'pageinfo'=>$pageInfo);
        $this->ajaxReturn($data);
    }


    /**
     * 获取资产交易列表数据
     */
    public final function get_asset_project(){
        $params = I('get.');

        foreach ($params as $key => $v) {
            if(!$v){
                unset($params[$key]);
            }elseif ($key == "cityid") {
                if(strpos($v, '%')){
                    $params[$key] = array('like', $v);
                }
            }elseif ($key == "asset_val") {
                if(strpos($v, "-")){
                    $a = explode("-",$v);
                    $params['asset_val'] = array(array('GT', (double)$a[0]*10000),array('ELT', (double)$a[1]*10000), 'AND');

                }else if($params[$key] != 0) {
                    $params['asset_val'] = array('GT',(double)$v);
                }else {
                    unset($params['asset_val']);
                }
            }elseif ($key == "transfer_val") {
                if(strpos($v, "-")){
                    $a = explode("-",$v);
                    $params['transfer_val'] = array(array('GT', (double)$a[0]*10000),array('ELT', (double)$a[1]*10000), 'AND');

                }else if($params[$key] != 0) {
                    $params['transfer_val'] = array('GT',(double)$v);
                }else {
                    unset($params['transfer_val']);
                }
            }elseif ($key == "keyWords") {
                $params['title'] = array('like', "%" . $v . "%");
                unset($params[$key]);
            }
        }
        //处理一级产业
        if(isset($params['industryid1'])){
            $params['industryid'] = array("like", $params['industryid1'] . ",%");
            unset($params['industryid1']);
        }

        $page = $params['page'];
        $pageNum = 5;
        unset($params['page']);

        $params['disabled'] = 0;
        $order = $params['order'];
        unset($params['order']);
        $count = D('Article/tzzgTransaction')->where($params)->count();
        if($order == "`total_amount` DESC"){
            $order = "`total_amount`*1 DESC";
        }
        $field = "catid,cityid,contact_name,contact_tel,inputtime,mid,asset_val,transfer_val,title,title_style,is_helpproject,is_industrialpark,industryid,company,transaction_mode";
        $_ress = D('Article/tzzgTransaction')->where($params)->field($field)->order($order)->limit(($page-1)*$pageNum,$pageNum)->select();
        $pageInfo = [
            'total' => $count,
            'pageSize'=>$pageNum,
            'curPage'=>$page,
            'totalPage'=>ceil($count/5)
        ];

        foreach ($_ress  as $key => $value) {
            $_ress[$key]['title_style'] = unserialize($_ress[$key]['title_style']);
            $_ress[$key]['regionName'] = cms_get_linkage_value($_ress[$key]['cityid']);
            $_ress[$key]['transaction_mode'] = cms_get_linkage_value($_ress[$key]['transaction_mode']);
            $_ress[$key]['transfer_val'] = (float)$_ress[$key]['transfer_val']/10000;
            $_ress[$key]['asset_val'] = (float)$_ress[$key]['asset_val']/10000;
            $_ress[$key]['industryid'] = cms_get_linkage_value($_ress[$key]['industryid']);
            if(strpos($_ress[$key]['industryid'], " ")){
                $_ress[$key]['industryid'] = explode(" ",$_ress[$key]['industryid'])[1];
            }
        }
        $data = array('pro'=>$_ress,'count'=>$count, 'pageinfo'=>$pageInfo);
        $this->ajaxReturn($data);
    }

    /**
     * 精准投资列表
     */
    public final function get_zjtz_project(){
        $params = I('get.');
        foreach ($params as $key => $v) {
            if(!$v){
                unset($params[$key]);
            }elseif ($key == "cityid") {
                if(strpos($v, '%')){
                    $params[$key] = array('like', $v);
                }
            }elseif ($key == "total_amount") {
                if(strpos($v, "-")){
                    $a = explode("-",$v);
                    $params['total_amount'] = array(array('GT', (double)$a[0]*10000),array('ELT', (double)$a[1]*10000), 'AND');

                }else if($params[$key] != 0) {
                    $params['total_amount'] = array('GT',(double)$v);
                }else {
                    unset($params['total_amount']);
                }
            }elseif ($key == "keyWords") {
                $params['title'] = array('like', "%" . $v . "%");
                unset($params[$key]);
            }
        }
        //处理一级产业
        if(isset($params['industryid1'])){
            $params['industryid'] = array("like", $params['industryid1'] . ",%");
            unset($params['industryid1']);
        }

        $page = $params['page'];
        $pageNum = 5;
        unset($params['page']);

        $params['disabled'] = 0;
        $order = $params['order'];
        unset($params['order']);
        $count = D('Article/tzzgPreinvest')->where($params)->count();
        if($order == "`total_amount` DESC"){
            $order = "`total_amount`*1 DESC";
        }
//        $field = "catid,cityid,contacts,contact_tel,inputtime,mid,asset_val,transfer_val,title,title_style,is_helpproject,is_industrialpark,industryid,company,transaction_mode";
        $_ress = D('Article/tzzgPreinvest')->where($params)->order($order)->limit(($page-1)*$pageNum,$pageNum)->select();
        $pageInfo = [
            'total' => $count,
            'pageSize'=>$pageNum,
            'curPage'=>$page,
            'totalPage'=>ceil($count/5)
        ];

        foreach ($_ress  as $key => $value) {
            $_ress[$key]['title_style'] = unserialize($_ress[$key]['title_style']);
            $_ress[$key]['regionName'] = cms_get_linkage_value($_ress[$key]['cityid']);
            $_ress[$key]['total_amount'] = (float)$_ress[$key]['total_amount']/10000;
            $_ress[$key]['industryid'] = cms_get_linkage_value($_ress[$key]['industryid'],false);
            $_ress[$key]['finacing_style'] = cms_get_linkage_value($_ress[$key]['finacing_style'],false);
            if(strpos($_ress[$key]['industryid'], " ")){
                $_ress[$key]['industryid'] = explode(" ",$_ress[$key]['industryid'])[1];
            }
        }

        $data = array('pro'=>$_ress,'count'=>$count, 'pageinfo'=>$pageInfo);
        $this->ajaxReturn($data);

    }

    /**
     * 政务 获取省级下的机构
     */
    public final function ajaxProvinceAgency(){
        $params = I('get.');
        $params['parentid'] = 0;
        $params['disabled'] = 0;
        $res = D('Article/Institution')->where($params)->select();

        $_res = [
            'code'=>200,
            'message'=>'success',
            'data'=>$res
        ];
        $this->ajaxReturn($_res);
    }

    /**
     * 机构下事业单位
     */
    public final function ajaxAgency(){
        $params = I('get.');
        $params['disabled'] = 0;
        $res = D('Article/Institution')->where($params)->order("listorder ASC")->select();
        $_res = [
            'code'=>200,
            'message'=>'success',
            'data'=>$res
        ];
        $this->ajaxReturn($_res);
    }

    /**
     * 获取省级下的市
     */
    public final function ajaxCity(){
        $params = I('get.');
//        $where['parentid'] = $params['linkageid'];

        $res = D('Article/Linkage')->where($params)->field('linkageid,name,parentid')->order("listorder ASC")->select();
        $_res = [
            'code'=>200,
            'message'=>'success',
            'data'=>$res
        ];
        $this->ajaxReturn($_res);
    }

    /**
     * 获取省级下的部门信息
     */
    public final function ajaxProvinceBumen(){
        $params = I('get.');
        $res = D('Article/Institution')->where($params)->order("listorder ASC")->find();
        $_res = [
            'code'=>200,
            'message'=>'success',
            'data'=>$res
        ];
        $this->ajaxReturn($_res);
    }

    /**
     * 获取省级数据（2019年1月5日）
     */
    public final function getProvinceData(){
        $params = I('get.');
        $page = $params['page'];
        $size = 5;
        $params['disabled'] = 0;
        /*
        $pageinfo=[
            'total' => $count,
            'pageSize' => $size,
            'curPage'  => $page,
            'totalPage' => ceil($count/$size),
        ];
        */
        if($params['typeid'] != 4030){
        //请求新闻类数据

            $field = 'inputtime,mid,title,description,thumb,units';
            $this->ajaxReturn($this->_getNews($params, $page, $size, $field));
        }else{
        //请求项目类数据
            $field = "address,cityid,company,contact_name,contact_tel,cooperation,finacing_style,industryid,inputtime,investment,mid,quasi_amount,thumb,title,units";
            $this->ajaxReturn($this->_getProjects($params, $page, $size, $field));
        }

    }

    /**
     * 获取省级下的数据
     */
    public final function getShengjiData(){
        $params = I("get.");
        $page = $params['page'];
        $size = 5;

        if(!isset($params['unitpid']) && $params['cid'] != 4030){
            // 点击 省份  并且 分类不是 项目展示
            //只有省id和分类id
            //默认机构
            $defAgency = D('Article/Institution')->where(['linkageid'=>$params['provinceid'],'disabled'=>0])->order("`listorder` ASC")->find();
            $units = $defAgency['parentid'];
            //默认部门
            $defBumen = D('Article/Institution')->where(['parentid'=>$units,'disabled'=>0])->order("`listorder` ASC")->find();
            $units .= ",".$defBumen['unitid'];
            //查询数据
            $where = [
                'typeid' => $params['cid'],
                'disabled' => 0,
                'units'  => $units,
                'catid' => 198
            ];
            $field = 'inputtime,mid,title,description,thumb,units,typeid';
            $this->ajaxReturn($this->_getNews($where, $page, $size, $field));
        }else if(isset($params['unitpid']) && $params['cid'] != 4030){
            //有选择共享单位  并且  分类不是项目展示
            $where = [
                'catid' => 198,
                'typeid' => $params['cid'],
                'units'  => $params['unitpid'] . "," . $params['unitid'],
                'disabled' => 0
            ];
            $field = 'inputtime,mid,title,description,thumb,units,typeid';
            $this->ajaxReturn($this->_getNews($where, $page, $size, $field));
        }else if(isset($params['unitpid']) && $params['cid'] == 4030){
            //有选择共享单位  并且  分类是项目展示
            $where = [
                'units' => $params['unitpid'] . "," . $params['unitid'],
                'typeid' => $params['cid'],
                'disabled' => 0,
                'catid'    => 199
            ];
            $field = "address,cityid,company,contact_name,contact_tel,cooperation,finacing_style,industryid,inputtime,investment,mid,quasi_amount,thumb,title,units";
            $this->ajaxReturn($this->_getProjects($where, $page, $size, $field));
        }
    }

    /**
     * 获取省级下的数据   辅助方法  省级获取项目列表
     */
    public final function _getProjects($where, $page, $size, $field){
        $count = D('Article/tzzgSjproject')->where($where)->count();
        $data = D('Article/tzzgSjproject')->where($where)->limit(($page-1)*$size, $size)->field($field)->order("`listorder` ASC")->select();
        $sql = M()->getLastSql();
        foreach ($data as $k=>$v){
            $data[$k]['finacing_style'] = cms_get_linkage_value($v['finacing_style']);
            $data[$k]['cityid'] = cms_get_linkage_value($v['cityid']);
            $data[$k]['industryid'] = cms_get_linkage_value($v['industryid'], false);
            $data[$k]['quasi_amount'] = (float)$v['quasi_amount']/10000;
            $data[$k]['typeid'] = 4030;
        }
        $ress = [
            'code' => 200,
            'message' => 'success',
            'type'  => 'projects',
            'data'=>$data,
            'pageinfo'=>[
                'total' => $count,
                'pageSize' => $size,
                'curPage'  => $page,
                'totalPage' => ceil($count/$size),
            ],
            'sql' => $sql
        ];

        return $ress;
    }
    /**
     * 获取省级下的数据   辅助方式  省级获取内容
     */
    public final function _getNews($where, $page, $size, $field){
        $count = D('Article/tzzgNews')->where($where)->count();
        $data = D('Article/tzzgNews')->where($where)->field($field)->limit(($page-1)*$size, $size)->order("`listorder` ASC")->select();
        //$sql = M()->getLastSql();
       if(!empty($data)){
           foreach ($data as $k=>$v){
               $data[$k]['inputtime'] = date('Y-m-d',$v['inputtime']);
           }
       }
        return $ress = [
            'code' => 200,
            'message' => 'success',
            'type'  => 'news',
            'data'=>$data,
            'pageinfo'=>[
                'total' => $count,
                'pageSize' => $size,
                'curPage'  => $page,
                'totalPage' => ceil($count/$size),
            ],
            //'sql' => $sql
        ];
    }

    /**
     * 省级文章内容详细页
     */
    public final function shengjiShowArticle(){
        $params = I('get.');

        $where = [
            'mid' => $params['mid'],
            'catid' => 198,
            'disabled' => 0
        ];

        $field = 'mid,catid,title,thumb,inputtime,typeid,copyfrom';
        //获取信息
        $tile_info = D('Article/tzzgNews')->where($where)->field($field)->find();
        $tile_info['copyfrom'] = trim($tile_info['copyfrom']);
        //获取内容
        $content = D('Article/tzzgNewsData')->where($where)->find();
        $content['content'] = cms_stripslashes($content['content']);
        //获取省下市级
        $city = D('Article/linkage')->where(['parentid'=>$params['pid']])->select();
        //获取省下机构
        $agency = D('Article/Institution')->where(['linkageid'=>$params['pid'],'parentid'=>0])->order("`listorder` ASC")->select();

        $_where = [
            'typeid'=>$params['typeid'],
            'catid' => 198,
            'units'=>$params['units'],
            'disabled'=>0
        ];
        //下一篇
        $next = D('Article/tzzgNews')->where($_where)->where(['mid' => ['gt', $params['mid']]])->order("`inputtime` ASC")->find();
        //上一篇
        $privs = D('Article/tzzgNews')->where($_where)->where(['mid' => ['lt', $params['mid']]])->order("`inputtime` DESC")->find();

        //默认第一个机构下的部门
        $units['pid'] = explode(",", $params['units'])[0];
        $units['unitid'] = explode(",", $params['units'])[1];

        $data = array_merge($tile_info,$content);
        $data['pid'] = $params['pid'];

        $this->assign('next',$next);
        $this->assign('privs',$privs);
        $this->assign('typeid', $params['typeid']);
        $this->assign('data',$data);
        $this->assign('city',$city);
        $this->assign('agency',$agency);
        $this->assign('units', $units);
        //页面显示
        $this->display("Article:show_zwxq.html");
    }

    /**
     * 政务 省级内容  上下篇
     */
    public final function sjNextPrivsArticle(){
        $params = I('get.');
        $_where = [
            'typeid'=>$params['typeid'],
            'units'=>$params['units'],
            'disabled'=>0,
            'catid' => 198
        ];
        //下一篇
        $next = D('Article/tzzgNews')->where($_where)->where(['mid' => ['gt', $params['mid']]])->order("`inputtime` ASC")->find();
        //上一篇
        $privs = D('Article/tzzgNews')->where($_where)->where(['mid' => ['lt', $params['mid']]])->order("`inputtime` DESC")->find();

        //内容
        $where = [
            'mid' => $params['mid'],
            'disabled' => 0
        ];
        $field = "inputtime,mid,title,thumb,typeid,units,copyfrom";
        $title_info = D('Article/tzzgNews')->where($where)->field($field)->find();
        $title_info['copyfrom'] = trim($title_info['copyfrom']);
        $content = D('Article/tzzgNewsData')->where($where)->find();
        $content['content'] = cms_stripslashes($content['content']);
        $content = array_merge($title_info,$content);

        $content['inputtime'] = date('Y-m-d', $content['inputtime']);
        $res = [
            'code' => 200,
            'message' => 'success',
            'data'  => [
                'next'  =>  $next,
                'privs' =>  $privs,
                'content'   => $content
            ]
        ];
        $this->ajaxReturn($res);
    }

    /**
     * 省级下文章内容带筛选条件
     */
    public final function sjGetNews(){
        $params = I("get.");
        $where = [
            'typeid'    => $params['typeid'],
            'units'     => $params['units'],
            'disabled'  => 0,
            'catid'     => 198,
        ];
        $field = "inputtime,mid,title,thumb,typeid,units,copyfrom";
        $title_info = D('Article/tzzgNews')->where($where)->field($field)->order("`listorder` ASC")->find();
        if(is_array($title_info)){
            $title_info['inputtime'] = date('Y-m-d', $title_info['inputtime']);
            $title_info['copyfrom'] = trim($title_info['copyfrom']);
            $content = D('Article/tzzgNewsData')->where(['mid'=>$title_info['mid']])->find();
            $data = array_merge($title_info,$content); //文章内容
            $data['content'] = cms_stripslashes($data['content']);
            $_where = [
                'typeid'=>$params['typeid'],
                'units'=>$params['units'],
                'disabled'=>0,
                'catid' => 198
            ];

            //下一篇
            $next = D('Article/tzzgNews')->where($_where)->where(['mid' => ['gt', $title_info['mid']]])->order("`inputtime` ASC")->find();
            //上一篇
            $privs = D('Article/tzzgNews')->where($_where)->where(['mid' => ['lt', $title_info['mid']]])->order("`inputtime` DESC")->find();
            $res = [
                'code'  => 200,
                'message'=>'success',
                'data' =>[
                    'content' => $data,
                    'next' => $next,
                    'privs'=> $privs
                ]
            ];

        }else{
            $res = [
                'code'  => 200,
                'message'=>'the request is not have data!',
                'data' => [
                    'content' => null,
                    'next' => null,
                    'privs'=> null
                ]
            ];
        }
        $this->ajaxReturn($res);
    }

    /**
     * 获取政务-省级  ajax 项目详细
     */
    public final function shengjiShowProject(){
        $params = I('get.');
        if(isset($params['mid'])){
            $where = [
                'mid' => $params['mid'],
                'units' => $params['units'],
                'typeid' => $params['typeid'],
                'disabeld' => 0
            ];
            $order = "`inputtime` DESC";
        }else{
            $where = [
                'units' => $params['units'],
                'typeid' => $params['typeid'],
                'diabled' => 0
            ];
            $order = "`inputtime` DESC";
        }

        //获取项目信息
        $field = "address,cityid,company,contact_name,contact_tel,cooperation,finacing_style,industryid,inputtime,investment,mid,quasi_amount,thumb,title,units";
        //获取信息
        $project_info = D('Article/tzzgSjproject')->where($where)->field($field)->order($order)->find();
        if($project_info != null){
            $project_info['inputtime'] = date("Y-m-d", $project_info['inputtime']);
            $project_info['finacing_style'] = cms_get_linkage_value($project_info['finacing_style']);
            $project_info['cooperation'] = cms_get_linkage_value($project_info['cooperation']);
            $project_info['cityid'] = cms_get_linkage_value($project_info['cityid']);
            $project_info['industryid'] = cms_get_linkage_value($project_info['industryid'],false);
            $project_info['quasi_amount'] = (float)$project_info['quasi_amount']/10000;
            $project_info['investment'] = (float)$project_info['investment']/10000;

            //获取项目内容
            $content = D('Article/tzzgSjprojectData')->where($where)->find();


            $where = [
                'mid' => $project_info['mid'],
                'units' => $project_info['units'],
                'typeid' => $project_info['typeid'],
                'disabeld' => 0
            ];

            //上一篇
            $privs = D("Article/tzzgSjproject")->where($where)->where(['mid' => ['lt', $project_info['mid']]])->order("`inputtime` DESC")->find();
            //下一篇
            $next = D("Article/tzzgSjproject")->where($where)->where(['mid' => ['gt', $project_info['mid']]])->order("`inputtime` ASC")->find();

            $_ress = array_merge($project_info,$content);
            $_ress['content'] = cms_stripslashes($_ress['content']);
            $ress = [
                'code'      =>  200,
                'message'   =>  'success',
                'data'      =>  $_ress,
                'privs'     =>  $privs,
                'next'      =>  $next
            ];
        }else{
            $ress = [
                'code' => 0,
                'message'=>'the request is not have data！',
                'data'=>null
            ];
        }
        $this->ajaxReturn($ress);
    }

    /**
     * 显示 政务-省级 项目详细 web页面
     */
    public final function disSjProject(){
        $params = I('get.');
        //获取省下市级
        $city = D('Article/linkage')->where(['parentid'=>$params['pid']])->select();
        //获取省下机构
        $agency = D('Article/Institution')->where(['linkageid'=>$params['pid'],'parentid'=>0])->order("`listorder` ASC")->select();

        //默认第一个机构下的部门
        $units['pid'] = explode(",", $params['units'])[0];
        $units['unitid'] = explode(",", $params['units'])[1];


        $this->assign('typeid', $params['typeid']);
        $this->assign('mid',$params['mid']);
        $this->assign('city',$city);
        $this->assign('agency',$agency);
        $this->assign('units', $units);
        //页面显示
        $this->display("Article:show_zwxq.html");
    }

    /**
     * 获取部委列表
     */
    public final function getBuWeiList(){
        $params = I('get.');

        $where = [
            'disabled' => 0
        ];
        $where = array_merge($params,$where);
        $field = "linkageid,name,parentid,unitid,image,abouts";
        $data = D("Article/Institution")->where($where)->field($field)->select();

        if(!empty($data)){
            $res['code'] = 200;
        }else{
            $res['code'] = 0;
        }

        $res['params'] = $params;
        $res['data'] = $data;

        $this->ajaxReturn($res);
    }

    /**
     * 获取部委信息
     */
    public final  function getBuWeiInfo(){
        $params = I('get.');
        $unitid = $params['unitid'];

        $where = [
            'linkageid'    =>  $unitid,
            'display'  =>  1
        ];
        $field = "introduce,image";
        $data = D("Article/Linkage")->where($where)->field($field)->find();
        $sql = M()->getLastSql();
        if($data){
            $res['code'] = 200;
        }else{
            $res['code'] = 0;
        }
        $res['params'] = $params;
        $res['data'] = $data;
        $res['sql'] = $sql;

        $this->ajaxReturn($res);
    }


    /**
     * 显示部委项目信息
     */
    public final function buweiProject(){
        $params = I('get.');

        $where = [
            'mid' => $params['mid'],
            'disabled' => 0
        ];
            $filed = "mid,catid,title,inputtime,cityid,address,industryid,investment,cooperation,company,contact_name,contact_tel,finacing_style,quasi_amount,group,views";
        $title = D("Article/tzzgZwproject")->where($where)->field($filed)->find();
        if($title){
            $content = D("Article/tzzgZwprojectData")->where($where)->find();
        }

        $title['inputtime'] = date("Y-m-d", $title['inputtime']);
        $title['cityid'] = cms_get_linkage_value($title['cityid']);
        $title['industryid'] = cms_get_linkage_value($title['industryid'], false);
        $title['cooperation'] = cms_get_linkage_value($title['cooperation']);
        $title['finacing_style'] = cms_get_linkage_value($title['finacing_style']);
        $title['investment'] = (float)$title['investment']/10000;
        $title['quasi_amount'] = (float)$title['quasi_amount']/10000;
        $type = array('group'=>$params['group'], 'typeid'=>4030);

        $content = array_merge($title, $content);
        $content['content'] = cms_stripslashes($content['content']);


        $seo = cms_site_seo($content['catid'], [
            'title'         => $content['title'],
            'description'   => "",
            'keywords'      => "",
        ]);

        //上下篇查询
        $where = [
            'mid' => $content['mid'],
            'disabled' => 0 ,
        ];
        $f = "mid,title";
        //上一篇
        $where_privs['mid'] = array("lt", $content['mid']);
        $order = "`inputtime` DESC";

        $privs = D("Aritcle/tzzgZwproject")->where($where)->where($where_privs)->field($f)->order($order)->find();
        //下一篇
        $where_next['mid'] = array('gt', $content['mid']);
        $order = "`inputtime` ASC";
        $nexts = D("Aritcle/tzzgZwproject")->where($where)->where($where_next)->field($f)->order($order)->find();

        $this->assign("privs", $privs);
        $this->assign("nexts", $nexts);
        $this->assign('content', $content);
        $this->assign("cat", $type);
        $this->assign("seo", $seo);
        $this->display("Article:show_zwbw.html");
    }

    /**
     * 部委项目详细
     */
    public final function ajaxBuweiProDetail(){
        $params = I('get.');
        $where = [
            'group' => $params['group'],
            'disabled' => 0
        ];
        $filed = "mid,catid,title,inputtime,cityid,address,industryid,investment,cooperation,company,contact_name,contact_tel,finacing_style,quasi_amount,group,views";
        if($params['mid']){
            $where['mid'] = $params['mid'];
            $_ress = D("Article/tzzgZwproject")->where($where)->field($filed)->find();

            if($_ress) {
                $content = D("Article/tzzgZwprojectData")->where($where)->find();
                $content['content'] = cms_stripslashes($content['content']);
                $_ress = array_merge($_ress, $content);
                $_ress['cityid'] = cms_get_linkage_value($_ress['cityid']);
                $_ress['cooperation'] = cms_get_linkage_value($_ress['cooperation']);
                $_ress['finacing_style'] = cms_get_linkage_value($_ress['finacing_style']);
                $_ress['industryid'] = cms_get_linkage_value($_ress['industryid'], false);
                $_ress['inputtime'] = date("Y-m-d", $_ress['inputtime']);
                $_ress['investment'] = (float)$_ress['investment']/10000;
                $_ress['quasi_amount'] = (float)$_ress['quasi_amount']/10000;
                //上下篇查询条件
                $_where = array(
                    "disabled" => 0
                );
                $field = "mid,title";
                //上一篇
                $order = "`inputtime` DESC";
                $_privs['mid'] = array("lt", $_ress['mid']);
                $privs = D("Article/tzzgZwproject")->where($_where)->where($_privs)->field($field)->order($order)->find();

                //下一篇
                $order = "`inputtime` ASC";
                $_nexts['mid'] = array("gt", $_ress['mid']);
                $nexts = D("Article/tzzgZwproject")->where($_where)->where($_nexts)->field($field)->order($order)->find();
            }
        }else{
            $_ress = D("Article/tzzgZwproject")->where($where)->field($filed)->order("`inputtime` DESC")->find();
            if($_ress){
                $content = D("Article/tzzgZwprojectData")->where($where)->find();
                $content['content'] = cms_stripslashes($content['content']);
                $_ress = array_merge($_ress, $content);
                $_ress['cityid'] = cms_get_linkage_value($_ress['cityid']);
                $_ress['cooperation'] = cms_get_linkage_value($_ress['cooperation']);
                $_ress['finacing_style'] = cms_get_linkage_value($_ress['finacing_style']);
                $_ress['industryid'] = cms_get_linkage_value($_ress['industryid']);
                $_ress['inputtime'] = date("Y-m-d", $_ress['inputtime']);
                $_ress['investment'] = (float)$_ress['investment']/10000;
                $_ress['quasi_amount'] = (float)$_ress['quasi_amount']/10000;
                //上下篇查询条件
                $_where = array(
                    "disabled" => 0
                );
                $field = "mid,title";
                //上一篇
                $order = "`inputtime` DESC";
                $_privs['mid'] = array("lt", $_ress['mid']);
                $privs = D("Article/tzzgZwproject")->where($_where)->where($_privs)->field($field)->order($order)->find();

                //下一篇
                $order = "`inputtime` ASC";
                $_nexts['mid'] = array("gt", $_ress['mid']);
                $nexts = D("Article/tzzgZwproject")->where($_where)->where($_nexts)->field($field)->order($order)->find();
            }

        }

        $ress = array(
            "data"  => $_ress,
            "privs" => $privs,
            "nexts" => $nexts,
            'params' => I('get.')
        );
        $this->ajaxReturn($ress);
    }


    /**
     * 显示市级页面
     */
    public final function showProvinceCity(){

        $where = [
            'display' => 1,
            'linkageid' => I('get.parentid'),
        ];

        //当前请求城市
        $city = D("Article/Linkage")->where(['linkageid'=>I('get.linkageid'), 'display'=>1])->find();
        //省级信息
        $province = D("Article/Linkage")->where($where)->find();
        $citys = D("Article/Linkage")->where(['parentid'=>I('get.parentid'), 'display'=>1])->select();
        //市级下区县
        $region = D("Article/Linkage")->where(['parentid'=>I('get.linkageid'), 'display'=>1])->select();
        //市级机构
        $agencys = $this->getCityAgency(I('get.linkageid'));
//        dump($agencys);die;
        //seo信息
        $seo = cms_site_seo($catid, [
            'title'         => $city['name'],
            'description'   => $description,
            'keywords'      => $keywords,
        ]);
//        dump($city);
//        die;
        $this->assign("count", count($region));
        $this->assign("agencys", $agencys);
        $this->assign("regions", $region);
        $this->assign('city', $city);
        $this->assign("select", I("get.linkageid"));
        $this->assign('citys', $citys);
        $this->assign('province', $province);
        $this->assign('seo', $seo);
        $this->assign('site', $this->site);
        $this->display("Article:list_shiji.html");
    }
    /**
     * 区县级
     */
    public final function diplayQu(){
        $params = I("get.");

        $p_region = getRegionName($params['parentid']);
        $region = getRegionName($params['linkageid']);

        $pp_region = D("Admin/Linkage")->where(['linkageid'=>$params['parentid'], 'display'=>1])->find();
        if($p_region && ($p_region['parentid'] != 0 || $p_region['parentid'] != 1)){
            $pp_region = getRegionName($p_region['parentid']);
        }

        $agencys = D('Admin/Institution')->where(['linkageid' => $params['linkageid'], 'disabled' => 0])->select();
        $countrys = D("Admin/Linkage")->where(['parentid' => $params['parentid'], 'display' => 1])->select();

        $seo = cms_site_seo(198, [
            'title'         => $region['name'],
            'description'   => $description,
            'keywords'      => $keywords,
        ]);

//        dump($agencys);
        $this->assign('agencys', $agencys);
        $this->assign('countrys', $countrys);
        $this->assign('province',$pp_region);
        $this->assign('city',$p_region);
        $this->assign('country',$region);
        $this->assign('seo', $seo);
        $this->display("Article:list_quxian.html");
//        dump($params);die;
    }

    /**
     * 获取市级机构
     */
    public final function getCityAgency($linkageid){

        $where = [
            'disabled' => 0,
            'linkageid' => $linkageid
        ];

        $field = "unitid,name,parentid,linkageid,abouts,image";
        $agencys = D("Article/Institution")->where($where)->select();
        return $agencys;
    }

    /**
     * 获取市级数据列表
     */
    public final function ProvinceCityData(){
        $params = I('get.');
        
        $size = 5;
        $page = $params['page'];
        unset($params['page']);
        $params['disabled'] = 0;

        $pageinfo = array();

        $new_field = "mid,catid,typeid,title,thumb,description,inputtime,units,group,disabled,copyfrom";
        $pro_field = "mid,catid,title,thumb,inputtime,cityid,address,industryid,investment,company,contact_name,contact_tel,finacing_style,quasi_amount,units";
        if(isset($params['units'])){

            if($params['typeid'] != 4030){
                //请求非项目（新闻）列表
                $count = D("Article/tzzgNews")->where($params)->count();
                $_ress = D("Article/tzzgNews")->where($params)->limit($size*($page-1),$size)->field($new_field)->select();
                if($_ress !== false){
                    $ress['code'] = '200';
                    foreach ($_ress as $k => $v){
                        $_ress[$k]['inputtime'] = date("Y-m-d", $v['inputtime']);
                    }
                }else{
                    $ress['code'] = '111';
                }
                $ress['type'] = 'news';
                $ress['data'] = $_ress;
                $ress['pageinfo'] = array(
                    'total' => $count,
                    'pageSize' => $size,
                    'curPage' => $page,
                    'totalPage' => ceil($count/$size)
                );
            }else{
                //请求项目列表
                $count = D("Article/tzzgSjproject")->where($params)->count();
                $_ress = D("Article/tzzgSjproject")->where($params)->limit($size*($page-1),$size)->field($pro_field)->select();

                if($_ress !== false){
                    $ress['code'] = '200';

                    foreach ($_ress as $k => $v){
                        $_ress[$k]['inputtime'] = date('Y-m-d', $v['inputtime']);
                        $_ress[$k]['quasi_amount'] = (float)$v['quasi_amount']/10000;
                        $_ress[$k]['cityid'] = cms_get_linkage_value($v['cityid']);
                        $_ress[$k]['industryid'] = cms_get_linkage_value($v['industryid'], false);
                        $_ress[$k]['finacing_style'] = cms_get_linkage_value($v['finacing_style']);
                    }
                }else{
                    $ress['code'] = '111';
                }
                $ress['type'] = 'project';
                $ress['data'] = $_ress;
                $ress['pageinfo'] = array(
                    'total' => $count,
                    'pageSize' => $size,
                    'curPage' => $page,
                    'totalPage' => ceil($count/$size)
                );

            }
            $this->ajaxReturn($ress);

        }else{
            //市区下的第一条机构为默认
            $unit['linkageid'] = $params['linkageid'];
            $unit['disabled'] = 0;
            $_info = D("Article/Institution")->where($unit)->order('`listorder` ASC')->find();

            $ress = array(
                'code'  =>  200,
                'type'  => 'news',
                'data'  => null
            );

            if($_info !== false && !empty($_info)){
                if($params['typeid'] != 4030){
                    //非项目（新闻）列表
                    $_where = [
                        'typeid' => $params['typeid'],
                        'units' => $_info['unitid'],
                        'disabled' => 0,
                    ];

                    $count = D("Article/tzzgNews")->where($_where)->count();
                    $_ress = D("Article/tzzgNews")->where($_where)->limit($size*($page-1),$size)->field($new_field)->select();

                    if($_ress !== false){
                        foreach ($_ress as $k => $v){
                            $_ress[$k]['inputtime'] = date("Y-m-d", $v['inputtime']);
                        }

                        $ress['code'] = 200;
                        $ress['data'] = $_ress;
                        $ress['pageinfo']  = array(
                            'total' => $count,
                            'pageSize' => $size,
                            'curPage' => $page,
                            'totalPage' => ceil($count/$size)
                        );
                    }else{
                        $ress['code'] = 111;
                        $ress['data'] = $_ress;
                        $ress['pageinfo']  = array(
                            'total' => $count,
                            'pageSize' => $size,
                            'curPage' => $page,
                            'totalPage' => ceil($count/$size)
                        );
                    }
                    $this->ajaxReturn($ress);
                }
            }else{
                $ress['code'] = 111;
                $this->ajaxReturn($ress);
            }
        }

    }



    /**
     * 联动菜单 图片和介绍获取
     * 参数 linkageid
     */
    public final function szcsxcCityInfo(){
        $params = I('get.');
        $params['display'] = 1;
        $data = D('Article/Linkage')->where($params)->field("introduce,image")->find();

        $this->ajaxReturn($data);
    }

    /**
     * 城市市州宣传获取数据
     */
    public final function szcsxcData(){
        $params = I('get.');
        $size = 5;
        $page = $params['page'];

        $ress = [];
        $ress['code'] = 200;
        $ress['params'] = $params;
        $ress['pageinfo']['curPage'] = $page;
        $ress['pageinfo']['pageSize'] = $size;

        if($params['type'] == 'news'){
            //请求新闻列表
            $where = [
                'typeid' => $params['typeid'],
                'cityid' => $params['cityid'],
                'disabled' => 0,
                'catid' => 206,
            ];

            $count = D("Article/tzzgCitynews")->where($where)->count();
            $field = "mid,catid,typeid,title,thumb,description,inputtime";
            $data = D("Article/tzzgCitynews")->where($where)->limit($size*($page-1),$size)->field($field)->select();
            if($data){
                foreach ($data as $k=>$v){
                    $data[$k]['inputtime'] = date('Y-m-d', $v['inputtime']);
                    $data[$k]['title'] = subtext($v['title'], 28);
                    $data[$k]['description'] = subtext($v['description'], 130);
                }
            }

            $ress['type'] = 'news';
            $ress['data'] = $data;
            $ress['pageinfo']['total'] = $count;
            $ress['pageinfo']['totalPage'] = ceil($count/$size);

        }else{
            //请求项目列表
            $where = [
                'disabled' => 0,
                'catid' => 207,
                'cityid' => $params['cityid']
            ];

            $count = D("Article/tzzgCitypro")->where($where)->count();
            $field = "mid,catid,title,company,contact_name,finacing_style,industryid,cityid,quasi_amount,inputtime";
            $data = D("Article/tzzgCitypro")->where($where)->limit($size*($page-1),$size)->field($field)->select();
            $ress['sql'] = M()->getLastSql();
            foreach ($data as $k=>$v){
                $data[$k]['inputtime'] = date('Y-m-d', $v['inputtime']);

                $data[$k]['quasi_amount'] = (float)$v['quasi_amount']/10000;
                $data[$k]['cityid'] = cms_get_linkage_value($v['cityid']);
                $data[$k]['industryid'] = cms_get_linkage_value($v['industryid'], false);
                $data[$k]['finacing_style'] = cms_get_linkage_value($v['finacing_style']);
            }

            $ress['type'] = 'project';
            $ress['data'] = $data;
            $ress['pageinfo']['total'] = $count;
            $ress['pageinfo']['totalPage'] = ceil($count/$size);
        }

        $this->ajaxReturn($ress);

    }

    /**
     * 获取会务招商列表数据
     */
    public final function conferenceListData(){
        $params = I('get.');

        $size = 5;
        $page = $params['page'];
        unset($params['page']);
        $pageinfo = array(
            'curPage' => $page,
            'pageSize' => $size
        );

        foreach ($params as $k=>$v){
            if(!$v){
                unset($params[$k]);
            }
        }
        $params['disabled'] = 0;
        $order = $params['order'];
        unset($params['order']);
        $params['inputtime'] = switchtime($params['inputtime']);
        $field = "mid,be_induid,catid,cityid,description,group,inputtime,suit_company,thumb,title,typeid,unit_group,units";
        if(isset($params['cityid'])){
            $like = array('like',$params['cityid']."%");
            $p = substr($params['cityid'],-1) != "," ?:substr($params['cityid'], 0, -1);
            $eq = array('eq',$p);
            $_where['cityid'] = array($eq, $like, 'or');
            unset($params['cityid']);

            $total = D("Article/tzzgHwnews")->where($params)->where($_where)->count();
            $data = D("Article/tzzgHwnews")->where($params)->where($_where)->order($order)->field($field)->limit($size*($page - 1), $size)->select();
        }else{
            $total = D("Article/tzzgHwnews")->where($params)->count();
            $data = D("Article/tzzgHwnews")->where($params)->order($order)->field($field)->limit($size*($page - 1), $size)->select();
            $sql = M()->getLastSql();
        }


        if($data){
            foreach ($data as $k=>$v){
                $data[$k]['inputtime'] = date('Y-m-d',$v['inputtime']);
                $data[$k]['be_induid'] = cms_get_linkage_value($v['be_induid'],false);
                $data[$k]['cityid'] = cms_get_linkage_value($v['cityid']);
                $data[$k]['group'] = cms_get_linkage_value($v['group']);
                $data[$k]['suit_company'] = cms_get_linkage_value($v['suit_company']);
                $data[$k]['unit_group'] = cms_get_linkage_value($v['unit_group']);
                $data[$k]['units'] = cms_get_linkage_value($v['units']);
                $data[$k]['typeid'] = cms_get_linkage_value($v['typeid']);

                $data[$k]['units'] = $data[$k]['units'] ? : $data[$k]['unit_group'];
                if($data[$k]['units']){
                    unset($data[$k]['unit_group']);
                }else{
                    $data[$k]['units'] = $data[$k]['unit_group'];
                    unset($data[$k]['unit_group']);
                }
            }
        }
        $pageinfo['total'] = $total;
        $pageinfo['totalPage'] = ceil($total/$size);

        $ress['pageinfo'] = $pageinfo;
//        $ress['params'] = $params;
        $ress['data'] = $data;
//        $ress['sql'] = $sql;
        $this->ajaxReturn($ress);
    }

    /**
     * 添加参会人员信息
     */
    public final function addParticipation(){
        $params = I("post.");

        //返回数据
        $ress = [];
        $ress['code'] = 200;
        $ress['msg'] = "success";
        //是否已提交过信息
        $where = [];
        $where['hwid'] = $params['hwid'];
        $where['phone'] = $params['phone'];
        $is_have = D('Article/tzzgParticipant')->where($where)->find();

        if($is_have){
            $ress['code'] = 101;
            $ress['msg'] = "该手机号已报名！";
        }else{
            $save = D('Article/tzzgParticipant')->add($params);
            if(!$save){
                $ress['code'] = 101;
                $ress['msg'] = "提交失败！";
            }
        }


        $this->ajaxReturn($ress);
    }

    /**
     * 获取商协会列表数据
     */
    public final function busassoclist(){
        $params = I('get.');

        $size = 28;
        $page = $params['page'];
        unset($params['page']);
        $pageinfo = array(
            'curPage' => $page,
            'pageSize' => $size
        );

        foreach ($params as $k => $v){
            if(!$v){
                unset($params[$k]);
            }else if($k == "keywords" && !trim($params[$k])){
                unset($params[$k]);
            }
        }
        $_where = [];
        $_where['typeid'] = $params['typeid'];
        $_where['disabled'] = 0;
        if(isset($params['cityid'])){
            $like = array('like',$params['cityid']."%");
            $p = substr($params['cityid'],-1) != "," ?:substr($params['cityid'], 0, -1);
            $eq = array('eq',$p);
            $_where['cityid'] = array($eq, $like, 'or');
        }

        if(isset($params['keywords'])){
            $_where['title'] = array('like', "%".trim($params['keywords'])."%");
        }
        $field = "mid,title,typeid";
        $total = D('Aritcle/tzzgBusassoc')->where($_where)->count();
        $data = D('Aritcle/tzzgBusassoc')->where($_where)->field($field)->limit($size*($page - 1), $size)->select();

        $pageinfo['total'] = $total;
        $pageinfo['totalPage'] = ceil($total/$size);

        $ress = [
            'data'      => $data,
            'pageinfo'  => $pageinfo
        ];

        $this->ajaxReturn($ress);
    }

    /**
     * 获取企业库列表
     */
    public final function getqyk(){
        $params = I('get.');
        $p = $params;
        $type = $params['type'];
        unset($params['type']);
        $size = 28;
        $page = $params['page'];
        unset($params['page']);
        $pageinfo = array(
            'curPage' => $page,
            'pageSize' => $size
        );

        foreach ($params as $k => $v){
            if(!$v){
                unset($params[$k]);
            }else if($k == "keywords" && !trim($params[$k])){
                unset($params[$k]);
            }
        }

        $_where = [];
        $_where['typeid'] = $params['typeid'];
        $_where['disabled'] = 0;
        if(isset($params['cityid'])){
            $like = array('like',$params['cityid']."%");
            $p = substr($params['cityid'],-1) != "," ?:substr($params['cityid'], 0, -1);
            $eq = array('eq',$p);
            $_where['cityid'] = array($eq, $like, 'or');
        }

        if(isset($params['keywords'])){
            $_where['title'] = array('like', "%".trim($params['keywords'])."%");
        }
        $field = "mid,title";

        if($type == "dzxqy"){
            $total = D('Aritcle/tzzgDzxqy')->where($_where)->count();
            $data = D('Aritcle/tzzgDzxqy')->where($_where)->field($field)->limit($size*($page - 1), $size)->select();
            $sql = M()->getLastSql();
        }else{
            $zcmoney = $params['zcmoney'];
            if(strpos($zcmoney,'-') !== false){
                $ars = explode('-',$zcmoney);
                $where['zcmoney'] = array(
                    array('gt',$ars[0]),
                    array('elt',$ars[1])
                );
            }else{
                if($zcmoney == '500'){
                    $where['zcmoney'] = array('elt',$zcmoney);
                }else{
                    $where['zcmoney'] = array('gt',$zcmoney);
                }
            }

            $total = D('Aritcle/tzzgZxxqy')->where($where)->where($_where)->count();
            $data = D('Aritcle/tzzgZxxqy')->where($where)->where($_where)->field($field)->limit($size*($page - 1), $size)->select();
            $sql = M()->getLastSql();
        }

        $pageinfo['total'] = $total;
        $pageinfo['totalPage'] = ceil($total/$size);

        $ress = [
            'params'    =>  $p,
            'pageinfo'  =>  $pageinfo,
            'data'      => $data,
            'sql'       => $sql
        ];

        $this->ajaxReturn($ress);
    }

    /**
     * 获取金融机构列表
     */
    public final function getjrjgList(){
        $params = I('get.');
        $data = D('Article/tzzgJrjg')->where($params)->select();
        $sql = M()->getLastSql();
        $ress['data'] = $data;
        $this->ajaxReturn($ress);
    }

    /**
     * 获取基金列表
     */
    public final function getfundlist(){
        $params = I('get.');

        $size = 28;
        $page = $params['page'];
        unset($params['page']);
        $pageinfo = array(
            'curPage' => $page,
            'pageSize' => $size
        );

        foreach ($params as $k => $v){
            if(!$v){
                unset($params[$k]);
            }else if($k == "keywords" && !trim($params[$k])){
                unset($params[$k]);
            }
        }

        $where = $params;
        $where['disabled'] = 0;

        if(isset($params['cityid'])){
            $like = array('like',$params['cityid']."%");
            $p = substr($params['cityid'],-1) != "," ?:substr($params['cityid'], 0, -1);
            $eq = array('eq',$p);
            $where['cityid'] = array($eq, $like, 'or');
        }

        if(isset($params['keywords'])){
            $where['title'] = array('like', "%".trim($params['keywords'])."%");
            unset($where['keywords']);
        }

        $field = "mid,title,company";
        $order = "`listorder` ASC";
        $total = D('Aritcle/tzzgFund')->where($where)->count();
        $data = D('Aritcle/tzzgFund')->where($where)->field($field)->order($order)->limit($size*($page - 1), $size)->select();
        $sql = M()->getLastSql();

        $pageinfo['total'] = $total;
        $pageinfo['totalPage'] = ceil($total/$size);


        $ress['where'] = $where;
        $ress['pageinfo'] = $pageinfo;
        $ress['data'] = $data;
        $ress['sql'] = $sql;
        $this->ajaxReturn($ress);
    }
}
