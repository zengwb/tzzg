<?php

namespace Article\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块搜索控制器类
 * 
 * @author T-01
 */
final class SearchController extends PageController {
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\PageController::_initialize()
     */
    public final function _initialize() { parent::_initialize(); }
    
    public final function index() {}
    
    public final function lists() {
        $ishtml = cms_config_array('Article', 'base')['cat_link'];
        $search = $ishtml ? 'search.html?' : substr(U('home/search/lists'),1).'&';
        
        $catid      = I('request.catid',  0, 'intval');
        $catid      = I('get.catid', $catid, 'intval');
        $catid      = I('post.catid',$catid, 'intval');
        
        $keyword    = I('request.key', null, 'cms_addslashes');
        $keyword    = I('get.key', $keyword, 'cms_addslashes');
        $keyword    = I('post.key',$keyword, 'cms_addslashes');
        $keyword    = trim($keyword);
        
        $where              = [];
        $where['catid']     = $catid;
        $ress = $this->category->where($where)->find();
        $modelid = $ress['modelid'];
        
        $order              = '`listorder` ASC';
        $where              = [];
        $where['module']    = 'Article';
        $where['disabled']  =  0;
        $ress = $this->apps->where($where)->order($order)->find();
        
        $modelid = $modelid ? : $ress['modelid'];
        
        $where              = [];
        $where['modelid']   = $modelid;
        $ress = $this->apps->where($where)->find();
        $table = $ress['table'];
        
        $catesub = $this->category->childid($catid, cms_siteid());
        
        $order              = '`inputtime` DESC';
        $like               = [];
        $like['title']      = ['LIKE', '%'.$keyword.'%'];
        $like['_logic']     = 'OR';
        $like['subtitle']   = ['LIKE', '%'.$keyword.'%'];
        $like['_logic']     = 'OR';
        $like['keywords']   = ['LIKE', '%'.$keyword.'%'];
        $like['_logic']     = 'OR';
        $like['description']= ['LIKE', '%'.$keyword.'%'];
        
        $where              = [];
        $where['disabled']  =  0;
        $where['_complex']  = $like;
        $where['catid']     = ['IN', $catesub];
        $count = D($table)->where($where)->count();
        $limit = '0,16';
        
        $keys = [
            $keyword => '<span class=\'cf30\'>'.$keyword.'</span>'
        ];
        $ress = D($table)->where($where)->limit($limit)
                         ->order($order)->select();
        $list = [];
        foreach ($ress as $key => $row) {
        $row['title2'] = $row['title'];
        $row['title2'] = mb_substr($row['title2'], 0, 32, 'utf-8');
        if (!$keyword) $row['title2'] = $row['title2'];
        else $row['title2'] = strtr($row['title2'], $keys);
        
        $args               = [];
        $args['catid']      = $row['catid'];
        $args['id']         = $row['mid'];
        $link = U('home/index/shows', $args);
        $html = 'show-'.$row['catid'].'-'.$row['mid'].'.html';
        $link = $ishtml ? $html : $link;
        $row['link'] = $row['url'] ? : $link;
        $list[$key] = $row;
        }
        
        $tag  = '<span class="cf60 bold">'.$keyword.'</span> 关键字，';
        $tag .= '共找到 <span class="cf60 bold">'.$count.'</span> 条记录！ ';
        
        $this->assign('list', $list);
        $this->assign('site', $this->site);
        
        $this->assign('catid', $catid);
        $this->assign('seo', cms_site_seo(0, [
            'title' => '搜索：'.$keyword
        ]));
        $this->assign('keyword', $keyword);
        $this->assign('tag', '_搜索：'.$tag);
        $this->assign('url', U('Home/Search/ajax2'));
        
        $this->assign('search', $search);
        
        $this->display('Article:search.html');
//         $this->display('./Resources/Template/'.$this->skin.'/Views/Article/search.html');
    }
    
    public final function ajax2() {
        header("Access-Control-Allow-Origin:*");
        
        $ishtml = cms_config_array('Article', 'base')['cat_link'];
        $search = $ishtml ? 'search.html?' : substr(U('home/search/lists'),1).'&';
        
        $catid      = I('get.catid', 0, 'intval');
        $keyword    = I('get.key', '', 'cms_addslashes');
        $keyword    = trim($keyword);
        $limit      = I('get.limit', '');
        
        $where              = [];
        $where['catid']     = $catid;
        $ress = $this->category->where($where)->find();
        $modelid = $ress['modelid'];
        
        $order              = '`listorder` ASC';
        $where              = [];
        $where['module']    = 'Article';
        $where['disabled']  =  0;
        $ress = $this->apps->where($where)->order($order)->find();
        $modelid = $modelid ? : $ress['modelid'];
        
        $where              = [];
        $where['modelid']   = $modelid;
        $ress = $this->apps->where($where)->find();
        $table = $ress['table'];
        
        $catesub = $this->category->childid($catid, cms_siteid());
        
        $order              = '`inputtime` DESC';
        $like               = [];
        $like['title']      = ['LIKE', '%'.$keyword.'%'];
        $like['_logic']     = 'OR';
        $like['subtitle']   = ['LIKE', '%'.$keyword.'%'];
        $like['_logic']     = 'OR';
        $like['keywords']   = ['LIKE', '%'.$keyword.'%'];
        $like['_logic']     = 'OR';
        $like['description']= ['LIKE', '%'.$keyword.'%'];
        
        $where              = [];
        $where['disabled']  =  0;
        $where['_complex']  = $like;
        $where['catid']     = ['IN', $catesub];
        $count = D($table)->where($where)->count();
        
        $ress = D($table)->where($where)->limit($limit)
                         ->order($order)->select();
        $list = [];
        foreach ($ress as $key => $row) {
        $row['id'] = $row['mid'];
        $row['keywords'] = strtr($row['keywords'], ['，' => ',']);
        $row['inputtime'] = date('Y年m月d日', $row['inputtime']);
        $row['style'] = unserialize($row['style']);
//         $row['thumb'] = strtr($row['thumb'], ['./'], '/');
        $row['description'] = mb_substr($row['description'], 0, 72, 'utf-8');
        
        $args               = [];
        $args['catid']      = $row['catid'];
        $args['id']         = $row['mid'];
        $link = U('home/index/shows', $args);
        $html = 'show-'.$row['catid'].'-'.$row['mid'].'.html';
        $link = $ishtml ? $html : $link;
        $row['link'] = $row['url'] ? : $link;
        $row['search'] = $this->site['domain'].$search;
        $list[$key] = $row;
        }
        echo json_encode($list);
    }
    
}
