<?php

namespace Article\Controller;

use Think\Upload;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块内容管理控制器类：呈现内容CURD常规管理
 * 
 * @author T-01
 */
final class ApproveController extends BaseController {
    
    public      $action     = [
        'index','add','more','all_move'
    ];

    /** 添加审批流程
     * @param $table 审批类型 来源v1_model表
     * @param $mid 添加的内容id
     * @return array{ code = >boolean,message => string }
     */
    public function add($table,$mid){
        $model_arr = model_all();
        if(!in_array($table,$model_arr['table']) || !$mid){ return ['code' => false,'message' => '参数错误!'];}

        $mould_type = $model_arr['name'][$table];//审批类型

        //>>>匹配模板
        $where_m = [
            'mould_type' => $mould_type,
            'state' => '1',//状态 1
            'order' => '1'//只用查询第一步
        ];
        $mould = M('ApproveMould')->join('left join v1_approve_flow as f on f.mould_id = v1_approve_mould.mould_id')
            ->where($where_m)
            ->order('date_time desc')//只使用最新的模板
            ->find();//查询对应的的模板流程

        if(!$mould){ return ['code' => false,'message' => '没有合适模板!']; } //没有合适模板

        //>>>校验用户权限 >>> 后台添加，暂时没法验证申请权限

        $apply_add = [
            'approve_id'     => $mould['approve_id'],//流程模板id
            'date_time'     => time(),//申请时间
            'proposer'      => null,//申请人id  后台添加，无
            'proposer_name' => null,//申请人   后台添加，无
            'operator'      => session('CMS_UserID'),//操作人员id
            'operator_name' => session('CMS_UserName'),//操作人
            'relation_tabel' => $table,//关联表
            'apply_detail_id' => $mid,//明细表id
            'state'         => '1',//默认状态 1：流程中
        ];

        $apply_id = M('Apply')->add($apply_add);//详情添加
        if(!$apply_id){ return ['code' => false,'message' => '审批单详情添加失败!']; }

        $apply_approve_add = [
            'apply_id' => $apply_id,//申请单id
            'order' => 1,//序号
            'approve_id' => $mould['approve_id'],//流程模板id
            'officer' => session('CMS_UserID'),//申请人/审批人 后台添加，当前管理员
            'date_time' => time(),//操作时间
            'result' => 1,//结果
            'remark' => null,//备注
            'process_name' => $mould['process_name'],//步骤名称
        ];

        $rs = M('ApplyApprove')->add($apply_approve_add);//流程流水添加 -- 第一步：申请
        if(!$rs){ return ['code' => false,'message' => '审批流水记录失败!']; }
        return ['code' => true,'message' => '审批发送成功'];
    }

    /** 审批列表
     * @param null $table 表名  来源v1_model表  **** ‘all’ 表示全部
     */
    public final function index($table = null){
        $model_arr = model_all();
        if(!in_array($table,$model_arr['table']) && $table != 'all' ){$this->error('参数错误!');}

        $admin_roleid = session('CMS_RoleID');//当前管理员的roleid
        $admin_id = session('CMS_UserID');//当前管理员的urserid
        $keyWord = I('post.key') ? I('post.key') : '';//搜索关键词
        unset($model_arr['name']['member_units']);//共享单位模块 不在此列

        if($table != 'all'){
            $mould_type = $model_arr['name'][$table];//审批类型
        }else{
            $mould_type = implode("','",$model_arr['name']);
        }

        //>>>查询当前管理员参与的审批流程;
        $where = "((officer_id is null and officer_range = $admin_roleid) or (officer_id = $admin_id)) and mould_type in ('".$mould_type."')";
        $data = M('ApproveFlow')->Distinct(true)->field('approve_id')
            ->join('left join v1_approve_mould as m on m.mould_id = v1_approve_flow.mould_id')
            ->where($where)->select();

        $approve_id = [];
        foreach($data as $v){
            $approve_id[] = $v['approve_id'];
        }
        $approve_id = implode(',',$approve_id);

        //查询审批流程中最后一步: 最后一步的审批管理员+超级管理员 可以一键审批
        $where_last['approve_id'] = array('in',$approve_id);
        $all_step = M('ApproveFlow')->field('`order` as num,`approve_id`,officer_range,officer_id')->where($where_last)->select();
        $last_step = [];
        foreach($all_step as $v){
            if($last_step[$v['approve_id']]['order']<$v['num']){
                $last_step[$v['approve_id']] = [
                    'order' => $v['num'],
                    'state' => $v['officer_id']?($v['officer_id'] == $admin_id?1:0):($v['officer_range'] == $admin_roleid?1:0),
                ];
            }
        }

        //>>>查询审批模板下的所有申请列表
        $where_apply['v1_apply.approve_id'] = array('in',$approve_id);

        $nums = M('Apply')->where($where_apply)->count();//分页
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);

        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyWord;

        $ress = M('Apply')->field('v1_apply.*,f.mould_id,m.mould_type,m.mould_name')
            ->join('left join v1_approve_flow as f on f.approve_id = v1_apply.approve_id and f.`order`=1')
            ->join('left join v1_approve_mould as m on m.mould_id = f.mould_id')
            ->order('date_time desc')
            ->limit(
                $page->firstRow.','.$page->listRows
            )
            ->where($where_apply)
            ->select();



        //查询项目详情
        $table_arr = [];
        foreach($ress as $v){
            $table_arr[$v['relation_tabel']][] = $v['apply_detail_id'];
        }

        $detail = [];
        foreach($table_arr as $k => $v){
            $model = M($k);
            $where = [
                'mid' => array('in',$v),
                'title' => array('like',"%$keyWord%")
            ];
            $detail[$k] = $model->where($where)->getField('mid,title');//暂时只查询项目标题
        }

        //整理数组
        foreach($ress as $k=>$v){
            $ress[$k]['title'] = $detail[$v['relation_tabel']][$v['apply_detail_id']];
            if(!$ress[$k]['title']){
                unset($ress[$k]);//如果没有对应的标题，删除！
            }
        }

        foreach($ress as $k => $v){
            $ress[$k]['title2'] = str_replace($keyWord,'<font style=\'color: red\'>'.$keyWord.'</font>',$v['title']);
        }

        $user_state = in_array($admin_id, C('SUPER_USERID'));//是否是超级管理员
        $role_state = in_array($admin_roleid, C('SUPER_ROLEID'));//是否是超级用户组

        $super_state = 0;
        foreach($ress as $k => $v){
            if(!$user_state && !$role_state){
                $ress[$k]['fly_state'] = $last_step[$v['approve_id']]['state'];
            }else{
                $super_state = 1;
                $ress[$k]['fly_state'] = 1;//是否能直接操作、批量操作
            }
        }

        $this->assign('mould_type', $model_arr['name']);//模型类型
        $this->assign('data', $ress);//数据
        $this->assign('table', $table);//模型表明
        $this->assign('state', $super_state);//超级管理员
        $this->assign('page', $page->show());//分页

        $this->display();
    }





    /**
     * 审批详情+提交审批
     */
    public final function more(){
        $admin_roleid = session('CMS_RoleID');//当前管理员的roleid
        $admin_id = session('CMS_UserID');//当前管理员的urserid

        if (IS_POST && I('post.dosubmit')) {
            $info = I('post.info', []);
            $result = I('post.dosubmit');
            if($result == '驳回'){
                $result = 0;
            }elseif($result == '同意'){
                $result = 1;
            }else{
                $this->error('状态错误！');
            }

            //>>>查询最大审批序号
            $max_order = M('ApproveFlow')->field('max(`order`) as max')->where(['approve_id'=> $info['approve_id']])->find()['max'];
            //审核序号 不能大于最大审批序号
            if($max_order<$info['order']){
                $this->error('最大审批序号错误！');
            }

            $apply_approve_add = [
                'apply_id' => $info['apply_id'],//申请单id
                'order' => $info['order'],//序号
                'approve_id' => $info['approve_id'],//流程模板id
                'officer' => $admin_id,//审批人
                'date_time' => time(),//操作时间
                'result' => $result,//结果
                'remark' => $info['remark'],//备注
                'process_name' => $info['process_name'],//步骤名称
            ];
            $ress = M('ApplyApprove')->add($apply_approve_add);//流程流水添加

            //驳回 直接修改申请单状态， 同意，判定是否是最后一步
            $where['apply_id'] = $info['apply_id'];//申请单id
            $msg = '';
            if($result){
                //同意 且是最后一步》》 修改状态
                if($max_order == $info['order']){
                    $re = self::state_table_role($info['apply_id'],9);

                    if(!$re) $msg = '修改项目状态失败';//$this->error('审批失败，或已审核！');

                    $save = [
                        'state' => 2,//状态
                        'update_time' => time(),//同意时间
                    ];
                    $ress = M('Apply')->where($where)->save($save);
                }
            }else{
                //驳回
                $re = self::state_table_role($info['apply_id'],1);

                if(!$re) $msg = '修改项目状态失败';//$this->error('审批失败，或已审核！');

                $save = [
                    'state' => 0,//状态
                    'update_time' => time(),//同意时间
                    'remark' => $info['remark'],//备注
                ];
                $ress = M('Apply')->where($where)->save($save);
            }

            $tips = '['.I('post.dosubmit').'申请] 操作';
            if (!$ress) $this->error($tips.'失败！'.$msg);
            else $this->success($tips.'成功！'.$msg, '', U('more',['apply_id' => $info['apply_id']]));
        } // @todo:

        $apply_id = I('get.apply_id', 0, 'intval');

        //查询审批详情
        $where['apply_id'] = $apply_id;
        $ress = M('Apply')->field('v1_apply.*,f.mould_id,m.mould_type,m.mould_name')
            ->join('left join v1_approve_flow as f on f.approve_id = v1_apply.approve_id and f.order=1')
            ->join('left join v1_approve_mould as m on m.mould_id = f.mould_id')
            ->order('date_time desc')
            ->where($where)->find();
        $ress_detail = M($ress['relation_tabel'])->where(['mid' => $ress['apply_detail_id']])->find();//详细内容详情
        $ress = array_merge($ress,$ress_detail);

        //查询审批流程
        $ress['flow'] = M('ApplyApprove')->field('v1_apply_approve.*,d.username as officer_name,u.username as user_name')
            ->join('left join v1_admin as d on d.userid = v1_apply_approve.officer and `order` !=1')
            ->join('left join v1_member_units as u on u.userid = v1_apply_approve.officer and `order` =1')
            ->where($where)->order('`order` asc')->select();

        $count = count($ress['flow']);
        //查询待审核步骤
        $where_flow['order'] = array('gt',$count);
        $where_flow['approve_id'] = $ress['approve_id'];
        $data_flow = M('ApproveFlow')->field('v1_approve_flow.*,r.name as role_name,a.username as user_name')
            ->join('left join v1_admin_role as r on r.roleid = v1_approve_flow.officer_range')
            ->join('left join v1_admin as a on a.userid = v1_approve_flow.officer_id')
            ->where($where_flow)->order('`order` asc')->select();

        foreach($data_flow as $v){
            $ress['flow'][] = $v;
        }


        $next_flow = $data_flow[0];//下一步
        $next_officer_id = $next_flow['officer_id'];//下一步专人审批
        $next_officer_range = $next_flow['officer_range'];//下一步审批角色
        $state = 0;//当前管理员是否能操作 0 不能  1 能
        if($ress['state'] == 1 && $next_officer_id && $next_officer_id == $admin_id ){
            $state = 1;//流程中、专人审核
        }elseif($ress['state'] == 1 && !$next_officer_id && $next_officer_range == $admin_roleid){
            $state = 1;//流程中、非专人审核、范围角色id相同
        }

        $this->assign('data', $ress);
        $this->assign('next_flow', $next_flow);
        $this->assign('state', $state);

        $this->display();
    }

    /**
     * 一键同意、驳回操作；带批量
     */
    public final function all_move(){
        $admin_roleid = session('CMS_RoleID');//当前管理员的roleid
        $admin_id = session('CMS_UserID');//当前管理员的urserid

        $apply_id = I('get.apply_id', 0, 'intval');
        $fruit = I('get.fruit', 0, 'intval');

        $info = I('post.info', [], 'cms_addslashes');
        $info = $apply_id ? [$apply_id] : $info['apply_id'];
        if (empty($info)) $this->error('请选择对象！');
        $apply_ids = implode(',',$info);

        if($fruit){ //通过
            $re = self::state_table_role($apply_ids,9);
        }else{ //驳回
            $re = self::state_table_role($apply_ids,1);
        }
        if(!$re) $this->error('审批失败，或已审核！');

        //查询申请单 对应的 流程模板id   ###暂时还是不记录流水了，步骤断层了
//        $where_apply['apply_id'] = array('in',$apply_ids);
//        $data = $this->applyApprove->field('apply_id,approve_id,order')->where($where_apply)->select();

//        $apply_approve_add = [];
//        foreach($data as $v){
//            $apply_approve_add = [
//                'apply_id' => $v['apply_id'],//申请单id
//                'order' => $info['order'],//序号
//                'approve_id' => $v['approve_id'],//流程模板id
//                'officer' => $admin_id,//审批人
//                'date_time' => time(),//操作时间
//                'result' => $fruit?1:0,//结果
//                'remark' => "一键审批",//备注
//            ];
//        }
//        $ress = $this->applyApprove->addAll($apply_approve_add);//流程流水添加
//        if(!$ress) $this->error('记录审批流水出错！');

        $where['apply_id'] = array('in',$apply_ids);
        $where['state'] = '1';
        $ress = M('Apply')->where($where)->save(['state'=>$fruit,'remark'=> "一键审批",'update_time'=>time()]);

        $temp = ' ';
        if (!$ress) $this->error('操作失败！', $temp);
        else $this->success('操作成功！', $temp, U('index?table=project'));
    }

    /** 审批完成后 修改对应数据状态
     * @param $apply_ids 申请单id 多个,号链接
     * @param int $level  修改内容状态  1驳回  9通过
     * @return bool
     */
    private function state_table_role($apply_ids,$level=9){
        $where['apply_id'] = array('in',$apply_ids);
        $all_mid = M('Apply')->field('relation_tabel as `table`,apply_detail_id as mid')
            ->where($where)->select();
        $table = $all_mid[0]['table'];

        $mids = [];
        foreach ($all_mid as $v) {
            $mids[] = $v['mid'];
            if($table != $v['table']){
                $this->error('不是同一个审批类型!');//>>>相同的mould_type 才能继续审批
            }
        }
        $mids = implode(',',$mids);
        $model = M($table);
        $ress =$model->where(['mid' => array('in',$mids)])->save(['level' => $level]);

        return $ress;
    }






    
}
