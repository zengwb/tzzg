<?php

namespace Article\Controller;

use Think\Upload;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块推荐位置控制器类：呈现位置CURD常规管理
 * 
 * @author T-01
 */
final class PositionController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state',
        'thumb', 'image',
    ];
    
    private     $appsModel  = [],
                $modeModel  = [],
                
                $cateModel  = [],
                $posiModel  = [],
                $dataModel  = [],
                
                $upload     = null;
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->appsModel    = D('Admin/Module');
        $this->modeModel    = D('Admin/Model');
        
        $this->cateModel    = D('Admin/Category');
        $this->posiModel    = D('Position');
        $this->dataModel    = D('PositionData');
        
        $this->upload = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Position/';
        
        $this->upload->subName  = null;
        $this->upload->saveName = date('YmdHis', time());
        $this->upload->ext  = ['jpg','jpeg','gif','png'];
    }
    
    /**
     * 位置管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $order = '`listorder` ASC, `posiid` ASC';
        $where = [];
        $where['name'] = ['LIKE', '%'.$keyword.'%'];
        $where['siteid'] = cms_siteid();
        
        $nums = $this->posiModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->posiModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title'] = $row['name'];
        else $row['title'] = strtr($row['name'], $keys);
        
        $where              = [];
        $where['modelid']   = $row['modelid'];
        $model = $this->modeModel->where($where)->find();
        $row['model'] = $model['name'] ? : '/';
        
        $where              = [];
        $where['catid']     = $row['catid'];
        $cates = $this->cateModel->where($where)->find();
        $row['catname'] = $cates['catname'] ? : '/';
        
        $where              = [];
        $where['posiid']    = $row['posiid'];
        $count = $this->dataModel->where($where)->count();
        $row['count']       = $count;
        $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', []);
        $list = [];
        
        foreach ($data['posiid'] as $key => $posiid) {
        $listorder = $this->posiModel->where([
            'posiid' => $posiid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->posiModel->where(['posiid' => $posiid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $posiid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('推送位置排序成功！', $listorder);
    }
    
    /**
     * 添加位置
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['siteid'] = cms_siteid();
        
        $where              = [];
        $where['module']    = $data['module'];
        $where['modelid']   = $data['modelid'];
        $where['catid']     = $data['catid'];
        $where['name']      = $data['name'];
        
        if (isset($where['modelid'])) unset($where['modelid']);
        if (isset($where['catid'])) unset($where['catid']);
        
        $ress = $this->posiModel->where($where)->find();
        $tips = '推送位置『'.$data['name'].'』已经存在！';
        if ($ress) $this->error($tips, $data);
        
        if (empty($_FILES['thumb']['error']) &&
            isset($_FILES['thumb'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('图例上传失败！');
        
        $data['thumb']  = $this->upload->rootPath;
        $data['thumb'] .= $file['thumb']['savepath'];
        $data['thumb'] .= $file['thumb']['savename'];
        }
        
        $tips = '添加推送『'.$data['name'].'』位置';
        $ress = $this->posiModel->add($data);
        if (!$ress) $this->error($tips.'失败！', $data);
        
        $inid = $this->posiModel->getLastInsID();
        
        if ($file && $inid) {
        $temp = [];
        $temp['filename']   = $file['thumb']['name'];
        $temp['filesize']   = $file['thumb']['size'];
        $temp['fileext']    = $file['thumb']['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['thumb'];
        cms_attachment($temp); // @todo: 写入附件
        }
        $this->posiModel->where(['posiid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', $data, U('index'));
        } // @todo: 
        
        $where              = [];
        $where['disabled']  =  0;
        $where['ismodel']   =  1;
        $apps = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('module', $apps);
        $this->assign('data', ['number' => 5, 'status' => 1]);
        $this->display('action');
    }
    
    /**
     * 修改位置
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $info = I('post.info', []);
        $data['siteid'] = cms_siteid();
        
        $where              = [];
        $where['module']    = $data['module'];
        $where['modelid']   = $data['modelid'];
        $where['catid']     = $data['catid'];
        $where['name']      = $data['name'];
        
        if (isset($data['modelid'])) unset($where['modelid']);
        if (isset($data['catid'])) unset($where['catid']);
        
        $ress = $this->posiModel->where($where)->find();
        $tips = '推送位置『'.$data['name'].'』已经存在！';
        if ($ress && $info['name'] != $data['name']) {
            $this->error($tips, $data);
        }
        
        if (empty($_FILES['thumb']['error']) &&
            isset($_FILES['thumb'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('图例上传失败！');
        
        $data['thumb']  = $this->upload->rootPath;
        $data['thumb'] .= $file['thumb']['savepath'];
        $data['thumb'] .= $file['thumb']['savename'];
        }
        if ($info['name'] == $data['name']) $name = $info['name'];
        else $name = $info['name'].'→'.$data['name'];
        
        if ($file) {
        $temp = [];
        $temp['filename']   = $file['thumb']['name'];
        $temp['filesize']   = $file['thumb']['size'];
        $temp['fileext']    = $file['thumb']['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['thumb'];
        cms_attachment($temp); //  @todo: 写入附件
        }
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $ress = $this->posiModel->where($where)->save($data);
        
        $tips = '修改推送『'.$name.'』位置';
        $info = ['posiid' => $info['posiid']];
        if (!$ress) $this->error($tips.'失败！', $info);
        else $this->success($tips.'成功！', $info, U('index'));
        } // @todo: 
        
        $posiid = I('get.posiid', 0, 'intval');
        $info = ['posiid' => $posiid];
        
        $where              = [];
        $where['disabled']  =  0;
        $where['ismodel']   =  1;
        $apps = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $data = $this->posiModel->where($where)->find();
        
        $this->assign('module', $apps);
        $this->assign('data', $data);
        $this->display('action');
    }
    
    /**
     * 删除位置
     */
    public final function delete() {
        $posiid = I('get.posiid', 0, 'intval');
        $info = I('post.info', []);
        $info = $posiid ? [$posiid] : $info['posiid'];
        $data = [];
        if (empty($info)) $this->error('请选择推送位置！');
        
        foreach ($info as $key => $posiid) {
        $data['posiid'][]   = $posiid;
        $where              = [];
        $where['posiid']    = $posiid;
        $ress = $this->posiModel->where($where)->find();
        
        $thumb = $ress['thumb'];
        if (file_exists($thumb)) @unlink($thumb);
        $this->dataModel->where($where)->delete();
        }
        $posiid = implode(',', $data['posiid']);
        
        $where              = [];
        $where['posiid']    = ['IN', $posiid];
        $ress = $this->posiModel->where($where)->delete();
        $info = 'posiid: '.$posiid;
        
        if (!$ress) $this->error('删除推送位置失败！', $info);
        else $this->success('删除推送位置成功！', $info);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $posiid = I('get.posiid', 0, 'intval');
        $info               = [];
        $info['posiid']     = $posiid;
        
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $ress2 = $this->posiModel->where($where)->find();
        $value = $ress2['status'] ? 0 : 1;
        
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $ress2 = $this->posiModel->where($where)->save([
            'status' => $value
        ]);
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $data2 = $this->posiModel->where($where)->find();
        $oper2 = $data2['status'] ? '禁用' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'推送『'.$data2['name'].'』位置';
        $tips2.= $state;
        cms_writelog($tips2, ['posiid' => $info['posiid']]);
        
        echo json_encode($data2['status']);
    }
    
    /**
     * 删除图例
     */
    public final function thumb() {
        $posiid = I('get.posiid', 0, 'intval');
        
        $where              = [];
        $where['posiid']    = $posiid;
        $ress2 = $this->posiModel->where($where)->find();
        $thumb = $ress2['thumb']; 
        
        if (file_exists($thumb)) @unlink($thumb);
        $ress2 = $this->posiModel->where($where)->save([
            'thumb' => null
        ]);
        if (!$ress2) $this->error('删除推送图例失败！');
        else $this->success('删除推送图例成功！');
    }
    
    /**
     * 查看图例
     */
    public final function image() {
        $this->assign('thumb', I('get.thumb', ''));
        $this->display();
    }
    
}
