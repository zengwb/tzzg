<?php

namespace Article\Controller;

use \Common\Controller\iPageController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块页面控制器类：配置或实例化应用模块
 * 
 * @author T-01
 */
abstract class PageController extends iPageController {
    
    protected   $site       = [],
                $skin       = '',
    
                $apps       = [],
                $category   = [],
                $page       = [],
                
                $search     = '';
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iPageController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
        
        $this->page         = D('Admin/Pages');
        $this->category     = D('Admin/Category');
        
        $this->apps         = D('Admin/Model');
        $this->site         = cms_site_info();
        $this->skin = $this->site['theme'] ? : C('DEFAULT_THEME');
        
        $this->site['logo'] = $this->site['setting']['logo'];
        $this->site['logo'] = strtr($this->site['logo'], ['./' => '/']);
        
        $this->site['public_beian'] = $this->site['setting']['public_beian'];
        
        $this->site['beian']= $this->site['setting']['beian'];
        
        $this->site['count']= $this->site['setting']['count'];
        $this->site['count']= htmlspecialchars_decode($this->site['count']);
        
        preg_match('/([0-9]+)/', $this->site['public_beian'], $code);
        $this->site['public_beian_code'] = $code[1] ? : '';
        
        C('DEFAULT_THEME', $this->skin.'/Views');
        C('TMPL_PARSE_STRING', [
            '__PLUG__' => C('TMPL_PARSE_STRING')['__PLUG__'],
            '__SKIN__' => C('TMPL_PARSE_STRING')['__SKIN__'].$this->skin.'/',
        ]);
        $ishtml = cms_config_array('Article', 'base')['cat_link'];
        $this->search = $ishtml ? '/search.html' : U('Home/Search/lists');
    }
    
}
