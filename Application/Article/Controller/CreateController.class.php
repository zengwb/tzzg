<?php

namespace Article\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 生成HTML控制器类
 * 
 * @author T-01
 */
final class CreateController extends BaseController {}
