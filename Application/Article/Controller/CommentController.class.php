<?php

namespace Article\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块评论控制器类：呈现评论CURD常规管理
 * 
 * @author T-01
 */
final class CommentController extends BaseController {
    
    public      $action     = [
        'index'
    ];
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    /**
     * 评论管理
     */
    public final function index() {
        echo __METHOD__;
    }
    
}
