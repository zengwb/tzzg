<?php

namespace Article\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块内容分类控制器类：呈现分类CURD常规管理
 * 
 * @author T-01
 */
final class TypeController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'model'
    ];
    
    private     $appsModel  = [],
                $modeModel  = [],
                
                $typeModel  = [],
                $cateModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->appsModel    = D('Admin/Module');
        $this->modeModel    = D('Admin/Model');
        
        $this->typeModel    = D('Type');
        $this->cateModel    = D('Admin/Category');
    }
    
    /**
     * 分类管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $order = '`listorder` ASC, `typeid` ASC';
        $where = [];
        $where['name'] = ['LIKE', '%'.$keyword.'%'];
        $where['siteid'] = cms_siteid();
        
        $nums = $this->typeModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->typeModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title'] = $row['name'];
        else $row['title'] = strtr($row['name'], $keys);
        
        $where = ['module' => $row['module']];
        $appss = $this->appsModel->where($where)->find();
        $row['module'] = $appss['name'] ? : '/';
        
        $where = ['modelid' => $row['modelid']];
        $model = $this->modeModel->where($where)->find();
        $row['model'] = $model['name'] ? : '/';
        
        $where = ['catid' => $row['catid']];
        $cates = $this->cateModel->where($where)->find();
        $row['catname'] = $cates['catname'] ?: '/';
        
        $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['typeid'] as $key => $typeid) {
        $listorder = $this->typeModel->where([
            'typeid' => $typeid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->typeModel->where(['typeid' => $typeid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $typeid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        self::success('内容分类排序成功！', $listorder);
    }
    
    /**
     * 添加分类
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $data['siteid'] = cms_siteid();
        
        if ($data['name'] && empty($data['description'])) {
            $data['description'] = $data['name'];
        }
        $where              = [];
        $where['module']    = $data['module'];
        $where['modelid']   = $data['modelid'];
        $where['catid']     = $data['catid'];
        $where['name']      = $data['name'];
        
        if (isset($data['modelid'])) unset($where['modelid']);
        if (isset($data['catid'])) unset($where['catid']);
        
        $type = $this->typeModel->where($where)->find();
        $tips = '内容分类『'.$data['name'].'』已经存在，当前操作';
        if ($type) $this->popup(false, $tips, $data);
        
        $tips = '添加内容『'.$data['name'].'』分类';
        $ress = $this->typeModel->add($data);
        
        if (!$ress) $this->popup(false, $tips, $data);
        $inid = $this->typeModel->getLastInsID();
        
        $this->typeModel->where(['typeid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->popup(true, $tips, $data, U('index'));
        } // @todo: 
        
        $where              = [];
        $where['ismodel']   =  1;
        $where['disabled']  =  0;
        $module = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('module', $module);
        $this->display('action');
    }
    
    /**
     * 修改分类
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $info = I('post.info', []);
        $data['siteid'] = cms_siteid();
        
        if ($data['name'] && empty($data['description'])) {
            $data['description'] = $data['name'];
        }
        $where              = [];
        $where['module']    = $data['module'];
        $where['modelid']   = $data['modelid'];
        $where['catid']     = $data['catid'];
        $where['name']      = $data['name'];
        
        if (isset($data['modelid'])) unset($where['modelid']);
        if (isset($data['catid'])) unset($where['catid']);
        
        $type = $this->typeModel->where($where)->find();
        $tips = '内容分类『'.$data['name'].'』已经存在，当前操作';
        
        if ($type && $info['name'] != $data['name']) {
            $this->popup(false, $tips, $data);
        }
        if ($info['name'] == $data['name']) $name = $info['name'];
        else $name = $info['name'].'→'.$data['name'];
        
        $where              = [];
        $where['typeid']    = $info['typeid'];
        $ress = $this->typeModel->where($where)->save($data);
        $tips = '修改内容『'.$name.'』分类';
        $info = ['typeid' => $info['typeid']];
        
        if (!$ress) $this->popup(false, $tips, $info);
        else $this->popup(true, $tips, $info);
        } // @todo: 
        
        $typeid = I('get.typeid', 0, 'intval');
        $info = ['typeid' => $typeid];
        
        $where = ['typeid' => $info['typeid']];
        $data = $this->typeModel->where($where)->find();
        
        $where              = [];
        $where['ismodel']   =  1;
        $where['disabled']  =  0;
        $module = $this->appsModel->where($where)->order(
            '`listorder` ASC, `moduleid` ASC'
        )->select();
        
        $this->assign('data', $data);
        $this->assign('module', $module);
        $this->display('action');
    }
    
    /**
     * 删除分类
     */
    public final function delete() {
        $typeid = I('get.typeid', 0, 'intval');
        $info = I('post.info', []);
        $info = $typeid ? [$typeid] : $info['typeid'];
        $data = [];
        if (empty($info)) $this->error('请选择内容分类！');
        
        foreach ($info as $key => $typeid) {
            $data['typeid'][] = $typeid;
        }
        $typeid = implode(',', $data['typeid']);
        
        $where              = [];
        $where['typeid']    = ['IN', $typeid];
        $type = $this->typeModel->where($where)->delete();
        $info = ['type' => $typeid];
        
        if (!$type) $this->error('删除内容分类失败！', $info);
        else $this->success('删除内容分类成功！', $info);
    }
    
    /**
     * 获取模型
     */
    public final function model() {
        $module = I('get.module', '', 'cms_addslashes');
        
        $where              = [];
        $where['module']    = $module;
        $where['disabled']  =  0;
        
        $model = $this->modeModel->where($where)->order(
            '`listorder` ASC, `modelid` ASC'
        )->select();
        
        echo json_encode($model);
    }
    
}
