<?php

namespace Article\Model;


class ProjectFainfoModel extends BaseModel
{
    /**
     * 查询数据
     * @return mixed
     */
      public function getDatainfo(){
          $data  = $this->field("id,name,city,area,indus_cate,deta_cate,content,total_inve,attract_inve,coop_ways,owner,contact_info")
                   ->select();
          return $data;
      }
/*
      public function getDealDate($datas){
         foreach()
      }*/
    /** 添加到失败是数据表
     * @param $datas
     */
    public function dealFalData($datas){
        $this->linkageModel = M("linkage");
        foreach ($datas as $data){
            $faldata_where['title'] = ['eq',$data['title']];
            if($this->where($faldata_where)->find()){
                //重复的数据
            }else{

                //通过编码反找具体的文字
                $data['city'] = $this->linkageModel->where('linkageid='.$data['city'])->getfield("name");
                empty($data['city'])?($data['city']=0):($data['city']=$data['city']);
                $data['area'] = $this->linkageModel->where('linkageid='.$data['area'])->getfield("name");
                empty($data['area'])?($data['area']=0):($data['area']=$data['area']);
                $data['indus_cate'] = $this->linkageModel->where('linkageid='.$data['indus_cate'])->getfield("name");
                empty($data['indus_cate'])?($data['indus_cate']=0):($data['indus_cate']=$data['indus_cate']);
                $coop_where='';
                $coop_where['linkageid'] = ['in',$data['coop_ways']];

                $coop_arr = $this->linkageModel->where($coop_where)->getfield("linkageid,name");
                $data['coop_ways'] = implode(',',$coop_arr);
                empty($data['coop_ways'])?($data['coop_ways']=0):($data['coop_ways']=$data['coop_ways']);


                //对联系信息的处理
                $this->add($data);
            }
        }
    }


}