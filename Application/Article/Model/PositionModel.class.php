<?php

namespace Article\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块推送位置模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class PositionModel extends BaseModel {}
