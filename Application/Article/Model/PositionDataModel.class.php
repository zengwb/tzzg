<?php

namespace Article\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 文章模块推送数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class PositionDataModel extends BaseModel {
    
    /**
     * 添加内容
     * 
     * @param array     $info       必填。添加内容
     * @param array     $data       必填。内容数据
     * 
     * @return boolean
     */
    public final function insert($info, $data = []) {
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $where['catid']     = $info['catid'];
        $where['contentid'] = $info['contentid'];
        if ($this->where($where)->find()) return;
        
        $info['content'] = serialize($data);
        return $this->add($info);
    }
    
    /**
     * 更新内容
     * 
     * @param array     $info       必填。更新条件
     * @param array     $data       必填。内容数据
     * 
     * @return boolean
     */
    public final function update($info, $data = []) {
        $where              = [];
        $where['posiid']    = $info['posiid'];
        $where['catid']     = $info['catid'];
        $where['contentid'] = $info['contentid'];
        
        $ress = $this->where($where)->find();
        if ($ress) {
        $info['content'] = serialize($data);
        return $this->where($where)->save($info);
        }
        return $this->insert($info, $data);
    }
    
    /**
     * 清除内容
     * 
     * @param array     $info       必填。清除条件
     * 
     * @return boolean
     */
    public final function clear($info) {
        $where              = [];
        $where['contentid'] = $info['contentid'];
        $where['catid']     = $info['catid'];
        
        return $this->where($where)->delete();
    }
    
    
}
