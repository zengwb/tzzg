<?php

namespace Linkage\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 链接模块分组数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class GroupModel extends BaseModel {
    protected $tableName = 'link_type';
}
