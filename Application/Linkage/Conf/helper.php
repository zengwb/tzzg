<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Linkage',
    'version'               => 'v1.0.01',
    
    'name'                  => '友情链接',
    'description'           => '',
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
    
];
