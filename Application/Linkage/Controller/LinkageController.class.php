<?php

namespace Linkage\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 链接模块内容管理控制器类：呈现链接CURD常规管理
 * 
 * @author T-01
 */
final class LinkageController extends BaseController {
    
    public      $action     = [
        'index', 'listorder', 'add', 'edit', 'delete', 'state'
    ];
    
    private     $typeModel  = [],
                $linkModel  = [],
                
                $upload     = null,
                $images     = null;
    
    /**
     * {@inheritDoc}
     * @see \Linkage\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->upload = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Linkage/';
        
        $this->upload->subName  = null;
        $this->upload->saveName = date('YmdHis', time());
        $this->upload->exts = ['jpg','jpeg','gif','png'];
        
        $this->images = (new \Think\Image());
        
        $this->typeModel    = D('Linkage/Group');
        $this->linkModel    = D('Linkage/Link');
    }
    
    /**
     * 链接管理
     */
    public final function index() {
        $keyword = I('request.key', null, 'cms_addslashes');
        $keyword = I('get.key', $keyword, 'cms_addslashes');
        $keyword = I('post.key',$keyword, 'cms_addslashes');
        
        $order = '`listorder` ASC, `linkid` ASC';
        $where              = [];
        $where['name']      = ['LIKE', '%'.$keyword.'%'];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        
        $nums = $this->linkModel->where($where)->count();
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyword;
        
        $keys = [
            $keyword => '<span class="cms-cf60">'.$keyword.'</span>'
        ];
        $ress = $this->linkModel->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order($order)->select();
        
        foreach ($ress as $key => $row) {
        if (!$keyword) $row['title'] = $row['name'];
        else $row['title'] = strtr($row['name'], $keys);
        
        $where              = [];
        $where['typeid']    = $row['typeid'];
        $type = $this->typeModel->where($where)->find();
        $row['type'] = $type['name'];
        
        $row['style'] = unserialize($row['style']);
        $data[$key] = $row;
        }
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['linkid'] as $key => $linkid) {
        $listorder = $this->linkModel->where([
            'linkid' => $linkid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->linkModel->where(['linkid' => $linkid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $linkid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('链接内容排序成功！', $listorder);
    }
    
    /**
     * 添加链接
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        $tcss = I('post.style',[]);
        
        $data['username']   = session('CMS_UserName');
        $data['siteid']     = cms_siteid();
        
        $data['style']      = serialize($tcss);
        $data['addtime']    = time();
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        if (empty($_FILES['image']['error']) &&
            isset($_FILES['image'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('图片上传失败！');
        
        $data['image']      = $this->upload->rootPath;
        $data['image']     .= $file['image']['savepath'];
        $data['image']     .= $file['image']['savename'];
        }
        
        $tips = '添加链接『'.$data['name'].'』信息';
        $ress = $this->linkModel->add($data);
        if (!$ress) $this->error($tips.'失败！', $data);
        
        $inid = $this->linkModel->getLastInsID();
        
        if ($file && $inid) {
        $temp = [];
        $temp['filename']   = $file['image']['name'];
        $temp['filesize']   = $file['image']['size'];
        $temp['fileext']    = $file['image']['ext'];
        $temp['isimage']    = 1;
        $temp['filepath']   = $data['image'];
        cms_attachment($temp); // @todo: 写入附件
        }
        $this->linkModel->where(['linkid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $type = $this->typeModel->where($where)->order(
            '`listorder` ASC, `typeid` ASC'
        )->select();
        
        $this->assign('type', $type);
        $this->assign('data', ['status' => 1]);
        
        $this->display('action');
    }
    
    /**
     * 修改链接
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $tcss = I('post.style',[]);
        
        $data['username']   = session('CMS_UserName');
        $data['siteid']     = cms_siteid();
        
        $data['style']      = serialize($tcss);
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        if (empty($_FILES['image']['error']) &&
            isset($_FILES['image'])) {
        $file = $this->upload->upload();
        if (!$file) $this->error('图片上传失败！');
        
        $data['image']      = $this->upload->rootPath;
        $data['image']     .= $file['image']['savepath'];
        $data['image']     .= $file['image']['savename'];
        }
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $where              = [];
        $where['linkid']    = $info['linkid'];
        
        $tips = '修改链接『'.$name.'』信息';
        $ress = $this->linkModel->where($where)->save($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $linkid = I('get.linkid', 0, 'intval');
        
        $info               = [];
        $info['linkid']     = $linkid;
        
        $where              = [];
        $where['linkid']    = $info['linkid'];
        
        $data = $this->linkModel->where($where)->find();
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $type = $this->typeModel->where($where)->order(
            '`listorder` ASC, `typeid` ASC'
        )->select();
        
        $this->assign('type', $type);
        $this->assign('data', $data);
        $this->assign('style', unserialize($data['style']));
        
        $this->display('action');
    }
    
    /**
     * 删除链接
     */
    public final function delete() {
        $linkid = I('get.linkid', 0, 'intval');
        $info = I('post.info', []);
        $info = $linkid ? [$linkid] : $info['linkid'];
        $data = [];
        if (empty($info)) $this->error('请选择链接内容！');
        
        foreach ($info as $key => $linkid) {
        $data['linkid'][]   = $linkid;
        $where              = [];
        $where['linkid']    = $linkid;
        $ress = $this->linkModel->where($where)->find();
        $file = $ress['image'];
        if (file_exists($file)) @unlink($file);
        }
        $where              = [];
        $where['linkid']    = ['IN', $data['linkid']];
        $ress = $this->linkModel->where($where)->delete();
        
        if (!$ress) $this->error('删除链接失败！', $data);
        else $this->success('删除链接成功！', $data);
    }
    
    /**
     * 重置状态
     */
    public final function state() {
        $linkid = I('get.linkid', 0, 'intval');
        $info               = [];
        $info['linkid']     = $linkid;
        
        $where              = [];
        $where['linkid']    = $info['linkid'];
        $ress2 = $this->linkModel->where($where)->find();
        $value = $ress2['status'] ? 0 : 1;
        
        $where              = [];
        $where['linkid']    = $info['linkid'];
        $ress2 = $this->linkModel->where($where)->save([
            'status' => $value
        ]);
        $where              = [];
        $where['linkid']    = $info['linkid'];
        $data2 = $this->linkModel->where($where)->find();
        $oper2 = $data2['status'] ? '隐藏' : '启用';
        
        if (!$ress2) $state = '失败！';
        else $state = '成功！';
        $tips2 = $oper2.'链接『'.$data2['name'].'』内容';
        $tips2.= $state;
        cms_writelog($tips2, ['linkid' => $info['linkid']]);
        
        echo json_encode($data2['status']);
    }
    
    /**
     * Ajax
     */
    public final function ajax() {
        $typeid = I('get.typeid', 0, 'intval');
        $info               = [];
        $info['typeid']     = $typeid;
        
        $where              = [];
        $where['typeid']    = $info['typeid'];
        
        $ress = $this->typeModel->where($where)->find();
        $setting = unserialize($ress['setting']);
        echo json_encode($setting['isimage']);
    }
    
    /**
     * 删除图片
     */
    public final function image() {
        $linkid = I('get.linkid', 0, 'intval');
        $info               = [];
        $info['linkid']     = $linkid;
        
        $where              = [];
        $where['linkid']    = $info['linkid'];
        
        $ress = $this->linkModel->where($where)->find();
        $file = $ress['image'];
        if (file_exists($file)) @unlink($file);
        
        $ress = $this->linkModel->where($where)->save([
            'image' => null
        ]);
        if (!$ress) $this->error('删除图片失败！', $file);
        else $this->success('删除图片成功！', $file);
    }
    
}
