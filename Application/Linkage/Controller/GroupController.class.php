<?php

namespace Linkage\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 链接模块分组管理控制器类：呈现分组CURD常规管理
 * 
 * @author T-01
 */
final class GroupController extends BaseController {
    
    public      $action     = [];
    
    private     $typeModel  = [],
                $linkModel  = [],
                $siteModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Linkage\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->typeModel    = D('Group');
        $this->linkModel    = D('Link');
        
        $this->siteModel    = D('Admin/Website');
    }
    
    /**
     * 分组管理
     */
    public final function index() {
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $ress = $this->typeModel->where($where)->order(
            '`listorder` ASC, `typeid` ASC'
        )->select();
        
        foreach ($ress as $key => $row) {
        $where              = [];
        $where['siteid']    = $row['siteid'];
        $site = $this->siteModel->where($where)->find();
        $row['site'] = $site['name'] ?: '全部站点';
        $lists[$key] = $row;
        }
        $this->assign('data', $lists);
        $this->display();
    }
    
    /**
     * 显示排序
     */
    public final function listorder() {
        $data = I('post.data', [], 'cms_addslashes');
        $list = [];
        
        foreach ($data['typeid'] as $key => $typeid) {
        $listorder = $this->typeModel->where([
            'typeid' => $typeid
        ])->find()['listorder'];
        if ($listorder == $data['listorder'][$key]) {
            continue;
        }
        $this->typeModel->where(['typeid' => $typeid])->save([
            'listorder' => $data['listorder'][$key]
        ]);
        $listorder = $listorder.'='.$data['listorder'][$key];
        $list['listorder'][] = $typeid.':'.$listorder;
        }
        $listorder = $list ? : 'null';
        $this->success('链接分组排序成功！', $listorder);
    }
    
    /**
     * 分组配置
     */
    public final function setting() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        $setting = serialize($data);
        
        $where              = [];
        $where['typeid']    = $info['typeid'];
        
        $ress = $this->typeModel->where($where)->save([
            'setting'=>$setting
        ]);
        $tips = '配置链接『'.$info['name'].'』分组';
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $typeid = I('get.typeid', 0, 'intval');
        
        $info               = [];
        $info['typeid']     = $typeid;
        
        $where              = [];
        $where['typeid']    = $info['typeid'];
        
        $data = $this->typeModel->where($where)->find();
        
        $this->assign('data', $data);
        $this->assign('setting', unserialize($data['setting']));
        
        $this->display();
    }
    
    /**
     * 添加分组
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['name']      = trim($data['name']);
        $where['siteid']    = $data['siteid'];
        
        $ress = $this->typeModel->where($where)->find();
        $tips = '链接分组『'.$data['name'].'』已经存在！';
        if ($ress) $this->error($tips);
        
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        $tips = '添加链接『'.$data['name'].'』分组';
        $ress = $this->typeModel->add($data);
        
        if (!$ress) $this->error($tips.'失败！');
        $inid = $this->typeModel->getLastInsID();
        
        $this->typeModel->where(['typeid' => $inid])->save([
            'listorder' => $inid
        ]);
        $this->success($tips.'成功！', '', U('index'));
        } // @todo:
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $this->assign('site', $site);
        $this->assign('data', ['siteid' => 0]);
        
        $this->display('action');
    }
    
    /**
     * 修改分组
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
        $info = I('post.info', []);
        $data = I('post.data', [], 'cms_addslashes');
        
        $where              = [];
        $where['name']      = trim($data['name']);
        $where['siteid']    = $data['siteid'];
        
        $ress = $this->typeModel->where($where)->find();
        $tips = '链接分组『'.$data['name'].'』已经存在！';
        
        if ($ress && $info['name'] != $data['name']) {
            $this->error($tips);
        }
        if (empty($data['description']) && !empty($data['name'])) {
            $data['description'] = $data['name'];
        }
        if ($info['name'] == $data['name']) {
            $name = $info['name'];
        } else {
            $name = $info['name'].'→'.$data['name'];
        }
        $tips = '修改链接『'.$data['name'].'』分组';
        
        $where              = [];
        $where['typeid']    = $info['typeid'];
        $ress = $this->typeModel->where($where)->save($data);
        
        if (!$ress) $this->error($tips.'失败！');
        else $this->success($tips.'成功！', '', U('index'));
        } // @todo: 
        
        $typeid = I('get.typeid', 0, 'intval');
        
        $info               = [];
        $info['typeid']     = $typeid;
        
        $where              = [];
        $where['typeid']    = $info['typeid'];
        $data = $this->typeModel->where($where)->find();
        
        $where              = [];
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $where['display']   =  1;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
            unset($where['siteid']);
        
        $site = $this->siteModel->where($where)->order(
            '`listorder` ASC, `siteid` ASC'
        )->select();
        
        $this->assign('site', $site);
        $this->assign('data', $data);
        
        $this->display('action');
    }
    
    /**
     * 删除分组
     */
    public final function delete() {
        $typeid = I('get.typeid', 0, 'intval');
        $info = I('post.info', []);
        $info = $typeid ? [$typeid] : $info['typeid'];
        $data = [];
        if (empty($info)) $this->error('请选择链接分组！');
        
        foreach ($info as $key => $typeid) {
        $data['typeid'][]   = $typeid;
        $where              = [];
        $where['typeid']    = $typeid;
        $ress = $this->linkModel->where($where)->select();
        
        foreach ($ress as $i => $row) {
        if (file_exists($row['image'])) @unlink($row['image']);
        }
        $this->linkModel->where($where)->delete();
        }
        $where              = [];
        $where['typeid']    = ['IN', $data['typeid']];
        $ress = $this->typeModel->where($where)->delete();
        
        if (!$ress) $this->error('删除分组失败！', $data);
        else $this->success('删除分组成功！', $data);
    }
    
}
