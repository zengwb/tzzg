<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'VIEW_PATH'             => './Resources/Template/',
    'TMPL_TEMPLATE_SUFFIX'  => '',          // 模板后缀（自定）
    'TMPL_FILE_DEPR'        => '/',         // 模板分割符
    
    'DEFAULT_THEME'         => 'Default',   // 默认主题
    
];
