<?php

namespace Home\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 主模块主控制器类 统计记录IP对页面的访问次数
 * 
 * @author T-01
 */
final class VisitController extends PageController {
    
    private     $article    = null;
    private     $web;
    
    /**
     * {@inheritDoc}
     * @see \Home\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    //根据IP和访问页导航的ID记录页面访问次数
    public final function index ($catid)
    {
        
        $code = 0;
        $data['siteid']= cms_siteid();
        $data['catid']= $catid;
        $data['time'] = date('Y-m-d',time());
        $ip = get_client_ip();
        $cook_name = $data.'-'.$ip;
        //cookie($cook_name,NULL);
        if(cookie($cook_name)){
            $data['ip'] = $ip;
            if(cookie($cook_name)['status']==1){
                $where['id'] = cookie($cook_name)['id'];
                D('Admin/visitip')->where($where)->setInc('num',1);
            }
        }else{
            $value['status'] = 1;
            $data['num'] =1;
            if(M('Visit')->add($data)){
                $data['ip'] = $ip;
                $vid['id'] = M('Visit')->query("select last_insert_id()")[0]['last_insert_id()'];
                if(M('Visitip')->add($data)){
                    $id = M('Visitip')->query("select last_insert_id()")[0]['last_insert_id()'];
                    $value['id'] = $id;
                    cookie($cook_name,$value,3600*24);
                    $code=1;
                }else{
                   M('Visit')->where($vid)->delete();
                }
                
           }
            
        }
        //dump($code);die;
        return $code;
    }
    //获取站点ID
    
}
