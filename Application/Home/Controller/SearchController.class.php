<?php

namespace Home\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 主模块搜索控制器类
 * 
 * @author T-01
 */
final class SearchController extends PageController {
    
    private     $search     = null;
    
    /**
     * {@inheritDoc}
     * @see \Home\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->search = (
            new \Article\Controller\SearchController()
        );
    }
    
    public final function index() { $this->search->index(); }
    public final function lists() { $this->search->lists(); }
    public final function ajax2() { $this->search->ajax2(); }
    
}
