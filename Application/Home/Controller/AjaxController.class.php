<?php

namespace Home\Controller;
final class AjaxController extends PageController {
    
    public final function _initialize() {
        parent::_initialize();
        $this->ajax = (
            new \Home\Model\AjaxModel()
        );
    }
    public final function projct()
    {
    	if(IS_AJAX){
    		$data = $_GET;
    		//dump($data);die;
    		if($data['updatetime'])$data['p'] = $data['updatetime'];
    		if($data['price'])$data['p'] = $data['price'];
    		$arr = $this->getfield($data);
    		$count = $_GET['count'];
    		//dump($arr);die;
    		if($count){
    			$result = $this->ajax->pro_list($arr,$data['table'],$count);
    		}else{
    			$result = $this->ajax->pro_list($arr,$data['table']);
    		}
    		$this->ajaxReturn($result);
    	}else{
    		$this->ajaxReturn(2);
    	}
    }
    public final function getfield($data)
    {
    	//dump($data);die;
    	switch ($data['table']){
    		case 'v1_tzzg_jzfu_xm':
    			if($data['cityid'])$arr['where']['cityid'] = ['like',"{$data['cityid']}%"];
    			$arr['p'] = $data['p'];
    			$arr['where']['linkageid'] = $data['linkageid'];
    			$arr['where']['linkageid_bu'] = $data['linkageid_bu'];
    			$arr['where']['hzfs'] = $data['hzfs'];
    			$arr['where']['jscsd'] = $data['jscsd'];
    			$arr['where']['zrj'] = $data['zrj'];
    			$arr['where']['fwlx'] = $data['fwlx'];
    			$arr['where']['zrdx'] = $data['zrdx'];
    			$arr['curPage'] = $data['curPage'];
    			break;
    		case 'v1_tzzg_fpkf':
    			$arr['where']['province_id'] = $data['province_id'];
                //dump($data['province_id']);die;
    			break;
    		case 'v1_tzzg_fpxm':
    			if($data['cityid'])$arr['where']['cityid'] = ['like',"{$data['cityid']}%"];
    			if($data['address'])$arr['where']['address'] = ['like',"{$data['address']}%"];
    			$arr['p'] = $data['p'];
    			$arr['where']['linkageid'] = $data['linkageid'];
    			$arr['where']['hzfs'] = $data['hzfs'];
    			$arr['where']['price_all'] = $data['price_all'];
    			$arr['where']['is_status'] = $data['is_status'];
    			$arr['where']['price_type'] = $data['price_type'];
    			$arr['where']['tzlb'] = $data['tzlb'];
    			$arr['curPage'] = $data['curPage'];
    			break;
    		default:
    			# code...
    			break;
    	}
    	foreach($arr as $k=>$v){
    		foreach($v as $k1=>$v1){
    			if(empty($v1))unset($arr[$k][$k1]);
    		}
    	}
    	return $arr;
    }
    public final function fpkfj()
    {
    	if(IS_AJAX){
    		$data = $_GET;
    		$arr = $this->getfield($data);
    		//dump($data);die;
    		$result = $this->ajax->fpkf_list($arr,$data['table']);
    		//dump($result);die;
    		$this->ajaxReturn($result);
    	}else{
    		$this->ajaxReturn(2);
    	}
    }
}
