<?php

namespace Home\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 主模块投票控制器类
 * 
 * @author T-01
 */
final class VoteController extends PageController {
    
    /**
     * {@inheritDoc}
     * @see \Article\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    public final function index() {
        echo __METHOD__;
    }
    
}
