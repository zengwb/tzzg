<?php

namespace Home\Controller;

use \Common\Controller\iPageController;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 主模块页面控制器类：配置或实例化系统模块
 * 
 * @author T-01
 */
abstract class PageController extends iPageController {
    
    /**
     * {@inheritDoc}
     * @see \Common\Controller\iHelperController::_initialize()
     */
    public function _initialize() { parent::_initialize(); }
    
}
