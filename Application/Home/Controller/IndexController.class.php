<?php

namespace Home\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 主模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    private     $article    = null;
    
    /**
     * {@inheritDoc}
     * @see \Home\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->article = (
            new \Article\Controller\IndexController()
        );
    }
    
    public final function index() { $this->article->index(); }
    public final function lists() { $this->article->lists(); }
    public final function shows() { $this->article->shows(); }
    public final function demo2() { $this->article->demo2(); }
    public final function ajax2() { $this->article->ajax2(); }
    public final function ajaxLinkage() { $this->article->ajaxLinkage(); }
    public final function ajaxGetProList(){ $this->article->ajaxGetProList();}
    public final function ajaxshen(){ $this->article->ajaxshen();}
    public final function ajaxGetBuweiList(){ $this->article->ajaxGetBuweiList();}
    public final function buweiinfo(){ $this->article->getBuWeiInfo();}
    public final function showBuweiProject(){ $this->article->buweiProject();}
    public final function ajaxGetBuweiDetail(){ $this->article->ajaxGetBuweiDetail();}
    public final function buweiProDetail(){ $this->article->ajaxBuweiProDetail();}
    public final function get_asset_project(){ $this->article->get_asset_project();}
    public final function get_zjtz_project(){ $this->article->get_zjtz_project();}
    public final function ajaxProvinceAgency(){ $this->article->ajaxProvinceAgency();}
    public final function ajaxAgency(){ $this->article->ajaxAgency();}
    public final function ajaxCity(){ $this->article->ajaxCity();}
    public final function ajaxProvinceBumen(){ $this->article->ajaxProvinceBumen();}
    public final function provinceData(){ $this->article->getProvinceData();}
    public final function getShengjiData(){ $this->article->getShengjiData();}
    public final function shengjiShowArticle(){ $this->article->shengjiShowArticle();}
    public final function shengjiShowProject(){ $this->article->shengjiShowProject();}
    public final function sjNextPrivsArticle(){ $this->article->sjNextPrivsArticle();}
    public final function disSjProject(){ $this->article->disSjProject();}
    public final function sjGetNews(){ $this->article->sjGetNews();}
    public final function showProvinceCity(){ $this->article->showProvinceCity();}
    public final function ProvinceCityData(){ $this->article->ProvinceCityData();}
    public final function showCityNews(){ $this->article->showCityNews();}
    public final function showCityPro(){ $this->article->showCityPro();}
    public final function szcsxcCityInfo(){ $this->article->szcsxcCityInfo();}
    public final function szcsxcData(){ $this->article->szcsxcData();}
    public final function conferencelist(){ $this->article->conferenceListData();}
    public final function addParticipation(){ $this->article->addParticipation();}
    public final function busassoclist(){ $this->article->busassoclist();}
    public final function qyk(){ $this->article->getqyk();}
    public final function getjrjg(){ $this->article->getjrjgList();}
    public final function getfundlist(){ $this->article->getfundlist();}
    public final function diplayQu(){ $this->article->diplayQu();}
    public final function ajaxpage(){
        $city = I('get.city',24);
        $page = I('get.page',1);
        $this->article->xiangmu($city,$page);
    }
    public final function ajaxbumen(){ 
        $linkageid = I('get.linkageid');
        $parentid = I('get.parentid');
        //dump(I('get.'));die;
        $this->article->bumen($linkageid,$parentid,1);
    }
    public final function get_financing_project(){ $this->article->get_financing_project();}

    /**
     * 购书用户注册
     */
    public final function bookbuyerRegistor(){
        $this->display('Member:registor.html');
    }

    /**
     * 发送短信验证码
     */
    public final function send_verify(){
        $code = 200;
        $message = "";
        $phone = I('get.');
        $is_registor = $this->is_registor($phone['phone']);

        switch ($is_registor){
            case 1;
                $code = 1;
                $message = "手机号码已经注册过";
                $res = [
                    'code'      =>  $code,
                    'message'   =>  $message
                ];
                $this->ajaxReturn($res);
                exit;
            case 2;
                //信息不完整
                $code = 2;
                $message = "您填写的信息不完整";

                $uinfo = D('Member/tzzgBookbuyers')->where(['phone'=>$phone['phone']])->find();
                $data['id'] = $uinfo['userid'];
                $res = [
                    'code'      =>  $code,
                    'message'   =>  $message,
                    'data'      => $data
                ];
                $this->ajaxReturn($res);
                exit;
            case 3;
                $verify = substr(base_convert(md5(uniqid(md5(microtime(true)),true)), 16, 10), 0, 6);
                $sensms = new \Sms\Yimei\SendSms();

                $content = '尊敬的用户，您好！您的注册验证码是：【'.$verify.'】,有效时间为3分钟';
                $resObj = $sensms->SendSingleSMS($phone['phone'],$content);
                $resObj->ciphertext = "";
                if($resObj->result == "SUCCESS"){
                    //保存session
                    session('verify',$verify);
                    $code = 200;
                    $message = "success";
                    $res = [
                        'code'      =>  $code,
                        'message'   =>  $message
                    ];
                    $this->ajaxReturn($res);
                    exit;
                }else{
                    $code = 1;
                    $message = "短信发送失败";
                    $res = [
                        'code'      =>  $code,
                        'message'   =>  $message
                    ];
                    $this->ajaxReturn($res);
                    exit;
                }
                break;
        }

    }

    /**
     * 手机号是否已注册
     */
    public final function is_registor($phone){
        $u = D('Member/tzzgBookbuyers')->where(['phone'=>$phone])->find();
        if($u){
            if(!$u['is_all']){
                //信息不完整
                return 2;
            }else{
                //信息完整
                return 1;
            }
        }else{
            return 3;
        }

    }

    /**
     * 验证手机、短信验证码、密码
     */
    public final function check_info(){
        $code = 200;
        $message = '';
        $data = [];

        $params = I('post.');
        if(strlen($params['phone']) == 0 || strlen($params['password']) == 0 || strlen($params['verify']) == 0){
            $code = 0;
            $message = "请确认信息是否填写完整";
            $res = array('code'=>$code, 'message'=>$message, 'data'=>$data);
            $this->ajaxReturn($res);
            exit;
        }

        //是否注册和信息是否完整
        $is_r = $this->is_registor($params['phone']);
        if($is_r == 1){
            $code = 0;
            $message = "该手机号已经注册";
            $res = array('code'=>$code, 'message'=>$message, 'data'=>$data);
            $this->ajaxReturn($res);
            exit;
        }else if($is_r == 2){
            $code = 2;
            $message = "填写信息不完整";
            $uinfo = D('Member/tzzgBookbuyers')->where(['phone'=>$params['phone']])->find();
            $data['id'] = $uinfo['userid'];
            $res = array('code'=>$code, 'message'=>$message, 'data'=>$data);
            $this->ajaxReturn($res);
            exit;
        }



        if(session('verify') != $params['verify']){
            $code = 1;
            $message = "验证码错误";
            $res = array('code'=>$code, 'message'=>$message, 'data'=>$data);
            $this->ajaxReturn($res);
            exit;
        }
        $params['password'] = cms_password($params['password'])['password'];

        $id = D('Member/tzzgBookbuyers')->add($params);
        $data['id'] = $id;
        $res = array('code'=>$code, 'message'=>$message, 'data'=>$data);
        $this->ajaxReturn($res);
    }

    /**
     * 填写注册单位等信息,显示页面
     */
    public final function registor_two(){
        $data = I("get.");
        $this->assign('data',$data);
        $this->display("Member:buybook.html");
    }

    /**
     * 验证注册单位等信息
     */
    public final function check_registor_two(){
        if(IS_POST){
            $code = 200;
            $message = "";
            $params = I("post.");
            foreach ($params as $k=>$v){
                if(!$v){
                    $code = 0;
                    $message = "请确认信息是否填写完整";
                    $res = array('code'=>$code, 'message'=>$message);
                    $this->ajaxReturn($res);
                    exit;
                }
            }
            $userid['userid'] = $params['userid'];
            unset($params['userid']);
            $params['is_all'] = 1;
            $m = D('Member/tzzgBookbuyers')->where($userid)->save($params);
            if($m === false){
                $code = 1;
                $message = "注册失败";
                //单位信息、税号、地址等数据写入失败后，删除之前写入的数据；
                D('Member/tzzgBookbuyers')->where(['userid'=>$userid['userid']])->delete();
            }
            $res = array('code'=>$code, 'message'=>$message);
            $this->ajaxReturn($res);
        }
    }

    /**
     * 获取城市介绍信息(包括国家部委单位)
     */
    public function getcityinfo(){
        $params = I('get.');
        $params['display'] = 1;
        $data = D('Article/Linkage')->where($params)->field('image,introduce')->find();
        if($data){
            $ress['code'] = 200;
            $ress['data'] = $data;
        }else{
            $ress['code'] = 111;
            $ress['data'] = $data;
        }
        $this->ajaxReturn($ress);
    }

    /**
     * 获取事业单位
     */
    public final function getunits(){
        $params = I('get.');
        $params['disabled'] = 0;
        $data = D('Article/Institution')->where($params)->field('name,image,abouts,unitid')->order('`listorder` ASC')->select();


        $where = [];
        $where['disabled'] = 0;
        $where['unitid'] = $params['parentid'];


        $ress['params'] = $params;
        if($data){
            $ress['code'] = 200;
            $ress['data'] = $data;
        }else{
            $_data = D('Article/Institution')->where($where)->field('name,image,abouts,unitid')->order('`listorder` ASC')->find();
            $sql = M()->getLastSql();
            $ress['sql'] = $sql;
            $ress['code'] = 111;
            $ress['data'] = $_data;
        }
        $this->ajaxReturn($ress);
    }

    public final function tsbg(){
        $seo = cms_site_seo($catid, [
            'title'         => '态势报告',
            'description'   => $data['description'],
            'keywords'      => $data['keywords'],
        ]);
        //site信息，logo
        $site = cms_site_info();
        $site['logo'] = $site['setting']['logo'];
        $site['logo'] = strtr($site['logo'], ['./' => '/']);
        $this->assign('site',$site);
        $this->assign('seo', $seo);
        $this->display('Article:list_tsbg.html');
    }

    public final function tsbgxx(){
        $this->display('Article:show_tsbgxx.html');
    }


    /**
     * 获取联动菜单下默认的第一个子菜单  省级-市级-区级
     */
    public final function getDefChild(){
        $params = I('get.');
        $params['display'] = 1;
        $data = D("Admin/Linkage")->where($params)->order("`listorder` ASC")->find();
        $this->ajaxReturn($data);
    }
}
