<?php
namespace Home\Model;
use Think\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');
header("content-type:text/html;charset=utf-8");
/**
 * 主模块页面数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class ajaxModel extends Model{

	protected $tableName;
	public final function pro_list($arr,$table,$count='')
	{
		$this->tableName = $table;
		$pageSize = 1;
		//dump($arr);die;
		$model = $this->table($this->tableName)->where($arr['where']);
		if(isset($arr['p'])){
			$model->order("{$arr['p']} desc");
			$model1->order("{$arr['p']} desc");
		}
		//->fetchSql(true)
		if($count)$model->limit($count);
		$mod= $model->page($arr['curPage'].','.$pageSize)->select();
		foreach($mod as $k=>$v){
			$mod[$k]['cityid'] = $this->city($v['cityid']);
			$mod[$k]['linkageid'] = $this->city($v['linkageid']);
			if($mod[$k]['hzfs'])$mod[$k]['hzfs'] = $this->city($v['hzfs']);
		}
		$data['content'] = $mod;
		$data['pageinfo']['total'] = $this->table($this->tableName)->where($arr['where'])->count();
		$data['pageinfo']['curPage'] = $arr['curPage'];
		$data['pageinfo']['pageSize'] = $pageSize;
		$data['pageinfo']['totalPage'] = $data['pageinfo']['total']/$pageSize;
		return $data;
	}
	public final function city($str)
	{
		if(strstr($str,',')){
			$arr['linkageid'] = ['in',explode(',', $str)];
		}else{
			$arr['linkageid'] = $str;
		}
		$data = $this->table('v1_linkage')->field('name')->where($arr);
		if(strstr($str,',')){
			$re = $data->select();
			$re = implode(',', array_column($re, 'name'));
		}else{
			$re = $data->find()['name'];
		}
		return $re;
	}
	public final function fpkf_list($arr,$table)
	{
		$this->tableName = $table;
		$data['js'] = $this->fpkf_jiesao($arr);
		$data['yw'] = $this->fpkf_yaowen($arr,1);
		$data['zcjd'] = $this->fpkf_zcjd($arr,1);
		//dump($data);die;
		return $data;
	}
	public final function fpkf_jiesao($arr)
	{
		$model = $this->table($this->tableName)->field('mid,catid,content_js,form,form_info')->where($arr['where'])->order("inputtime desc")->find();
		//dump($model);die;
		return $model;
	}
	public final function fpkf_yaowen($arr,$title='')
	{
		//SELECT * FROM `v1_linkage` where linkageid=4089
		$name = '要闻';
		$arr['where']['typeid'] = $this->type($name);
		$model = $this->table($this->tableName);
		if($title){
			$model->field('mid,catid,title');
		}
		$model = $model->where($arr['where'])->order("inputtime desc")->limit(9)->select();
		//->fetchSql(true)
		return $model;
	}
	public final function fpkf_zcjd($arr,$title='')
	{
		$name = '政策解读';
		$arr['where']['typeid'] = $this->type($name);
		$model = $this->table($this->tableName)->where($arr['where']);
		if($title){
			$model->field('mid,catid,title,description,thumb,inputtime');
		}
		$data = $model->order("inputtime desc")->limit(3)->select();
		foreach($data as $k=>$v){
			
			$data[$k]['inputtime'] = date('Y-m-d',$v['inputtime']);
		}
		//dump($data);die;
		return $data;
	}
	public function type($name)
	{
		$data['name'] = $name;
		$typeid = $this->table('v1_linkage')->field('linkageid')->where($data)->find()['linkageid'];
		//dump($typeid);die;
		return $typeid;
	}
}
