<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

return [
    
    'module'                => 'Member',
    'version'               => 'v1.0.01',
    
    'name'                  => '用户模块',
    'description'           => '',
    'core'                  => true,
    'model'                 => true,
    
    'remarks'               => [
        'author'            => 'YhCMS项目团队',
        'url'               => 'http://dev.yhcms.com.cn/',
    ],
    
];
