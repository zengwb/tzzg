<?php

namespace Member\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 用户模块用户管理控制器类：呈现用户CURD常规管理
 * 
 * @author T-01
 */
final class MemberController extends BaseController {
    
    public      $action     = [
        'index','edit','move','delete','move','state','go_up','verify'
    ];
    private    $upload     = null,
                $roleField  = [],

                $approveMould  = [],
                $approveFlow  = [],
                $applyDetail = [],
                $apply = [],
                $applyApprove = [],

                $userModel  = [],
                $roleModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Member\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();

        $this->roleField    = ['roleid', 'name', 'parentid'];

        $this->userModel    = D('MemberUnits');
        $this->roleModel    = D('MemberRole');

        $this->approveMould    = D('ApproveMould');
        $this->approveFlow    = D('ApproveFlow');
        $this->applyDetail    = D('ApplyDetail');
        $this->apply    = D('Apply');
        $this->applyApprove    = D('ApplyApprove');

        $this->upload       = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Statics/Avatar/';

        $this->upload->subName  = null;
        $this->upload->ext      = ['png', 'jpg'];
    }
    
    /**
     * 用户管理
     */
    public final function index() {

        $roleid = I('get.roleid', 0, 'intval');
        $keyWord = I('post.key') ? I('post.key') : '';//搜索关键词

        $info               = [];
        $info['roleid']     = $roleid;

        $where  = $info['roleid'] ? ['roleid' => $info['roleid']] : '';
        $where              = [];
        $where['roleid']    = $info['roleid'];
        if (!$roleid) $where = [];
        $where['username'] = ['like',"%$keyWord%"];//只搜索字段 username

        $nums = $this->userModel->where($where)->count();//分页
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);

        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyWord;

        $data   = [];
        $ress   = $this->userModel->where($where) ->limit(
            $page->firstRow.','.$page->listRows
        )->select();
        foreach ($ress as $key => $row) {
            $where  = ['roleid'  => $row['roleid']];
            $role   = $this->roleModel->where($where)->find();
            $temp   = ['0.0.0.0' => null];

            $row['rolename']    = $role['name'];
            $row['loginip']     = strtr($row['loginip'], $temp);

            $user   = in_array($row['userid'], C('SUPER_USERID'));
            $role   = in_array($row['roleid'], C('SUPER_ROLEID'));

            $temp   = $row['userid'] == session('CMS_UserID');
            $row['showmove']    = 0;

            $data[$key]         = $row; // 获取用户记录
        }

        $where              = [];
        $where['disabled']  =  0;

        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        foreach($data as $k => $v){
            $data[$k]['username2'] = str_replace($keyWord,'<font style=\'color: red\'>'.$keyWord.'</font>',$v['username']);
        }

        $this->assign('tree', $tree);//用户组下拉
        $this->assign('data', $data);//列表数据
        $this->assign('page', $page->show());//分页

        $this->display();
    }

    /**
     * 添加用户
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
            $data = I('post.data', [], 'cms_addslashes');
            $tips = '系统用户『'.$data['username'].'』已经存在！';

            $where              = [];
            $where['username']  = $data['username'];

            $user = $this->userModel->where($where)->find();
            if ($user) $this->error($tips);

            if (empty($_FILES['avater']['error']) &&
                isset($_FILES['avater'])) {
                $this->upload->saveName = $data['username'];

                $file = $this->upload->upload();
                if (!$file) $this->error('头像上传失败！');

                $data['avater']  = $this->upload->rootPath;
                $data['avater'] .= $file['avater']['savepath'];
                $data['avater'] .= $file['avater']['savename'];

                $temp = [];
                $temp['filename'] = $file['avater']['name'];
                $temp['filesize'] = $file['avater']['size'];
                $temp['fileext']  = $file['avater']['ext'];
                $temp['isimage']  = 1;
                $temp['filepath'] = $data['avater'];
                cms_attachment($temp); // @todo: 写入附件
            }
            $temp = C('OAUTH_CMSKEY');
            $data['passprot'] = $data['password'];
            $data['password'] = cms_password($data['passprot'], $temp);

            $data['city'] = implode(',',array_filter($data['city']));//>>处理省市区
            $data['unitid'] = implode(',',array_filter($data['unitid']));//>>上级事业单位
            $data['loginip'] = $_SERVER["REMOTE_ADDR"];

            $tips = '添加系统『'.$data['username'].'』用户';
            $ress = $this->userModel->add($data);

            if (!$ress) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('index'));
        } // @todo:

        $where              = [];
        $where['disabled']  = 0;

        $temp = $this->roleModel->treeList($where,'roleid,name,parentid,is_linkageid');

        $text = "<option is_linkageid='\$is_linkageid' value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        //>>>国家部委 角色特殊
        $special = 0;
        foreach($temp as $k => $v){
            if($v['name'] == '国家部委'){
                $special = $k;
            }
        }

        $this->assign('tree', $tree);
        $this->assign('special', $special);
        $this->assign('data', ['disabled' => 0]);

        $this->display('action');
    }

    /**
     * 编辑用户
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
            $info = I('post.info', []);
            $data = I('post.data', [], 'cms_addslashes');

            $where              = [];
            $where['username']  = $data['username'];

            $user = $this->userModel->where($where)->find();
            $tips = '系统用户『'.$data['username'].'』已经存在！';

            if ($info['username'] != $data['username'] &&
                $user) {
                $this->error($tips);
            }
            if ($info['username'] == $data['username']) {
                $name = $data['username'];
            } else {
                $name = $info['username'].'→'.$data['username'];
            }

            if (empty($_FILES['avater']['error']) &&
                isset($_FILES['avater'])) {
                $this->upload->saveName = $data['username'];

                $file = $this->upload->upload();
                if (!$file) $this->error('头像上传失败！');

                $data['avater']  = $this->upload->rootPath;
                $data['avater'] .= $file['avater']['savepath'];
                $data['avater'] .= $file['avater']['savename'];

                $temp = [];
                $temp['filename'] = $file['avater']['name'];
                $temp['filesize'] = $file['avater']['size'];
                $temp['fileext']  = $file['avater']['ext'];
                $temp['isimage']  = 1;
                $temp['filepath'] = $data['avater'];
                cms_attachment($temp); // @todo: 写入附件
            }
            $temp = C('OAUTH_CMSKEY');
            $data['passprot'] = $data['password'];
            $data['password'] = cms_password($data['passprot'], $temp);

            $data['city'] = implode(',',array_filter($data['city']));//>>处理省市区
            $data['unitid'] = implode(',',array_filter($data['unitid']));//>>上级事业单位

            $where              = [];
            $where['userid']    = $info['userid'];

            $tips = '修改系统『'.$name.'』用户';
            $ress = $this->userModel->where($where)->save($data);

            if (!$ress) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('index'));
        } // @todo:

        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];

        $data = $this->userModel->where($where)->find();
        $data['password']   = $data['passprot'];

        $user = in_array($data['userid'], C('SUPER_USERID'));
        $role = in_array($data['roleid'], C('SUPER_ROLEID'));

        $temp = $data['userid'] == session('CMS_UserID');
        $disabled = $user || $role || $temp;

        $where              = [];
        $where['disabled']  = 0;

        $temp = $this->roleModel->treeList($where,'roleid,name,parentid,is_linkageid');
        $text = "<option is_linkageid='\$is_linkageid' value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );

        //查询对应的省市区
        $city = [];
        if($data['city']){
            $linkage = M('linkage');
            $city = $linkage->field('linkageid,name')->where('linkageid in ('.$data['city'].')')->select();
        }

        //查询对应的事业单位
        $unitid = [];
        if($data['unitid']){
            $institution = M('institution');
            $unitid = $institution->field('unitid,name')->where('unitid in ('.$data['unitid'].')')->select();
        }

        //>>>国家部委 角色特殊
        $special = 0;
        foreach($temp as $k => $v){
            if($v['name'] == '国家部委'){
                $special = $k;
            }
        }

        $this->assign('city', $city);
        $this->assign('unitid', $unitid);
        $this->assign('special', $special);
        $this->assign('tree', $tree);
        $this->assign('data', $data);
        $this->assign('disabled', $disabled);

        $this->display('action');
    }

    /**
     * 删除用户
     */
    public final function delete() {
        $userid = I('get.userid', 0, 'intval');

        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';

        $info = $userid ? [$userid] : $info['userid'];
        if (empty($info)) $this->error('请选择用户！');

        foreach ($info as $key => $userid) {
            $where          = [];
            $where['userid']= $userid;

            $ress = $this->userModel->where($where)->find();
            if (file_exists($ress['avater'])) {
                @unlink($ress['avater']);
            }
            $data['userid'][] = $userid;
        }
        if (empty($data['userid'])) $this->error('请选择用户！');

        $userid = implode(',', $data['userid']);
        $where = ['userid' => ['IN', $userid]];

        $temp = 'userid: '.$userid;
        $ress = $this->userModel->where($where)->delete();
        if (!$ress) $this->error('删除用户失败！', $temp);
        else $this->success('删除用户成功！', $temp, U('index'));
    }

    /**
     * 移动用户
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit') || I('get.dosubmit')) {
            $roleid = I('get.roleid', 0, 'intval');

            $info = I('post.info', []);
            $data = I('post.data', [], 'cms_addslashes');
            $data['roleid'] = $roleid ?: $data['roleid'];

            if (!is_array($info['userid'])) {
                $info['userid'] = [$info['userid']];
            }

            if (empty($info['userid'])) $this->error('请选择用户！');

            foreach ($info['userid'] as $key => $userid) {
//                $where = ['userid' => $userid];
//                $ress = $this->userModel->where($where)->find();

                // 重置权限
//                $setting = unserialize($ress['setting']);
//                $setting['priv'] = '';
//                $setting['menu'] = '';
//
//                $setting = serialize($setting);
//
//                $this->userModel->where($where)->save(['setting'=>$setting]);
                $data['userid'][]   = $userid;
            }
            if (empty($data['userid'])) $this->error('请选择用户！');

            $where              = [];
            $where['userid']    = ['IN', $data['userid']];

            $temp = ['roleid' => $data['roleid']];
            $ress = $this->userModel->where($where)->save($temp);

            $where              = [];
            $where['roleid']    = $data['roleid'];

            $name = $this->roleModel->where($where)->find()['name'];
            $tips = '移动用户到『'.$name.'』用户组';

            $temp               = [];
            $temp['roleid']     = $data['roleid'];
            $temp['userid']     = implode(',', $data['userid']);

            if (IS_POST && I('post.dosubmit')) // @todo: 弹窗
                if (!$ress) $this->popup(false, $tips, $temp);
                else $this->popup(true, $tips, $temp);

            else // @todo: 普通页面
                if (!$ress) $this->error($tips.'失败！', $temp);
                else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo:

        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];

        $data = [];
        $data = $this->userModel->where($where)->find();

        $data = cms_stripslashes($data);
        $info['roleid']     = $data['roleid'];

        $where              = [];
        $where['roleid']    = $info['roleid'];

        $role = $this->roleModel->where($where)->find();

        $where              = [];
        $where['disabled']  =  0;
        $ress = $this->roleModel->treeList($where);

        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );

        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);

        $this->display();
    }

    /**
     * 更新状态
     */
    public final function state() {
        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];
        $ress = $this->userModel->where($where)->find();

        $info['roleid']     = $ress['roleid'];
        $info['username']   = $ress['username'];

        $state              = $ress['disabled'];
        $value              = $state ? 0 : 1;

        $where              = [];
        $where['userid']    = $info['userid'];

        $ress = $this->userModel->where($where)->save([
            'disabled'      => $value
        ]);

        $data = $this->userModel->where($where)->find();
        $oper = $data['disabled'] ? '启用系统' : '禁用系统';

        if (!$ress) $state = '失败！';
        else $state = '成功！';

        $tips = $oper.'『'.$data['username'].'』用户'.$state;
        cms_writelog($tips, ['userid' => $info['userid']]);

        echo json_encode($data['disabled']);
    }

    /**
     * 用户升级
     */
    public final function go_up() {
        if (IS_POST && I('post.dosubmit') || I('get.dosubmit')) {
            //>>>单个
            $roleid = I('get.roleid', 0, 'intval');

            $info = I('post.info', []);
            $data = I('post.data', [], 'cms_addslashes');
            $data['roleid'] = $roleid ?: $data['roleid'];

            if (!is_array($info['userid'])) {
                $info['userid'] = [$info['userid']];
            }
            if (empty($info['userid'])) $this->error('请选择用户！');

            $userid = $info['userid'][0];
            $where = ['userid' => $userid];
            $ress = $this->userModel
                ->join('left join v1_apply as a on a.proposer = v1_member_units.userid')
                ->join('left join v1_member_role as r on r.roleid = v1_member_units.roleid')
                ->where($where)->find();//查询用户信息

            //判定用户是否在申请中
            $apply_where['state'] = '1';
            $apply_where['proposer'] = $userid;
            $apply_state = $this->apply->where($apply_where)->find();

            if($apply_state){
                $this->error('用户『'.$ress['username'].'』正在申请中！');
            }
            if($ress['roleid'] == $data['roleid']){
                $this->error('不能与原角色一样');
            }

            //>>>匹配模板
            $where_m = [
                'mould_type' => '会员升级',
                'state' => '1',
                'order' => '1'//只用查询第一步
            ];
            $mould = $this->approveMould->join('left join v1_approve_flow as f on f.mould_id = v1_approve_mould.mould_id')
                ->where($where_m)
                ->order('date_time desc')//只使用最新的模板
                ->find();//查询“会员升级”的模板流程

            //>>>下一步流程
            $where_f['approve_id'] = $mould['approve_id'];
            $where_f['order'] = 2;
            $next_flow = $this->approveFlow->where($where_f)->find();

            $arr_roleid = [];
            userId_of_officer_range($ress['roleid'],$arr_roleid);
            //>>>校验用户权限
            if($mould['officer_id'] && $mould['officer_id'] != $userid){
                $this->error('该用户不能申请！');//专人申请
            }elseif(in_array($mould['officer_range'],$arr_roleid)){
                $this->error('该用户不能申请！');//范围申请
            }elseif(!$mould){
                $this->error('没有合适模板！');
            }

            $applyDetail_add = [
                'roleid' => $ress['roleid'],//原roleid
                'target_id' => $data['roleid']//升级目标id
            ];

            $apply_detail_id = $this->applyDetail->add($applyDetail_add);//明细添加
            if(!$apply_detail_id){$this->error('明细添加错误！');}

            $apply_add = [
                'approve_id'     => $mould['approve_id'],//流程模板id
                'date_time'     => time(),//申请时间
                'proposer'      => $userid,//申请人id
                'proposer_name' => $ress['username'],//申请人
                'operator'      => session('CMS_UserID'),//操作人员id
                'operator_name' => session('CMS_UserName'),//操作人
                'relation_tabel' => 'apply_detail',//关联表
                'apply_detail_id' => $apply_detail_id,//明细表id
                'state'         => '1',//默认状态 1：流程中
            ];

            $apply_id = $this->apply->add($apply_add);//详情添加
            if(!$apply_id){$this->error('详情添加错误！');}

            $apply_approve_add = [
                'apply_id' => $apply_id,//申请单id
                'order' => 1,//序号
                'approve_id' => $mould['approve_id'],//流程模板id
                'officer' => $userid,//申请人
                'date_time' => time(),//操作时间
                'result' => 1,//结果
                'remark' => null,//备注
                'process_name' => $mould['process_name'],//步骤名称
            ];

            $rs = $this->applyApprove->add($apply_approve_add);//流程流水添加 -- 第一步：申请

            $tips = '用户『'.$ress['username'].'』申请';

            $temp               = [];
            $temp['roleid']     = $data['roleid'];
            $temp['userid']     = implode(',', $data['userid']);

            if (IS_POST && I('post.dosubmit')) // @todo: 弹窗
                if (!$rs) $this->popup(false, $tips, $temp);
                else $this->popup(true, $tips, $temp);

            else // @todo: 普通页面
                if (!$rs) $this->error($tips.'失败！', $temp);
                else $this->success($tips.'成功！', $temp, U('upgrade/index'));
        } // @todo:

        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];

        $data = [];
        $data = $this->userModel
            ->join('left join v1_member_role as r on r.roleid =  v1_member_units.roleid')//查询用户信息，顺带查询用户角色的父级id
            ->where($where)->find();

        $data = cms_stripslashes($data);
        $info['roleid']     = $data['roleid'];

        $where              = [];
        $where['roleid']    = $info['roleid'];

        $role = $this->roleModel->where($where)->find();

        $where              = [];
        $where['disabled']  =  0;
        $where['parentid']  =  $data['parentid'];
        $ress = $this->roleModel->treeList($where);

        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp, $data['parentid'], $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );

        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);

        $this->display();
    }

    /**
     * 用户审核
     */
    public final  function verify(){
        $admin_roleid = session('CMS_RoleID');//当前管理员的roleid
        $admin_id = session('CMS_UserID');//当前管理员的urserid
        $keyWord = I('post.key') ? I('post.key') : '';//搜索关键词

        $model_arr = model_all();
        $arr_mould_type = $model_arr['name'];//模板类型
        $mould_type = $arr_mould_type['member_units'];//审批类型

        //>>>查询当前管理员参与的审批流程;
        $where = "((officer_id is null and officer_range = $admin_roleid) or (officer_id = $admin_id)) and mould_type = '".$mould_type."'";
        $data = $this->approveFlow->Distinct(true)->field('approve_id')
            ->join('left join v1_approve_mould as m on m.mould_id = v1_approve_flow.mould_id')
            ->where($where)->select();

        $approve_id = [];
        foreach($data as $v){
            $approve_id[] = $v['approve_id'];
        }
        $approve_id = implode(',',$approve_id);

        //查询审批流程中最后一步
        $where_last['approve_id'] = array('in',$approve_id);
        $all_step = $this->approveFlow->field('`order` as num,`approve_id`,officer_range,officer_id')->where($where_last)->select();
        $last_step = [];
        foreach($all_step as $v){
            if($last_step[$v['approve_id']]['order']<$v['num']){
                $last_step[$v['approve_id']] = [
                    'order' => $v['num'],
                    'state' => $v['officer_id']?($v['officer_id'] == $admin_id?1:0):($v['officer_range'] == $admin_roleid?1:0),
                ];
            }
        }

        //>>>查询单号的具体流程
        $where_apply['v1_apply.approve_id'] = array('in',$approve_id);
        $where_apply['proposer_name'] = ['like',"%$keyWord%"];//只搜索字段 proposer_name

        $nums = $this->apply->where($where_apply)->count();//分页
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);

        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyWord;

        $ress = $this->apply->field('v1_apply.*,d.*,f.mould_id,m.mould_type,m.mould_name,r1.name as role_name,r2.name as target_name')
            ->join('left join v1_apply_detail as d on d.apply_detail_id = v1_apply.apply_detail_id')
            ->join('left join v1_approve_flow as f on f.approve_id = v1_apply.approve_id and f.order=1')
            ->join('left join v1_approve_mould as m on m.mould_id = f.mould_id')
            ->join('left join v1_member_role as r1 on r1.roleid = d.roleid')
            ->join('left join v1_member_role as r2 on r2.roleid = d.target_id')
            ->order('date_time desc')
            ->where($where_apply)
            ->limit(
                $page->firstRow.','.$page->listRows
            )
            ->select();
        foreach($ress as $k => $v){
            $ress[$k]['proposer_name2'] = str_replace($keyWord,'<font style=\'color: red\'>'.$keyWord.'</font>',$v['proposer_name']);
        }

        $user_state = in_array($admin_id, C('SUPER_USERID'));//是否是超级管理员
        $role_state = in_array($admin_roleid, C('SUPER_ROLEID'));//是否是超级用户组

        $super_state = 0;
        foreach($ress as $k => $v){
            if(!$user_state && !$role_state){
                $ress[$k]['fly_state'] = $last_step[$v['approve_id']]['state'];
            }else{
                $super_state = 1;
                $ress[$k]['fly_state'] = 1;//是否能直接操作、批量操作
            }
        }

        $this->assign('data', $ress);//列表
        $this->assign('state', $super_state);//超管
        $this->assign('page', $page->show());//分页

        $this->display();
    }


    /**
     * 审批详情+提交审批
     */
    public final function more(){
        $admin_roleid = session('CMS_RoleID');//当前管理员的roleid
        $admin_id = session('CMS_UserID');//当前管理员的urserid

        if (IS_POST && I('post.dosubmit')) {
            $info = I('post.info', []);
            $result = I('post.dosubmit');
            if($result == '驳回'){
                $result = 0;
            }elseif($result == '同意'){
                $result = 1;
            }else{
                $this->error('状态错误！');
            }

            //>>>查询最大审批序号
            $max_order = $this->approveFlow->field('max(`order`) as max')->where(['approve_id'=> $info['approve_id']])->find()['max'];
            //审核序号 不能大于最大审批序号
            if($max_order<$info['order']){
                $this->error('最大审批序号错误！');
            }


            $apply_approve_add = [
                'apply_id' => $info['apply_id'],//申请单id
                'order' => $info['order'],//序号
                'approve_id' => $info['approve_id'],//流程模板id
                'officer' => $admin_id,//审批人
                'date_time' => time(),//操作时间
                'result' => $result,//结果
                'remark' => $info['remark'],//备注
                'process_name' => $info['process_name'],//步骤名称
            ];
            $ress = $this->applyApprove->add($apply_approve_add);//流程流水添加

            //驳回 直接修改申请单状态， 同意，判定是否是最后一步
            $where['apply_id'] = $info['apply_id'];//申请单id
            if($result){
                //同意 且是最后一步》》 修改状态
                if($max_order == $info['order']){

                    $re = self::state_member_role($info['apply_id']);
                    if(!$re) $this->error('升级失败！');

                    $save = [
                        'state' => 2,//状态
                        'update_time' => time(),//同意时间
                    ];
                    $ress = $this->apply->where($where)->save($save);
                }
            }else{
                //驳回
                $save = [
                    'state' => 0,//状态
                    'update_time' => time(),//同意时间
                    'remark' => $info['remark'],//备注
                ];
                $ress = $this->apply->where($where)->save($save);
            }

            $tips = '['.I('post.dosubmit').'申请] 操作';
            if (!$ress) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('verify'));
        } // @todo:

        $apply_id = I('get.apply_id', 0, 'intval');

        //查询审批详情
        $where['apply_id'] = $apply_id;
        $ress = $this->apply->field('v1_apply.*,d.*,f.mould_id,m.mould_type,m.mould_name,r1.name as role_name,r2.name as target_name')
            ->join('left join v1_apply_detail as d on d.apply_detail_id = v1_apply.apply_detail_id')
            ->join('left join v1_approve_flow as f on f.approve_id = v1_apply.approve_id and f.order=1')
            ->join('left join v1_approve_mould as m on m.mould_id = f.mould_id')
            ->join('left join v1_member_role as r1 on r1.roleid = d.roleid')
            ->join('left join v1_member_role as r2 on r2.roleid = d.target_id')
            ->order('date_time desc')
            ->where($where)->find();

        //查询审批流程
        $ress['flow'] = $this->applyApprove->field('v1_apply_approve.*,d.username as officer_name,u.username as user_name')
            ->join('left join v1_admin as d on d.userid = v1_apply_approve.officer and `order` !=1')
            ->join('left join v1_member_units as u on u.userid = v1_apply_approve.officer and `order` =1')
            ->where($where)->order('`order` asc')->select();

        $count = count($ress['flow']);
        //查询待审核步骤
        $where_flow['order'] = array('gt',$count);
        $where_flow['approve_id'] = $ress['approve_id'];
        $data_flow = $this->approveFlow->field('v1_approve_flow.*,r.name as role_name,a.username as user_name')
            ->join('left join v1_admin_role as r on r.roleid = v1_approve_flow.officer_range')
            ->join('left join v1_admin as a on a.userid = v1_approve_flow.officer_id')
            ->where($where_flow)->order('`order` asc')->select();

        foreach($data_flow as $v){
            $ress['flow'][] = $v;
        }


        $next_flow = $data_flow[0];//下一步
        $next_officer_id = $next_flow['officer_id'];//下一步专人审批
        $next_officer_range = $next_flow['officer_range'];//下一步审批角色
        $state = 0;//当前管理员是否能操作 0 不能  1 能
        if($ress['state'] == 1 && $next_officer_id && $next_officer_id == $admin_id ){
            $state = 1;//流程中、专人审核
        }elseif($ress['state'] == 1 && !$next_officer_id && $next_officer_range == $admin_roleid){
            $state = 1;//流程中、非专人审核、角色一样
        }

        $this->assign('data', $ress);
        $this->assign('next_flow', $next_flow);
        $this->assign('state', $state);

        $this->display();
    }

    /**
     * 一键同意、驳回操作；带批量
     */
    public final function all_move(){
        $admin_roleid = session('CMS_RoleID');//当前管理员的roleid
        $admin_id = session('CMS_UserID');//当前管理员的urserid
        $apply_id = I('get.apply_id', 0, 'intval');
        $fruit = I('get.fruit', 0, 'intval');

        $info = I('post.info', [], 'cms_addslashes');
        $info = $apply_id ? [$apply_id] : $info['apply_id'];
        if (empty($info)) $this->error('请选择对象！');
        $apply_ids = implode(',',$info);

        if($fruit){
            $re = self::state_member_role($apply_ids);
            if(!$re) $this->error('升级失败！');
        }

        //查询申请单 对应的 流程模板id   ###暂时还是不记录流水了，步骤断层了
//        $where_apply['apply_id'] = array('in',$apply_ids);
//        $data = $this->applyApprove->field('apply_id,approve_id,order')->where($where_apply)->select();

//        $apply_approve_add = [];
//        foreach($data as $v){
//            $apply_approve_add = [
//                'apply_id' => $v['apply_id'],//申请单id
//                'order' => $info['order'],//序号
//                'approve_id' => $v['approve_id'],//流程模板id
//                'officer' => $admin_id,//审批人
//                'date_time' => time(),//操作时间
//                'result' => $fruit?1:0,//结果
//                'remark' => "一键审批",//备注
//            ];
//        }
//        $ress = $this->applyApprove->addAll($apply_approve_add);//流程流水添加
//        if(!$ress) $this->error('记录审批流水出错！');

        $where['apply_id'] = array('in',$apply_ids);
        $where['state'] = '1';
        $ress = $this->apply->where($where)->save(['state'=>$fruit,'remark'=> "一键审批"]);

        $temp = ' ';
        if (!$ress) $this->error('操作失败！', $temp);
        else $this->success('操作成功！', $temp, U('verify'));
    }


    /** 审批完成后 会员等级更新
     * @param $apply_ids
     * @return bool
     */
    private function state_member_role($apply_ids){
        $where['apply_id'] = array('in',$apply_ids);
        $all_user = $this->apply->field('d.target_id as roleid,v1_apply.proposer as userid')
            ->join('left join v1_apply_detail as d on d.apply_detail_id = v1_apply.apply_detail_id')
            ->where($where)->select();
        $memberUnits = M('MemberUnits');
        $memberUnits->startTrans();
        $ress = true;
        foreach ($all_user as $v) {
            $rs = $memberUnits->save($v);
            if(!$rs){
                $ress = false;
            }
        }

        if($ress){
            $memberUnits->commit();
        }else{
            $memberUnits->rollback();
        }
        return $ress;
    }


    /** 查询 省/市/区
     * @param $parentid
     * @return string
     */
    public function choose_city($parentid){
        $where = [];
        $where['display'] = 1;
        $where['parentid'] = $parentid;
        $linkage = M('linkage');
        $city = $linkage->field('linkageid,name,parentid')->where($where)->select();
        echo  json_encode($city);
    }

    /** 查询 事业单位
     * @param $parentid
     * @return string
     */
    public function choose_unitid($parentid=0,$linkageid=0){
        $where = [];
        $where['parentid'] = $parentid;
        $where['linkageid'] = $linkageid;
        $where = array_filter($where);
        if(count($where) != 1){
            $ress = [
                'code' => false,
                'message' => '参数错误',
            ];
        }
        $where['display'] = 1;

        $institution = M('institution');
        $unitid = $institution->field('unitid,name')->where($where)->select();
        if($unitid){
            $ress = [
                'code' => true,
                'data' => $unitid,
            ];
        }else{
            $ress = [
                'code' => false,
                'message' => '数据为空',
            ];
        }
        echo  json_encode($ress);
    }
    
}
