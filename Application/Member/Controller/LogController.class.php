<?php

namespace Member\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 管理模块日志管理控制器类：呈现日志查看或删除操作
 * 
 * @author T-01
 */
final class LogController extends BaseController {
    
    public      $action     = [
        'index', 'login', 'error2', 'detail', 'delete',
    ];
    
    private     $oper2Model = [],
                $loginModel = [],
                $errorModel = [];
    
    /**
     * {@inheritDoc}
     * @see \Admin\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        
        $this->oper2Model   = D('Common/MemberOperLog');
        $this->loginModel   = D('Common/LoginLog');
        $this->errorModel   = D('Common/ErrorLog');
    }
    
    /**
     * 用户操作日志列表
     */
    public final function index() {
        $where              = [];
        $where['username']  = $this->username;

        if (in_array($this->roleid, C('SUPER_ROLEID')))
        unset($where['username']);

        $nums = $this->oper2Model->submeter()->where($where)->count();

        $rows = 100; // C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        
        $data = $this->oper2Model->submeter()->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order(
            '`opertime` DESC'
        )->select();
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 登录日志
     */
    public final function login() {
        $where              = [];
        $where['username']  = $this->username;
        if (in_array($this->roleid, C('SUPER_ROLEID')))
        unset($where['username']);
        
        $nums = $this->loginModel->submeter()->where($where)->count();
        $rows = 50; // C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);
        
        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        
        $data = $this->loginModel->submeter()->where($where)->limit(
            $page->firstRow.','.$page->listRows
        )->order(
            '`logintime` DESC'
        )->select();
        
        $this->assign('data', $data);
        $this->assign('page', $page->show());
        
        $this->display();
    }
    
    /**
     * 异常日志
     */
    public final function error2() {}
    
    /**
     * 查看日志
     */
    public final function detail() {
        $type2 = I('get.type' , 'index');
        $logid = I('get.logid', 0, 'intval');
        switch ($type2) {
            case 'index':
                $model = $this->oper2Model;
                $show2 = 'Oper2Detail';
                break;
            case 'login':
                $model = $this->loginModel;
                $show2 = 'LoginDetail';
                break;
            default :
                $model = $this->errorModel;
                $show2 = 'ErrorDetail';
                break;
        }
        $where              = [];
        $where['logid']     = $logid;
        $data = $model->submeter()->where($where)->find();
        
        $this->assign('data', $data);
        $this->display($show2);
    }
    
    /**
     * 删除日志
     */
    public final function delete() {
        $logid = I('get.logid', 0, 'intval');
        
        $info = I('post.info', []);
        $type = I('get.type' , 'index');
        $type = $info['type'] ? : $type;
        
        $info = $logid ? [$logid] : $info['logid'];
        $data = [];
        if (empty($info)) $this->error('请选择日志！');

        switch ($type) {
            case 'index':
                $model = $this->oper2Model;
                break;
            case 'login':
                $model = $this->loginModel;
                break;
            default :
                $model = $this->errorModel;
                break;
        }
        foreach ($info as $key => $logid) {
            $data['logid'][]= $logid;
            
            $where          = [];
            $where['logid'] = $logid;

            $model->submeter($where)->where($where)->delete();
        }
        $temp = [];
        $temp['type']       = $type;
        $temp['logid']      = implode(',', $data['logid']);
        
        $this->success('删除日志成功！', $temp);
        
//         foreach ($info as $key => $logid) $data['logid'][] = $logid;
//         $temp = [];
//         $temp['type']       = $type2;
//         $temp['logid']      = implode(',', $data['logid']);
        
//         $where              = [];
//         $where['logid']     = ['IN', $data['logid']];
        
//         $model->create($where);
//         $ress = $model->submeter($where)->where($where)->delete();
//         // @todo: 删除异常，待解决（无法删除）
//         if (!$ress) $this->error('删除日志失败！', $temp);
//         else $this->success('删除日志成功！', $temp, U('index'));
    }
    
}
