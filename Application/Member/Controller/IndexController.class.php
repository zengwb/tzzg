<?php

namespace Member\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 用户模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    /**
     * {@inheritDoc}
     * @see \Member\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    /**
     * 用户中心
     */
    public final function index() {
        echo __METHOD__;
    }
    
}
