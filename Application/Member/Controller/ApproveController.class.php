<?php

namespace Member\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 审批管理控制器类：呈现审批模板CURD常规管理
 * 
 * @author T-01
 */
final class ApproveController extends BaseController {
    
    public      $action     = [
        'index'
    ];
    private    $upload     = null,
                $roleField  = [],

                $approveMould  = [],
                $userRoleModel  = [],
                $adminRoleModel  = [],
                $adminModel  = [],
                $memberModel  = [],
                $approveFlow  = [];

    /**
     * {@inheritDoc}
     * @see \Member\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();

        $this->roleField    = ['roleid', 'name', 'parentid'];

        $this->approveMould    = D('ApproveMould');
        $this->userRoleModel    = D('MemberRole');
        $this->adminRoleModel    = D('Admin/AdminRole');
        $this->adminModel    = D('Admin');
        $this->memberModel    = D('MemberUnits');

        $this->approveFlow    = D('ApproveFlow');

        $this->upload       = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Statics/Avatar/';

        $this->upload->subName  = null;
        $this->upload->ext      = ['png', 'jpg'];
    }
    
    /**
     * 模板管理
     */
    public final function index() {
        $keyWord = I('post.key') ? I('post.key') : '';//搜索关键词
        $where['mould_name'] = ['like',"%$keyWord%"];//只搜索字段 mould_name

        $ress   = $this->approveMould->where($where)->select();

        foreach($ress as $k => $v){
            $ress[$k]['mould_name2'] = str_replace($keyWord,'<font style=\'color: red\'>'.$keyWord.'</font>',$v['mould_name']);
        }

        $this->assign('data', $ress);

        $this->display();
    }

    /**
     * 添加模板
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
            $data = I('POST.data');
            $mould_type = $data['mould_type'];//模板类型
            $mould_name = $data['mould_name'];//模板名称
            $state      = $data['state'];//模板状态

            //添加模板
            $add = [
                'mould_name'    => $mould_name,
                'mould_type'    => $mould_type,
                'state'         => $state,
                'date_time'      => time(),
            ];

            $rs = $this->approveMould->add($add);
            $re = false;//默认失败
            if($rs){
                $max_order      = count($data['process_name']);//审批步骤数量
                $process_name   = $data['process_name'];//步骤名称
                $officer_range  = $data['officer_range'];//审核角色范围
                $officer        = $data['officer'];//具体审核人员
                $approve_id     = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);;//流程模板id
                //添加成功后，添加模板流程步骤
                $add_flow = [];
                for($i = 0;$i<$max_order;$i++){
                    $add_flow[] = [
                        'approve_id'    => $approve_id,
                        'mould_id'      => $rs,
                        'mould_name'    => $mould_name,//模板名称
                        'order'          => $i+1,//序号
                        'process_name'  => $process_name[$i],//步骤名称
                        'officer_type'  => $i?1:2,//1 管理员； 2 会员用户 >>>第一步都是‘会员用户’申请，其他都是‘管理员’审核
                        'officer_range' => $officer_range[$i],//范围
                        'officer_id'    => $officer[$i],//审批人id/申请人id
                    ];
                }
                $rs = $this->approveFlow->addAll($add_flow);
                if($rs){
                    $re = true;
                }
            }

            $tips = '添加审批模板『'.$mould_name.'』';
            if (!$re) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('index'));
        } // @todo:

        $where              = [];
        $where['disabled']  = 0;

        //获取用户 角色
        $temp = $this->userRoleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree['user'] = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        //获取管理 角色
        $temp = $this->adminRoleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree['admin'] = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        $model_arr = model_all();
        $arr_mould_type = $model_arr['name'];//模板类型

        $this->assign('tree', $tree);
        $this->assign('arr_mould_type', $arr_mould_type);
        $this->assign('data', ['state' => 1]);

        $this->display('action');
    }

    /** 获取用户列表
     * @param $roleid
     * @param string $keyword
     * @return string
     */
    public function userList($roleid,$keyword=''){
        if($roleid){
            $where['roleid']    = $roleid;
            $where['disabled']  = 0;
            $ress = $this->memberModel->where($where)->field('userid,username,realname')->select();
            if($ress){
                $data['code']    = true;
                $data['data']    = $ress;
            }else{
                $data['code']    = false;
                $data['message']    = '没有数据!';
            }

        }else{
            $data['code']    = false;
            $data['message'] = '参数错误!';
        }
        echo json_encode($data);
    }

    /** 获取管理员列表
     * @param $roleid
     * @param string $keyword
     * @return string
     */
    public function adminList($roleid,$keyword=''){
        if($roleid){
            $where['roleid']    = $roleid;
            $where['disabled']  = 0;
            $ress = $this->adminModel->where($where)->field('userid,username,realname')->select();
            if($ress){
                $data['code']    = true;
                $data['data']    = $ress;
            }else{
                $data['code']    = false;
                $data['message']    = '没有数据!';
            }

        }else{
            $data['code']    = false;
            $data['message'] = '参数错误!';
        }
        echo json_encode($data);
    }




    /**
     * 编辑用户
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
            $data = I('POST.data');
            $info = I('POST.info');
            $mould_id   = $info['mould_id'];//模板Id
            $approve_id   = $info['approve_id'];//模板流程Id
            //$mould_type = $data['mould_type'];//模板类型
            $mould_name = $data['mould_name'];//模板名称
            $state      = $data['state'];//模板状态

            $where = [];
            $where['mould_id'] = $mould_id;

            //修改模板
            $save = [
                'mould_name'    => $mould_name,
                'state'         => $state,
                'date_time'      => time(),
            ];

            $rs = $this->approveMould->where($where)->save($save);
            $re = false;//默认失败
            if($rs){
                $max_order  = count($data['process_name']);//审批步骤数量
                $process_name   = $data['process_name'];//步骤名称
                $officer_range  = $data['officer_range'];//审核角色范围
                $officer        = $data['officer'];//具体审核人员
                //添加成功后，添加模板流程步骤
                $add_flow = [];
                for($i = 0;$i<$max_order;$i++){
                    $add_flow[] = [
                        'approve_id'    => $approve_id,
                        'mould_id'      => $mould_id,
                        'mould_name'    => $mould_name,//模板名称
                        'order'          => $i+1,//序号
                        'process_name'  => $process_name[$i],//步骤名称
                        'officer_type'  => $i?1:2,//1 管理员； 2 会员用户 >>>第一步都是‘会员用户’申请，其他都是‘管理员’审核
                        'officer_range' => $officer_range[$i],//范围
                        'officer_id'    => $officer[$i],//审批人id/申请人id
                    ];
                }
                $de = $this->approveFlow->where($where)->delete();
                if($de) {
                    $rs = $this->approveFlow->addAll($add_flow);
                    if($rs){
                        $re = true;
                    }
                }
            }

            $tips = '修改审批模板『'.$mould_name.'』';
            if (!$re) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('index'));
        } // @todo:

        //获取原有数据
        $mould_id = I('get.mould_id', 0, 'intval');
        $where['mould_id']    = $mould_id;
        $data = $this->approveMould->where($where)->find();//模板

        $flow = $this->approveFlow->field('v1_approve_flow.*,m.username,m.realname')
            ->join('left join v1_member_units as m on m.userid =  v1_approve_flow.officer_id')
            ->where('mould_id = '.$mould_id.' and `order` = 1')->select();//模板-流程1

        $flow_more = $this->approveFlow->field('v1_approve_flow.*,m.username,m.realname,r.name')
            ->join('left join v1_admin as m on m.userid =  v1_approve_flow.officer_id')
            ->join('left join v1_admin_role as r on r.roleid =  v1_approve_flow.officer_range')
            ->where('mould_id = '.$mould_id.' and `order` > 1')->order('`order` asc')->select();//模板-流程1+


        $where              = [];
        $where['disabled']  = 0;

        //获取用户 角色
        $temp = $this->userRoleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree['user'] = cms_tree_menu(
            $temp,  0,
            $text, '', [$flow[0]['officer_range']], ['field' => $this->roleField]
        );

        //获取管理 角色
        $temp = $this->adminRoleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree['admin'] = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        $model_arr = model_all();
        $arr_mould_type = $model_arr['name'];//模板类型
        $this->assign('arr_mould_type', $arr_mould_type);

        $this->assign('tree', $tree);
        $this->assign('data', $data);
        $this->assign('flow', $flow);
        $this->assign('flow_more', $flow_more);
        $this->assign('disabled', 1);

        $this->display('action');
    }

    /**
     * 删除模板
     */
    public final function delete() {
        $mould_id = I('get.mould_id', 0, 'intval');

        $info = I('post.info', [], 'cms_addslashes');
        $data = [];

        $info = $mould_id ? [$mould_id] : $info['mould_id'];
        if (empty($info)) $this->error('请选择模板！');

        foreach ($info as $key => $mould_id) {
            $where          = [];
            $where['mould_id']= $mould_id;

            $ress = $this->approveMould->where($where)->find();
            if (file_exists($ress['avater'])) {
                @unlink($ress['avater']);
            }
            $data['mould_id'][] = $mould_id;
        }
        if (empty($data['mould_id'])) $this->error('请选择模板！');

        $mould_id = implode(',', $data['mould_id']);
        $where = ['mould_id' => ['IN', $mould_id]];

        //查询是否有流程中的申请
        $where_apply = $where;
        $where_apply['state'] = '1';
        $re = $this->approveFlow->join('left join v1_apply as a on a.approve_id = v1_approve_flow.approve_id')->where($where_apply)->count();
        if($re){$this->error('删除模板失败 : 还有申请未完成审批流程！');}

        $temp = 'mould_id: '.$mould_id;
        $ress = $this->approveMould->where($where)->delete();
        $ress = $this->approveFlow->where($where)->delete();
        if (!$ress) $this->error('删除模板失败！', $temp);
        else $this->success('删除模板成功！', $temp, U('index'));
    }


    /**
     * 更新模板状态
     */
    public final function state() {
        $mould_id = I('get.mould_id', 0, 'intval');

        $where              = [];
        $where['mould_id']    = $mould_id;
        $ress = $this->approveMould->where($where)->find();

        $mould_name     = $ress['mould_name'];

        $state              = $ress['state'];
        $value              = $state ? 0 : 1;

        $ress = $this->approveMould->where($where)->save([
            'state'      => $value
        ]);

        $oper = $state ? '禁用模板' : '启用模板';

        if (!$ress) $message = '失败！';
        else $message = '成功！';

        $tips = $oper.'『'.$mould_name.'』用户'.$message;
        cms_writelog($tips, ['mould_id' => $mould_id]);

        echo json_encode($state);
    }


    
}
