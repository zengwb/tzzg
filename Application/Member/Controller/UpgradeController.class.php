<?php

namespace Member\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 用户模块用户管理控制器类：呈现用户CURD常规管理
 * 
 * @author T-01
 */
final class UpgradeController extends BaseController {
    
    public      $action     = [
        'index','edit','move','delete','move','state','go_up'
    ];
    private    $upload     = null,
                $roleField  = [],

                $userModel  = [],
                $roleModel  = [];
    
    /**
     * {@inheritDoc}
     * @see \Member\Controller\BaseController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();

        $this->roleField    = ['roleid', 'name', 'parentid'];

        $this->userModel    = D('MemberUnits');
        $this->roleModel    = D('MemberRole');

        $this->upload       = (new \Think\Upload());
        $this->upload->rootPath = C('CMS_ATTACH_PATH');
        $this->upload->rootPath.= 'Statics/Avatar/';

        $this->upload->subName  = null;
        $this->upload->ext      = ['png', 'jpg'];
    }
    
    /**
     * 用户管理
     */
    public final function index() {
        $roleid = I('get.roleid', 0, 'intval');
        $keyWord = I('post.key') ? I('post.key') : '';//搜索关键词

        $info               = [];
        $info['roleid']     = $roleid;

        $where  = $info['roleid'] ? ['roleid' => $info['roleid']] : '';
        $where              = [];
        $where['roleid']    = $info['roleid'];
        if (!$roleid) $where = [];
        $where['username'] = ['like',"%$keyWord%"];//只搜索字段 username

        $nums = $this->userModel->where($where)->count();//分页
        $rows = C('PAGES_NUMBER');
        $page = cms_page($nums, $rows);

        $page->setConfig('prev', '上一页');
        $page->setConfig('next', '下一页');
        $page->parameter['key'] = $keyWord;

        $data   = [];
        $ress   = $this->userModel->where($where)
            ->limit(
                $page->firstRow.','.$page->listRows
            )
            ->select();
        foreach ($ress as $key => $row) {
            $where  = ['roleid'  => $row['roleid']];
            $role   = $this->roleModel->where($where)->find();
            $temp   = ['0.0.0.0' => null];

            $row['rolename']    = $role['name'];
            $row['loginip']     = strtr($row['loginip'], $temp);

            $user   = in_array($row['userid'], C('SUPER_USERID'));
            $role   = in_array($row['roleid'], C('SUPER_ROLEID'));

            $temp   = $row['userid'] == session('CMS_UserID');
            $row['showmove']    = 0;

            $data[$key]         = $row; // 获取用户记录
        }

        $where              = [];
        $where['disabled']  =  0;

        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );

        foreach($data as $k => $v){
            $data[$k]['username2'] = str_replace($keyWord,'<font style=\'color: red\'>'.$keyWord.'</font>',$v['username']);
        }

        $this->assign('tree', $tree);
        $this->assign('data', $data);
        $this->assign('page', $page->show());//分页

        $this->display();
    }

    /**
     * 添加用户
     */
    public final function add() {
        if (IS_POST && I('post.dosubmit')) {
            $data = I('post.data', [], 'cms_addslashes');
            $tips = '系统用户『'.$data['username'].'』已经存在！';

            $where              = [];
            $where['username']  = $data['username'];

            $user = $this->userModel->where($where)->find();
            if ($user) $this->error($tips);

            if (empty($_FILES['avater']['error']) &&
                isset($_FILES['avater'])) {
                $this->upload->saveName = $data['username'];

                $file = $this->upload->upload();
                if (!$file) $this->error('头像上传失败！');

                $data['avater']  = $this->upload->rootPath;
                $data['avater'] .= $file['avater']['savepath'];
                $data['avater'] .= $file['avater']['savename'];

                $temp = [];
                $temp['filename'] = $file['avater']['name'];
                $temp['filesize'] = $file['avater']['size'];
                $temp['fileext']  = $file['avater']['ext'];
                $temp['isimage']  = 1;
                $temp['filepath'] = $data['avater'];
                cms_attachment($temp); // @todo: 写入附件
            }
            $temp = C('OAUTH_CMSKEY');
            $data['passprot'] = $data['password'];
            $data['password'] = cms_password($data['passprot'], $temp);

            $tips = '添加系统『'.$data['username'].'』用户';
            $ress = $this->userModel->add($data);

            if (!$ress) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('index'));
        } // @todo:

        $where              = [];
        $where['disabled']  = 0;

        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree = cms_tree_menu(
            $temp,  0,
            $text, '', [], ['field' => $this->roleField]
        );
        $this->assign('tree', $tree);
        $this->assign('data', ['disabled' => 0]);

        $this->display('action');
    }

    /**
     * 编辑用户
     */
    public final function edit() {
        if (IS_POST && I('post.dosubmit')) {
            $info = I('post.info', []);
            $data = I('post.data', [], 'cms_addslashes');

            $where              = [];
            $where['username']  = $data['username'];

            $user = $this->userModel->where($where)->find();
            $tips = '系统用户『'.$data['username'].'』已经存在！';

            if ($info['username'] != $data['username'] &&
                $user) {
                $this->error($tips);
            }
            if ($info['username'] == $data['username']) {
                $name = $data['username'];
            } else {
                $name = $info['username'].'→'.$data['username'];
            }

            if (empty($_FILES['avater']['error']) &&
                isset($_FILES['avater'])) {
                $this->upload->saveName = $data['username'];

                $file = $this->upload->upload();
                if (!$file) $this->error('头像上传失败！');

                $data['avater']  = $this->upload->rootPath;
                $data['avater'] .= $file['avater']['savepath'];
                $data['avater'] .= $file['avater']['savename'];

                $temp = [];
                $temp['filename'] = $file['avater']['name'];
                $temp['filesize'] = $file['avater']['size'];
                $temp['fileext']  = $file['avater']['ext'];
                $temp['isimage']  = 1;
                $temp['filepath'] = $data['avater'];
                cms_attachment($temp); // @todo: 写入附件
            }
            $temp = C('OAUTH_CMSKEY');
            $data['passprot'] = $data['password'];
            $data['password'] = cms_password($data['passprot'], $temp);

            $where              = [];
            $where['userid']    = $info['userid'];

            $tips = '修改系统『'.$name.'』用户';
            $ress = $this->userModel->where($where)->save($data);

            if (!$ress) $this->error($tips.'失败！');
            else $this->success($tips.'成功！', '', U('index'));
        } // @todo:

        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];

        $data = $this->userModel->where($where)->find();
        $data['password']   = $data['passprot'];

        $user = in_array($data['userid'], C('SUPER_USERID'));
        $role = in_array($data['roleid'], C('SUPER_ROLEID'));

        $temp = $data['userid'] == session('CMS_UserID');
        $disabled = $user || $role || $temp;

        $where              = [];
        $where['disabled']  = 0;

        $temp = $this->roleModel->treeList($where);
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";

        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );
        $this->assign('tree', $tree);
        $this->assign('data', $data);
        $this->assign('disabled', $disabled);

        $this->display('action');
    }

    /**
     * 删除用户
     */
    public final function delete() {
        $userid = I('get.userid', 0, 'intval');

        $info = I('post.info', [], 'cms_addslashes');
        $data = [];
        $temp = '';

        $info = $userid ? [$userid] : $info['userid'];
        if (empty($info)) $this->error('请选择用户！');

        foreach ($info as $key => $userid) {
            $where          = [];
            $where['userid']= $userid;

            $ress = $this->userModel->where($where)->find();
            if (file_exists($ress['avater'])) {
                @unlink($ress['avater']);
            }
            $data['userid'][] = $userid;
        }
        if (empty($data['userid'])) $this->error('请选择用户！');

        $userid = implode(',', $data['userid']);
        $where = ['userid' => ['IN', $userid]];

        $temp = 'userid: '.$userid;
        $ress = $this->userModel->where($where)->delete();
        if (!$ress) $this->error('删除用户失败！', $temp);
        else $this->success('删除用户成功！', $temp, U('index'));
    }

    /**
     * 移动用户
     */
    public final function move() {
        if (IS_POST && I('post.dosubmit') || I('get.dosubmit')) {
            $roleid = I('get.roleid', 0, 'intval');

            $info = I('post.info', []);
            $data = I('post.data', [], 'cms_addslashes');
            $data['roleid'] = $roleid ?: $data['roleid'];

            if (!is_array($info['userid'])) {
                $info['userid'] = [$info['userid']];
            }

            if (empty($info['userid'])) $this->error('请选择用户！');

            foreach ($info['userid'] as $key => $userid) {
                $where = ['userid' => $userid];
                $ress = $this->userModel->where($where)->find();

                //$user = in_array($ress['userid'], C('SUPER_USERID'));//不用判定是否是 超级管理员
                //$role = in_array($ress['roleid'], C('SUPER_ROLEID'));//不用判定是否是 超级管理角色

                //$temp = $ress['userid'] == session('CMS_UserID');//不用判定用户是否是登陆管理员

                //if ( $temp || empty($userid)) continue;

                // 重置权限
                $setting = unserialize($ress['setting']);
                $setting['priv'] = '';
                $setting['menu'] = '';

                $setting = serialize($setting);

                $this->userModel->where($where)->save(['setting'=>$setting]);
                $data['userid'][]   = $userid;
            }
            if (empty($data['userid'])) $this->error('请选择用户！');

            $where              = [];
            $where['userid']    = ['IN', $data['userid']];

            $temp = ['roleid' => $data['roleid']];
            $ress = $this->userModel->where($where)->save($temp);

            $where              = [];
            $where['roleid']    = $data['roleid'];

            $name = $this->roleModel->where($where)->find()['name'];
            $tips = '移动用户到『'.$name.'』用户组';

            $temp               = [];
            $temp['roleid']     = $data['roleid'];
            $temp['userid']     = implode(',', $data['userid']);

            if (IS_POST && I('post.dosubmit')) // @todo: 弹窗
                if (!$ress) $this->popup(false, $tips, $temp);
                else $this->popup(true, $tips, $temp);

            else // @todo: 普通页面
                if (!$ress) $this->error($tips.'失败！', $temp);
                else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo:

        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];

        $data = [];
        $data = $this->userModel->where($where)->find();

        $data = cms_stripslashes($data);
        $info['roleid']     = $data['roleid'];

        $where              = [];
        $where['roleid']    = $info['roleid'];

        $role = $this->roleModel->where($where)->find();

        $where              = [];
        $where['disabled']  =  0;
        $ress = $this->roleModel->treeList($where);

        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp, 0, $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );

        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);

        $this->display();
    }

    /**
     * 更新状态
     */
    public final function state() {
        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];
        $ress = $this->userModel->where($where)->find();

        $info['roleid']     = $ress['roleid'];
        $info['username']   = $ress['username'];

        $state              = $ress['disabled'];
        $value              = $state ? 0 : 1;

        $where              = [];
        $where['userid']    = $info['userid'];

        $ress = $this->userModel->where($where)->save([
            'disabled'      => $value
        ]);

        $data = $this->userModel->where($where)->find();
        $oper = $data['disabled'] ? '启用系统' : '禁用系统';

        if (!$ress) $state = '失败！';
        else $state = '成功！';

        $tips = $oper.'『'.$data['username'].'』用户'.$state;
        cms_writelog($tips, ['userid' => $info['userid']]);

        echo json_encode($data['disabled']);
    }

    /**
     * 用户升级
     */
    public final function go_up() {
        if (IS_POST && I('post.dosubmit') || I('get.dosubmit')) {
            $roleid = I('get.roleid', 0, 'intval');

            $info = I('post.info', []);
            $data = I('post.data', [], 'cms_addslashes');
            $data['roleid'] = $roleid ?: $data['roleid'];

            if (!is_array($info['userid'])) {
                $info['userid'] = [$info['userid']];
            }

            if (empty($info['userid'])) $this->error('请选择用户！');

            foreach ($info['userid'] as $key => $userid) {
                $where = ['userid' => $userid];
                $ress = $this->userModel->where($where)->find();

                $setting = unserialize($ress['setting']);
                $setting['priv'] = '';
                $setting['menu'] = '';

                $setting = serialize($setting);

                $this->userModel->where($where)->save(['setting'=>$setting]);
                $data['userid'][]   = $userid;
            }
            if (empty($data['userid'])) $this->error('请选择用户！');

            $where              = [];
            $where['userid']    = ['IN', $data['userid']];

            $temp = ['roleid' => $data['roleid']];
            $ress = $this->userModel->where($where)->save($temp);

            $where              = [];
            $where['roleid']    = $data['roleid'];

            $name = $this->roleModel->where($where)->find()['name'];
            $tips = '提升用户为『'.$name.'』';

            $temp               = [];
            $temp['roleid']     = $data['roleid'];
            $temp['userid']     = implode(',', $data['userid']);

            if (IS_POST && I('post.dosubmit')) // @todo: 弹窗
                if (!$ress) $this->popup(false, $tips, $temp);
                else $this->popup(true, $tips, $temp);

            else // @todo: 普通页面
                if (!$ress) $this->error($tips.'失败！', $temp);
                else $this->success($tips.'成功！', $temp, U('index'));
        } // @todo:

        $userid = I('get.userid', 0, 'intval');

        $info               = [];
        $info['userid']     = $userid;

        $where              = [];
        $where['userid']    = $info['userid'];

        $data = [];
        $data = $this->userModel
            ->join('left join v1_member_role as r on r.roleid =  v1_member_units.roleid')//查询用户信息，顺带查询用户角色的父级id
            ->where($where)->find();

        $data = cms_stripslashes($data);
        $info['roleid']     = $data['roleid'];

        $where              = [];
        $where['roleid']    = $info['roleid'];

        $role = $this->roleModel->where($where)->find();

        $where              = [];
        $where['disabled']  =  0;
        $where['parentid']  =  $data['parentid'];
        $ress = $this->roleModel->treeList($where);

        $temp = [];
        foreach ($ress as $index => $row) {
            $temp[$row['roleid']] = $row;
        }
        $text = "<option value='\$roleid' \$selected>
            \$spacer\$name</option>";
        $tree = cms_tree_menu(
            $temp, $data['parentid'], $text, '',
            [$data['roleid']], ['field' => $this->roleField]
        );

        $this->assign('info', $info);
        $this->assign('data', $data);
        $this->assign('tree', $tree);

        $this->display();
    }
    
}
