<?php

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');


/** 校验用户(包含本身)所有父级roleid
 * @param $roleid
 * @param $rs
 * @return array
 */
function userId_of_officer_range($roleid,&$rs){
    $rs[] = $roleid;
    $member_role = M('MemberRole');
    $where['disabled'] = 0;
    $where['roleid'] = $roleid;

    $ress = $member_role->where($where)->field('parentid')->find();//查询管理员角色的父级id

    if($ress['parentid']){
        admin_more_role($ress['parentid'],$rs);
    }else{
        return $rs;
    }
}

/** 查询管理员(包含本身)所有父级roleid
 * @param $roleid
 * @param $rs
 * @return array
 */
function admin_more_role($roleid,&$rs){
    $rs[] = $roleid;
    $admin_role = M('AdminRole');
    $where['disabled'] = 0;
    $where['roleid'] = $roleid;

    $ress = $admin_role->where($where)->field('parentid')->find();//查询管理员角色的父级id

    if($ress['parentid']){
        admin_more_role($ress['parentid'],$rs);
    }else{
        return $rs;
    }
}