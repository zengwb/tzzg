<?php

namespace Member\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 用户模块角色数据模型类：配置或实例化数据模型
 * 
 * @author T-01
 */
final class MemberRoleModel extends BaseModel {
    
    /**
     * 返回创建角色树形结构的数据（数组）
     * 
     * @param array     $where      必填。条件
     * @param string    $field      选填。字段
     * @param string    $order      选填。排序
     * 
     * @return array
     */
    public final function treeList($where, $field = '', $order = '') {
        $field = $field ?: 'roleid,name,parentid';
        $order = $order ? : '`listorder` ASC, `roleid` ASC';
        
        $data = [];
        $ress = $this->where($where)->field($field)->order(
            $order
        )->select();
        foreach ($ress as $key => $row) {
            $data[$row['roleid']] = $row;
        }
        return $data;
    }
    
    /**
     * 获取所有父级角色ID
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return string
     */
    public final function parentid($roleid) {
        $parentids  = '';
        $where      = ['roleid' => $roleid];
        
        $parentid   = $this->where($where)->find()['parentid'];
        $parentids  = $parentid;
        
        if ($parentid != 0) {
            $parentids.= ','.self::parentid($parentid);
        }
        return $parentids;
    }
    
    /**
     * 获取所有子角色ID（含当前角色ID，ID间以逗号分隔）
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return unknown|string|unknown
     */
    public final function childid($roleid) {
        $ids .= $roleid;
        $ress = $this->where(['parentid' => $roleid])->select();
        if (!$ress) return $ids;
        
        foreach ($ress as $key => $row) {
            $ids .= ','.$this->childid($row['roleid']);
        }
        return $ids;
    }
    
}
