<?php

namespace Member\Model;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 用户模块权限数据模型类：配置或实例化数据模型
 * 
 *
 */
final class MemberRolePrivModel extends BaseModel {
    
    /**
     * 获取菜单ID
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function menuid($roleid) {
        $data = [];
        $ress = $this->where(['roelid' => $roleid])->select();
        
        foreach ($ress as $key => $row) {
            $data[] = $row['menuid'];
        }
        return $data;
    }
    
    /**
     * 获取权限（菜单ID）
     * 
     * @param integer   $roleid     必填。角色ID
     * 
     * @return array
     */
    public final function privs($roleid) {
        $menu = D('Admin/Menu');
        $role = D('Member/MemberRole');
        
        $data = [];
        $ress = $this->where(['roleid' => $roleid])->select();

        foreach ($ress as $key => $row) {
            $data[$key] = $row['menuid'];
        }

        if ($data) return $data;
        
        $ress = $role->where(['roleid' => $roleid])->find();
        if ($ress['parentid']) {
            return $this->privs($ress['parentid']);
        }
    }
    
}
