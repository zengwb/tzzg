<?php

namespace Plugin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 插件模块主控制器类
 * 
 * @author T-01
 */
final class IndexController extends PageController {
    
    /**
     * {@inheritDoc}
     * @see \Plugin\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
    }
    
    public final function index() {
        phpinfo();
    }
    
}
