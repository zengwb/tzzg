<?php

namespace Plugin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 插件模块数据统计控制器类
 * 
 * @author T-01
 */
final class CountController extends PageController {
    
    private     $category   = [],
                $apps       = [],
                $hits       = [],
                
                $advert     = [];
    
    /**
     * {@inheritDoc}
     * @see \Plugin\Controller\PageController::_initialize()
     */
    public function _initialize() {
        parent::_initialize();
        
        $this->hits         = D('Hits');
        $this->category     = D('Admin/Category');
        $this->apps         = D('Admin/Model');
        
        $this->advert       = D('Advert/Advert');
    }
    
    /**
     * 文章统计
     */
    public final function hits() {
        $module = I('get.module', '', 'ucwords');
        $catid = I('get.catid', 0, 'intval');
        $catdir = I('get.catdir', '');
        $id = I('get.id', 0, 'intval');
        
        $count = cms_config_array('Plugin', 'count')['hits'];
        list($min, $max) = explode(',', $count);
        $hits = 1;
        if ($max) $hits = rand($min, $max);
        
        $where              = [];
        $where['catdir']    = $catdir;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $ress = $this->category->where($where)->find();
        $catid = $catid ? : $ress['catid'];
        
        $where              = [];
        $where['catid']     = $catid;
        $where['siteid']    = ['IN', [0, cms_siteid()]];
        $ress = $this->category->where($where)->find();
        
        $where              = [];
        $where['modelid']   = $ress['modelid'];
        $table = $this->apps->where($where)->find()['table'];
        
        $data = D($table.'_data')->where(['mid' => $id])->find();
        if (empty($data['views'])) {
            D($table.'_data')->where(['mid' => $id])->save([
                'views' => 0
            ]);
        }
        D($table.'_data')->where(['mid' => $id])->setInc('views', $hits);
        
        $hitid = $module.'-'.$catid.'-'.$id;
        $where              = [];
        $where['hitid']     = $hitid;
        $ress = $this->hits->where($where)->find();
        
        $days = date('Ymd', $ress['updatetime']) == date('Ymd', time());
        $week = date('YW',  $ress['updatetime']) == date('YW',  time());
        $month= date('Ym',  $ress['updatetime']) == date('Ym',  time());
        
        $views              = [];
        $views['total']     = $ress['total'] + $hits;
        $views['days']      = $days  ? $ress['days']  + $hits : $hits;
        $views['week']      = $week  ? $ress['week']  + $hits : $hits;
        $views['month']     = $month ? $ress['month'] + $hits : $hits;
        $views['updatetime']= time();
        
        if ($ress) {
            $where          = [];
            $where['hitid'] = $hitid;
            $this->hits->where($where)->save($views);
        } else {
            $views['catid'] = $catid;
            $views['hitid'] = $hitid;
            $views['total'] = $data['views'] + $hits;
            $views['days']  = $data['views'] + $hits;
            $views['week']  = $data['views'] + $hits;
            $views['month'] = $data['views'] + $hits;
            $this->hits->add($views);
        }
    }
    
    public final function advert() {
        $advertid = I('get.advertid', 0, 'intval');
        $linkurl  = I('get.linkurl', '', 'cms_addslashes');
        
        $where              = [];
        $where['advertid']  = $advertid;
        $this->advert->where($where)->setInc('clicks');
        
        header('location: '.$linkurl);
    }
    
}
