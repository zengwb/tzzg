<?php

namespace Plugin\Controller;

defined('APP_STATUS') && APP_STATUS || exit('ACCESS DENIED.');

/**
 * 项目数据接口
 * 
 * @author T-01
 */
final class ProjectApiController extends PageController {
    
    private $modelid            =  7, // 项目模型
            $currentModel       = [],
            
            $projectModel       = [],
            $projectDataModel   = [],
            
            $linkageModel       = [];
    
    /**
     * {@inheritDoc}
     * @see \Plugin\Controller\PageController::_initialize()
     */
    public final function _initialize() {
        parent::_initialize();
        $this->currentModel     = D('Admin/Model');
        
        $where                  = [];
        $where['modelid']       = $this->modelid;
        $where['siteid']        = cms_siteid();
        $where['disabled']      =  0;
        
        $model = $this->currentModel->where($where)->find();
        $this->projectModel     = D($model['table']);
        $this->projectDataModel = D($model['table'].'_data');
        
        $this->linkageModel     = D('Admin/Linkage');
    }
    
    /**
     * 项目详情
     */
    public final function details() {
        $projectid = I('get.id', 0, 'intval');
        
        $where = [];
        $where['mid'] = $projectid;
        $where['disabled'] = 0;
        $main = $this->projectModel->where($where)->find();
        
        $where = [];
        $where['mid'] = $main['mid'];
        $data = $this->projectDataModel->where($where)->find();
        
        $temp = [];
        $temp['projectid'] = $main['mid']; // 项目ID
        $temp['catid'] = $main['catid']; // 隶属栏目
        $temp['name'] = $main['title']; // 项目名称
        $temp['name_style'] = unserialize($main['title_style']); // 标题样式
        
        $temp['inputtime'] = $main['inputtime']; // 添加时间
        $temp['updatetime'] = $main['updatetime']; // 修改时间
        
        $temp['investment'] = $main['investment']; // 项目总投资额（亿元）
        $temp['attract_investment'] = $main['attract_investment']; // 项目引资额（亿元）
        
        $coop = explode(',', $main['cooperation']);
        $name = [];
        foreach ($coop as $key => $val) {
            $name[$key] = $this->name($val);
        }
        $temp['cooperation'] = implode('、', $name); // 合作方式
        
        $temp['company'] = $main['company'];
        $temp['contact'] = explode("\n", $main['contact']);
        
        list($province, $city, $county) = explode(',', $main['city']);
        $temp['province'] = $this->name($province); // 省份
        $temp['city'] = $this->name($city); // 市州
        $temp['county'] = $this->name($county); // 区县
        
        $temp['address'] = $main['address'];
        
        list($level1, $level2, $level3) = explode(',', $main['industry_typeid']);
        $temp['industry_level1'] = $this->name($level1);
        $temp['industry_level2'] = $this->name($level2);
        $temp['industry_level3'] = $this->name($level3);
        
        $temp['introduce'] = cms_htmlspecialchars($data['content']);
        
        print_r($temp); exit();
        echo json_encode($temp);
    }
    
    /**
     * 获取联动菜单名称
     * 
     * @param integer   $linkageid  必填。菜单ID
     */
    private final function name($linkageid) {
        $where = [];
        $where['linkageid'] = $linkageid;
        return $this->linkageModel->where($where)->find()['name'];
    }
    
}